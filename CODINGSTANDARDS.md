Введение
--------

### Важность

Соблюдать единый стандарт кодирования в большом проекте (а umi.cms 2
большой проект) очень важно:

-   Это позволит сделать код более приятным и читаемым
-   Новые люди будут быстрее входить в проект
-   Некоторые пункты позволят избежать ошибок
-   Повышает общую скорость разработки

### Интерпретация

Эти правила обязательны к выполнению. Слова "должен", "следует",
"желательно", "может быть" и т.п. используются в своем прямом значении и
не подразумевают двойной трактовки.

### Применимость

Этот стандарт применим ко всему php-коду в рамках проекта UMI.CMS 2. Cтарайтесь соблюдать основные
положения из стандарта для php для js кода тоже.

Именования
----------

Именование очень важный момент при создании новых классов, методов и
переменных. Относитесь к этому ответственно, но не перебарщивайте.
Название должно давать представление назначении (класса, метода,
функции), либо о содержании (переменной).

### Имена функций

Постарайтесь избежать введения новых глобальных функций в систему. Как
правило, каждая функция является частью какой-нибудь концепции, поэтому
неэтично, например отрывать статические методы от класса. Название
функции :

1.  должно начинаться с маленькой буквы
2.  не должно содержать чисел (**my\_func1()**, **my\_func\_2()**)
3.  слова в названии должны разделяться заглавной буквой, а не
    подчеркиванием за исключением случаев, когда необходимо соблюсти
    стиль кодирования "родных" функций php

``` {.php}
checkInterfaceLang();
removeDirectory($dir);
```

### Имена классов

1.  Названия классов должны начинаться с маленькой буквы
2.  Знаки подчеркивания в названии класса недопустимы
3.  Слова в названии класса должны выделяться заглавной буквой
4.  Чтобы избежать возможные конфликты имен (например для класса
    "**object**") допускается использование префикса **umi**

``` {.php}
class matches {
...
}

class umiObject {
...
}
```

### Имена методов

1.  Должны начинаться с маленькой буквы
2.  Не должны содержать знаков подчеркивания
3.  Геттеры и сэттеры должны иметь соответствующий префикс "get" или
    "set"

Это в той же степени касается и статических методов.

``` {.php}
$foo->getName();

$foo->setName($name);

$foo->commit();

umiObjects::getInstance();
```

### Именование в зависимости от режима доступа

В зависимости от режима доступа имена переменных и методов **не должны
изменяться**. Это бесполезно и в некоторых случаях даже вредно.

``` {.php}
class foo {
    private $x, $y;

    public function foo() {
        ...
    }

    private function bar() {
        ...
    }
}
```

### Порядок методов в классе

Порядок методов в классе следует выбирать в зависимости от режима
доступа к ним. Сначала **public**, потом **protected**, потом
**private**. Именование методов в зависимости режима доступа не должно
изменяться.

``` {.php}
class foo {
    public function foo();
    public function bar();

    private function zoo();
}
```

### Имена аргументов в методах

1.  Должны начинаться с маленькой буквы
2.  Не должны содержать знака подчеркивания
3.  Каждое новое слово должно начинаться с большой буквы
4.  **Не должно содержать префикса, указывающего на тип переменной**
    ("\$iTotal", "\$oObject")

``` {.php}
public function foo($first, $secondParam);
```

### Имена интерфейсов

1.  Должны начинаться с буквы "i"
2.  Не должны содержать знака подчеркивания
3.  Каждое новое слово в названии интерфейса должно начинаться с большой
    буквы

``` {.php}
interface iTransaction {
...
}

interface iUmiObject {

}
```

### Имена аргументов в интерфейсах

1.  Должны начинаться с маленькой буквы
2.  Не должны содержать знака подчеркивания
3.  Каждое новое слово должно начинаться с большой буквы
4.  **Не должно содержать префикса, указывающего на тип переменной**
    ("\$iTotal", "\$oObject")
5.  При указании дефолтного значения переменной, знак равенства должен
    быть обособлен пробелами, это повышает читаемость кода

В отличие от методов класса, где допустимы небольшие расхождения, тут
эти правила должны соблюдаться всегда.

``` {.php}
interface iUmiObject {
    public function setName($name);
    public function setValue($propName, $propValue = null);
}
```

### Глобальные переменные

Глобальные переменные в этом проекте использовать запрещено. Еще раз:
запрещено, без вопросов.

### Константы

1.  Должны быть написаны в верхнем регистре
2.  Слова нужно разделять символом подчеркивания
3.  Должны начинаться с префикса "UMI\_", чтобы избежать коллизии

``` {.php}
define("UMI_SOME_CONSTANT", "1");
```

*Данное требование в настоящий момент не выполняется.*

Форматирование
--------------

### Отступы

Все отступы должны быть оформлены знаком табуляции. Использование
пробелов для визуализации вложенности недопустимо. Размер tab'ов каждый
разработчик может выставить у себя в редакторе. Более того, многие IDE
позволяют конвертировать пробельные отступы в табуляцию и обратно, если
по каким-либо соображениям табы для вас неприемлемы. Но код, который
попадает в репозиторий обязательно должен использовать только знаки
табуляции.

### Фигурные скобки

Управляющие фигурные скобки должны находится на той же строке, что
оператор. Перед открывающей скобкой необходим 1 пробел. Код в условиях
всегда нужно брать в фигурные скобки даже в том случае, если он состоит
из 1й строки.

``` {.php}
if($a > $b) {
    ...
}

function foo() {
    ...
}

if($x < 0) {
    $x = -$x;
} else {
        $x = 0;
}
```

У вас может быть свое мнение по этому поводу, но данный способ
оформления блоков - устоявшийся стиль в в проекте.

### switch

1.  Всегда определяйте блок **default** и размещайте там код для
    генерации соответствующей ошибке, если это ошибочный случай
2.  Использовать передачу управления можно
3.  Каждый case блок должен заканчиваться оператором "break", либо
    "return"
4.  case блок должен оформляться в фигурные скобки всегда, кроме тех
    случаев, когда он состоит из только из 1го оператора **return**

``` {.php}
switch($foo) {
    case "a":
    case "b":
    case "c": {
        $bar = "abc";
        break;
    }

    case "d": {
        $bar = "cba";
        break;
    }

    default: {
        throw new Exception("Error found");
    }
}

switch($bar) {
    case "a":
        return "abc";

    ...

    default: {
        return "nothing";
    }
}
```

### Тернарный оператор сравнения

1.  Конструкция с использование тернарного оператора должна влезать в
    экран по ширине
2.  Избегайте использования несколько тернарных операторов в одной
    строке
3.  Если условие является выражением, берите его в круглые скобки

``` {.php}
$foo = $isTrue ? "It's true" : "It is false";

$bar = ($a > $b) ? $a : $b;
```

### Знак "&"

Не смотря на то, что знак "&" должен относится к типу переменной, в
данном проекте принято ставить "&" перед "\$". Этот стиль следует
соблюдать.

PS. передача значения по ссылке это не лучшая практика, попробуйте
обойтись без нее

``` {.php}
function foo(&$bar, umiObject $zoo) {
    ...
}
```

### Пробелы в выражениях

В выражениях всегда используйте пробелы для отделения операторов. Каждый
оператор (в т.ч. и оператор присваивания) должен выделяться пробелами.
**Конкатенация - тоже оператор и должна быть обособлена пробелами!**

``` {.php}
$x = $x * ($x + 10 / $y);
$s = 'Это строка' . $x . ' для примера оформления ' . 'конкатенации';

for($i = 0; $i < sizeof($arr); $i++) {
    ...
}
```

### Ветвление

По мере возможности, необходимо избегать лишних блоков ветвлений,
например, заменяя блок условия:

``` {.php}
if (substr($arg, -4, 4) == ".tpl") {
    $arg = preg_replace('|[\/]{2,}|', '/', $arg);
    $res = umiRemoteFileGetter::get($arg);
    ...
    return def_module::parseTPLMacroses($res, cmsController::getInstance()->getCurrentElementId(), false, $parseVariables);
} else {
    throw new publicException(getLabel('error-resource-not-found', null, $arg));
}
```

на блок с отрицанием:

``` {.php}
if (substr($arg, -4, 4) != ".tpl") {
    throw new publicException(getLabel('error-resource-not-found', null, $arg));
}

$arg = preg_replace('|[\/]{2,}|', '/', $arg);
$res = umiRemoteFileGetter::get($arg);
...
return def_module::parseTPLMacroses($res, cmsController::getInstance()->getCurrentElementId(), false, $parseVariables);
```

Документирование
----------------

Все классы должны быть задокументированы в phpdoc'е. Комментарии должны
быть написаны на русском языке, файл сохранен в кодировке UTF-8 без BOM.

``` {.php}
<?php
        /**
    * Этот класс делает то-то и то-то...
    * и еще кое-что.
        **/
    class foo {
        ...
    };
```

Каждый публичный и протектед-метод должен быть документирован:

-   Указано его назначение и поведение
-   Список и тип его аргументов
-   Значение и тип значения, которое вернет метод
-   Если метод абстрактный, должен быть указан признак @abstract
-   Если метод статический, должен быть указан признак @static
-   Если метод выбрасывает исключения, должны быть указаны типы и
    назначение исключений, используя @throws
-   Если метод deprecated, указывать признак @deprecated

Приватные методы тоже желательно документировать, но это остается на
усмотрение разработчика.

``` {.php}
class x {
    /**
        * Делает фу
        * @param umiObject $a объект, который ...
        * @param int $boo число Бу
        * @return Boolean true в случае успеха
    */
    public function foo(umiObject $a, $boo) {
        ...
        return false;
    }
}

abstract class umiTemplater {
     * @static
     * @abstract
     * Подключает и возвращает все шаблоны из файла-источника
     * Должен быть реализован в конкретном шаблонизаторе
     * Использует кэширование загруженных ранее источников
     *
     * @param string $templatesSource - файл с шаблонами
     * @return array - все шаблоны из источника
     *
     * @throws publicException - если шаблон не удалось подключить
     */
    abstract public static function loadTemplates($templatesSource);
}
```

Дополнительные замечания
------------------------

### "Волшебные методы" \_\_set, \_\_get, \_\_call

Не используйте их. В php их применение очень ограничено.

### Обработка ошибок

Для генерации ошибок старайтесь использовать только исключения. В
системе предусмотрено 3 уровня исключений:

-   coreException - любая ошибка в ядре. Прерывает выполнение и выводит
    информацию об ошибке.
-   privateException - критическая ошибка в модуле или прикладном коде.
    Прерывает выполнение и выводит информацию об ошибке.
-   publicException - некритическая ошибка. Не прерывает выполнение всей
    системы. Индикация ошибки происходит в зависимости от того, где эта
    ошибка возникла.

``` {.php}
function foo($element_id) {
    $element = umiHierarchy::getInstance()->getElement($element_id);
    if($element instanceof umiHierarchyElement) {
        ...
    } else {
        throw new publicException(getLabel('error-expect-element'));
    }
}
```

=== Отступ в

<?php ?>
=== Код внутри

<?php ... ?>
в данном проекте принято считать первым уровнем вложенности. Поэтому он
должен выделяться табуляцией.

``` {.php}
<?php
    class foo implements iFoo {
        //...
    };
?>
```

### php-теги

В качестве открывающего php-тега можно использовать только "

<?php". Такие сокращения, как "<?" писать нельзя.
Закрывающий php-тэг обязателен. Некоторые редакторы создают файлы без него, это можно настроить в настройках.

Переносы строк, пробелы и табуляции до открывающего тэга и после закрывающего запрещены.

<source lang="php">
<?php
    //...
?>
</source>
### Размер методов и функций

Желательно, чтобы размер кода в функции или метода не превышал 50ти
строк. Это \~ высота экрана на широком мониторе. Максимум - 100 строк.
Самый максимум - 150. Помните, что если метод или функция требует так
много строк, то вы допустили ошибку, пытаясь засунуть в него слишком
много логики. Имеет смысл его зарефакторить.

### Суперглобальные массивы

Работу с суперглобальными массивами рекомендуется производить через
соответствующие функции:

-   \$\_REQUEST - **getRequest**(\$key)
-   \$\_SESSION - **getSession**(\$key)
-   \$\_SERVER - **getServer**(\$key)
-   \$\_COOKIE - **getCookie**(\$key)

``` {.php}
<?php
    $id = getRequest('param0');

    $ip = getServer('REMOTE_ADDR');
?>
```

### Одинарные и двойные кавычки

Вы можете использовать как одинарные, так и двойные кавычки.

### Форматирование SQL-запросов

Простые запросы должны укладываться в 1 строку в кавычках. Если
необходимо написать сложный запрос, имеет смысл разбить его на несколько
строк с отступами и использовать heredoc. Обратите внимание, что
использование sql-запросов допустимо только на уровне драйвера. В
прикладном коде прямые запросы к базе использовать нельзя.

``` {.php}
$sql = "SELECT COUNT(*) FROM cms3_hierarchy";

$sql = <<<SQL
SELECT COUNT(*) FROM cms3_hierarchy
    WHERE id IN (...)
        ORDER BY ord
SQL;
```

### Повторный вызов коллекции

Постарайтесь избежать повторных вызовов одного и того же синглтона в
рамках области видимости. Рекомендуется положить ее в переменную.

``` {.php}
<?php
    function foo($element_id) {
        $hierarchy = umiHierarchy::getInstance();
        $element = $hierarchy->getElement($element_id);

        ...

        $hierarchy->delElement($element_id);
    }
?>
```
