<?php
	require_once CURRENT_WORKING_DIR . '/libs/config.php';

	/**
	 * @var HTTPOutputBuffer $buffer
	 */
	$buffer = outputBuffer::current('HTTPOutputBuffer');
	$buffer->contentType('text/plain');
	$buffer->charset('utf-8');

	$cmsController = cmsController::getInstance();
	$config = mainConfiguration::getInstance();
	$domain = $cmsController->getCurrentDomain();

	$disallowedPages = new selector('pages');
	//$disallowedPages->where('robots_deny')->equals(1);
	$disallowedPages->where('robots_deny_new')->equals(1);
	$disallowedPages->where('is_active')->equals(array(1,0));
	$disallowedPages->option('no-permissions')->value(true);
	$disallowedPages->where('lang')->isnotnull();

	$rules = '';

	foreach ($disallowedPages as $page) {
		$rules .= 'Disallow: ' . $page->link . PHP_EOL;
	}

	$rules .= <<<RULES
Disallow: /admin
Disallow: /index.php
Disallow: /emarket/addToCompare
Disallow: /emarket/basket
Disallow: /go-out.php
Disallow: /cron.php
Disallow: /filemonitor.php
Disallow: /search
Disallow: /files/ormco-catalogue-2017.pdf
Disallow: /emarket/premoderate/
Disallow: /users/activate/
Disallow: /delivery/oplatit-zakaz-po-nomeru/
Disallow: /users/forget/
Disallow: /users/update_password/
Disallow: /emarket/purchase/result/successful/
Disallow: /users/registrate/
Disallow: /emarket/purchasing_one_step/
Disallow: /users/forget_do/

RULES;

	$customPath = CURRENT_WORKING_DIR . '/robots/' . $domain->getId() . '.robots.txt';

	if (file_exists($customPath)) {
		$customRobots = file_get_contents($customPath);

		if ($customRobots !== '') {
			if (strpos($customRobots, '%disallow_umi_pages%') !== false) {
				$customRobots = str_replace('%disallow_umi_pages%', $rules, $customRobots);
			}

			$buffer->push($customRobots);
			$buffer->end();
			exit();
		}
	}

	$rules = 'Disallow: /?' . PHP_EOL . $rules;

	//$buffer->push('User-Agent: Googlebot' . PHP_EOL);
	//$buffer->push($rules . PHP_EOL);
	//$buffer->push('User-Agent: Yandex' . PHP_EOL);
	//$buffer->push($rules);

	$crawlDelay = $config->get('seo', 'crawl-delay');
	$primaryWww = (bool) $config->get('seo', 'primary-www');
	$host = $domain->getHost();
	$host = preg_replace('/^www./', '', $host);

	if ($primaryWww) {
		$host = 'www.' . $host;
	}

	$host = getSelectedServerProtocol() . "://{$host}";

	//$buffer->push("Host: {$host}" . PHP_EOL);
	//$buffer->push("Crawl-delay: {$crawlDelay}" . PHP_EOL . PHP_EOL);
	$buffer->push('User-Agent: *' . PHP_EOL);
	$buffer->push($rules . PHP_EOL);
	$buffer->push("Sitemap: {$host}/sitemap.xml" . PHP_EOL);
	$buffer->push("Crawl-delay: {$crawlDelay}" . PHP_EOL);
	$buffer->end();
