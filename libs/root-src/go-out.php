<?php
	require_once CURRENT_WORKING_DIR . '/libs/config.php';

	$url = getRequest('url');
	$host = getServer('HTTP_HOST') ? str_replace('www.', '', getServer('HTTP_HOST')) : false;
	$referer = getServer('HTTP_REFERER') ? parse_url(getServer('HTTP_REFERER')) : false;

	$refererHost = false;

	if ($referer && isset($referer['host'])) {
		$refererHost = $referer['host'];
	}

	/**
	 * @var HTTPOutputBuffer $buffer
	 */
	$buffer = outputBuffer::current('HTTPOutputBuffer');
	$buffer->contentType('text/plain');
	$buffer->charset('utf-8');

	if (!$url || !$refererHost || !$host || strpos($refererHost, $host) === false) {
		$buffer->status(404);
		$buffer->end();
	}

	$buffer->redirect($url);