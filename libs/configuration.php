<?php
	/**
	 * Класс менеджера системных настроек. Синглтон.
	 */
	class mainConfiguration implements iConfiguration {
		/**
		 * @var iConfiguration|null $instance экземпляр класса
		 */
		private static $instance = null;
		/**
		 * @var array $ini значения настроек
		 */
		private $ini = [];
		/**
		 * @var bool $edited значения настроек были изменены
		 */
		private $edited = false;
		/**
		 * @var bool $readOnlyConfig заблокировано сохранение изменений конфигурационного файла
		 */
		private $readOnlyConfig = false;

		/**
		 * @inheritdoc
		 */
		public static function getInstance() {
			if (self::$instance === null) {
				self::$instance = new mainConfiguration();
			}

			return self::$instance;
		}

		/**
		 * @inheritdoc
		 */
		public function __destruct() {
			if ($this->edited) {
				$this->writeIni();
			}
		}

		/**
		 * @inheritdoc
		 */
		public function getParsedIni() {
			return $this->ini;
		}

		/**
		 * @inheritdoc
		 */
		public function get($section, $variable) {
			if (isset($this->ini[$section]) && isset($this->ini[$section][$variable])) {
				$value = $this->ini[$section][$variable];
				$value = $this->removeSingeQuotes($value);

				if ($section == 'session' && $variable == 'active-lifetime' && $value < 1) {
					$value = 1440;
				}

				return $value;
			}

			return null;
		}

		/**
		 * @inheritdoc
		 */
		public function set($section, $variable, $value) {
			if (!isset($this->ini[$section])) {
				$this->ini[$section] = [];
			}

			if ($value === null && isset($this->ini[$section][$variable])) {
				unset($this->ini[$section][$variable]);
			} else {
				if ($section == 'session' && $variable == 'active-lifetime' && $value < 1) {
					$value = 1440;
				}

				$this->ini[$section][$variable] = $value;
			}

			$this->edited = true;
		}


		/**
		 * @inheritdoc
		 */
		public function getList($section) {
			if (isset($this->ini[$section]) && is_array($this->ini[$section])) {
				return array_keys($this->ini[$section]);
			}

			return null;
		}

		/**
		 * @inheritdoc
		 */
		public function includeParam($key, array $params =  null) {
			static $defaultParams = [];

			$path = $this->get('includes', $key);

			if (strpos($path, "{") !== false) {
				if (class_exists('cmsController') && !sizeof($defaultParams)) {
					$cmsController = cmsController::getInstance();

					if ($lang = $cmsController->getCurrentLang()) {
						$defaultParams['lang'] = $cmsController->getCurrentLang()->getPrefix();
					}

					if ($lang = $cmsController->getCurrentLang()) {
						$defaultParams['domain'] = $cmsController->getCurrentDomain()->getHost();
					}
				}

				$params = (is_null($params)) ? $defaultParams : array_merge($params, $defaultParams);

				foreach ($params as $i => $v) {
					$path = str_replace('{' . $i . '}', $v,  $path);
				}
			}

			if (substr($path, 0, 2) == "~/") {
				$path = CURRENT_WORKING_DIR . substr($path, 1);
			}

			return $path;
		}

		/**
		 * @inheritdoc
		 */
		public function setReadOnlyConfig($flag = true) {
			$this->readOnlyConfig = (bool) $flag;
			return $this;
		}

		/**
		 * Конструктор
		 * @throws Exception
		 */
		private function __construct() {
			if (!is_readable(CONFIG_INI_PATH)) {
				throw new Exception("Can't find configuration file");
			}

			$this->ini = parse_ini_file(CONFIG_INI_PATH, true);

			if (isset($this->ini['session']) && isset($this->ini['session']['active-lifetime']) && $this->ini['session']['active-lifetime'] < 1) {
				$this->ini['session']['active-lifetime'] = 1440;
			}

			$this->replaceConfigToFastCgiParams();
			$this->defineStateDirPathConstants();
		}

		/**
		 * Реализация синглтон
		 */
		private function __clone(){}

		/**
		 * Сохраняет настройки в config.ini
		 * @return bool удалось ли сохранить изменения
		 */
		private function writeIni() {
			if ($this->isReadOnlyConfig()) {
				return false;
			}

			$iniString = "";

			foreach ($this->ini as $section => $variables) {
				if (empty($variables)) {
					continue;
				}

				$iniString .= "[{$section}]" . PHP_EOL;

				foreach ($variables as $name => $value) {
					if (is_array($value)) {
						foreach ($value as $valueItem) {
							$valueItem = ($valueItem !== '') ? '"' . $valueItem . '"' : '';
							$iniString .= "{$name}[] = {$valueItem}" . PHP_EOL;
						}
					} else {
						$value = ($value !== '') ? '"' . $value . '"' : '';
						$iniString .= "{$name} = {$value}"  . PHP_EOL;
					}
				}
				$iniString .= PHP_EOL;
			}

			return (bool) file_put_contents(CONFIG_INI_PATH, $iniString);
		}

		/**
		 * Удаляет одинарные ковычки из значений параметров config.ini
		 * @param mixed $value значение параметра
		 * @return mixed
		 */
		private function removeSingeQuotes($value) {
			if (is_string($value)) {
				return trim($value, "'");
			}

			if (is_array($value)) {
				return array_map([$this, 'removeSingeQuotes'], $value);
			}

			return $value;
		}

		/**
		 * Переопределяет значения параметров конфига переданными от вебсервера
		 */
		private function replaceConfigToFastCgiParams(){
			foreach ($_SERVER as $key => $val) {
				if (strpos($key, 'cp_') !== false) {
					$key = str_replace('cp_', '', $key);
					$key = str_replace('_', '.', $key);
					$key = explode('.', $key, 2);
					$this->ini[$key[0]][$key[1]] = $val;
				}
			}
		}

		/**
		 * Объявляет глобальные константы с путями до директорий, в которых хранится некоторое состояние системы:
		 *
		 * 1) USER_FILES_PATH - директория с пользовательскими файлами;
		 * 2) USER_IMAGES_PATH - директория с пользовательскими изображениями;
		 * 3) ERRORS_LOGS_PATH - директория с логом исключений;
		 * 4) SYS_TEMP_PATH - директория со временными файлами;
		 */
		private function defineStateDirPathConstants(){
			$variables = [
				'user-files-path' => '/files',
				'user-images-path' => '/images',
				'errors-logs-path' => '/errors/logs',
				'sys-temp-path' => '/sys-temp'
			];

			foreach ($variables as $key => $value) {
				$path = CURRENT_WORKING_DIR . $value;

				if (isset($this->ini['includes'][$key]) && $this->ini['includes'][$key] !== '') {
					$path = $this->ini['includes'][$key];
				}

				if (strpos($path, '~') !== false){
					$path = CURRENT_WORKING_DIR . substr($path,1);
				}

				$constantName = strtoupper(str_replace('-', '_', $key));

				if (!defined($constantName)) {
					define($constantName, $path);
				}
			}
		}

		/**
		 * Определяет заблокировано ли сохранение изменений конфигурационного файла
		 * @return bool
		 */
		private function isReadOnlyConfig() {
			return $this->readOnlyConfig;
		}
	}