<?php
	/**
	 * Возвращает title текущей страницы
	 * @return string
	 */
	function macros_title() {
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();
		$regedit = regedit::getInstance();

		if ($cmsController->getCurrentMode() == '') {
			if ($elementId = $cmsController->getCurrentElementId()) {
				if ($element = $hierarchy->getElement($elementId)) {
					if ($title = $element->getValue('title')) {
						return getTitleWithPrefix($title);
					}
				}
			}
		}

		if ($cmsController->currentTitle) {
			return getTitleWithPrefix($cmsController->currentTitle);
		}

		$langId = $cmsController->getCurrentLang()->getId();
		$domainId = $cmsController->getCurrentDomain()->getId();
		$defaultTitle = trim((string) $regedit->getVal("//settings/default_title/{$langId}/{$domainId}"));
		$title = $defaultTitle ?: macros_header();

		return getTitleWithPrefix($title);
	}

	/**
	 * Добавляет в title страницы префикс или постфикс
	 * @param string $title
	 * @return string
	 */
	function getTitleWithPrefix($title) {
		$titlePrefix = getTitlePrefix();

		if (!is_string($title)) {
			return $titlePrefix;
		}

		if (strpos($titlePrefix, '%title_string%') !== false) {
			return (string) str_replace('%title_string%', $title, $titlePrefix);
		}

		return strlen($titlePrefix) ? $titlePrefix . ' ' . $title : $title;
	}

	/**
	 * Возвращает префикс для title по домену и языковой версии
	 * @param int|bool $langId id языка, если не передано - возьмет текущий
	 * @param int|bool $domainId id домена, если не передано - возьмет текущий
	 * @return string
	 */
	function getTitlePrefix($langId = false, $domainId = false) {
		$cmsController = cmsController::getInstance();
		$currentLang = $cmsController->getCurrentLang();
		$currentDomain = $cmsController->getCurrentDomain();

		if (!$langId && !$currentLang instanceof lang) {
			return '';
		}

		if (!$domainId && !$currentDomain instanceof domain) {
			return '';
		}

		$langId = (!$langId) ? $currentLang->getId() : (int) $langId;
		$domainId = (!$domainId) ? $currentDomain->getId() : (int) $domainId;

		$regedit = regedit::getInstance();
		return trim((string) $regedit->getVal('//settings/title_prefix/' . $langId . '/' . $domainId));
	}

	function macros_sitename() {
		$cmsController = cmsController::getInstance();
		$domainId = $cmsController->getCurrentDomain()->getId();
		$langId = $cmsController->getCurrentLang()->getId();
		$regedit = regedit::getInstance();
		$siteName = $regedit->getVal("//settings/site_name/{$domainId}/{$langId}") ? $regedit->getVal("//settings/site_name/{$domainId}/{$langId}") : $regedit->getVal('//settings/site_name');
		return $siteName;
	}

	function macros_header() {
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		if ($cmsController->currentHeader) {
			return $cmsController->currentHeader;
		}

		if ($elementId = $cmsController->getCurrentElementId()) {
			if ($element = $hierarchy->getElement($elementId)) {
				$header = ($tmp = $element->getValue('h1')) ? $tmp : '';
				return $header;
			}
		}

		$currentModule = $cmsController->getCurrentModule();
		$currentMethod = $cmsController->getCurrentMethod();

		if (isset($cmsController->langs[$currentModule][$currentMethod])) {
			return $cmsController->langs[$currentModule][$currentMethod];
		} else {
			return false;
		}
	}

	function macros_systemBuild() {
		return regedit::getInstance()->getVal('//modules/autoupdate/system_build');
	}

	function macros_menu() {
		$cmsController = cmsController::getInstance();
		$contentModule = $cmsController->getModule('content');

		return ($contentModule instanceof def_module) ? $contentModule->menu() : '';
	}

	function macros_describtion() {
		$cmsController = cmsController::getInstance();
		$regedit = regedit::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		$domainId = $cmsController->getCurrentDomain()->getId();
		$langId = $cmsController->getCurrentLang()->getId();

		$description = '';
		if ($elementId = $cmsController->getCurrentElementId()) {
			if ($element = $hierarchy->getElement($elementId)) {
				$description = $element->getValue('meta_descriptions');
			}
		}
		if (!$description) {
			$description = $regedit->getVal('//settings/meta_description/' . $langId . '/' . $domainId);
		}

		return $description;
	}

	function macros_keywords() {
		$cmsController = cmsController::getInstance();
		$regedit = regedit::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		$domainId = $cmsController->getCurrentDomain()->getId();
		$langId = $cmsController->getCurrentLang()->getId();

		$keywords = '';
		if ($elementId = $cmsController->getCurrentElementId()) {
			if ($element = $hierarchy->getElement($elementId)) {
				$keywords = $element->getValue('meta_keywords');
			}
		}
		if (!$keywords) {
			$keywords = $regedit->getVal('//settings/meta_keywords/' . $langId . '/' . $domainId);
		}

		return $keywords;
	}

	function macros_returnPid() {
		return cmsController::getInstance()->getCurrentElementId();
	}

	function macros_returnPreLang() {
		return cmsController::getInstance()->getPreLang();
	}

	function macros_returnDomain() {
		return getServer('HTTP_HOST');
	}

	function macros_returnDomainFloated() {
		$cmsController = cmsController::getInstance();

		if ($cmsController->getCurrentMode() == '') {
			return getServer('HTTP_HOST');
		} else {
			$arr = [];
			if (is_numeric(getRequest('param0'))) {
				$arr[] = getRequest('param0');
			}

			if (is_numeric(getRequest('param1'))) {
				$arr[] = getRequest('param1');
			}

			if (getRequest('parent')) {
				$arr[] = getRequest('parent');
			}

			foreach ($arr as $c) {
				if (is_numeric($c)) {
					try {
						if ($element = umiHierarchy::getInstance()->getElement($c)) {
							$domain_id = $element->getDomainId();
							if ($domain = domainsCollection::getInstance()->getDomain($domain_id)) {
								return $domain->getHost();
							}
						}
					} catch (baseException $e) {
						//Do nothing
					}
				}

				if (is_string($c)) {
					if ($domain_id = domainsCollection::getInstance()->getDomainId($c)) {
						if ($domain = domainsCollection::getInstance()->getDomain($domain_id)) {
							return $domain->getHost();
						}
					}
				}
			}

			return getServer('HTTP_HOST');
		}
	}

	function macros_curr_time() {
		return time();
	}

	function macros_skin_path() {
		if (getRequest('skin_sel')) {
			return getRequest('skin_sel');
		}

		$cookieJar = \UmiCms\Service::CookieJar();
		return ($cookieJar->get('skin')) ? $cookieJar->get('skin') : regedit::getInstance()->getVal('//skins');
	}

	function macros_current_user_id() {
		$auth = UmiCms\Service::Auth();
		return $auth->getUserId();
	}

	function macros_current_version_line() {
		if (defined('CURRENT_VERSION_LINE')) {
			return CURRENT_VERSION_LINE;
		} else {
			return 'pro';
		}
	}

	function macros_catched_errors() {
		$res = '';
		foreach (baseException::$catchedExceptions as $exception) {
			$res .= '<p>' . $exception->getMessage() . '</p>';
		}
		return $res;
	}

	function macros_current_alt_name() {
		$cmsController = cmsController::getInstance();

		if ($element_id = $cmsController->getCurrentElementId()) {
			if ($element = umiHierarchy::getInstance()->getElement($element_id)) {
				return $element->getAltName();
			} else {
				return '';
			}
		} else {
			return '';
		}
	}

	function macros_returnParentId() {
		$cmsController = cmsController::getInstance();

		if ($element_id = $cmsController->getCurrentElementId()) {
			if ($element = umiHierarchy::getInstance()->getElement($element_id)) {
				return $element->getParentId();
			} else {
				return '';
			}
		} else {
			return '';
		}
	}

	/**
	 * Возвращает значение csrf-токена
	 * @return bool|mixed|null
	 */
	function macros_csrf() {
		return \UmiCms\Service::Session()->get('csrf_token');
	}

	/** @deprecated */
	function macros_content() {
		static $res;
		if (!is_null($res)) {
			return $res;
		}

		$cmsController = cmsController::getInstance();

		$current_module = $cmsController->getCurrentModule();
		$current_method = $cmsController->getCurrentMethod();

		$previousValue = $cmsController->isContentMode;
		$cmsController->isContentMode = true;

		if ($module = $cmsController->getModule($current_module)) {
			$pid = $cmsController->getCurrentElementId();
			$permissions = permissionsCollection::getInstance();
			$templater = $cmsController->getCurrentTemplater();
			$isAdmin = $permissions->isAdmin();

			if ($pid) {
				$auth = UmiCms\Service::Auth();
				list($r, $w) = $permissions->isAllowedObject($auth->getUserId(), $pid);
				if ($r) {
					$is_element_allowed = true;
				} else {
					$is_element_allowed = false;
				}
			} else {
				$is_element_allowed = true;
			}

			if (system_is_allowed($current_module, $current_method) && ($is_element_allowed)) {
				if ($parsedContent = $cmsController->parsedContent) {
					return $parsedContent;
				}
				if ($cmsController->getCurrentMode() == 'admin') {
					try {
						if (!$templater->getIsInited()) {
							$res = $module->cms_callMethod($current_method, null);
						}
					} catch (publicException $e) {
						$templater->setDataSet($e);
						return $res = false;
					}
				} else {
					try {
						$res = $module->cms_callMethod($current_method, null);
					} catch (publicException $e) {
						$res = $e->getMessage();
					}
					$res = system_parse_short_calls($res);
					$res = str_replace('%content%', '%content ', $res);

					$res = templater::getInstance()->parseInput($res);

					$res = system_parse_short_calls($res);
				}

				if ($res !== false && is_array($res) == false) {
					if ($cmsController->getCurrentMode() != 'admin' && stripos($res, '%cut%') !== false) {
						if (array_key_exists('cut', $_REQUEST)) {
							if ($_REQUEST['cut'] == 'all') {
								$_REQUEST['cut_pages'] = 0;
								return str_ireplace('%cut%', '', $res);
							}
							$cut = (int) $_REQUEST['cut'];
						} else {
							$cut = 0;
						}

						$res_arr = preg_split('%cut%i', $res);

						if ($cut > (sizeof($res_arr) - 1)) {
							$cut = sizeof($res_arr) - 1;
						}
						if ($cut < 0) {
							$cut = 0;
						}

						$_REQUEST['cut_pages'] = sizeof($res_arr);
						$_REQUEST['cut_curr_page'] = $cut;

						$res = $res_arr[$cut];
					}

					$cmsControllerInstance = $cmsController;
					$cmsControllerInstance->parsedContent = $res;
					$cmsController->isContentMode = $previousValue;
					return $res;
				} else {
					$cmsController->isContentMode = $previousValue;
					return $res = '<notice>%core_templater% %core_error_nullvalue%</notice>';
				}
			} else {
				if ($cmsController->getCurrentMode() == 'admin' && $isAdmin) {
					if ($current_module == 'content' && $current_method == 'sitetree') {
						$regedit = regedit::getInstance();

						$modules = $regedit->getList('//modules');
						foreach ($modules as $item) {
							list($module) = $item;
							if (system_is_allowed($module) && $module != 'content') {
								$module_inst = $cmsController->getModule($module);
								$url = $module_inst->pre_lang . '/admin/' . $module . '/';
								$module_inst->redirect($url);
							}
						}
					}
				}

				if ($module = $cmsController->getModule('users')) {
					header('Status: 401 Unauthorized');

					$cmsController->setCurrentModule('users');
					$cmsController->setCurrentMethod('login');
					$cmsController->isContentMode = $previousValue;

					if ($isAdmin) {
						$module = $cmsController->getModule($current_module);
						if ($module->isMethodExists($current_method) == false) {
							$url = $module_inst->pre_lang . '/admin/content/sitetree/';
							$module->redirect($url);
						}
						if ($cmsController->getCurrentMode() == 'admin') {
							$e = new requreMoreAdminPermissionsException(getLabel('error-require-more-permissions'));
							$templater->setDataSet($e);
						}

						return $res = '<p><warning>%core_error_nopermission%</warning></p>' . $module->login();
					} else {
						if ($templater instanceof xslAdminTemplater && $templater->getParsed()) {
							throw new requireAdminPermissionsException('No permissions');
						}
						return $res = $module->login();
					}
				}

				return $res = '<warning>%core_templater% %core_error_nullvalue% %core_error_nopermission%</warning>';
			}
		}
		$cmsController->isContentMode = $previousValue;
		return $res = '%core_templater% %core_error_unknown%';
	}
