<?php
	$classes = [
		'iOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/iOutputBuffer.php'
		],

		'outputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/outputBuffer.php'
		],

		'iMapContainer' => [
			SYS_KERNEL_PATH . 'patterns/iMapContainer.php'
		],

		'iUmiEntinty' => [
			SYS_KERNEL_PATH . 'patterns/iUmiEntinty.php'
		],

		'iSingleton' => [
			SYS_KERNEL_PATH . 'patterns/iSingleton.php'
		],

		'iDomain' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iDomain.php'
		],

		'iDomainMirror' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iDomainMirror.php'
		],

		'iDomainsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iDomainsCollection.php'
		],

		'iLang' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iLang.php'
		],

		'iLangsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iLangsCollection.php'
		],

		'iTemplate' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iTemplate.php'
		],

		'iTemplatesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iTemplatesCollection.php'
		],

		'iUmiHierarchy' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iUmiHierarchy.php'
		],

		'iUmiHierarchyElement' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iUmiHierarchyElement.php'
		],

		'iUmiHierarchyType' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iUmiHierarchyType.php'
		],

		'iUmiHierarchyTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/iUmiHierarchyTypesCollection.php'
		],

		'iUmiField' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiField.php'
		],

		'iUmiFieldsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiFieldsCollection.php'
		],

		'iUmiFieldsGroup' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiFieldsGroup.php'
		],

		'iUmiFieldType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiFieldType.php'
		],

		'iUmiFieldTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiFieldTypesCollection.php'
		],

		'iUmiObject' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiObject.php'
		],

		'iUmiObjectProperty' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiObjectProperty.php'
		],

		'iUmiObjectsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiObjectsCollection.php'
		],

		'iUmiObjectType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiObjectType.php'
		],

		'iUmiObjectTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/iUmiObjectTypesCollection.php'
		],

		'iUmiEventsController' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/iUmiEventsController.php'
		],

		'iUmiEventPoint' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/iUmiEventPoint.php'
		],

		'iUmiEventListener' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/iUmiEventListener.php'
		],

		'iUmiMessage' => [
			SYS_KERNEL_PATH . 'subsystems/messages/iUmiMessage.php'
		],

		'iUmiMessages' => [
			SYS_KERNEL_PATH . 'subsystems/messages/iUmiMessages.php'
		],

		'iXmlTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/iXmlTranslator.php'
		],

		'singleton' => [
			SYS_KERNEL_PATH . 'patterns/singletone.php'
		],

		'umiEntinty' => [
			SYS_KERNEL_PATH . 'patterns/umiEntinty.php'
		],

		'baseException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/baseException.php'
		],

		'coreException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/coreException.php'
		],

		'coreBreakEventsException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/coreBreakEventsException.php'
		],

		'libXMLErrorException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/libXMLErrorException.php'
		],

		'selectorException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/selectorException.php'
		],

		'databaseException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/databaseException.php'
		],

		'wrongParamException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/wrongParamException.php'
		],

		'errorPanicException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/errorPanicException.php'
		],

		'fieldRestrictionException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/fieldRestrictionException.php'
		],

		'breakException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/breakException.php'
		],

		'wrongValueException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/wrongValueException.php'
		],

		'valueRequiredException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/valueRequiredException.php'
		],

		'publicException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/publicException.php'
		],

		'publicAdminException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/publicAdminException.php'
		],

		'maxKeysCountExceedingException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/maxKeysCountExceedingException.php'
		],

		'noObjectsFoundForIndexingException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/noObjectsFoundForIndexingException.php'
		],

		'expectElementException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/expectElementException.php'
		],

		'expectObjectException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/expectObjectException.php'
		],

		'expectObjectTypeException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/expectObjectTypeException.php'
		],

		'requireAdminPermissionsException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/requireAdminPermissionsException.php'
		],

		'requreMoreAdminPermissionsException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/requreMoreAdminPermissionsException.php'
		],

		'requireAdminParamException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/requireAdminParamException.php'
		],

		'wrongElementTypeAdminException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/wrongElementTypeAdminException.php'
		],

		'publicAdminPageLimitException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/publicAdminPageLimitException.php'
		],

		'publicAdminLicenseLimitException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/publicAdminLicenseLimitException.php'
		],

		'callbackNotFoundException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/callbackNotFoundException.php'
		],

		'maxIterationsExeededException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/maxIterationsExeededException.php'
		],

		'DependencyNotInjectedException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/DependencyNotInjectedException.php'
		],

		'umiRemoteFileGetterException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/umiRemoteFileGetterException.php'
		],

		'xsltOnlyException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/xsltOnlyException.php'
		],

		'tplOnlyException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/tplOnlyException.php'
		],

		'privateException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/privateException.php'
		],

		'ConnectionPool' => [
			SYS_KERNEL_PATH . 'subsystems/database/ConnectionPool.php'
		],

		'IConnection' => [
			SYS_KERNEL_PATH . 'subsystems/database/IConnection.php'
		],

		'IQueryResult' => [
			SYS_KERNEL_PATH . 'subsystems/database/IQueryResult.php'
		],

		'mysqlConnection' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/mysqlConnection.php'
		],

		'mysqlQueryResult' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/mysqlQueryResult.php'
		],

		'translatorWrapper' => [
			SYS_KERNEL_PATH . "utils/translators/translatorWrapper.php"
		],

		'umiExceptionHandler' => [
			SYS_ERRORS_PATH . 'umiExceptionHandler.php'
		],

		'iLogger' => [
			SYS_KERNEL_PATH . "utils/logger/iLogger.php"
		],

		'logger' => [
			SYS_KERNEL_PATH . "utils/logger/logger.php"
		],

		'UmiCms\System\Patterns\iArrayContainer' => [
			SYS_KERNEL_PATH . "patterns/iArrayContainer.php"
		],

		'UmiCms\System\Patterns\ArrayContainer' => [
			SYS_KERNEL_PATH . "patterns/ArrayContainer.php"
		],

		'UmiCms\System\Request\Cookies' => [
			SYS_KERNEL_PATH . "subsystems/Request/Cookies.php"
		],

		'UmiCms\System\Request\Files' => [
			SYS_KERNEL_PATH . "subsystems/Request/Files.php"
		],

		'UmiCms\System\Request\Get' => [
			SYS_KERNEL_PATH . "subsystems/Request/Get.php"
		],

		'UmiCms\System\Request\iCookies' => [
			SYS_KERNEL_PATH . "subsystems/Request/iCookies.php"
		],

		'UmiCms\System\Request\iFiles' => [
			SYS_KERNEL_PATH . "subsystems/Request/iFiles.php"
		],

		'UmiCms\System\Request\iGet' => [
			SYS_KERNEL_PATH . "subsystems/Request/iGet.php"
		],

		'UmiCms\System\Request\iPost' => [
			SYS_KERNEL_PATH . "subsystems/Request/iPost.php"
		],

		'UmiCms\System\Request\iRequest' => [
			SYS_KERNEL_PATH . "subsystems/Request/iRequest.php"
		],

		'UmiCms\System\Request\iServer' => [
			SYS_KERNEL_PATH . "subsystems/Request/iServer.php"
		],

		'UmiCms\System\Request\Post' => [
			SYS_KERNEL_PATH . "subsystems/Request/Post.php"
		],

		'UmiCms\System\Request\Request' => [
			SYS_KERNEL_PATH . "subsystems/Request/Request.php"
		],

		'UmiCms\System\Request\Server' => [
			SYS_KERNEL_PATH . "subsystems/Request/Server.php"
		],

		'UmiCms\System\Cookies\Cookie' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/Cookie.php"
		],

		'UmiCms\System\Cookies\CookieJar' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/CookieJar.php"
		],

		'UmiCms\System\Cookies\Factory' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/Factory.php"
		],

		'UmiCms\System\Cookies\iCookie' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/iCookie.php"
		],

		'UmiCms\System\Cookies\iCookieJar' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/iCookieJar.php"
		],

		'UmiCms\System\Cookies\iFactory' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/iFactory.php"
		],

		'UmiCms\System\Cookies\iResponsePool' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/iResponsePool.php"
		],

		'UmiCms\System\Cookies\ResponsePool' => [
			SYS_KERNEL_PATH . "subsystems/Cookies/ResponsePool.php"
		],

		'regedit' => [
			SYS_KERNEL_PATH . 'subsystems/regedit/iRegedit.php',
			SYS_KERNEL_PATH . 'subsystems/regedit/regedit.php'
		],

		'searchModel' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/iSearchModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/search/searchModel.php'
		],

		'FilterIndexGenerator' => [
			SYS_KERNEL_PATH . 'subsystems/models/filter/FilterIndexGenerator.php'
		],

		'FilterQueriesMaker' => [
			SYS_KERNEL_PATH . 'subsystems/models/filter/FilterQueriesMaker.php'
		],

		'permissionsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/permissions/iPermissionsCollection.php',
			SYS_KERNEL_PATH . 'subsystems/models/permissions/permissionsCollection.php'
		],

		'ranges' => [
			SYS_KERNEL_PATH . 'utils/ranges/ranges.php'
		],

		'translit' => [
			SYS_KERNEL_PATH . 'utils/translit/iTranslit.php',
			SYS_KERNEL_PATH . 'utils/translit/translit.php'
		],

		'dbSchemeConverter' => [
			SYS_KERNEL_PATH . 'utils/dbSchemeConverter/iDbSchemeConverter.php',
			SYS_KERNEL_PATH . 'utils/dbSchemeConverter/dbSchemeConverter.php'
		],

		'umiTarCreator' => [
			SYS_KERNEL_PATH . 'utils/tar/umiTarCreator.php'
		],

		'umiTarExtracter' => [
			SYS_KERNEL_PATH . 'utils/tar/umiTarExtracter.php'
		],

		'adminModuleTabs' => [
			SYS_KERNEL_PATH . 'utils/adminModuleTabs/iAdminModuleTabs.php',
			SYS_KERNEL_PATH . 'utils/adminModuleTabs/adminModuleTabs.php'
		],

		'umiTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/umiTemplater.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/IFullResult.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/IPhpExtension.php',
		],

		'umiTemplaterXSLT' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterXSLT.php'
		],

		'umiTemplaterTPL' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterTPL.php'
		],

		'umiTemplaterPHP' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterPHP.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/engine/PhpTemplateEngine.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/engine/ViewPhpExtension.php'
		],

		'templater' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iTemplater.php',
			SYS_KERNEL_PATH . 'subsystems/Deprecated/templater.php'
		],

		'tplTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/tplTemplater.php'
		],

		'xslTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/xslTemplater.php'
		],

		'xslAdminTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/xslAdminTemplater.php'
		],

		'cmsController' => [
			SYS_KERNEL_PATH . 'subsystems/cmsController/iCmsController.php',
			SYS_KERNEL_PATH . 'subsystems/cmsController/cmsController.php'
		],

		'umiExporter' => [
			SYS_KERNEL_PATH . 'subsystems/export/iUmiExporter.php',
			SYS_KERNEL_PATH . 'subsystems/export/umiExporter.php'
		],

		'xmlExporter' => [
			SYS_KERNEL_PATH . 'subsystems/export/iXmlExporter.php',
			SYS_KERNEL_PATH . 'subsystems/export/xmlExporter.php'
		],

		'umiXmlExporter' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iUmiXmlExporter.php',
			SYS_KERNEL_PATH . 'subsystems/Deprecated/umiXmlExporter.php'
		],

		'umiXmlImporter' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iUmiXmlImporter.php',
			SYS_KERNEL_PATH . 'subsystems/Deprecated/umiXmlImporter.php'
		],

		'xmlImporter' => [
			SYS_KERNEL_PATH . 'subsystems/import/iXmlImporter.php',
			SYS_KERNEL_PATH . 'subsystems/import/xmlImporter.php'
		],

		'iUmiImportRelations' => [
			SYS_KERNEL_PATH . 'subsystems/import/iUmiImportRelations.php'
		],

		'umiImportRelations' => [
			SYS_KERNEL_PATH . 'subsystems/import/umiImportRelations.php'
		],

		'umiImportSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/iUmiImportSplitter.php',
			SYS_KERNEL_PATH . 'subsystems/import/umiImportSplitter.php'
		],

		'language_morph' => [
			SYS_KERNEL_PATH . 'utils/languageMorph/iLanguageMorph.php',
			SYS_KERNEL_PATH . 'utils/languageMorph/language_morph.php'
		],

		'umiDate' => [
			SYS_KERNEL_PATH . 'entities/umiDate/iUmiDate.php',
			SYS_KERNEL_PATH . 'entities/umiDate/umiDate.php'
		],

		'iUmiFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/iUmiFile.php',
		],

		'umiFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/umiFile.php'
		],

		'iUmiImageFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/iUmiImageFile.php',
		],

		'umiImageFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/umiImageFile.php'
		],

		'umiRemoteFileGetter' => [
			SYS_KERNEL_PATH . 'entities/umiFile/umiRemoteFileGetter.php'
		],

		'umiDirectory' => [
			SYS_KERNEL_PATH . 'entities/umiDirectory/iUmiDirectory.php',
			SYS_KERNEL_PATH . 'entities/umiDirectory/umiDirectory.php'
		],

		'umiMail' => [
			SYS_KERNEL_PATH . 'entities/umiMail/interfaces.php',
			SYS_KERNEL_PATH . 'entities/umiMail/umiMail.php',
			SYS_KERNEL_PATH . 'entities/umiMail/umiMimePart.php'
		],

		'umiPagenum' => [
			SYS_KERNEL_PATH . 'utils/pagenum/iPagenum.php',
			SYS_KERNEL_PATH . 'utils/pagenum/pagenum.php'
		],

		'umiCaptcha' => [
			SYS_KERNEL_PATH . 'utils/captcha/iUmiCaptcha.php',
			SYS_KERNEL_PATH . 'utils/captcha/umiCaptcha.php'
		],

		'UmiCms\Classes\System\Utils\Captcha\Strategies\CaptchaStrategy' => [
			SYS_KERNEL_PATH . 'utils/captcha/strategies/CaptchaStrategy.php',
		],

		'UmiCms\Classes\System\Utils\Captcha\Strategies\ClassicCaptcha' => [
			SYS_KERNEL_PATH . 'utils/captcha/strategies/ClassicCaptcha.php',
		],

		'UmiCms\Classes\System\Utils\Captcha\Strategies\GoogleRecaptcha' => [
			SYS_KERNEL_PATH . 'utils/captcha/strategies/GoogleRecaptcha.php',
		],

		'UmiCms\Classes\System\Utils\Captcha\Strategies\NullCaptcha' => [
			SYS_KERNEL_PATH . 'utils/captcha/strategies/NullCaptcha.php',
		],

		'UmiCms\System\Protection\CsrfProtection' => [
			SYS_KERNEL_PATH . 'subsystems/protection/CsrfProtection.php'
		],

		'UmiCms\System\Protection\Security' => [
			SYS_KERNEL_PATH . 'subsystems/protection/Security.php'
		],

		'UmiCms\System\Protection\CsrfException' => [
			SYS_KERNEL_PATH . 'subsystems/protection/CsrfException.php'
		],

		'UmiCms\System\Protection\OriginException' => [
			SYS_KERNEL_PATH . 'subsystems/protection/OriginException.php'
		],

		'UmiCms\System\Protection\ReferrerException' => [
			SYS_KERNEL_PATH . 'subsystems/protection/ReferrerException.php'
		],

		'lang' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/lang.php'
		],

		'langsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/langsCollection.php'
		],

		'domain' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domain.php'
		],

		'domainMirror' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domainMirror.php'
		],

		'domainsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domainsCollection.php'
		],

		'template' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/template.php'
		],

		'templatesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/templatesCollection.php'
		],

		'umiHierarchyType' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyType.php'
		],

		'umiHierarchyTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyTypesCollection.php'
		],

		'umiHierarchyElement' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyElement.php'
		],

		'umiHierarchy' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchy.php'
		],

		'umiSelection' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iUmiSelection.php',
			SYS_KERNEL_PATH . 'subsystems/Deprecated/umiSelection.php'
		],

		'umiSelectionsParser' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iUmiSelectionsParser.php',
			SYS_KERNEL_PATH . 'subsystems/Deprecated/umiSelectionsParser.php'
		],

		'umiFieldType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldType.php'
		],

		'umiField' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiField.php'
		],

		'umiFieldsGroup' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldsGroup.php'
		],

		'umiObjectType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectType.php'
		],

		'umiTypesHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiTypesHelper.php'
		],

		'umiLinksHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiLinksHelper.php'
		],

		'umiPropertiesHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiPropertiesHelper.php'
		],

		'ClassConfig' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/ClassConfig.php'
		],

		'umiObjectProperty' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectProperty.php'
		],

		'umiObject' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObject.php'
		],

		'umiFieldTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldTypesCollection.php'
		],

		'umiFieldsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldsCollection.php'
		],

		'umiObjectTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectTypesCollection.php'
		],

		'umiObjectsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectsCollection.php'
		],

		'cacheFrontend' => [
			SYS_KERNEL_PATH . 'subsystems/cache/iCacheFrontend.php',
			SYS_KERNEL_PATH . 'subsystems/cache/cacheFrontend.php'
		],

		'umiEventPoint' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventPoint.php'
		],

		'umiEventListener' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventListener.php'
		],

		'umiEventsController' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventsController.php'
		],

		'umiEventFeed' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeed.php'
		],

		'umiEventFeedType' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeedType.php'
		],

		'umiEventFeedUser' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeedUser.php'
		],

		'backupModel' => [
			SYS_KERNEL_PATH . 'subsystems/models/backup/iBackupModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/backup/backupModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/backup/backupChange.php'
		],

		'UmiCms\Classes\System\Translators\TranslatorFactory' => [
			SYS_KERNEL_PATH . 'utils/translators/TranslatorFactory.php'
		],

		'xmlTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/xmlTranslator.php'
		],

		'jsonTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/jsonTranslator.php'
		],

		'UmiCms\Classes\System\Translators\PhpTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/phpTranslator.php'
		],

		'baseModuleAdmin' => [
			SYS_DEF_MODULE_PATH . 'baseModuleAdmin.php'
		],

		'matches' => [
			SYS_KERNEL_PATH . 'subsystems/matches/iMatches.php',
			SYS_KERNEL_PATH . 'subsystems/matches/matches.php'
		],

		'baseSerialize' => [
			SYS_KERNEL_PATH . 'subsystems/matches/serializes/iBaseSerialize.php',
			SYS_KERNEL_PATH . 'subsystems/matches/serializes/baseSerialize.php'
		],

		'RSSFeed' => [
			SYS_KERNEL_PATH . 'entities/RSS/interfaces.php',
			SYS_KERNEL_PATH . 'entities/RSS/RSSFeed.php',
			SYS_KERNEL_PATH . 'entities/RSS/RSSItem.php'
		],

		'umiCron' => [
			SYS_KERNEL_PATH . 'subsystems/cron/iUmiCron.php',
			SYS_KERNEL_PATH . 'subsystems/cron/umiCron.php'
		],

		'umiMessages' => [
			SYS_KERNEL_PATH . 'subsystems/messages/umiMessages.php'
		],

		'umiMessage' => [
			SYS_KERNEL_PATH . 'subsystems/messages/umiMessage.php'
		],

		'umiSubscriber' => [
			SYS_KERNEL_PATH . 'utils/subscriber/iUmiSubscriber.php',
			SYS_KERNEL_PATH . 'utils/subscriber/umiSubscriber.php'
		],

		'umiOpenSSL' => [
			SYS_KERNEL_PATH . 'utils/openssl/iUmiOpenSSL.php',
			SYS_KERNEL_PATH . 'utils/openssl/umiOpenSSL.php'
		],

		'umiLogger' => [
			SYS_KERNEL_PATH . 'utils/logger/iLogger.php',
			SYS_KERNEL_PATH . 'utils/logger/logger.php'
		],

		'umiConversion' => [
			SYS_KERNEL_PATH . 'utils/conversion/umiConversion.php',
			SYS_KERNEL_PATH . 'utils/conversion/iGenericConversion.php'
		],

		'__charssequences_ru' => [
			SYS_KERNEL_PATH . 'utils/conversion/__charssequences.ru.php'
		],

		'garbageCollector' => [
			SYS_KERNEL_PATH . 'subsystems/garbageCollector/iGarbageCollector.php',
			SYS_KERNEL_PATH . 'subsystems/garbageCollector/garbageCollector.php'
		],

		'manifest' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/interfaces.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/manifest.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/sampleCallback.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/jsonCallback.php'
		],

		'transaction' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/transaction.php'
		],

		'atomicAction' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/atomicAction.php'
		],

		'umiBranch' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiBranch.php'
		],

		'umiObjectPropertyBoolean' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyBoolean.php'
		],

		'umiObjectPropertyImgFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyImgFile.php'
		],

		'umiObjectPropertyRelation' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyRelation.php'
		],

		'umiObjectPropertyTags' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyTags.php'
		],

		'umiObjectPropertyDate' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyDate.php'
		],

		'umiObjectPropertyInt' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyInt.php'
		],

		'umiObjectPropertyString' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyString.php'
		],

		'umiObjectPropertyText' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyText.php'
		],

		'umiObjectPropertyFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyFile.php'
		],

		'umiObjectPropertyPassword' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyPassword.php'
		],

		'umiObjectPropertyWYSIWYG' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyWYSIWYG.php'
		],

		'umiObjectPropertyFloat' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyFloat.php'
		],

		'umiObjectPropertyPrice' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyPrice.php'
		],

		'umiObjectPropertySymlink' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertySymlink.php'
		],

		'umiObjectPropertyCounter' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyCounter.php'
		],

		'umiObjectPropertyOptioned' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyOptioned.php'
		],

		'umiObjectPropertyColor' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyColor.php'
		],

		'umiObjectPropertyLinkToObjectType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyLinkToObjectType.php'
		],

		'umiObjectPropertyMultipleImgFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyMultipleImgFile.php'
		],

		'baseXmlConfig' => [
			SYS_KERNEL_PATH . 'utils/xml/config/iBaseXmlConfig.php',
			SYS_KERNEL_PATH . 'utils/xml/config/baseXmlConfig.php'
		],

		'quickCsvExporter' => [
			SYS_KERNEL_PATH . 'utils/csv/export/quickCsvExporter.php'
		],

		'quickCsvImporter' => [
			SYS_KERNEL_PATH . 'utils/csv/import/quickCsvImporter.php'
		],

		'baseRestriction' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/baseRestriction.php'
		],

		'redirects' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/redirects.php'
		],

		'umiBaseStream' => [
			SYS_KERNEL_PATH . 'subsystems/streams/iUmiBaseStream.php',
			SYS_KERNEL_PATH . 'subsystems/streams/umiBaseStream.php'
		],

		'clusterCacheSync' => [
			SYS_KERNEL_PATH . 'subsystems/cacheSync/clusterCacheSync.php'
		],

		'umiObjectProxy' => [
			SYS_KERNEL_PATH . 'patterns/umiObjectProxy.php'
		],

		'umiObjectsExpiration' => [
			SYS_KERNEL_PATH . 'subsystems/expirations/iUmiObjectsExpiration.php',
			SYS_KERNEL_PATH . 'subsystems/expirations/umiObjectsExpiration.php'
		],

		'HTTPOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/HTTPOutputBuffer.php'
		],

		'HTTPDocOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/HTTPDocOutputBuffer.php'
		],

		'CLIOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/CLIOutputBuffer.php'
		],

		'objectProxyHelper' => [
			SYS_KERNEL_PATH . 'patterns/objectProxyHelper.php'
		],

		'antiSpamService' => [
			SYS_KERNEL_PATH . 'utils/antispam/antiSpamService.php'
		],

		'antiSpamHelper' => [
			SYS_KERNEL_PATH . 'utils/antispam/antiSpamHelper.php'
		],

		'alphabeticalIndex' => [
			SYS_KERNEL_PATH . 'utils/indexes/alphabetical/alphabeticalIndex.php'
		],

		'calendarIndex' => [
			SYS_KERNEL_PATH . 'utils/indexes/calendar/calendarIndex.php'
		],

		'selector' => [
			SYS_KERNEL_PATH . 'subsystems/selector/selector.php',
			SYS_KERNEL_PATH . 'subsystems/selector/where.php',
			SYS_KERNEL_PATH . 'subsystems/selector/types.php',
			SYS_KERNEL_PATH . 'subsystems/selector/order.php',
			SYS_KERNEL_PATH . 'subsystems/selector/executor.php',
			SYS_KERNEL_PATH . 'subsystems/selector/getter.php',
			SYS_KERNEL_PATH . 'subsystems/selector/options.php',
			SYS_KERNEL_PATH . 'subsystems/selector/group.php'
		],

		'selectorHelper' => [
			SYS_KERNEL_PATH . 'subsystems/selector/helper.php'
		],

		'loginzaAPI' => [
			SYS_KERNEL_PATH . '/utils/loginza/loginzaAPI.class.php'
		],

		'loginzaUserProfile' => [
			SYS_KERNEL_PATH . '/utils/loginza/loginzaUserProfile.class.php'
		],

		'session' => [
			SYS_KERNEL_PATH . '/subsystems/Deprecated/session.php'
		],

		'iSession' => [
			SYS_KERNEL_PATH . '/subsystems/Deprecated/iSession.php'
		],

		'UmiCms\System\Session\Session' => [
			SYS_KERNEL_PATH . '/subsystems/Session/Session.php'
		],

		'UmiCms\System\Session\iSession' => [
			SYS_KERNEL_PATH . '/subsystems/Session/iSession.php'
		],

		'iSessionValidation' => [
			SYS_KERNEL_PATH . '/subsystems/Deprecated/iSessionValidation.php'
		],

		'idna_convert' => [
			SYS_KERNEL_PATH . '/utils/idnaConverter/idna_convert.class.php'
		],

		'systemInfo' => [
			SYS_KERNEL_PATH . 'subsystems/regedit/systemInfo.php'
		],

		'imageUtils' => [
			SYS_KERNEL_PATH . '/utils/imageUtils/imageUtils.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/iImageProcessor.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/imageMagickProcessor.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/gdProcessor.php',

		],

		'MysqlLoggerCreator' => [
			SYS_KERNEL_PATH . 'utils/logger/mysql/iMysqlLoggerCreator.php',
			SYS_KERNEL_PATH . 'utils/logger/mysql/iMysqlLogger.php',
			SYS_KERNEL_PATH . 'utils/logger/mysql/MysqlLoggerCreator.php'
		],

		'mysqliConnection' => [
			SYS_KERNEL_PATH . 'subsystems/database/mysqliConnection.php',
			SYS_KERNEL_PATH . 'subsystems/database/mysqliQueryResult.php',
			SYS_KERNEL_PATH . 'subsystems/database/mysqliQueryResultIterator.php'
		],

		'jsonManifestCallback' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/interfaces.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/jsonCallback.php',
		],

		'SphinxClient' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/sphinx/sphinxapi.php',
		],

		'SphinxIndexGenerator' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/sphinx/SphinxIndexGenerator.php',
		],

		'UmiZipArchive' => [
			SYS_KERNEL_PATH . 'utils/UmiZipArchive/IUmiZipArchive.php',
			SYS_KERNEL_PATH . 'utils/UmiZipArchive/UmiZipArchive.php'
		],

		'BrowserDetect' => [
			SYS_KERNEL_PATH . 'utils/browser/Browser.php'
		],

		'iUmiBufferInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiBufferInjector.php'
		],

		'iUmiCollection' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiCollection.php'
		],

		'iUmiCollectionItem' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiCollectionItem.php'
		],

		'iUmiConfigInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiConfigInjector.php'
		],

		'iUmiDataBaseInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiDataBaseInjector.php'
		],

		'iUmiRegistryInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiRegistryInjector.php'
		],

		'iUmiDirectoriesInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiDirectoriesInjector.php'
		],

		'iUmiPagesInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiPagesInjector.php'
		],

		'iUmiTemplatesInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiTemplatesInjector.php'
		],

		'iUmiLanguagesInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiLanguagesInjector.php'
		],

		'iUmiService' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/iUmiService.php'
		],

		'iClassConfigManager' => [
			SYS_KERNEL_PATH . 'interfaces/iClassConfigManager.php'
		],

		'iClassConfig' => [
			SYS_KERNEL_PATH . 'interfaces/iClassConfig.php'
		],

		'iRedirects' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/iRedirects.php'
		],

		'iUmiRedirect' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/iUmiRedirect.php'
		],

		'umiRedirect' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/umiRedirect.php'
		],

		'umiRedirectsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/umiRedirectsCollection.php'
		],

		'AbstractReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/AbstractReference.php'
		],

		'ParameterReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/ParameterReference.php'
		],

		'ServiceReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/ServiceReference.php'
		],

		'InstantiableReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/InstantiableReference.php'
		],

		'ServiceContainer' => [
			SYS_KERNEL_PATH . 'subsystems/services/ServiceContainer.php'
		],

		'iServiceContainer' => [
			SYS_KERNEL_PATH . 'subsystems/services/iServiceContainer.php'
		],

		'ServiceContainerFactory' => [
			SYS_KERNEL_PATH . 'subsystems/services/ServiceContainerFactory.php'
		],

		'iServiceContainerFactory' => [
			SYS_KERNEL_PATH . 'subsystems/services/iServiceContainerFactory.php'
		],

		'tUmiDataBaseInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiDataBaseInjector.php'
		],

		'tUmiService' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/tUmiService.php'
		],

		'tUmiBufferInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiBufferInjector.php'
		],

		'tModulePart' => [
			SYS_KERNEL_PATH . 'traits/tModulePart.php'
		],

		'iModulePart' => [
			SYS_KERNEL_PATH . 'interfaces/iModulePart.php'
		],

		'tUmiConfigInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiConfigInjector.php'
		],

		'tUmiRegistryInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiRegistryInjector.php'
		],

		'tCommonCollectionItem' => [
			SYS_KERNEL_PATH . 'traits/tCommonCollectionItem.php'
		],

		'tClassConfigManager' => [
			SYS_KERNEL_PATH . 'traits/tClassConfigManager.php'
		],

		'tUmiPagesInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiPagesInjector.php'
		],

		'tUmiDirectoriesInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiDirectoriesInjector.php'
		],

		'tUmiTemplatesInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiTemplatesInjector.php'
		],

		'tUmiLanguagesInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiLanguagesInjector.php'
		],

		'tCommonCollection' => [
			SYS_KERNEL_PATH . 'traits/tCommonCollection.php'
		],

		'tUmiDomainsInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiDomainsInjector.php'
		],

		'tUmiImageFileInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiImageFileInjector.php'
		],

		'MailTemplatesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/mailTemplates/MailTemplatesCollection.php'
		],

		'MailTemplate' => [
			SYS_KERNEL_PATH . 'subsystems/mailTemplates/MailTemplate.php'
		],

		'staticCache' => [
			CURRENT_WORKING_DIR . '/libs/cacheControl.php'
		],

		'iUmiConstantMap' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiConstantMap.php'
		],

		'iUmiConstantMapInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiConstantMapInjector.php'
		],

		'iUmiDomainsInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiDomainsInjector.php'
		],

		'iUmiImageFileInjector' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiImageFileInjector.php'
		],

		'tCommonConstantMap' => [
			SYS_KERNEL_PATH . 'traits/tCommonConstantMap.php'
		],

		'tUmiConstantMapInjector' => [
			SYS_KERNEL_PATH . 'traits/tUmiConstantMapInjector.php'
		],

		'baseUmiCollectionConstantMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/baseUmiCollectionConstantMap.php'
		],

		'umiRedirectsConstantMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/umiRedirectsConstantMap.php'
		],

		'mailTemplatesConstantMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/mailTemplatesConstantMap.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Grabber' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Grabber.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\iGrabber' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/iGrabber.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\State' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/State.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\iState' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/iState.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\Step' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/Step.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\iStep' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/iStep.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\ObjectsNames' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/ObjectsNames.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\DesignTemplates' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/DesignTemplates.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\ObjectsFields' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/ObjectsFields.php'
		],

		'UmiCms\Classes\System\Utils\Links\Grabber\Steps\SitesUrls' => [
			SYS_KERNEL_PATH . 'utils/Links/Grabber/Steps/SitesUrls.php'
		],

		'UmiCms\Classes\System\Utils\Links\Collection' => [
			SYS_KERNEL_PATH . 'utils/Links/Collection.php'
		],

		'UmiCms\Classes\System\Utils\Links\iCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/iCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\Entity' => [
			SYS_KERNEL_PATH . 'utils/Links/Entity.php'
		],

		'UmiCms\Classes\System\Utils\Links\iEntity' => [
			SYS_KERNEL_PATH . 'utils/Links/iEntity.php'
		],

		'UmiCms\Classes\System\Utils\Links\ConstantMap' => [
			SYS_KERNEL_PATH . 'utils/Links/ConstantMap.php'
		],

		'UmiCms\Classes\System\Utils\Links\iSource' => [
			SYS_KERNEL_PATH . 'utils/Links/iSource.php'
		],

		'UmiCms\Classes\System\Utils\Links\iSourcesCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/iSourcesCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\Source' => [
			SYS_KERNEL_PATH . 'utils/Links/Source.php'
		],

		'UmiCms\Classes\System\Utils\Links\SourceConstantMap' => [
			SYS_KERNEL_PATH . 'utils/Links/SourceConstantMap.php'
		],

		'UmiCms\Classes\System\Utils\Links\SourcesCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/SourcesCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\SourceTypes' => [
			SYS_KERNEL_PATH . 'utils/Links/SourceTypes.php'
		],

		'UmiCms\Classes\System\Utils\Links\Checker\Checker' => [
			SYS_KERNEL_PATH . 'utils/Links/Checker/Checker.php'
		],

		'UmiCms\Classes\System\Utils\Links\Checker\iChecker' => [
			SYS_KERNEL_PATH . 'utils/Links/Checker/iChecker.php'
		],

		'UmiCms\Classes\System\Utils\Links\Checker\State' => [
			SYS_KERNEL_PATH . 'utils/Links/Checker/State.php'
		],

		'UmiCms\Classes\System\Utils\Links\Checker\iState' => [
			SYS_KERNEL_PATH . 'utils/Links/Checker/iState.php'
		],

		'UmiCms\Classes\System\Utils\Links\Injectors\iLinksCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/Injectors/iLinksCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\Injectors\iLinksSourcesCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/Injectors/iLinksSourcesCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\Injectors\tLinksCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/Injectors/tLinksCollection.php'
		],

		'UmiCms\Classes\System\Utils\Links\Injectors\tLinksSourcesCollection' => [
			SYS_KERNEL_PATH . 'utils/Links/Injectors/tLinksSourcesCollection.php'
		],

		'RequiredPropertyHasNoValueException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/RequiredPropertyHasNoValueException.php'
		],

		'WrongObjectTypeForProxyConstructionException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/WrongObjectTypeForProxyConstructionException.php'
		],

		'ExpectTemplateException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/ExpectTemplateException.php'
		],

		'RequiredFunctionIsNotExistsException' => [
			SYS_KERNEL_PATH . 'entities/exceptions/RequiredFunctionIsNotExistsException.php'
		],

		'UmiCms\Classes\System\Enums\Enum' => [
			SYS_KERNEL_PATH . 'enums/Enum.php'
		],

		'UmiCms\Classes\System\Enums\iEnum' => [
			SYS_KERNEL_PATH . 'enums/iEnum.php'
		],

		'UmiCms\Classes\System\Enums\EnumElementNotExistsException' => [
			SYS_KERNEL_PATH . 'enums/EnumElementNotExistsException.php'
		],

		'UmiCms\Classes\System\Entities\Country\Country' => [
			SYS_KERNEL_PATH . 'entities/Country/Country.php'
		],

		'UmiCms\Classes\System\Entities\Country\iCountry' => [
			SYS_KERNEL_PATH . 'entities/Country/iCountry.php'
		],

		'UmiCms\Classes\System\Entities\Country\CountriesFactory' => [
			SYS_KERNEL_PATH . 'entities/Country/CountriesFactory.php'
		],

		'UmiCms\Classes\System\Entities\Country\iCountriesFactory' => [
			SYS_KERNEL_PATH . 'entities/Country/iCountriesFactory.php'
		],

		'UmiCms\Classes\System\Entities\City\City' => [
			SYS_KERNEL_PATH . 'entities/City/City.php'
		],

		'UmiCms\Classes\System\Entities\City\iCity' => [
			SYS_KERNEL_PATH . 'entities/City/iCity.php'
		],

		'UmiCms\Classes\System\Entities\City\CitiesFactory' => [
			SYS_KERNEL_PATH . 'entities/City/CitiesFactory.php'
		],

		'UmiCms\Classes\System\Entities\City\iCitiesFactory' => [
			SYS_KERNEL_PATH . 'entities/City/iCitiesFactory.php'
		],

		'UmiCms\Service' => [
			SYS_KERNEL_PATH . 'subsystems/services/Service.php'
		],

		'UmiCms\iService' => [
			SYS_KERNEL_PATH . 'subsystems/services/iService.php'
		],

		'commerceML2Splitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/splitters/commerceML2Splitter.php'
		],

		'csvSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/splitters/CSVSplitter.php'
		],

		'transferSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/splitters/transferSplitter.php'
		],

		'umiDump20Splitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/splitters/umiDump20Splitter.php'
		],

		'umiDumpSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/Deprecated/umiDumpSplitter.php'
		],

		'ymlSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/splitters/ymlSplitter.php'
		],

		'UmiCms\System\Permissions\SystemUsersPermissions' => [
			SYS_KERNEL_PATH . 'subsystems/models/permissions/SystemUsersPermissions.php'
		],

		'UmiCms\System\Permissions\iSystemUsersPermissions' => [
			SYS_KERNEL_PATH . 'subsystems/models/permissions/iSystemUsersPermissions.php'
		],

		'UmiCms\System\Auth\Auth' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Auth.php'
		],

		'UmiCms\System\Auth\Authentication' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Authentication.php'
		],

		'UmiCms\System\Auth\Authorization' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Authorization.php'
		],

		'UmiCms\System\Auth\AuthorizationException' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Exceptions/AuthorizationException.php'
		],

		'UmiCms\System\Auth\WrongCredentialsException' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Exceptions/WrongCredentialsException.php'
		],

		'UmiCms\System\Auth\AuthenticationException' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Exceptions/AuthenticationException.php'
		],

		'UmiCms\System\Auth\PasswordHash\Algorithm' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/PasswordHash/Algorithm.php'
		],

		'UmiCms\System\Auth\PasswordHash\iAlgorithm' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/PasswordHash/iAlgorithm.php'
		],

		'UmiCms\System\Auth\PasswordHash\WrongAlgorithmException' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/PasswordHash/WrongAlgorithmException.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\ActivationCode' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/ActivationCode.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\Factory' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/Factory.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\iFactory' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/iFactory.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\iRule' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/iRule.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\LoginAndPassword' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/LoginAndPassword.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\LoginAndProvider' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/LoginAndProvider.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\Rule' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/Rule.php'
		],

		'UmiCms\System\Auth\AuthenticationRules\UserId' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/AuthenticationRules/UserId.php'
		],

		'UmiCms\System\Auth\iAuth' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Interfaces/iAuth.php'
		],

		'UmiCms\System\Auth\iAuthentication' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Interfaces/iAuthentication.php'
		],

		'UmiCms\System\Auth\iAuthorization' => [
			SYS_KERNEL_PATH . 'subsystems/models/auth/Interfaces/iAuthorization.php'
		]
	];
