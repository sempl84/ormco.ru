<?php
//die();
require_once 'standalone.php';

/**
 * Файл для получения структуры конкретных разделов каталогов в виде csv
 */
$parents = [4, 8313, 8314, 13202]; // родительские элементы
$types = [ // разрешенные типы
    84, // раздел
    85 // товар
];

$names = [
    "tork" => "Торк",
    "nomer_zuba" => "Номер зуба",
    //"artikul" => "Артикул",
    "paz" => "Паз",
    "kolco" => "Сторона",
    "smewenie_breketa_k_desne" => "Смещение брекета к десне",
    "nalichie_kryuchka" => "Наличие крючка",
    "sechenie_dugi" => "Сечение дуги",
    "material" => "Материал",
    "forma" => "Форма",
    "chelyust" => "Челюсть",
    "nalichie_petli_na_duge" => "Наличие петли на дуге",
    "rasstoyanie_mezhdu_petlyami" => "Расстояние между петлями",
    "breket_sistema" => "Брекет система",
    "razmer_dugi" => "Размер дуги",
    "temperatura_preformacii" => "Температура преформации",
    "struktura_dugi" => "Структура дуги",
    "nizkoe_trenie" => "Низкое трение",
    "cvet" => "Цвет",
    "lingval_naya_duga" => "Лингвальная дуга",
    "gruppa_zamkov" => "Группа замков",
];

$names = array_flip($names);
$max_level = 11;

$struct = array();
echo '<a href="https://ormco.ru/struct.csv" target="_blank">https://ormco.ru/struct.csv</a><br>'."\n";
file_put_contents($_SERVER['DOCUMENT_ROOT'].'/struct.csv', iconv("utf-8", "windows-1251", 'вкл/выкл') . ';' . iconv("utf-8", "windows-1251", 'Артикул') . ";" . implode(';', array_fill(1, $max_level+1, '')) . ";" . iconv("utf-8", "windows-1251", implode(';', array_keys($names))) . ";\n");

$start = memory_get_usage();

$hierarhy = umiHierarchy::getInstance();
foreach($parents as $parent){
    echo '*' . round((memory_get_usage() - $start) / 1024) . '<br />';
    $parent_page = $hierarhy->getElement($parent);
    if($parent_page instanceof umiHierarchyElement){
        save_item($parent_page->name, $parent_page->getIsActive(), $parent_page->getValue('artikul'), 1, $max_level, array());
        get_page_by_parent_new($parent, $types, 1, $max_level, $names, $start);
    }
    echo '**' . round((memory_get_usage() - $start) / 1024) . '<br />';
//    break;
}

function get_page_by_parent_new($page_id, $types, $current_level, &$max_level, $names, $start){
    $fieldsCollection = umiFieldsCollection::getInstance();
    $collection = umiObjectsCollection::getInstance();

//    echo implode('*', array_fill(1, $current_level, '')) . ' ' . round((memory_get_usage() - $start) / 1024) . '<br />';

    $pages = new selector('pages');
    $pages->where('hierarchy')->page($page_id)->childs(1);
    $pages->where('is_active')->equals(array(1,0));
    foreach($pages as $page){
        if($page->getObjectTypeId() == $types[0]){
            save_item($page->name, $page->getIsActive(), $page->getValue('artikul'), $current_level + 1, $max_level, array());
            get_page_by_parent_new($page->id, $types, $current_level + 1, $max_level, $names, $start);
        }elseif($page->getObjectTypeId() == $types[1]){
            $values = array();
            foreach($names as $name){
                $field_id = $page->getFieldId($name);
                $field = $fieldsCollection->getField($field_id);
                if($field instanceof umiField){
                    if($field->getDataType() == 'relation'){
                        $prop_values = $page->getValue($name);
                        if(!is_array($prop_values)){
                            $prop_values = [$prop_values];
                        }
                        $res = [];
                        foreach($prop_values as $prop_value){
                            $obj = $collection->getObject($prop_value);
                            if($obj instanceof umiObject){
                                $res[] = iconv("utf-8", "windows-1251", $obj->name);
                            }
                            unset($prop_value);
                            unset($obj);
                        }
                        $values[] = implode(',', $res);
                        unset($res);
                        unset($prop_values);
                    }else{
                        $values[] = $page->getValue($name);
                    }
                }
                unset($field);
                unset($field_id);
                unset($name);
            }
            save_item($page->name, $page->getIsActive(), $page->getValue('artikul'), $current_level + 1, $max_level, $values);
            unset($values);
        }
        unset($page);
//        echo implode('*', array_fill(1, $current_level, '')) . ' # ' . round((memory_get_usage() - $start) / 1024) . '<br />';
    }
    unset($pages);
    $fieldsCollection->clearCache();
    unset($fieldsCollection);
    umiHierarchy::getInstance()->clearCache();
    umiHierarchy::getInstance()->clearDefaultElementCache();
    umiHierarchy::getInstance()->unloadAllElements();
    $collection->clearCache();
    $collection->unloadAllObjects();
    unset($collection);
}

function save_string($str){
    if(!empty($str)){
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/struct.csv',  $str."\n", FILE_APPEND);
    }
    unset($str);
}

function save_item($name, $is_active, $artikul, $current_level, $max_level, $values){
    $res = [$is_active ? iconv("utf-8", "windows-1251", 'вкл') : iconv("utf-8", "windows-1251", 'выкл'), iconv("utf-8", "windows-1251", $artikul)];
    for($i=1;$i<=$max_level+1;$i++){
        if($i == $current_level){
            $res[] = iconv("utf-8", "windows-1251", $name);
        }else{
            $res[] = '';
        }
    }
    save_string(implode(';', array_merge($res, $values)));
    unset($res);
}