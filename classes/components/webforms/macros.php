<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class WebFormsMacros {
		/**
		 * @var webforms $module
		 */
		public $module;

		/**
		 * Возвращает содержимое страницы по умолчанию
		 * @return string|array
		 */
		public function page() {
			$cmsControllerInstance = cmsController::getInstance();
			$pageId = $cmsControllerInstance->getCurrentElementId();

			/**
			 * @var content $contentModule
			 */
			$contentModule = $cmsControllerInstance->getModule('content');

			if (!$contentModule instanceof def_module) {
				return false;
			}

			return $contentModule->content($pageId);
		}

		/**
		 * Отправляет письмо
		 * @param iUmiObject $message сообщение формы
		 * @param array $aRecipients получатели письма
		 * @param array $data отправляемые данные (@see webforms::formatMessage())
		 * @throws coreException
		 */
		public function sendMail(iUmiObject $message, array $aRecipients, array $data) {
			$umiObjectTypes = umiObjectTypesCollection::getInstance();

			$oMail = new umiMail();
			$aFTypes = array('file', 'img_file', 'swf_file');
			$aFields = $umiObjectTypes->getType($message->getTypeId())->getAllFields();

			/**
			 * @var iUmiField $oField
			 */
			foreach($aFields as $oField) {
				/**
				 * @var iUmiFieldType $oType
				 */
				$oType   = $oField->getFieldType();

				if (in_array($oType->getDataType(), $aFTypes)) {
					$oFile = $message->getValue($oField->getName());
					if ($oFile instanceof umiFile) {
						$oMail->attachFile($oFile);
					}
				}
			}

			$recpCount = 0;

			foreach ($aRecipients as $recipient) {
				foreach (explode(',', $recipient['email']) as $sAddress) {
					if (strlen(trim($sAddress))) {
						$oMail->addRecipient(trim($sAddress), $recipient['name']);
						$recpCount++;
					}
				}
			}

			if (!$recpCount) {
				/**
				 * @var webforms|$this $module
				 */
				$module = $this->module;
				$module->reportError(getLabel('error-no_recipients'));
			}

			$oMail->setFrom($data['from_email_template'], $data['from_template']);
			$oMail->setSubject($data['subject_template']);
			$oMail->setContent($data['master_template']);
			$oMail->commit();
			$oMail->send();
		}

		/**
		 * Отправляет автоматическое уведомление
		 * @param array $data отправляемые данные (@see webforms::formatMessage())
		 */
		public function sendAutoReply(array $data) {
			$oMailReply = new umiMail();

			if (isset($data['autoreply_email_recipient']) && $data['autoreply_email_recipient']) {
				$oMailReply->addRecipient($data['autoreply_email_recipient']);
			} else {
				$oMailReply->addRecipient($data['from_email_template'], $data['from_template']);
			}

			$oMailReply->setFrom($data['autoreply_from_email_template'], $data['autoreply_from_template']);
			$oMailReply->setSubject($data['autoreply_subject_template']);
			$oMailReply->setContent($data['autoreply_template']);
			$oMailReply->commit();
			$oMailReply->send();
		}

		/**
		 * Обрабатывает ошибку.
		 * Совершает редирект на referrer, подставляя
		 * в $_GET параметр идентификатор ошибки.
		 * @param string $errorMessage текст ошибки
		 * @param bool $interrupt прерывать выполнение скрипта
		 */
		public function reportError($errorMessage, $interrupt = true) {
			$this->module->errorNewMessage($errorMessage, (bool) $interrupt);
		}

		/**
		 * Завершает отправку сообщения формы.
		 * Совершает редирект на referrer
		 * или выводит сообщение об успехе.
		 * @param int $iObjectId идентификатор созданного сообщения.
		 * @return string
		 */
		public function finishSending($iObjectId) {
			$sRedirect = getRequest('ref_onsuccess');

			if ($sRedirect) {
				$this->module->redirect($sRedirect);
			}

			$sTemplateName = getRequest('system_template');
			if ($sTemplateName) {

				list($sSuccessString) = webforms::loadTemplates(
					"data/reflection/" . $sTemplateName,
					"send_successed"
				);

				if (strlen($sSuccessString)) {
					return $sSuccessString;
				}
			}

			if (isset($_SERVER['HTTP_REFERER'])) {
				$this->module->redirect($_SERVER['HTTP_REFERER']);
			}

			return '';
		}

		/**
		 * Возвращает данные для создания формы обратной связи
		 * @param bool|int|string $form_id идентификатор или имя формы
		 * @param string|int $who e-mail адресата или идентификатор адреса
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws coreException
		 */
		public function add($form_id = false, $who = "", $template = "webforms") {
			$aParam = array();
			$oTypes = umiObjectTypesCollection::getInstance();
			$iBaseTypeId = $oTypes->getTypeIdByHierarchyTypeName("webforms", "form");

			if (is_numeric($form_id)) {
				if ($oTypes->getParentTypeId($form_id) != $iBaseTypeId) {
					$form_id = false;
				}
			}

			if (!is_numeric($form_id) || $form_id === false) {
				$children = $oTypes->getChildTypeIds($iBaseTypeId);

				if (empty($children)) {
					list($template) = webforms::loadTemplates(
						"data/reflection/" . $template,
						"error_no_form"
					);
					return $template;
				}

				$i = 0;
				do {
					$oForm = $oTypes->getType($children[$i]); $i++;
				} while(
					$i < count($children) && $oForm->getName() != $form_id
				);

				$form_id = $oForm->getId();
			}

			$aParam['attribute:form_id']  = $form_id;
			$aParam['attribute:template'] = $template;

			if (!strlen($who)) {
				$mSelect = $this->writeAddressSelect($template, $form_id);

				if (is_array($mSelect)) {
					if (isset($mSelect['items'])) {
						$aParam['items'] =  $mSelect['items'];
					} else {
						return;
					}
				} else {
					if (strlen($mSelect)) {
						$aParam['address_select'] = $mSelect;
					} else {
						return;
					}
				}
			} else {
				$addressId = $this->module->guessAddressId($who);

				if (func_num_args() > 3) {
					$addresses = array_slice(func_get_args(), 3);

					foreach($addresses as &$addr) {
						$addr = $this->module->guessAddressId($addr);
					}

					$addresses = array_merge(array($addressId), $addresses);
					$aParam['res_to'] = $aParam['address_select'] = $this->writeSeparateAddresses($addresses, $template);
				} else {
					$aParam['res_to'] = $aParam['address_select'] = '<input type="hidden" name="system_email_to" value="' . $addressId . '" />';
				}
			}

			/**
			 * @var data|DataForms $dataModule
			 */
			$dataModule = cmsController::getInstance()->getModule('data');
			$aParam['groups'] = $dataModule->getCreateForm($form_id, $template);

			list($sBlock) = webforms::loadTemplates("data/reflection/" . $template, "form_block");
			return webforms::parseTemplate($sBlock, $aParam);
		}

		/**
		 * Возвращает список адресов
		 * @param string $template имя шаблона (для tpl)
		 * @param bool $iFormId выбранная форма
		 * @return mixed
		 * @throws selectorException
		 */
		public function writeAddressSelect($template, $iFormId = false) {
			list($block, $line) = webforms::loadTemplates(
				"data/reflection/" . $template,
				"address_select_block",
				'address_select_block_line'
			);

			$addresses = new selector('objects');
			$addresses->types('object-type')->name('webforms', 'address');
			$addresses->option('no-length')->value(true);
			$addresses->option('load-all-props')->value(true);
			$addresses = $addresses->result();
			$aItems = array();

			foreach($addresses as $address) {
				if (!$address instanceof umiObject) {
					continue;
				}
				$sTitle = $address->getValue('address_description');
				$aParam = array();
				$addressId = $address->getId();
				$aParam['attribute:id'] = $addressId;
				$aParam['node:text'] = $sTitle;

				if ($iFormId && $sFormsId = $address->getValue('form_id')) {
					if (in_array($iFormId, explode(',', $sFormsId))) {
						$aParam['attribute:selected'] = 'selected';
					}
				}

				$aItems[] = webforms::parseTemplate($line, $aParam, false, $addressId);
			}

			$aBlockParam = array();
			$aBlockParam['void:options'] = $aBlockParam['subnodes:items'] = $aItems;
			return webforms::parseTemplate($block, $aBlockParam);
		}

		/**
		 * Возращает список адресов указанных идентификаторов
		 * @param array $addressIds список идентификаторов адресов
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 */
		public function writeSeparateAddresses($addressIds, $template) {
			list($block, $line) = webforms::loadTemplates(
				"data/reflection/" . $template,
				"address_separate_block",
				'address_separate_block_line'
			);

			$collection = umiObjectsCollection::getInstance();
			$aItems = array();

			foreach($addressIds as $id) {

				if ($address = $collection->getObject($id)) {
					$aLine = array();
					$aLine['id'] = 'address_' . $id;
					$aLine['name'] = 'system_email_to[]';
					$aLine['value']	= $id;
					$aLine['description'] = $address->getValue('address_description');
					$aItems[] = webforms::parseTemplate($line, $aLine);
				}
			}

			$aBlockParam = array();
			$aBlockParam['void:lines'] = $aBlockParam['subnodes:items'] = $aItems;
			return webforms::parseTemplate($block, $aBlockParam);
		}

		/**
		 * Возвращает сообщение об успешном отправлении письма формыю
		 * @param bool|int|string $template имя шаблона (для tpl) или идентификатор шаблона письма
		 * @return string
		 * @throws selectorException
		 */
		public function posted($template = false) {
			$template = $template ? $template : (string) getRequest('template');
			$template = $template ? $template : (string) getRequest('param0');
			$res = false;

			if ($template && is_numeric($template)) {

				$sel = new selector('objects');
				$sel->types('object-type')->name('webforms', 'template');
				$sel->where('form_id')->equals($template);
				$sel->limit(0, 1);

				if ($sel->first) {
					/**
					 * @var iUmiObject|iUmiEntinty $oTemplate
					 */
					$oTemplate = $sel->first;
					$res = $oTemplate->getValue('posted_message');
				}

				if (!$res && !webforms::isXSLTResultMode()) {
					try {
						list($template) = webforms::loadTemplates("./tpls/webforms/" . $template, "posted");
						$res = $template;
					} catch (publicException $e) {}
				}
			}

			if (!$res ) {
				if (isDemoMode()) {
					$res = "%webforms_thank_you_demo%";
				} else {
					$res = "%webforms_thank_you%";
				}
			}

			return def_module::parseTPLMacroses($res);
		}

		/**
		 * Клиентский метод.
		 * Валидирует данные формы, создает объект сообщения
		 * и отправляет на адрес формы почтовое сообщение,
		 * сформированные по шаблону.
		 * При необходимости отправляет автоматическое
		 * почтовое уведомление.
		 * После выполнения перенаправляет на referrer.
		 * @return void|string
		 * @throws Exception
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 * @throws publicAdminException
		 */
		public function send() {
			if (!umiCaptcha::checkCaptcha()) {
				$this->reportError("%errors_wrong_captcha%");
			}

			$module = $this->module;

			if (isDemoMode()) {
				$url = getRequest('ref_onsuccess');

				if (!$url) {
					$url = $module->pre_lang . "/webforms/posted/";
				}

				$module->redirect($url);
			}

			$oTypes = umiObjectTypesCollection::getInstance();
			$iBaseTypeId = $oTypes->getTypeIdByHierarchyTypeName("webforms", "form");
			$iFormTypeId = getRequest('system_form_id');
			$sSenderIP = getServer('REMOTE_ADDR');
			$iTime = new umiDate(time());
			$aAddresses = getRequest('system_email_to');

			if (!is_array($aAddresses)) {
				$aAddresses = array($aAddresses);
			}

			$aRecipients = array();
			$sAddress = null;
			$sEmailTo = null;

			foreach ($aAddresses as $address){
				$sEmailTo = $module->guessAddressValue($address);
				$sAddress = $module->guessAddressName($address);
				$aRecipients[] = array(
					'email' => $sEmailTo,
					'name' => $sAddress
				);
			}

			if ($oTypes->getParentTypeId($iFormTypeId) != $iBaseTypeId) {
				$this->reportError("%wrong_form_type%");
			}

			if (($ef = $module->checkRequiredFields($iFormTypeId)) !== true) {
				$this->reportError(getLabel('error-required_list') . $module->assembleErrorFields($ef), false);
			}

			$_REQUEST['data']['new']['sender_ip'] = $sSenderIP;
			$oObjectsCollection = umiObjectsCollection::getInstance();
			$iObjectId = $oObjectsCollection->addObject($sAddress, $iFormTypeId);

			$auth = UmiCms\Service::Auth();
			$oObjectsCollection->getObject($iObjectId)->setOwnerId($auth->getUserId());

			/**
			 * @var data|DataForms $data
			 */
			try {
				webforms::$noRedirectOnPanic = true;
				$data = cmsController::getInstance()->getModule('data');
				$data->saveEditedObject($iObjectId, true);
				webforms::$noRedirectOnPanic = false;
			} catch (errorPanicException $e) {
				webforms::$noRedirectOnPanic = false;
				$this->reportError($e->getMessage());
			}

			$oObject = $oObjectsCollection->getObject($iObjectId);
			$oObject->setValue('destination_address', $sEmailTo);
			$oObject->setValue('sender_ip', $sSenderIP);
			$oObject->setValue('sending_time', $iTime);
			$aMessage = $module->formatMessage($iObjectId, true);

			$this->sendMail($oObject, $aRecipients, $aMessage);

			if (strlen($aMessage['autoreply_template'])) {
				$this->sendAutoReply($aMessage);
			}

			$oEventPoint = new umiEventPoint("webforms_post");
			$oEventPoint->setMode("after");
			$oEventPoint->setParam("email", $aMessage['from_email_template']);
			$oEventPoint->setParam("message_id", $iObjectId);
			$oEventPoint->setParam("form_id", $iFormTypeId);
			$oEventPoint->setParam("fio", $aMessage['from_template']);
			webforms::setEventPoint($oEventPoint);

			return $this->finishSending($iObjectId);
		}

	}
?>