<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "webforms";
	$INFO['config'] = "0";
	$INFO['ico'] = "ico_webforms";
	$INFO['default_method'] = "insert";
	$INFO['default_method_admin'] = "addresses";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/webforms/admin.php";
	$COMPONENTS[] = "./classes/components/webforms/class.php";
	$COMPONENTS[] = "./classes/components/webforms/customAdmin.php";
	$COMPONENTS[] = "./classes/components/webforms/customMacros.php";
	$COMPONENTS[] = "./classes/components/webforms/handlers.php";
	$COMPONENTS[] = "./classes/components/webforms/i18n.en.php";
	$COMPONENTS[] = "./classes/components/webforms/i18n.php";
	$COMPONENTS[] = "./classes/components/webforms/includes.php";
	$COMPONENTS[] = "./classes/components/webforms/install.php";
	$COMPONENTS[] = "./classes/components/webforms/lang.en.php";
	$COMPONENTS[] = "./classes/components/webforms/lang.php";
	$COMPONENTS[] = "./classes/components/webforms/macros.php";
	$COMPONENTS[] = "./classes/components/webforms/permissions.php";
