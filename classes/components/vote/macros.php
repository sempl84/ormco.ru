<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class VoteMacros {
		/**
		 * @var vote $module
		 */
		public $module;

		/**
		 * Возвращает данные для создания формы опроса или его результаты,
		 * если посетитель уже голосовал или опрос закрыт.
		 * @param string|int $path адрес опроса или его идентификатор
		 * @param string $template имя шаблона (для tpl)
		 * @return string
		 * @throws publicException
		 */
		public function poll($path = "", $template = "default") {
			$element_id = $this->module->analyzeRequiredPath($path);
			$element = umiHierarchy::getInstance()->getElement($element_id);

			if (!$element) {
				return "";
			}

			if ($this->module->checkIsVoted($element->getObjectId()) || $element->getValue('is_closed')) {
				return $this->results($element_id, $template);
			}

			return $this->insertvote($element_id, $template);
		}

		/**
		 * Учитывает ответ пользователя на опрос.
		 * @param string $template имя шаблона (для tpl)
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function post($template = "default") {
			if (!$template) {
				$template = getRequest('template');
			}

			if (!$template) {
				$template = "default";
			}

			if (!vote::isXSLTResultMode()) {
				list(
					$template_block,
					$template_block_voted,
					$template_block_closed,
					$template_block_ok
					) = vote::loadTemplates(
					"vote/" . $template,
					"js_block",
					"js_block_voted",
					"js_block_closed",
					"js_block_ok"
				);
			} else {
				list(
					$template_block,
					$template_block_voted,
					$template_block_closed,
					$template_block_ok
					) = array(
					false,
					false,
					false,
					false
				);
			}

			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$item_id = (int) getRequest('param0');
			$item = $umiObjectsCollection->getObject($item_id);

			$referrer = getServer("HTTP_REFERER") ? getServer("HTTP_REFERER") : "/";
			$module = $this->module;
			$module->errorRegisterFailPage($referrer);

			if (!$item instanceof umiObject) {
				$this->reportError(getLabel('error-page-does-not-exist'), true);
			}

			$poll_rel = $item->getValue("poll_rel");
			$object_id = $poll_rel;
			$object = $umiObjectsCollection->getObject($object_id);

			if (!$object instanceof umiObject) {
				$this->reportError(getLabel('error-page-does-not-exist'), true);
			}

			switch (true) {
				case ($module->checkIsVoted($object_id)) : {
					$res = ($template_block_voted) ? $template_block_voted : getLabel('error-already-voted');
					break;
				}
				case ($object->getValue("is_closed")) : {
					$res = ($template_block_closed) ? $template_block_closed : getLabel('error-vote-not-active-or-closed');
					break;
				}
				default : {
					$module->incrementVote($object, $item);
					$res = ($template_block_ok) ? $template_block_ok : getLabel('vote-success');
				}
			}

			$session = \UmiCms\Service::Session();
			$polledVotes = $session->get('vote_polled');

			$polledVotes = (is_array($polledVotes)) ? $polledVotes : [];
			$polledVotes[] = $object_id;

			$session->set('vote_polled', $polledVotes);

			$this->pollResult($res, $template_block);
		}

		/**
		 * Возвращает данные для создания формы опроса
		 * @param string|int $path адрес опроса или его идентификатор
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws publicException
		 */
		public function insertvote($path = "", $template = "default") {
			list(
				$template_block,
				$template_line,
				$template_submit
				) = vote::loadTemplates(
				"vote/" . $template,
				"vote_block",
				"vote_block_line",
				"vote_block_submit"
			);

			$hierarchy = umiHierarchy::getInstance();
			$objects = umiObjectsCollection::getInstance();
			$elementId = $this->module->analyzeRequiredPath($path);

			$element = $hierarchy->getElement($elementId);

			if (!$element) {
				throw new publicException(getLabel('error-page-does-not-exist', null, $path));
			}

			$block_arr = array(
				'id' => $elementId,
				'text' => $element->getValue('question')
			);

			$result = $element->getValue('answers');

			if (!is_array($result)) {
				$result = array();
			}

			$this->loadAnswers($result);
			$lines = array();

			foreach ($result as $item_id) {
				$item = $objects->getObject($item_id);
				$line_arr = array();
				$line_arr['attribute:id'] = $line_arr['void:item_id'] = $item_id;
				$line_arr['node:item_name'] = $item->getName();
				$lines[] = vote::parseTemplate($template_line, $line_arr, false, $item_id);
			}

			$is_closed = (bool) $element->getValue("is_closed");
			$block_arr['submit'] = ($is_closed) ? "" : $template_submit;
			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$umiLinkHelper = umiLinksHelper::getInstance();
			$block_arr['link'] = $umiLinkHelper->getLink($element);

			return vote::parseTemplate($template_block, $block_arr, $elementId);
		}

		/**
		 * Возвращает результаты опроса
		 * @param string|int $path адрес опроса или его идентификатор
		 * @param string $template имя шаблона (для tpl)
		 * @return bool
		 */
		public function results($path, $template = "default") {
			if (!$template) {
				$template = "default";
			}

			list($template_block, $template_line) = vote::loadTemplates(
				"vote/" . $template,
				"result_block",
				"result_block_line"
			);

			$element_id = $this->module->analyzeRequiredPath($path);
			$umiHierarchy = umiHierarchy::getInstance();
			$element = $umiHierarchy->getElement($element_id);

			if (!$element) {
				return false;
			}

			$block_arr = Array();
			$block_arr['id'] = $element_id;
			$block_arr['text'] = $element->getValue("question");
			$block_arr['vote_header'] = $element->getValue("h1");
			$block_arr['alt_name'] = $element->getAltName();
			$result = $element->getValue('answers');
			$items = Array();
			$total = 0;
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			foreach($result as $item_id) {
				$item = $umiObjectsCollection->getObject($item_id);
				$total += (int) $item->getPropByName("count")->getValue();
				$items[] = $item;
			}

			$lines = Array();

			/**
			 * @var iUmiObject|iUmiEntinty $item
			 */
			foreach($items as $item) {
				$line_arr = Array();
				$line_arr['node:item_name'] = $item->getName();
				$line_arr['attribute:score'] = $line_arr['void:item_result'] = $c = (int) $item->getValue("count");
				$curr_procs = ($total > 0) ? round((100 * $c) / $total) : 0;
				$line_arr['attribute:score-rel'] = $line_arr['void:item_result_proc'] = $curr_procs;
				$line_arr['void:item_result_proc_reverce'] = 100 - $curr_procs;
				$lines[] = vote::parseTemplate($template_line, $line_arr, false, $item->getId());
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total_posts'] = $total;
			$block_arr['link'] = $umiHierarchy->getPathById($element_id);
			return vote::parseTemplate($template_block, $block_arr, $element_id);
		}

		/**
		 * Завершает голосование опроса
		 * @param mixed $result результат опроса
		 * @param bool $templateBlock блок шаблона (для tpl)
		 */
		public function pollResult($result, $templateBlock = false) {
			$result = vote::parseTPLMacroses($result);

			if ($templateBlock) {
				$block_arr = [];
				$block_arr['res'] = $result;
				$r = vote::parseTemplate($templateBlock, $block_arr);
				$this->module->flush($r, "text/javascript");
			} else {
				$this->module->flush("alert('{$result}');", "text/javascript");
			}
		}

		/**
		 * Обрабатывает ошибку.
		 * Совершает редирект на referrer, подставляя
		 * в $_GET параметр идентификатор ошибки.
		 * @param string $errorMessage текст ошибки
		 * @param bool $interrupt прерывать выполнение скрипта
		 */
		public function reportError($errorMessage, $interrupt = true) {
			$this->module->errorNewMessage($errorMessage, $interrupt);
		}

		/**
		 * Возвращает последний добавленный опрос
		 * @param string $template имя шаблона (для tpl)
		 * @return string|void
		 * @throws selectorException
		 */
		public function insertlast($template = "default") {
			if (!$template) {
				$template = "default";
			}

			$votes = new selector('pages');
			$votes->types('object-type')->name('vote', 'poll');
			$votes->option('no-length')->value(true);
			$votes->order('publish_time')->desc();
			$votes->option('load-all-props')->value(true);
			$votes->option('no-length')->value(true);
			$votes->limit(0, 1);

			if ($votes->first) {
				$element_id = $votes->first->getId();
				return $this->poll($element_id, $template);
			}

			return;
		}

		/**
		 * Начисляет странице рейтинг
		 * @param string $template имя шаблона (для tpl)
		 * @param null|int $elementId идентификатор страницы, за которую голосуют
		 * @param null|int $bid оценка
		 * @return mixed
		 */
		public function setElementRating($template = "default", $elementId = null, $bid = null) {
			if (!$template) {
				$template = getRequest('param0');
			}

			if (!$elementId) {
				$elementId = (int) getRequest('param1');
			}

			if (!$bid) {
				$bid = (int) getRequest('param2');
			}

			$umiHierarchy = umiHierarchy::getInstance();
			$element = $umiHierarchy->getElement($elementId);

			if ($isPrivate = (bool) regedit::getInstance()->getVal("//modules/vote/is_private")) {
				if (!permissionsCollection::getInstance()->is_auth()) {
					return vote::renderTemplate(
						"vote/rate/" . $template,
						"rate_permitted",
						array("@permitted" => "permitted"),
						$elementId
					);
				}
			}

			if (!$element) {
				return vote::renderTemplate(
					"vote/rate/" . $template,
					"rate_not_found",
					array(),
					$elementId
				);
			}

			if ($this->module->getIsRated($elementId)) {
				return vote::renderTemplate(
					"vote/rate/" . $template,
					"rate_rated",
					$this->buildBlockArray($element),
					$elementId
				);
			}

			$rateVoters = (int) $element->getValue("rate_voters");
			$rateSum = (int) $element->getValue("rate_sum");
			$rateSum += $this->module->calculateBid($bid);
			$rateVoters++;

			$element->setValue("rate_voters", $rateVoters);
			$element->setValue("rate_sum", $rateSum);
			$element->commit();
			$umiHierarchy->unloadElement($elementId);

			$this->module->setIsRated($elementId);

			return vote::renderTemplate(
				"vote/rate/" . $template,
				"rate_ok",
				$this->buildBlockArray($element),
				$elementId
			);
		}

		/**
		 * Возвращает рейтинг страницы заданной страницы
		 * @param string $template имя шаблона (для tpl)
		 * @param null|int $elementId идентификатор страницы
		 * @return mixed
		 */
		public function getElementRating($template = "default", $elementId = null) {
			if (!$template) {
				$template = getRequest('param0');
			}

			if (!$elementId) {
				$elementId = (int) getRequest('param1');
			}

			$umiHierarchy = umiHierarchy::getInstance();
			$element = $umiHierarchy->getElement($elementId);

			if (!$element) {
				return vote::renderTemplate(
					"vote/rate/" . $template,
					"rate_not_found",
					array(),
					$elementId
				);
			}

			$blockArray = $this->buildBlockArray($element);
			$blockArray['is_rated'] = $this->module->getIsRated($elementId);
			$umiRegistry = regedit::getInstance();

			if ($umiRegistry->getVal("//modules/vote/is_graded")) {
				list($ratedTpl, $ratingTpl) = vote::loadTemplates(
					"vote/rate/" . $template,
					"rate_rated_graded",
					"rate_rating_graded"
				);
			} else {
				list($ratedTpl, $ratingTpl) = vote::loadTemplates(
					"vote/rate/" . $template,
					"rate_rated",
					"rate_rating"
				);
			}

			if ($umiRegistry->getVal("//modules/vote/is_private")) {
				$blockArray['@permitted'] = "permitted";
				if (!permissionsCollection::getInstance()->is_auth()) {
					return vote::renderTemplate(
						"vote/rate/" . $template,
						"rate_rating_permitted",
						$blockArray,
						$elementId
					);
				}
			}

			if ($blockArray['is_rated']) {
				return vote::parseTemplate($ratedTpl, $blockArray, $elementId);
			}

			return vote::parseTemplate($ratingTpl, $blockArray, $elementId);
		}

		/**
		 * Формирует данные опроса
		 * @param iUmiHierarchyElement $element опрос
		 * @return array
		 */
		private function buildBlockArray(iUmiHierarchyElement $element) {
			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $element
			 */
			$rateVoters = (int) $element->getValue("rate_voters");
			$rateSum = (int) $element->getValue("rate_sum");
			$rate = $this->module->calculateRate($rateSum, $rateVoters);
			$umiRegistry = regedit::getInstance();

			return array(
				'@is_graded' => $umiRegistry->getVal("//modules/vote/is_graded") ? '1' : '0',
				'request_id' => getRequest('requestId'),
				'element_id' => $element->getId(),
				'rate_sum' => $rateSum,
				'rate_voters' => $rateVoters,
				'rate' => (float) $rate,
				'ceil_rate' => round($rate)
			);
		}

		/**
		 * Загружает в память объекты ответов на опрос
		 * @param array $answersIds идентификаторы ответов на опрос
		 * @return bool
		 * @throws selectorException
		 */
		private function loadAnswers($answersIds) {
			if (!$answersIds) {
				return false;
			}

			if (!is_array($answersIds)) {
				$answersIds = array($answersIds);
			}

			if (count($answersIds) == 0) {
				return false;
			}

			$answers = new selector('objects');
			$answers->where('id')->equals($answersIds);
			$answers->option('no-length')->value(true);
			$answers->result();
			return true;
		}
	}
?>