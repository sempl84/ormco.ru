<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = [];
	$INFO['name'] = "stat";
	$INFO['config'] = "1";
	$INFO['default_method'] = "empty";
	$INFO['default_method_admin'] = "total";
	$INFO['collect'] = "0";
	$INFO['delete_after'] = "30";
	$INFO['items_per_page'] = "100";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = [];
	$COMPONENTS[] = "./classes/components/stat/admin.php";
	$COMPONENTS[] = "./classes/components/stat/autoload.php";
	$COMPONENTS[] = "./classes/components/stat/class.php";
	$COMPONENTS[] = "./classes/components/stat/customAdmin.php";
	$COMPONENTS[] = "./classes/components/stat/customMacros.php";
	$COMPONENTS[] = "./classes/components/stat/handlers.php";
	$COMPONENTS[] = "./classes/components/stat/i18n.en.php";
	$COMPONENTS[] = "./classes/components/stat/i18n.php";
	$COMPONENTS[] = "./classes/components/stat/includes.php";
	$COMPONENTS[] = "./classes/components/stat/install.php";
	$COMPONENTS[] = "./classes/components/stat/lang.en.php";
	$COMPONENTS[] = "./classes/components/stat/lang.php";
	$COMPONENTS[] = "./classes/components/stat/macros.php";
	$COMPONENTS[] = "./classes/components/stat/permissions.php";
	$COMPONENTS[] = "./classes/components/stat/classes/holidayRoutineCounter.php";
	$COMPONENTS[] = "./classes/components/stat/classes/openstat.php";
	$COMPONENTS[] = "./classes/components/stat/classes/simpleStat.php";
	$COMPONENTS[] = "./classes/components/stat/classes/statistic.php";
	$COMPONENTS[] = "./classes/components/stat/classes/statisticFactory.php";
	$COMPONENTS[] = "./classes/components/stat/classes/xmlDecorator.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/allTagsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/auditoryActivityXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/auditoryLoyalityXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/auditoryVolumeGrowthXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/auditoryVolumeXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/cityStatXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/entryByRefererXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/entryPointsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/exitPointsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/fastUserTagsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/hostsCommonXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/openstatAdsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/openstatCampaignsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/openstatServicesXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/openstatSourcesXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/pageNextXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/pagesHitsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/pathsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/refererByEntryXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sectionHitsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesDomainsConcreteXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesDomainsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesSEOConcreteXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesSEOKeywordsConcreteXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesSEOKeywordsXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesSEOXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/sourcesTopXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/tagXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/userStatXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/visitCommonHoursXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/visitCommonXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/visitersCommonHoursXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/visitersCommonXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/decorators/visitTimeXml.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/allTags.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/auditoryActivity.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/auditoryLoyality.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/auditoryVolumeGrowth.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/auditoryVolume.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/cityStat.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/entryByReferer.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/entryPoints.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/exitPoints.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/fastUserTags.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/hostsCommon.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/openstatAds.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/openstatCampaigns.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/openstatServices.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/openstatSources.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/pageNext.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/pagesHits.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/paths.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/refererByEntry.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sectionHits.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesDomainsConcrete.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesDomains.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesSEOConcrete.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesSEOKeywordsConcrete.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesSEOKeywords.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesSEO.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/sourcesTop.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/tag.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/userStat.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/visitCommonHours.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/visitCommon.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/visitersCommonHours.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/visitersCommon.php";
	$COMPONENTS[] = "./classes/components/stat/classes/reports/visitTime.php";
