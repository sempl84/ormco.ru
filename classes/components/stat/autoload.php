<?php
	$classes = [
		'simpleStat'	=> [
			dirname(__FILE__) . '/classes/simpleStat.php'
		],

		'statistic'		=> [
			dirname(__FILE__) . '/classes/statistic.php'
		],

		'statisticFactory'	=> [
			dirname(__FILE__) . '/classes/statisticFactory.php'
		],

		'xmlDecorator'	=> [
			dirname(__FILE__) . '/classes/xmlDecorator.php'
		],

		'holidayRoutineCounter'	=> [
			dirname(__FILE__) . '/classes/holidayRoutineCounter.php'
		],

		'openstat'	=> [
			dirname(__FILE__) . '/classes/openstat.php'
		]
	];