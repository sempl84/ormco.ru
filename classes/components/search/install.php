<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "search";
	$INFO['config'] = "1";
	$INFO['default_method'] = "search_do";
	$INFO['default_method_admin'] = "index_control";
	$INFO['per_page'] = "10";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/search/admin.php";
	$COMPONENTS[] = "./classes/components/search/class.php";
	$COMPONENTS[] = "./classes/components/search/customAdmin.php";
	$COMPONENTS[] = "./classes/components/search/customMacros.php";
	$COMPONENTS[] = "./classes/components/search/handlers.php";
	$COMPONENTS[] = "./classes/components/search/i18n.en.php";
	$COMPONENTS[] = "./classes/components/search/i18n.php";
	$COMPONENTS[] = "./classes/components/search/includes.php";
	$COMPONENTS[] = "./classes/components/search/install.php";
	$COMPONENTS[] = "./classes/components/search/lang.en.php";
	$COMPONENTS[] = "./classes/components/search/lang.php";
	$COMPONENTS[] = "./classes/components/search/macros.php";
	$COMPONENTS[] = "./classes/components/search/permissions.php";
	$COMPONENTS[] = "./classes/components/search/sphinx.php";
?>
