<?php
	/**
	 * Класс функционала административной панели
	 */
	class ExchangeAdmin {

		use baseModuleAdmin;
		/**
		 * @var exchange $module
		 */
		public $module;

		/**
		 * Выполняет одну итерацию импорта заданного
		 * сценария и возвращает результат операции.
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws publicException
		 */
		public function import_do() {
			if (isDemoMode()) {
				throw new publicAdminException(getLabel('label-stop-in-demo'));
			}

			$this->setDataType("list");
			$this->setActionType("view");

			$id = getRequest('param0');
			$objects = umiObjectsCollection::getInstance();

			$settings = $objects->getObject($id);
			if (!$settings instanceof umiObject) {
				throw new publicException(getLabel("exchange-err-settings_notfound"));
			}

			$importFile = $settings->getValue('file');
			if (!($importFile instanceof umiFile) || ($importFile->getIsBroken())) {
				throw new publicException(getLabel("exchange-err-importfile"));
			}

			$format_id = $settings->getValue('format');
			$importFormat = $objects->getObject($format_id);
			if (!$importFormat instanceof umiObject) {
				throw new publicException(getLabel("exchange-err-format_undefined"));
			}

			$suffix = $importFormat->getValue('sid');
			$session = \UmiCms\Service::Session();
			$import_offset = (int) $session->get("import_offset_" . $id);
			$umiConfig = mainConfiguration::getInstance();
			$blockSize = $umiConfig->get("modules", "exchange.splitter.limit") ? $umiConfig->get("modules", "exchange.splitter.limit") : 25;

			$splitter = umiImportSplitter::get($suffix);

			if ($splitter instanceof csvSplitter) {
				$scenarioEncodingId = $settings->getValue('encoding_import');
				$scenarioEncodingCode = '';
				$scenarioEncoding = $objects->getObject($scenarioEncodingId);

				if ($scenarioEncoding instanceof iUmiObject) {
					$scenarioEncodingCode = $scenarioEncoding->getName();
				}

				$defaultConfigEncoding = $umiConfig->get('system', 'default-exchange-encoding');
				$defaultEncoding = 'windows-1251';

				$encoding = $scenarioEncodingCode ? $scenarioEncodingCode : $defaultConfigEncoding;

				try {
					$splitter->setEncoding($encoding);
				} catch (InvalidArgumentException $e) {
					$splitter->setEncoding($defaultEncoding);
				}
			}

			$splitter->load($importFile->getFilePath(), $blockSize, $import_offset);
			$doc = $splitter->getDocument();
			$dump = $splitter->translate($doc);

			$oldIgnoreSiteMap =  umiHierarchy::$ignoreSiteMap;
			umiHierarchy::$ignoreSiteMap = true;

			$importer = new xmlImporter();
			$importer->loadXmlString($dump);

			$elements = $settings->getValue('elements');

			if (is_array($elements) && count($elements)) {
				$importer->setDestinationElement($elements[0]);
			}

			$importer->setIgnoreParentGroups($splitter->ignoreParentGroups);
			$importer->setAutoGuideCreation($splitter->autoGuideCreation);
			$importer->setRenameFiles($splitter->getRenameFiles());

			$eventPoint = new umiEventPoint("exchangeImport");
			$eventPoint->setMode("before");
			$eventPoint->addRef("importer", $importer);
			$eventPoint->call();

			$importer->execute();

			umiHierarchy::$ignoreSiteMap = $oldIgnoreSiteMap;

			$progressKey = "import_offset_" . $id;
			$session->set($progressKey, $splitter->getOffset());

			if ($splitter->getIsComplete()) {
				$session->del($progressKey);
			}

			if ($splitter->getIsComplete()) {
				$importFinished = new umiEventPoint('exchangeOnImportFinish');
				$importFinished->setMode('after');
				$importFinished->addRef('settings', $settings);
				$importFinished->addRef('splitter', $splitter);
				$importFinished->setParam('scenario_id', $id);
				$importFinished->call();
			}

			$data = array(
				"attribute:complete" => (int) $splitter->getIsComplete(),
				"attribute:created" => $importer->getCreatedEntityCount(),
				"attribute:updated" => $importer->getUpdatedEntityCount(),
				"attribute:deleted" => $importer->getDeletedEntityCount(),
				"attribute:errors" => $importer->getErrorCount(),
				"nodes:log" => $importer->getImportLog()
			);

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Выполняет одну итерацию подготовки страниц к экспорту в
		 * формате YML и возвращает результат операции.
		 * @throws publicException
		 */
		public function prepareElementsToExport() {
			$objectId = getRequest('param0');
			$complete = false;

			$objects = umiObjectsCollection::getInstance();
			$object = $objects->getObject($objectId);
			$formatId = $object->getValue('format');
			$format = $objects->getObject($formatId);
			$suffix = $format->getValue('sid');

			if ($suffix != 'YML') {
				$data = array(
					"attribute:complete" => (int) $complete,
					"attribute:preparation" => (int) !$complete,
				);

				$this->setData($data);
				$this->doData();
				return;
			}

			$session = \UmiCms\Service::Session();
			$offset = (int) $session->get("export_offset_" . $objectId);
			$umiConfig = mainConfiguration::getInstance();
			$blockSize = $umiConfig->get("modules", "exchange.splitter.limit") ? $umiConfig->get("modules", "exchange.splitter.limit") : 25;

			if (!file_exists(SYS_TEMP_PATH . "/yml/" . $objectId . 'el')) {
				throw new publicException(
					'<a href="' . getLabel("label-errors-no-information") . '" target="blank">' . getLabel("label-errors-no-information") .'</a>'
				);
			}

			$elementsToExport = unserialize(file_get_contents(SYS_TEMP_PATH . "/yml/" . $objectId . 'el'));
			$elements = umiHierarchy::getInstance();

			$errors = array();
			for ($i = $offset; $i <= $offset + $blockSize -1; $i++) {

				if (!array_key_exists($i, $elementsToExport)) {
					$complete = true;
					break;
				}

				$element = $elements->getElement($elementsToExport[$i]);

				if ($element instanceof umiHierarchyElement) {
					try {
						$element->updateYML();
					} catch (Exception $e) {
						$errors[] = $e->getMessage() . " #{$elementsToExport[$i]}";
					}
				}
			}

			$progressKey = "export_offset_" . $objectId;
			$progress = $offset + $blockSize;

			$session->set($progressKey, $progress);

			if ($complete) {
				$session->del($progressKey);
			}

			$data = array(
				"attribute:complete" => (int) $complete,
				"nodes:log" => $errors
			);

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Выполняет сценарий экспорта и возвращает содержимое полученного файла
		 * в буффер, либо инициирует скачивание этого файла.
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws publicException
		 */
		public function get_export() {
			if (isDemoMode()) {
				throw new publicAdminException(getLabel('label-stop-in-demo'));
			}

			$id = (int) getRequest('param0');
			$as_file = getRequest('as_file');
			$objects = umiObjectsCollection::getInstance();

			$settings = $objects->getObject($id);

			if (!$settings instanceof umiObject) {
				throw new publicException(getLabel("exchange-err-settings_notfound"));
			}

			$format_id = $settings->getValue('format');
			$exportFormat = $objects->getObject($format_id);

			if (!$exportFormat instanceof umiObject) {
				throw new publicException(getLabel("exchange-err-format_undefined"));
			}

			$suffix = $exportFormat->getValue('sid');
			/**
			 * @var umiExporter $exporter
			 */
			$exporter = umiExporter::get($suffix);

			if ($settings->getValue('source_name')) {
				$exporter->setSourceName($settings->getValue('source_name'));
			}

			if ($exporter instanceof csvExporter) {
				/**
				 * @var umiExporter|csvExporter $exporter
				 */
				$scenarioEncodingId = $settings->getValue('encoding_export');
				$scenarioEncodingCode = '';
				$scenarioEncoding = $objects->getObject($scenarioEncodingId);

				if ($scenarioEncoding instanceof iUmiObject) {
					$scenarioEncodingCode = $scenarioEncoding->getName();
				}

				$defaultConfigEncoding = mainConfiguration::getInstance()->get('system', 'default-exchange-encoding');
				$defaultEncoding = 'windows-1251';
				$encoding = $scenarioEncodingCode ? $scenarioEncodingCode : $defaultConfigEncoding;

				try{
					$exporter->setEncoding($encoding);
				} catch (InvalidArgumentException $e) {
					$exporter->setEncoding($defaultEncoding);
				}

			}

			$cache_time = (int) $settings->cache_time;
			$temp_dir = SYS_TEMP_PATH . '/export/';

			if (!is_dir($temp_dir)) {
				mkdir($temp_dir, 0777, true);
			}

			$cache_file_path = $temp_dir . $id . "." . $exporter->getFileExt();

			if ($exporter->getIsCompleted()) {
				$exportFinished = new umiEventPoint('exchangeOnExportFinish');
				$exportFinished->setMode('after');
				$exportFinished->addRef('settings', $settings);
				$exportFinished->addRef('exporter', $exporter);
				$exportFinished->setParam('scenario_id', $id);
				$exportFinished->call();
			}

			if ($as_file === '1') {
				$temp_folder = $temp_dir . $id;

				if (is_dir($temp_folder)) {

					$tempArchivePath = $temp_dir . $id . ".zip";

					if (file_exists($tempArchivePath)) {
						unlink($tempArchivePath);
					}

					$archive = new UmiZipArchive($tempArchivePath);
					$archive->add(array($temp_dir . $id . ".xml", $temp_folder), SYS_TEMP_PATH . '/export');
					shell_exec("rm -rf {$temp_folder}");

					$zipFile = new umiFile($temp_dir . $id . ".zip");
					$zipFile->download();
				}

				$cache_file = new umiFile($cache_file_path);
				$cache_file->download();
			}

			if ($as_file === '0') {
				if (!file_exists($cache_file_path) || !$cache_time || ($cache_time && (time() > filectime($cache_file_path) + $cache_time * 60))) {
					$result = $exporter->export($settings->getValue('elements'), $settings->getValue('excluded_elements'));
					if ($result) {
						file_put_contents($cache_file_path, $result);
					}
				}
				$buffer = $exporter->setOutputBuffer();
				$buffer->push(file_get_contents($cache_file_path));
				$buffer->end();
			}

			if ($as_file === null && (!file_exists($cache_file_path) || !$cache_time || ($cache_time && (time() > filectime($cache_file_path) + $cache_time * 60)))) {
				$result = $exporter->export($settings->getValue('elements'), $settings->getValue('excluded_elements'));
				if ($result) {
					file_put_contents($cache_file_path, $result);
				}
			}


			$data = array(
				"attribute:complete" => (int) $exporter->getIsCompleted()
			);

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список сценариев импорта
		 * @throws coreException
		 * @throws selectorException
		 */
		public function import() {
			$this->setDataType("list");
			$this->setActionType("view");

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$sel = new selector('objects');
			$sel->types('object-type')->name('exchange', 'import');
			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result(), "objects");
			$this->setData($data, $sel->length());
			$this->doData();
		}

		/**
		 * Возвращает список сценариев экспорта
		 * @throws coreException
		 * @throws selectorException
		 */
		public function export() {
			$this->setDataType("list");
			$this->setActionType("view");

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$sel = new selector('objects');
			$sel->types('object-type')->name('exchange', 'export');
			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result(), "objects");
			$this->setData($data, $sel->length());
			$this->doData();
		}

		/**
		 * Возвращает данные для построения формы создания сущности модуля.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do, то создает сущность
		 * и перенаправляет на страницу, где ее можно отредактировать.
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws publicException
		 * @throws wrongElementTypeAdminException
		 */
		public function add() {
			$type = (string) getRequest('param0');
			$mode = (string) getRequest('param1');
			$this->setHeaderLabel("header-exchange-add-" . $type);

			$inputData = array(
				'type' => $type,
				'allowed-element-types'	=> array(
					'import',
					'export'
				),
			);

			if ($mode == "do") {
				$object = $this->saveAddedObjectData($inputData);
				$this->module->saveScenarioCache($object->getId());
				$this->chooseRedirect($this->module->pre_lang . '/admin/exchange/edit/' . $object->getId() . '/');
			}

			$this->setDataType("form");
			$this->setActionType("create");

			$data = $this->prepareData($inputData, "object");
			$data['default-encoding'] = $this->module->getDefaultEncoding();
			$data['object-type'] = $type;
			/**
			 * @var iUmiObject|iUmiEntinty $csvFormat
			 */
			$csvFormat = $this->module->getFormatByCode('CSV', $type);

			if ($csvFormat instanceof iUmiObject) {
				$data['csv-format-id'] = $csvFormat->getId();
			}

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для построения формы редактирования сущности модуля.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do, то сохраняет изменения сущности
		 * и осуществляет перенаправление. Адрес перенаправления зависит от режиме кнопки "Сохранить".
		 * @throws coreException
		 * @throws expectObjectException
		 */
		public function edit() {
			$object = $this->expectObject("param0", true);
			$mode = (string) getRequest('param1');
			$objectId = $object->getId();
			$this->setHeaderLabel("header-exchange-edit-" . $this->getObjectTypeMethod($object));

			$inputData = Array(
				"object" => $object,
				"allowed-element-types"	=> Array(
					'import',
					'export'
				)
			);

			if ($mode == "do") {
				$this->saveEditedObjectData($inputData);
				$this->module->saveScenarioCache($objectId);
				$this->chooseRedirect();
			}

			$this->setDataType("form");
			$this->setActionType("modify");

			$data = $this->prepareData($inputData, "object");
			$data['default-encoding'] = $this->module->getDefaultEncoding();
			$objectType = $object->getType();
			$type = '';

			if ($objectType instanceof iUmiObjectType) {
				$type = $objectType->getMethod();
			}

			$data['object-type'] = $type;
			$csvFormat = $this->module->getFormatByCode('CSV', $type);
			/**
			 * @var iUmiObject|iUmiEntinty $csvFormat
			 */
			if ($csvFormat instanceof iUmiObject) {
				$data['csv-format-id'] = $csvFormat->getId();
			}

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Удаляет сущности модуля
		 * @throws coreException
		 * @throws expectObjectException
		 * @throws wrongElementTypeAdminException
		 */
		public function del() {
			$objects = getRequest('element');

			if (!is_array($objects)) {
				$objects = Array($objects);
			}

			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);

				$params = Array(
					'object' => $object,
					'allowed-element-types' => Array(
						'import',
						'export'
					)
				);

				$this->deleteObject($params);
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($objects, "objects");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 */
		public function getDatasetConfiguration($param = '') {
			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();

			switch ($param) {
				case 'export' : {
					$loadMethod = 'export';
					$typeId		= $umiObjectTypesCollection->getTypeIdByHierarchyTypeName('exchange', 'export');
					$defaults	= 'name[400px]|format[350px]';
					break;
				}
				default: {
					$loadMethod = 'import';
					$typeId		= $umiObjectTypesCollection->getTypeIdByHierarchyTypeName('exchange', 'import');
					$defaults	= 'name[400px]|format[350px]';
				}
			}

			return array(
				'methods' => array(
					array(
						'title'		=> getLabel('smc-load'),
						'forload'	=> true,
						'module'	=> 'exchange',
						'#__name'	=> $loadMethod
					),
					array(
						'title'		=> getLabel('smc-delete'),
						'module'	=> 'exchange',
						'#__name'	=> 'del',
						'aliases'	=> 'tree_delete_element,delete,del'
					)
				),
				'types' => array(
					array(
						'common' 	=> 'true',
						'id' 		=> $typeId
					)
				),
				'stoplist' => array(''),
				'default' => $defaults
			);
		}
	}
?>
