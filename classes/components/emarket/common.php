<?php
	/**
	 * Класс методов для клиентского и административного режимов
	 */
	class EmarketCommon {
		/**
		 * @var emarket $module
		 */
		public $module;

		/**
		 * Возвращает данные заказа
		 * @param bool $orderId идентификатор заказа
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws publicException
		 */
		public function order($orderId = false, $template = 'default') {
			if (!$template) {
				$template = 'default';
			}

			$permissions = permissionsCollection::getInstance();
			$orderId = (int) ($orderId ? $orderId : getRequest('param0'));

			if (!$orderId) {
				throw new publicException("You should specify order id");
			}

			$order = order::get($orderId);

			if ($order instanceof order == false) {
				throw new publicException("Order #{$orderId} doesn't exist");
			}

			if (
				!$permissions->isSv() &&
				($order->getName() !== 'dummy') &&
				(customer::get()->getId() != $order->customer_id) &&
				!$permissions->isAllowedMethod($permissions->getUserId(), "emarket", "control")
			) {
				throw new publicException(getLabel('error-require-more-permissions'));
			}

			list($tpl_block, $tpl_block_empty) = emarket::loadTemplates(
				"emarket/" . $template,
				'order_block',
				'order_block_empty'
			);

			$discount = $order->getDiscount();
			$totalAmount = $order->getTotalAmount();
			$originalPrice = $order->getOriginalPrice();
			$actualPrice = $order->getActualPrice();
			$deliveryPrice = $order->getDeliveryPrice();
			$bonusDiscount = $order->getBonusDiscount();

			if ($originalPrice == $actualPrice) {
				$originalPrice = null;
			}

			/**
			 * @var emarket|EmarketCommon $module
			 */
			$module = $this->module;
			$discountAmount = ($originalPrice) ? $originalPrice + $deliveryPrice - $actualPrice - $bonusDiscount : 0;
			$steps = null;

			if (cmsController::getInstance()->getCurrentMode() != 'admin') {
				/**
				 * @var emarket|EmarketCommon|EmarketPurchasingStages $module
				 */
				$steps = $module->getPurchaseSteps($template, null);
			}

			$result = array(
				'attribute:id'	=> ($orderId),
				'xlink:href'	=> ('uobject://' . $orderId),
				'customer'		=> ($order->getName() == 'dummy') ? null : $module->renderOrderCustomer($order, $template),
				'subnodes:items'=> ($order->getName() == 'dummy') ? null : $module->renderOrderItems($order, $template),
				'delivery'		=> $module->renderOrderDelivery($order, $template),
				'summary'		=> array(
					'amount'		=> $totalAmount,
					'price'			=> $module->formatCurrencyPrice(array(
						'original'		=> $originalPrice,
						'delivery'		=> $deliveryPrice,
						'actual'		=> $actualPrice,
						'discount'		=> $discountAmount,
						'bonus'			=> $bonusDiscount
					))
				),
				'discount_value' => $order->getDiscountValue(),
				'steps' => $steps
			);

			if ($order->number) {
				$result['number'] = $order->number;
				$result['status'] = selector::get('object')->id($order->status_id);
			}

			if (sizeof($result['subnodes:items']) == 0) {
				$tpl_block = $tpl_block_empty;
			}

			$result['void:total-price'] = $module->parsePriceTpl($template, $result['summary']['price']);
			$result['void:delivery-price'] = $module->parsePriceTpl($template, $module->formatCurrencyPrice(
				array('actual' => $deliveryPrice)
			));

			$result['void:bonus'] = $module->parsePriceTpl($template, $module->formatCurrencyPrice(
				array('actual' => $bonusDiscount)
			));

			$result['void:total-amount'] = $totalAmount;
			$result['void:discount_id'] = false;

			if ($discount instanceof discount) {
				$result['discount'] = array(
					'attribute:id'		=> $discount->id,
					'attribute:name'	=> $discount->getName(),
					'description'		=> $discount->getValue('description')
				);
				$result['void:discount_id'] = $discount->id;
			}

			return emarket::parseTemplate($tpl_block, $result, false, $order->id);
		}

		/**
		 * Возвращает оформленные цены
		 * @param string $template имя шаблона (для tpl)
		 * @param array $priceData значения цен
		 * @return array
		 */
		public function parsePriceTpl($template = 'default', $priceData = array()) {
			if (emarket::isXSLTResultMode()) {
				return $priceData;
			}

			list($tpl_original, $tpl_actual) = emarket::loadTemplates(
				"emarket/" . $template,
				'price_original',
				'price_actual'
			);

			$originalPrice = getArrayKey($priceData, 'original');
			$actualPrice = getArrayKey($priceData, 'actual');

			$result = array();
			$result['original'] = emarket::parseTemplate(($originalPrice ? $tpl_original : ''), $priceData);
			$result['actual'] = emarket::parseTemplate(($actualPrice ? $tpl_actual : ''), $priceData);
			return $result;
		}

		/**
		 * Формирует цену и пересчитывает ее разные валюты
		 * @param int|float $price цены
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 */
		public function applyPriceCurrency($price = 0, $template = 'default') {
			list($tpl_block) = emarket::loadTemplates(
				"emarket/{$template}",
				'price_block'
			);

			/**
			 * @var emarket|EmarketCommon $module
			 */
			$module = $this->module;
			$price = $module->parsePriceTpl($template, $module->formatCurrencyPrice(array(
				'actual' => $price
			)));

			$result = array(
				'price' => $price
			);
			$result['void:price-original'] = getArrayKey($result['price'], 'original');
			$result['void:price-actual'] = getArrayKey($result['price'], 'actual');

			return emarket::parseTemplate($tpl_block, $result);
		}

		/**
		 * Пересчитывает каждую цену в валюту и возвращает список полученных цен
		 * @param array $prices список цен
		 * @param iUmiObject $currency валюта, в которую требуется пересчитать цены
		 * @param iUmiObject $defaultCurrency валюта по умолчанию
		 * @return array
		 * @throws coreException
		 */
		public function formatCurrencyPrice($prices, iUmiObject $currency = null, iUmiObject $defaultCurrency = null) {
			/**
			 * @var emarket|EmarketCommon $module
			 */
			$module = $this->module;

			if (is_null($defaultCurrency)) {
				/**
				 * @var iUmiObject|iUmiEntinty $defaultCurrency
				 */
				$defaultCurrency = $module->getDefaultCurrency();
			}

			/**
			 * @var iUmiObject|iUmiEntinty $currentCurrency
			 */
			$currentCurrency = $module->getCurrentCurrency();

			/**
			 * @var iUmiObject|iUmiEntinty $currency
			 */
			if (is_null($currency)) {
				$currency = $currentCurrency;
			} else {
				if (($currency->getId() == $currentCurrency->getId()) && ($defaultCurrency == $module->getDefaultCurrency())) {
					return $prices;
				}
			}

			$result = array(
				'attribute:name' => $currency->getName(),
				'attribute:code' => $currency->getValue('codename'),
				'attribute:rate' => $currency->getValue('rate'),
				'attribute:nominal' => $currency->getValue('nominal'),
				'void:currency_name' => $currency->getValue('name')
			);

			if ($currency->getValue('prefix')) {
				$result['attribute:prefix'] = $currency->getValue('prefix');
			} else {
				$result['void:prefix'] = false;
			}

			if ($currency->getValue('suffix')) {
				$result['attribute:suffix'] = $currency->getValue('suffix');
			} else {
				$result['void:suffix'] = false;
			}

			foreach ($prices as $key => $price) {
				if ($price == null) {
					$result[$key] = null;
					continue;
				}

				$price = $price * $defaultCurrency->getValue('nominal') * $defaultCurrency->getValue('nominal');
				$price = $price  / $currency->getValue('rate') / $currency->getValue('nominal');
				$result[$key] = round($price, 2);
			}

			return $result;
		}

		/**
		 * Возвращает данные о адресе или способе доставки заказа
		 * @param order $order заказ
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 */
		public function renderOrderDelivery(order $order, $template = 'default') {
			$objectsCollection = umiObjectsCollection::getInstance();

			list($tpl, $tplMethod, $tplAddress, $tplPrice) = emarket::loadTemplates(
				'emarket/' . $template,
				'order_delivery',
				'delivery_method',
				'delivery_address',
				'delivery_price'
			);

			$result = array();
			$method = $objectsCollection->getObject($order->delivery_id);

			if ($method instanceof iUmiObject == false) {
				return emarket::parseTemplate($tpl, $result);
			}

			$deliveryMethod = array(
				'attribute:id' => $method->getId(),
				'attribute:name' => $method->getName(),
				'xlink:href' => ('uobject://' . $method->getId()),
			);

			$result['method'] = emarket::parseTemplate($tplMethod, $deliveryMethod);

			/**
			 * @var umiObject $address
			 */
			$address = $objectsCollection->getObject($order->getValue('delivery_address'));

			if ($address instanceof iUmiObject) {
				$country = $objectsCollection->getObject($address->getValue('country'));
				$countryName = $country instanceof iUmiObject ? $country->getName() : '';
				$deliveryAddress = array(
					'attribute:id' => $address->getId(),
					'attribute:name' => $address->getName(),
					'xlink:href' => ('uobject://' . $address->getId()),
					'country' => $countryName,
					'index' => $address->getValue('index'),
					'region' => $address->getValue('region'),
					'city' => $address->getValue('city'),
					'street' => $address->getValue('street'),
					'house' => $address->getValue('house'),
					'flat' => $address->getValue('flat'),
					'comment' => $address->getValue('order_comments'),
				);
				$result['address'] = emarket::parseTemplate($tplAddress, $deliveryAddress);
			}

			$result['price'] = emarket::parseTemplate($tplPrice, $this->formatCurrencyPrice(array(
				'delivery' => $order->getValue('delivery_price')
			)));

			return emarket::parseTemplate($tpl, $result);
		}

		/**
		 * Возвращает данные покупателя
		 * @param order $order заказ покупателя
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws publicException
		 */
		public function renderOrderCustomer(order $order, $template = 'default') {
			$customer = selector::get('object')->id($order->customer_id);

			if ($customer instanceof iUmiObject == false) {
				throw new publicException(getLabel('error-object-does-not-exist', null, $order->customer_id));
			}

			/**
			 * @var iUmiObject|iUmiEntinty $customer
			 */
			list($tpl_user, $tpl_guest) = emarket::loadTemplates(
				"emarket/customer/" . $template,
				"customer_user",
				"customer_guest"
			);

			/**
			 * @var iUmiObjectType $objectType
			 */
			$objectType = selector::get('object-type')->id($customer->getTypeId());
			$tpl = ($objectType->getModule() == 'users') ? $tpl_user : $tpl_guest;

			return emarket::parseTemplate($tpl, array(
				'full:object' => $customer
			), false, $customer->getId());
		}

		/**
		 * Возвращает данные списка товаров заказа
		 * @param order $order заказ
		 * @param string $template имя шаблона (для tpl)
		 * @return array
		 */
		public function renderOrderItems(order $order, $template = 'default') {
			$items_arr = array();
			$objects = umiObjectsCollection::getInstance();

			list($tpl_item, $tpl_options_block, $tpl_options_block_empty, $tpl_options_item) = emarket::loadTemplates(
				"emarket/" . $template,
				'order_item',
				'options_block',
				'options_block_empty',
				'options_item'
			);

			$orderItems = $order->getItems();

			if (count($orderItems) == 0) {
				return emarket::parseTemplate($tpl_options_block_empty, array());
			}

			/**
			 * @var emarket|EmarketCommon $module
			 */
			$module = $this->module;
			$isBasket = emarket::isBasket($order);
			/**
			 * @var orderItem $orderItem
			 */
			foreach ($orderItems as $orderItem) {
				$orderItemId = $orderItem->getId();

				$item_arr = array(
					'attribute:id'	=> $orderItemId,
					'attribute:name'=> htmlspecialchars($orderItem->getName()),
					'xlink:href'	=> 'uobject://' . $orderItemId,
					'amount'		=> $orderItem->getAmount(),
					'options'		=> null
				);

				$plainPriceOriginal = $orderItem->getItemPrice();

				if ($isBasket) {
					$itemDiscount = $orderItem->getDiscount();
					$plainPriceActual = ($itemDiscount instanceof itemDiscount) ? $itemDiscount->recalcPrice($plainPriceOriginal) : $plainPriceOriginal;
					$pricesDiff = ($plainPriceOriginal - $plainPriceActual);
					$discountValue = ($pricesDiff < 0) ? 0 : $pricesDiff;
				} else {
					$discountValue = $orderItem->getDiscountValue();
					$plainPriceActual = $plainPriceOriginal - $discountValue;
				}

				$totalPriceOriginal = $orderItem->getTotalOriginalPrice();
				$totalPriceActual = $orderItem->getTotalActualPrice();

				if ($plainPriceOriginal == $plainPriceActual) {
					$plainPriceOriginal = null;
				}

				if ($totalPriceOriginal == $totalPriceActual) {
					$totalPriceOriginal = null;
				}

				$item_arr['price'] = $module->formatCurrencyPrice(array(
					'original'	=> $plainPriceOriginal,
					'actual'	=> $plainPriceActual
				));

				$item_arr['total-price'] = $module->formatCurrencyPrice(array(
					'original'	=> $totalPriceOriginal,
					'actual'	=> $totalPriceActual
				));

				$item_arr['price'] = $module->parsePriceTpl($template, $item_arr['price']);
				$item_arr['total-price'] = $module->parsePriceTpl($template, $item_arr['total-price']);
				$item_arr['discount_value'] = (float) $discountValue;

				$status = order::getCodeByStatus($order->getOrderStatus());

				if (!$status || $status == 'basket') {
					$element = $orderItem->getItemElement();
				} else {
					$symlink = $orderItem->getObject()->getValue('item_link');

					if (is_array($symlink) && sizeof($symlink)) {
						list($item) = $symlink;
						$element = $item;
					} else {
						$element = null;
					}
				}

				/**
				 * @var iUmiHierarchyElement|iUmiEntinty $element
				 */
				if ($element instanceof iUmiHierarchyElement) {
					$item_arr['page'] = $element;
					$item_arr['void:element_id'] = $element->getId();
					$item_arr['void:link'] = $element->link;
				}

				$discountAmount = $totalPriceOriginal ? $totalPriceOriginal - $totalPriceActual : 0;
				$discount = $orderItem->getDiscount();

				if ($discount instanceof itemDiscount) {
					$item_arr['discount'] = array(
						'attribute:id' => $discount->getId(),
						'attribute:name' => $discount->getName(),
						'description' => $discount->getValue('description'),
						'amount' => $discountAmount
					);
					$item_arr['void:discount_id'] = $discount->getId();
				}

				$elementId = ($element instanceof iUmiHierarchyElement) ? $element->getId() : null;

				if ($orderItem instanceof optionedOrderItem) {
					/**
					 * @var optionedOrderItem $orderItem
					 */
					$options = $orderItem->getOptions(); $options_arr = array();

					foreach ($options as $optionInfo) {
						$optionId = $optionInfo['option-id'];
						$price = $optionInfo['price'];
						$fieldName = $optionInfo['field-name'];
						$option = $objects->getObject($optionId);
						if ($option instanceof iUmiObject) {
							$option_arr = array(
								'attribute:id' => $optionId,
								'attribute:name' => $option->getName(),
								'attribute:price' => $price,
								'attribute:field-name' => $fieldName,
								'attribute:element_id' => $elementId,
								'xlink:href'=> ('uobject://' . $optionId)
							);

							$options_arr[] = emarket::parseTemplate($tpl_options_item, $option_arr, false, $optionId);
						}
					}

					$item_arr['options'] = emarket::parseTemplate($tpl_options_block, array(
						'nodes:option' => $options_arr,
						'void:items' => $options_arr
					));
				}

				$items_arr[] = emarket::parseTemplate($tpl_item, $item_arr);
			}

			return $items_arr;
		}
	}
?>