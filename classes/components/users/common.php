<?php

	/** Класс методов для клиентского и административного режимов */
	class UsersCommon {

		/** Адрес API ulogin */
		const ULOGIN_URL = 'http://ulogin.ru/token.php?';

		/** @var users $module */
		public $module;

		/**
		 * Выводит форму авторизации
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function login($template = "default") {
			if (!$template) {
				$template = "default";
			}

			$from_page = getRequest('from_page');

			if (!$from_page) {
				$from_page = getServer('REQUEST_URI');
			}

			if (defined("CURRENT_VERSION_LINE") && CURRENT_VERSION_LINE == 'demo') {
				list($template_login) = users::loadTemplates("users/" . $template, "login_demo");
			} else {
				list($template_login) = users::loadTemplates("users/" . $template, "login");
			}

			$block_arr = [];
			$block_arr['from_page'] = users::protectStringVariable($from_page);

			return users::parseTemplate($template_login, $block_arr);
		}

		/**
		 * Выводит форму авторизации для пользователя либо информацию об авторизованном пользователе
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed|void
		 */
		public function auth($template = "default") {
			if (!$template) {
				$template = "default";
			}

			if ($this->module->is_auth()) {
				if (cmsController::getInstance()->getCurrentMode() == "admin") {
					$this->module->redirect($this->module->pre_lang . "/admin/");
				} else {
					list($template_logged) = users::loadTemplates("users/" . $template, "logged");

					$block_arr = [];
					$block_arr['xlink:href'] = "uobject://" . $this->module->user_id;
					$block_arr['user_id'] = $this->module->user_id;
					$block_arr['user_name'] = $this->module->user_fullname;
					$block_arr['user_login'] = $this->module->user_login;

					return users::parseTemplate($template_logged, $block_arr, false, $this->module->user_id);
				}
			}

			return $this->login($template);
		}

		/**
		 * Авторизует пользователя
		 * @return mixed|string
		 * @throws publicAdminException если авторизация не удалась через административную панель
		 */
		public function login_do() {
			$res = "";
			$login = getRequest('login');
			$password = getRequest('password');
			$from_page = getRequest('from_page');

			if (strlen($login) == 0) {
				return $this->auth();
			}

			$permissions = permissionsCollection::getInstance();
			$cmsController = cmsController::getInstance();

			$user = $permissions->checkLogin($login, $password);

			if ($user instanceof iUmiObject) {
				/* @var iUmiObject|iUmiEntinty $user */

				if (getSession('fake-user') == 1) {
					return ($this->module->restoreUser(true)) ? $this->auth() : $res;
				}

				$permissions->loginAsUser($user);

				$session = session::getInstance();

				if ($permissions->isAdmin($user->getId())) {
					$session->set('csrf_token', md5(rand() . microtime()));
					if ($permissions->isSv($user->getId())) {
						$session->set('user_is_sv', true);
					}
				}

				$session->setValid();
				session::commit();
				system_runSession();

				$oEventPoint = new umiEventPoint("users_login_successfull");
				$oEventPoint->setParam("user_id", $user->id);
				users::setEventPoint($oEventPoint);
				$module = $this->module;

				if ($cmsController->getCurrentMode() == "admin") {
					ulangStream::getLangPrefix();
					system_get_skinName();
					/* @var UsersAdmin|users $module */
					$module->chooseRedirect($from_page);
				} else {
					/* @var UsersMacros|users $module */
					if (!$from_page) {
						$from_page = getServer('HTTP_REFERER');
					}

					$module->redirect($from_page ? $from_page : ($module->pre_lang . '/users/auth/'));
				}
			} else {
				$oEventPoint = new umiEventPoint("users_login_failed");
				$oEventPoint->setParam("login", $login);
				$oEventPoint->setParam("password", $password);
				users::setEventPoint($oEventPoint);

				if ($cmsController->getCurrentMode() == "admin") {
					throw new publicAdminException(getLabel('label-text-error'));
				}

				/**
				 * @var users|UsersMacros $this
				 */
				return $this->auth();
			}

			return $res;
		}

		/**
		 * Регистрирует пользователя с помощью Loginza
		 * @return mixed
		 * @throws coreException
		 * @throws selectorException
		 */
		public function loginza() {
			/* @var users|UsersCommon $this */
			if (empty($_POST['token'])) {
				return $this->auth();
			}

			$loginzaAPI = new loginzaAPI();
			$profile = $loginzaAPI->getAuthInfo($_POST['token']);

			if (empty($profile)) {
				return $this->auth();
			}

			$profile = new loginzaUserProfile($profile);

			$nickname = $profile->genNickname();
			$provider = $profile->genProvider();
			$provider_url = parse_url($provider);
			$provider_name = str_ireplace('www.', '', $provider_url['host']);
			$login = $nickname . "@" . $provider_name;
			$password = md5($profile->genRandomPassword());
			$email = $profile->genUserEmail();
			$lname = $profile->getLname();
			$fname = $profile->getFname();

			if (!$fname) {
				$fname = $nickname;
			}

			$this->tryToLoginAsExistingSocialUser($login, $provider_name);

			if (!preg_match("/.+@.+\..+/", $email)) {
				while (true) {
					$email = $nickname . rand(1, 100) . "@" . getServer('HTTP_HOST');
					if ($this->module->checkIsUniqueEmail($email)) {
						break;
					}
				}
			}

			$this->createNewSocialUserAndLogin([
				'login' => $login,
				'password' => $password,
				'email' => $email,
				'firstName' => $fname,
				'lastName' => $lname,
				'network' => $provider_name
			]);
		}

		/**
		 * Регистрирует пользователя с помощью сервиса ulogin.ru
		 *
		 * В ответе от uLogin обязательно должен прийти параметры:
		 * 'network'
		 * 'first_name'
		 * 'nickname'
		 * 'email'
		 *
		 * @link http://ulogin.ru/help.php#fields описание параметров
		 * @return mixed
		 * @throws coreException
		 * @throws selectorException
		 */
		public function ulogin() {
			if (empty($_POST['token'])) {
				return $this->auth();
			}

			$params = [
					'token' => $_POST['token'],
					'host' => $_SERVER['HTTP_HOST']
			];

			$response = umiRemoteFileGetter::get(self::ULOGIN_URL . http_build_query($params));
			$data = json_decode($response);

			if (empty($data->network) || empty($data->first_name) ||
					empty($data->nickname) || empty($data->email)
			) {
				return $this->auth();
			}

			$network = $data->network;
			$login = $data->nickname . '@' . $network;

			$this->tryToLoginAsExistingSocialUser($login, $network);

			$password = md5($this->module->getRandomPassword());
			$firstName = $data->first_name;
			$lastName = (isset($data->last_name) ? $data->last_name : '');
			$candidateEmail = $data->email;

			$email = $candidateEmail;
			$prefix = 1;

			while (!$this->module->checkIsUniqueEmail($email)) {
				$email = $prefix . $candidateEmail;
				$prefix += 1;
			}

			$this->createNewSocialUserAndLogin([
					'login' => $login,
					'password' => $password,
					'email' => $email,
					'firstName' => $firstName,
					'lastName' => $lastName,
					'network' => $network
			]);
		}

		protected function tryToLoginAsExistingSocialUser($login, $network) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('login')->equals($login);
			$sel->where('loginza')->equals($network);

			$user = $sel->first;
			$fromPage = getRequest("from_page");

			if ($user instanceof iUmiObject) {
				permissionsCollection::getInstance()->loginAsUser($user);
				session_commit();
				$this->module->redirect($fromPage ? $fromPage : ($this->module->pre_lang . '/users/auth/'));
			}
		}

		protected function createNewSocialUserAndLogin(array $data) {
			$umiObjects = umiObjectsCollection::getInstance();

			$userType = selector::get('object-type')->name('users', 'user');
			$userId = $umiObjects->addObject($data['login'], $userType->getId());
			$user = $umiObjects->getObject($userId);

			$user->setValue("login", $data['login']);
			$user->setValue("password", $data['password']);
			$user->setValue("e-mail", $data['email']);
			$user->setValue("fname", $data['firstName']);
			$user->setValue("lname", $data['lastName']);
			$user->setValue('loginza', $data['network']);
			$user->setValue("register_date", time());
			$user->setValue("is_activated", '1');
			$user->setValue("activate_code", md5(uniqid(rand(), true)));

			$_SESSION['cms_login'] = $data['login'];
			$_SESSION['cms_pass'] = $data['password'];
			$_SESSION['user_id'] = $userId;
			session_commit();

			$groupId = regedit::getInstance()->getVal("//modules/users/def_group");
			$user->setValue("groups", [$groupId]);

			/** @var data|DataForms $dataModule */
			$dataModule = cmsController::getInstance()->getModule('data');
			$dataModule->saveEditedObject($userId, true);
			$user->commit();

			$fromPage = getRequest("from_page");
			$this->module->redirect($fromPage ? $fromPage : ($this->module->pre_lang . '/users/auth/'));
		}

	}

?>
