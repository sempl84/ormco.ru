<?php
	$classes = [
			'AppointmentService' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentService.php'
			],

			'iAppointmentService' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentService.php'
			],

			'AppointmentServiceGroup' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentServiceGroup.php'
			],

			'iAppointmentServiceGroup' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentServiceGroup.php'
			],

			'AppointmentEmployee' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployee.php'
			],

			'iAppointmentEmployee' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployee.php'
			],

			'AppointmentEmployeeService' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployeeService.php'
			],

			'iAppointmentEmployeeService' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployeeService.php'
			],

			'AppointmentEmployeeSchedule' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployeeSchedule.php'
			],

			'iAppointmentEmployeeSchedule' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployeeSchedule.php'
			],

			'AppointmentOrder' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentOrder.php'
			],

			'iAppointmentOrder' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentOrder.php'
			],

			'AppointmentServicesCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentServicesCollection.php'
			],

			'AppointmentServiceGroupsCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentServiceGroupsCollection.php'
			],

			'AppointmentEmployeesCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesCollection.php'
			],

			'AppointmentEmployeesServicesCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesServicesCollection.php'
			],

			'AppointmentEmployeesSchedulesCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesSchedulesCollection.php'
			],

			'AppointmentOrdersCollection' => [
					SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentOrdersCollection.php'
			],

			'appointmentEmployeesConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesConstantMap.php'
			],

			'appointmentEmployeesSchedulesConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesSchedulesConstantMap.php'
			],

			'appointmentEmployeesServicesConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesServicesConstantMap.php'
			],

			'appointmentOrdersConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentOrdersConstantMap.php'
			],

			'appointmentServiceGroupsConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentServiceGroupsConstantMap.php'
			],

			'appointmentServicesConstantMap' => [
					SYS_KERNEL_PATH . 'subsystems/mappings/appointmentServicesConstantMap.php'
			]
	];
