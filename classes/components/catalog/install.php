<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "catalog";
	$INFO['config'] = "1";
	$INFO['default_method'] = "category";
	$INFO['default_method_admin'] = "tree";
	$INFO['per_page'] = 10;

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/catalog/admin.php";
	$COMPONENTS[] = "./classes/components/catalog/class.php";
	$COMPONENTS[] = "./classes/components/catalog/customAdmin.php";
	$COMPONENTS[] = "./classes/components/catalog/customMacros.php";
	$COMPONENTS[] = "./classes/components/catalog/events.php";
	$COMPONENTS[] = "./classes/components/catalog/handlers.php";
	$COMPONENTS[] = "./classes/components/catalog/i18n.en.php";
	$COMPONENTS[] = "./classes/components/catalog/i18n.php";
	$COMPONENTS[] = "./classes/components/catalog/includes.php";
	$COMPONENTS[] = "./classes/components/catalog/install.php";
	$COMPONENTS[] = "./classes/components/catalog/lang.en.php";
	$COMPONENTS[] = "./classes/components/catalog/lang.php";
	$COMPONENTS[] = "./classes/components/catalog/macros.php";
	$COMPONENTS[] = "./classes/components/catalog/permissions.php";
?>
