<?php
	/**
	 * Класс, содержащий обработчики событий
	 */
	class ForumHandlers {
		/**
		 * @var forum $module
		 */
		public $module;

		/**
		 * Обработчик события изменения активности страницы.
		 * Если страница топик или сообщение форума - запустит
		 * актуализации счетчиков конференции форума.
		 * @param iUmiEventPoint $oEventPoint событие изменения активности страницы
		 * @return bool
		 */
		public function onElementActivity(iUmiEventPoint $oEventPoint) {
			if ($oEventPoint->getMode() !== 'after') {
				return false;
			}

			$o_element = $oEventPoint->getRef('element');

			if (!$o_element instanceof umiHierarchyElement) {
				return false;
			}

			if ($this->module->isTopicOrMessage($o_element)) {
				$this->module->recalcCounts($o_element);
				return true;
			}

			return false;
		}

		/**
		 * Обработчик события удаления страницы.
		 * Если страница топик или сообщение форума - запустит
		 * актуализации счетчиков конференции форума.
		 * @param iUmiEventPoint $oEventPoint событие удаления страницы
		 * @return bool
		 */
		public function onElementRemove(iUmiEventPoint $oEventPoint) {
			if ($oEventPoint->getMode() !== 'after') {
				return false;
			}

			$o_element = $oEventPoint->getRef('element');

			if (!$o_element instanceof umiHierarchyElement) {
				return false;
			}

			if ($this->module->isTopicOrMessage($o_element)) {
				$this->module->recalcCounts($o_element);
				return true;
			}

			return false;
		}

		/**
		 * Обработчик события добавления страницы.
		 * Если страница топик или сообщение форума - запустит
		 * актуализации счетчиков конференции форума.
		 * @param iUmiEventPoint $oEventPoint событие добавления страницы
		 * @return bool
		 */
		public function onElementAppend(iUmiEventPoint $oEventPoint) {
			if ($oEventPoint->getMode() !== 'after') {
				return false;
			}

			$o_element = $oEventPoint->getRef('element');

			if (!$o_element instanceof umiHierarchyElement) {
				return false;
			}

			if ($this->module->isTopicOrMessage($o_element)) {
				$this->module->recalcCounts($o_element);

				$publish_time = new umiDate(strtotime(date('d.m.Y H:i:00')));
				$o_element->setValue("publish_time", $publish_time);
				$o_element->commit();
				return true;
			}

			return false;
		}

		/**
		 * Обработчик события добавления сообщения форума с клиентской части.
		 * Запускает проверку сообщения на предмет содержания спама.
		 * @param iUmiEventPoint $event событие добавления сообщения
		 */
		public function onMessagePost(iUmiEventPoint $event) {
			$messageId = $event->getParam("message_id");
			antiSpamHelper::checkForSpam($messageId);
		}

		/**
		 * Обработчик события добавления сообщения форума с клиентской части.
		 * Отправляет пользователям, подписанным на топик, почтовое уведомление
		 * о новом сообщении
		 * @param iUmiEventPoint $oEvent событие добавления сообщения
		 * @return bool
		 * @throws selectorException
		 */
		public function onDispatchChanges(iUmiEventPoint $oEvent) {
			$sTemplate = "default";

			try {
				list($sTemplateSubject, $sTemplateMessage) = forum::loadTemplatesForMail(
					"forum/mails/" . $sTemplate,
					"mail_subject",
					"mail_message"
				);
			} catch (publicException $e) {
				return false;
			}

			$iTopicId = $oEvent->getParam("topic_id");
			$iMessageId = $oEvent->getParam("message_id");
			$hierarchy = umiHierarchy::getInstance();
			$message = $hierarchy->getElement($iMessageId);

			$sel = new selector('objects');
			$sel->types('object-type')->name("users", "user");
			$sel->where('subscribed_pages')->equals($iTopicId);

			if (!$sel->length()) {
				return false;
			}

			$block_arr = Array();

			$sTemplateSubject = forum::parseTemplateForMail($sTemplateSubject, $block_arr, $iMessageId);
			$umiRegistry = regedit::getInstance();
			$sFromEmail = $umiRegistry->getVal("//settings/email_from");
			$sFromFio = $umiRegistry->getVal("//settings/fio_from");

			$oMail = new umiMail();
			$oMail->setFrom($sFromEmail, $sFromFio);
			$oMail->setSubject($sTemplateSubject);

			/**
			 * @var iUmiObject|iUmiEntinty $oUser
			 */
			foreach ($sel->result() as $oUser) {
				$oMailUser = clone $oMail;
				$sUserMail = $oUser->getValue('e-mail');
				$block_arr['h1'] = $message->getValue('h1');
				$block_arr['message'] = $message->getValue('message');

				$hierarchy->forceAbsolutePath(true);
				$block_arr['unsubscribe_link'] = $hierarchy->getPathById($iTopicId) . "?unsubscribe=" . base64_encode($oUser->getId());
				$sTemplateMessageUser = forum::parseTemplateForMail($sTemplateMessage, $block_arr, $iMessageId);
				$oMailUser->setContent($sTemplateMessageUser);
				$hierarchy->forceAbsolutePath(false);

				if (umiMail::checkEmail($sUserMail)) {
					$sUserFio = $oUser->getValue('lname') . " ". $oUser->getValue('fname') . " " . $oUser->getValue('father_name');
					$oMailUser->addRecipient($sUserMail, $sUserFio);
					$oMailUser->commit();
					$oMailUser->send();
				}
			}

			return true;
		}

		/**
		 * Обработчик события добавления топика форума с клиентской части.
		 * Включает топик форума в выпуск рассылки модуля "Рассылки".
		 * @param iUmiEventPoint $oEvent событие добавления топика форума.
		 * @return bool
		 * @throws coreException
		 */
		public function onAddTopicToDispatch(iUmiEventPoint $oEvent) {
			$iDispatchId = regedit::getInstance()->getVal("//modules/forum/dispatch_id");

			if (!$iDispatchId) {
				return false;
			}

			/**
			 * @var dispatches $dispatches_module
			 */
			$dispatches_module = cmsController::getInstance()->getModule('dispatches');

			if (!$dispatches_module instanceof def_module) {
				return false;
			}

			$iTopicId = (int) $oEvent->getParam('topic_id');
			$oTopicElement = umiHierarchy::getInstance()->getElement($iTopicId);

			if (!$oTopicElement instanceof umiHierarchyElement) {
				return false;
			}

			$sTitle = (string) getRequest('title');
			$sMessage = (string) getRequest('body');

			$umiObjects = umiObjectsCollection::getInstance();
			$iHierarchyTypeId = umiHierarchyTypesCollection::getInstance()->getTypeByName("dispatches", "message")->getId();
			$iMsgTypeId =  umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeId($iHierarchyTypeId);
			$iMsgObjId = $umiObjects->addObject($sTitle, $iMsgTypeId);

			$oMsgObj = $umiObjects->getObject($iMsgObjId);

			if (!$oMsgObj instanceof umiObject) {
				return false;
			}

			$iReleaseId = $dispatches_module->getNewReleaseInstanceId($iDispatchId);

			$oMsgObj->setValue('release_reference', $iReleaseId);
			$oMsgObj->setValue('header', $sTitle);
			$oMsgObj->setValue('body', $sMessage);
			$oMsgObj->commit();

			return true;
		}
	}
?>