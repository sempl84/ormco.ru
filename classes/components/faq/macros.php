<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class FAQMacros {

		/**
		 * @var faq $module
		 */
		public $module;

		/**
		 * Возвращает данные вопроса
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int|string $element_path идентификатор или адрес вопроса
		 * @return mixed
		 */
		public function question($template = "default", $element_path = false) {
			list($template_block) = faq::loadTemplates(
				"faq/" . $template,
				"question"
			);

			$element_id = $this->module->analyzeRequiredPath($element_path);
			$element = umiHierarchy::getInstance()->getElement($element_id);

			$line_arr = Array();

			if ($element instanceof iUmiHierarchyElement) {
				$line_arr['id'] = $element_id;
				$line_arr['text'] = $element->getName();
				$line_arr['alt_name'] = $element->getAltName();
				$line_arr['link'] = umiLinksHelper::getInstance()->getLink($element);
				$line_arr['question'] = nl2br($element->getValue("question"));
				$line_arr['answer'] = ($answer = $element->getValue("answer")) ? nl2br($answer) : nl2br($element->getValue("content"));
			}

			faq::pushEditable("faq", "question", $element_id);
			return faq::parseTemplate($template_block, $line_arr, $element_id);
		}

		/**
		 * Возвращает категории вопросов, дочерние заданной странице
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int|string $element_path идентификатор или адрес родитеской страницы
		 * @param bool|int $limit ограничение на количество выводимых категорий
		 * @param bool $ignore_paging игнорировать пагинацию
		 * @return mixed
		 * @throws selectorException
		 */
		public function project($template = "default", $element_path = false, $limit = false, $ignore_paging = false) {
			list($template_block, $template_block_empty, $template_line) = faq::loadTemplates(
				"faq/" . $template,
				"categories_block",
				"categories_block_empty",
				"categories_block_line"
			);

			$project_id = $this->module->analyzeRequiredPath($element_path);
			$per_page = ($limit) ? $limit : $this->module->per_page;
			$curr_page = (int) getRequest('p');

			if ($ignore_paging) {
				$curr_page = 0;
			}

			$categories = new selector('pages');
			$categories->types('object-type')->name('faq', 'category');
			$categories->where('hierarchy')->page($project_id);
			$categories->option('load-all-props')->value(true);
			$categories->limit($curr_page * $per_page, $per_page);
			$result = $categories->result();
			$total = $categories->length();

			if ($total == 0) {
				return $template_block_empty;
			}

			$block_arr = Array();
			$block_arr['total'] = $total;
			$umiLinksHelper = umiLinksHelper::getInstance();
			$lines = Array();

			foreach ($result as $categories) {
				if (!$categories instanceof umiHierarchyElement) {
					continue;
				}

				$element_id = $categories->getId();
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:name'] = $line_arr['void:text'] = $categories->getName();
				$line_arr['void:alt_name'] = $categories->getAltName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($categories);
				$line_arr['xlink:href'] = "upage://" . $element_id;

				faq::pushEditable("faq", "category", $element_id);
				$lines[] = faq::parseTemplate($template_line, $line_arr, $element_id);
			}

			$block_arr['subnodes:lines'] = $lines;
			$block_arr['per_page'] = $per_page;
			$block_arr['total'] = $total;

			return faq::parseTemplate($template_block, $block_arr, $project_id);
		}

		/**
		 * Возвращает список вопросов, дочерних заданной странице
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int|string $element_path идентификатор или адрес родитеской страницы
		 * @param bool|int $limit ограничение на количество выводимых вопросов
		 * @param bool $ignore_paging игнорировать пагинацию
		 * @param bool $order режим сортировки: true -> ASC, false -> DESC
		 * @param bool $showSpam выводить вопросы, отмеченные как спам
		 * @return mixed
		 * @throws selectorException
		 */
		public function category($template = "default", $element_path = false, $limit = false, $ignore_paging = false, $order = true, $showSpam = false) {
			if (!$template) {
				$template = "default";
			}

			list($template_block, $template_block_empty, $template_line) = faq::loadTemplates(
				"faq/" . $template,
				"questions_block",
				"questions_block_empty",
				"questions_block_line"
			);

			$category_id = $this->module->analyzeRequiredPath($element_path);
			$per_page = ($limit) ? $limit : $this->module->per_page;
			$curr_page = (int) getRequest('p');

			if ($ignore_paging) {
				$curr_page = 0;
			}

			$questions = new selector('pages');
			$questions->types('object-type')->name('faq', 'question');
			$questions->where('hierarchy')->page($category_id);

			if (!$showSpam) {
				$questions->where('is_spam')->notequals(1);
			}

			$questions->option('load-all-props')->value(true);

			if ($order) {
				$questions->order('ord')->asc();
			} else {
				$questions->order('ord')->desc();
			}

			$questions->limit($curr_page * $per_page, $per_page);
			$result = $questions->result();
			$total = $questions->length();

			if ($total == 0) {
				return $template_block_empty;
			}

			$block_arr = Array();
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$lines = Array();
			$umiLinksHelper = umiLinksHelper::getInstance();

			foreach ($result as $question) {
				if (!$question instanceof umiHierarchyElement) {
					continue;
				}

				$element_id = $question->getId();
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:name'] = $line_arr['void:text'] = $question->getName();
				$line_arr['void:alt_name'] = $question->getAltName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($question);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['question'] = nl2br($question->getValue("question"));
				$line_arr['answer'] = ($answer = $question->getValue("answer")) ? nl2br($answer) : nl2br($question->getValue("content"));

				faq::pushEditable("faq", "question", $element_id);
				$lines [] = faq::parseTemplate($template_line, $line_arr, $element_id);
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			return faq::parseTemplate($template_block, $block_arr, $category_id);
		}

		/**
		 * Возвращает список проектов
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int $limit ограничение на количество выводимых проектов
		 * @param bool $ignore_paging игнорировать пагинацию
		 * @return mixed
		 * @throws selectorException
		 */
		public function projects($template = "default", $limit = false, $ignore_paging = false) {
			list($template_block, $template_block_empty, $template_line) = faq::loadTemplates(
				"faq/" . $template,
				"projects_block",
				"projects_block_empty",
				"projects_block_line"
			);

			$per_page = ($limit) ? $limit : $this->module->per_page;
			$curr_page = (int) getRequest('p');

			if ($ignore_paging) {
				$curr_page = 0;
			}

			$projects = new selector('pages');
			$projects->types('object-type')->name('faq', 'project');
			$projects->option('load-all-props')->value(true);
			$projects->limit($curr_page * $per_page, $per_page);
			$result = $projects->result();
			$total = $projects->length();

			if ($total == 0) {
				return $template_block_empty;
			}

			$block_arr = Array();
			$lines = Array();
			$umiLinksHelper = umiLinksHelper::getInstance();

			foreach ($result as $project) {
				if (!$project instanceof umiHierarchyElement) {
					continue;
				}

				$element_id = $project->getId();
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:name'] = $line_arr['void:text'] = $project->getName();
				$line_arr['void:alt_name'] = $project->getAltName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($project);
				$line_arr['xlink:href'] = "upage://" . $element_id;

				faq::pushEditable("faq", "project", $element_id);
				$lines[] = faq::parseTemplate($template_line, $line_arr, $element_id);
			}

			$block_arr['subnodes:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;

			return faq::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Возвращает данные для создания формы добавления вопроса в заданную категорию
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int|string $category_path идентификатор или адрес страницы категории
		 * @return mixed
		 */
		public function addQuestionForm($template = "default", $category_path = false) {
			list($template_add_user, $template_add_guest) = faq::loadTemplates(
				"faq/" . $template,
				"question_add_user",
				"question_add_guest"
			);

			$category_id = $this->module->analyzeRequiredPath($category_path);

			if (permissionsCollection::getInstance()->isAuth()) {
				$template_add = $template_add_user;
			} else {
				$template_add = $template_add_guest;
			}

			$block_arr['action'] = $this->module->pre_lang . "/faq/post_question/" . $category_id . "/";

			return faq::parseTemplate($template_add, $block_arr, $category_id);
		}

		/**
		 * Обрабатывает ошибку.
		 * Совершает редирект на referrer, подставляя
		 * в $_GET параметр идентификатор ошибки.
		 * @param string $errorMessage текст ошибки
		 * @param bool $interrupt прерывать выполнение скрипта
		 */
		public function reportError($errorMessage, $interrupt = true) {
			$this->module->errorNewMessage($errorMessage, (bool) $interrupt);
		}

		/**
		 * Завершает создание вопроса.
		 * Совершает редирект на метод, который создает вопрос.
		 * @param int $element_id идентификатор созданного вопроса.
		 */
		public function finishPosting($element_id) {
			$this->module->redirect($this->module->pre_lang . '/faq/post_question/?posted=' . $element_id);
		}

		/**
		 * Создает вопрос и отправляет админстратору
		 * почтовое уведомление.
		 * @return bool|null
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function post_question() {
			$iPosted= getRequest('posted');
			$session = \UmiCms\Service::Session();

			if ($iPosted) {
				$tickets = (array) $session->get('tickets');
				$sPosted = getArrayKey($tickets, $iPosted);
				return $sPosted;
			}

			$referrer = getServer('HTTP_REFERER');
			$this->module->errorRegisterFailPage($referrer);
			$parent_element_id = (int) getRequest('param0');

			$email = htmlspecialchars(getRequest('email'));
			$nick = htmlspecialchars(getRequest('nick'));
			$title = htmlspecialchars(getRequest('title'));
			$question = htmlspecialchars(getRequest('question'));
			$ip = getServer('REMOTE_ADDR');

			if (!strlen($title)) {
				$this->reportError("%error_faq_required_title%");
			}

			if (!strlen($question)) {
				$this->reportError("%error_faq_required_question%");
			}

			$permissions = permissionsCollection::getInstance();
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$auth = UmiCms\Service::Auth();

			if (!strlen($email)) {
				$user_id = $auth->getUserId();

				if ($user = $umiObjectsCollection->getObject($user_id)) {
					$email = $user->getValue('e-mail');
				}
			}

			$postTime = time();

			if (!umiCaptcha::checkCaptcha()) {
				$this->reportError("%errors_wrong_captcha%");
			}

			$oEventPoint = new umiEventPoint("faq_post_question");
			$oEventPoint->setMode("before");
			$oEventPoint->setParam("parent_element_id", $parent_element_id);
			$oEventPoint->setParam("test_captcha", umiCaptcha::checkCaptcha());
			faq::setEventPoint($oEventPoint);

			$cmsController = cmsController::getInstance();
			/**
			 * @var users $oUsers
			 */
			$oUsers = $cmsController->getModule('users');
			$is_active = false;

			if ($permissions->isAuth()) {
				$user_id = $auth->getUserId();
				$iAuthorId = $oUsers->createAuthorUser($user_id);
				$is_active = $permissions->isSv($user_id);
			} else {
				$iAuthorId = $oUsers->createAuthorGuest($nick, $email, $ip);
			}

			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$object_type_id = $umiObjectTypesCollection->getTypeIdByHierarchyTypeName("faq", "question");
			$umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
			$hierarchy_type_id = $umiHierarchyTypesCollection->getTypeByName("faq", "question")->getId();
			$hierarchy = umiHierarchy::getInstance();

			$parentElement = $hierarchy->getElement($parent_element_id);
			$tpl_id = $parentElement->getTplId();
			$domain_id = $parentElement->getDomainId();
			$lang_id = $parentElement->getLangId();

			$element_id = $hierarchy->addElement(
				$parent_element_id,
				$hierarchy_type_id,
				$title,
				$title,
				$object_type_id,
				$domain_id,
				$lang_id,
				$tpl_id
			);

			$permissions->setDefaultPermissions($element_id);

			$element = $hierarchy->getElement($element_id);
			$element->setIsActive($is_active);
			$element->setIsVisible(false);
			$element->setValue("question", $question);
			$element->setValue("publish_time", $postTime);
			$element->setName($title);
			$element->setValue("h1", $title);
			$element->setValue("author_id", $iAuthorId);
			$element->commit();

			$umiRegistry = regedit::getInstance();
			$from = $umiRegistry->getVal("//settings/fio_from");
			$from_email = $umiRegistry->getVal("//settings/email_from");
			$admin_email = $umiRegistry->getVal("//settings/admin_email");

			list(
				$confirm_mail_subj_user, $confirm_mail_user, $confirm_mail_subj_admin, $confirm_mail_admin
				) = faq::loadTemplatesForMail(
				"faq/default",
				"confirm_mail_subj_user",
				"confirm_mail_user",
				"confirm_mail_subj_admin",
				"confirm_mail_admin"
			);

			$mail_arr = Array();
			$mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
			$mail_arr['question'] = $question;
			$protocol = getSelectedServerProtocol();
			$mail_arr['question_link'] = $protocol . "://" . $domain . $this->module->pre_lang. "/admin/faq/edit/" . $element_id . "/";
			$mail_adm_subj = faq::parseTemplateForMail($confirm_mail_subj_admin, $mail_arr);
			$mail_adm_content = faq::parseTemplateForMail($confirm_mail_admin, $mail_arr);

			$confirmAdminMail = new umiMail();
			$confirmAdminMail->addRecipient($admin_email);
			$confirmAdminMail->setFrom($email, $nick);
			$confirmAdminMail->setSubject($mail_adm_subj);
			$confirmAdminMail->setContent($mail_adm_content);
			$confirmAdminMail->commit();
			$confirmAdminMail->send();

			if (!$umiRegistry->getVal("//modules/faq/disable_new_question_notification")) {
				$user_mail = Array();
				$user_mail['domain'] = $domain = $_SERVER['HTTP_HOST'];
				$user_mail['question'] = $question;
				$user_mail['ticket'] = $element_id;
				$mail_usr_subj = faq::parseTemplateForMail($confirm_mail_subj_user, $user_mail);
				$mail_usr_content = faq::parseTemplateForMail($confirm_mail_user, $user_mail);

				$confirmMail = new umiMail();
				$confirmMail->addRecipient($email);
				$confirmMail->setFrom($from_email, $from);
				$confirmMail->setSubject($mail_usr_subj);
				$confirmMail->setContent($mail_usr_content);
				$confirmMail->commit();
				$confirmMail->send();

				$tickets = $session->get('tickets');
				$tickets = (is_array($tickets)) ? $tickets : [];
				$tickets[$element_id] = $mail_usr_content;
				$session->set('tickets', $tickets);
			}

			$oEventPoint = new umiEventPoint("faq_post_question");
			$oEventPoint->setMode("after");
			$oEventPoint->setParam("element_id", $element_id);
			faq::setEventPoint($oEventPoint);

			$this->finishPosting($element_id);
		}
	}
