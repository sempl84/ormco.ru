<?php
	class payanywayatolPayment extends payment {
		/**
		 * Статусные коды PayAnyway (см. документацию ОПИСАНИЕ MONETA.ASSISTANT)
		 */
		// Заказ оплачен. Уведомление об оплате магазину доставлено
		const ready        = 200;
		const delivery	   = 200;
		// Заказ находится в обработке. Точный статус оплаты заказа определить невозможно.
		const editing	   = 302;
		const waiting	   = 302;
		// Заказ создан и готов к оплате. Уведомление об оплате магазину не доставлено.
		const payment	   = 402;
		const accepted	   = 402;
		// Заказ не является актуальным в магазине (например, заказ отменен).
		const canceled	   = 500;
		const rejected	   = 500;

		public function validate() { return true; }

		public static function getOrderId() {
			return (int) getRequest('MNT_TRANSACTION_ID');
		}

		public function process($template = null) {
			$this->order->order();
			$currency    = strtoupper( mainConfiguration::getInstance()->get('system', 'default-currency') );
			if ($currency == 'RUR'){
				$currency = 'RUB';
			}
			$amount				= number_format($this->order->getActualPrice(), 2, '.', '');
			$orderId			= $this->order->getId() . '.' . time();
			$merchantId			= $this->object->mnt_id;
			$dataIntegrityCode	= $this->object->mnt_data_integrity_code;
			$successUrl			= $this->object->mnt_success_url;
			$failUrl			= $this->object->mnt_fail_url;
			$testMode			= $this->object->mnt_test_mode;
			$systemUrl			= $this->object->mnt_system_url;
			if (empty($testMode)){
				$testMode = 0;
			}
			$signature	 = md5("{$merchantId}{$orderId}{$amount}{$currency}{$testMode}{$dataIntegrityCode}");
			$param = array();
			$param['formAction'] 		= "https://{$systemUrl}/assistant.htm";
			$param["orderId"] = $this->order->id;
			$param['mntId'] 			= $merchantId;
			$param['mnTransactionId']   = $orderId;
			$param['mntCurrencyCode'] 	= $currency;
			$param['mntAmount'] 	 	= $amount;
			$param['mntTestMode'] 	 	= $testMode;
			$param['mntSignature'] 		= $signature;
			$param['mntSuccessUrl'] 	= $successUrl;
			$param['mntFailUrl'] 	 	= $failUrl;
			$param['mntAtolInfo'] 	 	= self::atolInfo($this->order->id);
			
			$objects = umiObjectsCollection::getInstance();
      		$order = order::get($this->order->id);
			
			$customer_id = $order->getCustomerId();
      		$customer = $objects->getObject($customer_id);
      		$customerEmail = $customer->getValue('e-mail');
      		if(!$customerEmail)$customerEmail = $customer->getValue('email');
			
			$customerFIO = $customer->getValue('lname').' '.$customer->getValue('fname').' '.$customer->getValue('father_name');
			
			$param['MNT_CUSTOM3'] 	 	= $customerEmail;
			$param['MNT_DESCRIPTION'] 	 	= $customerFIO . '. Заказ №' . ($this->order->number); //self::atolInfoItems($this->order->id);;
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', 'start: ' . print_r($this->order->id) ."\n". date('H:i:s') ."\n", FILE_APPEND);
			
			$this->order->setPaymentStatus('initialized');
			list($templateString) = def_module::loadTemplates("emarket/payment/payanyway/".$template, "form_block");
			return def_module::parseTemplate($templateString, $param);
		}

		public function poll() {
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "POLL \n". date('H:i:s') ."\n", FILE_APPEND);
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "POLL DATA \n".print_r($_REQUEST,true) ."\n", FILE_APPEND);

			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType('text/xml');
			$responseCode = payanywayatolPayment::canceled;

			if (!is_null(getRequest('MNT_ID')) && !is_null(getRequest('MNT_TRANSACTION_ID')) && !is_null(getRequest('MNT_AMOUNT')) && !is_null(getRequest('MNT_CURRENCY_CODE')) && !is_null(getRequest('MNT_TEST_MODE')) && !is_null(getRequest('MNT_SIGNATURE'))) {
				if ($this->checkSignature()){
					$amount = (float) getRequest('MNT_AMOUNT');
					$orderActualPrice = (float) $this->order->getActualPrice();
					if ( (getRequest('MNT_COMMAND') === null or getRequest('MNT_COMMAND') != 'CHECK') && ($orderActualPrice == $amount) ) {
						$this->order->setPaymentStatus('accepted');
						$responseCode = payanywayatolPayment::ready;
						// send ATOL
						/*
						$orderId = $this->order->getId();
						$orderObj = umiObjectsCollection::getInstance()->getObject($orderId);
				        if ($orderObj->uuid_atol == ''){
				        	//self::atolSendDocument($this->order->getId());
				        }
						*/


					} else {
						$statusCode = $this->order->getCodeByStatus($this->order->getOrderStatus());
						$responseCode = constant('payanywayatolPayment::'.$statusCode);
					}
				}
			}
			$buffer->push($this->getResponse($responseCode));
			$buffer->end();
		}

		public function checkSignature() {
			$params = '';
			//if (getRequest('MNT_COMMAND')) $params .= getRequest('MNT_COMMAND');
			$params .= getRequest('MNT_ID');
			$params .= getRequest('MNT_TRANSACTION_ID');
			//if (getRequest('MNT_OPERATION_ID'))
			$params .= getRequest('MNT_OPERATION_ID');
			//if (getRequest('MNT_AMOUNT'))
			$params .= getRequest('MNT_AMOUNT');
			$params .= getRequest('MNT_CURRENCY_CODE');
			$params .= getRequest('MNT_SUBSCRIBER_ID');
			$params .= getRequest('MNT_TEST_MODE');
			$params .= $this->object->mnt_data_integrity_code;
			$signature = md5($params);
			$c_s = getRequest('MNT_SIGNATURE');
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', print_r($signature,true) ."\n". date('H:i:s') ."\n", FILE_APPEND);
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', print_r($c_s,true) ."\n". date('H:i:s') ."\n", FILE_APPEND);
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', strcasecmp($signature, $c_s ) ."\n". date('H:i:s') ."\n", FILE_APPEND);

			if(strcasecmp($signature, $c_s ) == 0) {
				return true;
			}

			// [MNT_COMMAND] + MNT_ID + MNT_TRANSACTION_ID + [MNT_OPERATION_ID] + [MNT_AMOUNT] + MNT_CURRENCY_CODE +                     MNT_TEST_MODE + mnt_data_integrity_code
			//                 MNT_ID + MNT_TRANSACTION_ID +  MNT_OPERATION_ID  +  MNT_AMOUNT  + MNT_CURRENCY_CODE + MNT_SUBSCRIBER_ID + MNT_TEST_MODE + КОД ПРОВЕРКИ ЦЕЛОСТНОСТИ ДАННЫХ)
			return false;
		}

		public function getResponse($resultCode) {
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "getResponse \n". date('H:i:s') ."\n", FILE_APPEND);

			$signature = md5($resultCode . getRequest('MNT_ID') . getRequest('MNT_TRANSACTION_ID') . $this->object->mnt_data_integrity_code);
			$result = '<?xml version="1.0" encoding="UTF-8" ?>';
			$result .= '<MNT_RESPONSE>';
			$result .= '<MNT_ID>' . getRequest('MNT_ID') . '</MNT_ID>';
			$result .= '<MNT_TRANSACTION_ID>' . getRequest('MNT_TRANSACTION_ID') . '</MNT_TRANSACTION_ID>';
			$result .= '<MNT_RESULT_CODE>' . $resultCode . '</MNT_RESULT_CODE>';
			//$result .= '<MNT_DESCRIPTION>' . getRequest('MNT_ID') . '</MNT_DESCRIPTION>';
			//$result .= '<MNT_AMOUNT>' . getRequest('MNT_AMOUNT') . '</MNT_AMOUNT>';
			$result .= '<MNT_SIGNATURE>' . $signature . '</MNT_SIGNATURE>';
			
			
			$result .= '</MNT_RESPONSE>';
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "getResponse:xml \n".print_r($result,true) ."\n". date('H:i:s') ."\n", FILE_APPEND);

			return $result;

			/*+<?xml version="1.0" encoding="UTF-8"?>
			+<MNT_RESPONSE>
			 +<MNT_ID></MNT_ID>
			 +<MNT_TRANSACTION_ID></MNT_TRANSACTION_ID>
			 +<MNT_RESULT_CODE></MNT_RESULT_CODE>
			 <MNT_DESCRIPTION></MNT_DESCRIPTION>
			 <MNT_AMOUNT></MNT_AMOUNT>
			 +<MNT_SIGNATURE></MNT_SIGNATURE>
			 <MNT_ATTRIBUTES>
			 <ATTRIBUTE>
			 <KEY></KEY>
			 <VALUE></VALUE>
			 </ATTRIBUTE>
			 </MNT_ATTRIBUTES>
			+</MNT_RESPONSE>*/
		}


		 /*{
		 	"uuid": "c389b890-97ed-40f8-826b-c85cc1fec166",
		 	"error": {
		 		"code": 8,
		 		"text": "Ошибка валидации входящего чека с GUID \"c389b890-97ed-40f8-826b-c85cc1fec166\": StringExpected: #/external_id; ArrayItemNotValid: #/receipt.items[0]\n{\n  NotAnyOf: #/receipt.items[0]\n  {\n    PropertyRequired: #/receipt.items[0].tax\n  }\n  {\n    PropertyRequired: #/receipt.items[0].barcode\n  }\n  {\n    PropertyRequired: #/receipt.items[0].text\n  }\n  \n}\n",
		 		"type": "system"
			},
			"status":
			"fail",
			"payload": null,
			"timestamp":
			"14.07.2017 12:22:37"}
		 */
		public function atolGetToken() {
			//https://online.atol.ru/possystem/v3/getToken?login=%3Clogin%3E&pass=%3Cpass%3E
			$login = 'orthodontia-ru';
			$pass = '3*cVJliH';
			$kassaApiUrl = 'https://online.atol.ru/possystem';
			$kassaApiVersion = 'v3';
			$method = 'getToken';

			$url = $kassaApiUrl . "/" . $kassaApiVersion . "/";

			$data = array("login" => $login, "pass" => $pass);

			$result = self::sendHttpRequest($url, $method, $data);
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | atolGetToken result:\n" . print_r($result, true) . "\n", FILE_APPEND);

			$result = @json_decode($result, true);

			//$er_text = isset($result['text'])? $result['text'] : 'getToken error';
	        //return (isset($result['token']) && $result['token']!='') ? $result['token'] : $er_text;
	        return (isset($result['token']) && $result['token']!='') ? $result['token'] : false;


		}

		//public function atolSendDocument(order $order){
		public function atolSendDocument($orderId = NULL){
	        if(!$orderId){
	        	file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | no orderId :\n", FILE_APPEND);
	        	//return false;
	        	return ;
	        }
	        $tokenid = self::atolGetToken();
	        if (!$tokenid) {
	        	file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | atolGetToken false:\n", FILE_APPEND);
	            //return false;
	            return ;
	        }


			$objects = umiObjectsCollection::getInstance();
			$kassaApiUrl = 'https://online.atol.ru/possystem';
			$kassaApiVersion = 'v3';
			$groupCode = 'orthodontia-ru_579';
	        $url = $kassaApiUrl . "/" . $kassaApiVersion . "/";
	        $method = $groupCode . "/sell";

			$inn = '7806182285';
			$callback_url = '';
			$paymentAddress = 'Москва, Волгоградский проспект, 42, к. 9';

			$sno = 'osn';
	/*
	«osn» – общая СН;
	«usn_income» – упрощенная СН (доходы);
	«usn_income_outcome» – упрощенная СН (доходы минус расходы);
	«envd» – единый налог на вмененныйдоход;
	«esn» – единый сельскохозяйственныйналог;
	«patent» – патентная СН.
	*/
			$paymentType = 1;
			/*
	«1» – электронными;
	«2» – предварительная оплата (аванс);
	«3» – последующая оплата (кредит);
	«4» – иная форма оплаты (встречное предоставление);
	«5» – «9» – расширенные типыоплаты. Для каждого фискального типа оплаты можно указать расширенный тип оплаты.
			*/

			$tax = 'vat18';
			$taxMultiplier = 18;
			$itemsTotalSum = '';


	        $order = order::get($orderId);
	        //$orderId = $order->getId();

			//$customerEmail
			$customer_id = $order->getCustomerId();
	        $customer = $objects->getObject($customer_id);
	        $customerEmail = $customer->getValue('e-mail');
	        if(!$customerEmail)$customerEmail = $customer->getValue('email');

			$customerPhone = '';//$customer->getValue('phone');


	        $orderItems = $order->getItems();
	        $discount = 1;
	        if($order->getActualPrice() - $order->getDeliveryPrice() != $order->getOriginalPrice()){
	            $discount = ($order->getActualPrice() - $order->getDeliveryPrice()) / $order->getOriginalPrice();
	        }
	        foreach ($orderItems as $item){
	            $element = $item->getItemElement();

				$name =$item->getName();
				$price = (double) number_format($item->getTotalActualPrice() / $item->getAmount() * $discount, 2, '.', '');
				$quantity = (double) $item->getAmount();
				$vat = $tax;
				$sum = (double)$quantity * $price;
		        //$tax_sum = (double) round($sum * $taxMultiplier, 2);
		        $tax_sum = (double) round(round((($sum * $taxMultiplier) / (100 + $taxMultiplier)) * 100) / 100 , 2);

				$itemsTotalSum += $sum;
	            $items[] = array(
	            	'name'=>$name,
	            	'price'=>$price,
	            	'quantity'=>$quantity,
	            	'tax'=>$vat,
	            	'sum'=>$sum,
	            	'tax_sum'=>$tax_sum,
				);
	        }
	        if($order->getDeliveryPrice()){
	            $deliveryPrice = (double) number_format($order->getDeliveryPrice(), 2, '.', '');
	            $items[] = array(
	            	'name'=>'Доставка',
	            	'price'=>$deliveryPrice,
	            	'quantity'=>1,
	            	'vat'=>'none',
	            	'sum'=>$deliveryPrice,
	            	'tax_sum'=> '',
				);

	        }


	        $data = array(
	            'timestamp' => date('d.m.Y H:i:s'),
	            'external_id' => $orderId,
	            'service' => array(
	                'inn' => $inn,
	                'callback_url' => $callback_url,
	                'payment_address' => $paymentAddress,
	            ),
	            'receipt' => array(
	                'items' => $items,
	                'total' => $itemsTotalSum,
	                'payments' => array(
	                    array(
	                        'sum' => $itemsTotalSum,
	                        'type' => $paymentType,
	                    ),
	                ),
	                'attributes' => array(
	                    'sno' => $sno,
	                    'email' => $customerEmail,
	                    'phone' => $customerPhone,
	                ),
	            ),
	        );




			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | send Datas:\n" . print_r($data, true) . "\n", FILE_APPEND);

			$respond = $this->sendHttpRequest($url, $method, $data, $tokenid);
			file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | result send Datas:\n" . print_r($respond, true) . "\n", FILE_APPEND);

	        $result = false;
	        // пример ответа
	        // {"uuid":"ea5991ab-05f3-4c10-980a-3b3f3d58ed13","timestamp":"18.05.2017 16:33:23","status":"wait","error":null}
	        if ($respond) {
	            $respondArray = @json_decode($respond, true);
	            if (is_array($respondArray) && count($respondArray)) {
	                foreach ($respondArray AS $respondItemKey => $respondItemValue) {
	                    if ($respondItemKey == 'error' && (!$respondItemValue || $respondItemValue == 'null')) {
	                        $result = true;
	                    }
	                }
	            }
				$orderObj = $objects->getObject($orderId);
		        if (isset($respondArray['error']['text']) && $respondArray['error']['text'] !='') {
		            $orderObj->setValue('payment_error_atol', $respondArray['error']['text']);
		        } else {
		            $orderObj->setValue('uuid_atol', $respondArray['uuid']);
		        }
				$orderObj->commit();

	        }

	        //return $result;
	        return ;
	    }

		private function sendHttpRequest($url, $method, $data, $tokenid = null){
	        // запрос надо сделать через curl
	        $jsonData = json_encode($data);


	        $operationUrl = $url . $method;
	        if ($tokenid) {
	            $operationUrl .= "?tokenid=" . $tokenid;
	        }
			//var_dump($operationUrl);
			//var_dump($jsonData);
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $operationUrl);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Content-Type: application/json',
	                'Content-Length: ' . strlen($jsonData))
	        );

	        $result = curl_exec($ch);

	        $res = curl_exec($ch);
	        if ($res === false) {
	            file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | sendHttpRequest atolonline Response error:\n" . var_export(curl_error($ch), true) . "\n", FILE_APPEND);
			}else {
	        	file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpawatol.txt', date('H:i:s')." | sendHttpRequest atolonline Response:\n" . print_r($result,true) . "\n", FILE_APPEND);
			}

	        curl_close($ch);
	        return $result;


		}

		public function atolInfo($orderId = NULL){
	        if(!$orderId){
	        	return ;
	        }

			$objects = umiObjectsCollection::getInstance();

			$tax = "1105";//'НДС 0%';
			
			/*
- 1104 - НДС 0%
- 1103 - НДС 10%
- 1102 - НДС 18% (c 01.01.2019 ставка 20%)
- 1105 - НДС не облагается
- 1107 - НДС с рассч. ставкой 10%
- 1106 - НДС с рассч. ставкой 18% (c 01.01.2019 - 20%)
			*/
			$taxMultiplier = 0;
			$itemsTotalSum = '';

	        $order = order::get($orderId);
	        $orderObj = $objects->getObject($orderId);

			//$customerEmail
			$customer_id = $order->getCustomerId();
	        $customer = $objects->getObject($customer_id);
	        $customerEmail = $customer->getValue('e-mail');
	        if(!$customerEmail)$customerEmail = $customer->getValue('email');

	        $orderItems = $order->getItems();
	        $discount = 1;
	        if($order->getActualPrice() - $order->getDeliveryPrice() != $order->getOriginalPrice()){
	            $discount = ($order->getActualPrice() - $order->getDeliveryPrice()) / $order->getOriginalPrice();
	        }
	        foreach ($orderItems as $item){
	            $element = $item->getItemElement();

				$name =$item->getName();
				$price = (double) number_format($item->getTotalActualPrice() / $item->getAmount() * $discount, 2, '.', '');
				$quantity = (double) $item->getAmount();
				
				$vat = $tax; // дефолтное значение НДС в 0%
				$nds_id = $element->nds;
				if($nds_id){
					$nds_obj = $objects->getObject($nds_id);
					if($nds_obj){
						$element_vat = $nds_obj->vat;
						if (!isset($element_vat) || $element_vat !== '') {
						  $vat = $element_vat;  // значение НДС из справочника
						  $taxMultiplier = (int) $nds_obj->name;
						  if($taxMultiplier == 18) $taxMultiplier = 20;
						}
						
					}
				}
				
				$sum = (double)$quantity * $price;
		        //$tax_sum = (double) round($sum * $taxMultiplier, 2);
		        $tax_sum = (double) round(round((($sum * $taxMultiplier) / (100 + $taxMultiplier)) * 100) / 100 , 2);

				$itemsTotalSum += $sum;
	            $items[] = array(
	            	'n'=>$name,
	            	'p'=>$price,
	            	'q'=>$quantity,
	            	't'=>$vat
				);
	        }
	        // if($order->getDeliveryPrice()){
	            // $deliveryPrice = (double) number_format($order->getDeliveryPrice(), 2, '.', '');
	            // $items[] = array(
	            	// 'n'=>'Доставка',
	            	// 'p'=>$deliveryPrice,
	            	// 'q'=>1,
	            	// 't'=>'none',
	            	// 'sum'=>$deliveryPrice,
	            	// 'tax_sum'=> '',
				// );
//
	        // };

			$result = array(
				"customer" => $customerEmail,
				"items" => $items,
				"fio" => $orderObj->order_lname .' '. $orderObj->order_fname .' '. $orderObj->order_father_name
			);

				/*
			array(
"customer" => "test@test.ru",
"items" =>
array(
array("n" => "product 1 name", "p" => "product 1 price", "q" => "product 1
quantity", "t" => "vatType"),
array("n" => "product 2 name", "p" => "product 2 price", "q" => "product 2
quantity", "t" => "vatType"),
),
)
			*/
	        return $jsonData = json_encode($result);
	    }


public function atolInfoItems($orderId = NULL){
	        if(!$orderId){
	        	return ;
	        }


	        $order = order::get($orderId);


	        $orderItems = $order->getItems();
	        foreach ($orderItems as $item){
						$name =$item->getName();
				
	            $items[] = $name;
	        }
	  
				
	        return implode(',', $items);
	    }




	};
?>
