<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$i18n = array(
    'header-emarket-listImportDiscountsLog' => 'Список логов импорта скидок',
    'header-emarket-viewImportDiscountsLog' => 'Просмотр лога импорта скидок',
);