<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class admin_emarket_import_discounts extends def_module
{
    public function listImportDiscountsLog()
    {
        $dir = $this->_getImportDiscountsLogDir(false);
        if (!is_dir($dir)) {
            throw new publicException('Не найдена директория ' . $dir);
        }
        
        $items = array();
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile() || $file->getExtension() != 'log') {
                continue;
            }
            
            $id = pathinfo($file->getFilename(), PATHINFO_FILENAME);
            $items[$id] = array(
                'attribute:id' => $id,
                'node:date' => date('d.m.Y H:i:s', $id)
            );
        }
        
        if (!$items) {
            throw new publicException('Не найдены логи');
        }
        
        krsort($items);
        
        $this->setDataType('settings');
        $this->setActionType('view');
        $this->setData(array('items' => array('nodes:item' => $items)));
        $this->doData();
        
        return null;
    }
    
    public function viewImportDiscountsLog()
    {
        $id = trim(getRequest('param0'));
        if (!$id) {
            throw new publicException('Не передан id лога');
        }
        
        $dir = $this->_getImportDiscountsLogDir(false);
        if (!is_dir($dir)) {
            throw new publicException('Не найдена директория ' . $dir);
        }
        
        $filePath = $dir . '/' . $id . '.log';
        if (!file_exists($filePath)) {
            throw new publicException('Не найден файл ' . $filePath);
        }
        
        $data = array(
            'attribute:id' => $id,
            'log' => nl2br(file_get_contents($filePath))
        );
        
        $arCustomFileNames = array(
            emarket_import_discounts::custom_log_file_input => 'Запрос',
            emarket_import_discounts::custom_log_file_response => 'Ответ'
        );
    
        $files = array();
    
        foreach ($arCustomFileNames as $customFileName => $title) {
            $customFilePath = $this->_getImportDiscountsLogCustomFilePath($id, $customFileName);
            if (!file_exists($customFilePath)) {
                continue;
            }
            
            $files[] = array(
                'attribute:name' => $customFileName,
                'attribute:title' => $title,
                'node:path' => $customFilePath
            );
        }
        
        if(count($files)) {
            $data['files'] = array('nodes:file' => $files);
        }
        
        $this->setDataType('settings');
        $this->setActionType('view');
        $this->setData($data);
        $this->doData();
        
        return null;
    }
    
    public function viewImportDiscountsLogCustomFile()
    {
        $id = trim(getRequest('param0'));
        if (!$id) {
            throw new publicException('Не передан id лога');
        }
        
        $fileName = trim(getRequest('param1'));
        if(!$fileName) {
            throw new publicException('Не передано имя файла');
        }
        
        $dir = $this->_getImportDiscountsLogDir(false);
        if (!is_dir($dir)) {
            throw new publicException('Не найдена директория ' . $dir);
        }
        
        $filePath = $this->_getImportDiscountsLogCustomFilePath($id, $fileName);
        if (!file_exists($filePath)) {
            throw new publicException('Не найден файл ' . $filePath);
        }
        
        $buffer = outputBuffer::current();
        if(!$buffer instanceof HTTPOutputBuffer) {
            throw new publicException('Неверный буфер вывода');
        }
        $buffer->clear();
        $buffer->contentType('application/json');
        $buffer->option('generation-time', false);
        $buffer->push(file_get_contents($filePath));
        $buffer->end();
    }
}