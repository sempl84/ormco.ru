<?php
abstract class __emarket_custom_admin {
    //TODO: Write here your own macroses (admin mode)
//    public function partialRecalc() {
//        if (isDemoMode()) {
//            return false;
//        }
//
//        $this->setDataType("settings");
//        $this->setActionType("view");
//
//        $page = (int) getRequest("page");
//
//        $emarketTop = new emarketTop();
//        if ($page == 0) {
//            $emarketTop->clearTableTop();
//            regedit::getInstance()->setVal('//modules/emarket/last-reindex-result', false);
//            regedit::getInstance()->setVal('//modules/emarket/last-reindex-date', date('Y-m-d'));
//        }
//        $config = mainConfiguration::getInstance();
//        $total = (int) $this->allOrdersRecalculate();
//        $limit = $config->get('modules', 'emarket.reindex.max-items');
//        if ($limit == 0) {
//            $limit = 5;
//        }
//        $result = $this->recalculation($limit, $page);
//
//        if ($result['current'] >= $total) {
//            regedit::getInstance()->setVal('//modules/emarket/last-reindex-result', true);
//        }
//
//        $data = Array(
//            'index-items' => Array(
//                'attribute:current' => $result['current'],
//                'attribute:total' => $total,
//                'attribute:page' => $result['page']
//            )
//        );
//
//        $this->setData($data);
//        return $this->doData();
//    }
//
//    public function allOrdersRecalculate() {
//        $sel = new selector('objects');
//        $sel->types('object-type')->name('emarket', 'order');
//        $sel->where('status_id')->isnotnull(true);
//        $sel->where('order_date')->less(strtotime(date('Y-m-d')));
//        return $sel->length;
//    }
//
//    public function recalculation($limit, $page) {
//        $emarketTop = new emarketTop();
//        $offset = $page * $limit;
//
//        $sel = new selector('objects');
//        $sel->types('object-type')->name('emarket', 'order');
//        $sel->where('status_id')->isnotnull(true);
//        $sel->where('order_date')->less(strtotime(date('Y-m-d')));
//        $sel->limit($offset, $limit);
//
//        foreach ($sel->result as $order) {
//            $emarketTop->addOrder($order);
//        }
//
//        return array(
//            'page' => $page + 1,
//            'current' => $offset + $limit
//        );
//    }

    public function realpayments_custom() {
        $fields_filter = getRequest('fields_filter');
        $fromDate = '';
        $toDate = '';
        if (!empty($fields_filter) && array_key_exists('order_date', $fields_filter)) {
            $string = $fields_filter['order_date']['gt'];
            $pattern = '/\./';
            $varDаte = preg_split($pattern, $string);
            $fromDate = mktime(0, 0, 0, $varDаte[1], $varDаte[0], $varDаte[2]);

            $string = $fields_filter['order_date']['lt'];
            $pattern = '/\./';
            $varDаte = preg_split($pattern, $string);
            $toDate = mktime(0, 0, 0, $varDаte[1], $varDаte[0], $varDаte[2]);
        }
        $range = $this->getDateRange($fromDate, $toDate);

        if ($this->ifNotXmlMode()) {
            $data = array(
                '@fromDate' => $range['fromDate'],
                '@toDate' => $range['toDate'],
            );
            $this->setData($data);
            return $this->doData();
        }

        $limit = getRequest('per_page_limit');
        $curr_page = (int) getRequest('p');
        $offset = $limit * $curr_page;


        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isnotnull(true);
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
        $sel->where('name')->notequals('dummy');
        $sel->limit($offset, $limit);
        if (!getRequest('order_filter')) {
            $sel->order('order_date')->desc();
        }
        selectorHelper::detectFilters($sel);

        $domains = getRequest('domain_id');
        if (is_array($domains) && sizeof($domains)) {
            $domainsCollection = domainsCollection::getInstance();
            if (sizeof($domainsCollection->getList()) > 1) {
                $sel->where('domain_id')->equals($domains[0]);
            }
        }

        $this->setDataRange($limit, $offset);
        $data = $this->prepareData($sel->result, "objects");
        $data = array_merge($data, array(
            '@fromDate' => $range['fromDate'],
            '@toDate' => $range['toDate'],
        ));
        $this->setData($data, $sel->length);

        return $this->doData();
    }
}