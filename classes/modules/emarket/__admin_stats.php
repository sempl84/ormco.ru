<?php
abstract class __emarket_admin_stats extends baseModuleAdmin {
    protected static $allowedStats = array(
        'RegUsers', 'OrderAll', 'OrderStatusNull', 'OrderPayment', 'OrderCanceled', 'OrderReady', 'SumAll', 'SumMiddle', 'AddItems',
        'OrderAllFull',
        'OrderStatusNullFull',
        'OrderPaymentFull',
        'OrderCanceledFull',
        'OrderReadyFull',
        'OrderToPayFull',
    );

    public function stats() {
        $this->setDataType("list");
        $this->setActionType("view");
        $range = $this->getDateRange(getRequest('fromDate'), getRequest('toDate'));

        $params = array(
            "stats" => array(),
            "popular" => array(
                '@max-popular' => mainConfiguration::getInstance()->get('modules', 'emarket.popular.max-items'),
                '@currency' => mainConfiguration::getInstance()->get('system', 'default-currency'),
            )
        );
        foreach (self::$allowedStats as $stat) {
            $params['stats'][$stat . ":stat-" . $stat] = NULL;
        }

        $data = $this->prepareData($params, "settings");
        $data = array_merge($data, array(
            '@fromDate' => $range['fromDate'],
            '@toDate' => $range['toDate'],
        ));

        $this->setData($data);
        return $this->doData();
    }

    /**
     * Запускает сбор статистики
     */
    public function statRun() {
        $statName = getRequest('param0');
        $range = $this->getDateRange(getRequest('param1'), getRequest('param2'));

        if (array_search($statName, self::$allowedStats) !== FALSE) {
            $statName = 'test' . $statName;
        } else {
            throw new coreException("Selected stat not allowed");
        }
        $buffer = outputBuffer::current();
        $buffer->option('generation-time', false);
        $buffer->clear();
        $buffer->push(
                json_encode(array(
            'result' => $this->$statName($range, getRequest('param3'))
                        )
                )
        );
        $buffer->end();
    }

    // AGH - переделан из-за слишком долгого времени выполнения запроса
    public function testRegUsers($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        if ($all != 'all') {
            $sel->where('register_date')->between($range['fromDate'], $range['toDate']);
        }

        $query = $sel->query();
        $sql = 'SELECT COUNT(id) as `ct` FROM cms3_objects WHERE `id` IN (SELECT o.id ' . substr($query, strpos($query, 'FROM')) . ')';
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        $total = 0;
        foreach ($result as $row) {
            $total = $row['ct'];
        }

        return array(
            'status' => true,
            'value' => $total,
        );
    }

    public function testOrderStatusNull($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isnull(true);
        $sel->where('order_items')->isnotnull();
        if ($all != 'all') {
            $sel->where('order_create_date')->between($range['fromDate'], $range['toDate']);
        }
        $sel->group('id');

        $query = $sel->query();
        $sql = 'SELECT COUNT(id) as `ct` FROM cms3_objects WHERE `id` IN (SELECT o.id ' . substr($query, strpos($query, 'FROM')) . ')';
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        $total = 0;
        foreach ($result as $row) {
            $total = $row['ct'];
        }

        return array(
            'status' => true,
            'value' => $total,
        );
    }

    // AGH - переделан под новые статусы
    public function testOrderPayment($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//            $sel->where('status_id')->equals(order::getStatusByCode('payment'));
//        $sel->option('or-mode')->field('payment_status_id','status_payment_1c'); // OR-MODE работает некорректно, поэтому потребовалась инъекция с преобразованием SQL-зпроса
        $sel->where('payment_status_id')->equals(order::getStatusByCode('accepted', 'order_payment_status'));
        $sel->where('status_payment_1c')->equals(1595921); // оплачен - статус из 1С
        $sel->group('id');
        if ($all != 'all') {
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
            $query = str_replace(" AND oc_669_lj.rel_val = '1595921' AND ", " OR oc_669_lj.rel_val = '1595921') AND (", $sel->query());
        }else{
            $query = str_replace(' AND oc_669_lj.rel_val', ' OR oc_669_lj.rel_val', $sel->query());
        }

        $sql = 'SELECT COUNT(id) as `ct` FROM cms3_objects WHERE `id` IN (SELECT o.id ' . substr($query, strpos($query, 'FROM')) . ')';
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        $total = 0;
        foreach ($result as $row) {
            $total = $row['ct'];
        }

        return array(
            'status' => true,
            'value' => $total,
        );
    }

    // TODO: AGH - переделать под новые статусы
    public function testOrderCanceled($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->field('status_id','status_1c');
        $sel->where('status_id')->equals(order::getStatusByCode('canceled'));
//        $sel->where('status_1c')->equals(111111);// тут должен быть какой-то статус отмены заказа из 1С
        if ($all != 'all') {
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
        }
        $sel->group('id');
        return array(
            'status' => true,
            'value' => $sel->length,
        );
    }

    // AGH - переделан под новые статусы
    public function testOrderReady($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->field('status_id','status_1c'); // OR-MODE работает некорректно, поэтому потребовалась инъекция с преобразованием SQL-зпроса
        $sel->where('status_id')->equals(order::getStatusByCode('ready'));
        $sel->where('status_1c')->equals(1595826);// 1595826 - статус "Отгружен" из 1С
        if ($all != 'all') {
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
            $query = str_replace(" AND oc_659_lj.rel_val = '1595826' AND ", " OR oc_659_lj.rel_val = '1595826') AND (", $sel->query());
        }else{
            $query = str_replace(' AND oc_659_lj.rel_val', ' OR oc_659_lj.rel_val', $sel->query());
        }
        $sql = 'SELECT COUNT(id) as `ct` FROM cms3_objects WHERE `id` IN (SELECT o.id ' . substr($query, strpos($query, 'FROM')) . ')';
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        $total = 0;
        foreach ($result as $row) {
            $total = $row['ct'];
        }

        return array(
            'status' => true,
            'value' => $total,
        );
    }

    public function testOrderAll($range, $all) {
        if ($all != 'all') {
            $sel = new selector('objects');
            $sel->types('object-type')->name('emarket', 'order');
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
            $sel->where('status_id')->isnull(false);
            return array(
                'status' => true,
                'value' => $sel->length,
            );
        } else {
            $sel = new selector('objects');
            $sel->types('object-type')->name('emarket', 'order');
            $sel->where('status_id')->isnull(false);
            return array(
                'status' => true,
                'value' => $sel->length,
            );
        }
    }

    // AGH - переделан под новые статусы
    public function testSumAll($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->field('status_id','status_1c'); // OR-MODE работает некорректно, поэтому потребовалась инъекция с преобразованием SQL-зпроса
        $sel->where('status_id')->equals(order::getStatusByCode('ready'));
        $sel->where('status_1c')->equals(1595826);// 1595826 - статус "Отгружен" из 1С
        if ($all != 'all') {
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
            $query = str_replace(" AND oc_659_lj.rel_val = '1595826' AND ", " OR oc_659_lj.rel_val = '1595826') AND (", $sel->query());
        }else{
            $query = str_replace(' AND oc_659_lj.rel_val', ' OR oc_659_lj.rel_val', $sel->query());
        }
        $sql = "SELECT SUM(float_val) as `summa` FROM cms3_object_content WHERE field_id = '131' AND obj_id IN (SELECT o.id " . substr($query, strpos($query, 'FROM')) . ')';

        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);

        $sum = 0;
        foreach ($result as $row) {
            $sum = $row['summa'];
        }

        return array(
            'status' => true,
            'value' => $sum ? number_format(round($sum, 2), 2, '.', ' ') : 0,
        );
    }

    // AGH - переделан под новые статусы
    public function testSumMiddle($range, $all) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->field('status_id','status_1c'); // OR-MODE работает некорректно, поэтому потребовалась инъекция с преобразованием SQL-зпроса
        $sel->where('status_id')->equals(order::getStatusByCode('ready'));
        $sel->where('status_1c')->equals(1595826);// 1595826 - статус "Отгружен" из 1С
        if ($all != 'all') {
            $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
            $query = str_replace(" AND oc_659_lj.rel_val = '1595826' AND ", " OR oc_659_lj.rel_val = '1595826') AND (", $sel->query());
        }else{
            $query = str_replace(' AND oc_659_lj.rel_val', ' OR oc_659_lj.rel_val', $sel->query());
        }
        $sql = "SELECT COUNT(obj_id) as `ct`, SUM(float_val) as `summa` FROM cms3_object_content WHERE field_id = '131' AND obj_id IN (SELECT o.id " . substr($query, strpos($query, 'FROM')) . ')';

        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult($sql, true);
        $result->setFetchType(IQueryResult::FETCH_ASSOC);

        $sum = 0;
        $total = 0;
        foreach ($result as $row) {
            $sum = $row['summa'];
            $total = $row['ct'];
        }

        return array(
            'status' => true,
            'value' => $sum ? number_format(round($sum / $total, 2), 2, '.', ' ') : 0,
        );
    }

    public function testAddItems($range, $all) {
        if ($all != 'all') {
            $sel = new selector('objects');
            $sel->types('hierarchy-type')->name('catalog', 'object');
            $sel->where('date_create_object')->between($range['fromDate'], $range['toDate']);
            return array(
                'status' => true,
                'value' => $sel->length,
            );
        } else {
            $sel = new selector('objects');
            $sel->types('hierarchy-type')->name('catalog', 'object');
            return array(
                'status' => true,
                'value' => $sel->length,
            );
        }
    }

    public function getMostPopularProduct() {
        $range = $this->getDateRange(getRequest('param0'), getRequest('param1'));
        $emarketTop = new emarketTop();
        $config = mainConfiguration::getInstance();
        $sort = getRequest('sort');
        $result = $emarketTop->getTop($range, $config->get('modules', 'emarket.popular.max-items'), $sort);
        $buffer = outputBuffer::current();
        $buffer->option('generation-time', false);
        $buffer->clear();
        $buffer->push(json_encode(array('result' => $result)));
        $buffer->end();
    }

    public function partialRecalc() {
        if (isDemoMode()) {
            return false;
        }

        $this->setDataType("settings");
        $this->setActionType("view");

        $page = (int) getRequest("page");

        $emarketTop = new emarketTop();
        if ($page == 0) {
            $emarketTop->clearTableTop();
            regedit::getInstance()->setVal('//modules/emarket/last-reindex-result', false);
            regedit::getInstance()->setVal('//modules/emarket/last-reindex-date', date('Y-m-d'));
        }
        $config = mainConfiguration::getInstance();
        $total = (int) $emarketTop->allOrdersRecalculate();
        $limit = $config->get('modules', 'emarket.reindex.max-items');
        if ($limit == 0) {
            $limit = 5;
        }
        $result = $emarketTop->recalculation($limit, $page);

        if ($result['current'] >= $total) {
            regedit::getInstance()->setVal('//modules/emarket/last-reindex-result', true);
        }

        $data = Array(
            'index-items' => Array(
                'attribute:current' => $result['current'],
                'attribute:total' => $total,
                'attribute:page' => $result['page']
            )
        );

        $this->setData($data);
        return $this->doData();
    }

    public function getLastReindexDate() {
        $this->setDataType("list");
        $this->setActionType("view");

        $reindexDate = regedit::getInstance()->getVal('//modules/emarket/last-reindex-date');
        $reindexResult = (bool) regedit::getInstance()->getVal('//modules/emarket/last-reindex-result');

        $this->setData(array(
            'reindexDate' => $reindexDate,
            'reindexResult' => $reindexResult
        ), 2);

        return $this->doData();
    }

    /**
     * Функции новой статистики
     */

    /**
     * Всего заказов
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderAllFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
        $sel->where('status_id')->isnull(false);

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Всего брошенных корзин
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderStatusNullFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isnull(true);
        $sel->where('order_items')->isnotnull();
        $sel->where('order_create_date')->between($range['fromDate'], $range['toDate']);
        $sel->group('id');

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Заказов оплачивается
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderPaymentFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->option('or-mode')->fields('payment_status_id','status_payment_1c');
        $sel->where('payment_status_id')->equals(order::getStatusByCode('accepted', 'order_payment_status'));
        $sel->where('status_payment_1c')->equals(1595921); // оплачен - статус из 1С
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
        $sel->group('id');

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Заказов отменено
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderCanceledFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->fields('status_id','status_1c');
        $sel->where('status_id')->equals(order::getStatusByCode('canceled'));
//        $sel->where('status_1c')->equals(111111);// тут должен быть какой-то статус отмены заказа из 1С
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);
        $sel->group('id');

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Заказов готово
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderReadyFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
//        $sel->option('or-mode')->fields('status_id','status_1c'); // OR-MODE работает некорректно, поэтому потребовалась инъекция с преобразованием SQL-зпроса
//        $sel->where('status_id')->equals(order::getStatusByCode('ready'));
        $sel->where('status_1c')->equals(1595826); // 1595826 - статус "Отгружен" из 1С
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Заказов в статусе К оплате
     * @param type $range
     * @param type $all
     * @return type
     */
    public function testOrderToPayFull($range, $all) {
        if ($all == 'all') {
            return array('status' => true, 'value' => '');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_1c')->equals(1566620); // 1566620 - статус "К оплате" из 1С
        $sel->where('order_date')->between($range['fromDate'], $range['toDate']);

        $sum = 0;
        $count = $sel->length;
        foreach($sel as $order){
            $sum += $order->getValue('total_price');
        }

        return array(
            'status' => true,
            'value' => self::get_message($count, $sum)
        );
    }

    /**
     * Функция для получения форматированного сообщения
     * @param type $count
     * @param type $sum
     * @return type
     */
    private function get_message($count, $sum){
        $med = 0;
        if($count > 0){
            $med = round($sum / $count * 100)/100;
        }
        return '<b>' . $count . '</b> на сумму <b>' . number_format($sum, 2, ',', ' ') . '</b> руб., средний чек <b>' . number_format($med, 2, ',', ' ') . '</b> руб.';
    }
}