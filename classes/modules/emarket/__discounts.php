<?php
	abstract class __emarket_discounts {

		public function getAllDiscounts($codeName = false, $resetCache = false) {
			static $discounts = array();

			if ($resetCache || cmsController::$IGNORE_MICROCACHE) {
				$discounts = array();
			}

			if ($codeName && isset($discounts[$codeName])) {
				return $discounts[$codeName];
			} elseif (isset($discounts['all'])) {
				return $discounts['all'];
			}

			$cacheFrontend = cacheFrontend::getInstance();
			$key = ($codeName) ? (string) $codeName . '_discounts' : 'discounts_list';
			$cachedDiscounts = $cacheFrontend->loadData($key);
			if (is_array($cachedDiscounts)) {
				return $cachedDiscounts;
			}

			$sel = new selector('objects');
			$sel->types('hierarchy-type')->name('emarket', 'discount');
			$sel->where('is_active')->equals(true);
			if ($codeName) {
				$sel->where('discount_type_id')->equals($this->getDiscountTypeId($codeName));


				// Данный код предназначен для того, чтобы получать скидки, относящиеся либо только к конкретному пользователю, либо не имеющему пользователя в списке. Это важно, так как в противном случае число возможных скидок сликом велико (от 1000). А при таком раскладе, даже если выполнение проверки на 1 скидку занимает 0,01 секунду, то длительность проверки на все скидки - 10 секунд, что недопустимо
				// ---------
				$rules = new selector('objects');
				$rules->types('object-type')->id(318);
				$permissions = permissionsCollection::getInstance();
				if ($codeName == 'item' && $permissions->isAuth()) {
					$rules->option('or-mode')->field('users');
					$rules->where('users')->equals($permissions->getUserId());
					$rules->where('users')->isnull(true);
				}else{
					$rules->where('users')->isnull(true);
				}

				$rusel_arr = array(0);
				foreach($rules as $rule){
					$rusel_arr[] = $rule->id;
				}

				$sel->where('discount_rules_id')->equals($rusel_arr);
				// ---------

				$discounts[$codeName] = $sel->result();
				$cacheFrontend->saveData($key, $discounts[$codeName], 3600);
				return $discounts[$codeName];
			}
			$sel->option('load-all-props')->value(true);
			$sel->option('no-length')->value(true);
			$discounts['all'] = $sel->result();
			$cacheFrontend->saveData($key, $discounts['all'], 3600);
			return $discounts['all'];
		}

		public function getDiscountTypeId($codeName) {
			return discount::getTypeId($codeName);
		}

		public function discountInfo($discountId = false, $template = 'default') {
			if(!$template) $template = 'default';
			list($tpl_block, $tpl_block_empty) = def_module::loadTemplates("emarket/discounts/{$template}",
				'discount_block', 'discount_block_empty');

			try {
				$discount = itemDiscount::get($discountId);
			} catch (privateException $e) {
				$discount = null;
			}

			if($discount instanceof discount) {
				$info = array(
					'attribute:id'		=> $discount->id,
					'attribute:name'	=> $discount->getName(),
					'description'		=> $discount->getValue('description')
				);
				return def_module::parseTemplate($tpl_block, $info, false, $discount->id);
			} else {
				return def_module::parseTemplate($tpl_block_empty, array());
			}
		}
	};
?>
