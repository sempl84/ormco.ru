<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-partners-lists" => "Группы и страницы",
"header-partners-config" => "Настройки модуля",
"header-partners-add" => "Добавление",
"header-partners-edit" => "Редактирование",
 
"header-partners-add-groupelements"   => "Добавление группы",
"header-partners-add-item_element"     => "Добавление страницы",
 
"header-partners-edit-groupelements"   => "Редактирование группы",
"header-partners-edit-item_element"     => "Редактирование страницы",
 
"header-partners-activity" => "Изменение активности",
 
'perms-partners-view' => 'Просмотр страниц',
'perms-partners-lists' => 'Управление страницами',

'module-partners' => 'Партнеры',
 
);
 
?>