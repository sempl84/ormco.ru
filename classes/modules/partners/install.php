<?php

$INFO = Array();

$INFO['name'] = "partners";
$INFO['filename'] = "modules/partners/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/partners/__admin.php";
$COMPONENTS[1] = "./classes/modules/partners/class.php";
$COMPONENTS[2] = "./classes/modules/partners/i18n.php";
$COMPONENTS[3] = "./classes/modules/partners/lang.php";
$COMPONENTS[4] = "./classes/modules/partners/permissions.php";
?>