<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class ormcoUniOne extends def_module
{
    const registry_param_api_key = '//modules/ormcoUniOne/api_key';
    
    public function __construct()
    {
        parent::__construct();
        
        $cmsController = cmsController::getInstance();

        if ($cmsController->getCurrentMode() == "admin") {
            $this->__loadLib("__admin.php");
            $this->__implement("__ormcoUniOneAdmin");
    
            $this->__loadLib("__admin_abandoned_orders.php");
            $this->__implement("__ormcoUniOneAdminAbandonedOrders");

            $commonTabs = $this->getCommonTabs();
            if ($commonTabs instanceof iAdminModuleTabs) {
                $commonTabs->add('dashboard');
                $commonTabs->add('abandonedOrders', array(
                    'abandonedOrdersLogList', 'abandonedOrdersLogView'
                ));
            }
            
            $configTabs = $this->getConfigTabs();
            if($configTabs instanceof iAdminModuleTabs) {
                $configTabs->add('config');
                $configTabs->add('abandonedOrdersConfig');
            }
        } else {
            $this->autoDetectAttributes();
        }
    
        $this->__loadLib("__macros.php");
        $this->__implement("OrmcoUniOneMacros");
    
        $this->__loadLib("__macros_abandoned_orders.php");
        $this->__implement("OrmcoUniOneMacrosAbandonedOrders");
    }
    
    /**
     * @return UnioneApi
     * @throws publicException
     */
    public function getApi()
    {
        $apiKey = regedit::getInstance()->getVal(self::registry_param_api_key);
        if(!$apiKey) {
            throw new publicException('Не перед apikey');
        }
        
        return new UnioneApi($apiKey, CURRENT_WORKING_DIR . '/sys-temp/ormcoUnione/api/log');
    }
}