<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UnioneApi
{
    const server = 'https://go2.unisender.ru/';
    
    private $apiKey;
    private $logDir;
    
    /**
     * UnioneApi constructor.
     * @param $apiKey
     * @param $logDir
     */
    public function __construct($apiKey, $logDir)
    {
        $this->apiKey = $apiKey;
        $this->logDir = rtrim($logDir, '/');
    }
    
    public function emailSend(UnioneApiEmailSendRequest $requestModel, $server = null)
    {
        return $this->request('email/send', array(
            'message' => $requestModel->getData()
        ));
    }
    
    /**
     * @param null $limit
     * @param null $offset
     * @return UnioneApiTemplateModel[]|null
     * @throws publicException
     */
    public function templateList($limit = null, $offset = null)
    {
        $data = [];
        if ($limit) {
            $data['limit'] = $limit;
        }
        
        if ($offset) {
            $data['offset'] = $offset;
        }
        
        $result = $this->request('template/list', $data);
        if (getArrayKey($result, 'status') != 'success') {
            throw new publicException('Ошибка при выполении запроса');
        }
        
        $return = array();
        
        $templates = getArrayKey($result, 'templates');
        if ($templates) {
            foreach ($templates as $template) {
                $return[] = UnioneApiTemplateModel::loadFromArray($template);
            }
        }
        
        return count($return) ? $return : null;
    }
    
    private function request($url, $data, $server = null)
    {
        if (!is_array($data)) {
            $data = [];
        }
        
        $data['api_key'] = $this->apiKey;
        
        $url = rtrim($server ?: self::server, '/') . '/ru/transactional/api/v1/' . ltrim($url, '/') . '.json';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        
        $result = json_decode($response, true);
        if (!is_array($result)) {
            if($this->logDir) {
                SiteLogHelper::clearLogDir($this->logDir, time() - (86400 * 30));
                
                if(!is_dir($this->logDir)) {
                    mkdir($this->logDir, 0775, true);
                }
                
                $logFileName = $this->logDir . '/' . time() . '_' . rand(100, 999);
                
                file_put_contents($logFileName . '_request.json', json_encode(array(
                    'url' => $url,
                    'data' => $data
                )));
    
                file_put_contents($logFileName . '_response.json', json_encode(array(
                    'response' => $response,
                    'curl_error' => $error
                )));
            }
            throw new publicException('Неверный формат ответа');
        }
        
        if (getArrayKey($result, 'status') == 'error') {
            throw new publicException(getArrayKey($result, 'message'), getArrayKey($result, 'code'));
        }
        
        return $result;
    }
}