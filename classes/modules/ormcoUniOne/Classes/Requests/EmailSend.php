<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UnioneApiEmailSendRequest
{
    private $recipients;
    private $templateId;
    private $globalSubstitutions;
    private $globalMetaData;
    private $bodyHtml;
    private $bodyPlaintext;
    private $bodyAmp;
    private $subject;
    private $fromEmail;
    private $fromName;
    private $replyTo;
    
    public function addRecipient($email, $substitutions = array(), $metaData = array())
    {
        $recipient = array(
            'email' => $email
        );
        
        if (is_array($substitutions) && count($substitutions)) {
            $recipient['substitutions'] = $substitutions;
        }
        
        if (is_array($metaData) && count($metaData)) {
            $recipient['metadata'] = $metaData;
        }
        
        $this->recipients[] = $recipient;
    }
    
    /**
     * @param mixed $templateId
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
    }
    
    /**
     * @param mixed $globalSubstitutions
     */
    public function setGlobalSubstitutions($globalSubstitutions)
    {
        if (is_array($globalSubstitutions) && count($globalSubstitutions)) {
            $this->globalSubstitutions = $globalSubstitutions;
        }
    }
    
    /**
     * @param mixed $globalMetaData
     */
    public function setGlobalMetaData($globalMetaData)
    {
        if (is_array($globalMetaData) && count($globalMetaData)) {
            $this->globalMetaData = $globalMetaData;
        }
    }
    
    /**
     * @param mixed $bodyHtml
     */
    public function setBodyHtml($bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;
    }
    
    /**
     * @param mixed $bodyPlaintext
     */
    public function setBodyPlaintext($bodyPlaintext)
    {
        $this->bodyPlaintext = $bodyPlaintext;
    }
    
    /**
     * @param mixed $bodyAmp
     */
    public function setBodyAmp($bodyAmp)
    {
        $this->bodyAmp = $bodyAmp;
    }
    
    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    /**
     * @param mixed $fromEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }
    
    /**
     * @param mixed $fromName
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }
    
    /**
     * @param mixed $replyTo
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
    }
    
    public function getData()
    {
        $recipients = $this->recipients;
        if(!$recipients) {
            throw new publicException('Не указаны получатели сообщения');
        }
        
        $request = array(
            'recipients' => $recipients,
        );
        
        if($this->templateId) {
            $request['template_id'] = $this->templateId;
        } else {
            $body = array();
            if($this->bodyHtml) {
                $body['html'] = $this->bodyHtml;
            }
    
            if($this->bodyPlaintext) {
                $body['plaintext'] = $this->bodyPlaintext;
            }
    
            if(!$body) {
                throw new publicException('Не указаны обязательные параметры для объекта body');
            }
    
            if($this->bodyAmp) {
                $body['amp'] = $this->bodyAmp;
            }
            
            $request['body'] = $body;
        }
        
        if(is_array($this->globalSubstitutions) && count($this->globalSubstitutions)) {
            $request['global_substitutions'] = $this->globalSubstitutions;
        }
    
        if(is_array($this->globalMetaData) && count($this->globalMetaData)) {
            $request['global_metadata'] = $this->globalMetaData;
        }
        
        if($this->subject) {
            $request['subject'] = $this->subject;
        }
        
        if($this->fromEmail) {
            $request['from_email'] = $this->fromEmail;
        }
    
        if($this->fromName) {
            $request['from_name'] = $this->fromName;
        }
        
        if($this->replyTo) {
            $request['reply_to'] = $this->replyTo;
        }
        
        return $request;
    }
}