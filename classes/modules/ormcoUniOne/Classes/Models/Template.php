<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UnioneApiTemplateModel
{
    private $id;
    private $name;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    public static function loadFromArray($data)
    {
        $model = new self();
        $model->id = getArrayKey($data, 'id');
        $model->name = getArrayKey($data, 'name');
        
        return $model;
    }
}