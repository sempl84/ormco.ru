<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrmcoUniOneMacros extends ormcoUniOne
{
    public function getOrmcoUniOneTemplateList()
    {
        $templates = $this->getApi()->templateList();
        if(!$templates) {
            return null;
        }
        
        $items = array();
        foreach($templates as $template) {
            $items[] = array(
                'attribute:id' => $template->getId(),
                'node:name' => $template->getName()
            );
        }
        
        if(!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
}