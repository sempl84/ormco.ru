<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrmcoUniOneMacrosAbandonedOrders extends ormcoUniOne
{
    const registry_param_enabled = '//modules/ormcoUniOne/abandoned_orders/enabled';
    const registry_param_template_1_id = '//modules/ormcoUniOne/abandoned_orders/template_1/id';
    const registry_param_template_1_offset = '//modules/ormcoUniOne/abandoned_orders/template_1/offset';
    const registry_param_template_1_test_email = '//modules/ormcoUniOne/abandoned_orders/template_1/test_email';
    const registry_param_template_2_id = '//modules/ormcoUniOne/abandoned_orders/template_2/id';
    const registry_param_template_2_offset = '//modules/ormcoUniOne/abandoned_orders/template_2/offset';
    const registry_param_template_2_test_email = '//modules/ormcoUniOne/abandoned_orders/template_2/test_email';
    
    /**
     * @param $timeStart
     * @param $timeEnd
     * @return selector
     * @throws selectorException
     * @throws publicException
     */
    public function _buildAbandonedOrdersSelector($timeStart, $timeEnd)
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        $sel->where('updatetime')->between($timeStart, $timeEnd);
        $sel->where(SiteEmarketOrderModel::field_number)->isnull();
        $sel->where(SiteEmarketOrderModel::field_order_items)->isnotnull();
        $sel->order('id')->asc();
        
        return $sel;
    }
    
    public function _canSendAbandonedOrderMessage(umiObject $order)
    {
        $customerId = $order->getValue(SiteEmarketOrderModel::field_customer_id);
        if (!$customerId) {
            return false;
        }
        
        $customer = umiObjectsCollection::getInstance()->getObject($customerId);
        if (!$customer instanceof umiObject || $customer->getMethod() != SiteUsersUserModel::method) {
            return false;
        }
        
        $email = $customer->getValue(SiteUsersUserModel::field_email);
        if (!$email) {
            return false;
        }
        
        if (!$order->getValue(SiteEmarketOrderModel::field_order_items)) {
            return false;
        }
        
        return true;
    }
    
    public function _canSendAbandonedOrderTemplateMessageByMessageNumber(umiObject $order, $messageNumber)
    {
        return intval($order->getValue(SiteEmarketOrderModel::field_abandoned_orders_message_number)) < $messageNumber;
    }
    
    public function _canSendAbandonedOrderTemplateMessageByDate(umiObject $order, $offset, $time = null)
    {
        $time = intval($time);
        if ($time <= 0) {
            $time = SiteDateHelper::normalizeToMinute(time());
        }
        
        if (SiteDateHelper::normalizeToMinute(intval($order->getUpdateTime())) > ($time - $offset)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * @param $timeStart
     * @return int|null
     * @throws publicException
     * @throws selectorException
     */
    public function testFindAbandonedOrdersByDays($days)
    {
        $sel = $this->_buildAbandonedOrdersSelector(time() - ($days * 86400), time());
        $sel->limit(0, 10);
        
        return $sel->length();
    }
    
    public $system;
    
    public function _sendAbandonedOrdersEmail(array $orders, $template, array $arAllowedEmail, $forceEmail = null, $server = null)
    {
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        
        cmsController::getInstance()->getModule('emarket');
        
        $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName();
        
        $linkCart = $domain . '/emarket/cart/';
        $templateResourcesLink = $domain . '/templates/ormco/img/mail';
        
        $request = new UnioneApiEmailSendRequest();
        $request->setTemplateId($template);
        $request->setGlobalSubstitutions(array(
            'domainLogo' => $domain . '/templates/ormco/img/letters/logo.png',
            'domain' => $domain,
            'linkCart' => $linkCart
        ));
        
        $this->system = system_buildin_load('system');
        
        $arOrderObjectId = array();
        
        foreach ($orders as $orderObject) {
            if (!$orderObject instanceof umiObject) {
                continue;
            }
            
            $order = order::get($orderObject->getId());
            if (!$order instanceof order) {
                continue;
            }
            
            $customerId = $order->getCustomerId();
            if (!$customerId) {
                continue;
            }
            
            $customer = $umiObjectsCollection->getObject($customerId);
            if (!$customer instanceof umiObject || $customer->getMethod() != SiteUsersUserModel::method) {
                continue;
            }
            
            if ($forceEmail) {
                $email = $forceEmail;
            } else {
                $email = $customer->getValue(SiteUsersUserModel::field_email);
                
                if (count($arAllowedEmail) && !in_array($email, $arAllowedEmail)) {
                    $email = false;
                }
            }
            
            if (!$email) {
                continue;
            }
            
            $items = $order->getItems();
            if (!count($items)) {
                continue;
            }
            
            $item1Html = $this->_renderAbandonedOrderItemHtml($items[0], $domain, $linkCart);
            if (count($items) > 1) {
                $item2Html = '<td style="padding: 0; min-width: 12px;"></td>' . $this->_renderAbandonedOrderItemHtml($items[1], $domain, $linkCart);
            } else {
                $item2Html = '';
            }
            
            if (count($items) > 2) {
                $item3Html = '<td style="padding: 0; min-width: 12px;"></td>
          <td style="padding: 15px; min-width: 15%; max-width: 146px; background: #fff;">
            <table style="border: 0; border-collapse: collapse;">
              <tr>
                <td style="padding: 0 0 0 16px; vertical-align: middle; font-size: 14px; line-height: 16px;"><a href="//' . $linkCart . '?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=smotret_vse" style="color: #2AA8EC; text-decoration: none;">Смотреть все</a></td>
                <td style="padding: 0 0 0 6px; vertical-align: middle;"><a href="//' . $linkCart . '?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=smotret_vse" style="text-decoration: none; "><img src="http://' . $templateResourcesLink . '/arrow.png" alt="" style="width: 24px; display: block; outline: none; border: 0;" /></a></td>
              </tr>
            </table>
          </td>';
            } else {
                $item3Html = '';
            }
            
            $substitutions = array(
                'customerName' => SiteUsersUserModel::getUserFullName($customer),
                'item1' => trim($item1Html),
                'item2' => trim($item2Html),
                'item3' => trim($item3Html)
            );
            
            $request->addRecipient($email, $substitutions);
            
            $arOrderObjectId[] = $orderObject->getId();
        }
        
        if (!$arOrderObjectId) {
            return false;
        }
        
        $this->getApi()->emailSend($request, $server);
        
        return $arOrderObjectId;
    }
    
    public function _renderAbandonedOrderItemHtml(orderItem $item, $domain, $linkCart)
    {
        $name = $item->getName();
        if (mb_strlen($name) >= 38) {
            $name = mb_substr($name, 0, 32) . '...';
        }
        $link = false;
        $image = false;
        $article = '';
        
        $element = $item->getItemElement(true);
        if ($element instanceof umiHierarchyElement) {
            $link = umiLinksHelper::getInstance()->getLink($element);
            $image = $element->getValue(SiteCatalogObjectModel::field_photo);
            $article = $element->getValue(SiteCatalogObjectModel::field_article);
        }
        
        if ($image instanceof umiImageFile && !$image->getIsBroken()) {
            $imageSource = $image->getFilePath(true);
        } else {
            $imageSource = '/templates/ormco/img/nofoto.jpg';
        }
        
        if ($this->system instanceof system) {
            $thumb = $this->system->makeThumbnailFull('.' . $imageSource, 150, 'auto');
            
            if (is_array($thumb) && isset($thumb['src'])) {
                $imageSource = $thumb['src'];
            }
        }
        
        if ($link) {
            $href = 'href="//' . $domain . $link . '?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=product_pics"';
        } else {
            $href = '';
        }
        
        if ($article) {
            $articleHtml = '<p style="font-size: 10px; line-height: 14px; color: #c2c2c2; margin: 0 0 14px;">Артикул: ' . $article . '</p>';
        } else {
            $articleHtml = '';
        }
        
        $price = number_format($item->getItemPrice(), 0, '', ' ');
        
        return <<<HTML
<td style="padding: 15px; min-width: 15% max-width: 146px; background: #fff; text-align: left;">
    <p style="margin: 0 0 16px; text-align: center;"><a {$href} style="outline: none; text-decoration: none;"><img src="http://{$domain}{$imageSource}?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=product_pics" alt="" style="border: 0; outline: none; max-width: 100% !important;" /></a></p>
    <p style="font-weight: 600; font-size: 16px; line-height: 18px; color: #377BB5; margin: 0 0 8px;">{$price} ₽</p>
    <p style="font-size: 14px; line-height: 18px; color: #333342; margin: 0 0 4px; height: 36px;">{$name}</p>
    {$articleHtml}
    <table style="border: 0; border-collapse: collapse;">
        <tr>
            <td style="padding: 0; vertical-align: middle; font-size: 14px; line-height: 16px;"><a href="//{$linkCart}?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=kupit" style="color: #2AA8EC; text-decoration: none;">Купить</a></td>
            <td style="padding: 0 0 0 6px; vertical-align: middle;"><a href="//{$linkCart}?utm_source=triggers_ormcoru&utm_medium=email&utm_campaign=dropped_basket&utm_content=kupit" style="text-decoration: none; "><img src="http://{$domain}/templates/ormco/img/mail/arrow.png" alt="" style="width: 24px; display: block; outline: none; border: 0;" /></a></td>
        </tr>
    </table>
</td>
HTML;
    }
    
    public function testCanSendAbandonedOrderTemplateMessage($orderId = null, $messageNumber = null, $time = null)
    {
        if (!$orderId) {
            throw new publicException('Не передан id заказа');
        }
        
        cmsController::getInstance()->getModule('emarket');
        $order = order::get($orderId);
        if (!$order instanceof order) {
            throw new publicException('Не найден заказ ' . $orderId);
        }
        
        $messageNumber = intval($messageNumber);
        
        switch ($messageNumber) {
            case 1:
            {
                $offset = regedit::getInstance()->getVal(self::registry_param_template_1_offset);
                break;
            }
            case 2:
            {
                $offset = regedit::getInstance()->getVal(self::registry_param_template_2_offset);
                break;
            }
            default:
            {
                throw new publicException('Неизвестный номер шаблона письма');
            }
        }
        
        if (!$this->_canSendAbandonedOrderTemplateMessageByMessageNumber($order->getObject(), 1)) {
            return 'false by number';
        }
        
        if (!$this->_canSendAbandonedOrderTemplateMessageByDate($order->getObject(), intval($offset) * 60)) {
            return 'false by date';
        }
        
        return 'true';
    }
    
    public function testRenderAbandonedOrderItemHtml($orderItemId = null)
    {
        $orderItemId = intval($orderItemId);
        if ($orderItemId < 0) {
            throw new publicException('Не передан id наименования заказа');
        }
        
        if (!class_exists('orderItem')) {
            cmsController::getInstance()->getModule('emarket');
        }
        
        $orderItem = orderItem::get($orderItemId);
        if (!$orderItem instanceof orderItem) {
            throw new publicException('Не найдено наименование заказа ' . $orderItemId);
        }
        
        $this->system = system_buildin_load('system');
        
        return $this->_renderAbandonedOrderItemHtml($orderItem, 'ormco-test3.dpromo.su', '/emarket/cart/');
    }
    
    public function testNormalizeToMinute()
    {
        return SiteDateHelper::normalizeToMinute(time());
    }
}