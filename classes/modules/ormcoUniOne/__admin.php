<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __ormcoUniOneAdmin extends baseModuleAdmin
{
    public function dashboard()
    {
        $this->setDataType("settings");
        $this->setActionType("view");
    
        if ($this->ifNotXmlMode()) {
            $this->doData();
            return;
        }
    
        $this->setData(array());
        $this->doData();
    }
    
    public function config() {
        $regedit = regedit::getInstance();
        
        $params = array(
            'unione-config-api' => array(
                'string:unione-config-api-key' => NULL
            )
        );
        
        $mode = getRequest('param0');
        
        if($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(ormcoUniOne::registry_param_api_key, $params['unione-config-api']['string:unione-config-api-key']);
            $this->chooseRedirect();
        }
    
        $params['unione-config-api']['string:unione-config-api-key'] = $regedit->getVal(ormcoUniOne::registry_param_api_key);
        
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
    
    public function getDatasetConfiguration($param = '')
    {
        $arParams = array();
        
        if (strpos($param, '|') !== false) {
            $data = explode('|', $param);
            $param = $data[0];
            $arParams[] = $data[1];
        }
        
        $module = $this;
        /* @var $module __ormcoUniOneAdmin */
        
        switch ($param) {
            default:
            {
                throw new publicException('Неизвестный параметр');
            }
        }
    }
}