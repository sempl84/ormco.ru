<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$i18n = Array(
    'module-ormcoUniOne' => 'UniOne',
    
    'header-ormcoUniOne-dashboard' => 'Панель управления',
    
    'header-ormcoUniOne-abandonedOrders' => 'Брошенные корзины',
    'header-ormcoUniOne-abandonedOrdersLogList' => 'Логи',
    'header-ormcoUniOne-abandonedOrdersLogView' => 'Просмотр лога',
    
    'header-ormcoUniOne-config' => 'Базовые настройки',
    'group-unione-config-api' => 'Интеграция по API',
    'option-unione-config-api-key' => 'Api Key',
    
    'header-ormcoUniOne-abandonedOrdersConfig' => 'Брошенные корзины',
    'group-unione-abandonedOrdersConfig' => 'Настройки',
    'option-unione-abandonedOrdersConfig-enabled' => 'Включена отправка сообщений',
    'option-unione-abandonedOrdersConfig-template-1-id' => 'Письмо 1: Шаблон',
    'option-unione-abandonedOrdersConfig-template-1-offset' => 'Письмо 1: Через сколько часов считать корзину брошенной',
    'option-unione-abandonedOrdersConfig-template-1-test-email' => 'Письмо 1: E-mail адреса для тестирования (через запятую)',
    'option-unione-abandonedOrdersConfig-template-2-id' => 'Письмо 2: Шаблон',
    'option-unione-abandonedOrdersConfig-template-2-offset' => 'Письмо 2: Через сколько часов считать корзину брошенной',
    'option-unione-abandonedOrdersConfig-template-2-test-email' => 'Письмо 2: E-mail адреса для тестирования (через запятую)',
    
    'perms-ormcoUniOne-admin' => 'Управление модулем',
);