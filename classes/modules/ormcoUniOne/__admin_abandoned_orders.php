<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __ormcoUniOneAdminAbandonedOrders extends __ormcoUniOneAdmin
{
    public function abandonedOrdersConfig()
    {
        $regedit = regedit::getInstance();
        
        $params = array(
            'unione-abandonedOrdersConfig' => array(
                'boolean:unione-abandonedOrdersConfig-enabled' => NULL,
                'select:unione-abandonedOrdersConfig-template-1-id' => NULL,
                'int:unione-abandonedOrdersConfig-template-1-offset' => NULL,
                'string:unione-abandonedOrdersConfig-template-1-test-email' => NULL,
                'select:unione-abandonedOrdersConfig-template-2-id' => NULL,
                'int:unione-abandonedOrdersConfig-template-2-offset' => NULL,
                'string:unione-abandonedOrdersConfig-template-2-test-email' => NULL,
            )
        );
        
        $module = cmsController::getInstance()->getModule('ormcoUniOne');
        if (!$module instanceof ormcoUniOne) {
            throw new publicException('Не найден модуль ormcoUniOne');
        }
        
        $templateOptions = array(
            'empty' => 'Не выбран',
        );
        
        $templates = $module->getApi()->templateList();
        if ($templates) {
            foreach ($templates as $template) {
                $templateOptions[$template->getId()] = $template->getName();
            }
        }
        
        $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-1-id'] = $templateOptions;
        $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-2-id'] = $templateOptions;
        
        $mode = getRequest('param0');
        
        if ($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_enabled, $params['unione-abandonedOrdersConfig']['boolean:unione-abandonedOrdersConfig-enabled']);
            $template1Id = $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-1-id'];
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_id, $template1Id != 'empty' ? $template1Id : false);
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_offset, $params['unione-abandonedOrdersConfig']['int:unione-abandonedOrdersConfig-template-1-offset']);
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_test_email, $params['unione-abandonedOrdersConfig']['string:unione-abandonedOrdersConfig-template-1-test-email']);
            $template2Id = $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-2-id'];
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_id, $template2Id != 'empty' ? $template2Id : false);
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_offset, $params['unione-abandonedOrdersConfig']['int:unione-abandonedOrdersConfig-template-2-offset']);
            $regedit->setVar(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_test_email, $params['unione-abandonedOrdersConfig']['string:unione-abandonedOrdersConfig-template-2-test-email']);
            $this->chooseRedirect();
        }
        
        $params['unione-abandonedOrdersConfig']['boolean:unione-abandonedOrdersConfig-enabled'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_enabled);
        $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-1-id']['value'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_id);
        $params['unione-abandonedOrdersConfig']['int:unione-abandonedOrdersConfig-template-1-offset'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_offset);
        $params['unione-abandonedOrdersConfig']['string:unione-abandonedOrdersConfig-template-1-test-email'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_test_email);
        $params['unione-abandonedOrdersConfig']['select:unione-abandonedOrdersConfig-template-2-id']['value'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_id);
        $params['unione-abandonedOrdersConfig']['int:unione-abandonedOrdersConfig-template-2-offset'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_offset);
        $params['unione-abandonedOrdersConfig']['string:unione-abandonedOrdersConfig-template-2-test-email'] = $regedit->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_test_email);
    
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
    
    public function abandonedOrders()
    {
        $this->redirect('/admin/ormcoUniOne/abandonedOrdersLogList/');
    }
    
    public function abandonedOrdersLogList()
    {
        $this->setDataType("settings");
        $this->setActionType("view");
        
        $data = array();
        
        $dir = OrmcoUniOneSendAbandonedOrdersMessage::getLogDir();
        if (is_dir($dir)) {
            $items = array();
            
            $iterator = new DirectoryIterator($dir);
            
            foreach ($iterator as $file) {
                if (!$file->isFile()) {
                    continue;
                }
                
                if ($file->getExtension() != 'log') {
                    continue;
                }
                
                $id = str_replace('.log', '', $file->getFilename());
                if(strlen($id) > 8) {
                    $date = $id;
                    $text = date('d.m.Y H:i:s', $date);
                } else {
                    $date = strtotime($id);
                    $text = date('d.m.Y', $date);
                }
                
                $items[$date] = array(
                    'attribute:link' => '/admin/ormcoUniOne/abandonedOrdersLogView/' . $id . '/',
                    'node:text' => $text
                );
            }
            
            if (count($items)) {
                arsort($items);
                
                $data['items'] = array('nodes:item' => $items);
            }
        }
        
        $this->setData($data);
        $this->doData();
    }
    
    public function abandonedOrdersLogView()
    {
        $this->setDataType("settings");
        $this->setActionType("view");
        
        $data = array();
        
        $dir = OrmcoUniOneSendAbandonedOrdersMessage::getLogDir();
        if (is_dir($dir)) {
            $id = getRequest('param0');
            
            $file = $dir . '/' . $id . '.log';
            if (file_exists($file)) {
                $reverse = (bool)getRequest('reverse');
                
                $fileContents = trim(file_get_contents($file));
                
                if($reverse) {
                    $data['attribute:reverse'] = true;
                    $log = implode('<br />', array_reverse(explode(PHP_EOL, $fileContents)));
                } else {
                    $log = nl2br($fileContents);
                }
                
                $data['attribute:id'] = $id;
                $data['log'] = $log;
            }
        }
        
        $this->setData($data);
        $this->doData();
    }
}