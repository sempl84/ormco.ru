<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

$classes = [
    'UnioneApi' => __DIR__ . '/Classes/Api.php',
    'UnioneApiTemplateModel' => __DIR__ . '/Classes/Models/Template.php',
    'UnioneApiEmailSendRequest' => __DIR__ . '/Classes/Requests/EmailSend.php',
];