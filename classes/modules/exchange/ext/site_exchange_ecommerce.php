<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class site_exchange_ecommerce
{
    public function getExchangeEcommerceEmarketPurchaseCode($orderId = null)
    {
        if (!$orderId) {
            throw new publicException('Не передан id заказа');
        }

        $emarket = cmsController::getInstance()->getModule('emarket');
        if (!$emarket instanceof emarket) {
            throw new publicException('Не найден модуль emarket');
        }

        if (!class_exists('order')) {
            throw new publicException('Не найден класс order');
        }

        $order = order::get($orderId);
        if (!$order instanceof order) {
            throw new publicException('Не найден заказ ' . $orderId);
        }

        if ($order->isEmpty()) {
            return null;
        }

        $number = $order->getValue('number');

        if (!$number) {
            return null;
        }

        $actionField = array(
            'id' => $number
        );

        $actionField['revenue'] = $order->getActualPrice();
        if ($deliveryPrice = $order->getDeliveryPrice()) {
            $actionField['shipping'] = $deliveryPrice;
        }

        if ($coupon = $order->getValue(SiteEmarketOrderModel::field_ormco_coupon_code)) {
            $actionField['coupon'] = $coupon;
        }

        $data = array();

        foreach ($order->getItems() as $item) {
            if (!$item instanceof optionedOrderItem) {
                continue;
            }

            $element = $item->getItemElement(true);
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }

            $arItem = array(
                'id' => $element->getId(),
                'name' => $element->getName(),
                'price' => $item->getItemPrice(),
            );

            $arItem['quantity'] = $item->getAmount();

            $arCategory = $this->_prepareEcommerceCategoryData($element->getId());
            if ($arCategory) {
                $arItem['category'] = implode(', ', $arCategory);
            }

            $data[] = $arItem;
        }

        $result = array(
            'actionField' => $actionField,
            'products' => $data
        );

        return array('params' => json_encode($result), 'order_id' => $orderId);
    }

    public function getExchangeEcommerceDetailCode($pageId = null, $price = null)
    {
        $pageId = (int)$pageId;
        if ($pageId <= 0) {
            throw new publicException('Не передан id блока');
        }

        $page = umiHierarchy::getInstance()->getElement($pageId);
        if (!$page instanceof umiHierarchyElement || $page->getMethod() != SiteCatalogObjectModel::method) {
            throw new publicException('Не найдена страница ' . $pageId);
        }

        return array(
            'attribute:id' => $pageId,
            'params' => json_encode($this->_prepareEcommerceProductData($page, $price))
        );
    }

    static $ecommercePromotions = array();

    /**
     * @param null $id
     * @param null $name
     * @param null $creative
     * @param null $position
     * @param null $returnParams
     * @return array|null
     * @throws publicException
     */
    public function addExchangeEcommercePromoView($id = null, $name = null, $creative = null, $position = null, $returnParams = null)
    {
        if (!$id) {
            throw new publicException('Не передан id блока');
        }

        $block = array(
            'id' => $id
        );

        $name = trim(str_replace('()', '', $name));
        if ($name) {
            $block['name'] = $name;
        }

        $creative = trim($creative);
        if ($creative) {
            $block['creative'] = $creative;
        }

        $position = trim($position);
        if ($position) {
            $block['position'] = $position;
        }

        self::$ecommercePromotions[] = $block;

        return $returnParams ? array('params' => json_encode($block, ENT_QUOTES)) : null;
    }

    static $ecommerceImpressions = array();

    /**
     * @param null $pageId
     * @param null $price
     * @param null $list
     * @param null $position
     * @return array|null
     * @throws publicException
     */
    public function addExchangeEcommerceImpression($pageId = null, $price = null, $list = null, $position = null, $returnParams = null)
    {
        $pageId = (int)$pageId;
        if ($pageId <= 0) {
            throw new publicException('Не передан id блока');
        }

        $page = umiHierarchy::getInstance()->getElement($pageId);
        if (!$page instanceof umiHierarchyElement || $page->getMethod() != SiteCatalogObjectModel::method) {
            throw new publicException('Не найдена страница ' . $pageId);
        }

        $price = (float)$price;
        if ($price <= 0) {
            throw new publicException('Не передана стоимость');
        }

        $list = trim($list);
        if (!$list) {
            throw new publicException('Не передано название списка');
        }

        $position = (int)$position;
        if ($position <= 0) {
            throw new publicException('Не передана позиция в списке');
        }

        $name = $page->getName();

        $arCategory = $this->_prepareEcommerceCategoryData($pageId);
        if ($arCategory) {
            $category = implode(', ', $arCategory);
        } else {
            $category = '';
        }

        self::$ecommerceImpressions[] = array(
            'name' => $name,
            'id' => $pageId,
            'price' => $price,
            'category' => $category,
            'list' => $list,
            'position' => $position
        );

        if (!$returnParams) {
            return null;
        }

        $params = array(
            'actionField' => array(
                'list' => $list
            ),
            'products' => array(
                array(
                    'name' => $name,
                    'id' => $pageId,
                    'price' => $price,
                    'category' => $category,
                    'position' => $position
                )
            )
        );

        return array(
            'attribute:id' => $pageId,
            'attribute:list' => $list,
            'params' => json_encode($params, ENT_QUOTES)
        );
    }

    public function getExchangeEcommerceProductData($pageId = null, $price = null)
    {
        $page = umiHierarchy::getInstance()->getElement($pageId);
        if (!$page instanceof umiHierarchyElement || $page->getMethod() != SiteCatalogObjectModel::method) {
            throw new publicException('Не найдена страница ' . $pageId);
        }

        $price = (float)$price;
        if ($price <= 0) {
            throw new publicException('Не передана стоимость');
        }

        return array(
            'attribute:id' => $pageId,
            'params' => json_encode($this->_prepareEcommerceProductData($page, $price), ENT_QUOTES)
        );
    }

    public function getExchangeEcommerceBracketsData($bracketsData = null)
    {
        if (!$bracketsData) {
            throw new publicException('Не переданы id товаров');
        }

        $arBrackets = explode('_', $bracketsData);
        if (!count($arBrackets)) {
            throw new publicException('Не переданы id товаров');
        }

        $productsData = array();

        $umiHierarchy = umiHierarchy::getInstance();
        $umiHierarchy->loadElements($arBrackets);

        foreach ($arBrackets as $bracket) {
            $arBracket = explode("|", $bracket);
            if (!isset($arBracket[0]) || !isset($arBracket[1])) {
                continue;
            }

            $page = $umiHierarchy->getElement($arBracket[0]);
            if (!$page instanceof umiHierarchyElement || $page->getMethod() != SiteCatalogObjectModel::method) {
                continue;
            }

            $productsData[] = $this->_prepareEcommerceProductData($page, $page->getValue(SiteCatalogObjectModel::field_price), $arBracket[1]);
        }

        if (!count($productsData)) {
            throw new publicException('Не найдены данные');
        }

        $buffer = outputBuffer::current();
        if (!$buffer instanceof HTTPOutputBuffer) {
            throw new publicException('Неверный буфер');
        }

        $buffer->clear();

        $buffer->contentType('application/json');
        $buffer->push(json_encode(array('products' => $productsData)));
        $buffer->send();
        exit;
    }

    static $ecommerceCheckout = array();

    public function addExchangeEcommerceCheckoutCode($step = null)
    {
        if (!$step) {
            throw new publicException('Не передан step');
        }

        $emarket = cmsController::getInstance()->getModule('emarket');
        if (!$emarket instanceof emarket) {
            throw new publicException('Не найден модуль emarket');
        }

        /* @var $emarket emarket|__emarket_purchasing */

        $order = $emarket->getBasketOrder();
        if (!$order instanceof order) {
            throw new publicException('Не найден объект корзины');
        }

        $products = array();
        foreach ($order->getItems() as $orderItem) {
            if (!$orderItem instanceof orderItem) {
                continue;
            }

            $element = $orderItem->getItemElement(true);
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }

            $products[] = $this->_prepareEcommerceProductData($element, $orderItem->getItemPrice(), $orderItem->getAmount());
        }

        if (!count($products)) {
            throw new publicException('Не найдены данные о продуктах');
        }

        self::$ecommerceCheckout = array(
            'actionField' => array(
                'step' => $step
            ),
            'products' => $products
        );
    }

    const session_param_send_user_id = 'send_user_id';

    public function getExchangeEcommerceCode()
    {
        $data = array();

        if (count(self::$ecommercePromotions)) {
            $data[] = array(
                'event' => 'ecommercePromoView',
                'ecommerce' => array(
                    'promoView' => array(
                        'promotions' => self::$ecommercePromotions
                    )
                )
            );
        }

        if (count(self::$ecommerceImpressions)) {
            $ecommerceImpressionsCount = count(self::$ecommerceImpressions) - 1;
            $ecommerceImpressionsLimit = 200;
            $ecommerceImpressionsBlocksCount = intval(floor($ecommerceImpressionsCount / $ecommerceImpressionsLimit));

            for ($i = 0; $i <= $ecommerceImpressionsBlocksCount; $i++) {
                $ecommerceImpressionsBlock = array_slice(self::$ecommerceImpressions, $i * $ecommerceImpressionsLimit, $ecommerceImpressionsLimit);
                if (!$ecommerceImpressionsBlock) {
                    continue;
                }

                $data[] = array(
                    'event' => 'ecommerceImpressions',
                    'ecommerce' => array(
                        'impressions' => $ecommerceImpressionsBlock
                    )
                );
            }
        }

        if (count(self::$ecommerceCheckout)) {
            $data[] = array(
                'event' => 'ecommerceCheckout',
                'ecommerce' => array(
                    'checkout' => self::$ecommerceCheckout
                )
            );
        }

        return $this->_renderEcommerceCode($data);
    }

    public function getExchangeEcommerceCodeBeforeGTM()
    {
        $data = array();

        if (Service::Session()->get(self::session_param_send_user_id)) {
            $userId = Service::Auth()->getUserId();
            if ($userId != Service::SystemUsersPermissions()->getGuestUserId()) {
                $user = umiObjectsCollection::getInstance()->getObject($userId);
                if ($user instanceof umiObject) {
                    $commonUid = $user->getValue(SiteUsersUserModel::field_common_uid);
                    if ($commonUid) {
                        $data[] = array(
                            'UserID' => $commonUid
                        );
                    }
                }
            }

            Service::Session()->del(self::session_param_send_user_id);
        }

        return $this->_renderEcommerceCode($data);
    }

    public function _renderEcommerceCode($data = array())
    {
        if (!count($data)) {
            return null;
        }

        $code = array();
        $code[] = 'var is_already_show = {"ecommercePromoView":false, "ecommerceImpressions":false, "ecommerceCheckout":false};';
        $code[] = 'function do_on_scroll_to_element(selector, is_allready_show_index, do_function){'
                . '    window.addEventListener("scroll",function(){'
                . '        if(is_already_show[is_allready_show_index] === false){'
                . '            if(document.documentElement.scrollTop >= document.querySelector(selector).getBoundingClientRect().top){'
                . '                do_function();'
                . '                is_already_show[is_allready_show_index] = true;'
                . '            }'
                . '        }'
                . '    });'
                . '}';

        foreach ($data as $event) {
            switch ($event['event']) {
//                case 'ecommercePromoView':
//                    $code[] = 'do_on_scroll_to_element("", function(){';
//                    $code[] = 'dataLayer.push(' . json_encode($event) . ')';
//                    $code[] = '});';
//                    break;
                case 'ecommerceImpressions':
                    $code[] = 'do_on_scroll_to_element(".viewed-products", "ecommerceImpressions", function(){';
                    $code[] = 'dataLayer.push(' . json_encode($event) . ')';
                    $code[] = '});';
                    break;
//                case 'ecommerceCheckout':
//                    $code[] = 'do_on_scroll_to_element("", function(){';
//                    $code[] = 'dataLayer.push(' . json_encode($event) . ')';
//                    $code[] = '});';
//                    break;
                default:
                    $code[] = 'dataLayer.push(' . json_encode($event) . ')';
                    break;
            }
        }

        return array('code' => implode(PHP_EOL, $code));
    }

    public function _prepareEcommerceCategoryData($pageId)
    {
        $umiHierarchy = umiHierarchy::getInstance();

        $parents = umiHierarchy::getInstance()->getAllParents($pageId);

        $arCategories = array();

        if (count($parents) > 2) {
            for ($i = 2; $i < count($parents); $i++) {
                $parent = $umiHierarchy->getElement($parents[$i]);
                if (!$parent instanceof umiHierarchyElement) {
                    continue;
                }

                $arCategories[] = $parent->getName();
            }
        }

        return count($arCategories) ? $arCategories : false;
    }

    public function _prepareEcommerceProductData(umiHierarchyElement $page, $price, $quantity = null)
    {
        $price = (float)$price;
        if ($price <= 0) {
            throw new publicException('Не передана стоимость');
        }

        $pageId = $page->getId();

        $arCategory = $this->_prepareEcommerceCategoryData($pageId);
        if ($arCategory) {
            $category = implode(', ', $arCategory);
        } else {
            $category = '';
        }

        $return = array(
            'name' => $page->getName(),
            'id' => $pageId,
            'price' => $price,
            'category' => $category
        );

        if ($quantity) {
            $return['quantity'] = $quantity;
        }

        return $return;
    }

    public function onUsersLoginSuccessFullExchangeEcommerce(iUmiEventPoint $eventPoint)
    {
        Service::Session()->set(self::session_param_send_user_id, true);
    }
}