<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$permissions = Array(
    'ecommerce' => Array(
        'getExchangeEcommerceEmarketPurchaseCode',
        'addExchangeEcommercePromoView',
        'addExchangeEcommerceImpression',
        'addExchangeEcommerceCheckoutCode',
        'getExchangeEcommerceDetailCode',
        'getExchangeEcommerceProductData',
        'getExchangeEcommerceBracketsData',
        'getExchangeEcommerceCode',
        'getExchangeEcommerceCodeBeforeGTM',
    )
);