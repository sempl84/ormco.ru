<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$i18n = array(
    'perms-exchange-ecommerce' => 'Google Ecommerce'
);