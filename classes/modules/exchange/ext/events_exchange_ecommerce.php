<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

new umiEventListener('users_login_successfull', 'exchange', 'onUsersLoginSuccessFullExchangeEcommerce');