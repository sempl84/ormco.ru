<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class ormcoCoupons extends def_module
{
    public function __construct()
    {
        parent::__construct();

        $cmsController = cmsController::getInstance();

        if ($cmsController->getCurrentMode() == "admin") {
            $this->__loadLib("__admin.php");
            $this->__implement("__ormcoCouponsAdmin");
    
            $this->__loadLib("__admin_coupons.php");
            $this->__implement("__ormcoCouponsAdminCoupons");

            $commonTabs = $this->getCommonTabs();
            if ($commonTabs instanceof iAdminModuleTabs) {
                $commonTabs->add('coupons', array(
                    'couponsList', 'viewCoupon', 'couponSendNotification',
                ));
                $commonTabs->add('history', array(
                    'historyList', 'viewHistory'
                ));
            }
        } else {
            $this->autoDetectAttributes();
        }

        $this->__loadLib("__macros.php");
        $this->__implement("OrmcoCouponsMacros");
        
        $this->__loadLib("__import.php");
        $this->__implement("OrmcoCouponsImport");
    }

    public function getObjectEditLink($objectId = false, $method = false)
    {
        switch ($method) {
            case SiteOrmcoCouponsCouponModel::method: {
                $editLink = '/admin/ormcoCoupons/viewCoupon/' . $objectId . '/';
                break;
            }
            case SiteOrmcoCouponsHistoryModel::method: {
                $editLink = '/admin/ormcoCoupons/viewHistory/' . $objectId . '/';
                break;
            }
            default:
                $editLink = false;
        }

        return $editLink;
    }
}