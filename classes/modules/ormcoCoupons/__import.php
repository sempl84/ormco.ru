<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrmcoCouponsImport extends ormcoCoupons
{
    public function ormcoCouponsImportFromInput()
    {
        $json = file_get_contents('php://input');
        
        $logDir = $this->_getImportLogDir();
        SiteLogHelper::clearLogDir($logDir, time() - (86400 * 15));
        if (!is_dir($logDir)) {
            mkdir($logDir, 0775, true);
        }
        
        file_put_contents($logDir . '/' . date('Ymd_His') . '.json', $json);
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->charset('UTF-8');
        $buffer->contentType('application/json');
        $buffer->option('generation-time', false);
        
        try {
            $this->_ormcoCouponsImportData($json);
            $response = array(
                'status' => 'successful',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage()
            );
        }
        
        $buffer->push(json_encode($response));
        $buffer->send();
        exit;
    }
    
    /**
     * @param string $file
     * @return bool
     * @throws coreException
     * @throws publicException
     * @throws selectorException
     */
    public function testOrmcoCouponsImportFromFile($file = '')
    {
        $file = trim($file);
        if (!$file) {
            throw new publicException('Не передано название файла');
        }
        
        $file = CURRENT_WORKING_DIR . '/sys-temp/ormcoCoupons/import/file/' . $file . '.json';
        if (!file_exists($file)) {
            throw new publicException('Не найден файл ' . $file);
        }
        
        return $this->_ormcoCouponsImportData(file_get_contents($file));
    }
    
    /**
     * @param $json
     * @return bool
     * @throws coreException
     * @throws publicException
     * @throws selectorException
     */
    public function _ormcoCouponsImportData($json)
    {
        $data = json_decode($json, true);
        if (!is_array($data)) {
            throw new publicException('Неверный формат данных');
        }
        
        $arErrorRequiredFields = array_diff(array(
            'id', 'name', 'dateBegin', 'dateEnd', 'discountBase', 'discountFocus', 'users', 'focusProducts'
        ), array_keys($data));
        if (count($arErrorRequiredFields)) {
            throw new publicException('Не заполнены обязательные поля ' . implode(', ', $arErrorRequiredFields));
        }
        
        $seminarId = getArrayKey($data, 'id');
        $seminarName = getArrayKey($data, 'name');
        $users = getArrayKey($data, 'users');
        if (!$users) {
            throw new publicException('Не заполнен массив пользователей');
        }
        
        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method);
        
        $sel = new selector('objects');
        $sel->types('object-type')->id($objectTypeId);
        $sel->where(SiteOrmcoCouponsCouponModel::field_seminar_id)->equals($seminarId);
        $sel->order('id')->asc();
        
        $arUserToObjectId = array();
        foreach ($sel as $object) {
            if (!$object instanceof umiObject) {
                continue;
            }
            
            $userId = $object->getValue(SiteOrmcoCouponsCouponModel::field_user_id);
            if (!$userId) {
                $object->delete();
                continue;
            }
            
            $arUserToObjectId[$userId] = $object->getId();
        }
        
        $umiImportRelations = umiImportRelations::getInstance();
        /* @var $umiImportRelations umiImportRelations */
        $sourceId = $umiImportRelations->getSourceId('commerceML2');
        $arFocusPageId = array();
        foreach ($data['focusProducts'] as $pageId) {
            $sitePageId = $umiImportRelations->getNewIdRelation($sourceId, $pageId);
            if (!$sitePageId) {
                continue;
            }
            
            $arFocusPageId[] = intval($sitePageId);
        }
        
        $arFocusCategories = array();
        $rawFocusCategories = getArrayKey($data, 'focusCategories');
        if (is_array($rawFocusCategories)) {
            foreach ($rawFocusCategories as $rawFocusCategory) {
                $arFocusCategory = array();
                
                $focusCategoryName = trim(getArrayKey($rawFocusCategory, 'name'));
                if ($focusCategoryName) {
                    $arFocusCategory[SiteOrmcoCouponsCouponModel::discount_focus_categories_param_name] = $focusCategoryName;
                }
                
                if (!$arFocusCategory) {
                    continue;
                }
                
                $focusCategoryLink = trim(getArrayKey($rawFocusCategory, 'link'));
                if ($focusCategoryLink && $focusCategoryLink != '#') {
                    $arFocusCategory[SiteOrmcoCouponsCouponModel::discount_focus_categories_param_link] = $focusCategoryLink;
                }
                
                $arFocusCategories[] = $arFocusCategory;
            }
        }
        $focusCategoriesString = count($arFocusCategories) ? json_encode($arFocusCategories) : '';
        
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        
        foreach ($users as $user) {
            $commonUid = getArrayKey($user, 'common_uid');
            if (!$commonUid) {
                continue;
            }
            
            $coupon = getArrayKey($user, 'coupon');
            if (!$coupon) {
                continue;
            }
            
            $userId = SiteUsersUserModel::getUserIdByCommonUid($commonUid);
            if (!$userId) {
                continue;
            }
            
            if (isset($arUserToObjectId[$userId])) {
                $object = $umiObjectsCollection->getObject($arUserToObjectId[$userId]);
            } else {
                $object = null;
            }
            
            $bNewCoupon = false;
            
            if (!$object instanceof umiObject) {
                $objectId = $umiObjectsCollection->addObject($coupon, $objectTypeId);
                if (!$objectId) {
                    throw new publicException('Ошибка при создании промокода для пользователя ' . $userId);
                }
                
                $object = $umiObjectsCollection->getObject($objectId);
                if (!$object instanceof umiObject) {
                    throw new publicException('Ошибка при создании промокода для пользователя ' . $userId);
                }
                
                $object->setValue(SiteOrmcoCouponsCouponModel::field_seminar_id, $seminarId);
                $object->setValue(SiteOrmcoCouponsCouponModel::field_user_id, $userId);
                $object->setValue(SiteOrmcoCouponsCouponModel::field_state, SiteOrmcoCouponsCouponStateModel::getObjectIdByCode(SiteOrmcoCouponsCouponStateModel::code_active));
                $bNewCoupon = true;
            }
            
            $object->setName($coupon);
            $object->setValue(SiteOrmcoCouponsCouponModel::field_date_begin, getArrayKey($data, 'dateBegin'));
            $object->setValue(SiteOrmcoCouponsCouponModel::field_date_end, getArrayKey($data, 'dateEnd'));
            $object->setValue(SiteOrmcoCouponsCouponModel::field_code, $coupon);
            $object->setValue(SiteOrmcoCouponsCouponModel::field_discount_base, getArrayKey($data, 'discountBase'));
            $object->setValue(SiteOrmcoCouponsCouponModel::field_discount_focus, getArrayKey($data, 'discountFocus'));
            $object->setValue(SiteOrmcoCouponsCouponModel::field_discount_focus_page_id, $arFocusPageId);
            $object->setValue(SiteOrmcoCouponsCouponModel::field_discount_focus_categories, $focusCategoriesString);
            $object->setValue(SiteOrmcoCouponsCouponModel::field_seminar_name, $seminarName);
            $object->commit();
            
            if ($bNewCoupon) {
                $this->_sendNewCouponNotification($object);
                SiteOrmcoCouponsCouponModel::addLog($object, 'Купон создан');
            } else {
                SiteOrmcoCouponsCouponModel::addLog($object, 'Обновлены данные для купона');
            }
            
            if (isset($arUserToObjectId[$userId])) {
                unset($arUserToObjectId[$userId]);
            }
        }
        
        /* Деактивая промокодов, которые не пришли в выгрузке */
        foreach ($arUserToObjectId as $objectId) {
            $object = $umiObjectsCollection->getObject($objectId);
            if (!$object instanceof umiObject) {
                continue;
            }
            
            $object->setValue(SiteOrmcoCouponsCouponModel::field_state, SiteOrmcoCouponsCouponStateModel::getObjectIdByCode(SiteOrmcoCouponsCouponStateModel::code_inactive));
            $object->commit();
            
            SiteOrmcoCouponsCouponModel::addLog($object, 'Купон деактивирован');
        }
        
        return true;
    }
    
    public function generateOrmcoCouponsImportJson()
    {
        $data = array(
            'id' => 1,
            'name' => 'Название семинара',
            'dateBegin' => date('YmdHis', time()),
            'dateEnd' => date('YmdHis', time() + (3600 * 14)),
            'discountBase' => 5,
            'discountFocus' => 10,
            'users' => array(
                array(
                    'common_uid' => 683,
                    'coupon' => 'coupon1',
                )
            ),
            'focusProducts' => array(
                'cd6980b2-004b-4319-95f5-72fb198b073c',
                'fdc74c90-cfac-4d42-98d2-2536c656083b'
            ),
            'focusCategories' => array(
                array(
                    'name' => 'Раздел 1',
                    'link' => '#'
                ),
                array(
                    'name' => 'Раздел 2',
                ),
                array(
                    'name' => 'Раздел 3',
                )
            )
        );
        
        $buffer = outputBuffer::current();
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->option('generation-time', false);
        $buffer->contentType('application/json');
        $buffer->push(json_encode($data, JSON_UNESCAPED_UNICODE));
        $buffer->end();
    }
    
    public function ormcoCouponsImportHistoryFromInput()
    {
        $json = file_get_contents('php://input');
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->charset('UTF-8');
        $buffer->contentType('application/json');
        $buffer->option('generation-time', false);
        
        try {
            $this->_ormcoCouponsImportHistoryData($json);
            $response = array(
                'status' => 'successful',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage()
            );
        }
        
        $buffer->push(json_encode($response));
        $buffer->send();
        exit;
    }
    
    /**
     * @param $json
     * @return bool
     * @throws coreException
     * @throws publicException
     * @throws selectorException
     */
    public function _ormcoCouponsImportHistoryData($json)
    {
        $data = json_decode($json, true);
        if (!is_array($data)) {
            throw new publicException('Неверный формат данных');
        }
        
        $items = getArrayKey($data, 'items');
        if (!$items) {
            throw new publicException('Не найден ключ items');
        }
        
        foreach ($items as $item) {
            $this->_ormcoCouponsImportHistoryItem($item);
        }
        
        return true;
    }
    
    public function _ormcoCouponsImportHistoryItem($data)
    {
        if (!is_array($data)) {
            throw new publicException('Неверный формат данных');
        }
        
        $arErrorRequiredFields = array_diff(array(
            'id', 'name', 'user_id', 'date'
        ), array_keys($data));
        if (count($arErrorRequiredFields)) {
            throw new publicException('Не заполнены обязательные поля ' . implode(', ', $arErrorRequiredFields));
        }
        
        $oldUserId = getArrayKey($data, 'user_id');
        $userId = SiteUsersUserModel::getUserIdByCommonUid($oldUserId);
        if (!$userId) {
            throw new publicException('Не найден пользователь ' . $oldUserId);
        }
        
        $id = getArrayKey($data, 'id');
        
        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName(SiteOrmcoCouponsHistoryModel::module, SiteOrmcoCouponsHistoryModel::method);
        
        $sel = new selector('objects');
        $sel->types('object-type')->id($objectTypeId);
        $sel->where(SiteOrmcoCouponsHistoryModel::field_user_id)->equals($userId);
        $sel->where(SiteOrmcoCouponsHistoryModel::field_1c_id)->equals($id);
        $sel->order('id')->asc();
        $sel->limit(0, 1);
        
        $object = getArrayKey($sel->result(), 0);
        if (!$object instanceof umiObject) {
            $umiObjectsCollection = umiObjectsCollection::getInstance();
            
            $objectId = $umiObjectsCollection->addObject($id, $objectTypeId);
            if (!$objectId) {
                throw new publicException('Ошибка при создании записи для промокода ' . $id);
            }
            
            $object = $umiObjectsCollection->getObject($objectId);
            if (!$object instanceof umiObject) {
                throw new publicException('Ошибка при создании записи для промокода ' . $id);
            }
            
            $object->setValue(SiteOrmcoCouponsHistoryModel::field_user_id, $userId);
            $object->setValue(SiteOrmcoCouponsHistoryModel::field_1c_id, $id);
        }
        
        $object->setValue(SiteOrmcoCouponsHistoryModel::field_coupon_name, getArrayKey($data, 'name'));
        $object->setValue(SiteOrmcoCouponsHistoryModel::field_date, getArrayKey($data, 'date'));
        $object->setValue(SiteOrmcoCouponsHistoryModel::field_used, getArrayKey($data, 'used') == 1);
        $object->setValue(SiteOrmcoCouponsHistoryModel::field_coupon_percent, getArrayKey($data, 'percent'));
        $object->commit();
        
        return true;
    }
    
    /**
     * @param string $file
     * @return bool
     * @throws coreException
     * @throws publicException
     * @throws selectorException
     */
    public function testOrmcoCouponsImportHistoryFromFile($file = '')
    {
        $file = trim($file);
        if (!$file) {
            throw new publicException('Не передано название файла');
        }
        
        $file = CURRENT_WORKING_DIR . '/sys-temp/ormcoCoupons/import/history/' . $file . '.json';
        if (!file_exists($file)) {
            throw new publicException('Не найден файл ' . $file);
        }
        
        return $this->_ormcoCouponsImportHistoryData(file_get_contents($file));
    }
    
    public function generateOrmcoCouponsImportHistoryJson()
    {
        $items = array();
        $items[] = array(
            'id' => 'Id записи в 1С',
            'user_id' => 683,
            'date' => time(),
            'name' => 'Название промокода',
            'percent' => 5,
            'used' => 1
        );
        $items[] = array(
            'id' => 'Id записи в 1С #2',
            'user_id' => 683,
            'date' => time(),
            'name' => 'Название промокода',
            'percent' => 5,
            'used' => 0
        );
        
        $data = array(
            'items' => $items
        );
        
        $buffer = outputBuffer::current();
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->option('generation-time', false);
        $buffer->contentType('application/json');
        $buffer->push(json_encode($data, JSON_UNESCAPED_UNICODE));
        $buffer->end();
    }
    
    public function _getImportLogDir()
    {
        return CURRENT_WORKING_DIR . '/sys-temp/ormcoCoupons/log/import';
    }
    
    public function ormcoCouponsUpdateCouponStatusFromInput()
    {
        $json = file_get_contents('php://input');
    
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->charset('UTF-8');
        $buffer->contentType('application/json');
        $buffer->option('generation-time', false);
    
        try {
            $data = json_decode($json, true);
            if (!is_array($data)) {
                throw new publicException('Неверный формат данных');
            }
        
            $this->_ormcoCouponsUpdateCouponObjectStatus($data);
            $response = array(
                'status' => 'successful',
            );
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage()
            );
        }
    
        $buffer->push(json_encode($response));
        $buffer->send();
        exit;
    }
    
    public function ormcoCouponsUpdateMultipleCouponStatusFromInput()
    {
        $json = file_get_contents('php://input');
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->charset('UTF-8');
        $buffer->contentType('application/json');
        $buffer->option('generation-time', false);
        
        try {
            $response = $this->_ormcoCouponsUpdateMultipleCouponStatus($json);
        } catch (Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => $e->getMessage()
            );
        }
        
        $buffer->push(json_encode($response));
        $buffer->send();
        exit;
    }
    
    public function _ormcoCouponsUpdateMultipleCouponStatus($json)
    {
        $data = json_decode($json, true);
        if (!is_array($data) || !isset($data['items'])) {
            throw new publicException('Неверный формат данных');
        }
        
        $arSuccessCouponCode = array();
        foreach($data['items'] as $item) {
            try {
                $this->_ormcoCouponsUpdateCouponObjectStatus($item);
                $arSuccessCouponCode[] = $item['coupon_code'];
            } catch (Exception $e) {}
        }
        
        return array(
            'status' => 'successful',
            'coupon_code' => $arSuccessCouponCode
        );
    }
    
    public function _ormcoCouponsUpdateCouponObjectStatus($data)
    {
        $arErrorRequiredFields = array_diff(array(
            'seminar_id', 'user_id', 'coupon_code', 'status'
        ), array_keys($data));
        if (count($arErrorRequiredFields)) {
            throw new publicException('Не заполнены обязательные поля ' . implode(', ', $arErrorRequiredFields));
        }
    
        $oldUserId = $data['user_id'];
        $userId = SiteUsersUserModel::getUserIdByCommonUid($oldUserId);
        if (!$userId) {
            throw new publicException('Не найден пользователь ' . $oldUserId);
        }
        
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method);
        $sel->where(SiteOrmcoCouponsCouponModel::field_seminar_id)->equals($data['seminar_id']);
        $sel->where(SiteOrmcoCouponsCouponModel::field_user_id)->equals($userId);
        $sel->where(SiteOrmcoCouponsCouponModel::field_code)->equals($data['coupon_code']);
        $sel->order('id')->asc();
        $sel->limit(0, 1);
        
        $coupon = getArrayKey($sel->result(), 0);
        if(!$coupon instanceof umiObject) {
            throw new publicException('Не найден купон');
        }
        
        $stateId = SiteOrmcoCouponsCouponStateModel::getObjectIdByCode($data['status']);
        if(!$stateId) {
            throw new publicException('Не найден статус ' . $data['status']);
        }
        
        $coupon->setValue(SiteOrmcoCouponsCouponModel::field_state, $stateId);
        $coupon->commit();
    
        SiteOrmcoCouponsCouponModel::addLog($coupon, 'У купона обновлен статус. Новый статус - ' . $data['status']);
    
        return true;
    }
    
    public function testOrmcoCouponsUpdateCouponStatusFromFile($file = '')
    {
        $file = trim($file);
        if (!$file) {
            throw new publicException('Не передано название файла');
        }
        
        $file = CURRENT_WORKING_DIR . '/sys-temp/ormcoCoupons/update_coupon_status/' . $file . '.json';
        if (!file_exists($file)) {
            throw new publicException('Не найден файл ' . $file);
        }
        
        return $this->_ormcoCouponsUpdateCouponObjectStatus(json_decode(file_get_contents($file), true));
    }
    
    public function generateOrmcoCouponsUpdateCouponStatusJson()
    {
        $data = array(
            'seminar_id' => '1',
            'user_id' => '683',
            'coupon_code' => 'coupon1',
            'status' => 'active'
        );
        
        $buffer = outputBuffer::current();
        /* @var $buffer HTTPOutputBuffer */
        $buffer->clear();
        $buffer->option('generation-time', false);
        $buffer->contentType('application/json');
        $buffer->push(json_encode($data, JSON_UNESCAPED_UNICODE));
        $buffer->end();
    }
}