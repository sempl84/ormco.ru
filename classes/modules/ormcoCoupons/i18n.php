<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$i18n = Array(
    'module-ormcoCoupons' => 'Промокоды',
    
    'header-ormcoCoupons' => 'Промокоды',
    
    'header-ormcoCoupons-coupons' => 'Промокоды',
    'header-ormcoCoupons-couponsList' => 'Список промокодов',
    'label-viewCoupon' => 'Подробная информация',
    
    'header-ormcoCoupons-viewCoupon' => 'Данные промокода',
    
    'header-ormcoCoupons-couponSendNotification' => 'Отправить уведомление',
    
    'header-ormcoCoupons-history' => 'История',
    'header-ormcoCoupons-historyList' => 'История',
    'label-viewHistory' => 'Подробная информация',
    
    'header-ormcoCoupons-viewHistory' => 'Данные истории',
    
    'perms-ormcoCoupons-site' => 'Просмотр данных',
    'perms-ormcoCoupons-admin' => 'Управление модулем',
);