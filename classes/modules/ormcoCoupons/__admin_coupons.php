<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __ormcoCouponsAdminCoupons extends __ormcoCouponsAdmin
{
    public function coupons()
    {
        $this->chooseRedirect('/admin/ormcoCoupons/couponsList/');
    }
    
    public function couponsList()
    {
        $this->setDataType('list');
        $this->setActionType('view');
        
        if (!$this->ifNotXmlMode()) {
            $limit = getRequest('per_page_limit');
            $curr_page = getRequest('p');
            $offset = $curr_page * $limit;
            
            $sel = new selector('objects');
            $sel->types('object-type')->name(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method);
            $sel->limit($offset, $limit);
            selectorHelper::detectFilters($sel);
            if (!getRequest('order_filter')) {
                $sel->order('id')->desc();
            }
            
            $data = $this->prepareData($sel->result(), 'objects');
            
            //Завершаем вывод
            $this->setData($data, $sel->length());
            $this->setDataRangeByPerPage($limit, $curr_page);
        }
        
        $this->doData();
        
        return null;
    }
    
    public function viewCoupon()
    {
        $object = $this->expectObject('param0', true);
        
        $inputData = array(
            'object' => $object,
            'allowed-element-types' => array(SiteOrmcoCouponsCouponModel::method)
        );
        
        $this->setDataType("form");
        $this->setActionType("modify");
        
        $data = $this->prepareData($inputData, 'object');
        
        $this->setData($data);
        $this->doData();
        
        return null;
    }
    
    public function couponSendNotification()
    {
        $coupon = $this->expectObject('param0');
        if(!$coupon instanceof umiObject || $coupon->getMethod() != SiteOrmcoCouponsCouponModel::method) {
            throw new publicException('Не найден промокод');
        }
        
        $email = trim(getRequest('email'));
        if(!$email) {
            throw new publicException('Не передан email');
        }
        
        $this->_sendNewCouponNotification($coupon, $email);
        $this->redirect('/admin/ormcoCoupons/viewCoupon/' . $coupon->getId() . '/');
    }
    
    public function getDatasetConfigurationCouponsList()
    {
        return [
            'methods' => [
                [
                    'title' => getLabel('smc-load'),
                    'forload' => true,
                    'module' => 'ormcoCoupons',
                    '#__name' => 'couponsList'
                ],
            ],
            'types' => [
                [
                    'common' => 'true',
                    'id' => SiteOrmcoCouponsCouponModel::method
                ]
            ],
            'stoplist' => [],
            'default' => 'name[250px]'
        ];
    }
    
    public function history()
    {
        $this->chooseRedirect('/admin/ormcoCoupons/historyList/');
    }
    
    public function historyList()
    {
        $this->setDataType('list');
        $this->setActionType('view');
        
        if (!$this->ifNotXmlMode()) {
            $limit = getRequest('per_page_limit');
            $curr_page = getRequest('p');
            $offset = $curr_page * $limit;
            
            $sel = new selector('objects');
            $sel->types('object-type')->name(SiteOrmcoCouponsHistoryModel::module, SiteOrmcoCouponsHistoryModel::method);
            $sel->limit($offset, $limit);
            selectorHelper::detectFilters($sel);
            if (!getRequest('order_filter')) {
                $sel->order('id')->desc();
            }
            
            $data = $this->prepareData($sel->result(), 'objects');
            
            //Завершаем вывод
            $this->setData($data, $sel->length());
            $this->setDataRangeByPerPage($limit, $curr_page);
        }
        
        $this->doData();
        
        return null;
    }
    
    public function viewHistory()
    {
        $object = $this->expectObject('param0', true);
        
        $inputData = array(
            'object' => $object,
            'allowed-element-types' => array(SiteOrmcoCouponsHistoryModel::method)
        );
        
        $this->setDataType("form");
        $this->setActionType("modify");
        
        $data = $this->prepareData($inputData, 'object');
        
        $this->setData($data);
        $this->doData();
        
        return null;
    }
    
    public function getDatasetConfigurationHistoryList()
    {
        return [
            'methods' => [
                [
                    'title' => getLabel('smc-load'),
                    'forload' => true,
                    'module' => 'ormcoCoupons',
                    '#__name' => 'historyList'
                ],
            ],
            'types' => [
                [
                    'common' => 'true',
                    'id' => SiteOrmcoCouponsHistoryModel::method
                ]
            ],
            'stoplist' => [],
            'default' => 'name[250px]'
        ];
    }
}