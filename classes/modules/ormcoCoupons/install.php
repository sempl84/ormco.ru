<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$INFO = Array();

$INFO['version'] = "1.0";
$INFO['version_line'] = "pro";

$INFO['name'] = "ormcoCoupons";
$INFO['title'] = "Промокоды";
$INFO['filename'] = "modules/ormcoCoupons/class.php";
$INFO['ico'] = "ico_ormcoCoupons";
$INFO['default_method'] = "";
$INFO['default_method_admin'] = "dashboard";

$SQL_INSTALL = Array();

$moduleDir = "./classes/modules/ormcoCoupons";

$COMPONENTS = array(
    $moduleDir . '/__admin.php',
    $moduleDir . '/class.php',
    $moduleDir . '/i18n.php',
    $moduleDir . '/lang.php',
    $moduleDir . '/permissions.php',
);