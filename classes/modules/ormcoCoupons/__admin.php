<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __ormcoCouponsAdmin extends baseModuleAdmin
{
    public function dashboard()
    {
        $this->chooseRedirect('/admin/ormcoCoupons/couponsList/');
    }
    
    public function getDatasetConfiguration($param = '')
    {
        $arParams = array();
        
        if (strpos($param, '|') !== false) {
            $data = explode('|', $param);
            $param = $data[0];
            $arParams[] = $data[1];
        }
        
        $module = $this;
        /* @var $module __ormcoCouponsAdmin|__ormcoCouponsAdminCoupons */
        
        switch ($param) {
            case 'couponsList':
            {
                return $module->getDatasetConfigurationCouponsList();
            }
            case 'historyList':
            {
                return $module->getDatasetConfigurationHistoryList();
            }
        }
        
        throw new publicException('Неизвестный параметр');
    }
}