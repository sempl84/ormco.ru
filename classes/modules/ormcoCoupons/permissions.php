<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$permissions = Array(
    'site' => array(
        'getOrmcoCouponsActiveItems',
        'getOrmcoCouponsMultipleAddToCartContent',
        'getOrmcoCouponsMultipleCartContent',
        'getOrmcoCouponsHistoryUsedItems',
        'getOrmcoCouponsHistoryNotUsedItems',
    ),
    'admin' => array(
        'dashboard'
    ),
);