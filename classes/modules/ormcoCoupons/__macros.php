<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrmcoCouponsMacros extends ormcoCoupons
{
    public function getOrmcoCouponsActiveItems()
    {
        $coupons = SiteOrmcoCouponsCouponModel::getUserActiveCoupons(Service::Auth()->getUserId());
        if (!$coupons) {
            return null;
        }
        
        $items = array();
        foreach ($coupons as $coupon) {
            if (!$coupon instanceof umiObject) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $coupon->getId(),
                'attribute:code' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_code),
                'name' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_seminar_name),
                'discount' => array(
                    'attribute:percent' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_base)
                )
            );
            
            $focusDiscount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus);
            if ($focusDiscount && $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus_page_id)) {
                $focus = array(
                    'attribute:percent' => $focusDiscount
                );
                
                $categories = $this->_renderOrmcoCouponsCouponDiscountFocusCategoriesItems($coupon);
                if ($categories) {
                    $focus['categories'] = array('nodes:category' => $categories);
                }
                
                $item['focus'] = $focus;
            }
            
            $dateEnd = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_date_end);
            if ($dateEnd instanceof umiDate && $dateEnd->getDateTimeStamp()) {
                $daysLeft = SiteOrmcoCouponsCouponModel::getDaysLeft($dateEnd->getDateTimeStamp());
                $item['days-left'] = array(
                    'attribute:string' => $daysLeft . ' ' . SiteStringHelper::getNumEnding($daysLeft, 'день', 'дня', 'дней'),
                    'node:value' => $daysLeft
                );
            }
            
            $items[] = $item;
        }
        
        if (!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    public function getOrmcoCouponsUserActiveItems($userId = null)
    {
        if (!$userId) {
            throw new publicException('Не передан id пользователя');
        }
        
        $coupons = SiteOrmcoCouponsCouponModel::getUserActiveCoupons($userId);
        if (!$coupons) {
            return null;
        }
        
        $items = array();
        foreach ($coupons as $coupon) {
            if (!$coupon instanceof umiObject) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $coupon->getId(),
                'attribute:code' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_code),
                'name' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_seminar_name),
                'discount' => array(
                    'attribute:percent' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_base)
                )
            );
            
            $focusDiscount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus);
            if ($focusDiscount && $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus_page_id)) {
                $focus = array(
                    'attribute:percent' => $focusDiscount
                );
                
                $categories = $this->_renderOrmcoCouponsCouponDiscountFocusCategoriesItems($coupon);
                if ($categories) {
                    $focus['categories'] = array('nodes:category' => $categories);
                }
                
                $item['focus'] = $focus;
            }
            
            $dateEnd = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_date_end);
            if ($dateEnd instanceof umiDate && $dateEnd->getDateTimeStamp()) {
                $daysLeft = SiteOrmcoCouponsCouponModel::getDaysLeft($dateEnd->getDateTimeStamp());
                $item['days-left'] = array(
                    'attribute:string' => $daysLeft . ' ' . SiteStringHelper::getNumEnding($daysLeft, 'день', 'дня', 'дней'),
                    'node:value' => $daysLeft
                );
            }
            
            $items[] = $item;
        }
        
        if (!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    public function getOrmcoCouponsCouponDiscountFocusCategoriesItems($couponId = false)
    {
        if (!$couponId) {
            throw new publicException('Не передан id купона');
        }
        
        $coupon = umiObjectsCollection::getInstance()->getObject($couponId);
        if (!$coupon instanceof umiObject) {
            throw new publicException('Не найден купон ' . $couponId);
        }
        
        $categories = $this->_renderOrmcoCouponsCouponDiscountFocusCategoriesItems($coupon);
        if (!$categories) {
            return null;
        }
        
        return array('categories' => array('nodes:category' => $categories));
    }
    
    public function _renderOrmcoCouponsCouponDiscountFocusCategoriesItems(umiObject $coupon)
    {
        $raw = json_decode($coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus_categories), true);
        if (!is_array($raw)) {
            return false;
        }
        
        $items = array();
        foreach ($raw as $row) {
            $name = getArrayKey($row, SiteOrmcoCouponsCouponModel::discount_focus_categories_param_name);
            if (!$name) {
                continue;
            }
            
            $item = array(
                'node:name' => $name
            );
            
            $link = getArrayKey($row, SiteOrmcoCouponsCouponModel::discount_focus_categories_param_link);
            if ($link) {
                $item['attribute:link'] = $link;
            }
            
            $items[] = $item;
        }
        
        if (!count($items)) {
            return false;
        }
        
        return $items;
    }
    
    public function getOrmcoCouponsMultipleAddToCartContent()
    {
        $coupons = SiteOrmcoCouponsCouponModel::getUserActiveCoupons(Service::Auth()->getUserId());
        if (count($coupons) < 2) {
            return null;
        }
        
        $settingsPage = SiteContentPageSettingsModel::getPage();
        if (!$settingsPage instanceof umiHierarchyElement) {
            return null;
        }
        
        $template = $settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_multiple_add_to_cart_content);
        if (trim(strip_tags($template)) == '') {
            return null;
        }
        
        $couponCode = '{{:code}}';
        
        $tplTemplater = umiTemplater::create('TPL');
        $variables = array(
            'coupon_code' => $couponCode,
        );
        
        return array(
            'content' => $tplTemplater->parse($variables, $template)
        );
    }
    
    public function getOrmcoCouponsMultipleCartContent()
    {
        $coupons = SiteOrmcoCouponsCouponModel::getUserActiveCoupons(Service::Auth()->getUserId());
        if (count($coupons) < 2) {
            return null;
        }
        
        $settingsPage = SiteContentPageSettingsModel::getPage();
        if (!$settingsPage instanceof umiHierarchyElement) {
            return null;
        }
        
        $template = $settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_multiple_cart_content);
        if (trim(strip_tags($template)) == '') {
            return null;
        }
        
        $couponCode = '';
        
        $emarket = cmsController::getInstance()->getModule('emarket');
        if ($emarket instanceof emarket) {
            $order = $emarket->getBasketOrder();
            if ($order instanceof order) {
                $couponCode = $order->getValue(SiteEmarketOrderModel::field_ormco_coupon_code);
            }
        }
        
        $tplTemplater = umiTemplater::create('TPL');
        $variables = array(
            'coupon_code' => $couponCode,
        );
        
        return array(
            'content' => $tplTemplater->parse($variables, $template)
        );
    }
    
    public function getOrmcoCouponsHistoryUsedItems()
    {
        return $this->_renderOrmcoCouponsHistoryItems(true);
    }
    
    public function getOrmcoCouponsHistoryNotUsedItems()
    {
        return $this->_renderOrmcoCouponsHistoryItems(false);
    }
    
    public function _renderOrmcoCouponsHistoryItems($used)
    {
        $limit = 5;
        $numpagesParam = $used ? 'p_used' : 'p_not_used';
        $offset = intval(getRequest($numpagesParam));
        
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteOrmcoCouponsHistoryModel::module, SiteOrmcoCouponsHistoryModel::method);
        $sel->where(SiteOrmcoCouponsHistoryModel::field_user_id)->equals(Service::Auth()->getUserId());
        $sel->where(SiteOrmcoCouponsHistoryModel::field_used)->equals($used);
        $sel->where(SiteOrmcoCouponsHistoryModel::field_date)->more(0);
        $sel->order(SiteOrmcoCouponsHistoryModel::field_date)->desc();
        $sel->limit($offset * $limit, $limit);
        
        $result = $sel->result();
        if (!$result) {
            return null;
        }
        
        $total = $sel->length();
        
        $items = array();
        foreach ($result as $object) {
            if (!$object instanceof umiObject) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $object->getId(),
                'attribute:date' => $object->getValue(SiteOrmcoCouponsHistoryModel::field_date)->getDateTimeStamp(),
                'attribute:used' => $object->getValue(SiteOrmcoCouponsHistoryModel::field_used),
                'node:name' => $object->getValue(SiteOrmcoCouponsHistoryModel::field_coupon_name)
            );
            
            $percent = $object->getValue(SiteOrmcoCouponsHistoryModel::field_coupon_percent);
            if ($percent) {
                $item['attribute:percent'] = $percent;
            }
            
            $items[] = $item;
        }
        
        if (!count($items)) {
            return null;
        }
        
        return array(
            'attribute:numpages-param' => $numpagesParam,
            'items' => array('nodes:item' => $items),
            'total' => $total,
            'per_page' => $limit
        );
    }
    
    public function _sendNewCouponNotification(umiObject $coupon, $forceEmail = '')
    {
        $settingsPage = umiHierarchy::getInstance()->getElement(236);
        if (!$settingsPage instanceof umiHierarchyElement) {
            return false;
        }
        
        $userId = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_user_id);
        if (!$userId) {
            return false;
        }
        
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject) {
            return false;
        }
        
        $forceEmail = trim($forceEmail);
        if (!$forceEmail) {
            $email = $user->getValue(SiteUsersUserModel::field_email);
            $arDebugEmails = explode(PHP_EOL, trim($settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_notification_debug_emails)));
            if (is_array($arDebugEmails) && count($arDebugEmails) && !in_array($email, $arDebugEmails)) {
                $email = '';
            }
        } else {
            $email = $forceEmail;
        }
        
        if (!umiMail::checkEmail($email)) {
            return false;
        }
        
        $senderEmail = trim($settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_notification_sender_email));
        if (!$senderEmail) {
            return false;
        }
        
        $content = $this->_generateNewCouponNotificationContent($coupon, $user);
        if (!$content) {
            return false;
        }
        
        $mail = new umiMail();
        $mail->addRecipient($forceEmail, $user->getValue(SiteUsersUserModel::field_surname) . ' ' . $user->getValue(SiteUsersUserModel::field_name) . ' ' . $user->getValue(SiteUsersUserModel::field_father_name));
        $mail->setFrom($senderEmail, trim($settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_notification_sender_name)));
        $mail->setSubject(trim($settingsPage->getValue(SiteContentPageSettingsModel::field_coupons_notification_subject)));
        $mail->setContent($content);
        $mail->commit();
        $mail->send();
        
        return true;
    }
    
    public function _generateNewCouponNotificationContent(umiObject $coupon, umiObject $user = null)
    {
        if (!$user instanceof umiObject) {
            $userId = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_user_id);
            if (!$userId) {
                return false;
            }
            
            $user = umiObjectsCollection::getInstance()->getObject($userId);
            if (!$user instanceof umiObject) {
                return false;
            }
        }
        
        $arFocusCategoryName = array();
        $arFocusCategories = json_decode(trim($coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus_categories)), true);
        if (is_array($arFocusCategories) && count($arFocusCategories)) {
            foreach ($arFocusCategories as $focusCategory) {
                if (!is_array($focusCategory) || !isset($focusCategory[SiteOrmcoCouponsCouponModel::discount_focus_categories_param_name])) {
                    continue;
                }
                $arFocusCategoryName[] = $focusCategory[SiteOrmcoCouponsCouponModel::discount_focus_categories_param_name];
            }
        }
        
        $variables = array(
            'user_name' => $user->getValue(SiteUsersUserModel::field_name),
            'user_father_name' => $user->getValue(SiteUsersUserModel::field_father_name),
            'seminar' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_seminar_name),
            'focus_category_name' => implode(', ', $arFocusCategoryName),
            'coupon_code' => $coupon->getValue(SiteOrmcoCouponsCouponModel::field_code)
        );
        
        list($templateLine) = def_module::loadTemplatesForMail("emarket/default", "new_coupon_notification");
        $result = def_module::parseTemplateForMail($templateLine, $variables);
        
        return $result;
    }
    
    public function testSendNewCouponNotification($couponId = false)
    {
        if (!$couponId) {
            throw new publicException('Не передан id промокода');
        }
        
        $coupon = umiObjectsCollection::getInstance()->getObject($couponId);
        if (!$coupon instanceof umiObject) {
            throw new publicException('Не найден промокод ' . $couponId);
        }
        
        return $this->_sendNewCouponNotification($coupon, 'aghigay@gmail.com');
//        return $this->_sendNewCouponNotification($coupon, 'e-ioffe@yandex.ru');
    }
    
    public function testGenerateNewCouponNotificationContent($couponId = false)
    {
        if (!$couponId) {
            throw new publicException('Не передан id промокода');
        }
        
        $coupon = umiObjectsCollection::getInstance()->getObject($couponId);
        if (!$coupon instanceof umiObject) {
            throw new publicException('Не найден промокод ' . $couponId);
        }
        
        $content = $this->_generateNewCouponNotificationContent($coupon);
        
        return $content ? $content : 'Произошла ошибка';
    }
    
    public function renderOrmcoCouponsLog($objectId = false)
    {
        if (!$objectId) {
            throw new publicException('Не передан id купона');
        }
        
        $object = umiObjectsCollection::getInstance()->getObject($objectId);
        if (!$object instanceof umiObject) {
            throw new publicException('Не найден купон ' . $objectId);
        }
        
        return nl2br(trim($object->getValue(SiteOrmcoCouponsCouponModel::field_log)));
    }
    
    public function testCalculateCouponDiscountPrice($orderItemId = null)
    {
        if(!$orderItemId) {
            throw new publicException('Не передан id наименования заказа');
        }
        
        $orderItem = umiObjectsCollection::getInstance()->getObject($orderItemId);
        if(!$orderItem instanceof umiObject) {
            throw new publicException('Не найдено объект наименования заказа ' . $orderItemId);
        }
        
        $couponDiscountObjectId = $orderItem->getValue(SiteEmarketOrderItemModel::field_ormco_coupon_id);
        if(!$couponDiscountObjectId) {
            throw new publicException('Не заполнено поле ' . SiteEmarketOrderItemModel::field_ormco_coupon_id);
        }
        
        $couponDiscountObject = umiObjectsCollection::getInstance()->getObject($couponDiscountObjectId);
        if(!$couponDiscountObject instanceof umiObject) {
            throw new publicException('Не найден купон ' . $couponDiscountObjectId);
        }
        
        $element = getArrayKey($orderItem->getValue('item_link'), 0);
        if(!$element instanceof umiHierarchyElement) {
            throw new publicException('Не найден товар');
        }
        
        return SiteOrmcoCouponsCouponModel::calculateCouponDiscountPrice($couponDiscountObject, $element->getId(), $orderItem->getValue('item_price'), 0);
    }
}