<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-doclocator-lists" => "Группы и страницы",
"header-doclocator-config" => "Настройки модуля",
"header-doclocator-add" => "Добавление",
"header-doclocator-edit" => "Редактирование",
 
"header-doclocator-add-groupelements"   => "Добавление группы",
"header-doclocator-add-item_element"     => "Добавление страницы",
 
"header-doclocator-edit-groupelements"   => "Редактирование группы",
"header-doclocator-edit-item_element"     => "Редактирование страницы",
 
"header-doclocator-activity" => "Изменение активности",
 
'perms-doclocator-view' => 'Просмотр страниц',
'perms-doclocator-lists' => 'Управление страницами',

'module-doclocator' => 'Доктор локатор',
 
);
 
?>