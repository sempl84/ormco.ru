<?php

$INFO = Array();

$INFO['name'] = "doclocator";
$INFO['filename'] = "modules/doclocator/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/doclocator/__admin.php";
$COMPONENTS[1] = "./classes/modules/doclocator/class.php";
$COMPONENTS[2] = "./classes/modules/doclocator/i18n.php";
$COMPONENTS[3] = "./classes/modules/doclocator/lang.php";
$COMPONENTS[4] = "./classes/modules/doclocator/permissions.php";
?>