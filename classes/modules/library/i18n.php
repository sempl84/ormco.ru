<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-library-lists" => "Группы и страницы",
"header-library-config" => "Настройки модуля",
"header-library-add" => "Добавление",
"header-library-edit" => "Редактирование",
 
"header-library-add-groupelements"   => "Добавление группы",
"header-library-add-item_element"     => "Добавление страницы",
 
"header-library-edit-groupelements"   => "Редактирование группы",
"header-library-edit-item_element"     => "Редактирование страницы",
 
"header-library-activity" => "Изменение активности",
 
'perms-library-view' => 'Просмотр страниц',
'perms-library-lists' => 'Управление страницами',

'module-library' => 'Библиотека',
 
);
 
?>