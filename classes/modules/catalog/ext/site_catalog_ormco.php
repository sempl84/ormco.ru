<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class site_catalog_ormco
{
    /**
     * @var $module content
     */
    public $module;
    
    /**
     * @param null $pageId
     * @return array[][]|null
     * @throws publicException
     */
    public function getCatalogIndexCategories($pageId = null)
    {
        $page = $this->_getIndexPage($pageId);
    
        $categoryHierarchyTypeId = umiTypesHelper::getInstance()->getHierarchyTypeIdByName(SiteCatalogCategoryModel::module, SiteCatalogCategoryModel::method);
        if(!$categoryHierarchyTypeId) {
            throw new publicException('Не найден тип данных ' . SiteCatalogCategoryModel::method);
        }
        
        $categories = $page->getValue(SiteContentPageIndexModel::field_categories);
        if(!$categories) {
            return null;
        }
        
        $umiLinksHelper = umiLinksHelper::getInstance();
        
        $items = array();
        foreach($categories as $category) {
            if(!$category instanceof umiHierarchyElement) {
                continue;
            }
            
            if(!$category->getIsActive()  || $category->getTypeId() != $categoryHierarchyTypeId) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $category->getId(),
                'attribute:link' => $umiLinksHelper->getLink($category),
                'node:name' => $category->getName()
            );
            
            $image = $category->getValue('menu_pic_a');
            if(!$image instanceof umiImageFile || $image->getIsBroken()) {
                $image = $category->getValue('header_pic');
            }
            
            if($image instanceof umiImageFile && !$image->getIsBroken()) {
                $item['image'] = $image->getFilePath(true);
            }
            
            $items[] = $item;
        }
        
        if(!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    /**
     * @param null $pageId
     * @return array[][]|null
     * @throws publicException
     */
    public function getCatalogIndexProducts($pageId = null)
    {
        $page = $this->_getIndexPage($pageId);
        
        $productHierarchyTypeId = umiTypesHelper::getInstance()->getHierarchyTypeIdByName(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
        if(!$productHierarchyTypeId) {
            throw new publicException('Не найден тип данных ' . SiteCatalogCategoryModel::method);
        }
        
        $products = $page->getValue(SiteContentPageIndexModel::field_products);
        if(!$products) {
            return null;
        }
        
        $umiLinksHelper = umiLinksHelper::getInstance();
        
        $items = array();
        foreach($products as $product) {
            if(!$product instanceof umiHierarchyElement) {
                continue;
            }
            
            if(!$product->getIsActive()  || $product->getTypeId() != $productHierarchyTypeId) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $product->getId(),
                'attribute:link' => $umiLinksHelper->getLink($product),
            );
            
            $name = trim($product->getValue('h1_alternative'));
            if(!$name) {
                $name = $product->getName();
            }
            
            $item['node:name'] = $name;
            
            $article = trim($product->getValue(SiteCatalogObjectModel::field_article));
            if($article) {
                $item['article'] = $article;
            }
            
            $photo = $product->getValue('menu_pic_a');
            if(!$photo instanceof umiImageFile || $photo->getIsBroken()) {
                $photo = $product->getValue(SiteCatalogObjectModel::field_photo);
            }
            
            if($photo instanceof umiImageFile && !$photo->getIsBroken()) {
                $item['photo'] = $photo->getFilePath(true);
            }
            
            $items[] = $item;
        }
        
        if(!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    /**
     * @param null $pageId
     * @return iUmiHierarchyElement|umiHierarchyElement
     * @throws publicException
     */
    public function _getIndexPage($pageId = null)
    {
        if(!$pageId) {
            throw new publicException('Не передан id страницы');
        }
        
        $page = umiHierarchy::getInstance()->getElement($pageId);
        if(!$page instanceof umiHierarchyElement || $page->getObjectTypeId() != SiteContentPageIndexModel::object_type_id) {
            throw new publicException('Не найдена страница ' . $pageId);
        }
        
        return $page;
    }
}