<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$permissions = array(
    'view' => array(
        'getCatalogIndexCategories',
        'getCatalogIndexProducts',
    )
);