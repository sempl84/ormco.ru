<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

new umiEventListener('systemCreateElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemModifyElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemSwitchElementActivity', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemDeleteElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemMoveElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemVirtualCopyElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemKillElement', 'catalog', 'onCatalogCategoryTreeElement');
new umiEventListener('systemRestoreElement', 'catalog', 'onCatalogCategoryTreeElement');

new umiEventListener('exchangeOnAddElement', 'catalog', 'onCatalogCategoryTreeExchangeElement');
new umiEventListener('exchangeOnUpdateElement', 'catalog', 'onCatalogCategoryTreeExchangeElement');