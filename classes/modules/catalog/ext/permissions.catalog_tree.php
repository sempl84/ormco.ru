<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

$permissions = Array(
    'view' => array(
        'getCategoryTree'
    ),
    'tree' => Array(
        'onCatalogCategoryTreeElement'
    ),
);