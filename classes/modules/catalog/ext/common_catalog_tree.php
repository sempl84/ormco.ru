<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class common_catalog_tree
{
    /**
     * @var $module catalog
     */
    public $module;

    public $arTree = array();
    public $arParents = array();
    public $arCategories = array();
    public $level = 0;
    public $activeParentId;
    
    public function getCategoryTree($basePageId = 0, $level = false, $currentPageId = false)
    {
        return $this->_getCategoryTreeItems($basePageId, $level, $currentPageId);
    }

    public function _getCategoryTreeItems($basePageId = 0, $level = false, $currentPageId = false)
    {
        $cacheFile = $this->_getCategoryTreeCacheFile();
        $data = array();

        $bUpdateCache = false;

        if (permissionsCollection::getInstance()->isSv() && getRequest('update_tree_cache')) {
            $bUpdateCache = true;
        } elseif (!file_exists($cacheFile) || (filemtime($cacheFile) < time() - 86400)) {
            $bUpdateCache = true;
        }

        if ($bUpdateCache === false) {
            $data = json_decode(file_get_contents($cacheFile), true);
        }

        if (!isset($data['tree']) || !isset($data['categories']) || !isset($data['parents']) || !isset($data['roots'])) {
            $data = $this->_prepareCategoryTreeData();

            file_put_contents($cacheFile, json_encode($data));
        }

        $this->arTree = $data['tree'];
        $this->arParents = $data['parents'];
        $this->arCategories = $data['categories'];
        $this->level = 0;
        $this->activeParentId = null;

        if ($currentPageId && isset($this->arCategories[$currentPageId])) {
            $this->arCategories[$currentPageId]['attribute:status'] = 'active';
        }

        $level = intval($level);

        $return = array();

        if ($basePageId) {
            $arRoots = getArrayKey($this->arTree, $basePageId);
        } else {
            $arRoots = $data['roots'];
        }

        if ($arRoots) {
            foreach ($arRoots as $rootId) {
                $return[] = $this->_renderCategoryTreeItem($rootId, $level);
            }
        }

        if (!$return) {
            return null;
        }

        return array(
            'items' => array(
                'nodes:item' => $return
            )
        );
    }

    public function _prepareCategoryTreeData()
    {
        $basePageId = 4;

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'category');
        $sel->where('hierarchy')->page($basePageId)->level(10);
        $sel->where('is_active')->equals(1);
        $sel->order('ord')->asc();

        $tree = array();
        $categories = array();
        $parents = array();
        $roots = array();

        $hierarchy = umiHierarchy::getInstance();
        $linksHelper = umiLinksHelper::getInstance();

        foreach ($sel as $category) {
            if (!$category instanceof umiHierarchyElement) {
                continue;
            }

            $parentId = $category->getParentId();

            $categoryId = $category->getId();

            if ($parentId == $basePageId) {
                $roots[] = $categoryId;
            }

            if (!isset($tree[$parentId])) {
                $tree[$parentId] = array();
            }

            $tree[$parentId][] = $categoryId;
            $categories[$categoryId] = $this->_prepareCategoryTreeItem($category, $linksHelper->getLinkByParts($category));
            $parents[$categoryId] = $parentId;

            $hierarchy->unloadElement($categoryId);
        }

        return array(
            'tree' => $tree,
            'categories' => $categories,
            'parents' => $parents,
            'roots' => $roots,
        );
    }

    public function _prepareCategoryTreeItem(umiHierarchyElement $category, $link)
    {
        return array(
            'attribute:id' => $category->getId(),
            'attribute:name' => $category->getName(),
            'attribute:link' => $link,
        );
    }
    
    public function _renderCategoryTreeItem($id, $level)
    {
        if (!isset($this->arCategories[$id])) {
            return false;
        }

        if ($level) {
            if ($this->level >= $level) {
                return false;
            }
        }

        $this->level++;

        $items = array();
        $line = $this->arCategories[$id];

        if (isset($this->arTree[$id]) && count($this->arTree[$id]) > 0) {
            foreach ($this->arTree[$id] as $childId) {
                $child = $this->_renderCategoryTreeItem($childId, $level);

                if ($child) {
                    $items[] = $child;
                }
            }
        }

        if (count($items) > 0) {
            $line['items'] = array(
                'nodes:item' => $items
            );
        }

        if (getArrayKey($line, 'attribute:status') == 'active') {
            $this->activeParentId = $this->arParents[$id];
        } elseif ($this->activeParentId == $id) {
            $line['attribute:status'] = 'active';
            $this->activeParentId = $this->arParents[$id];
        }

        $this->level--;
        return $line;
    }

    public function onCatalogCategoryTreeElement(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return false;
        }

        $element = $eventPoint->getRef('element');

        if ($element instanceof umiHierarchyElement && $element->getMethod() == 'category') {
            $this->_flushCategoryTreeCache($element->getDomainId(), $element->getLangId());
        }

        return true;
    }

    public function _flushCategoryTreeCache($domainId = false, $langId = false)
    {
        $file = $this->_getCategoryTreeCacheFile($domainId, $langId);

        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function _getCategoryTreeCacheFile($domainId = false, $langId = false)
    {
        if (!$domainId) {
            $domainId = cmsController::getInstance()->getCurrentDomain()->getId();
        }

        if (!$langId) {
            $langId = cmsController::getInstance()->getCurrentLang()->getId();
        }

        $file = CURRENT_WORKING_DIR . '/sys-temp/ormco/catalog/category_tree/' . $domainId . '_' . $langId . '.json';

        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0777, true);
        }

        return $file;
    }

    public function onCatalogCategoryTreeExchangeElement(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return false;
        }

        $element = $eventPoint->getRef('element');
        if(!$element instanceof umiHierarchyElement || $element->getMethod() != 'category') {
            return false;
        }

        $this->_flushCategoryTreeCache($element->getDomainId(), $element->getLangId());

        return true;
    }
}