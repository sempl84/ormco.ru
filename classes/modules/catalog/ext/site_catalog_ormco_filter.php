<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class site_catalog_ormco_filter extends catalog
{
    public function getCatalogOrmcoSmartFilters($template = 'default', $categoryId, $isAdaptive = true, $level, $typeId = false, $groupNames = false)
    {
        $module = $this;
        /* @var $module catalog|__filter_catalog */
        $result = $module->getSmartFilters($template, $categoryId, $isAdaptive, $level, $typeId, $groupNames);

        $groups = getArrayKey($result, 'nodes:group');
        if ($groups) {
            foreach ($groups as $groupIndex => $group) {
                $fields = getArrayKey($group, 'nodes:field');
                if (!$fields) {
                    continue;
                }

                foreach ($fields as $fieldIndex => $field) {
                    if(!$this->_isFilterFieldAllowed($field)) {
                        unset($fields[$fieldIndex]);
                    }
                }

                if (count($fields)) {
                    $result['nodes:group'][$groupIndex]['nodes:field'] = $fields;
                } else {
                    unset($result['nodes:group'][$groupIndex]);
                }
            }
        }

        return $result;
    }

    public function _isFilterFieldAllowed($field)
    {
        if(getArrayKey($field, 'attribute:name') == 'price') {
            return false;
        }

        $dataType = getArrayKey($field, 'attribute:data-type');

        if($dataType == 'boolean') {
            $items = getArrayKey($field, 'nodes:item');
            foreach($items as $item) {
                if(getArrayKey($item, 'node:value') == '0') {
                    return false;
                }
            }
        }

        if($dataType == 'relation') {
            $items = getArrayKey($field, 'nodes:item');

            if(is_array($items) && count($items) == 1) {
                return false;
            }
        }

        return true;
    }
}