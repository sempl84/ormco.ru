<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class site_catalog_ormco_braces extends catalog
{
    public function bracket($categoryId = null)
    {
        if (!$categoryId) {
            $categoryId = cmsController::getInstance()->getCurrentElementId();
        }
        
        if (!$categoryId) {
            throw new publicException('Не передан id раздела');
        }
        
        $category = umiHierarchy::getInstance()->getElement($categoryId);
        if (!$category instanceof umiHierarchyElement || $category->getMethod() != SiteCatalogCategoryModel::method) {
            throw new publicException('Не найден раздел ' . $categoryId);
        }
        
        $p18 = 5766;
        $p22 = 5816;
        
        $dataModule = cmsController::getInstance()->getModule('data');
        $userId = Service::Auth()->getUserId();
        
        $cacheFileName = 'bracket_' . $categoryId . '_' . $userId . '.txt';
        
        if(!isset($_REQUEST['update_bracket_cache'])) {
            $result = $dataModule->try_load_from_cache(
                'catalog',
                'bracket',
                $cacheFileName,
                1800
            ); // кеширование 0,5 часа
            if ($result !== null) {
                return $result;
            }
        }
        
        $result = array(
            'nodes:block' => array(
                $this->_bracketPaz($category, $p18),
                $this->_bracketPaz($category, $p22)
            )
        );
        
        $dataModule->save_to_cache(
            'catalog',
            'bracket',
            $cacheFileName,
            $result
        );
        
        return $result;
    }
    
    public function _bracketPaz(umiHierarchyElement $category, $pazObjectId)
    {
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        $pazObject = $umiObjectsCollection->getObject($pazObjectId);
        if (!$pazObject instanceof umiObject) {
            throw new publicException('Не найден объект паза ' . $pazObjectId);
        }
        
        $pazName = $pazObject->getName();
        
        $categoryId = $category->getId();
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->id($this->getProductHierarchyTypeId());
        $sel->where('hierarchy')->page($categoryId)->level(1);
        $sel->where('nomer_zuba')->isnull(false);
        $sel->where('common_quantity')->eqmore(1);
        $sel->where('paz')->equals($pazObjectId);
        $total = $sel->length();
        
        $umiLinksHelper = umiLinksHelper::getInstance();
        
        cmsController::getInstance()->getModule('emarket');
    
        $arTotalOriginalPrice = array();
        $arTotalActualPrice = array();
        $arTotalUserDiscount = array();
        $arTotalCouponDiscount = array();
        
        $items = array();
        foreach ($sel as $element) {
            if(!$element instanceof umiHierarchyElement) {
                continue;
            }
    
            $elementId = $element->getId();
            $name = $element->getName();
            $altName = $element->getAltName();
            $link = $umiLinksHelper->getLinkByParts($element);
            $artikul = $element->getValue('artikul');
    
            $arNomerZuba = $element->getValue('nomer_zuba');
            
            $torkObjectId = $element->getValue('tork');
            if (!$torkObjectId) {
                $torkObjectId = '0';
            }
            
            $torkName = str_replace(array(5970, 5882, 5962, 1374133, 1759782, 2918879, 2957376, 2957377), array('minus', 'st', 'plus', 'plusplus', 'top', 'super', 'supertop', 'stminus'), $torkObjectId);
            if(!$torkName) {
                $torkName = 'st';
            }
            
            $torkNameOption = str_replace(array(5970, 5882, 5962, 1374133, 1759782, 2918879, 2957376, 2957377), array('-', 'ст.', '+', '++', 'верхние', 'супер торк', '++', 'ст\-'), $torkObjectId);
            if (!$torkNameOption) {
                $torkNameOption = 'ст.';
            }
            
            $nalichieKryuchka = $element->getValue('nalichie_kryuchka');
            $nalichieKryuchkaName = ($nalichieKryuchka == 1) ? 'hk' : '';
            
            $smewenie = $element->getValue('smewenie_breketa_k_desne');
            $smewenieName = ($smewenie == 1) ? 'go' : '';
            
            $originalPrice = $element->getValue('price');
            $userDiscount = 0;
            $couponDiscount = 0;
    
            $itemDiscount = itemDiscount::search($element);
            if ($itemDiscount instanceof itemDiscount) {
                $actualPrice = $itemDiscount->recalcPrice($originalPrice);
                $userDiscount = $originalPrice - $actualPrice;
            } else {
                $actualPrice = $originalPrice;
            }
    
            $couponDiscountObject = SiteOrmcoCouponsCouponModel::getUserCouponByPageId($elementId);
            if ($couponDiscountObject instanceof umiObject) {
                $couponDiscountPrice = SiteOrmcoCouponsCouponModel::calculateCouponDiscountPrice($couponDiscountObject, $elementId, $actualPrice, $originalPrice);
                if ($couponDiscountPrice != $actualPrice) {
                    $couponDiscount = $actualPrice - $couponDiscountPrice;
                    $actualPrice = $couponDiscountPrice;
                }
            }
            
            $baseItem = array(
                'attribute:id' => $elementId,
                'attribute:alt_name' => $altName,
                'attribute:link' => $link,
                'artikul' => $artikul,
                'price' => $actualPrice,
                'price_origin' => $originalPrice,
                'nomer_zuba' => '',
                'paz' => array(
                    'attribute:id' => $pazObjectId,
                    'attribute:name' => $pazName
                ),
                'hk' => $nalichieKryuchka ? 1 : 0,
                'go' => $smewenie ? 1 : 0,
                'tork' => array(
                    'attribute:id' => $torkObjectId,
                    'attribute:name' => $torkNameOption
                ),
                'xlink:href' => 'upage://' . $elementId,
                'node:text' => $name
            );
    
            foreach ($arNomerZuba as $nomerZubaObjectId) {
                $nomerZubaObject = $umiObjectsCollection->getObject($nomerZubaObjectId);
                if(!$nomerZubaObject instanceof umiObject) {
                    continue;
                }
                
                $nomerZuba = $nomerZubaObject->getName();
                
                $item = $baseItem;
                $item['nomer_zuba'] = $nomerZuba;
                
                $hkGo = $nalichieKryuchkaName . $smewenieName;
                $hkGo = ($hkGo != '') ? $hkGo : "def";
                
                $items[$nomerZuba][$hkGo][$torkName] = $item;
                $items[$nomerZuba][$hkGo]['attribute:name'] = $hkGo;
                $items[$nomerZuba]['attribute:tooth'] = $nomerZuba;
                
                $arTotalOriginalPrice[$nomerZuba] = $originalPrice;
                $arTotalActualPrice[$nomerZuba] = $actualPrice;
                
                if($userDiscount) {
                    $arTotalUserDiscount[$nomerZuba] = $userDiscount;
                }
                if($couponDiscount) {
                    $arTotalCouponDiscount[$nomerZuba] = $couponDiscount;
                }
            }
        }
        
        $arComplectPageId = array();
        if($pazObjectId == 5816) {
            $arComplect = $category->getValue('set_022');
        } else {
            $arComplect = $category->getValue('set_018');
        }
        
        foreach ($arComplect as $complectPage) {
            if(!$complectPage instanceof umiHierarchyElement) {
                continue;
            }
            
            $arComplectPageId[] = $complectPage->getId();
        }
    
        $selComplect = new selector('pages');
        $selComplect->types('hierarchy-type')->id($this->getProductHierarchyTypeId());
        
        if (sizeof($arComplectPageId) > 0) {
            $selComplect->where('id')->equals($arComplectPageId);
        } else {
            $selComplect->where('hierarchy')->page($categoryId)->level(1);
            $selComplect->where('nomer_zuba')->isnull(true);
            $selComplect->where('paz')->equals($pazObjectId);
        }
        
        $complects = array();
        
        foreach ($selComplect as $element) {
            if(!$element instanceof umiHierarchyElement) {
                continue;
            }
            
            $item = array(
                'attribute:id' => $element->getId(),
                'attribute:alt_name' => $element->getAltName(),
                'attribute:link' => $umiLinksHelper->getLinkByParts($element),
                'node:text' => $element->getName()
            );
            
            $complects[] = $item;
        }
    
        $result = array(
            'attribute:id' => $pazObjectId,
            'attribute:name' => $pazName,
            'nodes:complect' => $complects,
            'nodes:item' => $items,
            'paz' => array(
                'attribute:id' => $pazObjectId,
                'attribute:name' => $pazName
            ),
            'total' => $total,
            'items_count' => count($items),
            'category_id' => $categoryId
        );
        
        $totalActualPrice = array_sum($arTotalActualPrice);
        $totalOriginalPrice = array_sum($arTotalOriginalPrice);
    
        $arPrice = array(
            'actual' => $totalActualPrice
        );
    
        if ($totalOriginalPrice != $totalActualPrice) {
            $arPrice['original'] = $totalOriginalPrice;
        }
        
        $result['price'] = $arPrice;
    
        if (count($arTotalUserDiscount)) {
            $totalUserDiscount = array_sum($arTotalUserDiscount);
            $result['user_discount'] = array(
                'attribute:percent' => SiteEmarketHelper::calculateDiscountPercent($totalOriginalPrice - $totalUserDiscount, $totalOriginalPrice),
                'amount' => $totalUserDiscount
            );
        }
    
        if ($arTotalCouponDiscount) {
            $totalCouponDiscount = array_sum($arTotalCouponDiscount);
            $result['coupon_discount'] = array(
                'attribute:percent' => SiteEmarketHelper::calculateDiscountPercent($totalOriginalPrice - $totalCouponDiscount, $totalOriginalPrice),
                'amount' => $totalCouponDiscount
            );
        }

        return $this->parseTemplate('', $result);
    }
}