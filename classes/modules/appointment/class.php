<?php
	/**
	 * Заглушка модуля "Онлайн-запись".
	 * Сам модуль работает только в новом режиме работы модулей.
	 */
	class appointment extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$commonTabs = $this->getCommonTabs();

				if ($commonTabs instanceof iAdminModuleTabs) {
					$commonTabs->add('orders');
				}
			}
		}

		/**
		 * Возвращает ссылку на редактирование страницы с данными для виджета записи
		 * @param int $elementId идентификатор страницы
		 * @return array
		 */
		public function getEditLink($elementId) {
			return [
				false,
				false
			];
		}

		/**
		 * Заглушка метода административной панели по умолчанию
		 * @throws publicAdminException
		 */
		public function orders() {
			throw new publicAdminException(getLabel('use-compatible-modules', 'appointment'));
		}
	};
?>
