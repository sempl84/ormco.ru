<?php
	/**
	 * Языковые константы для русской версии
	 */
	$i18n = [
		'header-appointment-orders' => 'Заявки на запись',
		"module-appointment"	=> "Онлайн-запись",
		"use-compatible-modules" => "Модуль работает только в <a target=\"_blank\" href=\"http://dev.docs.umi-cms.ru/nastrojka_sistemy/dostupnye_sekcii/sekciya_system/#sel=103:1,103:3\">новом режиме работы модулей</a>"
	];
?>
