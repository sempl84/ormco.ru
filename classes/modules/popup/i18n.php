<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-popup-lists" => "Группы и страницы",
"header-popup-config" => "Настройки модуля",
"header-popup-add" => "Добавление",
"header-popup-edit" => "Редактирование",
 
"header-popup-add-groupelements"   => "Добавление группы",
"header-popup-add-item_element"     => "Добавление страницы",
 
"header-popup-edit-groupelements"   => "Редактирование группы",
"header-popup-edit-item_element"     => "Редактирование страницы",
 
"header-popup-activity" => "Изменение активности",
 
'perms-popup-view' => 'Просмотр страниц',
'perms-popup-lists' => 'Управление страницами',

'module-popup' => 'Поп-ап',
 
);
 
?>