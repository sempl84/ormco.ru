<?php

$INFO = Array();

$INFO['name'] = "popup";
$INFO['filename'] = "modules/popup/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/popup/__admin.php";
$COMPONENTS[1] = "./classes/modules/popup/class.php";
$COMPONENTS[2] = "./classes/modules/popup/i18n.php";
$COMPONENTS[3] = "./classes/modules/popup/lang.php";
$COMPONENTS[4] = "./classes/modules/popup/permissions.php";
?>