<?php
new umiEventListener("users_settings_do", "users", "onUsersSettingsNormalizePhone");
new umiEventListener("users_registrate", "users", "onUsersSettingsNormalizePhone");

new umiEventListener("systemCreateObject", "users", "onUsersObjectNormalizePhone");
new umiEventListener("systemModifyObject", "users", "onUsersObjectNormalizePhone");
new umiEventListener("systemModifyPropertyValue", "users", "onUsersPropertyValueNormalizePhone");