<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class common_users_personal
{
    public function onUsersSettingsNormalizePhone(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return;
        }
        
        $userId = $eventPoint->getParam('user_id');
        if (!$userId) {
            return;
        }
        
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject || $user->getMethod() != SiteUsersUserModel::method) {
            return;
        }
        
        $user->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($user->getValue(SiteUsersUserModel::field_phone)));
        $user->commit();
    }
    
    public function onUsersObjectNormalizePhone(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return;
        }
        
        $user = $eventPoint->getRef('object');
        if (!$user instanceof umiObject || $user->getMethod() != SiteUsersUserModel::method) {
            return;
        }
        
        $user->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($user->getValue(SiteUsersUserModel::field_phone)));
        $user->commit();
    }
    
    public function onUsersPropertyValueNormalizePhone(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return;
        }
        
        if(!in_array($eventPoint->getParam('property'), array(SiteUsersUserModel::field_phone, SiteUsersUserModel::field_system_normalized_phone))) {
            return;
        }
        
        $user = $eventPoint->getRef('entity');
        if (!$user instanceof umiObject || $user->getMethod() != SiteUsersUserModel::method) {
            return;
        }
    
        $user->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($user->getValue(SiteUsersUserModel::field_phone)));
        $user->commit();
    }
}