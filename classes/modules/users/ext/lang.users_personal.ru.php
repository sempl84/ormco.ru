<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$C_LANG = Array(
    'discounts' => 'Мои скидки',
    'address' => 'Редактирование адреса'
);