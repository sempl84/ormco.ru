<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;
use UmiCms\System\Auth\PasswordHash\WrongAlgorithmException;

class site_users_sign_in extends users
{
    public function sign_in()
    {
        switch (getRequest('param0')) {
            case 'email':
            {
                if (AjaxHelper::isAjaxRequest()) {
                    try {
                        $this->_signInByEmailAjax();
                        AjaxHelper::sendResponse(array(
                            'success' => true,
                        ));
                    } catch (Exception $e) {
                        AjaxHelper::sendResponse(array(
                            'error' => $e->getMessage()
                        ));
                    }
                } else {
                    $this->login_do();
                    $this->redirect('/users/login_do/');
                }
                break;
            }
            case 'phone_send_code':
            {
                if (AjaxHelper::isAjaxRequest()) {
                    try {
                        $this->_sendPhoneCode();
                        AjaxHelper::sendResponse(array(
                            'success' => true,
                        ));
                    } catch (Exception $e) {
                        AjaxHelper::sendResponse(array(
                            'error' => $e->getMessage()
                        ));
                    }
                } else {
                    $this->redirect('/users/login/');
                }
                break;
            }
            case 'phone_validate_code':
            {
                if (AjaxHelper::isAjaxRequest()) {
                    try {
                        $this->_validatePhoneByCode();
                        $userId = $this->_findUserByRawPhone($this->_getRequestPhone());
                        if ($userId) {
                            $this->_authorizeUserById($userId);
                            $response = array(
                                'action' => 'auth'
                            );
                        } else {
                            $response = array(
                                'action' => 'register'
                            );
                        }
                        AjaxHelper::sendResponse($response);
                    } catch (Exception $e) {
                        AjaxHelper::sendResponse(array(
                            'error' => $e->getMessage()
                        ));
                    }
                } else {
                    $this->redirect('/users/login/');
                }
                break;
            }
            case 'register':
            {
                if (AjaxHelper::isAjaxRequest()) {
                    try {
                        $this->_validatePhoneByCode();
                        $userId = $this->_registerUser();
                        if ($userId) {
                            $this->_authorizeUserById($userId);
                            $response = array(
                                'action' => 'auth'
                            );
                        } else {
                            throw new publicException('Во время регистрации произошла ошибка');
                        }
                        AjaxHelper::sendResponse($response);
                    } catch (Exception $e) {
                        AjaxHelper::sendResponse(array(
                            'error' => $e->getMessage()
                        ));
                    }
                } else {
                    $this->redirect('/users/login/');
                }
            }
            default:
            {
                $this->redirect('/users/login/');
            }
        }
    }

    /**
     * @throws WrongAlgorithmException
     * @throws baseException
     * @throws publicException
     */
    public function _signInByEmailAjax()
    {
        $login = trim(getRequest('login'));
        if (!$login) {
            throw new publicException('Не указан логин');
        }

        $password = trim(getRequest('password'));
        if (!$password) {
            throw new publicException('Не указан пароль');
        }

        $auth = Service::Auth();
        $userId = $auth->checkLogin($login, $password);
        if (!$userId) {
            throw new publicException('Неверный логин или пароль');
        }

        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject) {
            throw new publicException('Неверный логин или пароль');
        }

        $hashedPassword = $user->getValue('password');
        $hashAlgorithm = Service::PasswordHashAlgorithm();

        if ($hashAlgorithm->isHashedWithMd5($hashedPassword, $password)) {
            $hashedPassword = $hashAlgorithm->hash($password, $hashAlgorithm::SHA256);
            $user->setValue('password', $hashedPassword);
            $user->commit();
        }

        $this->_authorizeUserById($user->getId());
    }

    const session_param_validation_request_time = 'validation_request_time';
    const session_param_validation_phone = 'validation_phone';
    const session_param_validation_code = 'validation_code';
    const session_param_validation_error_count = 'validation_error_count';

    public function _sendPhoneCode()
    {
        $requestPhone = $this->_getRequestPhone();

        $phone = SiteUsersUserModel::normalizePhone($requestPhone);
        if (!$phone) {
            throw new publicException('Указан неверный номер телефона');
        }

        $userId = $this->_findUserByRawPhone($phone);
        if (!$userId) {
            throw new publicException('no_register');
        }

        $session = Service::Session();

        if ($requestPhone == $session->get(self::session_param_validation_phone)) {
            $requestTime = intval($session->get(self::session_param_validation_request_time));
            if ($requestTime > 0 && (time() < $requestTime + 60)) {
                throw new publicException('Вы сможете отправить новый код через ' . (time() - $requestTime) . ' секунд');
            }
        }

        $code = $this->_generateCode();
        OrmcoSiteDataSmsHelper::sendSms($phone, 'Проверочный код ' . $code);

        $session->set(self::session_param_validation_request_time, time());
        $session->set(self::session_param_validation_error_count, 0);
        $session->set(self::session_param_validation_phone, $requestPhone);
        $session->set(self::session_param_validation_code, $code);
    }

    public function _validatePhoneByCode()
    {
        $session = Service::Session();

        $requestPhone = $session->get(self::session_param_validation_phone);
        if ($requestPhone != $this->_getRequestPhone()) {
            throw new publicException('Указан неверный номер телефона');
        }

        if (!SiteUsersUserModel::normalizePhone($requestPhone)) {
            throw new publicException('Указан неверный номер телефона');
        }

        $code = getRequest('code');
        if (Service::Session()->get(self::session_param_validation_code) != $code) {
            $session->set(self::session_param_validation_error_count, intval($session->get(self::session_param_validation_error_count) + 1));
            throw new publicException('Неверный код');
        }
    }

    public function _findUserByRawPhone($phone)
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteUsersUserModel::module, SiteUsersUserModel::method);
        $sel->where(SiteUsersUserModel::field_system_normalized_phone)->equals(SiteUsersUserModel::normalizePhone($phone));
        $sel->where(SiteUsersUserModel::field_is_activated)->equals(1);
        $sel->order('id')->asc();
        $sel->option('return', 'id');

        $result = $sel->result();
        if (count($result) > 1) {
            throw new publicException('Произошла ошибка, авторизуйтесь по E-mail и обратитесь в поддержку');
        }

        return $result ? getArrayKey(getArrayKey($result, 0), 'id') : false;
    }

    public function _generateCode()
    {
        return rand(1000, 9999);
    }

    public function _authorizeUserById($userId)
    {
        $session = Service::Session();
        $session->del(self::session_param_validation_request_time);
        $session->del(self::session_param_validation_error_count);
        $session->del(self::session_param_validation_phone);
        $session->del(self::session_param_validation_code);

        $userId = intval($userId);

        Service::Auth()->loginUsingId($userId);

        $oEventPoint = new umiEventPoint("users_login_successfull");
        $oEventPoint->setParam("user_id", $userId);
        users::setEventPoint($oEventPoint);
    }

    public function _getRequestPhone()
    {
        return getRequest(SiteUsersUserModel::field_phone);
    }

    /**
     * @return int
     * @throws WrongAlgorithmException
     * @throws baseException
     * @throws coreException
     * @throws errorPanicException
     * @throws publicException
     * @throws wrongValueException
     */
    public function _registerUser()
    {
        if ($this->is_auth()) {
            $this->redirect("/");
        }

        $phone = $this->_getRequestPhone();
        if (!$phone) {
            throw new publicException('Не передан номер телефона');
        }

        if ($this->_findUserByRawPhone($phone)) {
            throw new publicException('Пользователь с таким телефоном уже зарегистрирован');
        }

        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if (!$objectTypeId) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }

        $email = getRequest(SiteUsersUserModel::field_email);
        if (!$email) {
            throw new publicException('Не указан e-mail');
        }

        $dadataData = null;

        $isDadataEnabled = OrmcoSiteDataDadataHelper::isDadataEnabled();
        if($isDadataEnabled) {
            $dadataData = json_decode(trim(getRequest('dadata')), true);

            OrmcoSiteDataDadataHelper::validateData(trim(getRequest('address')), $dadataData);
        }

        def_module::$noRedirectOnPanic = true;

        $template = 'default';
        $withoutActivation = true;

        $login = $this->validateLogin($email, false, true);
        $email = $this->validateEmail($email, false, !$withoutActivation);
        $password = SiteUsersUserModel::generatePassword();

        $this->errorThrow('public');

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("before");
        $oEventPoint->setParam("login", $login);
        $oEventPoint->addRef("password", $password);
        $oEventPoint->addRef("email", $email);
        $this->setEventPoint($oEventPoint);

        //Creating user...
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        $userId = $umiObjectsCollection->addObject($login, $objectTypeId);
        $activationCode = md5($login . time());

        $user = $umiObjectsCollection->getObject($userId);
        $user->setValue("login", $login);

        $hashAlgorithm = Service::PasswordHashAlgorithm();
        $encodedPassword = $hashAlgorithm->hash($password);
        $user->setValue("password", $encodedPassword);
        $user->setValue("e-mail", $email);

        $user->setValue(SiteUsersUserModel::field_is_activated, $withoutActivation);
        $user->setValue("activate_code", $activationCode);
        $session = Service::Session();
        $user->setValue("referer", urldecode($session->get("http_referer")));
        $user->setValue("target", urldecode($session->get("http_target")));
        $user->setValue("register_date", time());
        $user->setOwnerId($userId);

        $user->setValue(SiteUsersUserModel::field_phone, $phone);
        $user->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($phone));
        $user->setValue(SiteUsersUserModel::field_phone_valid, true);

        $registry = regedit::getInstance();

        $user->setValue("groups", array($registry->getVal("//modules/users/def_group")));

        $dataModule = cmsController::getInstance()->getModule('data');
        $dataModule->saveEditedObjectWithIgnorePermissions($userId, true, true);

        SiteUsersUserModel::validateProfStatus($user);

        if($isDadataEnabled) {
            OrmcoSiteDataDadataHelper::setUserAddressData($user, $dadataData);
        }

        SiteUsersUserModel::validateRegion($user);

        $user->commit();

        //Forming mail...
        list(
            $template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
            ) = def_module::loadTemplatesForMail("users/register/" . $template,
            "mail_registrated", "mail_registrated_subject", "mail_registrated_noactivation", "mail_registrated_subject_noactivation"
        );

        if ($withoutActivation && $template_mail_noactivation && $template_mail_subject_noactivation) {
            $template_mail = $template_mail_noactivation;
            $template_mail_subject = $template_mail_subject_noactivation;
        }

        $mailData = array(
            'user_id' => $userId,
            'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
            'activate_link' => getSelectedServerProtocol() . "://" . $domain . "/users/activate/" . $activationCode . "/",
            'login' => $login,
            'password' => $password,
            'lname' => $user->getValue("lname"),
            'fname' => $user->getValue("fname"),
            'father_name' => $user->getValue("father_name"),
        );

        $mailContent = def_module::parseTemplateForMail($template_mail, $mailData, false, $userId);
        $mailSubject = def_module::parseTemplateForMail($template_mail_subject, $mailData, false, $userId);

        $fio = $user->getValue("lname") . " " . $user->getValue("fname") . " " . $user->getValue("father_name");

        $emailFrom = $registry->getVal("//settings/email_from");
        $nameFrom = $registry->getVal("//settings/fio_from");

        $registrationMail = new umiMail();
        $registrationMail->addRecipient($email, $fio);
        $registrationMail->setFrom($emailFrom, $nameFrom);
        $registrationMail->setSubject($mailSubject);
        $registrationMail->setContent($mailContent);
        $registrationMail->commit();
        $registrationMail->send();

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("after");
        $oEventPoint->setParam("user_id", $userId);
        $oEventPoint->setParam("login", $login);
        $this->setEventPoint($oEventPoint);

        if(!OrmcoHelper::isDev()) {
            $this->exportNewUser($user);
        }

        return $userId;
    }
}