<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class site_users_personal extends users
{
    public function discounts()
    {
    
    }
    
    public function getUsersPersonalDiscount()
    {
        $sel = new selector('objects');
        $sel->types('object-type')->id(318);
        $sel->where('users')->equals(permissionsCollection::getInstance()->getUserId());
        $sel->order('id')->asc();
        $sel->option('return', 'id');
        
        if ($sel->length() != 1) {
            throw new publicException('Не найден Валидатор скидки');
        }
        
        $ruleObjectId = getArrayKey(getArrayKey($sel->result(), 0), 'id');
        if (!$ruleObjectId) {
            throw new publicException('Не найден Валидатор скидки');
        }
        
        $sel = new selector('objects');
        $sel->types('object-type')->id(41);
        $sel->where('discount_rules_id')->equals($ruleObjectId);
        $sel->where('is_active')->equals(1);
        $sel->option('return', 'discount_modificator_id');
        
        $discountModificatorObjectId = getArrayKey(getArrayKey($sel->result(), 0), 'discount_modificator_id');
        if (!$discountModificatorObjectId) {
            throw new publicException('Не найден модификатор скидки');
        }
        
        $discountModificator = umiObjectsCollection::getInstance()->getObject($discountModificatorObjectId);
        if (!$discountModificator instanceof umiObject) {
            throw new publicException('Не найден модификатор скидки ' . $discountModificatorObjectId);
        }
        
        return array(
            'discount' => array(
                'attribute:id' => $discountModificatorObjectId,
                'node:value' => $discountModificator->getValue('proc')
            )
        );
    }
    
    public function address()
    {
        if(!OrmcoSiteDataDadataHelper::isDadataEnabled()) {
            $this->redirect('/users/settings/');
        }
        
        $action = getRequest('param0');
        if($action) {
            switch ($action) {
                case 'do':
                {
                    try {
                        $this->_saveAddress();
                        $this->redirect('/users/settings/?address=ok');
                    } catch (Exception $e) {
                        $this->errorSetErrorPage('/users/address/');
                        $this->errorAddErrors($e);
                        $this->errorThrow('public');
                    }
                    break;
                }
                default:
                {
                    $this->redirect('/users/address/');
                }
            }
        }
    }
    
    public function _saveAddress()
    {
        $user = umiObjectsCollection::getInstance()->getObject(UmiCms\Service::Auth()->getUserId());
        if(!$user instanceof umiObject || $user->getId() == UmiCms\Service::SystemUsersPermissions()->getGuestUserId()) {
            throw new publicException('Не найден объект пользователя');
        }
        
        $data = json_decode(trim(getRequest('data')), true);
        
        OrmcoSiteDataDadataHelper::validateData(trim(getRequest('address')), $data);
        OrmcoSiteDataDadataHelper::setUserAddressData($user, $data);
        $user->commit();
    
        $this->exportModifyUser($user);
    }
}