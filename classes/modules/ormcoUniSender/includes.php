<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$autoloadFile = __DIR__ . '/autoload.php';
if(file_exists($autoloadFile)) {
    require_once $autoloadFile;
    
    if(isset($classes) && is_array($classes)) {
        foreach($classes as $class => $path) {
            if(!class_exists($class)) {
                require_once $path;
            }
        }
    }
}