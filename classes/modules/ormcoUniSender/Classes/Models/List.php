<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UniSenderApiListModel
{
    private $id;
    private $title;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    public static function loadFromArray($data)
    {
        $model = new self();
        $model->id = getArrayKey($data, 'id');
        $model->title = getArrayKey($data, 'title');
        
        return $model;
    }
}