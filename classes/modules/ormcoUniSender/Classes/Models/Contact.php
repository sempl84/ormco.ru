<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UniSenderApiContactModel
{
    private $email;
    
    private $emailStatus;
    
    const email_status_new = 'new';
    const email_status_active = 'active';
    const email_status_inactive = 'inactive';
    const email_status_unsubscribed = 'unsubscribed';
    
    private $emailAvailability;
    
    private $emailListIds = array();
    
    private $phone;
    
    private $tags = array();
    
    private $data = array();
    
    public function __construct($email)
    {
        $this->email = $email;
    }
    
    /**
     * @param mixed $emailStatus
     */
    public function setEmailStatus($emailStatus)
    {
        if (in_array($emailStatus, array(
            self::email_status_new,
            self::email_status_active,
            self::email_status_inactive,
            self::email_status_unsubscribed,
        ))) {
            $this->emailStatus = $emailStatus;
        }
    }
    
    /**
     * @param mixed $emailAvailability
     */
    public function setEmailAvailability($emailAvailability)
    {
        $this->emailAvailability = (bool)$emailAvailability;
    }
    
    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $phone = str_replace(array('-', '(', ')', ' '), '', $phone);
        
        if($phone[0] == '8') {
            $phone = '+7' . substr($phone, 1);
        }
        
        $this->phone = $phone;
    }
    
    public function addEmailListId($emailListId)
    {
        $this->emailListIds[] = $emailListId;
    }
    
    public function addTag($tag)
    {
        $this->tags[] = $tag;
    }
    
    public function addData($field, $value)
    {
        $this->data[$field] = $value;
    }
    
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @return mixed
     */
    public function getEmailStatus()
    {
        return $this->emailStatus;
    }
    
    /**
     * @return mixed
     */
    public function getEmailAvailability()
    {
        return $this->emailAvailability;
    }
    
    /**
     * @return array
     */
    public function getEmailListIds()
    {
        return $this->emailListIds;
    }
    
    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }
    
    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}