<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class OrmcoUniSenderMacros extends ormcoUniSender
{
    public function getOrmcoUniSenderLists()
    {
        $lists = $this->getApi()->getLists();
        if (!$lists) {
            return null;
        }
        
        $items = array();
        foreach ($lists as $list) {
            $items[] = array(
                'attribute:id' => $list->getId(),
                'node:title' => $list->getTitle()
            );
        }
        
        if (!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    public function getContactFromUser(umiObject $user)
    {
        $email = $user->getValue(SiteUsersUserModel::field_email);
        if (!$email) {
            throw new publicException('У пользователя не указан email');
        }
        
        $contact = new UniSenderApiContactModel($email);
        $contact->setPhone($user->getValue(SiteUsersUserModel::field_phone));
        $contact->addData('surname', $user->getValue(SiteUsersUserModel::field_surname));
        $contact->addData('Name', $user->getValue(SiteUsersUserModel::field_name));
        $contact->addData('fathername', $user->getValue(SiteUsersUserModel::field_father_name));
        $contact->addData('status', ObjectHelper::getObjectName($user->getValue(SiteUsersUserModel::field_prof_status)));
        $contact->addData('region', ObjectHelper::getObjectName($user->getValue(SiteUsersUserModel::field_region)));
        
        return $contact;
    }
    
    public function testGetUserContactModel($userId = null)
    {
        return array('full:contact' => $this->getContactFromUser($this->_getUserObject($userId)));
    }
    
    public function testSendUserContact($userId = null)
    {
        $contactsListId = $this->_getContactsListId();
        $contact = $this->getContactFromUser($this->_getUserObject($userId));
        $contact->addEmailListId($contactsListId);
        
        return $this->getApi()->importContacts([$contact]);
    }
    
    public function _getUserObject($userId = null)
    {
        if (!$userId) {
            throw new publicException('Не передан id пользователя');
        }
        
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject) {
            throw new publicException('Не найден пользователь ' . $userId);
        }
        
        return $user;
    }
    
    public function testGetContact($email = null)
    {
        if (!$email) {
            throw new publicException('Не передан email');
        }
        
        return array('full:contact' => $this->getApi()->getContact($email));
    }
    
    const session_param_subscribe_success = 'uni_sender_subscribe_message';
    const session_param_subscribe_error = 'uni_sender_subscribe_error';
    const session_param_subscribe_email = 'uni_sender_subscribe_email';
    
    /**
     * @return array|null
     * @throws publicException
     */
    public function getUniSenderSubscribeForm()
    {
        if (!$this->_getContactsListId()) {
            return null;
        }
        
        $data = array(
            'can-subscribe' => true
        );
        
        $session = Service::Session();
        
        if ($session->get(self::session_param_subscribe_success)) {
            $data['success'] = true;
            $session->del(self::session_param_subscribe_success);
            $session->del(self::session_param_subscribe_email);
        } elseif ($error = $session->get(self::session_param_subscribe_error)) {
            $data['error'] = $error;
            $data['email'] = $session->get(self::session_param_subscribe_email);
            $session->del(self::session_param_subscribe_error);
            $session->del(self::session_param_subscribe_email);
        }
        
        return $data;
    }
    
    public function uniSenderSubscribe()
    {
        $isAjaxRequest = AjaxHelper::isAjaxRequest();
    
        try {
            $this->_doUniSenderSubscribe();
            if ($isAjaxRequest) {
                AjaxHelper::sendResponse(array(
                    'success' => true
                ));
            } else {
                Service::Session()->set(self::session_param_subscribe_success, true);
                Service::Session()->del(self::session_param_subscribe_error);
                
                $referer = getServer('HTTP_REFERER');
                if(!$referer) {
                    $referer = '/';
                }
                
                $this->redirect($referer . '?subscribe=success');
            }
        } catch (Exception $e) {
            if ($isAjaxRequest) {
                AjaxHelper::sendResponse(array(
                    'error' => $e->getMessage()
                ));
            } else {
                Service::Session()->del(self::session_param_subscribe_success);
                Service::Session()->set(self::session_param_subscribe_error, $e->getMessage());
    
                if(!$referer) {
                    $referer = '/';
                }
                
                $this->redirect($referer . '?subscribe=error');
            }
        }
    }
    
    /**
     * @return bool
     * @throws publicException
     */
    public function _doUniSenderSubscribe()
    {
        $contactsListId = $this->_getContactsListId();
        
        $email = trim(getRequest('email'));
        if (!umiMail::checkEmail($email)) {
            throw new publicException('Неверный e-mail');
        }
        
        Service::Session()->set(self::session_param_subscribe_email, $email);
        
        $user = null;
        $userId = SiteUsersUserModel::getUserIdByEmail($email);
        if ($userId) {
            $user = umiObjectsCollection::getInstance()->getObject($userId);
            if ($user instanceof umiObject && $user->getValue(SiteUsersUserModel::field_uni_sender_exported)) {
                throw new publicException('E-mail уже подписан');
            }
        }
        
        $api = $this->getApi();
        
        if ($user instanceof umiObject) {
            $contact = $this->getContactFromUser($user);
        } elseif($api->getContact($email)) {
            throw new publicException('E-mail уже подписан');
        } else {
            $contact = new UniSenderApiContactModel($email);
        }
        
        $contact->addEmailListId($contactsListId);
        $api->importContacts(array($contact));
        
        if ($user instanceof umiObject) {
            if ($api->getContact($email)) {
                $user->setValue(SiteUsersUserModel::field_uni_sender_exported, true);
                $user->commit();
            }
        }
        
        return true;
    }
    
    public function _canUniSenderSubscribe()
    {
        $userId = Service::Auth()->getUserId();
        
        if ($userId == UmiCms\Service::SystemUsersPermissions()->getGuestUserId()) {
            return true;
        }
        
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if ($user instanceof umiObject && $user->getValue(SiteUsersUserModel::field_uni_sender_exported)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * @return mixed
     * @throws publicException
     */
    public function _getContactsListId()
    {
        $contactsListId = regedit::getInstance()->getVal(ormcoUniSender::registry_param_contacts_list_id);
        if (!$contactsListId) {
            throw new publicException('Не выбран список для импорта контакта');
        }
        
        return $contactsListId;
    }
}