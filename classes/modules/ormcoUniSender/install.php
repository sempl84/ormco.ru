<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$INFO = Array();

$INFO['version'] = '1.0';
$INFO['version_line'] = 'pro';

$INFO['name'] = 'ormcoUniSender';
$INFO['title'] = 'UniSender';
$INFO['filename'] = 'modules/ormcoUniSender/class.php';
$INFO['config'] = '1';
$INFO['ico'] = 'ico_ormcoUniSender';
$INFO['default_method'] = '';
$INFO['default_method_admin'] = 'dashboard';

$SQL_INSTALL = Array();

$moduleDir = './classes/modules/ormcoUniSender';

$COMPONENTS = array(
    $moduleDir . '/__admin.php',
    $moduleDir . '/class.php',
    $moduleDir . '/i18n.php',
    $moduleDir . '/lang.php',
    $moduleDir . '/permissions.php',
);