<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$i18n = Array(
    'module-ormcoUniSender' => 'UniSender',
    
    'header-ormcoUniSender-dashboard' => 'Панель управления',
    
    'header-ormcoUniSender-syncContacts' => 'Синхронизация контактов',
    'header-ormcoUniSender-syncContactsLogList' => 'Логи',
    'header-ormcoUniSender-syncContactsLogView' => 'Просмотр лога',
    
    'header-ormcoUniSender-config' => 'Базовые настройки',
    'group-uniSender-config-api' => 'Интеграция по API',
    'option-uniSender-config-api-key' => 'Api Key',
    'option-uniSender-config-test-email' => 'Тестовый e-mail',
    'group-uniSender-sender-contacts' => 'Передача контактов',
    'option-uniSender-sender-contacts-list' => 'Список контактов',
    
    'perms-ormcoUniSender-admin' => 'Управление модулем',
    
    'perms-ormcoUniSender-subscribe' => 'Подписка на рассылку',
);