<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$INFO = Array();

$INFO['version'] = "1.0";
$INFO['version_line'] = "pro";

$INFO['name'] = "ormcoSiteData";
$INFO['title'] = "Данные";
$INFO['filename'] = "modules/ormcoSiteData/class.php";
$INFO['ico'] = "ico_ormcoSiteData";
$INFO['default_method'] = "ormcoSiteData";
$INFO['default_method_admin'] = "dashboard";
$INFO['config'] = "1";

$SQL_INSTALL = Array();

$moduleDir = str_replace(CURRENT_WORKING_DIR, '.', dirname(__FILE__));

$COMPONENTS = array(
    $moduleDir . '/__admin.php',
    $moduleDir . '/class.php',
    $moduleDir . '/i18n.php',
    $moduleDir . '/lang.php',
    $moduleDir . '/permissions.php',
);