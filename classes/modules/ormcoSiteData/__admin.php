<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

/**
 * Class __ormcoSiteDataAdmin
 */
class __ormcoSiteDataAdmin extends baseModuleAdmin
{
    public function dashboard()
    {
        outputBuffer::current()->redirect('/admin/ormcoSiteData/sms/');
    }

    public function config()
    {
        outputBuffer::current()->redirect('/admin/ormcoSiteData/smsConfig/');
    }

    public function methodAddElement($type, $mode, $editMethod, $parent = false)
    {
        $inputData = Array(
            "type" => $type,
            "parent" => $parent,
            "allowed-element-types" => array(
                $type
            )
        );

        if ($mode == "do") {
            $elementId = $this->saveAddedElementData($inputData);
            $this->chooseRedirect("{$this->pre_lang}/admin/ormcoSiteData/{$editMethod}/{$elementId}/");
        }

        $this->setDataType("form");
        $this->setActionType("create");

        $data = $this->prepareData($inputData, "page");

        $this->setData($data);
        $this->doData();
    }

    /**
     * @param umiHierarchyElement $element
     * @param $type
     * @param $mode
     * @return null
     * @throws coreException
     * @throws expectElementException
     * @throws wrongElementTypeAdminException
     */
    public function methodEditElement($type)
    {
        $element = $this->expectElement('param0');

        $inputData = Array(
            'element' => $element,
            'allowed-element-types' => array(
                $type
            ),
        );

        if (getRequest('param1') == "do") {
            $this->saveEditedElementData($inputData);
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("modify");

        $data = $this->prepareData($inputData, 'page');

        $this->setData($data);
        $this->doData();

        return null;
    }

    public function methodActivityPages($allowedElementTypes)
    {
        if (!is_array($allowedElementTypes)) {
            $allowedElementTypes = array($allowedElementTypes);
        }

        $elements = getRequest('element');
        if (!is_array($elements)) {
            $elements = Array($elements);
        }
        $isActive = getRequest('active');

        foreach ($elements as $elementId) {
            $element = $this->expectElement($elementId, false, true);
            $this->switchActivity(array(
                'element' => $element,
                'allowed-element-types' => $allowedElementTypes,
                'activity' => $isActive
            ));
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($elements, "pages");
        $this->setData($data);

        $this->doData();

        return null;
    }

    public function methodDeleteElements($allowedElementTypes)
    {
        if (!is_array($allowedElementTypes)) {
            $allowedElementTypes = array($allowedElementTypes);
        }

        $elements = getRequest('element');
        if(!is_array($elements)) {
            $elements = Array($elements);
        }

        foreach($elements as $elementId) {
            $element = $this->expectElement($elementId, false, true);
            $params = Array(
                "element" => $element,
                "allowed-element-types" => $allowedElementTypes
            );

            $this->deleteElement($params);
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($elements, "pages");
        $this->setData($data);

        return null;
    }

    public function methodEditObject($type)
    {
        $object = $this->expectObject('param0');

        $inputData = Array(
            'object' => $object,
            'allowed-element-types' => array(
                $type
            ),
        );

        if (getRequest('param1') == "do") {
            $this->saveEditedObjectData($inputData);
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("modify");

        $data = $this->prepareData($inputData, 'object');

        $this->setData($data);
        $this->doData();

        return null;
    }

    public function methodActivityObject($allowedObjectTypes, $field)
    {
        if (!is_array($allowedObjectTypes)) {
            $allowedObjectTypes = array($allowedObjectTypes);
        }

        $objects = getRequest('element');
        if (!is_array($objects)) {
            $objects = Array($objects);
        }
        $isActive = getRequest('active');

        foreach ($objects as $objectId) {
            $object = $this->expectObject($objectId, false, true);
            if(!in_array($object->getMethod(), $allowedObjectTypes)) {
                continue;
            }

            $object->setValue($field, $isActive);
            $object->commit();
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($objects, "objects");
        $this->setData($data);

        $this->doData();

        return null;
    }

    /**
     * @param string $param
     * @return array
     * @throws publicException
     */
    public function getDatasetConfiguration($param = '')
    {
        $arParams = array();

        if(strpos($param, '|') !== false) {
            $data = explode('|', $param);
            $param = $data[0];
            $arParams[] = $data[1];
        }

        switch ($param) {
            default: {
                throw new publicException('Не найдены настройки для getDatasetConfiguration');
            }
        }
    }
}