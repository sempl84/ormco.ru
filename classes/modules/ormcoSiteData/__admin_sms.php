<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

/**
 * Class __ormcoSiteDataAdmin
 */
class __ormcoSiteDataAdminSms extends __ormcoSiteDataAdmin
{
    public function sms()
    {
        $this->setDataType('settings');
        $this->setActionType('view');
        
        $this->setData(array());
        $this->doData();
    }
    
    public function smsConfig()
    {
        $regedit = regedit::getInstance();
        
        $params = array(
            'ormcoSiteData-smsConfig-api' => array(
                'string:ormcoSiteData-smsConfig-api-token' => NULL,
                'string:ormcoSiteData-smsConfig-api-from' => NULL,
            ),
        );
        
        $mode = getRequest('param0');
        
        if ($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(OrmcoSiteDataSmsHelper::registry_param_bytehand_token, $params['ormcoSiteData-smsConfig-api']['string:ormcoSiteData-smsConfig-api-token']);
            $regedit->setVar(OrmcoSiteDataSmsHelper::registry_param_bytehand_from, $params['ormcoSiteData-smsConfig-api']['string:ormcoSiteData-smsConfig-api-from']);
            $this->chooseRedirect();
        }
        
        $params['ormcoSiteData-smsConfig-api']['string:ormcoSiteData-smsConfig-api-token'] = $regedit->getVal(OrmcoSiteDataSmsHelper::registry_param_bytehand_token);
        $params['ormcoSiteData-smsConfig-api']['string:ormcoSiteData-smsConfig-api-from'] = $regedit->getVal(OrmcoSiteDataSmsHelper::registry_param_bytehand_from);
        
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
    
    public function sendSms()
    {
        $phone = SiteUsersUserModel::normalizePhone(trim(getRequest('phone')));
        if (!$phone) {
            throw new publicException('Не передан номер телефона');
        }
        
        $message = trim(getRequest('message'));
        if (!$message) {
            throw new publicException('Не передано сообщение');
        }
        
        OrmcoSiteDataSmsHelper::sendSms($phone, $message);
        
        $this->redirect('/admin/ormcoSiteData/sms/');
    }
}