<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class ormcoSiteData extends def_module
{
    const module = 'ormcoSiteData';

    public function __construct()
    {
        parent::__construct();

        if (cmsController::getInstance()->getCurrentMode() == "admin") {
            $this->initTabs();
            $this->includeAdminClasses();
        }

        $this->includeCommonClasses();
    }

    /**
     * Создает вкладки административной панели модуля
     */
    protected function initTabs()
    {
        $commonTabs = $this->getCommonTabs();

        if ($commonTabs instanceof iAdminModuleTabs) {
            $commonTabs->add('sms');
            $commonTabs->add('dadata');
        }
        
        $configTabs = $this->getConfigTabs();
        if($configTabs instanceof iAdminModuleTabs) {
            $configTabs->add('smsConfig');
            $configTabs->add('dadataConfig');
        }
    }

    /**
     * Подключает классы функционала административной панели
     */
    protected function includeAdminClasses()
    {
        $this->__loadLib("__admin.php");
        $this->__implement("__ormcoSiteDataAdmin");

        $this->__loadLib("__admin_sms.php");
        $this->__implement("__ormcoSiteDataAdminSms");
    
        $this->__loadLib("__admin_dadata.php");
        $this->__implement("__ormcoSiteDataAdminDadata");
    }

    /**
     * Подключает общие классы функционала
     */
    protected function includeCommonClasses()
    {
        $this->__loadLib("__macros_dadata.php");
        $this->__implement("OrmcoSiteDataMacrosDadata");
    }
}