<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$i18n = Array(
    'module-ormcoSiteData' => 'Данные сайта',

    'header-ormcoSiteData-sms' => 'SMS',
    
    'header-ormcoSiteData-smsConfig' => 'Настройки SMS',
    'group-ormcoSiteData-smsConfig-api' => 'API',
    'option-ormcoSiteData-smsConfig-api-token' => 'Токен',
    'option-ormcoSiteData-smsConfig-api-from' => 'Отправитель',
    
    'header-ormcoSiteData-dadata' => 'Dadata',
    
    'header-ormcoSiteData-dadataConfig' => 'Настройки Dadata',
    'group-ormcoSiteData-dadataConfig-api' => 'API',
    'option-ormcoSiteData-dadataConfig-api-token' => 'Токен',

    'perms-ormcoSiteData-admin' => 'Управление модулем',
    'perms-ormcoSiteData-auth' => 'Авторизованные пользователи',
    'perms-ormcoSiteData-guest' => 'Гостевые права',
);