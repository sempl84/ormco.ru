<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

class OrmcoSiteDataMacrosDadata extends ormcoSiteData
{
    public function getSiteDataDadataToken()
    {
        $token = regedit::getInstance()->getVal(OrmcoSiteDataDadataHelper::registry_param_token);
        
        if (!$token) {
            return null;
        }
        
        return array(
            'token' => $token
        );
    }
    
    public function getSiteDataIsUserDadataEmpty()
    {
        $user = umiObjectsCollection::getInstance()->getObject(UmiCms\Service::Auth()->getUserId());
        if(!$user instanceof umiObject || $user->getId() == UmiCms\Service::SystemUsersPermissions()->getGuestUserId()) {
            throw new publicException('Не найден объект пользователя');
        }
        
        if(isset($_REQUEST['test_dadata_alert'])) {
            $isEmpty = true;
        } else {
            $isEmpty = !$user->getValue(SiteUsersUserModel::field_address_raw);
        }
        
        return array('attribute:is-empty' => intval($isEmpty));
    }
}