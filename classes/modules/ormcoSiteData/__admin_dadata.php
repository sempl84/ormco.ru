<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

/**
 * Class __ormcoSiteDataAdmin
 */
class __ormcoSiteDataAdminDadata extends __ormcoSiteDataAdmin
{
    public function dadata()
    {
        outputBuffer::current()->redirect('/admin/ormcoSiteData/dadataConfig/');
    }
    
    public function dadataConfig()
    {
        $regedit = regedit::getInstance();
        
        $params = array(
            'ormcoSiteData-dadataConfig-api' => array(
                'string:ormcoSiteData-dadataConfig-api-token' => NULL,
            ),
        );
        
        $mode = getRequest('param0');
        
        if ($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(OrmcoSiteDataDadataHelper::registry_param_token, $params['ormcoSiteData-dadataConfig-api']['string:ormcoSiteData-dadataConfig-api-token']);
            $this->chooseRedirect();
        }
        
        $params['ormcoSiteData-dadataConfig-api']['string:ormcoSiteData-dadataConfig-api-token'] = $regedit->getVal(OrmcoSiteDataDadataHelper::registry_param_token);
        
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
}