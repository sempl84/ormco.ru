<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$permissions = Array(
    'admin' => array(
        'sms', 'smsConfig'
    ),
    'auth' => array(
        'getSiteDataIsUserDadataEmpty',
    ),
    'guest' => array(
        'getSiteDataDadataToken',
    )
);