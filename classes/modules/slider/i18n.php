<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-slider-lists" => "Группы и страницы",
"header-slider-config" => "Настройки модуля",
"header-slider-add" => "Добавление",
"header-slider-edit" => "Редактирование",
 
"header-slider-add-groupelements"   => "Добавление группы",
"header-slider-add-item_element"     => "Добавление страницы",
 
"header-slider-edit-groupelements"   => "Редактирование группы",
"header-slider-edit-item_element"     => "Редактирование страницы",
 
"header-slider-activity" => "Изменение активности",
 
'perms-slider-view' => 'Просмотр страниц',
'perms-slider-lists' => 'Управление страницами',

'module-slider' => 'Слайдер',
 
);
 
?>