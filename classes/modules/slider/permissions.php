<?php
$permissions = array(
    'view' => array(
        'groupelements', 'item_element', 'group', 'view', 'listGroup', 'listElements',
        'getSliderItems'
    ),
    'lists' => array('lists', 'add', 'edit', 'del', 'groupelements.edit', 'item_element.edit', 'activity', 'publish')
);
?>