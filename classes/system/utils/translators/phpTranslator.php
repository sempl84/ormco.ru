<?php

namespace UmiCms\Classes\System\Translators;

/**
 * Транслятор массива c обозначением XML-сущностей в массив,
 * в котором отсутствует излишняя информация, описывающая XML
 * Термины:
 * Key - ключ исходного массива (обозначение XML-сущности включая ее имя)
 * Symbol - обозначение XML-сущности, то есть тип сущности
 * Branch - отдельная ветка исходного массива
 * Class PhpTranslator
 * @package UmiCms\Classes\System\Translators
 */
class PhpTranslator {

	/** @const символ, разделяющий обозначение XML-сущности и ее имя */
	const SYMBOL_DELIMITER = ':';

	/** @var array список коротких обозначений XML-сущностей */
	private static $shortSymbolList = [
		'@', '#', '+', '%', '*'
	];

	/** @var array список полных обозначений XML-сущностей */
	private static $fullSymbolList = [
		'attr',
		'attribute',
		'list',
		'nodes',
		'node',
		'void',
		'xml',
		'full',
		'xlink',
		'@xlink',
		'comment',
		'subnodes'
	];

	/** @var array список XML-сущностей, которые не будут транслированы */
	private static $redundantSymbolList = [
		'%', 'xlink', '@xlink', 'xml'
	];

	/**
	 * Выполняет непосредственную трансляцию данных в массив,
	 * в котором отсутствует излишняя информация, описывающая XML
	 * @param array $data данные в формате c обозначением XML-сущностей
	 * @return array
	 * @throws \ErrorException если переданы данные в некорректном для трансляции формате
	 */
	public function translate($data) {
		$this->validate($data);

		$result = [];

		foreach ($data as $key => $branch) {
			if ($this->isRedundant($key)) {
				continue;
			}

			$branch = $this->removeRedundantInfo($branch);

			$newKey = $this->cleanKey($key);
			$result[$newKey] = $this->translateBranch($branch);
		}

		return $result;
	}

	/**
	 * Транслирует данные ветки массива
	 * @param mixed $data транслируемые данные
	 * @return array результат трансляции
	 */
	private function translateBranch($data) {
		if ( is_array($data) ) {
			return $this->translate($data);
		}

		return $data;
	}

	/**
	 * Производит удаление излишних данных.
	 * Если передан массив, то он будет обработан таким образом, что из него будут удалены все ключи,
	 * которые не предоставляют полезной информации.
	 * @example
	 * До
	 * [
	 * 	'key' => [
	 * 		'key1' => ['key2' => 'value']
	 * 	 ]
	 * ]
	 * После
	 * ['key' => 'value']
	 * @param mixed $data обрабатываемые данные
	 * @return mixed нормализованные данные
	 */
	private function removeRedundantInfo($data) {
		if ( !is_array($data) ) {
			return $data;
		}

		$keyList = array_keys($data);

		if (count($keyList) !== 1) {
			return $data;
		}

		$singleKey = $keyList[0];

		if ( !is_string($singleKey) ) {
			return $data;
		}

		return $this->removeRedundantInfo($data[$singleKey]);
	}

	/**
	 * Производит чистку ключа от излишней информации путем удаление его частей
	 * @param string $key ключ исходного массива
	 * @return string очищенный ключ
	 */
	private function cleanKey($key) {
		$short = $this->getShortSymbol($key);

		if ($short) {
			return substr($key, 1);
		}

		$full = $this->getFullSymbol($key);

		if ($full) {
			return substr($key, strlen($full) + strlen(self::SYMBOL_DELIMITER));
		}

		return $key;
	}

	/**
	 * Возвращает является ли ключ лишним, то есть будет ли он и его ветка включены в результирующий массив
	 * @param string $key ключ исходного массива
	 * @return bool
	 */
	private function isRedundant($key) {
		$symbol = $this->getSymbolFromKey($key);
		return in_array($symbol, self::$redundantSymbolList);
	}

	/**
	 * Возвращает обозначения XML-сущности ключа
	 * @param string $key ключ исходного массива
	 * @return null|string если обозначение было найдено, то строка с именем обозначения или null в ином случае
	 */
	private function getSymbolFromKey($key) {
		$full = $this->getFullSymbol($key);

		if ($full) {
			return $full;
		}

		return $this->getShortSymbol($key);
	}

	/**
	 * Возвращает короткое обозначение XML-сущности, которое содержится в переданном ключе
	 * @param string $key ключ исходного массива
	 * @return string|null если обозначение было найдено, то строка c именем обозначения или null в ином случае
	 */
	private function getShortSymbol($key) {
		$firstSymbol = substr($key, 0, 1);
		$foundKey = array_search($firstSymbol, self::$shortSymbolList);

		if ($foundKey === false) {
			return null;
		}

		return self::$shortSymbolList[$foundKey];

	}

	/**
	 * Возвращает полное обозначение XML-сущности, которое содержится в переданном ключе
	 * @param string $key ключ исходного массива
	 * @return string|null если обозначение было найдено, то строка c именем обозначения или null в ином случае
	 */
	private function getFullSymbol($key) {
		$delimiterPosition = strpos($key, self::SYMBOL_DELIMITER);

		if ( $delimiterPosition === false ) {
			return null;
		}

		$symbolCandidate = substr($key, 0, $delimiterPosition);

		if ( !in_array($symbolCandidate, self::$fullSymbolList) ) {
			return null;
		}

		return $symbolCandidate;
	}

	/**
	 * Производит валидацию исходных данных
	 * @param mixed $data исходные данные для трансляции
	 * @return bool true, если данные валидны
	 * @throws \ErrorException если переданы данные в некорректном для трансляции формате
	 */
	private function validate($data) {
		if ( !is_array($data) ) {
			throw new \ErrorException('Array expected');
		}

		return true;
	}
}