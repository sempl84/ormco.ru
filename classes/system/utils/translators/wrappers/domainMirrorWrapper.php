<?php
	class domainMirrorWrapper extends translatorWrapper {
		public function translate($data) {
			return $this->translateData($data);
		}
		
		protected function translateData(iDomainMirror $domainMirror) {
			return array(
				'attribute:id'		=> $domainMirror->getId(),
				'attribute:host'	=> $domainMirror->getHost(),
			);
		}
	};