<?php

	namespace UmiCms\Classes\System\Utils\Captcha\Strategies;

	/**
	 * Стратегия работы с капчей Google reCaptcha
	 * @link https://www.google.com/recaptcha
	 */
	class GoogleRecaptcha extends CaptchaStrategy {

		/** @inheritdoc */
		public function getName() {
			return 'recaptcha';
		}

		/** @const ссылка на скрипт виджета */
		const WIDGET_URL = 'https://www.google.com/recaptcha/api.js';

		/** @const ссылка на api проверки */
		const VERIFICATION_URL = 'https://www.google.com/recaptcha/api/siteverify';

		/** @inheritdoc */
		public function generate($template, $inputId, $captchaHash, $captchaId) {
			if (!$this->isRequired()) {
				return '';
			}

			list($recaptchaTemplate) = \def_module::loadTemplates('captcha/' . $template, 'recaptcha');
			$variables = [
				'recaptcha-url' => self::WIDGET_URL,
				'recaptcha-class' => 'g-recaptcha',
				'recaptcha-sitekey' => (string) \regedit::getInstance()->getVal('//settings/recaptcha-sitekey'),
			];

			return \def_module::parseTemplate($recaptchaTemplate, $variables);
		}

		/**
		 * @inheritdoc
		 * @link https://developers.google.com/recaptcha/docs/verify
		 */
		public function isValid() {
			if (!$this->isRequired()) {
				return true;
			}

			$response = getRequest('g-recaptcha-response');
			$secret = \regedit::getInstance()->getVal('//settings/recaptcha-secret');

			if (!$response || !$secret) {
				return false;
			}

			$params = [
				'secret' => $secret,
				'response' => $response,
				'remoteip' => getServer('REMOTE_ADDR'),
			];

			$localName = false;
			$addHeaders = false;
			$result = \umiRemoteFileGetter::get(self::VERIFICATION_URL, $localName, $addHeaders, $params);
			$result = json_decode($result, true);

			return is_array($result) && array_key_exists('success', $result) && (bool) $result['success'];
		}

	}
