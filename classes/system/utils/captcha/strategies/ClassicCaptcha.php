<?php

	namespace UmiCms\Classes\System\Utils\Captcha\Strategies;

	/**
	 * Классическая стратегия работы с капчей.
	 * Капча рисуется объектами класса \captchaDrawer.
	 */
	class ClassicCaptcha extends CaptchaStrategy {

		/** @const ID_PARAM имя параметра для идентификатора CAPTCHA */
		const ID_PARAM = 'captcha_id';

		/** @inheritdoc */
		public function getName() {
			return 'captcha';
		}

		/**
		 * Генерирует код вызова капчи
		 * @param String $template = "default" шаблон для генерации кода капчи
		 * @param String $inputId = "sys_captcha" id инпута для капчи
		 * @param String $captchaHash = "" md5-хеш кода, который будет выведен на картинке для предворительно проверки на
		 *   клиенте
		 * @param String $captchaId идентификатор CAPTCHA
		 * @return Array|String результат обработки в зависимости от текущего шаблонизатора
		 */
		public function generate($template, $inputId, $captchaHash, $captchaId) {
			if (!$this->isRequired()) {
				return \def_module::isXSLTResultMode() ? [
					'comment:explain' => 'Captcha is not required for logged users'
				] : '';
			}

			if (!$template) {
				$template = "default";
			}

			if (!$inputId) {
				$inputId = "sys_captcha";
			}

			if (!$captchaHash) {
				$captchaHash = "";
			}

			$randomString = "?" . time();

			$block_arr = [];
			$block_arr['void:input_id'] = $inputId;
			$block_arr['attribute:captcha_hash'] = $captchaHash;
			$block_arr['attribute:random_string'] = $randomString;
			$block_arr['url'] = [
				'attribute:random-string' => $randomString,
				'node:value' => '/captcha.php',
			];

			if ($captchaId) {
				$block_arr['url']['attribute:id_param'] = self::ID_PARAM;
				$block_arr['url']['attribute:id'] = $captchaId;
			}

			list($template_captcha) = \def_module::loadTemplates("captcha/" . $template, "captcha");
			return \def_module::parseTemplate($template_captcha, $block_arr);
		}

		/** @inheritdoc */
		public function isRequired() {
			if (!parent::isRequired()) {
				return false;
			}

			if (!\regedit::getInstance()->getVal('//settings/captcha-remember')) {
				return true;
			}

			return (\UmiCms\Service::Session()->get('is_human') != 1);
		}

		/** @inheritdoc */
		public function isValid() {
			if (!$this->isRequired()) {
				return true;
			}

			$session = \UmiCms\Service::Session();

			if (!$session->isExist('umi_captcha')) {
				return false;
			}

			$captcha = $session->get('umi_captcha');
			$captchaCode = $captcha;
			$userCaptchaCode = (string) getRequest('captcha');

			if (is_array($captcha)) {
				$captchaId = (string) getRequest(self::ID_PARAM);

				if (!isset($captcha[$captchaId])) {
					return false;
				}

				$captchaCode = $captcha[$captchaId];
			}

			return $this->compareCodes($captchaCode, $userCaptchaCode);
		}

		/** @inheritdoc */
		public function getDrawer() {
			$config = \mainConfiguration::getInstance();
			$name = $config->get('anti-spam', 'captcha.drawer') ?: 'default';
			return $this->findDrawer($name);
		}

		/**
		 * Сравнивает коды CAPTCHA и возвращает результат сравнения
		 * @param string $captchaCode сохранный код CAPTCHA
		 * @param mixed $userCode код, отправленный пользователем
		 * @return bool
		 */
		private function compareCodes($captchaCode, $userCode) {
			$codeHashFromUser = md5($userCode);
			$session = \UmiCms\Service::Session();

			if ($captchaCode == $codeHashFromUser) {
				$session->set('is_human', 1);
				return true;
			}

			$session->del('is_human');
			return false;
		}

	}
