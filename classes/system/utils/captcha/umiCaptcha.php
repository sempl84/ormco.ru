<?php

	use \UmiCms\Classes\System\Utils\Captcha\Strategies;

	/**
	 * Класс, контролирующий работу с капчей
	 */
	class umiCaptcha implements iUmiCaptcha {

		/** @var Strategies\CaptchaStrategy стратегия работы с капчей */
		private static $strategy;

		/**
		 * Возвращает строковой идентификатор стратегии капчи
		 * @return string
		 */
		public static function getName() {
			return self::getStrategy()->getName();
		}

		/**
		 * Генерирует код вызова капчи
		 * @param String $template = "default" шаблон для генерации кода капчи
		 * @param String $inputId = "sys_captcha" id инпута для капчи
		 * @param String $captchaHash = "" md5-хеш кода, который будет выведен на картинке для предворительно проверки на
		 *   клиенте
		 * @param String $captchaId идентификатор CAPTCHA
		 * @return Array|String результат обработки в зависимости от текущего шаблонизатора
		 */
		public static function generateCaptcha($template = "default", $inputId = "sys_captcha", $captchaHash = "", $captchaId = '') {
			return self::getStrategy()->generate($template, $inputId, $captchaHash, $captchaId);
		}

		/**
		 * Проверяет валидность выбранной на сайте CAPTCHA
		 * @return bool
		 */
		public static function checkCaptcha() {
			return self::getStrategy()->isValid();
		}

		/**
		 * Получает объект отрисовки капчи
		 * @return captchaDrawer объект отрисовки капчи
		 * @throws coreException
		 */
		public static function getDrawer() {
			return self::getStrategy()->getDrawer();
		}

		/**
		 * Возвращает стратегию работы с капчей
		 * @return Strategies\CaptchaStrategy
		 */
		private static function getStrategy() {
			if (!self::$strategy) {
				self::determineStrategy();
			}
			return self::$strategy;
		}

		/**
		 * Определяет стратегию работы с капчей
		 */
		private static function determineStrategy() {
			if (regedit::getInstance()->getVal('//settings/enable-recaptcha')) {
				self::$strategy = new Strategies\GoogleRecaptcha();
			} elseif (mainConfiguration::getInstance()->get('anti-spam', 'captcha.enabled')) {
				self::$strategy = new Strategies\ClassicCaptcha();
			} else {
				self::$strategy = new Strategies\NullCaptcha();
			}
		}

		/**
		 * @deprecated
		 * Нужно ли выводить капчу текущему пользователю/проверять введенную капчу?
		 * @return bool
		 */
		public static function isNeedCaptha() {
			return self::getStrategy()->isRequired();
		}

	}
