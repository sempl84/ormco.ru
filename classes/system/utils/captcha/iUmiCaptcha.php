<?php

	interface iUmiCaptcha {
		public static function generateCaptcha($template = "default", $inputId = "sys_captcha", $captchaHash = "");

		public static function isNeedCaptha();

		public static function checkCaptcha();

		public static function getDrawer();
	}

	/**
	 * Абстрактный класс для генерации картинки-капчи
	 */
	abstract class captchaDrawer {

		/** @const length длина кода */
		const length = 6;

		/** @const доступные для генерации кода символы */
		const alphabet = '23456789qwertyuipasdfghjkzxcvbnm';

		/**
		 * Генерирует картинку-капчу с переданным кодом
		 * @param string $randomCode произвольный код, который нужно изобразить на картинке капчи
		 * @return mixed
		 */
		abstract public function draw($randomCode);

		/**
		 * Генерирует произвольный код
		 * @return string
		 */
		public function getRandomCode() {
			$lastIndex = strlen(self::alphabet) - 1;

			/** @var $alphabet сохранение в локальную переменную для совместимости с PHP 5.4 */
			$alphabet = self::alphabet;
			$code = '';

			for ($i = 0; $i < self::length; $i++) {
				$code .= $alphabet[mt_rand(0, $lastIndex)];
			}

			return $code;
		}

	}
