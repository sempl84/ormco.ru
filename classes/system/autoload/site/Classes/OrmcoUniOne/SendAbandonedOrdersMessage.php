<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class OrmcoUniOneSendAbandonedOrdersMessage
{
    private $id;
    
    private $action;
    
    const action_prepare = 'prepare';
    const action_send = 'send';
    
    private $params = array();
    
    const param_time_start = 'time_start';
    const param_time_end = 'time_end';
    const param_order_id = 'order_id';
    
    const param_template_1_id = 'template_1_id';
    const param_template_1_offset = 'template_1_offset';
    const param_template_1_test_email = 'template_1_test_email';
    
    const param_template_2_id = 'template_2_id';
    const param_template_2_offset = 'template_2_offset';
    const param_template_2_test_email = 'template_2_test_email';
    
    /**
     * @var ormcoUniOne|OrmcoUniOneMacrosAbandonedOrders
     */
    private $ormcoUniOne;
    
    private $isDone = false;
    
    private $checkLock = false;
    
    private $start = 0;
    
    /**
     * @throws publicException
     * @throws coreException
     */
    public function __construct()
    {
        outputBuffer::current(CRON == 'CLI' ? 'CLIOutputBuffer' : 'HTTPOutputBuffer');
        
        $this->ormcoUniOne = cmsController::getInstance()->getModule('ormcoUniOne');
        if (!$this->ormcoUniOne instanceof ormcoUniOne) {
            throw new publicException('Не найден модуль ormcoUniOne');
        }
        
        $this->loadParams();
        
        $this->start = microtime(true);
    }
    
    public function init($timeStart = null, $timeEnd = null)
    {
        $this->clearLog();
        
        $this->id = time();
        $this->action = self::action_prepare;
        $this->params = array();
        
        $offsetMultiplier = 3600;
        
        $registry = regedit::getInstance();
        
        $template1Id = $registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_id);
        $template1Offset = intval($registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_offset)) * $offsetMultiplier;
        
        if ($template1Id && $template1Offset) {
            $this->params[self::param_template_1_test_email] = $registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_1_test_email);
        }
    
        $this->params[self::param_template_1_id] = $template1Id;
        $this->params[self::param_template_1_offset] = $template1Offset;
        
        $template2Id = $registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_id);
        $template2Offset = intval($registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_offset)) * $offsetMultiplier;
        
        if ($template2Id && $template2Offset) {
            $this->params[self::param_template_2_test_email] = $registry->getVal(OrmcoUniOneMacrosAbandonedOrders::registry_param_template_2_test_email);
        }
    
        $this->params[self::param_template_2_id] = $template2Id;
        $this->params[self::param_template_2_offset] = $template2Offset;
    
        if (!$timeStart || !$timeEnd) {
            if ($template1Offset && $template2Offset) {
                $minOffset = min($template1Offset, $template2Offset);
                $maxOffset = max($template1Offset, $template2Offset);
            } elseif ($template1Offset) {
                $minOffset = $template1Offset;
                $maxOffset = $template1Offset;
            } elseif ($template2Offset) {
                $minOffset = $template2Offset;
                $maxOffset = $template2Offset;
            } else {
                $minOffset = 0;
                $maxOffset = 0;
            }
            
            $timeEnd = time() - $minOffset;
            $timeStart = $timeEnd - $maxOffset - $offsetMultiplier;
            
            $minTimeOffset = 86400 * 2;
            
            if(($timeEnd - $timeStart) < $minTimeOffset) {
                $timeStart = $timeEnd - $minTimeOffset;
            }
        } else {
            $timeStart = min($timeStart, $timeEnd);
            $timeEnd = max($timeStart, $timeEnd);
        }
        
        // Корректируем время до целых минут
        $timeStart = SiteDateHelper::normalizeToMinute($timeStart);
        $timeEnd = SiteDateHelper::normalizeToMinute($timeEnd) + 60;
        
        $this->params[self::param_time_start] = $timeStart;
        $this->params[self::param_time_end] = $timeEnd;
        
        $this->saveParams();
        $this->addLog('Запущена отправка данных о брошенных корзинах');
    }
    
    public function process()
    {
        $bProcess = true;
        
        if ($this->checkLock && $this->getLock()) {
            $bProcess = false;
        }
        
        if ($bProcess) {
            $this->setLock();
            
            try {
                do {
                    switch ($this->action) {
                        case self::action_prepare:
                        {
                            $this->doPrepare();
                            break;
                        }
                        case self::action_send:
                        {
                            $this->doSend();
                            break;
                        }
                    }
                } while (!$this->isDone() && $this->hasTimeForNextAction());
            } catch (Exception $e) {
                $this->addError('Ошибка: ' . $e->getMessage());
            }
            
            $this->removeLock();
        } else {
            $this->addLog('Скрипт уже запущен');
        }
    }
    
    public function isDone()
    {
        return $this->isDone;
    }
    
    /**
     * @throws publicException
     * @throws selectorException
     */
    private function doPrepare()
    {
        $timeStart = $this->params[self::param_time_start];
        $timeEnd = $this->params[self::param_time_end];
        
        $this->addLog('Получаем список брошенных корзин с ' . date('d.m.Y H:i:s', $timeStart) . ' по ' . date('d.m.Y H:i:s', $timeEnd));
        
        $sel = $this->ormcoUniOne->_buildAbandonedOrdersSelector($timeStart, $timeEnd);
        $sel->option('return', 'id');
        
        $arOrderId = array();
        foreach ($sel as $row) {
            $arOrderId[] = intval($row['id']);
        }
        
        if (count($arOrderId)) {
            $this->params[self::param_order_id] = $arOrderId;
            $this->action = self::action_send;
            $this->saveParams();
            
            $this->addLog('Найдено брошенных корзин - ' . count($arOrderId));
        } else {
            $this->finish('Не найдены заказы для отправки');
        }
    }
    
    /**
     * @return void
     */
    private function doSend()
    {
        $arOrderId = getArrayKey($this->params, self::param_order_id);
        if (!is_array($arOrderId)) {
            $this->addError('Не найден параметр ' . self::param_order_id);
            $this->finish();
            return;
        }
        
        $limit = 100;
        $arCheckOrderId = array_splice($arOrderId, 0, $limit);
        
        $arTemplate1OrderObjects = array();
        $arTemplate2OrderObjects = array();
        
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        foreach ($arCheckOrderId as $orderId) {
            $orderObject = $umiObjectsCollection->getObject($orderId);
            if (!$orderObject instanceof umiObject) {
                continue;
            }
            
            if (!$this->ormcoUniOne->_canSendAbandonedOrderMessage($orderObject)) {
                $this->addLog('Корзина ' . $orderId . ' не соответствует критериям для отправки сообщения о брошенной корзине');
                continue;
            }
            
            if ($this->params[self::param_template_1_id] && $this->ormcoUniOne->_canSendAbandonedOrderTemplateMessageByMessageNumber($orderObject, 1) && $this->ormcoUniOne->_canSendAbandonedOrderTemplateMessageByDate($orderObject, $this->params[self::param_template_1_offset])) {
                $arTemplate1OrderObjects[] = $orderObject;
            } elseif($this->params[self::param_template_2_id] && $this->ormcoUniOne->_canSendAbandonedOrderTemplateMessageByMessageNumber($orderObject, 2)) {
                $template2Offset = $this->params[self::param_template_2_offset];
                
                $messageDate = $orderObject->getValue(SiteEmarketOrderModel::field_abandoned_orders_message_date);
                
                if($messageDate instanceof umiDate) {
                    $messageDateTime = SiteDateHelper::normalizeToMinute($messageDate->getDateTimeStamp());
                    $orderUpdateTime = SiteDateHelper::normalizeToMinute($orderObject->getUpdateTime());
                    
                    if($messageDateTime == $orderUpdateTime) {
                        $template2Offset = $template2Offset - $this->params[self::param_template_1_offset];
                    }
                }
                
                if ($template2Offset > 0 && $this->ormcoUniOne->_canSendAbandonedOrderTemplateMessageByDate($orderObject, $template2Offset)) {
                    $arTemplate2OrderObjects[] = $orderObject;
                }
            }
        }
        
        if (count($arTemplate1OrderObjects)) {
            $this->sendAbandonedOrdersMessageByTemplate($arTemplate1OrderObjects, $this->params[self::param_template_1_id], 1, $this->params[self::param_template_1_test_email]);
        }
    
        if (count($arTemplate2OrderObjects)) {
            $this->sendAbandonedOrdersMessageByTemplate($arTemplate2OrderObjects, $this->params[self::param_template_2_id], 2, $this->params[self::param_template_2_test_email]);
        }
        
        if (count($arOrderId)) {
            $this->params[self::param_order_id] = $arOrderId;
            $this->saveParams();
        } else {
            $this->finish('Отправка завершена');
        }
    }
    
    /**
     * @param $arOrderObject
     * @param $templateId
     * @param $messageNumber
     * @param $testEmailString
     */
    private function sendAbandonedOrdersMessageByTemplate($arOrderObject, $templateId, $messageNumber, $testEmailString)
    {
        $arAllowedEmail = array();
        
        if($testEmailString) {
            $arTestEmail = explode(',', $testEmailString);
            foreach ($arTestEmail as $email) {
                $email = trim($email);
                if (!$email) {
                    continue;
                }
    
                $arAllowedEmail[] = $email;
            }
        }
        
        $umiObjectsCollection = umiObjectsCollection::getInstance();
    
        $arSentObjectId = $this->ormcoUniOne->_sendAbandonedOrdersEmail($arOrderObject, $templateId, $arAllowedEmail);
        if(is_array($arSentObjectId) && count($arSentObjectId)) {
            foreach ($arSentObjectId as $objectId) {
                $object = $umiObjectsCollection->getObject($objectId);
                if (!$object instanceof umiObject) {
                    continue;
                }
        
                $this->addLog('Отправлено письмо #' . $messageNumber . ' для корзины ' . $objectId);
        
                $object->setValue(SiteEmarketOrderModel::field_abandoned_orders_message_number, $messageNumber);
                $object->setValue(SiteEmarketOrderModel::field_abandoned_orders_message_date, time());
                $object->commit();
            }
        }
    }
    
    public function setCheckLock($checkLock)
    {
        $this->checkLock = $checkLock === true;
    }
    
    public function isActive()
    {
        return (bool)$this->id;
    }
    
    private function finish($message = '')
    {
        $this->isDone = true;
        
        $this->addLog($message);
        
        $file = $this->getParamsFile();
        if (file_exists($file)) {
            unlink($file);
        }
        
        unset($this->id);
    }
    
    private function loadParams()
    {
        $file = $this->getParamsFile();
        if (!file_exists($file)) {
            return;
        }
        
        $params = json_decode(file_get_contents($file), true);
        if (!is_array($params)) {
            return;
        }
        
        $this->id = getArrayKey($params, 'id');
        $this->action = getArrayKey($params, 'action');
        $this->params = getArrayKey($params, 'params');
    }
    
    private function saveParams()
    {
        $file = $this->getParamsFile();
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0775, true);
        }
        
        file_put_contents($file, json_encode(array(
            'id' => $this->id,
            'action' => $this->action,
            'params' => $this->params
        )));
        
        chmod($file, 0777);
    }
    
    private function getParamsFile()
    {
        return $this->getBaseDir() . '/params.json';
    }
    
    private $log = array();
    
    private function addLog($message = '')
    {
        if (!$this->id || !$message) {
            return;
        }
        
        $this->log[] = $message;
        
        $file = $this->getLogDir() . '/' . date('Ymd', $this->id) . '.log';
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0775, true);
        }
        
        file_put_contents($file, date('H:i:s') . ': ' . $message . PHP_EOL, FILE_APPEND);
    }
    
    private $hasError = false;
    
    private function addError($message)
    {
        $this->hasError = true;
        
        $this->addLog($message);
    }
    
    public function hasError()
    {
        return $this->hasError;
    }
    
    public function getLog()
    {
        return $this->log;
    }
    
    private function clearLog()
    {
        $dir = $this->getLogDir();
        if (!is_dir($dir)) {
            return;
        }
        
        /* Логи хранятся 15 дней*/
        $time = time() - (86400 * 15);
        
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
            
            if ($file->getMTime() < $time) {
                unlink($file->getRealPath());
            }
        }
    }
    
    private static function getBaseDir()
    {
        return CURRENT_WORKING_DIR . '/sys-temp/ormcoUnione/abandoned_orders';
    }
    
    public static function getLogDir()
    {
        return self::getBaseDir() . '/log';
    }
    
    private function getLock()
    {
        return is_file($this->getLockFile());
    }
    
    private function removeLock()
    {
        $file = $this->getLockFile();
        if (file_exists($file)) {
            unlink($file);
        }
    }
    
    private function setLock()
    {
        file_put_contents($this->getLockFile(), time());
    }
    
    private function getLockFile()
    {
        return $this->getBaseDir() . '/.lock';
    }
    
    private function hasTimeForNextAction()
    {
        return (microtime(true) - $this->start) <= 50;
    }
    
    public function testFinish()
    {
        $this->addError('Тестовая ошибка');
        $this->finish();
    }
}