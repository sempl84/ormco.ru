<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class OrmcoUniSendersSyncContacts
{
    private $id;
    
    private $action;
    
    const action_sync = 'sync';
    const action_export = 'export';
    
    private $params = array();
    
    const param_email_list_id = 'email_list_id';
    const param_test_email = 'test_email';
    const param_user_id = 'user_id';
    
    /**
     * @var ormcoUniSender|OrmcoUniSenderMacros
     */
    private $ormcoUniSender;
    
    private $isDone = false;
    
    private $checkLock = false;
    
    private $start = 0;
    
    /**
     * @throws publicException
     * @throws coreException
     */
    public function __construct()
    {
        outputBuffer::current(defined('CRON') && CRON == 'CLI' ? 'CLIOutputBuffer' : 'HTTPOutputBuffer');
        
        $this->ormcoUniSender = cmsController::getInstance()->getModule('ormcoUniSender');
        if (!$this->ormcoUniSender instanceof ormcoUniSender) {
            throw new publicException('Не найден модуль ormcoUniSender');
        }
        
        $this->loadParams();
        
        $this->start = microtime(true);
    }
    
    public function init($emailListId, $testEmail = null)
    {
        $this->clearLog();
        
        $this->id = time();
        
        $this->action = self::action_sync;
        
        $this->params = array(
            self::param_email_list_id => $emailListId,
            self::param_user_id => array()
        );
        
        if(!is_array($testEmail)) {
            $testEmail = trim($testEmail);
        }
        
        if($testEmail) {
            $arTestEmail = array();
            
            if(is_array($testEmail)) {
                foreach($testEmail as $email) {
                    if(!umiMail::checkEmail($email)) {
                        continue;
                    }
                    
                    $arTestEmail[] = $email;
                }
            } elseif(umiMail::checkEmail($testEmail)) {
                $arTestEmail[] = $testEmail;
            }
            
            if(count($arTestEmail)) {
                $testEmail = $arTestEmail;
            } else {
                $testEmail = null;
            }
        } else {
            $testEmail = null;
        }
        
        $this->params[self::param_test_email] = $testEmail;
        
        $this->saveParams();
    
        $this->addLog('Запущена синхронизация данных');
    }
    
    public function process()
    {
        $bProcess = true;
        
        if ($this->checkLock && $this->getLock()) {
            $bProcess = false;
        }
        
        if ($bProcess) {
            $this->setLock();
            
            try {
                do {
                    switch ($this->action) {
                        case self::action_sync:
                        {
                            $this->doSync();
                            break;
                        }
                        case self::action_export:
                        {
                            $this->doExport();
                            break;
                        }
                    }
                } while (!$this->isDone() && $this->hasTimeForNextAction());
            } catch (Exception $e) {
                $this->addError('Ошибка: ' . $e->getMessage());
            }
            
            $this->removeLock();
        } else {
            $this->addLog('Скрипт уже запущен');
        }
    }
    
    public function isDone()
    {
        return $this->isDone;
    }
    
    /**
     * @throws publicException
     * @throws selectorException
     */
    private function doSync()
    {
        $sel = $this->buildSelector();
    
        $lastUserId = intval(getArrayKey($this->params, 'last_user_id'));
        $sel->where('id')->more($lastUserId);
        
        $limit = 20;
        $sel->limit(0, $limit);
        $sel->order('id')->asc();
        
        $result = $sel->result();
    
        if($lastUserId === 0) {
            $this->addLog('Получаем список пользователей для синхронизации');
        }
    
        $arUserId = getArrayKey($this->params, self::param_user_id);
    
        if ($result) {
            if (!is_array($arUserId)) {
                $arUserId = array();
            }
            
            foreach ($sel as $user) {
                if (!$user instanceof umiObject) {
                    continue;
                }
                
                $email = $user->getValue(SiteUsersUserModel::field_email);
                
                $contact = $this->ormcoUniSender->getApi()->getContact($email);
                if ($contact instanceof UniSenderApiContactModel) {
                    $user->setValue(SiteUsersUserModel::field_uni_sender_exported, true);
                    $user->commit();
                    $this->addLog('Пользователь ' . $email . ' есть в базе UniSender');
                } else {
                    $arUserId[] = $user->getId();
                }
                
                $lastUserId = $user->getId();
            }

            $this->params[self::param_user_id] = $arUserId;
            $this->params['last_user_id'] = $lastUserId;
    
            $this->saveParams();
        } else {
            if(count($arUserId)) {
                $this->addLog('Получен список пользователей для синхронизации');
                $this->action = self::action_export;
                $this->saveParams();
            } else {
                $this->finish('Не найден список пользователей для синхронизации');
            }
        }
    }
    
    /**
     * @throws publicException
     * @throws selectorException
     */
    private function doExport()
    {
        $emailListId = getArrayKey($this->params, self::param_email_list_id);
        if(!$emailListId) {
            $this->finish('Не выбран список для импорта контактов');
            return;
        }
        
        $arUserId = getArrayKey($this->params, self::param_user_id);
        if ($arUserId) {
            $limit = 50;
            
            $arExportUserId = array_splice($arUserId, 0, $limit);
            
            $sel = $this->buildSelector();
            $sel->where('id')->equals($arExportUserId);
            
            $contacts = array();
            $arUserEmail = array();
            
            foreach($sel as $user) {
                if(!$user instanceof umiObject) {
                    continue;
                }
                
                $contact = $this->ormcoUniSender->getContactFromUser($user);
                if(!$contact instanceof UniSenderApiContactModel) {
                    $this->addLog('Ошибка при получении контакта для пользователя ' . $user->getId());
                    continue;
                }
    
                $arUserEmail[$user->getId()] = $user->getValue(SiteUsersUserModel::field_email);
    
                $contact->addEmailListId($emailListId);
                
                $contacts[] = $contact;
            }
            
            if($contacts) {
                $this->ormcoUniSender->getApi()->importContacts($contacts);
    
                $umiObjectsCollection = umiObjectsCollection::getInstance();
                
                foreach($arUserEmail as $userId => $email) {
                    if(!$this->ormcoUniSender->getApi()->getContact($email)) {
                        continue;
                    }
                    
                    $user = $umiObjectsCollection->getObject($userId);
                    $user->setValue(SiteUsersUserModel::field_uni_sender_exported, true);
                    $user->commit();
                    
                    $this->addLog('Отправлены данные для пользователя ' . $email . ' (' . $userId . ')');
                }
            }
            
            $this->params[self::param_user_id] = $arUserId;
        } else {
            $this->finish('Синхронизация завершена');
        }
    }
    
    /**
     * @return selector
     * @throws selectorException
     */
    private function buildSelector()
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteUsersUserModel::module, SiteUsersUserModel::method);
        $sel->where(SiteUsersUserModel::field_is_activated)->equals(1);
        $sel->where('groups')->equals(334);
        $sel->where('id')->notequals(permissionsCollection::getGuestId());
        $sel->where(SiteUsersUserModel::field_uni_sender_exported)->notequals(1);
        
        $testEmail = getArrayKey($this->params, self::param_test_email);
        if(is_array($testEmail)) {
            $sel->where(SiteUsersUserModel::field_email)->equals($testEmail);
        }
        
        return $sel;
    }
    
    public function setCheckLock($checkLock)
    {
        $this->checkLock = $checkLock === true;
    }
    
    public function isActive()
    {
        return (bool)$this->id;
    }
    
    private function finish($message = '')
    {
        $this->isDone = true;
        
        $this->addLog($message);
        
        $file = $this->getParamsFile();
        if (file_exists($file)) {
            unlink($file);
        }
        
        unset($this->id);
    }
    
    private function loadParams()
    {
        $file = $this->getParamsFile();
        if (!file_exists($file)) {
            return;
        }
        
        $params = json_decode(file_get_contents($file), true);
        if (!is_array($params)) {
            return;
        }
        
        $this->id = getArrayKey($params, 'id');
        $this->action = getArrayKey($params, 'action');
        $this->params = getArrayKey($params, 'params');
    }
    
    private function saveParams()
    {
        $file = $this->getParamsFile();
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0777, true);
        }
        
        file_put_contents($file, json_encode(array(
            'id' => $this->id,
            'action' => $this->action,
            'params' => $this->params
        )));
        
        chmod($file, 0777);
    }
    
    private function getParamsFile()
    {
        return $this->getBaseDir() . '/params.json';
    }
    
    private $log = array();
    
    private function addLog($message = '')
    {
        if (!$this->id || !$message) {
            return;
        }
        
        $this->log[] = $message;
        
        $file = $this->getLogDir() . '/' . date('Ymd', $this->id) . '.log';
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0777, true);
        }
        
        file_put_contents($file, date('H:i:s') . ': ' . $message . PHP_EOL, FILE_APPEND);
        
        chmod($file, 0777);
    }
    
    private $hasError = false;
    
    private function addError($message)
    {
        $this->hasError = true;
        
        $this->addLog($message);
    }
    
    public function hasError()
    {
        return $this->hasError;
    }
    
    public function getLog()
    {
        return $this->log;
    }
    
    private function clearLog()
    {
        $dir = $this->getLogDir();
        if (!is_dir($dir)) {
            return;
        }
        
        /* Логи хранятся 15 дней*/
        $time = time() - (60 * 60 * 24 * 15);
        
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
            
            if ($file->getMTime() < $time) {
                unlink($file->getPath());
            }
        }
    }
    
    private static function getBaseDir()
    {
        return CURRENT_WORKING_DIR . '/sys-temp/ormcoUniSender/sync_contacts';
    }
    
    public static function getLogDir()
    {
        return self::getBaseDir() . '/log';
    }
    
    private function getLock()
    {
        return is_file($this->getLockFile());
    }
    
    private function removeLock()
    {
        $file = $this->getLockFile();
        if (file_exists($file)) {
            unlink($file);
        }
    }
    
    private function setLock()
    {
        file_put_contents($this->getLockFile(), time());
        
        chmod($this->getLockFile(), 0777);
    }
    
    private function getLockFile()
    {
        return $this->getBaseDir() . '/.lock';
    }
    
    private function hasTimeForNextAction()
    {
        return (microtime(true) - $this->start) <= 50;
    }
    
    public function testFinish()
    {
        $this->addError('Тестовая ошибка');
        $this->finish();
    }
}