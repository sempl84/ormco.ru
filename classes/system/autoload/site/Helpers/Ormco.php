<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class OrmcoHelper
{
    const catalog_page_id = 4;
    const discount_page_id = 293;
    const configurator_page_id = 13574;
    const news_page_id = 5;
    
    public static function isDev()
    {
        if(cmsController::getInstance()->getCurrentDomain()->getCurrentHostName() === 'ormco-test3.dpromo.su') {
            return true;
        }
        
        return false;
    }
}