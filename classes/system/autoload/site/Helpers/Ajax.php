<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class AjaxHelper
{
    public static function sendResponse($response)
    {
        $buffer = outputBuffer::current();
        /* @var HTTPOutputBuffer $buffer */
        $buffer->contentType('text/javascript');
        $buffer->option('generation-time', false);
        
        if (is_array($response)) {
            $response = json_encode($response);
        }
        
        $buffer->push($response);
        $buffer->end();
        
        exit;
    }
    
    public static function isAjaxRequest()
    {
        return getRequest('xhr') || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest');
    }
}