<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

/**
 * Вспомогательный класс для рендера скидки
 */
class SiteRenderOrderItemDiscountHelper
{
    private $discountAmount;
    private $couponDiscountValue = 0;
    private $orderItemAmount = 0;
    
    /**
     * @var itemDiscount $itemDiscount
     */
    private $itemDiscount;
    
    private $couponId;
    private $couponDiscountPercent;
    private $couponCode;
    private $couponFocusDiscount;
    private $couponActiveTo;
    
    public function render()
    {
        if (!$this->discountAmount) {
            return null;
        }
        
        $return = array();
        
        $totalDiscountPercent = 0;
        $totalCouponDiscountValue = $this->couponDiscountValue * $this->orderItemAmount;
        
        if ($this->itemDiscount instanceof itemDiscount && ($this->discountAmount != $totalCouponDiscountValue)) {
            $itemDiscountPercent = $this->itemDiscount->getDiscountModificator()->getValue('proc');
            $totalDiscountPercent += $itemDiscountPercent;
            $return['user'] = array(
                'attribute:id' => $this->itemDiscount->getId(),
                'attribute:percent' => $itemDiscountPercent,
                'attribute:name' => $this->itemDiscount->getName(),
                'node:value' => $this->discountAmount - $totalCouponDiscountValue
            );
        }
        
        if ($this->couponDiscountValue) {
            $totalDiscountPercent += $this->couponDiscountPercent;
            
            $coupon = array(
                'attribute:id' => $this->couponId,
                'attribute:percent' => $this->couponDiscountPercent,
                'attribute:code' => $this->couponCode,
                'attribute:focus-discount' => $this->couponFocusDiscount ? 1 : 0,
                'attribute:value' => $this->couponDiscountValue,
                'total' => $this->couponDiscountValue * $this->orderItemAmount
            );
            
            if($this->couponActiveTo) {
                $daysLeft = SiteOrmcoCouponsCouponModel::getDaysLeft($this->couponActiveTo);
                $coupon['active_to'] = array(
                    'attribute:timestamp' => $this->couponActiveTo,
                    'attribute:days-left' => $daysLeft . ' ' . SiteStringHelper::getNumEnding($daysLeft, 'день', 'дня', 'дней'),
                    'node:value' => date('d.m.Y H:i:s', $this->couponActiveTo)
                );
            }
            
            $return['coupon'] = $coupon;
        }
        
        $return['total'] = array(
            'attribute:percent' => $totalDiscountPercent,
            'node:value' => $this->discountAmount
        );
        
        return $return;
    }
    
    /**
     * @param mixed $discountAmount
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;
    }
    
    /**
     * @param int $couponDiscountValue
     */
    public function setCouponDiscountValue($couponDiscountValue)
    {
        $this->couponDiscountValue = $couponDiscountValue;
    }
    
    /**
     * @param int $orderItemAmount
     */
    public function setOrderItemAmount($orderItemAmount)
    {
        $this->orderItemAmount = $orderItemAmount;
    }
    
    /**
     * @param mixed $itemDiscount
     */
    public function setItemDiscount(itemDiscount $itemDiscount)
    {
        $this->itemDiscount = $itemDiscount;
    }
    
    /**
     * @param mixed $couponId
     */
    public function setCouponId($couponId)
    {
        $this->couponId = $couponId;
    }
    
    /**
     * @param mixed $couponDiscountPercent
     */
    public function setCouponDiscountPercent($couponDiscountPercent)
    {
        $this->couponDiscountPercent = $couponDiscountPercent;
    }
    
    /**
     * @param mixed $couponCode
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    }
    
    /**
     * @param mixed $couponFocusDiscount
     */
    public function setCouponFocusDiscount($couponFocusDiscount)
    {
        $this->couponFocusDiscount = $couponFocusDiscount ? true : false;
    }
    
    /**
     * @param mixed $couponActiveTo
     */
    public function setCouponActiveTo($couponActiveTo)
    {
        $this->couponActiveTo = $couponActiveTo;
    }
}