<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteDateHelper
{
    /**
     * Коррекция времени до ближайшей минуты
     *
     * @param $date
     * @return int
     */
    public static function normalizeToMinute($date)
    {
        return $date - intval(date('s', $date));
    }
}