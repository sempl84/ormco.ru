<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteEmarketHelper
{
    public static function calculateDiscountPercent($discountPrice, $originalPrice)
    {
        return 100 - ceil($discountPrice * 100 / $originalPrice);
    }
}