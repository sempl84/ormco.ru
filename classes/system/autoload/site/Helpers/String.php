<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteStringHelper
{
    public static function getNumEnding($number, $one = '', $two = '', $five = '')
    {
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $five;
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $one;
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $two;
                    break;
                default:
                    $ending = $five;
            }
        }
        return $ending;
    }
}