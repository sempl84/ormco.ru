<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteGoogleAnalyticsHelper
{
    public static function getClientId()
    {
        $cid = explode('.', Service::CookieJar()->get('_ga'));
        if(count($cid) == 4) {
            unset($cid[0]);
            unset($cid[1]);
            $cid = implode('.', $cid);
        } else {
            $cid = 1765107134.162330742622222;
        }
        
        return $cid;
    }
}