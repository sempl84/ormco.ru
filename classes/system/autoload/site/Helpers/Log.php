<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteLogHelper
{
    public static function clearLogDir($dir, $validTo)
    {
        if(!is_dir($dir)) {
            return false;
        }
        
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
        
            if ($file->getCTime() < $validTo) {
                unlink($file->getRealPath());
            }
        }
        
        return true;
    }
}