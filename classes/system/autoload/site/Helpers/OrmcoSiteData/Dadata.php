<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class OrmcoSiteDataDadataHelper
{
    const registry_param_token = '//modules/ormcoSiteData/dadata/token';
    
    public static function isDadataEnabled()
    {
        return (bool)regedit::getInstance(self::registry_param_token);
    }
    
    public static function validateData($address, $data)
    {
        if(!$address) {
            throw new publicException('Не передан адрес');
        }
    
        if(!is_array($data) || !isset($data['value']) || !isset($data['data'])) {
            throw new publicException('Не переданы данные dadata');
        }
    
        if(mb_strtolower($data['value']) != mb_strtolower($address)) {
            throw new publicException('Неверные данные dadata');
        }
    }
    
    public static function setUserAddressData(umiObject $user, $data)
    {
        $user->setValue(SiteUsersUserModel::field_address_postal_code, getArrayKey($data['data'], 'postal_code'));
        $user->setValue(SiteUsersUserModel::field_address_country, getArrayKey($data['data'], 'country'));
        $user->setValue(SiteUsersUserModel::field_address_country_iso, getArrayKey($data['data'], 'country_iso_code'));
        $user->setValue(SiteUsersUserModel::field_address_region, getArrayKey($data['data'], 'region_with_type'));
        $user->setValue(SiteUsersUserModel::field_address_area, getArrayKey($data['data'], 'area_with_type'));
        $user->setValue(SiteUsersUserModel::field_address_city, getArrayKey($data['data'], 'city_with_type'));
        $user->setValue(SiteUsersUserModel::field_address_settlement, getArrayKey($data['data'], 'settlement_with_type'));
        $user->setValue(SiteUsersUserModel::field_address_street, getArrayKey($data['data'], 'street_with_type'));
        $user->setValue(SiteUsersUserModel::field_address_house, getArrayKey($data['data'], 'house'));
        
        $blockType = getArrayKey($data['data'], 'block_type');
        $block = getArrayKey($data['data'], 'block');
        $user->setValue(SiteUsersUserModel::field_address_corpus, $blockType === 'корпус' ? $block : '');
        $user->setValue(SiteUsersUserModel::field_address_building, $blockType === 'стр' ? $block : '');
        $user->setValue(SiteUsersUserModel::field_address_liter, $blockType === 'литера' ? $block : '');
        
        $flatType = getArrayKey($data['data'], 'flat_type');
        $flat = getArrayKey($data['data'], 'flat');
        $user->setValue(SiteUsersUserModel::field_address_room, $flatType === 'помещ' ? $flat : '');
        $user->setValue(SiteUsersUserModel::field_address_flat, $flatType === 'кв' ? $flat : '');
        $user->setValue(SiteUsersUserModel::field_address_office, $flatType === 'офис' ? $flat : '');
        
        $user->setValue(SiteUsersUserModel::field_address_raw, $data['unrestricted_value']);
    }
}