<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class OrmcoSiteDataSmsHelper
{
    const registry_param_bytehand_token = '//modules/ormcoSiteData/sms/bytehand/token';
    const registry_param_bytehand_from = '//modules/ormcoSiteData/sms/bytehand/from';
    
    public static function isSmsEnabled()
    {
        return (bool)regedit::getInstance(self::registry_param_bytehand_token);
    }
    
    public static function sendSms($phone, $message, $from = '')
    {
        $token = trim(regedit::getInstance()->getVal(self::registry_param_bytehand_token));
        if (!$token) {
            throw new publicException('Не указан token');
        }
        
        if (!$from) {
            $from = regedit::getInstance()->getVal(self::registry_param_bytehand_from);
            
            if (!$from) {
                $from = 'SMS-INFO';
            }
        }
        
        $data = array(
            'receiver' => $phone,
            'text' => $message
        );
        
        if ($from) {
            $data['sender'] = $from;
        }
        
        $ch = curl_init('https://api.bytehand.com/v2/sms/messages');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $headers = array(
            'Content-Type: application/json;charset=UTF-8',
            'X-Service-Key:' . $token
        );
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $response = curl_exec($ch);
        
        return $response;
    }
}