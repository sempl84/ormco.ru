<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteCatalogCategoryModel
{
    const module = 'catalog';
    const method = 'category';
    
    const group_choose_brackets_block = 'category_choose_brackets_block';
    const field_choose_brackets_block_hide_button = 'category_choose_brackets_block_hide_button';
}