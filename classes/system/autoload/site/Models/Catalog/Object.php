<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteCatalogObjectModel
{
    const module = 'catalog';
    const method = 'object';
    
    const field_photo = 'photo';
    
    const field_price = 'price';
    
    const field_tork = 'tork';
    
    const field_article = 'artikul';
}