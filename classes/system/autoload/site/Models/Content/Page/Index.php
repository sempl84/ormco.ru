<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteContentPageIndexModel
{
    const object_type_id = 291;
    
    const field_categories = 'category_on_main';
    const field_products = 'objects_on_main';
}