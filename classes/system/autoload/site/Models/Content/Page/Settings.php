<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteContentPageSettingsModel
{
    const object_type_id = 137;
    
    const group_discounts = 'config_discounts';
    const field_discounts_personal_title = 'config_discounts_personal_title';
    const field_discounts_personal_content = 'config_discounts_personal_content';
    
    const group_coupons = 'config_coupons';
    const field_coupons_multiple_add_to_cart_content = 'config_coupons_multiple_add_to_cart_content';
    const field_coupons_multiple_cart_content = 'config_coupons_multiple_cart_content';
    
    const group_coupons_notification = 'config_coupons_notification';
    const field_coupons_notification_sender_email = 'config_coupons_notification_sender_email';
    const field_coupons_notification_sender_name = 'config_coupons_notification_sender_name';
    const field_coupons_notification_subject = 'config_coupons_notification_subject';
    const field_coupons_notification_content = 'config_coupons_notification_content';
    const field_coupons_notification_debug_emails = 'config_coupons_notification_debug_emails';
    
    const group_counters = 'config_counters';
    const field_counters_gtm_id = 'config_counters_gtm_id';
    const field_counters_gtm_account_id = 'config_counters_gtm_account_id';
    const field_counters_gtm_refund_source_id = 'config_counters_gtm_refund_source_id';
    
    const group_redesign_pages = 'index_redesign_pages';
    const field_redesign_pages_policy = 'index_redesign_pages_policy';
    
    const group_redesign_warn = 'index_redesign_warn';
    const field_redesign_warn_is_active = 'index_redesign_warn_is_active';
    const field_redesign_warn_title = 'index_redesign_warn_title';
    const field_redesign_warn_content = 'index_redesign_warn_content';
    const field_redesign_warn_link = 'index_redesign_warn_link';
    
    const group_contacts = 'config_contacts';
    const field_contacts_email = 'config_contacts_email';
    
    /**
     * @return umiHierarchyElement|false
     * @throws selectorException
     */
    public static function getPage()
    {
        static $page = null;
        
        if(is_null($page)) {
            $sel = new selector('pages');
            $sel->types('object-type')->id(self::object_type_id);
            $sel->where('is_active')->equals(1);
            $sel->order('id')->asc();
            $sel->limit(0, 1);
            
            $page = getArrayKey($sel->result(), 0);
        }
        
        return $page instanceof umiHierarchyElement ? $page : false;
    }
}