<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteNewsItemModel
{
    const module = 'news';
    const method = 'item';
    
    const field_anons_pic = 'anons_pic';
}