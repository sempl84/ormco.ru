<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteUsersUserModel
{
    const module = 'users';
    const method = 'user';
    
    const field_is_activated = 'is_activated';
    const field_email = 'e-mail';
    
    const group_info = 'short_info';
    const field_name = 'fname';
    const field_surname = 'lname';
    const field_father_name = 'father_name';
    
    const field_prof_status = 'prof_status';
    const prof_status_id_other = 14645;
    const prof_status_id_not_doctor = 14646;
    const prof_status_id_student = 2737568;
    
    const field_country = 'country';
    const country_id_russia = 14531;
    const country_id_other = 14538;
    
    const field_region = 'region';
    const region_id_not_russia = 1884760;
    
    const field_city = 'city';
    const field_phone = 'phone';
    const field_other_specialization = 'other_specialization';
    
    const field_university = 'vuz';
    const university_id_other = 2548595;
    
    const field_common_uid = 'common_uid';
    
    const field_phone_valid = 'phone_valid';
    const field_phone_valid_code = 'phone_valide_code';
    
    const group_coupons = 'user_coupons';
    const field_coupons_system_field = 'user_coupons_system_field';
    
    const group_ga = 'user_ga';
    const field_ga_cid = 'user_ga_cid';
    
    const group_uni_sender = 'user_uni_sender';
    const field_uni_sender_exported = 'user_uni_sender_exported';
    
    const group_system = 'user_system';
    const field_system_normalized_phone = 'user_system_normalized_phone';
    
    const group_address = 'user_address';
    const field_address_postal_code = 'user_address_postal_code';
    const field_address_country = 'user_address_country';
    const field_address_country_iso = 'user_address_country_iso';
    const field_address_region = 'user_address_region';
    const field_address_area = 'user_address_area';
    const field_address_city = 'user_address_city';
    const field_address_settlement = 'user_address_settlement';
    const field_address_street = 'user_address_street';
    const field_address_house = 'user_address_house';
    const field_address_corpus = 'user_address_corpus';
    const field_address_building = 'user_address_building';
    const field_address_liter = 'user_address_liter';
    const field_address_room = 'user_address_room';
    const field_address_flat = 'user_address_flat';
    const field_address_office = 'user_address_office';
    const field_address_raw = 'user_address_raw';
    
    /**
     * @param $commonUid
     * @return bool|int
     * @throws selectorException
     */
    public static function getUserIdByCommonUid($commonUid)
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(self::module, self::method);
        $sel->where(self::field_common_uid)->equals($commonUid);
        $sel->order('id')->asc();
        $sel->limit(0, 1);
        $sel->option('return', 'id');
        
        $return = getArrayKey(getArrayKey($sel->result(), 0), 'id');
        if (!$return) {
            return false;
        }
        
        return intval($return);
    }
    
    public static function getUserFullName(umiObject $user)
    {
        $arName = array();
        
        if ($surname = trim($user->getValue(SiteUsersUserModel::field_surname))) {
            $arName[] = $surname;
        }
        
        if ($name = trim($user->getValue(SiteUsersUserModel::field_name))) {
            $arName[] = $name;
        }
        
        if ($fatherName = trim($user->getValue(SiteUsersUserModel::field_father_name))) {
            $arName[] = $fatherName;
        }
        
        return implode(' ', $arName);
    }
    
    public static function getUserIdByEmail($email)
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteUsersUserModel::module, SiteUsersUserModel::method);
        $sel->where(self::field_email)->equals($email);
        $sel->order('id')->asc();
        $sel->limit(0, 1);
        $sel->option('return', 'id');
        
        $result = $sel->result();
        return $result ? $result[0]['id'] : false;
    }
    
    public static function normalizePhone($phone)
    {
        $phone = trim(str_replace(array(' ', '-', '+', '(', ')', '_'), '', $phone));
        
        return strlen($phone) == 11 ? $phone : false;
    }
    
    public static function generatePassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
    
    public static function validateProfStatus(umiObject $user)
    {
        $otherSpecialization = false;
        $university = false;
        
        switch ($user->getValue(self::field_prof_status)) {
            case self::prof_status_id_other:
            {
                $otherSpecialization = $user->getValue(self::field_other_specialization);
                break;
            }
            case self::prof_status_id_student:
            {
                $university = $user->getValue(self::field_university);
                break;
            }
        }
        
        $user->setValue(self::field_other_specialization, $otherSpecialization);
        $user->setValue(self::field_university, $university);
    }
    
    public static function validateRegion(umiObject $user)
    {
        if ($user->getValue(self::field_country) != self::country_id_russia) {
            $region = $user->getValue(self::field_region);
        } else {
            $region = self::region_id_not_russia;
        }
        
        $user->setValue(self::field_region, $region);
    }
}