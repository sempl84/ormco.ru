<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteSliderItemElement
{
    const module = 'slider';
    const method = 'item_element';
    
    const group_redesign = 'slider_item_redesign';
    const field_redesign_is_active = 'slider_item_redesign_is_active';
    const field_redesign_title = 'slider_item_redesign_title';
    const field_redesign_text = 'slider_item_redesign_text';
    const field_redesign_button_text = 'slider_item_redesign_button_text';
    const field_redesign_image = 'slider_item_redesign_image';
    const field_redesign_link = 'slider_item_link';
}