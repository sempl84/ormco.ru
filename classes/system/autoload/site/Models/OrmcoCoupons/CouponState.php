<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteOrmcoCouponsCouponStateModel
{
    const module = 'ormcoCoupons';
    const method = 'ormcoCouponsCouponState';
    
    const code_active = 'active';
    const code_freeze = 'freeze';
    const code_inactive = 'inactive';
    
    const group_properties = 'coupon_state_properties';
    const field_code = 'coupon_state_code';
    
    public static function getCodeByObjectId($objectId)
    {
        $object = umiObjectsCollection::getInstance()->getObject($objectId);
        if (!$object instanceof umiObject) {
            return false;
        }
        
        return $object->getValue(self::field_code);
    }
    
    public static function getObjectIdByCode($code)
    {
        $sel = new selector('objects');
        $sel->types('object-type')->name(self::module, self::method);
        $sel->where(self::field_code)->equals($code);
        $sel->order('id')->asc();
        $sel->limit(0, 1);
        $sel->option('return', 'id');
        
        $objectId = getArrayKey(getArrayKey($sel->result(), 0), 'id');
        if(!$objectId) {
            return false;
        }

        return $objectId;
    }
}