<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteOrmcoCouponsHistoryModel
{
    const module = 'ormcoCoupons';
    const method = 'ormcoCouponsHistory';
    
    const group_properties = 'history_properties';
    const field_used = 'history_used';
    const field_date = 'history_date';
    const field_user_id = 'history_user_id';
    const field_1c_id = 'history_1c_id';
    
    const group_coupon = 'history_coupon_properties';
    const field_coupon_name = 'history_coupon_name';
    const field_coupon_percent = 'history_coupon_percent';
}