<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class SiteOrmcoCouponsCouponModel
{
    const module = 'ormcoCoupons';
    const method = 'ormcoCouponsCoupon';
    
    const group_properties = 'coupon_properties';
    const field_date_begin = 'coupon_date_begin';
    const field_date_end = 'coupon_date_end';
    const field_user_id = 'coupon_user_id';
    const field_code = 'coupon_code';
    const field_state = 'coupon_state';
    const field_log = 'coupon_log';
    
    const group_discount = 'coupon_discount_properties';
    const field_discount_base = 'coupon_discount_base';
    const field_discount_focus = 'coupon_discount_focus';
    const field_discount_focus_page_id = 'coupon_discount_focus_page_id';
    const field_discount_focus_categories = 'coupon_discount_focus_categories';
    const discount_focus_categories_param_name = 'name';
    const discount_focus_categories_param_link = 'link';
    
    const group_seminar = 'coupon_seminar_properties';
    const field_seminar_id = 'coupon_seminar_id';
    const field_seminar_name = 'coupon_seminar_name';
    
    public static function getUserActiveCoupons($userId)
    {
        static $arUserCoupons = array();
        
        if (!self::isCouponEnabledForUser($userId)) {
            return false;
        }
        
        if (!isset($arUserCoupons[$userId])) {
            $sel = new selector('objects');
            $sel->types('object-type')->name(self::module, self::method);
            $sel->where(self::field_date_begin)->eqless(time());
            $sel->where(self::field_date_end)->eqmore(time());
            $sel->where(self::field_user_id)->equals($userId);
            $sel->where(self::field_state)->equals(SiteOrmcoCouponsCouponStateModel::getObjectIdByCode(SiteOrmcoCouponsCouponStateModel::code_active));
            $sel->order(self::field_date_begin)->asc();
            
            $arUserCoupons[$userId] = $sel->result();
        }
        
        return $arUserCoupons[$userId];
    }
    
    /**
     * @param int $userId
     * @return false|umiObject
     */
    public static function getUserCoupon($userId = 0)
    {
        if (!$userId) {
            $userId = Service::Auth()->getUserId();
        }
        
        $coupons = self::getUserActiveCoupons($userId);
        
        return $coupons ? $coupons[0] : false;
    }
    
    /**
     * @param $pageId
     * @param int $userId
     * @return false|umiObject
     */
    public static function getUserCouponByPageId($pageId, $userId = 0)
    {
        if (!$userId) {
            $userId = Service::Auth()->getUserId();
        }
        
        $coupons = self::getUserActiveCoupons($userId);
        if (!$coupons) {
            return false;
        }
        
        $couponDiscount = 0;
        $couponDiscountObject = null;
        
        foreach ($coupons as $coupon) {
            if (!$coupon instanceof umiObject) {
                continue;
            }
            
            $baseDiscount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_base);
            if ($baseDiscount > $couponDiscount) {
                $couponDiscount = $baseDiscount;
                $couponDiscountObject = $coupon;
            }
            
            if (self::isCouponDiscountFocusPage($coupon, $pageId)) {
                $focusDiscount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus);
                if ($focusDiscount > $couponDiscount) {
                    $couponDiscount = $focusDiscount;
                    $couponDiscountObject = $coupon;
                }
            }
        }
        
        return $couponDiscountObject instanceof umiObject ? $couponDiscountObject : false;
    }
    
    public static function getCouponObjectByCode($code)
    {
        static $arCouponCode = array();
        
        if(!isset($arCouponCode[$code])) {
            $sel = new selector('objects');
            $sel->types('object-type')->name(self::module, self::method);
            $sel->where(self::field_code)->equals($code);
            $sel->order('id')->asc();
            $sel->limit(0, 1);
            
            $object = getArrayKey($sel->result(), 0);
            if($object instanceof umiObject) {
                $arCouponCode[$code] = $object;
            } else {
                $arCouponCode[$code] = false;
            }
        }
        
        return $arCouponCode[$code];
    }
    
    public static function isCouponDiscountFocusPage(umiObject $coupon, $pageId)
    {
        if (!$coupon->getValue(self::field_discount_focus)) {
            return false;
        }
        
        $arFocusPageId = array();
        foreach ($coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus_page_id) as $focusPage) {
            if (!$focusPage instanceof umiHierarchyElement) {
                continue;
            }
            
            $arFocusPageId[] = $focusPage->getId();
        }
        
        return in_array($pageId, $arFocusPageId);
    }
    
    public static function calculateCouponDiscountPrice(umiObject $coupon, $pageId, $price, $originalPrice)
    {
        if (self::isCouponDiscountFocusPage($coupon, $pageId)) {
            $discount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_focus);
        } else {
            $discount = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_discount_base);
        }
        
        $price = $price - round($originalPrice * $discount / 100, 2);
        $maxDiscountPrice = $originalPrice * 0.65;
        if ($price < $maxDiscountPrice) {
            $price = $maxDiscountPrice;
        }
        
        return $price;
    }
    
    public static function logMessage($message)
    {
        $dir = CURRENT_WORKING_DIR . '/sys-temp/ormcoCoupons/log/coupon';
        if (!is_dir($dir)) {
            mkdir($dir, 0775, true);
        }
        
        SiteLogHelper::clearLogDir($dir, time() - (86400 * 7));
        
        $file = $dir . '/' . date('Ymd') . '.log';
        file_put_contents($file, date('H:i:s') . ' ' . $message . PHP_EOL, FILE_APPEND);
    }
    
    public static function getDaysLeft($timestamp)
    {
        return ceil(($timestamp - time()) / 86400);
    }
    
    public static function addLog(umiObject $object, $message)
    {
        $log = trim($object->getValue(self::field_log));
        if ($log) {
            $log .= PHP_EOL;
        }
        $log .= date('d.m.Y H:i:s') . ' ' . $message;
        $object->setValue(self::field_log, $log);
        $object->commit();
    }
    
    /**
     * Проверка, может ли userId использовать купоны.
     * Если userId - гость или незарегистрированный покупатель, то он не может использовать купоны
     *
     * @param $userId
     * @return bool
     */
    public static function isCouponEnabledForUser($userId)
    {
        if ($userId == Service::SystemUsersPermissions()->getGuestUserId()) {
            return false;
        }
        
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject || $user->getMethod() != SiteUsersUserModel::method) {
            return false;
        }
        
        return true;
    }
}