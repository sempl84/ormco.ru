<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteWebformsFormFeedbackModel
{
    const object_type_id = 351;
    
    const field_name = 'fio';
    const field_email = 'email';
    const field_phone = 'telefon';
    const field_city = 'gorod';
    const field_link = 'stranica_otpravki_formy';
    const field_agree = 'allow_personal_data';
    const field_is_doctor = 'ya_vrach';
    const field_clinic_name = 'nazvanie_kliniki';
    const field_clinic_address = 'adres_kliniki';
}