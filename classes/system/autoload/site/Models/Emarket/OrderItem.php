<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteEmarketOrderItemModel
{
    const module = 'emarket';
    const method = 'order_item';
    
    const group_ormco_coupon = 'ormco_coupon_properties';
    const field_ormco_coupon_id = 'ormco_coupon_id';
    const field_ormco_coupon_code = 'ormco_coupon_code';
    const field_ormco_coupon_focus_discount = 'ormco_coupon_focus_discount';
    const field_ormco_coupon_discount_value = 'ormco_coupon_discount_value';
    const field_ormco_coupon_discount_percent = 'ormco_coupon_discount_percent';
    const field_ormco_coupon_disable_update_price = 'ormco_coupon_disable_update_price';
}