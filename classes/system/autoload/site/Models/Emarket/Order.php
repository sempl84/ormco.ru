<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteEmarketOrderModel
{
    const module = 'emarket';
    const method = 'order';
    
    const field_date = 'order_date';
    const field_number = 'number';
    const field_customer_id = 'customer_id';
    const field_order_items = 'order_items';
    const field_create_date = 'order_create_date';
    
    const group_ormco_coupon = 'order_ormco_coupon_properties';
    const field_ormco_coupon_id = 'order_ormco_coupon_id';
    const field_ormco_coupon_code = 'order_ormco_coupon_code';
    const field_ormco_coupon_discount_value = 'order_ormco_coupon_discount_value';
    
    const group_abandoned_orders = 'order_abandoned_orders';
    const field_abandoned_orders_message_number = 'order_abandoned_orders_message_number';
    const field_abandoned_orders_message_date = 'order_abandoned_orders_message_date';
    
    const group_ga = 'order_ga';
    const field_ga_cid = 'order_ga_cid';
    const field_ga_order_postfix = 'order_ga_order_postfix';
    
    public static function getGaOrderNumber(order $order)
    {
        $number = $order->getNumber();
        
        $postfix = intval($order->getValue(self::field_ga_order_postfix));
        if($postfix > 0) {
            $number .= '-' . $postfix;
        }
        
        return $number;
    }
}