<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

$classesDir = dirname(__FILE__) . '/site/Classes';
$helpersDir = dirname(__FILE__) . '/site/Helpers';
$modelsDir = dirname(__FILE__) . '/site/Models';

$classes = [
    'OrmcoUniOneSendAbandonedOrdersMessage' => $classesDir . '/OrmcoUniOne/SendAbandonedOrdersMessage.php',
    'OrmcoUniSendersSyncContacts' => $classesDir . '/OrmcoUniSender/SyncContacts.php',
    
    'OrmcoSiteDataDadataHelper' => $helpersDir . '/OrmcoSiteData/Dadata.php',
    'OrmcoSiteDataSmsHelper' => $helpersDir . '/OrmcoSiteData/Sms.php',
    
    'AjaxHelper' => $helpersDir . '/Ajax.php',
    'SiteDateHelper' => $helpersDir . '/Date.php',
    'SiteEmarketHelper' => $helpersDir . '/Emarket.php',
    'SiteGoogleAnalyticsHelper' => $helpersDir . '/GoogleAnalytics.php',
    'SiteLogHelper' => $helpersDir . '/Log.php',
    'ObjectHelper' => $helpersDir . '/Object.php',
    'OrmcoHelper' => $helpersDir . '/Ormco.php',
    'SiteRenderOrderItemDiscountHelper' => $helpersDir . '/RenderOrderItemDiscount.php',
    'SiteStringHelper' => $helpersDir . '/String.php',
    
    'SiteCatalogCategoryModel' => $modelsDir . '/Catalog/Category.php',
    'SiteCatalogObjectModel' => $modelsDir . '/Catalog/Object.php',
    
    'SiteContentPageIndexModel' => $modelsDir . '/Content/Page/Index.php',
    'SiteContentPageSettingsModel' => $modelsDir . '/Content/Page/Settings.php',
    
    'SiteEmarketOrderModel' => $modelsDir . '/Emarket/Order.php',
    'SiteEmarketOrderItemModel' => $modelsDir . '/Emarket/OrderItem.php',
    
    'SiteNewsItemModel' => $modelsDir . '/News/Item.php',
    
    'SiteOrmcoCouponsCouponModel' => $modelsDir . '/OrmcoCoupons/Coupon.php',
    'SiteOrmcoCouponsCouponStateModel' => $modelsDir . '/OrmcoCoupons/CouponState.php',
    'SiteOrmcoCouponsHistoryModel' => $modelsDir . '/OrmcoCoupons/History.php',
    
    'SiteSliderItemElement' => $modelsDir . '/Slider/ItemElement.php',
    
    'SiteUsersUserModel' => $modelsDir . '/Users/User.php',
    
    'SiteWebformsFormFeedbackModel' => $modelsDir . '/Webforms/Form/Feedback.php',
];