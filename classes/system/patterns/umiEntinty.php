<?php
	/**
	 * Базовый класс для классов, которые реализуют ключевые сущности ядра системы.
	 * Реализует основные интерфейсы, которые должна поддерживать любая сущность.
	 * TODO Check all PHPDoc's
	 */
	abstract class umiEntinty implements iUmiEntinty {
		protected $id, $is_updated = false;

		protected $bNeedUpdateCache = false;
		/** @var bool нужно ли производить сохранение изменений сущности в деструкторе */
		protected $savingInDestructor;

		/**
		 * Конструктор сущности, должен вызываться из коллекций
		 * @param int $id идентификатор сущности
		 * @param array|bool $row массив значений, который теоретически может быть передан в конструктор для оптимизации
		 * @param bool $instantLoad нужно ли произвести немедленную загрузку данных
		 * @param bool $savingInDestructor нужно ли сохраненять изменения сущности в деструкторе
		 * @throws privateException
		 */
		public function __construct($id, $row = false, $instantLoad = true, $savingInDestructor = true) {
			$this->setId($id);
			$this->savingInDestructor = $savingInDestructor;
			$this->is_updated = false;
			if($instantLoad && $this->loadInfo($row) === false) {
				throw new privateException("Failed to load info for {$this->store_type} with id {$id}");
			}
		}

		/**
		 * Запрещаем копирование
		 */
		public function __clone() {
			throw new coreException('umiEntinty must not be cloned');
		}

		/**
		 * Деструктор сущности проверят, были ли внесены изменения. Если да, то они сохраняются
		 */
		public function __destruct() {
			if (!$this->savingInDestructor) {
				return;
			}

			if ($this->is_updated) {
				$this->save();
				$this->setIsUpdated(false);
				$this->updateCache();
			} elseif ($this->bNeedUpdateCache) {
				// В memcached кидаем только при деструкте и только если были какие-то изменения
				$this->updateCache();
			}
		}

		/**
		 * Вернуть id сущности
		 * @return Integer $id
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * Изменить id сущности
		 * @param Integer $id новый id сущности
		 */
		protected function setId($id) {
			$this->id = (int) $id;
		}

		/**
		 * @inheritdoc
		 */
		public function getIsUpdated() {
			return $this->is_updated;
		}

		/**
		 * @inheritdoc
		 */
		public function setIsUpdated($is_updated = true) {
			$this->is_updated = (bool) $is_updated;
			$this->bNeedUpdateCache = $this->is_updated;
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function beforeSerialize($reget = false) {}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function afterSerialize() {}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function afterUnSerialize() {}

		/**
		 * Загрузить необходимую информацию о сущности из БД. Требует реализации в дочернем классе.
		 * @param array|bool $row массив с информацией о сущности
		 * @return
		 */
		abstract protected function loadInfo($row = false);

		/**
		 * Сохранить в БД информацию о сущности. Требует реализации в дочернем классе.
		 */
		abstract protected function save();

		/**
		 * Применить совершенные изменения, если они есть. Если нет, вернет false
		 * @return Boolean true если изменения примененые и при этом не возникло ошибок
		 */
		public function commit() {
			if (!$this->is_updated) {
				return false;
			}

			$res = $this->save();

			if (cacheFrontend::getInstance()->getIsConnected()) {
				$this->updateCache();
			}

			$this->setIsUpdated(false);

			return $res;
		}

		/**
		 * Заново прочитать все данные сущности из БД. Внесенные изменения скорее всего будут утеряны
		 * @return Boolean результат операции зависит от реализации loadInfo() в дочернем классе
		 */
		public function update() {
			$res = $this->loadInfo();
			$this->setIsUpdated(false);
			$this->updateCache();
			return $res;
		}

		/**
		 * Отфильтровать значения, попадающие в БД
		 * @param String $string значение
		 * @return String отфильтрованное значение
		 */
		public static function filterInputString($string) {
			$connection = ConnectionPool::getInstance()->getConnection();
			return $connection->escape($string);
		}

		/**
		 * Magic method
		 * @return id объекта
		 */
		public function __toString() {
			return (string) $this->getId();
		}

		/**
		 * Перевести строковую константу по ее ключу
		 * @param String $label ключ строковой константы
		 * @return String значение константы в текущей локали
		 */
		public function translateLabel($label) {
			$str = strpos($label, 'i18n::') === 0
				? getLabel(substr($label, 6))
				: getLabel($label);
			return $str === null ? $label : $str;
		}

		/**
		 * Обновить версию сущности, которая находится в кеше
		 */
		protected function updateCache() {
			cacheFrontend::getInstance()->save($this, $this->store_type);
		}

		/**
		 * Получить ключ строковой константы, если она определена, либо вернуть саму строку
		 * @param String $str строка, для которых нужно определить ключ
		 * @param String $pattern='' префикс ключа, используется внутри системы
		 * @return String ключ константы, либо параметр $str, если такого значение нет в списке констант
		 */
		protected function translateI18n($str, $pattern = '') {
			$label = ulangStream::getI18n($str, $pattern);
			return $label === null ? $str : $label;
		}

		/**
		 * @deprecated
		 */
		protected function disableCache() {
			return null;
		}
	}
