<?php
interface iUmiEntinty {
	public function getId();
	public function commit();
	public function update();
	/**
	 * Проверяет была ли обновлена сущность
	 * @return bool
	 */
	public function getIsUpdated();
	/**
	 * Устанавливает была ли сущность изменена
	 * @param bool $is_updated была ли изменена сущность
	 */
	public function setIsUpdated($is_updated = true);

	public static function filterInputString($string);
}