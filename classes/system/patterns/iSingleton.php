<?php
interface iSingleton {
	public static function getInstance($c = NULL);
}