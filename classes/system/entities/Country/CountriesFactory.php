<?php
namespace UmiCms\Classes\System\Entities\Country;
/**
 * Фабрика стран
 * @package UmiCms\Classes\System\Entities\Country;
 */
class CountriesFactory implements iCountriesFactory {

	/**
	 * {@inheritdoc}
	 */
	public static function createByObject(\iUmiObject $object) {
		return new Country($object);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function createByObjectId($objectId) {
		$objectId = (int) $objectId;
		$object = \umiObjectsCollection::getInstance()
			->getObject($objectId);

		if (!$object instanceof \iUmiObject) {
			$exceptionMessage = sprintf(getLabel('error-cannot-get-country-by-id'), $objectId);
			throw new \expectObjectException($exceptionMessage);
		}

		return self::createByObject($object);
	}
}