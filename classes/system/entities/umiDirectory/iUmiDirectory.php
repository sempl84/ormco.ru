<?php

	interface iUmiDirectory {
		public function getPath();
		public function getName();

		public function getIsBroken();
		/**
		 * Проверяет доступна ли директория на чтение и возвращает результат проверки
		 * @return bool
		 */
		public function isReadable();
		public function getFSObjects($objectType = 0, $mask = "", $onlyReadable = false);
		public function getFiles($mask = "", $onlyReadable = false);
		public function getDirectories($mask = "", $onlyReadable = false);

		public function getAllFiles($i_obj_type=0, $s_mask="", $b_only_readable=false);

		public function delete($recursion = true);
		/**
		 * Удаляет пустую директорию и возвращает результат операции
		 * @return bool
		 */
		public function deleteEmptyDirectory();
		public static function requireFolder($folder, $basedir = "");
	};

?>