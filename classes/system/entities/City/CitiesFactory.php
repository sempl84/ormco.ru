<?php
namespace UmiCms\Classes\System\Entities\City;
/**
 * Фабрика городов
 * namespace UmiCms\Classes\System\Entities\City;
 */
class CitiesFactory implements iCitiesFactory {

	/**
	 * {@inheritdoc}
	 */
	public static function createByObject(\iUmiObject $object) {
		return new City($object);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function createByObjectId($objectId) {
		$objectId = (int) $objectId;
		$object = \umiObjectsCollection::getInstance()
			->getObject($objectId);

		if (!$object instanceof \iUmiObject) {
			$exceptionMessage = sprintf(getLabel('error-cannot-get-city-by-id'), $objectId);
			throw new \expectObjectException($exceptionMessage);
		}

		return self::createByObject($object);
	}
}