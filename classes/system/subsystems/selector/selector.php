<?php

	/**
	 * Selector - механизм формирования выборок, который должен заменить старый механизм выборок
	 * с помощью umiSelection и umiSelectionParser. Класс selector создан для того, чтобы избавиться
	 * от необходимости использования дополнительных классов и упростить определение искомых полей.
	 */
	class selector implements IteratorAggregate {
		/** Доступные режимы работы */
		protected static $modes = ['objects', 'pages'];

		/** Системные свойства страниц при фильтрации */
		protected static $sysPagesWhereFields = [
			'id',
			'name',
			'owner',
			'domain',
			'lang',
			'is_deleted',
			'is_active',
			'is_visible',
			'updatetime',
			'is_default',
			'template_id',
			'alt_name',
			'ord',
			'*',
		];

		/** Системные свойства страниц при группировке */
		protected static $sysPagesGroupFields = [
			'name',
			'owner',
			'domain',
			'lang',
			'is_deleted',
			'is_active',
			'is_visible',
			'updatetime',
			'is_default',
			'template_id',
			'obj_id',
			'obj_type_id',
			'ord',
		];

		/** Системные свойства объектов при фильтрации */
		protected static $sysObjectsWhereFields = ['id', 'name', 'owner', 'guid', 'updatetime', 'ord', '*'];

		/** Системные свойства объектов при группировке */
		protected static $sysObjectsGroupFields = ['id', 'name', 'owner', 'guid', 'updatetime', 'ord'];

		/** Системные свойства при сортировке */
		protected static $sysOrderFields = ['name', 'ord', 'rand', 'updatetime', 'id'];

		/** Режим работы */
		protected $mode;

		/** Исполнитель выборки */
		protected $executor;

		/** Опции селектора */
		protected $options = [];

		/** Параметры выборки */
		protected $permissions = null;
		protected $limit;
		protected $offset;
		protected $types = [];
		protected $hierarchy = [];

		/** Свойства, по которым идет выборка (системные и обычные) */
		protected $whereSysProps = [];
		protected $groupSysProps = [];
		protected $orderSysProps = [];
		protected $whereFieldProps = [];
		protected $groupFieldProps = [];
		protected $orderFieldProps = [];

		/** Результаты выборки */
		protected $result = null;

		/** Количество результатов без учета LIMIT */
		protected $length = null;

		/**
		 * Получить сущность заданного типа.
		 * Возвращает экземпляр selectorGetter, на котором нужно вызвать метод для получения сущности.
		 * Примеры использования:
		 * 1) Получить объект (класс umiObject) по id
		 * selector::get('object')->id(50);
		 * 2) Получить страницу (класс umiHierarchyElement) по id
		 * selector::get('page')->id(50);
		 * 3) Получить объектный тип данных (класс umiObjectType) по id
		 * selector::get('object-type')->id(50);
		 * 4) Получить объектный тип данных по связке модуль/метод базового типа
		 * selector::get('object-type')->name('users', 'user');
		 * 5) Получить базовый тип данных (класс umiHierarchyType) по связке модуль/метод
		 * selector::get('hierarchy-type')->name('users', 'user');
		 * 6) Получить поле (класс umiField) по id
		 * selector::get('field')->id(50);
		 * 7) Получить тип поля (класс umiFieldType) по id
		 * selector::get('field-type')->id(50);
		 * 8) Получить домен (класс domain) по id
		 * selector::get('domain')->id(50);
		 * 9) Получить домен по доменному имени
		 * selector::get('domain')->host('site.ru');
		 * 10) Получить язык (класс lang) по id
		 * selector::get('lang')->id(50);
		 * 11) Получить язык по префиксу
		 * selector::get('lang')->prefix('ru');
		 * @param string $requestedType тип искомой сущности
		 * @return selectorGetter
		 */
		public static function get($requestedType) {
			return new selectorGetter($requestedType);
		}

		/**
		 * @param string $mode Режим: 'objects' или 'pages'
		 */
		public function __construct($mode) {
			$this->setMode($mode);
		}

		/**
		 * Указать тип, по которому ведется выборка
		 * @param bool|string $typeClass тип ('object-type' или 'hierarchy-type')
		 * @return array|selectorType
		 */
		public function types($typeClass = false) {
			$this->throwIfAlreadyExecuted();
			if ($typeClass === false) {
				return $this->types;
			}
			return $this->types[] = new selectorType($typeClass, $this);
		}

		/**
		 * Указать фильтр по полю
		 * @param string $fieldName название поля
		 * @return selectorWhereProp|selectorWhereHierarchy|selectorWherePermissions
		 * @throws selectorException если поле выбрано неверно или не существует
		 */
		public function where($fieldName) {
			$this->throwIfAlreadyExecuted();
			if ($fieldName == 'hierarchy') {
				if ($this->mode == 'objects') {
					throw new selectorException("Hierarchy filter is not suitable for \"objects\" selector mode");
				}
				return $this->hierarchy[] = new selectorWhereHierarchy();
			}

			if ($fieldName == 'permissions') {
				if ($this->mode == 'objects') {
					throw new selectorException("Permissions filter is not suitable for \"objects\" selector mode");
				}
				if (is_null($this->permissions)) {
					$this->permissions = new selectorWherePermissions();
				}
				return $this->permissions;
			}

			if (in_array($fieldName, ($this->mode == 'pages') ? self::$sysPagesWhereFields : self::$sysObjectsWhereFields)) {
				return $this->whereSysProps[] = new selectorWhereSysProp($fieldName);
			}

			$fieldsId = $this->searchField($fieldName);
			if (!$fieldsId) {
				throw new selectorException(__METHOD__ . ": Field \"{$fieldName}\" is not presented in selected object types");
			}
			if (count($fieldsId) > 1) {
				/**
				 * @var selectorOption $option
				 */
				$option = $this->option('or-mode');
				$optionValue = (array) $option->value;
				$optionFields = (isset($optionValue['fields'])) ? $optionValue['fields'] : [];
				$optionFields[] = $fieldName;

				call_user_func_array([$option, 'fields'], $optionFields);
			} else {
				$fieldsId = [$fieldsId];
			}
			return $this->whereFieldProps[] = new selectorWhereFieldProp($fieldsId, $this->option('search-in-related-object')->value);
		}

		/**
		 * Сортировать результат по полю
		 * @param string $fieldName Имя поля для сортировки
		 * @return selectorOrderField
		 * @throws selectorException если поле не существует
		 */
		public function order($fieldName) {
			$this->throwIfAlreadyExecuted();
			if (in_array($fieldName, self::$sysOrderFields)) {
				return $this->orderSysProps[] = new selectorOrderSysProp($fieldName);
			} else {
				$fieldId = $this->searchField($fieldName);
				if (!$fieldId) {
					throw new selectorException(__METHOD__ . ": Field \"{$fieldName}\" is not presented in selected objects types");
				}
				return $this->orderFieldProps[] = new selectorOrderFieldProp($fieldId);
			}
		}

		/**
		 * Группировать результаты по полю
		 * @param string $fieldName поле для группировки
		 * @return selectorGroupField
		 * @throws selectorException если поле не существует
		 */
		public function group($fieldName) {
			$this->throwIfAlreadyExecuted();
			if (in_array($fieldName, ($this->mode == 'pages') ? self::$sysPagesGroupFields : self::$sysObjectsGroupFields)) {
				return $this->groupSysProps[] = new selectorGroupSysProp($fieldName);
			} else {
				$fieldId = $this->searchField($fieldName, true);
				if (!$fieldId) {
					throw new selectorException(__METHOD__ . ": Field \"{$fieldName}\" is not presented in selected objects types");
				}
				return $this->groupFieldProps[] = new selectorGroupFieldProp($fieldId);
			}
		}

		/**
		 * Ограничить количество результатов выборки
		 * @param int $offset отступ
		 * @param int $limit нужное число результатов
		 */
		public function limit($offset, $limit) {
			$this->throwIfAlreadyExecuted();
			$this->limit = (int) $limit;
			$this->offset = (int) $offset;
		}

		/**
		 * Результат работы селектора
		 * Запускает executor для объекта
		 * @return array выбранные поля
		 */
		public function result() {
			if (is_null($this->result)) {
				if ((sizeof($this->orderSysProps) == 0)) {
					$this->order('ord')->asc();
				}
				if ($this->mode == 'pages' && is_null($this->permissions) && !$this->option('no-permissions')->value) {
					$this->where('permissions');
				}
				$return = $this->option('return')->value;

				if (is_array($return) && in_array('count', $return)) {
					$this->result = $this->executor()->length();
				} else {
					$this->result = $this->executor()->result();
					$this->length = $this->executor()->length;
				}
			}

			$this->unloadExecutor();
			return $this->result;
		}

		/**
		 * Получить количество элементов в выборке
		 * @return int
		 */
		public function length() {
			if (is_null($this->length)) {
				if ($this->mode == 'pages' && is_null($this->permissions)) {
					$this->where('permissions');
				}
				$length = $this->executor()->length();

				if (in_array('count', $this->option('return')->value)) {
					$this->result = $length;
				} else {
					$this->result = $this->executor()->result();
				}
				$this->length = $length;
			}

			$this->unloadExecutor();
			return $this->length;
		}

		/**
		 * Добавить опцию
		 * @param string $name название опции
		 * @param mixed $value значение опции
		 * @return mixed
		 * @throws selectorException
		 */
		public function option($name, $value = null) {
			$this->throwIfAlreadyExecuted();
			if (!isset($this->options[$name])) {
				$selectorOption = new selectorOption($name);
				$this->options[$name] = $selectorOption;
			}
			if (!is_null($value)) {
				$this->options[$name]->value($value);
			}
			return $this->options[$name];
		}

		/** Сбросить результат выборки для повторного использования селектора */
		public function flush() {
			$this->result = null;
			$this->length = null;
		}

		/** {@inheritdoc} */
		public function __get($prop) {
			switch ($prop) {
				case 'length':
				case 'total':
					return $this->length();
				case 'result':
					return $this->result();
				case 'first':
					return (sizeof($this->result())) ? $this->result[0] : null;
				case 'last':
					return (sizeof($this->result())) ? $this->result[sizeof($this->result) - 1] : null;
			}

			$allowedProps = ['mode', 'offset', 'limit', 'whereFieldProps', 'orderFieldProps', 'groupFieldProps',
				'whereSysProps', 'orderSysProps', 'groupSysProps', 'types', 'permissions', 'hierarchy', 'options'];

			if (in_array($prop, $allowedProps)) {
				return $this->$prop;
			}
		}

		/**
		 * Проверяет наличие свойства
		 * @param string $prop имя свойства
		 * @return bool
		 */
		public function __isset($prop) {
			return in_array($prop, [
				'length',
				'total',
				'result',
				'first',
				'last',
				'mode',
				'offset',
				'limit',
				'whereFieldProps',
				'orderFieldProps',
				'groupFieldProps',
				'whereSysProps',
				'orderSysProps',
				'groupSysProps',
				'types',
				'permissions',
				'hierarchy',
				'options'
			]);
		}

		/**
		 * Реализация интерфейса IteratorAggregate
		 * @return ArrayIterator
		 */
		public function getIterator() {
			$this->result();
			return new ArrayIterator($this->result);
		}

		/**
		 * Получить запрос, сформированный executor'ом
		 * @return string
		 */
		public function query() {
			if ($this->mode == 'pages') {
				if (sizeof($this->orderSysProps) == 0) {
					$this->order('ord')->asc();
				}
				if (is_null($this->permissions)) {
					$this->where('permissions');
				}
			}
			return $this->executor()->query();
		}

		/**
		 * Получает ID поля по его имени
		 * @param string $fieldName имя поля
		 * @param boolean $returnFirst , если true, то будет возвращен ID первого поля
		 * @return int|array ID поля или массив ID полей
		 */
		public function searchField($fieldName, $returnFirst = false) {
			$fieldIds = [];

			if ($this->mode == 'pages' && count($this->types) == 0) {
				$type = new selectorType('object-type', $this);
				$type->guid('root-pages-type');
				$this->types[] = $type;
			}

			foreach ($this->types as $type) {
				$fieldId = $type->getFieldsId($fieldName);
				if ($fieldId) {
					if (is_array($fieldId)) {
						$fieldIds = array_unique(array_merge($fieldIds, $fieldId));
					} else {
						$fieldIds[] = $fieldId;
					}
				}
			}

			if (count($fieldIds) === 1) {
				return (int) array_shift($fieldIds);
			}

			return $returnFirst ? array_shift($fieldIds) : $fieldIds;
		}

		/**
		 * Прервать попытку второго исполнения
		 * @throws selectorException
		 */
		protected function throwIfAlreadyExecuted() {
			if ($this->executor && $this->executor->getSkipExecutedCheckState()) {
				return;
			}
			if (!is_null($this->result) || !is_null($this->length)) {
				$message = getLabel('error-selector-executed');
				throw new selectorException($message);
			}
		}

		/** Инициализировать и вернуть экземпляр selectorExecutor */
		protected function executor() {
			if (!$this->executor) {
				$this->executor = new selectorExecutor($this);
			}
			return $this->executor;
		}

		/** Выгрузить из памяти selectorExecutor, если выборка уже сделана */
		protected function unloadExecutor() {
			if (!is_null($this->length) && !is_null($this->result)) {
				unset($this->executor);
			}
		}

		/**
		 * Установить режим работы селектора
		 * @param string $mode режим
		 * @throws selectorException
		 */
		protected function setMode($mode) {
			if (!in_array($mode, self::$modes)) {
				$modes = implode(', ', self::$modes);
				throw new selectorException("This mode \"{$mode}\" is not supported, choose one of these: {$modes}");
			}
			$this->mode = $mode;
			if ($mode == 'pages') {
				$this->setDefaultPagesWhere();
			}
		}

		/**
		 * Устанавливает значения системных свойств по умолчанию для выборки по страницам
		 * @throws selectorException
		 */
		protected function setDefaultPagesWhere() {
			$cmsController = cmsController::getInstance();
			$this->where('domain')->equals($cmsController->getCurrentDomain());
			$this->where('lang')->equals($cmsController->getCurrentLang());
			$this->where('is_deleted')->equals(0);

			if ($cmsController->getCurrentMode() != 'admin') {
				$this->where('is_active')->equals(1);
			}
		}
	}

?>
