<?php
	class sampleManifestCallback implements iManifestCallback {
	
		public function onBeforeTransactionExecute(iTransaction $transaction) {
			$this->log(getLabel('manifest-transaction-start') . " \"" . $transaction->getTitle() . "\"");
		}
		
		public function onAfterTransactionExecute(iTransaction $transaction) {}
		
		public function onBeforeActionExecute(iAtomicAction $action) {
			$this->log(getLabel('manifest-action-start') . " \"" . $action->getTitle() . "\"");
		}
		
		public function onAfterActionExecute(iAtomicAction $action) {}
		
		public function onBeforeExecute() {
			$this->log(getLabel('manifest-start'));
		}
		
		public function onAfterExecute() {
			$this->log(getLabel('manifest-finish'));
		}
		
		public function onBeforeRollback() {
			$this->log(getLabel('manifest-rollback-start'), true);
		}
		
		public function onAfterRollback() {}
		
		public function onException(Exception $e) {
			$this->log(getLabel('manifest-exception'), true);
		}
		
		protected function log($msg, $isError = false) {
			if($isError) {
				$msg = "<font color=\"red\">{$msg}</font>";
			}
			echo $msg, "\n";
		}
		
		public function onBeforeActionRollback(iAtomicAction $action) {
			$this->log(getLabel('manifest-action-rollback') . " \"" . $action->getTitle() . "\"", true);
		}
		
		public function onAfterActionRollback(iAtomicAction $action) {}
	};
?>