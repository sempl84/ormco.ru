<?php

class regedit extends singleton implements iRegedit {
	protected $cacheFilePath, $cache = Array(), $cacheSaved = false;

	/**
	 * @param null $c
	 *
	 * @return regedit
	 */
	public static function getInstance($c = NULL) {
		return parent::getInstance(__CLASS__);
	}


	public function getKey($path, $rightOffset = 0, $cacheOnly = false) {
		static $cache = array();
		$path = trim($path, "/");

		if (isset($this->cache['keys'][$path])) {
			return $this->cache['keys'][$path];
		}

		if ($cacheOnly) {
			return $this->cache['keys'][$path] = false;
		}

		$keyId = 0; $previousPaths = array();
		$connection = ConnectionPool::getInstance()->getConnection();

		foreach(explode("/", $path) as $key) {
			$key = $connection->escape($key);
			$previousPaths[] = $key;
			$currentKey = implode('/', $previousPaths);

			if (isset($cache[$currentKey])) {
				$keyId = $cache[$currentKey];
				continue;
			}

			$sql = "SELECT id FROM cms_reg WHERE rel = '$keyId' AND var = '{$key}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() > 0) {
				list($keyId) = $result->fetch();
				$cache[$currentKey] = $keyId;
			} else {
				return $this->cache['keys'][$path] = false;
			}
		}
		return $this->cache['keys'][$path] = (int) $keyId;
	}

	/**
	 * Получить значение
	 *
	 * @param string $path
	 *
	 * @return mixed
	 */
	public function getVal($path, $cacheKey = false) {
		$keyId = $this->getKey($path, $cacheKey);

		if (isset($this->cache['values'][$path])) {
			return $this->cache['values'][$path];
		}

		if (!$keyId) {
			return $this->cache['values'][$path] = false;
		}

		if (isset($this->cache['values'][$keyId])) {
			return $this->cache['values'][$keyId];
		}

		$this->cacheSaved = false;
		$connection = ConnectionPool::getInstance()->getConnection();
		$sql = "SELECT val FROM cms_reg WHERE id = '{$keyId}'";
		$result = $connection->queryResult($sql, true);
		$result->setFetchType(IQueryResult::FETCH_ROW);

		list($value) = $result->fetch();
		return $this->cache['values'][$keyId] = $value;
	}

	public function setVar($path, $value) {
		return $this->setVal($path, $value);
	}

	public function setVal($path, $value) {
		$this->resetCache();
		$keyId = $this->getKey($path);
		if ($keyId == false) {
			$keyId = $this->createKey($path);
		}

		$connection = ConnectionPool::getInstance()->getConnection();
		$value = $connection->escape($value);
		$sql = "UPDATE cms_reg SET val = '{$value}' WHERE id = '{$keyId}'";
		$connection->query($sql);

		$this->resetCache();
		return true;
	}

	public function delVar($path) {
		$keyId = $this->getKey($path);

		if (!$keyId) {
			return false;
		}

		$connection = ConnectionPool::getInstance()->getConnection();
		$sql = "DELETE FROM cms_reg WHERE rel = '{$keyId}' OR id = '{$keyId}'";
		$connection->query($sql, true);
		$this->resetCache();
		return true;
	}

	public function getList($path) {
		if (isset($this->cache['lists'][$path])) {
			return $this->cache['lists'][$path];
		}

		$keyId = $this->getKey($path);

		if ($path == "//") {
			$keyId = 0;
		}

		if (!$keyId && $path != "//") {
			return $this->cache['lists'][$path] = false;
		}

		if (isset($this->cache['lists'][$keyId])) {
			return $this->cache['lists'][$keyId];
		}

		$this->cacheSaved = false;
		$connection = ConnectionPool::getInstance()->getConnection();
		$sql = "SELECT id, var, val FROM cms_reg WHERE rel = '{$keyId}' ORDER BY id ASC";
		$result = $connection->queryResult($sql, true);
		$result->setFetchType(IQueryResult::FETCH_ARRAY);

		$values = Array();

		foreach ($result as $row) {
			list($umi_temp, $var, $val) = $row;
			$values[] = Array($var, $val);
		}

		return $this->cache['lists'][$keyId] = $values;
	}

	final public static function checkSomething($a, $b, $return=false) {
		return true;
		if (isLocalMode()) {
			return true;
		}

		$trialLifetime = 3600*24*30;
		$isCommerceEnc = self::getInstance()->getVal("//modules/autoupdate/system_edition") == 'commerce_enc';

		foreach ($b as $versionLine => $c3) {
			$isValid = (substr($a, -12, 12) == substr($c3, -12, 12 ));

			if ($isValid === true) {
				if (!defined('CURRENT_VERSION_LINE')) {
					define("CURRENT_VERSION_LINE", $versionLine);
				}

				if ($versionLine == "trial" || $isCommerceEnc) {
					if (file_exists(SYS_CACHE_RUNTIME . "trash")) {
						unlink(SYS_CACHE_RUNTIME . "trash");
					}

					$createTime = filectime(__FILE__);
					$currentTime = time();

					if (($currentTime - $createTime) > $trialLifetime) {
						if ($return) {
							return false;
						} else {
							try {
								$buffer = outputBuffer::current();
							} catch (coreException $e) {
								$buffer = outputBuffer::current('HTTPOutputBuffer');
							}

							if ($buffer instanceof HTTPOutputBuffer) {
								$buffer->status(500);
							}

							$buffer->push(file_get_contents(CURRENT_WORKING_DIR . "/errors/trial_expired.html"));
							$buffer->end();
						}
					}
				}

				return true;
			}
		}

		return false;
	}


	final public function checkSelfKeycode() {
		if (isDemoMode()) {
			return false;
		}

		$keycode = $this->getVal("//settings/keycode");

		if (strlen($keycode)==0) {
			return false;
		}

		$codename = $this->getVal("//settings/system_edition");

		$pro = array('commerce', 'business', 'corporate', 'ultimate', 'commerce_enc', 'business_enc', 'corporate_enc');
		$internalCodeName = in_array($codename, $pro) ? 'pro' : $codename;

		$b = array($internalCodeName => umiTemplater::getSomething($internalCodeName));

		return self::checkSomething($keycode, $b, true);
	}

	final public function doTesting($testMessage) {
		$requestUrl = base64_decode('aHR0cDovL3VwZGF0ZXMudW1pLWNtcy5ydS91cGRhdGVzZXJ2ZXIvP3R5cGU9YWRkLWNtcy1zdGF0');
		$testMessage = array('message' => json_decode($testMessage, true));
		$response = umiRemoteFileGetter::get($requestUrl, false, false, $testMessage, false, 'POST', 3);

		$domResponse = new DOMDocument();

		if (!$domResponse->loadXML($response)) {
			return false;
		}

		$xpath = new DOMXPath($domResponse);
		$message = $xpath->evaluate('/response/message');

		if (!$message instanceof DOMNodeList) {
			return false;
		}

		if ($message->length == 0) {
			return false;
		}

		$this->setVal('//settings/last_mess_time', time());
		return true;
	}

	public function getDaysLeft() {
		return 30 - floor((time() - filectime(__FILE__)) / (3600*24));
	}


	protected function __construct() {
		$config = mainConfiguration::getInstance();
		$this->cacheFilePath = $config->includeParam('system.runtime-cache') . 'registry';
		$this->loadCache();
	}

	public function __destruct() {
		if(!$this->cacheSaved) {
			$this->saveCache();
		}
	}

	protected function loadCache() {
		$cacheFrontend = cacheFrontend::getInstance();

		if($cacheFrontend->getIsConnected()) {
				if($cache = $cacheFrontend->loadSql("registry")) {
					$this->cache = unserialize($cache);
					$this->cacheSaved = true;
					return;
				}
		}

		if(file_exists($this->cacheFilePath)) {
			$cache = unserialize(file_get_contents($this->cacheFilePath));
			if(is_array($cache)) {
				$this->cacheSaved = true;
				$this->cache = $cache;
			}
		}
	}

	protected function saveCache() {
		if(is_array($this->cache)) {
			if(is_dir(dirname($this->cacheFilePath))) {
				file_put_contents($this->cacheFilePath, serialize($this->cache));
			}
			if(cacheFrontend::getInstance()->getIsConnected()) {
				cacheFrontend::getInstance()->saveSql("registry", serialize($this->cache));
			}
		}
		$this->cacheSaved = true;
	}

	protected function createKey($path) {
		$path = trim($path, "/");
		$subKeyPath = "//";

		$relId = 0;
		$keyId = null;
		$connection = ConnectionPool::getInstance()->getConnection();

		foreach (explode("/", $path) as $key) {
			$key = $connection->escape($key);
			$subKeyPath .= $key . "/";

			if ($keyId = $this->getKey($subKeyPath)) {
				$relId = $keyId;
			} else {
				$sql = "INSERT INTO cms_reg (rel, var, val) VALUES ('{$relId}', '{$key}', '')";
				$connection->query($sql);
				$relId = $keyId = (int) $connection->insertId();
				$this->resetCache();
			}
		}
		return $keyId;
	}

	/**
	 * Очищает кэш реестра
	 * @param bool|array $keys массив, содержащий строковые названия веток кэша,
	 * которые будут очищены ('keys' или 'values').
	 */
	public function resetCache($keys = false) {
		if(is_array($keys)) {
			foreach($keys as $key) {
				if(isset($this->cache[$key])) {
					unset($this->cache[$key]);
				}
			}
		} else {
			$this->cache = Array();
		}

		$this->saveCache();
	}
};

?>
