<?php
	interface iRegedit {
		public function getKey($keyPath, $rightOffset = 0, $cacheOnly = false);

		public function getVal($keyPath, $cacheKey = false);
		public function setVar($keyPath, $value);
		public function setVal($keyPath, $value);

		public function delVar($keyPath);

		public function getList($keyPath);
		public function resetCache($keys = false);
	};
?>