<?php

	/**
	 * Класс реализация механизма кеширования в сервер Redis
	 */
	class redisCacheEngine implements iCacheEngine {
		/**
		 * Свойство в которое сохраняется созданный объект клиента к Redis
		 * @var bool
		 */
		private $redis = false;
		/**
		 * Хост сервера с Redis
		 * @var string
		 */
		private $host = '';
		/**
		 * Порт сервера Redis
		 * @var string
		 */
		private $port = '';
		/**
		 * Номер используемой базы
		 * @var string
		 */
		private $baseNumber = '';
		/**
		 * Ключ доступа к серверу Redis, если необходим
		 * @var string
		 */
		private $authKey = '';
		/**
		 * Префикс для сохраняемых ключей
		 * @var string
		 */
		private $namespace = 'umic:%u:';
		/**
		 * @const string DEFAULT_HOST хост подключения по умолчанию
		 */
		const DEFAULT_HOST = '127.0.0.1';
		/**
		 * @const int DEFAULT_PORT порт подключения по умолчанию
		 */
		const DEFAULT_PORT = 6379;

		/**
		 * Возвращает экземпляр объекта клиента к Redis
		 * @return bool|Redis
		 */
		private function getClient() {
			if (!class_exists('Redis')) {
				return false;
			}

			if (!$this->redis || ($this->redis instanceof Redis && !$this->redis->ping())) {

				$this->redis = new Redis();

				if (!$this->redis->connect($this->host, $this->port)) {
					return false;
				}

				if (!empty($this->authKey)) {
					if (!$this->redis->auth($this->authKey)) {
						return false;
					}
				}

				if (!empty($this->baseNumber)) {
					if (!$this->redis->select($this->baseNumber)) {
						return false;
					}
				}
			}

			return $this->redis;
		}

		public function __construct() {
			$conf = mainConfiguration::getInstance();

			$md = hash('crc32', $conf->get('system', 'salt'));
			$this->namespace = sprintf($this->namespace, $md);

			$this->host = $conf->get('cache', 'redis.host');

			if (empty($this->host)) {
				$this->host = self::DEFAULT_HOST;
			}

			$this->port = $conf->get('cache', 'redis.port');

			if (empty($this->port)) {
				$this->port = self::DEFAULT_PORT;
			}

			$this->baseNumber = $conf->get('cache', 'redis.base');
			$this->authKey = $conf->get('cache', 'redis.auth');

			$this->getClient();
		}

		public function saveObjectData($key, $object, $expire) {
			return $this->saveRawData($key, $object, $expire);
		}

		public function saveRawData($key, $string, $expire) {
			$redis = $this->getClient();
			return (bool) $redis ? $redis->set($this->namespace . md5($key), serialize($string), $expire) : false;
		}

		public function loadObjectData($key) {
			return $this->loadRawData($key);
		}

		public function loadRawData($key) {
			$key = $this->namespace . md5($key);
			$redis = $this->getClient();

			if (!$redis) {
				return null;
			}

			$res = $redis ? $redis->get($key) : null;
			return $res ? unserialize($res) : null;
		}

		public function delete($key) {
			$key = $this->namespace . md5($key);
			$redis = $this->getClient();
			return (bool) $redis ? $redis->del($key) : false;
		}

		public function flush() {
			$redis = $this->getClient();

			if (!$redis) {
				return false;
			}

			$redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
			$iterator = null;

			while ($keys = $redis->scan($iterator, $this->namespace . '*')) {
				if (is_array($keys) && !empty($keys)) {
					$redis->del($keys);
				}
			}

			return true;
		}

		public function getIsConnected() {
			return (bool) $this->getClient();
		}

		public function getCacheSize() {
			return 'not yet implemented';
		}
	}
