<?php

	/**
	 * Класс кеширования через базу данных
	 */
	class databaseCacheEngine implements iCacheEngine {
		/**
		 * @var IConnection|null $connection ресурс соединения с бд
		 */
		private $connection;
		/**
		 * @var iRegedit|null $registry реестр
		 */
		private $registry;
		/**
		 * @var iConfiguration|null $configuration конфигурация
		 */
		private $configuration;
		/**
		 * @var bool $connection доступен ли кеширующий механизм
		 */
		private $connected = true;

		/**
		 * Конструктор
		 */
		public function __construct() {
			$this->connection = ConnectionPool::getInstance()
				->getConnection();
			$this->connected = ($this->connection instanceof IConnection);
		}

		/**
		 * Доступен ли кеширующий механизм
		 * @return bool
		 */
		public function getIsConnected() {
			return (bool) $this->connected;
		}

		/**
		 * Сохраняет объект в кеш
		 * @param string $key ключ кеша
		 * @param object $object сохраняемый объект
		 * @param int $expire время жизни кеша в секундах
		 * @return bool
		 */
		public function saveObjectData($key, $object, $expire) {
			return $this->saveRawData($key, $object, $expire);
		}

		/**
		 * Сохраняет данные в кеш
		 * @param string $key ключ кеша
		 * @param mixed $data данные
		 * @param int $expire время жизни кеша в секундах
		 * @return bool
		 */
		public function saveRawData($key, $data, $expire) {
			$connection = $this->getConnection();
			$escapedKey = $connection->escape($key);
			$data = $connection->escape(serialize($data));
			$createTime = time();
			$expire = time() + (int) $expire;

			$sql = <<<sql
INSERT INTO `cms3_data_cache` (`key`, `value`, `create_time`, `expire_time`, `entry_time`, `entries_number`)
	VALUES('$escapedKey', '$data', $createTime, $expire, 0, 0)
		ON DUPLICATE KEY UPDATE `value` = '$data', `create_time` = $createTime, `expire_time` = $expire;
sql;
			$connection->query($sql);

			return true;
		}

		/**
		 * Загружает объект из кеша и возвращает его
		 * @param string $key ключ кеша
		 * @return object|null
		 */
		public function loadObjectData($key) {
			return $this->loadRawData($key);
		}

		/**
		 * Загружает данные из кеша и возвращает их
		 * @param string $key ключ кеша
		 * @return mixed|null
		 */
		public function loadRawData($key) {
			$connection = $this->getConnection();
			$escapedKey = $connection->escape($key);

			$sql = <<<SQL
SELECT `value`, `expire_time`
	FROM `cms3_data_cache`
		WHERE `key` = '$escapedKey';
SQL;
			$cacheRowList = $connection->queryResult($sql);

			if ($cacheRowList->length() == 0) {
				return null;
			}

			$cacheRow = $cacheRowList->fetch();

			if (time() > $cacheRow['expire_time']) {
				$this->delete($key);
				return null;
			}

			$debugEnable = (bool) $this->getConfiguration()
				->get('cache', 'engine.debug');

			if ($debugEnable) {
				$this->updateEntry($key);
			}

			return unserialize($cacheRow['value']);
		}

		/**
		 * Удаляет кеш по ключу
		 * @param string $key ключ кеша
		 * @return bool
		 */
		public function delete($key) {
			$connection = $this->getConnection();
			$escapedKey = $connection->escape($key);

			$sql = <<<SQL
DELETE FROM `cms3_data_cache`
	WHERE `key` = '$escapedKey';
SQL;
			$connection->query($sql);

			return true;
		}

		/**
		 * Удаляет весь кеш
		 * @return bool
		 */
		public function flush() {
			$sql = <<<SQL
TRUNCATE TABLE `cms3_data_cache`;
SQL;
			$this->getConnection()
				->query($sql);

			return true;
		}

		/**
		 * Возвращает размер кеша в байтах
		 * @return int
		 */
		public function getCacheSize() {
			$sql = <<<SQL
SELECT ROUND(data_length + index_length) AS total_size
	FROM information_schema.tables
		WHERE table_schema = DATABASE()
			AND table_name = 'cms3_data_cache';
SQL;
			$connection = $this->getConnection();
			$result = $connection->queryResult($sql);

			if ($result->length() == 0) {
				return 0;
			}
			
			$row = $result->fetch();
			return (int) $row['total_size'];
		}

		/**
		 * Возвращает экземпляр ресурса соединения с бд
		 * @return IConnection
		 * @throws RequiredPropertyHasNoValueException
		 */
		private function getConnection() {
			$connection = $this->connection;

			if (!$connection instanceof IConnection) {
				throw new RequiredPropertyHasNoValueException('You should inject IConnection first');
			}

			return $connection;
		}

		/**
		 * Возвращает экземпляр реестра
		 * @return iRegedit
		 * @throws RequiredPropertyHasNoValueException
		 */
		private function getRegistry() {
			if ($this->registry === null) {
				$this->registry = regedit::getInstance();
			}

			if (!$this->registry instanceof iRegedit) {
				throw new RequiredPropertyHasNoValueException('You should inject iRegedit first');
			}

			return $this->registry;
		}

		/**
		 * Возвращает экземплял конфигурации
		 * @return iConfiguration
		 * @throws RequiredPropertyHasNoValueException
		 */
		private function getConfiguration() {
			if ($this->configuration === null) {
				$this->configuration = mainConfiguration::getInstance();
			}

			if (!$this->configuration instanceof iConfiguration) {
				throw new RequiredPropertyHasNoValueException('You should inject iConfiguration first');
			}

			return $this->configuration;
		}

		/**
		 * Обновляет количество обращений к кешу
		 * @param string $key ключ кеша
		 * @return bool
		 */
		private function updateEntry($key) {
			$connection = $this->getConnection();
			$escapedKey = $connection->escape($key);
			$entryTime = time();

			$sql = <<<SQL
UPDATE `cms3_data_cache`
	SET `entry_time` = $entryTime, `entries_number` = `entries_number` + 1
		WHERE `key` = '$escapedKey';
SQL;
			$connection->query($sql);

			return true;
		}

		/**
		 * Выполняет периодические операции над кешем:
		 *  1) Раз в час удаляет кеш с истекшим временем жизни
		 *  2) Раз в день (ночью) оптимизирует таблицу с кешем
		 * @return bool
		 */
		public function doPeriodicOperations() {
			$umiRegistry = $this->getRegistry();
			$currentDate = getdate(time());
			$nextCleanTime = (int) $umiRegistry->getVal('//settings/next_clean_time');

			if ($currentDate[0] >= $nextCleanTime) {
				$this->dropExpired();
				$umiRegistry->setVal('//settings/next_clean_time', $currentDate[0] + 3600);
			}

			$nextOptimiseTime = (int) $umiRegistry->getVal('//settings/next_optimise_time');

			if ($currentDate[0] >= $nextOptimiseTime && ($currentDate['hours'] > 0 && $currentDate['hours'] <= 6)) {
				$this->optimise();
				$umiRegistry->setVal('//settings/next_optimise_time', $currentDate[0] + 86400);
			}

			return true;
		}

		/**
		 * Удаляет кеш с истекшим временем жизни
		 * @return bool
		 */
		private function dropExpired() {
			$time = (int) time();

			$sql = <<<SQL
DELETE FROM `cms3_data_cache`
	WHERE `expire_time` < $time;
SQL;
			$this->getConnection()
				->query($sql);

			return true;
		}

		/**
		 * Оптимизирует таблицу с кешем
		 * @return bool
		 */
		private function optimise() {
			$sql = <<<SQL
OPTIMIZE TABLE `cms3_data_cache`;
SQL;
			$this->getConnection()
				->query($sql);

			return true;
		}

	}
