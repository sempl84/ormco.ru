<?php
	/**
	 * Внешний проксирующий класс для работы с кешем.
	 * В зависимости от возможностей окружения выбирается оптимальный cacheEngine.
	 */
	class cacheFrontend extends singleton implements iCacheFrontend, iSingleton {
		/**
		 * @var bool $cacheMode режим работы кеша
		 */
		public static $cacheMode;
		/**
		 * @var int|bool $currentDomainId идентификатор текущего домена
		 */
		public static $currentDomainId = false;
		/**
		 * @var int|bool $currentlangId идентификатор текущего языка
		 */
		public static $currentlangId = false;
		/**
		 * @var bool $adminMode включен ли режим администрирования (в нем данные не загружаются из кеша)
		 */
		public static $adminMode = false;
		/**
		 * @var iCacheEngine $cacheEngine класс текущего кеширующего механизма
		 */
		protected $cacheEngine;
		/**
		 * @var string $cacheEngineName название текущего кеширующего механизма
		 */
		protected $cacheEngineName = "";
		/**
		 * @var bool $connected подключен ли текущий кеширующий механизм
		 */
		protected $connected = false;
		/**
		 * @var bool $enabled доступно ли кеширование
		 */
		protected $enabled = false;
		/**
		 * @var bool $is_sleep приостановлено ли кеширование
		 */
		protected $is_sleep = false;
		/**
		 * @var string $mode режим
		 */
		protected $mode = "";
		/**
		 * @var string|null $queryStringHash  хеш от текущего значения PHP_URL_QUERY
		 */
		private $queryStringHash = null;

		/**
		 * @inherit
		 * @param string $c
		 * @return cacheFrontend
		 */
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * @deprecated
		 * @return bool
		 */
		public function getIsConnected() {
			return $this->connected;
		}

		/**
		 * Сохранить в кеш объект из ядра системы
		 * @param umiEntinty $object объект класса дочернего от umiEntinty
		 * @param string $objectType = "unknown" тип объекта
		 * @param int $expire = 86400 TTL записи
		 * @return bool результат операции
		 */
		public function save(umiEntinty $object, $objectType = "unknown", $expire = 86400) {
			if (!$this->isNeedToSave($expire)) {
				return false;
			}

			if (!$this->isValidObjectStoreType($objectType)) {
				return false;
			}

			if ($expire == 86400) {
				$expire = $this->tryGetCommonCacheTime($expire);
			}

			$key = $this->createObjectKey($object->getId(), $objectType);
			$this->clusterSync($key);
			$object->beforeSerialize();
			$result = $this->cacheEngine->saveObjectData($key, $object, $expire);
			$object->afterSerialize();
			return $result;
		}

		/**
		 * Загрузить из кеша объект ядра системы
		 * @param int $objectId id объекта
		 * @param string $objectType = "unknown" тип объекта
		 * @return object|false результат операции
		 */
		public function load($objectId, $objectType = "unknown") {
			if (!$this->isNeedToLoad()) {
				return false;
			}

			if (!$this->isValidObjectStoreType($objectType)) {
				return false;
			}

			$key = $this->createObjectKey($objectId, $objectType);
			$object = $this->cacheEngine->loadObjectData($key);

			if ($object instanceof umiEntinty) {
				$object->afterUnSerialize();
			}

			return $object;
		}

		/**
		 * Сохранить данные как результат запроса
		 * @param string $sqlString текст запроса, будет использоваться в качестве ключа
		 * @param mixed $sqlResult результат запроса (не #Resoure)
		 * @param int $expire = 2 TTL записи
		 * @return bool результат операции
		 */
		public function saveSql($sqlString, $sqlResult, $expire = 30) {
			if (!$this->isNeedToSave($expire)) {
				return false;
			}

			if ($expire == 30) {
				$expire = $this->tryGetCommonCacheTime($expire);
			}

			$key = $this->createObjectKey($this->convertSqlToHash($sqlString), 'sql');
			$this->clusterSync($key);
			return $this->cacheEngine->saveRawData($key, $sqlResult, $expire);
		}

		/**
		 * Загрузить данные как результат запроса
		 * @param string $sqlString текст запроса, будет использоваться в качестве ключа
		 * @return mixed результат запроса
		 */
		public function loadSql($sqlString) {
			if (!$this->isNeedToLoad()) {
				return false;
			}

			$key = $this->createObjectKey($this->convertSqlToHash($sqlString), 'sql');
			return $this->cacheEngine->loadRawData($key);
		}

		/**
		 * Сохранить обычные данные (строка, число)
		 * @param string $key ключ записи
		 * @param mixed $data данные
		 * @param int $expire TTL записи
		 * @return mixed значение ключа
		 */
		public function saveData($key, $data, $expire = 5) {
			if (!$this->isNeedToSave($expire)) {
				return false;
			}

			if (!$this->isValidDataKey($key)) {
				return false;
			}

			if ($expire == 5) {
				$expire = $this->tryGetCommonCacheTime($expire);
			}

			$key = $this->createKey($key);
			$this->clusterSync($key);
			return $this->cacheEngine->saveRawData($key, $data, $expire);
		}

		/**
		 * @deprecated
		 */
		public function saveObject($key, $data, $expire = 5) {
			if (!$this->isNeedToSave($expire)) {
				return false;
			}

			$key = $this->createKey($key);
			$this->clusterSync($key);
			return $this->cacheEngine->saveRawData($key, $data, $expire);
		}

		/**
		 * @deprecated
		 */
		public function saveElement($key, $data, $expire = 10) {
			if (!$this->isNeedToSave($expire)) {
				return false;
			}

			$key = $this->createKey($key);
			$this->clusterSync($key);
			return $this->cacheEngine->saveObjectData($key, $data, $expire);
		}

		/**
		 * Загрузить обычные данные (строка, число)
		 * @param string $key ключ записи
		 * @return mixed значение ключа
		 */
		public function loadData($key) {
			if (!$this->isNeedToLoad()) {
				return false;
			}

			if (!$this->isValidDataKey($key)) {
				return false;
			}

			$key = $this->createKey($key);
			return $this->cacheEngine->loadRawData($key);
		}

		/**
		 * Приостановить работу кеша
		 * @param bool $sleep = false флаг "сна"
		 * @return void
		 */
		public function makeSleep($sleep = false) {
			$this->is_sleep = $sleep;
		}

		/**
		 * Удалить ключ из кеша
		 * @param string $id ключ
		 * @param string|bool $type = false тип данных
		 * @return bool результат операции
		 */
		public function del($id, $type = false) {
			if (!$this->isNeedToDelete()) {
				return false;
			}

			$key = $this->createObjectKey($id, $type);
			$this->clusterSync($key);
			return $this->cacheEngine->delete($key);
		}

		/**
		 * Удалить ключ из кеша без его дополнительной обработки
		 * @param string $key чистый ключ
		 * @param bool $addSuffix = false если true, то в конец ключа будет добавлен суффикс ключа текущей установки
		 * @return bool результат операции
		 */
		public function deleteKey($key, $addSuffix = false) {
			if ($this->cacheEngine instanceof iCacheEngine) {
				if ($addSuffix) {
					$key .= $this->getKeySuffix(null, $key);
				}
				return $this->cacheEngine->delete($key);
			} else {
				return false;
			}
		}

		/**
		 * Сбросить весь кеш
		 */
		public function flush() {
			if ($this->cacheEngine instanceof iCacheEngine) {
				$this->cacheEngine->flush();
			}
		}

		/**
		 * Получить список возможных cacheEngines в порядке приоритета
		 * @param bool $enabledOnly вывести только доступные cacheEngines
		 * @return array список названий cacheEngine
		 */
		public static function getPriorityEnginesList($enabledOnly = false) {
			$list = array(
				'database',
				'apc',
				'eaccelerator',
				'xcache',
				'redis',
				'memcache',
				'fs'
			);
			if ($enabledOnly) {
				foreach($list as $i => $engineName) {
					if (self::checkEngine($engineName) == false) {
						unset($list[$i]);
					}
				}
			}
			return $list;
		}

		/**
		 * Выбрать наиболее подходящий кеширующий движок
		 * @param array $engines список названий доступных кеширущих движков
		 * @return string|bool название cacheEngine, либо false, если ничего не подходит
		 */
		public static function chooseCacheEngine($engines) {
			if (!is_array($engines)) {
				return false;
			}

			$result = array_intersect(self::getPriorityEnginesList(), $engines);
			if (sizeof($result)) {
				reset($result);
				return current($result);
			} else {
				return false;
			}
		}

		/**
		 * Получить название текущего cache engine
		 * @return string название cacheEngine'а
		 */
		public function getCurrentCacheEngineName() {
			return $this->cacheEngineName;
		}

		/**
		 * Изменить cacheEngine, который используется системой
		 * @param string $cacheEngineName название cacheEngine'а
		 * @return bool true, если изменение прошло успешно
		 */
		public function switchCacheEngine($cacheEngineName) {
			if (!$cacheEngineName) {
				return $this->saveCacheEngineName("");
			}
			if ($this->checkEngine($cacheEngineName)) {
				$this->flush();
				return $this->saveCacheEngineName($cacheEngineName);
			} else {
				return true;
			}
		}

		/**
		 * Возвращает хеш от текущего значения PHP_URL_QUERY
		 * @return string
		 */
		public function getQueryStringHash() {
			if (is_null($this->queryStringHash)) {
				$queryString = getServer('QUERY_STRING');
				$query = '';
				$matches = array();
				$success = preg_match('/([\?|\&][^\/#]*)/', $queryString, $matches);
				if ($success && isset($matches[0])) {
					$query = $matches[0];
				}
				$this->queryStringHash = md5($query);
			}
			return $this->queryStringHash;
		}

		/**
		 * Возвращает объем кеша в байтах
		 * @return bool
		 */
		public function getCacheSize() {
			if (!$this->cacheEngine instanceof iCacheEngine) {
				return false;
			}
			return $this->cacheEngine->getCacheSize();
		}

		/**
		 * Доступно ли кеширование
		 * @return bool
		 */
		public function isCacheEnabled() {
			return $this->enabled;
		}

		/**
		 * Запускает выполнение периодических операций у текущего кеширующего механизма
		 * @return mixed
		 */
		public function doPeriodicOperations() {
			if (!$this->cacheEngine instanceof iCacheEngine) {
				return false;
			}

			if (!method_exists($this->cacheEngine, __FUNCTION__)) {
				return false;
			}

			return $this->cacheEngine->doPeriodicOperations();
		}

		/**
		 * Конструктор
		 */
		protected function __construct() {
			$this->detectCacheEngine();
			$cacheEngine = $this->loadEngine($this->cacheEngineName);
			if ($cacheEngine instanceof iCacheEngine) {
				$this->connected = (bool) $cacheEngine->getIsConnected();
				if ($this->connected) {
					$this->enabled = true;
					self::$cacheMode = true;
					$this->cacheEngine = $cacheEngine;
					$this->checkLimit($cacheEngine);
					$this->checkIpAddress();
				}
			}
		}

		/**
		 * Проверяет не был ли превышен лимит на размер кеша, если был
		 * превышен - кеш полностью удаляется.
		 * @param iCacheEngine $cacheEngine кеширующий механизм
		 * @return bool
		 */
		protected function checkLimit(iCacheEngine $cacheEngine) {
			$umiConfigs = mainConfiguration::getInstance();
			$cacheLimit = $umiConfigs->get('cache', 'cache-size-limit');

			if (!is_numeric($cacheLimit)) {
				return true;
			}

			$cacheSize = $cacheEngine->getCacheSize();

			if (!is_numeric($cacheSize)) {
				return true;
			}

			if ($cacheLimit >= $cacheSize) {
				return true;
			}

			$cacheEngine->flush();
			return false;
		}

		/**
		 * Проверяет вхождение ip адреса пользователя в фильтр.
		 * Если ip адрес попадает под фильтр, то включается режим
		 * администрирования (см. self::$adminMode).
		 */
		protected function checkIpAddress() {
			$umiConfigs = mainConfiguration::getInstance();
			$filterIps = $umiConfigs->get('cache', 'filter.ip');

			if (is_array($filterIps) && in_array(getServer('REMOTE_ADDR'), $filterIps)) {
				self::$adminMode = true;
			}
		}

		/**
		 * Хеширует sql запрос и возвращает результат хеширования
		 * @param string $sql sql запрос
		 * @return string
		 */
		protected function convertSqlToHash($sql) {
			return sha1($sql);
		}

		/**
		 * Создает и возвращает ключ кеша для сущностей системы
		 * @param int $id идентификатор сущности (объекта класса, дочернего umiEntinty)
		 * @param string $type тип сущности (обычно содержится в свойстве $store_type,  см. например umiObject)
		 * @return string
		 */
		protected function createObjectKey($id, $type) {
			return $id . "_" . $type . $this->getKeySuffix($type, $id);
		}

		/**
		 * Создает и возвращает ключ кеша
		 * @param int $id идентификатор
		 * @return string
		 */
		protected function createKey($id) {
			return $id . $this->getKeySuffix('data', $id);
		}

		/**
		 * Возвращает суффикс для ключа кеша
		 * @param string $storeType тип хранимой информации
		 * @param string $key ключа кеша
		 * @return string
		 */
		protected function getKeySuffix($storeType, $key) {
			if (!is_string($storeType)) {
				$storeType = 'unknown';
			}

			static $suffix = null;

			if (is_null($suffix)) {
				$siteId = (defined('DEMO_DB_NAME')) ? DEMO_DB_NAME : CURRENT_WORKING_DIR;
				$siteId = sha1($siteId . SYS_CACHE_SALT);
				$suffix = $this->mode . '_' . $siteId;
			}

			$currentLangId = '';
			$currentDomainId = '';
			$needHierarchyPostfix = (
				!in_array($storeType, $this->getSpecialStoreTypes()) && !in_array($key, $this->getSystemListsKeys())
			);

			if ($needHierarchyPostfix) {
				$currentLangId = (is_numeric(self::$currentlangId)) ? self::$currentlangId : 'null';
				$currentDomainId = (is_numeric(self::$currentDomainId)) ? self::$currentDomainId : 'null';
			}

			return $suffix . $currentLangId . $currentDomainId;
		}

		/**
		 * Возвращает перечень типой хранимой информации, для
		 * которых не нужно добавлять в ключ идентификаторы домены и языка
		 * @return array
		 */
		protected function getSpecialStoreTypes() {
			return array(
				'message',
				'field',
				'fields_group',
				'field_type',
				'object',
				'property',
				'object_type',
				'domain',
				'lang',
				'template',
				'element',
				'element_type',
				'sql'
			);
		}

		/**
		 * Возвращает перечень системных ключей кеша получения списков объектов
		 * @return array
		 */
		protected function getSystemListsKeys() {
			return array(
				'field_types_list',
				'domains_list',
				'langs_list',
				'templates_list',
				'hierarchy_types_list',
				'currencies_list',
				'discounts_list',
				'payments_list'
			);
		}

		/**
		 * Загружает класс cacheEngine по его имени
		 * и возвращает его экземпляр
		 * @param string $engineName имя cacheEngine
		 * @return iCacheEngine|null
		 * @throws coreException
		 */
		protected function loadEngine($engineName) {
			if (!is_string($engineName)) {
				return null;
			}

			$filePath = SYS_KERNEL_PATH . 'subsystems/cache/engines/' . $engineName . '.php';
			if (file_exists($filePath)) {
				$className = $engineName . 'CacheEngine';

				if (!class_exists($className)) {
					include $filePath;
				}

				if (class_exists($className)) {
					return new $className;
				} else {
					throw new coreException('Failed to load cache engine: class "' . $className . '" not found in "' . $filePath . '"');
				}
			} else {
				return null;
			}
		}

		/**
		 * Определить текущий cacheEngine и установить как текущий
		 */
		protected function detectCacheEngine() {
			if ($cacheEngineName = $this->loadCacheEngineName()) {
				if ($this->checkEngine($cacheEngineName)) {
					$this->cacheEngineName = $cacheEngineName;
					return true;
				}
			}

			if ($cacheEngineName == "none") {
				return false;
			}

			if ($cacheEngineName == "auto") {
				$cacheEngineName = $this->autoDetectCacheEngine();
			}

			if ($cacheEngineName) {
				$this->cacheEngineName = $cacheEngineName;
				$this->saveCacheEngineName($cacheEngineName);
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Определяет backendEngine из списка доступных
		 * @return string|bool название доступного cacheEngine, либо false
		 */
		protected function autoDetectCacheEngine() {
			$list = $this->getPriorityEnginesList();
			foreach($list as $cacheEngineName) {
				if ($cacheEngineName == 'fs') {
					continue;
				}
				if ($this->checkEngine($cacheEngineName)) {
					return $cacheEngineName;
				}
			}
			return false;
		}

		/**
		 * Определить, доступен ли cacheEngine
		 * @param string $engineName название cacheEngine'а
		 * @return bool true, если cacheEngine доступен
		 */
		protected function checkEngine($engineName) {
			switch($engineName) {
				case 'apc': {
					return function_exists('apc_store');
				}

				case 'eaccelerator': {
					return function_exists('eaccelerator_put');
				}

				case 'xcache': {
					return function_exists('xcache_set');
				}

				case 'memcache': {
					return class_exists('Memcache');
				}

				case 'shm': {
					return function_exists('shm_attach');
				}

				case 'redis': {
					return class_exists('Redis');
				}

				case 'fs':
				case 'database': {
					return true;
				}

				default: {
					return false;
				}
			}
		}

		/**
		 * Сохранить название текущего cacheEngine во временный файл
		 * @param string $cacheEngineName название cacheEngine
		 * @return bool true, если сохранение произошло без ошибок
		 */
		protected function saveCacheEngineName($cacheEngineName) {
			$config = mainConfiguration::getInstance();
			$config->set('cache', 'engine', $cacheEngineName);
			$config->__destruct();
			return true;
		}

		/**
		 * Получить название текущего cacheEngine из временного файла
		 * @return string название текущего cacheEngine
		 */
		protected function loadCacheEngineName() {
			$config = mainConfiguration::getInstance();
			return $config->get('cache', 'engine');
		}

		/**
		 * @param string $key ключ кеша
		 */
		protected function clusterSync($key) {
			static $inst;

			if (CLUSTER_CACHE_CORRECTION) {
				if (!$inst) {
					$inst = clusterCacheSync::getInstance();
				}
				$suffix = $this->getKeySuffix(null, $key);
				$inst->notify(substr($key, 0, strlen($key) - strlen($suffix)));
			}
		}

		/**
		 * Пытается вернуть время жизни кеша из конфигурации системы,
		 * в случае неудачи - возвращает переданное значение
		 * @param int $expire время жизни кеша
		 * @return int
		 */
		private function tryGetCommonCacheTime($expire) {
			$config = mainConfiguration::getInstance();
			if ($config->get('cache', 'streams.cache-enabled')) {
				$newExpire = (int) $config->get('cache', 'streams.cache-lifetime');
				if ($newExpire > 0) {
					$expire = $newExpire;
				}
			}
			return $expire;
		}

		/**
		 * Валидирует ключ кеша
		 * @param string $key ключ кеша
		 * @return bool
		 */
		private function isValidDataKey($key) {
			$config = mainConfiguration::getInstance();
			$rules = $config->get('cache', 'not-allowed-methods');

			if (is_array($rules)) {
				foreach($rules as $rule) {
					if ($rule && strpos($key, $rule) !== false) {
						return false;
					}
				}
			}

			$rules = $config->get('cache', 'not-allowed-streams');

			if (is_array($rules)) {
				foreach($rules as $rule) {
					if ($rule && strpos($key, $rule) !== false) {
						return false;
					}
				}
			}
			return true;
		}

		/**
		 * Валидирует тип сохраняемой сущности
		 * @param string $objectStoreType тип сохраняемой сущности
		 * @return bool
		 */
		private function isValidObjectStoreType($objectStoreType) {
			$config = mainConfiguration::getInstance();
			$rules = $config->get('cache', 'not-allowed-store-type');
			if (is_array($rules)) {
				foreach ($rules as $rule) {
					if ($rule && $objectStoreType == $rule) {
						return false;
					}
				}
			}
			return true;
		}

		/**
		 * Необходимо ли совершать операции удаления кеша
		 * @return bool
		 */
		private function isNeedToDelete() {
			if (!$this->enabled) {
				return false;
			}
			if (!self::$cacheMode) {
				return false;
			}
			if ($this->is_sleep) {
				return false;
			}
			if (!$this->cacheEngine instanceof iCacheEngine) {
				return false;
			}
			return true;
		}

		/**
		 * Необходимо ли совершить операцию сохранения в кеш
		 * @param int $expire время жизни кеша в секундах
		 * @return bool
		 */
		private function isNeedToSave($expire) {
			if (!$this->enabled) {
				return false;
			}
			if (!self::$cacheMode) {
				return false;
			}
			if ($this->is_sleep) {
				return false;
			}
			if (!$expire) {
				return false;
			}
			if (!$this->cacheEngine instanceof iCacheEngine) {
				return false;
			}
			return true;
		}

		/**
		 * Необходимо ли совершать операции чтения кеша
		 * @return bool
		 */
		private function isNeedToLoad() {
			if (!$this->enabled) {
				return false;
			}
			if (!self::$cacheMode) {
				return false;
			}
			if ($this->is_sleep) {
				return false;
			}
			if (self::$adminMode) {
				return false;
			}
			if (!$this->cacheEngine instanceof iCacheEngine) {
				return false;
			}
			return true;
		}
	}