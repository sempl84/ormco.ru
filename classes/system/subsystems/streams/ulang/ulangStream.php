<?php

	/**
	 * Класс для работы с языковыми константами
	 *
	 * Умеет:
	 * 	- находить перевод языковой константы
	 *  - находить языковую константу по ее переводу
	 *  - возвращать префикс языковой версии сайта
	 *  - возвращать все языковые константы в форматах 'js' и 'dtd'
	 *
	 * @link http://dev.docs.umi-cms.ru/prakticheskie_primery/internacionalizaciya_sajta/internacionalizaciya_shablonov_dannyh/
	 */
	class ulangStream extends umiBaseStream {
		const i18nPrefix = 'i18n::';
		/**
		 * @var string $langPrefix текущий префикс языковой версии сайта
		 */
		private static $langPrefix;
		/**
		 * @var string $currentModuleName название текущего модуля
		 */
		private static $currentModuleName;
		/**
		 * @var string $templatesDirectoryPath путь до директории с шаблонами сайта
		 */
		private static $templatesDirectoryPath;

		/** @var array кэш загруженных переводов языковых констант */
		protected static $i18nCache = [];

		/** @var string протокол потока */
		protected $scheme = 'ulang';

		/**
		 * Выполняет запрос по протоколу ulang://
		 * @link http://php.net/manual/en/streamwrapper.stream-open.php
		 *
		 * @param string $path url запроса
		 * @param string $mode тип доступа к потоку
		 * @param int $options дополнительные флаги
		 * @param string $opened_path путь до открытого ресурса
		 * @return bool
		 */
		public function stream_open($path, $mode, $options, $opened_path) {
			static $cache = [];
			$info = parse_url($path);
			$path = trim(getArrayKey($info, 'host') . getArrayKey($info, 'path'), '/');

			if (substr($path, -5, 5) == ':file') {
				$dtdContent = $this->getExternalDTD(substr($path, 0, strlen($path) - 5));
				return $this->setData($dtdContent);
			}

			if (strpos(getArrayKey($info, 'query'), 'js') !== false) {
				$mode = 'js';
			} else {
				if (strpos($path, 'js') !== false) {
					$mode = 'js';
					$path = substr($path, 0, strlen($path) - 3);
				} else {
					$mode = 'dtd';
				}
			}

			if ($mode == 'js') {
				$buffer = outputBuffer::current();
				$buffer->contentType('text/javascript');
				$data = $this->generateJavaScriptLabels($path);
				return $this->setData($data);
			}

			if (isset($cache[$path])) {
				$data = $cache[$path];
			} else {
				$i18nMixed = self::loadI18NFiles($path);
				$data = $cache[$path] = $this->translateToDTD($i18nMixed);
			}

			return $this->setData($data);
		}

		/**
		 * Устанавливает название текущего модуля
		 * @param string $name название текущего модуля
		 * @throws coreException
		 */
		public static function setCurrentModuleName($name) {
			if (!is_string($name) || strlen($name) === 0) {
				throw new coreException('Incorrect module name given');
			}

			self::$currentModuleName = $name;
		}

		/**
		 * Устанавливает путь до директории с шаблонами сайта
		 * @param string $path путь до директории с шаблонами сайта
		 * @throws coreException
		 */
		public static function setTemplatesDirectoryPath($path) {
			if (!is_string($path) || strlen($path) === 0) {
				throw new coreException('Incorrect directory path given');
			}

			self::$templatesDirectoryPath = $path;
		}

		/**
		 * Устанавливает текущий префикс языковой версии сайта
		 * @param string $langPrefix префикс языковой версии сайта
		 * @throws coreException
		 */
		public static function setLangPrefix($langPrefix) {
			if (!is_string($langPrefix) || strlen($langPrefix) === 0) {
				throw new coreException('Incorrect language prefix given');
			}

			self::$langPrefix = $langPrefix;
		}

		/**
		 * Преобразовывает переводы констант в формат Document type definition
		 * @param array $translationMap массив вида [label => translation, ...]
		 * @return string
		 */
		protected function translateToDTD(array $translationMap) {
			$dtd = "<!ENTITY quote '&#34;'>\n";
			$dtd .= "<!ENTITY nbsp '&#160;'>\n";
			$dtd .= "<!ENTITY middot '&#183;'>\n";
			$dtd .= "<!ENTITY reg '&#174;'>\n";
			$dtd .= "<!ENTITY copy '&#169;'>\n";
			$dtd .= "<!ENTITY raquo '&#187;'>\n";
			$dtd .= "<!ENTITY laquo '&#171;'>\n";

			foreach ($translationMap as $label => $translation) {
				$translation = $this->protectEntityValue($translation);
				$dtd .= "<!ENTITY {$label} \"{$translation}\">\n";
			}

			return $dtd;
		}

		/**
		 * Экранирует значения символов для выдачи в формате XML
		 * @param string $translation перевод константы
		 * @return string
		 */
		protected function protectEntityValue($translation) {
			$from = ['&', '"', '%'];
			$to = ['&amp;', '&quote;', '&#037;'];
			return str_replace($from, $to, $translation);
		}

		/**
		 * Обрабатывает строку запроса констант.
		 * Возвращает названия модулей, чьи константы нужно загрузить.
		 * @param string $path строка с указанием загружаемых файлов констант
		 * @return array
		 */
		protected static function parseLangsPath($path) {
			$protocol = 'ulang://';

			if (0 === strpos($path, $protocol)) {
				$path = substr($path, strlen($protocol));
			}

			$path = trim($path, '/');
			return explode('/', $path);
		}

		/**
		 * Возвращает массив переводов констант в формате [label => translation, ...]
		 * @param string $path строка с указанием загружаемых файлов констант
		 * @return array
		 */
		protected static function loadI18NFiles($path) {
			$currentModule = self::getCurrentModuleName();
			$translationCache = self::$i18nCache;
			$requireList = self::parseLangsPath($path);
			$langPrefix = self::getLangPrefix();

			if (!in_array($currentModule, $requireList)) {
				$requireList[] = $currentModule;
			}

			$files = [
				[
					'dir' => SYS_MODULES_PATH,
					'file' => 'i18n.php'
				],
				[
					'dir' => SYS_MODULES_PATH,
					'file' => 'i18n.' . $langPrefix . '.php'
				],
			];

			$resourcesDirectory = self::getTemplatesDirectoryPath();

			if ($resourcesDirectory) {
				$files[] = [
					'dir' => $resourcesDirectory . '/classes/modules/',
					'file' => 'i18n.php'
				];
				$files[] = [
					'dir' => $resourcesDirectory . '/classes/modules/',
					'file' => 'i18n.' . $langPrefix . '.php'
				];
			}

			$allTranslations = [];

			foreach ($requireList as $requireName) {
				if ($requireName == false) {
					continue;
				}

				$folder = ($requireName == 'common') ? '' : $requireName . '/';

				if (array_key_exists($requireName, $translationCache)) {
					$currentTranslations = $translationCache[$requireName];
				} else {
					$currentTranslations = [];

					foreach ($files as $source) {
						$filename = $source['dir'] . $folder . $source['file'];

						if (file_exists($filename)) {
							/** @var array $i18n переменная наполняется в загружаемом i18n-файле */
							$i18n = [];
							include $filename;
							foreach ($i18n as $label => $translation) {
								$currentTranslations[$label] = $translation;
							}
						}
					}

					$externalTranslations = self::loadExtI18n($folder);
					foreach ($externalTranslations as $label => $translation) {
						if (!array_key_exists($label, $currentTranslations)) {
							$currentTranslations[$label] = $translation;
						}
					}
				}

				if (is_array($currentTranslations)) {
					$translationCache[$requireName] = $currentTranslations;
					foreach ($currentTranslations as $label => $translation) {
						$allTranslations[$label] = $translation;
					}
				}
			}

			self::$i18nCache = $translationCache;
			return $allTranslations;
		}

		/**
		 * Подключает файлы локализации i18n из расширений
		 * @param string $moduleFolder директория с модулем
		 * @return array
		 */
		public static function loadExtI18n($moduleFolder) {
			$langPrefix = self::getLangPrefix();
			$langPath = SYS_MODULES_PATH . $moduleFolder . 'ext/i18n.*.' . $langPrefix . '.php';
			$fileNames = glob($langPath);

			$translations = [];

			if (is_array($fileNames)) {
				foreach ($fileNames as $filename) {
					if (file_exists($filename)) {
						/** @var array $i18n переменная наполняется в загружаемом i18n-файле */
						$i18n = [];
						include $filename;
						foreach ($i18n as $label => $translation) {
							$translations[$label] = $translation;
						}
					}
				}
			}

			return $translations;
		}

		public static function getLabelSimple($label, array $params = []) {
			return self::getLabel($label, false, array_merge([$label, false], $params));
		}

		/**
		 * Возвращает перевод языковой константы
		 * @param string $label языковая константа
		 * @param bool|string $path строка с указанием загружаемых файлов констант
		 * @param null|array $args дополнительные аргументы для форматирования перевода константы
		 * @return mixed|string
		 */
		public static function getLabel($label, $path = false, $args = null) {
			static $cache = [];
			static $langPrefix = false;

			if ($langPrefix === false) {
				$langPrefix = self::getLangPrefix();
			}

			$langPath = ($path == false) ? 'common/data' : $path;

			if (isset($cache[$langPath])) {
				$translationMap = $cache[$langPath];
			} else {
				$translationMap = self::loadI18NFiles($langPath);
				$cache[$langPath] = &$translationMap;
			}

			if (isset($translationMap[$label])) {
				$translation = $translationMap[$label];
			} elseif (!$path && strpos($label, 'module-') === 0) {
				$moduleName = str_replace('module-', '', $label);
				$translation = self::getLabel($label, $moduleName, $args);
			} else {
				$translation = $label;
			}

			if (is_array($args) && count($args) > 2) {
				$translation = vsprintf($translation, array_slice($args, 2));
			}

			return $translation;
		}

		/**
		 * Возвращает языковую константу по ее переводу
		 * @param string $translation перевод константы
		 * @param string $typePrefix префикс константы (например `fields-group`)
		 * @param bool $searchAllEntries найти все константы по вхождению строки в перевод
		 * @return array|null|string
		 */
		public static function getI18n($translation, $typePrefix = '', $searchAllEntries = false) {
			/** @var string[][] $cache сохраненные переводы языковых констант */
			static $cache = [];

			if (!$translation) {
				return $translation;
			}

			$translation = (string) $translation;
			$langPath = 'common/data';

			if (isset($cache[$langPath])) {
				$translationMap = $cache[$langPath];
			} else {
				$translationMap = self::loadI18NFiles($langPath);
				$cache[$langPath] = $translationMap;
			}

			if ($searchAllEntries) {
				$result = self::findAllTranslations($translationMap, $translation);
			} else {
				$result = self::findTranslation($translationMap, $translation, $typePrefix);
			}

			if ($result === null) {
				return $result;
			}

			$allowedPrefixes = [
				'object-type',
				'hierarchy-type',
				'field',
				'fields-group',
				'field-type',
				'object'
			];

			if (is_string($result)) {
				$isAllowed = false;
				$trimmedPrefix = str_replace(self::i18nPrefix, '', $typePrefix);

				if (!$trimmedPrefix) {
					return $result;
				}

				foreach ($allowedPrefixes as $allowedPrefix) {
					if (0 === strpos($trimmedPrefix, $allowedPrefix)) {
						$isAllowed = true;
						break;
					}
				}

				return $isAllowed ? $result : null;
			}

			$allowedResult = [];

			/** @var array $result */
			foreach ($result as $pendingTranslation) {
				$isAllowed = false;
				$trimmedPrefix = str_replace(self::i18nPrefix, '', $typePrefix);

				if (!$trimmedPrefix) {
					return $result;
				}

				foreach ($allowedPrefixes as $allowedPrefix) {
					if (0 === strpos($trimmedPrefix, $allowedPrefix)) {
						$isAllowed = true;
						break;
					}
				}

				if ($isAllowed) {
					$allowedResult[] = $pendingTranslation;
				}
			}

			if (count($allowedResult) == 0) {
				return null;
			}

			return $allowedResult;
		}

		/**
		 * Возвращает все языковые константы по переводу
		 * @see $this->getI18n()
		 * @param array $translationMap
		 * @param string $key
		 * @return array
		 */
		private static function findAllTranslations(array $translationMap, $key) {
			$key = mb_convert_case($key, MB_CASE_LOWER);
			$result = [];

			foreach ($translationMap as $label => $translation) {
				$translation = mb_convert_case($translation, MB_CASE_LOWER);
				if (false !== strpos($translation, $key)) {
					$result[] = self::i18nPrefix . $label;
				}
			}

			return $result;
		}

		/**
		 * Возвращает языковую константу по переводу
		 * @see $this->getI18n()
		 * @param array $translationMap
		 * @param string $key
		 * @param string $typePrefix
		 * @return string
		 */
		private static function findTranslation(array $translationMap, $key, $typePrefix) {
			foreach ($translationMap as $label => $translation) {
				if ($translation == $key) {
					if ($typePrefix && (0 !== strpos($label, $typePrefix))) {
						continue;
					}

					return self::i18nPrefix . $label;
				}
			}
		}

		/**
		 * Возвращает текущий префикс языковой версии сайта.
		 * Если префикс не задан - пытается его определить.
		 * @return bool|mixed|null|String
		 */
		public static function getLangPrefix() {
			if (null !== self::$langPrefix) {
				return self::$langPrefix;
			}

			$cmsController = cmsController::getInstance();
			$prefix = $cmsController->getCurrentLang()->getPrefix();

			if (!defined('VIA_HTTP_SCHEME') && $cmsController->getCurrentMode() != 'admin') {
				return self::$langPrefix = $prefix;
			}

			if (defined('VIA_HTTP_SCHEME')) {
				$elementId = $cmsController->getCurrentElementId();
				$element = umiHierarchy::getInstance()->getElement($elementId);
				if ($element instanceOf umiHierarchyElement) {
					return self::$langPrefix = langsCollection::getInstance()->getLang($element->getLangId())->getPrefix();
				} else {
					return self::$langPrefix = $prefix;
				}
			}

			$cookieJar = \UmiCms\Service::CookieJar();

			if (null !== (self::$langPrefix = getArrayKey($_POST, 'ilang'))) {
				$cookieJar->set('ilang', self::$langPrefix, time() + 3600 * 24 * 31);
				return self::$langPrefix;
			}

			if (null !== (self::$langPrefix = getArrayKey($_GET, 'ilang'))) {
				$cookieJar->set('ilang', self::$langPrefix, time() + 3600 * 24 * 31);
				return self::$langPrefix;
			}

			if (null !== (self::$langPrefix = $cookieJar->get('ilang'))) {
				$cookieJar->set('ilang', self::$langPrefix, time() + 3600 * 24 * 31);
				return self::$langPrefix;
			}

			return self::$langPrefix = $prefix;
		}

		/**
		 * Возвращает js-код для работы с константами во фронтенде.
		 * Будут доступны константы с префиксами 'js', 'module' и 'error'.
		 * @param string $path строка с указанием загружаемых файлов констант
		 * @return string
		 */
		protected function generateJavaScriptLabels($path) {
			$translationMap = self::loadI18NFiles($path);
			$regedit = regedit::getInstance();
			$modulesList = $regedit->getList('//modules');

			foreach ($modulesList as $moduleName) {
				list($moduleName) = $moduleName;
				if (!isset($translationMap['module-' . $moduleName])) {
					$translationMap['module-' . $moduleName] = self::getLabel('module-' . $moduleName, $moduleName);
				}
			}

			$result = <<<INITJS
function getLabel(key, str) {if(setLabel.langLabels[key]) {var res = setLabel.langLabels[key];if(str) {res = res.replace(/\%s/g, str);}return res;} else {return "[" + key + "]";}}
function setLabel(key, label) {setLabel.langLabels[key] = label;}setLabel.langLabels = new Array();


INITJS;
			foreach ($translationMap as $label => $translation) {
				if (strpos($label, 'js-') === 0 || strpos($label, 'module-') === 0 || strpos($label, 'error-') === 0) {
					$label = $this->filterOutputString($label);
					$translation = $this->filterOutputString($translation);
					$result .= "setLabel('{$label}', '{$translation}');\n";
				}
			}

			umiBaseStream::$allowTimeMark = false;
			return $result;
		}

		/**
		 * Экранирует строку
		 * @param string $string
		 * @return mixed
		 */
		protected function filterOutputString($string) {
			$from = ["\r\n", "\n", "'"];
			$to = ["\\r\\n", "\\n", "\\'"];
			return str_replace($from, $to, $string);
		}

		/**
		 * Загружает файл в формате Document type definition
		 * @param string $path путь до файла
		 * @return string
		 */
		protected function getExternalDTD($path) {
			$cmsController = cmsController::getInstance();
			$prefix = $cmsController->getCurrentLang()->getPrefix();

			$info = getPathInfo($cmsController->getTemplatesDirectory() . $path);

			$left = getArrayKey($info, 'dirname') . '/' . getArrayKey($info, 'filename');
			$right = getArrayKey($info, 'extension');

			$primaryPath = $left . '.' . $prefix . '.' . $right;
			$secondaryPath = $left . '.' . $right;

			if (is_file($primaryPath)) {
				return file_get_contents($primaryPath);
			}

			if (is_file($secondaryPath)) {
				return file_get_contents($secondaryPath);
			}

			return '';
		}

		/** @deprecated */
		protected function isRestrictedRef($ref) {
			$arr = ['field-', 'object-type-', 'hierarchy-type-', 'fields-group-', 'field-type-'];

			for ($i = 0; $i < count($arr); $i++) {
				if (substr($ref, 0, strlen($arr[$i])) == $arr[$i]) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Возвращает название текущего модуля.
		 * Если оно не установлено - пытается его определить.
		 * @return string
		 */
		private static function getCurrentModuleName() {
			if (self::$currentModuleName !== null) {
				return self::$currentModuleName;
			}

			return self::$currentModuleName = cmsController::getInstance()
				->getCurrentModule();
		}

		/**
		 * Возвращает путь до директории с шаблонами сайта.
		 * Если путь не задан, то пытается его определить.
		 * @return string
		 */
		private static function getTemplatesDirectoryPath() {
			if (self::$templatesDirectoryPath !== null) {
				return self::$templatesDirectoryPath;
			}

			$templatesDirectoryPath = (string) cmsController::getInstance()
				->getResourcesDirectory();

			return self::$templatesDirectoryPath = rtrim($templatesDirectoryPath, '/');
		}
	}
