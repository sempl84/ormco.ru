<?php
namespace UmiCms;
/**
 * Класс фасада для получения сервисов
 * @package UmiCms\Service
 */
class Service implements iService {
	/**
	 * @inheritdoc
	 * @return \umiRedirectsCollection
	 */
	public static function Redirects() {
		return self::getServiceContainer()->get('Redirects');
	}

	/**
	 * @inheritdoc
	 * @return \MailTemplatesCollection
	 */
	public static function MailTemplates() {
		return self::getServiceContainer()->get('MailTemplates');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Auth\iAuth
	 */
	public static function Auth() {
		return self::getServiceContainer()->get('Auth');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Protection\CsrfProtection
	 */
	public static function CsrfProtection() {
		return self::getServiceContainer()->get('CsrfProtection');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Permissions\iSystemUsersPermissions
	 */
	public static function SystemUsersPermissions() {
		return self::getServiceContainer()->get('SystemUsersPermissions');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Auth\PasswordHash\iAlgorithm
	 */
	public static function PasswordHashAlgorithm() {
		return self::getServiceContainer()->get('PasswordHashAlgorithm');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Request\Request
	 */
	public static function Request() {
		return self::getServiceContainer()->get('Request');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Cookies\CookieJar
	 */
	public static function CookieJar() {
		return self::getServiceContainer()->get('CookieJar');
	}

	/**
	 * @inheritdoc
	 * @return \UmiCms\System\Session\iSession
	 */
	public static function Session() {
		return self::getServiceContainer()->get('Session');
	}

	/**
	 * Возвращает контейнер сервисов
	 * @return \iServiceContainer
	 */
	private static function getServiceContainer() {
		return \ServiceContainerFactory::create();
	}
}