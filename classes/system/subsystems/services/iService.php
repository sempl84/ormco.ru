<?php
namespace UmiCms;
/**
 * Интерфейс фасада для получения сервисов
 * @package UmiCms\Service
 */
interface iService {
	/**
	 * Возвращает экземпляр класса коллекции редиректов
	 * @return \umiRedirectsCollection
	 */
	public static function Redirects();

	/**
	 * Возвращает экземпляр класса коллекции шаблонов писем
	 * @return \MailTemplatesCollection
	 */
	public static function MailTemplates();

	/**
	 * Возвращает экземпляр класса фасада для работы с аутентификацией и авторизацией пользователя
	 * @return \UmiCms\System\Auth\iAuth
	 */
	public static function Auth();

	/**
	 * Возвращает экземпляр класса защиты от CSRF
	 * @return \UmiCms\System\Protection\CsrfProtection
	 */
	public static function CsrfProtection();

	/**
	 * Возвращает экземпляр класса прав системных пользователей
	 * @return \UmiCms\System\Permissions\iSystemUsersPermissions
	 */
	public static function SystemUsersPermissions();

	/**
	 * Возвращает экземпляр класса алгоритма хеширования паролей
	 * @return \UmiCms\System\Auth\PasswordHash\iAlgorithm
	 */
	public static function PasswordHashAlgorithm();
	/**
	 * Возвращает экземпляр класса http запроса
	 * @return \UmiCms\System\Request\Request
	 */
	public static function Request();

	/**
	 * Возвращает экземпляр класса для работы с куками
	 * @return \UmiCms\System\Cookies\CookieJar
	 */
	public static function CookieJar();

	/**
	 * Возвращает экземпляр класса для работы с сессией
	 * @return \UmiCms\System\Session\iSession
	 */
	public static function Session();
}