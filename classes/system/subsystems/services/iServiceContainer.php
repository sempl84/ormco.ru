<?php
/**
 * Интерфейс сервис контейнера
 */
interface iServiceContainer {
	/**
	 * Конструктор
	 * @param array $rules правила инстанциирования сервисов
	 * @param array $parameters параметры для инстанцирования сервисов
	 */
	public function __construct(array $rules = [], array $parameters = []);
	/**
	 * Возвращает сервис по его имени
	 * @param string $name имя сервиса
	 * @return object
	 * @throws Exception
	 */
	public function get($name);
	/**
	 * Существуют ли правила инстанциирования для сервиса
	 * @param string $name имя сервиса
	 * @return bool
	 */
	public function hasRules($name);
	/**
	 * Добавить правила инстанциирования сервисов
	 * @param array $rules
	 */
	public function addRules(array $rules);
	/**
	 * Добавить параметры инстанциирования сервисов
	 * @param array $params
	 */
	public function addParameters(array $params);
	/**
	 * Существуют ли параметры инстанциирования сервиса
	 * @param string $name имя сервиса
	 * @return bool
	 */
	public function hasParameter($name);
}