<?php
	use UmiCms\System\Request;
	use UmiCms\System\Cookies;
	/**
	 * Фабрика контейнеров сервисов
	 * Через него следует получать желаемый контейнер,
	 * @example $ServiceContainer = ServiceContainerFactory::create();
	 */
	class ServiceContainerFactory implements iServiceContainerFactory {
		/** @var ServiceContainer[] $serviceContainerList список контейнеров сервисов */
		private static $serviceContainerList = [];

		/**
		 * {@inheritdoc}
		 */
		public static function create($type = self::DEFAULT_CONTAINER_TYPE, array $rules = [], array $parameters = []) {
			if (isset(self::$serviceContainerList[$type])) {
				return self::$serviceContainerList[$type];
			}

			$defaultRules = self::getDefaultRules();
			$defaultParameters = self::getDefaultParameters();

			if ($type !== self::DEFAULT_CONTAINER_TYPE) {
				$rules = array_merge($defaultRules, $rules);
				$parameters = array_merge($defaultParameters, $parameters);
			} else {
				$rules = $defaultRules;
				$parameters = $defaultParameters;
			}

			return self::$serviceContainerList[$type] = new ServiceContainer($rules, $parameters);
		}

		/**
		 * Возвращает список параметров по умолчанию для контейнера сервисов
		 * @return array
		 * @throws Exception
		 * @throws coreException
		 */
		protected static function getDefaultParameters() {
			return [
				'connection' => ConnectionPool::getInstance()->getConnection(),
				'configuration' => mainConfiguration::getInstance(),
				'httpBuffer' => outputBuffer::current('HTTPOutputBuffer'),
				'serverProtocol' => getSelectedServerProtocol(),
				'imageFileHandler' => new umiImageFile(__FILE__),
				'baseUmiCollectionConstantMap' => new baseUmiCollectionConstantMap(),
				'registry' => regedit::getInstance(),
				'directoriesHandler' => new umiDirectory(__FILE__),
				'umiRedirectsCollection' => 'Redirects',
				'MailTemplatesCollection' => 'MailTemplates',
				'selectorObjects' => new \selector('objects'),
				'cookies' => new Request\Cookies(),
				'files' => new Request\Files(),
				'get' => new Request\Get(),
				'post' => new Request\Post(),
				'server' => new Request\Server(),
				'cookiesFactory' => new Cookies\Factory(),
				'cookiesResponsePool' => new Cookies\ResponsePool()
			];
		}

		/**
		 * Возвращает список правил инстанцирования сервисов по умолчанию для контейнера сервисов
		 * @return array
		 */
		protected static function getDefaultRules() {
			return [
				'Redirects' => [
					'class' => 'umiRedirectsCollection',
					'arguments' => [
						new ParameterReference('umiRedirectsCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setConfiguration',
							'arguments' => [
								new ParameterReference('configuration')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new InstantiableReference('umiRedirectsConstantMap')
							]
						],
						[
							'method' => 'setBuffer',
							'arguments' => [
								new ParameterReference('httpBuffer')
							]
						],
						[
							'method' => 'setProtocol',
							'arguments' => [
								new ParameterReference('serverProtocol')
							]
						]
					]
				],

				'MailTemplates' => [
					'class' => 'MailTemplatesCollection',
					'arguments' => [
						new ParameterReference('MailTemplatesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new InstantiableReference('mailTemplatesConstantMap')
							]
						]
					]
				],

				'AuthenticationRulesFactory' => [
					'class' => 'UmiCms\System\Auth\AuthenticationRules\Factory',
					'arguments' => [
						new ServiceReference('PasswordHashAlgorithm'),
						new ParameterReference('selectorObjects')
					]
				],

				'PasswordHashAlgorithm' => [
					'class' => 'UmiCms\System\Auth\PasswordHash\Algorithm'
				],

				'Authentication' => [
					'class' => 'UmiCms\System\Auth\Authentication',
					'arguments' => [
						new ServiceReference('AuthenticationRulesFactory'),
						new ServiceReference('Session')
					]
				],

				'Authorization' => [
					'class' => 'UmiCms\System\Auth\Authorization',
					'arguments' => [
						new ServiceReference('Session'),
						new ServiceReference('CsrfProtection'),
						new ServiceReference('permissionsCollection'),
						new ServiceReference('CookieJar')
					]
				],

				'SystemUsersPermissions' => [
					'class' => 'UmiCms\System\Permissions\SystemUsersPermissions',
					'arguments' => [
						new ServiceReference('objects')
					]
				],

				'Auth' => [
					'class' => 'UmiCms\System\Auth\Auth',
					'arguments' => [
						new ServiceReference('Authentication'),
						new ServiceReference('Authorization'),
						new ServiceReference('SystemUsersPermissions')
					]
				],

				'CsrfProtection' => [
					'class' => '\UmiCms\System\Protection\CsrfProtection',
					'arguments' => [
						new ServiceReference('Session'),
						new ServiceReference('cmsController')
					],
					'calls' => [
						[
							'method' => 'loadToken'
						]
					]
				],

				'Request' => [
					'class' => '\UmiCms\System\Request\Request',
					'arguments' => [
						new ParameterReference('cookies'),
						new ParameterReference('server'),
						new ParameterReference('post'),
						new ParameterReference('get'),
						new ParameterReference('files')
					]
				],

				'CookieJar' => [
					'class' => 'UmiCms\System\Cookies\CookieJar',
					'arguments' => [
						new ParameterReference('cookiesFactory'),
						new ParameterReference('cookiesResponsePool'),
						new ParameterReference('cookies')
					]
				],

				'Session' => [
					'class' => 'UmiCms\System\Session\Session',
					'arguments' => [
						new ParameterReference('configuration'),
						new ServiceReference('CookieJar')
					]
				],

				'templates' => [
					'class' => 'templatesCollection',
				],

				'pages' => [
					'class' => 'umiHierarchy',
				],

				'domains' => [
					'class' => 'domainsCollection',
				],

				'cmsController' => [
					'class' => 'cmsController',
				],

				'objects' => [
					'class' => 'umiObjectsCollection',
				],

				'permissionsCollection' => [
					'class' => 'permissionsCollection',
				]
			];
		}
	}
