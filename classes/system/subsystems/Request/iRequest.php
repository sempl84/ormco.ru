<?php
namespace UmiCms\System\Request;
/**
 * Интерфейс http запроса
 * @package UmiCms\System\Request
 */
interface iRequest {
	/**
	 * Конструктор
	 * @param iCookies $cookies контейнер кук запроса
	 * @param iServer $server контейнер серверных переменных
	 * @param iPost $post контейнер POST параметров
	 * @param iGet $get контейнер GET параметров
	 * @param iFiles $files контейнер загруженных файлов
	 */
	public function __construct(iCookies $cookies, iServer $server, iPost $post, iGet $get, iFiles $files);
	/**
	 * Возвращает контейнер кук запроса
	 * @return iCookies
	 */
	public function cookies();
	/**
	 * Возвращает контейнер серверных переменных
	 * @return iServer
	 */
	public function server();
	/**
	 * Возвращает контейнер POST параметров
	 * @return iPost
	 */
	public function post();
	/**
	 * Возвращает контейнер GET параметров
	 * @return iGet
	 */
	public function get();
	/**
	 * Возвращает контейнер загруженных файлов
	 * @return iFiles
	 */
	public function files();
	/**
	 * Возвращает метод
	 * @return string
	 */
	public function method();
	/**
	 * Возвращает хост
	 * @return string
	 */
	public function host();
	/**
	 * Возвращает user agent
	 * @return string
	 */
	public function userAgent();
	/**
	 * Возвращает ip адрес
	 * @return string
	 */
	public function remoteAddress();
	/**
	 * Возвращает uri
	 * @return string
	 */
	public function uri();
	/**
	 * Возвращает query
	 * @return string
	 */
	public function query();
}