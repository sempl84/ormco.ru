<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Интерфейс контейнера POST параметров
 * @package UmiCms\System\Request
 */
interface iPost extends Patterns\iArrayContainer {}