<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Класс контейнера кук запроса
 * @package UmiCms\System\Request
 */
class Cookies extends Patterns\ArrayContainer implements iCookies {
	/**
	 * @inheritdoc
	 */
	public function __construct(array $array = []) {
		if (empty($array)) {
			$this->array = $_COOKIE;
		} else {
			parent::__construct($array);
		}
	}
}