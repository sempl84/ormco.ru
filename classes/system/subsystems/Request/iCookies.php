<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Интерфейс контейнера кук запроса
 * @package UmiCms\System\Request
 */
interface iCookies extends Patterns\iArrayContainer {}