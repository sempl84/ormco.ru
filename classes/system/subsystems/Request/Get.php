<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Класс контейнера GET параметров
 * @package UmiCms\System\Request
 */
class Get extends Patterns\ArrayContainer implements iGet {
	/**
	 * @inheritdoc
	 */
	public function __construct(array $array = []) {
		if (empty($array)) {
			$this->array = $_GET;
		} else {
			parent::__construct($array);
		}
	}
}