<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Интерфейс контейнера загруженных файлов
 * @package UmiCms\System\Request
 */
interface iFiles extends Patterns\iArrayContainer {}