<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Интерфейс контейнера GET параметров
 * @package UmiCms\System\Request
 */
interface iGet extends Patterns\iArrayContainer {}