<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Класс контейнера POST параметров
 * @package UmiCms\System\Request
 */
class Post extends Patterns\ArrayContainer implements iPost {
	/**
	 * @inheritdoc
	 */
	public function __construct(array $array = []) {
		if (empty($array)) {
			$this->array = $_POST;
		} else {
			parent::__construct($array);
		}
	}
}