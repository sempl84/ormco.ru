<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Интерфейс контейнера серверных переменных
 * @package UmiCms\System\Request
 */
interface iServer extends Patterns\iArrayContainer {}