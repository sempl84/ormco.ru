<?php
namespace UmiCms\System\Request;
/**
 * Класс http запроса
 * @package UmiCms\System\Request
 */
class Request implements iRequest {
	/**
	 * @var iCookies $cookies контейнер кук запроса
	 */
	private $cookies;
	/**
	 * @var iServer $server контейнер серверных переменных
	 */
	private $server;
	/**
	 * @var iPost $post контейнер POST параметров
	 */
	private $post;
	/**
	 * @var iGet $get контейнер GET параметров
	 */
	private $get;
	/**
	 * @var iFiles $files контейнер загруженных файлов
	 */
	private $files;

	/**
	 * @inheritdoc
	 */
	public function __construct(iCookies $cookies, iServer $server, iPost $post, iGet $get, iFiles $files) {
		$this->cookies = $cookies;
		$this->server = $server;
		$this->post = $post;
		$this->get = $get;
		$this->files = $files;
	}

	/**
	 * @inheritdoc
	 */
	public function cookies() {
		return $this->cookies;
	}

	/**
	 * @inheritdoc
	 */
	public function server() {
		return $this->server;
	}

	/**
	 * @inheritdoc
	 */
	public function post() {
		return $this->post;
	}

	/**
	 * @inheritdoc
	 */
	public function get() {
		return $this->get;
	}

	/**
	 * @inheritdoc
	 */
	public function files() {
		return $this->files;
	}

	/**
	 * @inheritdoc
	 */
	public function method() {
		return $this->server()->get('REQUEST_METHOD');
	}

	/**
	 * @inheritdoc
	 */
	public function host() {
		return $this->server()->get('HTTP_HOST');
	}

	/**
	 * @inheritdoc
	 */
	public function userAgent() {
		return $this->server()->get('HTTP_USER_AGENT');
	}

	/**
	 * @inheritdoc
	 */
	public function remoteAddress() {
		return $this->server()->get('REMOTE_ADDR');
	}

	/**
	 * @inheritdoc
	 */
	public function uri() {
		return $this->server()->get('REQUEST_URI');
	}

	/**
	 * @inheritdoc
	 */
	public function query() {
		return $this->server()->get('QUERY_STRING');
	}
}