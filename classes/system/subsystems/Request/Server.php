<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Класс контейнера серверных переменных
 * @package UmiCms\System\Request
 */
class Server extends Patterns\ArrayContainer implements iServer {
	/**
	 * @inheritdoc
	 */
	public function __construct(array $array = []) {
		if (empty($array)) {
			$this->array = $_SERVER;
		} else {
			parent::__construct($array);
		}
	}
}