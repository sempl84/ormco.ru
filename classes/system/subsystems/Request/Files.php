<?php
namespace UmiCms\System\Request;
use UmiCms\System\Patterns;
/**
 * Класс контейнера загруженных файлов
 * @package UmiCms\System\Request
 */
class Files extends Patterns\ArrayContainer implements iFiles {
	/**
	 * @inheritdoc
	 */
	public function __construct(array $array = []) {
		if (empty($array)) {
			$this->array = $_FILES;
		} else {
			parent::__construct($array);
		}
	}
}