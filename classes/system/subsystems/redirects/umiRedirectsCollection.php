<?php
	/**
	 * Коллекция и менеджер редиректов.
	 * Примеры использования можно посмотреть в методах:
	 *
	 * 	1) UmiRedirectsAdmin::lists();
	 *  2) UmiRedirectsAdmin::add();
	 *  3) UmiRedirectsAdmin::edit();
	 *  4) UmiRedirectsAdmin::del();
	 *  5) UmiRedirectsAdmin::saveValue();
	 */
	class umiRedirectsCollection implements
		iUmiCollection,
		iRedirects,
		iUmiDataBaseInjector,
		iUmiConfigInjector,
		iUmiBufferInjector,
		iUmiService,
		iUmiConstantMapInjector,
		iClassConfigManager
	{

		use tUmiDataBaseInjector;
		use tUmiService;
		use tUmiBufferInjector;
		use tUmiConfigInjector;
		use tUmiConstantMapInjector;
		use tCommonCollection;
		use tClassConfigManager;

		/**
		 * @var string $urlSuffix суффикс для адреса
		 */
		private $urlSuffix;
		/**
		 * @var string $urlProtocol протокол адреса
		 */
		private $urlProtocol = 'http';
		/**
		 * @var array $autoCreateRedirectHandledEvents список имен событий, для которых модуль создает слушателей
		 */
		private $autoCreateRedirectHandledEvents = [
			'systemModifyElement',
			'systemMoveElement'
		];
		/**
		 * @var string $collectionItemClass класс элементов данной коллекции
		 */
		private $collectionItemClass = 'umiRedirect';

		/** @var array конфигурация класса */
		private static $classConfig = [
			'service' => 'Redirects',
			'fields' => [
				[
					'name' => 'ID_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'used-in-creation' => false,
				],
				[
					'name' => 'SOURCE_FIELD_NAME',
					'type' => 'STRING_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'TARGET_FIELD_NAME',
					'type' => 'STRING_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'STATUS_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'MADE_BY_USER_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'title' => 'Made By user',
					'required' => true,
				],
			]
		];

		/**
		 * {@inheritdoc}
		 * @return umiRedirectsCollection
		 */
		public static function getInstance($c = NULL) {
			$serviceContainer = ServiceContainerFactory::create();
			return $serviceContainer->get(self::getConfig()->get('service'));
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function add($source, $target, $status = 301, $madeByUser = false) {
			if ($source == $target) {
				return false;
			}

			$connection = $this->getConnection();
			$source = $connection->escape($this->parseUri($source));
			$target = $connection->escape($this->parseUri($target));
			$status = (int) $status;

			if (!umiRedirect::getRedirectMessage($status)) {
				return false;
			}

			$madeByUser = (int) $madeByUser;
			$connection->query("START TRANSACTION /* Adding new redirect records */");
			$map = $this->getMap();

			try {
				$redirectsToSource = $this->get(
					[
						$map->get('TARGET_FIELD_NAME') => $source
					]
				);

				/** @var iUmiRedirect $redirectToSource */
				foreach ($redirectsToSource as $redirectToSource) {
					/** @var umiRedirect $redirect */
					$redirect = $this->create(
						[
							$map->get('SOURCE_FIELD_NAME') => $redirectToSource->getSource(),
							$map->get('TARGET_FIELD_NAME') => $target,
							$map->get('STATUS_FIELD_NAME') => $status,
							$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
						]
  					);

					if ($redirect->getSource() === $redirect->getTarget()) {
						$this->del($redirect->getId());
						$this->del($redirectToSource->getId());
					}
				}

				$this->delete(
					[
						$map->get('TARGET_FIELD_NAME') => $source
					]
				);

				$doublesExists = $this->isExists(
					[
						$map->get('TARGET_FIELD_NAME') => $target,
						$map->get('SOURCE_FIELD_NAME') => $source
					]
				);

				if ($doublesExists) {
					throw new Exception('Prevent doubles');
				}

				$this->create(
					[
						$map->get('TARGET_FIELD_NAME') => $target,
						$map->get('SOURCE_FIELD_NAME') => $source,
						$map->get('STATUS_FIELD_NAME') => $status,
						$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
					]
				);
			} catch (Exception $e) {
				$connection->query("ROLLBACK");
				return false;
			}

			$connection->query("COMMIT");
			return true;
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function getRedirectsIdBySource($source, $madeByUser = false) {
			$connection = $this->getConnection();
			$source = $connection->escape($this->parseUri($source));
			$madeByUser = (int) $madeByUser;
			$map = $this->getMap();
			$redirects = [];

			try {
				$result = $this->get(
					[
						$map->get('SOURCE_FIELD_NAME') => $source,
						$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
					]
				);
			} catch (Exception $e) {
				return $redirects;
			}

			/**
			 * @var iUmiRedirect|iUmiCollectionItem $redirect
			 */
			foreach ($result as $redirect) {
				$redirects[$redirect->getId()] = [
					$redirect->getSource(),
					$redirect->getTarget(),
					$redirect->getStatus()
				];
			}

			return $redirects;
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function getRedirectIdByTarget($target, $madeByUser = false) {
			$connection = $this->getConnection();
			$target = $connection->escape($this->parseUri($target));
			$madeByUser = (int) $madeByUser;
			$map = $this->getMap();
			$redirects = [];

			try {
				$result = $this->get(
					[
						$map->get('TARGET_FIELD_NAME') => $target,
						$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
					]
				);
			} catch (Exception $e) {
				return $redirects;
			}

			/**
			 * @var iUmiRedirect|iUmiCollectionItem $redirect
			 */
			foreach ($result as $redirect) {
				$redirects[$redirect->getId()] = [
					$redirect->getSource(),
					$redirect->getTarget(),
					$redirect->getStatus()
				];
			}

			return $redirects;
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function del($id) {
			$this->delete(
				[
					$this->getMap()->get('ID_FIELD_NAME') => $id
				]
			);

			return true;
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function redirectIfRequired($source, $madeByUser = false) {
			$connection = $this->getConnection();
			$source = $connection->escape($this->parseUri($source));
			$sourceCandidates = $this->slasherize($source);
			$map = $this->getMap();
			$madeByUser = (int) $madeByUser;

			try {
				$redirects = $this->get(
					[
						$map->get('SOURCE_FIELD_NAME') => $sourceCandidates,
						$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
					]
				);
			} catch (Exception $e) {
				return false;
			}

			if (count($redirects) > 0) {
				/** @var umiRedirect $redirect */
				$redirect = array_shift($redirects);
				$target = $this->parseUri($redirect->getTarget());
				$status = $redirect->getStatus();

				if (strpos($target, $this->getProtocol()) !== 0) {
					$target = '/' . $target;
				}

				if ($madeByUser || $this->isValidLink($target)) {
					$this->redirect($target, $status);
				}
			}

			$sourceParts = explode("/", trim($source, "/"));

			do {
				array_pop($sourceParts);
				$subSource = implode("/", $sourceParts) . "/";
				$subSource = $connection->escape($this->parseUri($subSource));

				if (!strlen($subSource)) {
					if (count($sourceParts) > 0)  {
						continue;
					}
					break;
				}

				$subSourceCandidates = $this->slasherize($subSource);

				try {
					$subRedirects = $this->get(
						[
							$map->get('SOURCE_FIELD_NAME') => $subSourceCandidates,
							$map->get('MADE_BY_USER_FIELD_NAME') => $madeByUser
						]
					);
				} catch (Exception $e) {
					return false;
				}

				if (count($subRedirects) > 0) {
					/** @var umiRedirect $subRedirect */
					$subRedirect = array_shift($subRedirects);
					$subSource = $subRedirect->getSource();
					$subTarget = $this->parseUri($subRedirect->getTarget());
					$subStatus = $subRedirect->getStatus();

					$sourceUriSuffix = substr($source, strlen($subSource));
					$subTarget .= $sourceUriSuffix;

					if (strpos($subTarget, $this->getProtocol()) !== 0) {
						$subTarget = '/' . $subTarget;
					}

					if ($this->isValidLink($subTarget)) {
						$this->redirect($subTarget, $subStatus);
					}
				}

			} while (count($sourceParts) > 1);

			return false;
		}

		/**
		 * {@inheritdoc}
		 */
		public function init() {
			$config = $this->getConfiguration();
			$map = $this->getMap();

			if ($config->get($map->get('CONFIG_SECTION'), $map->get('CONFIG_AUTO_CREATE_REDIRECT_ENABLE'))) {

				foreach ($this->getHandledEventsForAutoCreating() as $eventName) {
					new umiEventListener(
						$eventName,
						$map->get('AUTO_CREATE_REDIRECT_HANDLER_MODULE'),
						$map->get('AUTO_CREATE_REDIRECT_HANDLER_METHOD')
					);
				}

			}
		}

		/**
		 * {@inheritdoc}
		 * @throws Exception
		 */
		public function deleteAllRedirects() {
			$this->delete([]);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getCollectionItemClass() {
			return $this->collectionItemClass;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getTableName() {
			return $this->getMap()->get('TABLE_NAME');
		}

		/**
		 * Устанавливает протокол адреса
		 * @param string $protocol протокол
		 * @throws Exception
		 */
		public function setProtocol($protocol) {
			if (!is_string($protocol) || !in_array($protocol, ['https', 'http'])) {
				throw new Exception('Incorrect protocol given');
			}

			$this->urlProtocol = $protocol;
		}

		/**
		 * Возвращает протокол адреса
		 * @return string
		 */
		public function getProtocol() {
			return $this->urlProtocol;
		}

		/**
		 * Возвращает разобранный адрес
		 * @param string $uri адрес
		 * @return mixed|string
		 */
		protected function parseUri($uri) {
			$uri = ltrim($uri, '/');
			$urlSuffix = $this->getUrlSuffix();

			if ($urlSuffix == '/') {
				return rtrim($uri, '/');
			}

			$suffix = addcslashes($urlSuffix, '\^.$|()[]*+?{},');
			$pattern = '/(' . $suffix . ')/';
			$cleanUri = preg_replace($pattern, '', $uri);
			return (is_null($cleanUri))? $uri : $cleanUri;
		}

		/**
		 * Возвращает суффикс адреса
		 * @return string
		 * @throws Exception
		 */
		protected function getUrlSuffix() {
			if (!is_null($this->urlSuffix)) {
				return $this->urlSuffix;
			}

			$configuration = $this->getConfiguration();
			$map = $this->getMap();

			if ($configuration->get($map->get('CONFIG_SECTION'), $map->get('CONFIG_URL_SUFFIX_ENABLE'))) {
				return $this->urlSuffix = (string) $configuration->get($map->get('CONFIG_SECTION'), $map->get('CONFIG_URL_SUFFIX'));
			}

			return $this->urlSuffix = '/';
		}

		/**
		 * Производит редирект
		 * @param string $target куда перенаправлять
		 * @param int $status код статуса перенаправления
		 * @return bool
		 * @throws Exception
		 */
		protected function redirect($target, $status) {
			$statusMessage = umiRedirect::getRedirectMessage($status);

			if (!$statusMessage) {
				return false;
			}

			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = $this->getBuffer();

			if ($referrer = getServer('HTTP_REFERER')) {
				$buffer->setHeader('Referrer', (string) $referrer);
			}

			$buffer->redirect($target, $status . ' ' . $statusMessage, $status);
			$buffer->end();
		}

		/**
		 * Возвращает список имен событий изменения страниц
		 * @return array
		 */
		protected function getHandledEventsForAutoCreating() {
			return $this->autoCreateRedirectHandledEvents;
		}

		/**
		 * Можно ли делать редирект на ссылку?
		 * @param string $target ссылка
		 * @return bool
		 */
		protected function isValidLink($target) {
			if ($this->isExternalLink($target)) {
				return true;
			}

			if (preg_match('/^https?:\/\/[^\/]*\/(\S*)/', $target, $matches)) {
				$path = $matches[1];
			} else {
				$path = $target;
			}

			$expectNotActivePages = false;
			$errorsCount = 0;
			$domainId = domainsCollection::getInstance()
				->getDomainIdByUrl($target);
			$languageId = langsCollection::getInstance()
				->getLanguageIdByUrl($target);
			$targetPageId = umiHierarchy::getInstance()
				->getIdByPath($path, $expectNotActivePages, $errorsCount, $domainId, $languageId);

			return ($targetPageId !== false);
		}

		/**
		 * Является ли ссылка внешней?
		 * @param string $target ссылка
		 * @return bool
		 */
		protected function isExternalLink($target) {
			$domainCollection = domainsCollection::getInstance();
			return ($domainCollection->getDomainIdByUrl($target) === false);
		}

		/**
		 * Возвращает возможные комбинации написания источника редиректа
		 * @param string $source источник редиректа
		 * @return array
		 */
		private function slasherize($source) {
			return [
				$source,
				$source . '/',
				'/' . $source,
				'/' . $source . '/',
			];
		}

	}
