<?php
interface iUmiMessages {
	public function create();

	public function getMessages($recipientId = false, $onlyNew = false);
	public function getSendedMessages($senderId = false);

	static public function getAllowedTypes();
}