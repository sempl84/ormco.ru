<?php

	/**
	 * Контроллер приложения UMI.CMS.
	 */
	class cmsController extends singleton implements iSingleton, iCmsController {

		public static $IGNORE_MICROCACHE = false;

		public $isContentMode = false;

		public $parsedContent = false;
		public $currentTitle = false;
		public $currentHeader = false;
		public $currentMetaKeywords = false;
		public $currentMetaDescription = false;
		public $currentEditElementId = false;

		public $langs = [];
		public $pre_lang;
		public $langs_export = [];
		public $errorUrl;
		public $headerLabel = false;

		/**
		 * @var lang|\false $current_lang
		 */
		protected $current_lang = false;
		/**
		 * @var domain|\false $current_domain
		 */
		protected $current_domain = false;
		/**
		 * @var templater|\false $current_templater
		 */
		protected $current_templater = false;

		protected $modules = [];
		protected $current_module = false;
		protected $current_method = false;
		protected $current_mode;
		protected $current_element_id = false;
		protected $calculated_referer_uri = false;
		protected $modulesPath;
		protected $url_prefix = '';
		protected $adminDataSet = [];

		/**
		 * @static
		 * @var array Методы, вызов которых надо игнорировать в режиме XSLT. Предназначено для избавления от заплатки
		 *     $this->breakMe
		 */
		private $skipExecuteMethods = [
				'eshop/compare',
				'faq/question', 'faq/project', 'faq/category',
				'blogs20/blog', 'blogs20/post', 'blogs20/postEdit',
				'catalog/category', 'catalog/getObjectsList', 'catalog/object', 'catalog/viewObject', 'catalog/search',
				'content/content', 'content/sitemap',
				'dispatches/unsubscribe', 'dispatches/subscribe', 'dispatches/subscribe_do',
				'emarket/compare', 'emarket/order', 'emarket/purchase',
				'filemanager/shared_file',
				'forum/confs_list', 'forum/conf', 'forum/topic', 'forum/topic_last_message', 'forum/conf_last_message',
				'news/lastlist', 'news/rubric', 'news/view', 'news/related_links', 'news/item', 'news/listlents', 'news/lastlents',
				'photoalbum/album', 'photoalbum/photo',
				'search/search_do', 'search/suggestions',
				'users/settings', 'users/registrate', 'users/registrate_done', 'users/activate', 'users/auth',
				'vote/poll', 'vote/insertvote', 'vote/results',
				'webforms/page', 'webforms/posted'
		];

		/**
		 * {@inheritdoc}
		 */
		protected function __construct() {
			$config = mainConfiguration::getInstance();
			$this->modulesPath = $config->includeParam('system.modules') . getCompatibleModulesPath();
			$this->init();
		}

		/**
		 *
		 * @param string|null $c
		 * @return cmsController
		 */
		public static function getInstance($c = null) {
			return parent::getInstance(__CLASS__);
		}

		public function loadBuildInModule($moduleName) {
			//TODO
		}

		/**
		 * {@inheritdoc}
		 */
		public function isCurrentModeAdmin() {
			return ($this->getCurrentMode() == self::ADMIN_MODE_ID);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getAdminModeId() {
			return self::ADMIN_MODE_ID;
		}

		/**
		 * Возвращает объект главного класса модуля по его имени
		 * @param string $moduleName имя модуля
		 * @param bool $resetCache не использовать кеш, по-умолчанию отключен
		 * @return def_module|false
		 */
		public function getModule($moduleName, $resetCache = false) {
			if (!$moduleName) {
				return false;
			}

			if (array_key_exists($moduleName, $this->modules) && !$resetCache) {
				return $this->modules[$moduleName];
			} else {
				return $this->loadModule($moduleName);
			}
		}

		public function isModule($moduleName) {
			$regedit = regedit::getInstance();

			if ($regedit->getVal('//modules/' . $moduleName)) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Устанавливает модуль.
		 * @param string $installPath установочный файл
		 * @throws publicAdminException если установочный файл не существует
		 */
		public function installModule($installPath) {
			$INFO = [];
			$COMPONENTS = [];

			if (!file_exists($installPath)) {
				throw new publicAdminException(getLabel("label-errors-13052"), 13052);
			}

			/** @noinspection PhpIncludeInspection */
			require_once $installPath;

			$nameByPath = null;

			if (preg_match('|classes\/modules\/(\S+)\/|i', $installPath, $matches)) {
				$nameByPath = $matches[1];
			}

			if (is_null($nameByPath) && preg_match('|classes\/components\/(\S+)\/|i', $installPath, $matches)) {
				$nameByPath = $matches[1];
			}

			if ($nameByPath != $INFO['name']) {
				throw new publicAdminException(getLabel("label-errors-13053"), 13053);
			}

			$this->checkModuleByName($nameByPath);
			$this->checkModuleComponents($COMPONENTS);

			def_module::install($INFO);
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getCurrentModule() {
			return $this->current_module;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setCurrentModule($moduleName) {
			$this->current_module = (string) $moduleName;
			return $this;
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getCurrentMethod() {
			return $this->current_method;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setCurrentMethod($methodName) {
			$magic = [
				"__construct",
				"__destruct",
				"__call",
				"__callStatic",
				"__get",
				"__set",
				"__isset",
				"__unset",
				"__sleep",
				"__wakeup",
				"__toString",
				"__invoke",
				"__set_state",
				"__clone"
			];

			if (in_array($methodName, $magic)) {
				$this->setCurrentModule("content");
				$methodName = "notfound";
			}

			$this->current_method = (string) $methodName;
			return $this;
		}

		/**
		 * Возвращает имя текущего иерархического элемента.
		 * @return string
		 */
		public function getCurrentElementId() {
			return $this->current_element_id;
		}

		/**
		 * @inheritdoc
		 * @return iLang
		 * @throws coreException
		 */
		public function getCurrentLang() {
			if (!$this->current_lang instanceof iLang) {
				$this->detectLang();
			}

			return $this->current_lang;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setCurrentLang(iLang $lang) {
			$this->current_lang = $lang;
			cacheFrontend::$currentlangId = $lang->getId();
			$this->detectPreLang();
			return $this;
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getCurrentMode() {
			if ($this->current_mode === null) {
				$this->detectMode();
			}

			return $this->current_mode;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setCurrentMode($mode) {
			$this->current_mode =  (string) $mode;
			return $this;
		}

		/**
		 * @inheritdoc
		 * @return iDomain
		 * @throws coreException
		 */
		public function getCurrentDomain() {
			if (!$this->current_domain instanceof iDomain) {
				$this->detectDomain();
			}

			return $this->current_domain;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setCurrentDomain(iDomain $domain) {
			$this->current_domain = $domain;
			cacheFrontend::$currentDomainId = $domain->getId();
			$this->detectPreLang();
			return $this;
		}

		/**
		 * Возвращает текущий шаблонизатор.
		 * @throws coreException
		 * @return umiTemplater
		 */
		public function getCurrentTemplater($forceRefresh = false) {
			if (!$this->current_templater instanceof umiTemplater || $forceRefresh) {
				$this->detectCurrentTemplater();
			}

			if (!$this->current_templater instanceof umiTemplater) {
				throw new coreException("Can't detect current templater.");
			}

			return $this->current_templater;
		}

		/**
		 * Получить директорию с ресурсами для текущего шаблона.
		 * @param bool $httpMode
		 * @return string
		 */
		public function getResourcesDirectory($httpMode = false) {
			if ($this->getCurrentMode() == 'admin') {
				if (($template = templatesCollection::getInstance()->getDefaultTemplate()) instanceof template) {
					return $template->getResourcesDirectory($httpMode);
				}

				return false;
			}

			if (($template = $this->detectCurrentDesignTemplate()) instanceof template) {
				return $template->getResourcesDirectory($httpMode);
			} else {
				return false;
			}
		}

		/**
		 * Получить текущую директорию с шаблонами
		 * @return string
		 */
		public function getTemplatesDirectory() {
			if (($template = $this->detectCurrentDesignTemplate()) instanceof template) {
				return $template->getTemplatesDirectory();
			} else {
				return CURRENT_WORKING_DIR . "xsltTpls/";
			}
		}

		/**
		 * @deprecated
		 * Оставлено для совместимости, ни на что не влияет
		 * @return umiTemplater
		 */
		public function setCurrentTemplater() {
			return $this->getCurrentTemplater();
		}

		/**
		 * Возвращает глобальные переменные в зависимости от
		 * текущего состояния системы
		 * @param bool $forcePrepare - если true, переменные будут еще раз инициализированы
		 * @return array
		 */
		public function getGlobalVariables($forcePrepare = false) {
			static $globalVariables;

			if (!$forcePrepare && !is_null($globalVariables)) {
				return $globalVariables;
			}

			$globalVariables = [];

			switch (true) {
				case ($this->getCurrentMode() == 'admin') : {
					$globalVariables = $this->prepareAdminSideGlobalVariables();
					break;
				}
				case (def_module::isXSLTResultMode()) : {
					$globalVariables = $this->prepareClientSideGlobalVariablesForXSLT();
					break;
				}
				default: {
					$globalVariables = $this->prepareClientSideGlobalVariablesForTPL();
				}
			}

			$eventPoint = new umiEventPoint('globalVariablesCollected');
			$eventPoint->setMode('after');
			$eventPoint->addRef('variables', $globalVariables);
			$eventPoint->call();

			return $globalVariables;
		}

		/**
		 * Запускает umi-stream, возвращает результат работы
		 * @param string $uri Адрес потока в формате "usomestream://id?params"
		 * @throws coreException Если не удалось открыть поток
		 * @return string $data результат работы потока
		 */
		public function executeStream($uri) {
			if (($data = @file_get_contents($uri)) === false) {
				// bugfix: failed to open stream: infinite recursion prevented
				$uri .= (strpos($uri, "?") === false) ? '?umiHash=' : '&umiHash=';
				$uri .= md5($uri);

				if (($data = @file_get_contents($uri)) === false) {
					preg_match("/(\w+:\/\/)/i", $uri, $matches);
					$stream = $matches[1];

					throw new coreException("Failed to open $stream stream");
				}
			}

			return $data;
		}

		/**
		 * Предназначен для избавления от заплатки $this->breakMe
		 * Возвращает false, если метод вызывать не нужно
		 *
		 * @param string $module
		 * @param string $method
		 *
		 * @return boolean
		 */
		public function isAllowedExecuteMethod($module, $method) {
			return !in_array($module . '/' . $method, $this->skipExecuteMethods);
		}

		/**
		 * Определяет текущий шаблон дизайна
		 * @return null|template - текущий шаблон дизайна, либо null
		 */
		public function detectCurrentDesignTemplate() {
			$templates = templatesCollection::getInstance();

			// пытаемся взять шаблон из request
			$template = null;
			if ($templateId = getRequest('template_id')) {
				$template = $templates->getTemplate((int) $templateId);
			}
			// пытаемся взять текущий шаблон страницы
			if (!$template instanceof template) {
				$template = $templates->getCurrentTemplate();
			}

			return $template;
		}

		public function analyzePath($reset = false) {
			if (isset($_SERVER['REQUEST_URI']) && !$this->isCorrectRequestUri($_SERVER['REQUEST_URI'])) {
				$this->current_element_id = false;
				$this->setCurrentModule("content");
				$this->setCurrentMethod("content");
				return;
			}

			$path = trim(getRequest('path'), '/');

			if (!is_null(getRequest('scheme'))) {
				if (preg_replace('/[^\w]/im', "", getRequest('scheme')) == 'upage') {
					preg_match_all('/[\d]+/', $path, $elementId);
					$this->current_element_id = $elementId[0][0];
				}

				return;
			}

			$regedit = regedit::getInstance();
			$hierarchy = umiHierarchy::getInstance();
			$config = mainConfiguration::getInstance();
			/** @var HTTPOutputBuffer $buffer */
			$buffer = outputBuffer::current();
			$redirects = UmiCms\Service::Redirects();

			if ($reset === true) {
				$this->reset();
			}

			$urlSuffix = $config->get('seo', 'url-suffix');
			$pos = strrpos($path, $urlSuffix);
			if ($pos && ($pos + strlen($urlSuffix) == strlen($path))) {
				$path = substr($path, 0, $pos);
			}

			if ($config->get('seo', 'url-suffix.add')) {
				def_module::requireSlashEnding();
			}

			$redirects->redirectIfRequired(getServer('REQUEST_URI'), true);

			if ($config->get('seo', 'watch-redirects-history')) {
				$redirects->init();
			}

			$pathArray = $this->getPathArray();
			$sz = sizeof($pathArray);
			$urlArray = [];
			$p = 0;
			for ($i = 0; $i < $sz; $i++) {
				$subPath = $pathArray[$i];

				if ($i <= 1) {
					if (($subPath == $this->getCurrentMode()) || ($subPath == $this->getCurrentLang()->getPrefix())) {
						continue;
					}
				}

				$urlArray[] = $subPath;

				$subPathType = $this->getSubPathType($subPath);

				if ($subPathType == "PARAM") {
					$_REQUEST['param' . $p++] = $subPath;
				}
			}

			if (!$this->getCurrentModule()) {
				if ($this->getCurrentMode() == "admin") {
					if ($regedit->getVal("//settings/default_module_admin_changed") || !$moduleName = $regedit->getVal("//modules/events")) {
						$moduleName = $regedit->getVal("//settings/default_module_admin");
					} else {
						$moduleName = $regedit->getVal("//modules/events");
					}
					$this->autoRedirectToMethod($moduleName);
				} else {
					$moduleName = $regedit->getVal("//settings/default_module");
				}
				$this->setCurrentModule($moduleName);
			}

			if (!$this->getCurrentMethod()) {
				if ($this->getCurrentMode() == "admin") {
					return $this->autoRedirectToMethod($this->getCurrentModule());
				} else {
					$method_name = $regedit->getVal("//modules/" . $this->getCurrentModule() . "/default_method");
				}
				$this->setCurrentMethod($method_name);
			}

			if ($this->getCurrentMode() == "admin") {
				return;
			}

			$elementId = false;
			$sz = sizeof($urlArray);
			$subPath = "";
			$errorsCount = 0;

			for ($i = 0; $i < $sz; $i++) {
				$subPath .= "/" . $urlArray[$i];

				if (!($tmp = $hierarchy->getIdByPath($subPath, false, $errorsCount))) {
					$elementId = false;
					break;
				} else {
					$elementId = $tmp;
				}
			}

			if ($elementId) {
				if ($errorsCount > 0 && !defined("DISABLE_AUTOCORRECTION_REDIRECT")) {
					$path = $hierarchy->getPathById($elementId);

					if ($i == 0) {
						if ($this->isModule($urlArray[0])) {
							$elementId = false;
						}
					}

					$buffer->status('301 Moved Permanently');
					$buffer->redirect($path);
				}

				$element = $hierarchy->getElement($elementId);
				if ($element instanceof umiHierarchyElement) {
					if ($element->getIsDefault()) {
						$path = $hierarchy->getPathById($elementId);
						$buffer->status('301 Moved Permanently');
						$buffer->redirect($path);
					}
				}
			} elseif (isset($urlArray[0])) {
				if ($this->isModule($urlArray[0])) {
					$module = $this->getModule($urlArray[0]);
					if (isset($urlArray[1]) && !$module->isMethodExists($urlArray[1])) {
						$this->setCurrentModule('content');
						$this->setCurrentMethod('content');
					}
				} else {
					$this->setCurrentModule('content');
					$this->setCurrentMethod('content');
				}
			}

			if (($path == "" || $path == $this->getCurrentLang()->getPrefix()) && $this->getCurrentMode() != "admin") {
				if ($elementId = $hierarchy->getDefaultElementId($this->getCurrentLang()->getId(),
						$this->getCurrentDomain()->getId())
				) {
					$this->current_element_id = $elementId;
				}
			}

			if ($element = $hierarchy->getElement($elementId, true)) {
				$type = umiHierarchyTypesCollection::getInstance()->getType($element->getTypeId());

				if (!$type) {
					return false;
				}

				$this->setCurrentModule($type->getName());

				if ($ext = $type->getExt()) {
					$this->setCurrentMethod($ext);
				} else {
					$this->setCurrentMethod("content");  //Fixme: content "constructor". Maybe, fix in future?
				}

				$this->current_element_id = $elementId;
			}

			if ($this->getCurrentModule() == "content" && $this->getCurrentMethod() == "content" && !$elementId) {
				$redirects->redirectIfRequired($path);
			}
		}

		/**
		 * Корректно ли значение серверной переменной REQUEST_URI
		 * @param string $requestUri значение серверной переменной REQUEST_URI
		 * @return bool
		 */
		private function isCorrectRequestUri($requestUri) {
			return !preg_match('/(^[^\?]*\/&)/', $requestUri);
		}

		public function setAdminDataSet($dataSet) {
			$this->adminDataSet = $dataSet;
		}

		public function loadLangs() {
			$modules = regedit::getInstance()->getList("//modules");
			$this->langs = [];

			foreach ($modules as $module) {
				$moduleName = $module[0];

				$langPath = SYS_MODULES_PATH . $moduleName . '/lang.php';

				if (file_exists($langPath)) {
					/** @noinspection PhpIncludeInspection */
					require $langPath;
				}

				if (isset($C_LANG) && is_array($C_LANG)) {
					$this->langs[$moduleName] = $C_LANG;
					unset($C_LANG);
				}

				if (isset($LANG_EXPORT) && is_array($LANG_EXPORT)) {
					foreach ($LANG_EXPORT as $key => $value) {
						$this->langs[$key] = $value;
					}
					unset($LANG_EXPORT);
				}

				$langPath = SYS_MODULES_PATH . $moduleName . '/lang.' . $this->getCurrentLang()->getPrefix() . '.php';

				if (file_exists($langPath)) {
					/** @noinspection PhpIncludeInspection */
					require $langPath;

					if (isset($C_LANG) && is_array($C_LANG)) {
						$this->langs[$moduleName] = $C_LANG;
						unset($C_LANG);
					}

					if (isset($LANG_EXPORT) && is_array($LANG_EXPORT)) {
						foreach ($LANG_EXPORT as $key => $value) {
							$this->langs[$key] = $value;
						}
						unset($LANG_EXPORT);
					}
				}

				$this->loadExtLang($moduleName);
			}

			// загружаем общие языковые файлы
			$tryPath = SYS_MODULES_PATH . 'lang.' . $this->getCurrentLang()->getPrefix() . '.php';

			if (!file_exists($tryPath)) {
				$tryPath = SYS_MODULES_PATH . 'lang.php';
			}
			/** @noinspection PhpIncludeInspection */
			include_once $tryPath;

			if (isset($LANG_EXPORT)) {
				$this->langs = array_merge($this->langs, $LANG_EXPORT);
				unset($LANG_EXPORT);
			}
		}

		/**
		 * Подключает языковой файл из расширения.
		 * @param string $moduleName название модуля
		 */
		public function loadExtLang($moduleName) {
			$langPath = SYS_MODULES_PATH . $moduleName . '/ext/lang.*.' . $this->getCurrentLang()->getPrefix() . '.php';

			$langPaths = glob($langPath);

			if (!is_array($langPaths)) {
				return;
			}

			foreach ($langPaths as $filename) {
				if (file_exists($filename)) {
					/** @noinspection PhpIncludeInspection */
					require $filename;

					if (isset($C_LANG) && is_array($C_LANG)) {
						foreach ($C_LANG as $key => $value) {
							$this->langs[$moduleName][$key] = $value;
						}
						unset($C_LANG);
					}

					if (isset($LANG_EXPORT) && is_array($LANG_EXPORT)) {
						foreach ($LANG_EXPORT as $key => $value) {
							$this->langs[$key] = $value;
						}
						unset($LANG_EXPORT);
					}
				}
			}
		}

		public function getModulesList() {
			$regedit = regedit::getInstance();
			$list = $regedit->getList('//modules');
			$result = [];
			foreach ($list as $arr) {
				$result[] = getArrayKey($arr, 0);
			}

			return $result;
		}

		public function getRequestId() {
			static $requestId = false;
			if ($requestId === false) {
				$requestId = time();
			}

			return $requestId;
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getPreLang() {
			if ($this->pre_lang === null) {
				$this->detectPreLang();
			}

			return $this->pre_lang;
		}

		/**
		 * @inheritdoc
		 * @return iCmsController
		 */
		public function setPreLang($prefix) {
			$this->pre_lang = (string) $prefix;
			return $this;
		}

		public function calculateRefererUri() {
			$session = \UmiCms\Service::Session();

			if ($referrer = getRequest('referer')) {
				$session->set('referer', $referrer);
			} else {
				if ($referrer = $session->get('referer')) {
					$session->del('referer');
				} else {
					$referrer = getServer('HTTP_REFERER');
				}
			}

			$this->calculated_referer_uri = $referrer;
		}

		public function getCalculatedRefererUri() {
			if ($this->calculated_referer_uri === false) {
				$this->calculateRefererUri();
			}

			return $this->calculated_referer_uri;
		}

		public function setUrlPrefix($prefix = '') {
			$this->url_prefix = $prefix;
		}

		public function getUrlPrefix() {
			return $this->url_prefix ? $this->url_prefix : '';
		}

		protected function autoRedirectToMethod($module) {
			$preLang = $this->getPreLang();
			$method = regedit::getInstance()->getVal("//modules/" . $module . "/default_method_admin");

			$url = $preLang . "/admin/" . $module . "/" . $method . "/";

			outputBuffer::current()->redirect($url);
		}

		final private function doSomething() {
			if (isDemoMode() || isCronCliMode()) {
				return true;
			}

			try {
				$buffer = outputBuffer::current();
			} catch (coreException $e) {
				$buffer = outputBuffer::current('HTTPOutputBuffer');
			}

			if (defined("CURRENT_VERSION_LINE") && !isDemoMode()) {
				if ($buffer instanceof HTTPOutputBuffer) {
					$buffer->status(500);
				}

				$buffer->push(file_get_contents(CURRENT_WORKING_DIR . "/errors/invalid_license.html"));
				$buffer->end();
			}

			if (!is_writable(SYS_CACHE_RUNTIME) || (file_exists(SYS_CACHE_RUNTIME . 'registry') && !is_writable(SYS_CACHE_RUNTIME . 'registry'))) {
				if ($buffer instanceof HTTPOutputBuffer) {
					$buffer->status(500);
				}

				$buffer->push(file_get_contents(CURRENT_WORKING_DIR . "/errors/invalid_permissions.html"));
				$buffer->end();
			}

			$keycode = regedit::getInstance()->getVal("//settings/keycode");

			if ($this->doStrangeThings($keycode)) {
				return true;
			}

			$compKeycode = [
					'pro'      => umiTemplater::getSomething("pro"),
					'ultimate' => umiTemplater::getSomething("ultimate"),
					'shop'     => umiTemplater::getSomething("shop"),
					'lite'     => umiTemplater::getSomething("lite"),
					'start'    => umiTemplater::getSomething("start"),
					'trial'    => umiTemplater::getSomething("trial")
			];

			if (regedit::checkSomething($keycode, $compKeycode)) {
				return true;
			} else {
				if ($buffer instanceof HTTPOutputBuffer) {
					$buffer->status(500);
				}

				$buffer->push(file_get_contents(CURRENT_WORKING_DIR . "/errors/invalid_license.html"));
				$buffer->end();
			}
		}

		final private function doStrangeThings($keycode) {
			if (isDemoMode()) {
				return true;
			}

			$licenseFile = SYS_CACHE_RUNTIME . 'trash';
			$cmpKeycode = false;
			$expire = 604800;

			if (file_exists($licenseFile)) {
				if ((time() - filemtime($licenseFile)) > $expire) {
					$cmpKeycode = base64_decode(file_get_contents($licenseFile));
				}
			} else {
				file_put_contents($licenseFile, base64_encode($keycode));
			}

			if ($cmpKeycode !== false && $keycode) {
				if ($keycode === $cmpKeycode) {
					return true;
				}
			}

			return false;
		}

		private function loadModule($moduleName) {
			if (!defined("CURRENT_VERSION_LINE")) {
				define("CURRENT_VERSION_LINE", "");
			}

			if (regedit::getInstance()->getVal("//modules/" . $moduleName) != $moduleName) {
				return false;
			}

			$modulePath = $this->modulesPath . $moduleName . DIRECTORY_SEPARATOR . 'class.php';

			if (!file_exists($modulePath)) {
				return false;
			}

			if (!class_exists($moduleName)) {
				/** @noinspection PhpIncludeInspection */
				require($modulePath);
			}

			$module = new $moduleName();
			$module->pre_lang = $this->getPreLang();
			$module->pid = $this->getCurrentElementId();
			$this->modules[$moduleName] = $module;

			return $module;
		}

		/**
		 * Проверка наличия всех компонентов модуля
		 *
		 * @param $components
		 * @throws coreException
		 * @return bool
		 */
		private function checkModuleComponents($components) {
			if (!is_array($components)) {
				return false;
			}

			$files = [];
			foreach ($components as $component) {
				$file = preg_replace('/.\/(.+)/', CURRENT_WORKING_DIR . '/' . "$1", $component);
				if (!file_exists($file) || !is_readable($file)) {
					$files[] = $file;
				}
			}

			if (count($files)) {
				$error = getLabel("label-errors-13058") . "\n";
				foreach ($files as $file) {
					$error .= getLabel('error-file-does-not-exist', null, $file) . "\n";
				}

				throw new coreException($error);
			}

			return true;
		}

		/**
		 * Проверяет, что модуль доступен для данной лицензии.
		 * @param string $moduleName - имя модуля
		 * @throws publicAdminException
		 */
		private function checkModuleByName($moduleName) {
		    return true;
		    
			if (!defined("UPDATE_SERVER")) {
				define("UPDATE_SERVER", base64_decode('aHR0cDovL3VwZGF0ZXMudW1pLWNtcy5ydS91cGRhdGVzZXJ2ZXIv'));
			}

			$regedit = regedit::getInstance();
			$domainsCollection = domainsCollection::getInstance();

			$info = [
					'type'     => 'get-modules-list',
					'revision' => $regedit->getVal("//modules/autoupdate/system_build"),
					'host'     => $domainsCollection->getDefaultDomain()->getHost(),
					'ip'       => getServer('SERVER_ADDR'),
					'key'      => $regedit->getVal("//settings/keycode")
			];
			$url = UPDATE_SERVER . "?" . http_build_query($info, '', '&');

			$result = $this->getFile($url);

			if (!$result) {
				throw new publicAdminException(getLabel("label-errors-13054"), 13054);
			}

			$xml = new DOMDocument();
			if (!$xml->loadXML($result)) {
				throw new publicAdminException(getLabel("label-errors-13055"), 13055);
			}

			$xpath = new DOMXPath($xml);

			// Проверяем, возможно сервер возвратил ошибку.
			$errors = $xpath->query('error');

			if ($errors->length != 0) {
				/** @var DomElement $error */
				$error = $errors->item(0);
				$code = $error->getAttribute("code");
				throw new publicAdminException(getLabel("label-errors-" . $code), $code);
			}

			$modules = $xpath->query("module");
			if ($modules->length == 0) {
				throw new publicAdminException(getLabel("label-errors-13056"), 13056);
			}

			$moduleName = strtolower($moduleName);

			$modules = $xpath->query("module[@name='" . $moduleName . "']");
			if ($modules->length != 0) {
				/** @var DomElement $module */
				$module = $modules->item(0);

				if ($module->getAttribute("active") != "1") {
					throw new publicAdminException(getLabel("label-errors-13057"), 13057);
				}
			}
		}

		/**
		 * Выполняет запрос к серверу обновлений
		 *
		 * @param mixed $url - сформированная строка запроса
		 * @throws publicAdminException
		 * @return string;
		 */
		private function getFile($url) {
			try {
				return umiRemoteFileGetter::get($url);
			} catch (Exception $e) {
				throw new publicAdminException(getLabel("label-errors-13041"), 13041);
			}
		}

		/**
		 * Является ли запрошенная связка модуль/метод шлюзом?
		 * @return bool
		 */
		private function isGateway() {
			$gateways = [
				['exchange', 'auto'],
				['exchange', 'export1C'],
				['users', 'login_do']
			];

			$customGateways = mainConfiguration::getInstance()->get('system', 'gateways');

			if (is_array($customGateways)) {
				foreach ($customGateways as $pair) {
					$gateways[] = explode('-', $pair);
				}
			}

			$module = $this->getCurrentModule();
			$method = $this->getCurrentMethod();

			return (array_search([$module, $method], $gateways) !== false);
		}

		/**
		 * Подготавливает и возвращает глобальные переменные в режиме работы со стороны админки
		 * @throws Exception
		 * @return array
		 */
		private function prepareAdminSideGlobalVariables() {
			if (!$this->isGateway() && strtoupper($_SERVER['REQUEST_METHOD']) == "POST") {
				$security = \UmiCms\System\Protection\Security::getInstance();
				$security->checkReferrer();
				$security->checkCsrf();
			}

			$permissions = permissionsCollection::getInstance();
			$domains = domainsCollection::getInstance();
			$regedit = regedit::getInstance();
			$session = \UmiCms\Service::Session();
			$sessionLifeTime = $session->getMaxActiveTime();

			$result = [
					'@module'           => $this->getCurrentModule(),
					'@method'           => $this->getCurrentMethod(),
					'@lang'             => $this->getCurrentLang()->getPrefix(),
					'@lang-id'          => $this->getCurrentLang()->getId(),
					'@pre-lang'         => $this->getPreLang(),
					'@domain'           => $this->getCurrentDomain()->getHost(),
					'@domain-id'        => $this->getCurrentDomain()->getId(),
					'@session-lifetime' => $sessionLifeTime,
					'@system-build'     => $regedit->getVal("//modules/autoupdate/system_build"),
					'@referer-uri'      => $this->getCalculatedRefererUri(),
					'@user-id'          => UmiCms\Service::Auth()->getUserId(),
					'@interface-lang'   => ulangStream::getLangPrefix(),
					'@csrf'             => $session->get('csrf_token')
			];

			if (isDemoMode()) {
				$result['@demo'] = 1;
			}

			if ($requestUri = getServer('REQUEST_URI')) {
				$requestUriInfo = parse_url($requestUri);
				$requestUri = getArrayKey($requestUriInfo, 'path');
				$queryParams = getArrayKey($requestUriInfo, 'query');
				if ($queryParams) {
					parse_str($queryParams, $queryParamsArr);
					if (isset($queryParamsArr['p'])) {
						unset($queryParamsArr['p']);
					}
					if (isset($queryParamsArr['xmlMode'])) {
						unset($queryParamsArr['xmlMode']);
					}

					$queryParams = http_build_query($queryParamsArr, '', '&');
					if ($queryParams) {
						$requestUri .= '?' . $queryParams;
					}
				}
				$result['@request-uri'] = $requestUri;
			}

			$result['@edition'] = (defined('CURRENT_VERSION_LINE')) ? CURRENT_VERSION_LINE : '';
			$result['@disableTooManyChildsNotification'] = (int) mainConfiguration::getInstance()->get('system',
					'disable-too-many-childs-notification');

			$isUserAdmin = $permissions->isAdmin();

			switch (true) {
				case ($isUserAdmin && $permissions->isAllowedDomain($result['@user-id'], $result['@domain-id']) == 0): {
					$result['data'] = new requreMoreAdminPermissionsException(getLabel("error-require-more-permissions"));
					/** @var HTTPOutputBuffer $buffer */
					$buffer = outputBuffer::current('HTTPOutputBuffer');
					$buffer->status('403 Forbidden');
					break;
				}
				case system_is_allowed($this->getCurrentModule(), $this->getCurrentMethod()): {
					try {
						if ($module = $this->getModule($this->getCurrentModule())) {
							$module->cms_callMethod($this->getCurrentMethod(), null);
						}

						$result['data'] = $this->adminDataSet;
					} catch (publicException $e) {
						$result['data'] = $e;
					}
					break;
				}
				case $isUserAdmin: {
					$result['data'] = new requreMoreAdminPermissionsException(getLabel("error-require-more-permissions"));
					/** @var HTTPOutputBuffer $buffer */
					$buffer = outputBuffer::current('HTTPOutputBuffer');
					$buffer->status('403 Forbidden');
					break;
				}
				case ($this->getCurrentModule() != 'events' && $this->getCurrentMethod() != 'last'): {
					/** @var HTTPOutputBuffer $buffer */
					$buffer = outputBuffer::current('HTTPOutputBuffer');
					$buffer->status('403 Forbidden');
					break;
				}
			}

			if (!is_null($domainFloated = getRequest('domain'))) {
				$result['@domain-floated'] = $domainFloated;
				$result['@domain-floated-id'] = $domains->getDomainId($domainFloated);
			} else {
				if ($this->currentEditElementId) {
					$element = umiHierarchy::getInstance()->getElement($this->currentEditElementId);
					if ($element instanceof umiHierarchyElement) {
						$domain = $domains->getDomain($element->getDomainId());

						if ($domain instanceof domain) {
							$result['@domain-floated'] = $domainFloated = $domain->getHost();
						}
					}
				} else {
					$result['@domain-floated'] = $result['@domain'];
				}
			}

			return $result;
		}

		/**
		 * Подготавливает и возвращает глобальные переменные в режиме работы со стороны сайта для режима TPL.
		 * @throws coreException если нет модуля пользователи
		 * @return array
		 */
		private function prepareClientSideGlobalVariablesForTPL() {
			$permissions = permissionsCollection::getInstance();

			$currentModule = $this->getCurrentModule();
			$currentMethod = $this->getCurrentMethod();
			$elementId = $this->getCurrentElementId();
			$userId = UmiCms\Service::Auth()->getUserId();

			// check permissions
			$notPermitted = true;
			if ($permissions->isAllowedMethod($userId, $currentModule, $currentMethod)) {
				$notPermitted = false;
				if ($elementId) {
					list($r) = $permissions->isAllowedObject($userId, $elementId);
					$notPermitted = !$r;
				}
			}
			// если нет прав на текущую страницу либо на доступ к текущему методу
			if ($notPermitted) {
				/** @var HTTPOutputBuffer $buffer */
				$buffer = outputBuffer::current('HTTPOutputBuffer');
				$buffer->status('401 Unauthorized');
				$this->setCurrentModule('users');
				$this->setCurrentMethod('login');

				/** @var users $moduleUsers */
				if (!$moduleUsers = $this->getModule('users')) {
					throw new coreException('Module "users" not found.');
				}

				return ['content' => $moduleUsers->login()];
			}

			$module = $this->getModule($currentModule);
			try {
				$content = $module->cms_callMethod($currentMethod, []);
			} catch (publicException $e) {
				$content = $e->getMessage();
			}

			return ['content' => $content];
		}

		/**
		 * Подготавливает и возвращает глобальные переменные в режиме работы со стороны сайта для режима XSLT.
		 * @throws coreException
		 * @return array
		 */
		private function prepareClientSideGlobalVariablesForXSLT() {
			if ( $this->useOnlyBaseClientVariables() ) {
				return $this->getBaseClientVariables();
			}

			$globalVariables = [];

			$permissions = permissionsCollection::getInstance();
			$objectsCollection = umiObjectsCollection::getInstance();
			$hierarchy = umiHierarchy::getInstance();

			$auth = UmiCms\Service::Auth();
			$userId = $auth->getUserId();
			$elementId = $this->getCurrentElementId();

			$currentModule = $this->getCurrentModule();
			$currentMethod = $this->getCurrentMethod();

			// check permissions
			$notPermitted = true;
			if ($permissions->isAllowedMethod($userId, $currentModule, $currentMethod)) {
				$notPermitted = false;
				if ($elementId) {
					list($r) = $permissions->isAllowedObject($userId, $elementId);
					if (!$r) {
						$notPermitted = true;
						$globalVariables['attribute:not-permitted'] = 1;
					}
				}
			}

			if ($notPermitted) {
				$currentModule = "users";
				$currentMethod = "login";

				$this->setCurrentModule($currentModule);
				$this->setCurrentMethod($currentMethod);
			}

			$this->currentHeader = def_module::parseTPLMacroses(macros_header());

			$globalVariables = array_merge( $globalVariables, $this->getBaseClientVariables() );

			if (!is_null(getRequest('p'))) {
				$globalVariables['@paging'] = "yes";
			}

			/** @var social_networks $socialModule */
			$socialModule = cmsController::getInstance()->getModule("social_networks");

			if ($socialModule && ($currentSocial = $socialModule->getCurrentSocial())) {
				$globalVariables['@socialId'] = $currentSocial->getId();
				$_REQUEST['template_id'] = (int) $currentSocial->getValue('template_id');
				$this->getCurrentTemplater(true);
			}

			if ($requestUri = getServer('REQUEST_URI')) {
				$requestUriInfo = @parse_url($requestUri);
				$requestUri = getArrayKey($requestUriInfo, 'path');
				$queryParams = getArrayKey($requestUriInfo, 'query');

				// TODO: разобраться, зачем из социального модуля срезаются query params.
				if ($socialModule && ($currentSocial = $socialModule->getCurrentSocial())) {
					$queryParams = '';
				}

				if ($queryParams) {
					parse_str($queryParams, $queryParamsArr);
					if (isset($queryParamsArr['p'])) {
						unset($queryParamsArr['p']);
					}
					if (isset($queryParamsArr['xmlMode'])) {
						unset($queryParamsArr['xmlMode']);
					}

					$queryParams = http_build_query($queryParamsArr, '', '&');
					if ($queryParams) {
						$requestUri .= '?' . $queryParams;
					}
				}
				$globalVariables['@request-uri'] = $requestUri;
			}

			$userInfo = [];
			$userId = $auth->getUserId();
			$userInfo['@id'] = $userId;
			$userType = 'guest';
			if ($permissions->isAuth() && ($user = $objectsCollection->getObject($userId))) {
				if (getRequest('mobile_application') == 'true') {
					if (!regedit::getInstance()->getVal('//modules/emarket/')) {
						$globalVariables['data']['error'] = getLabel("error-module-emarket-absent");

						return $globalVariables;
					} elseif (!$permissions->isAllowedMethod($userId, 'emarket', 'mobile_application_get_data')) {
						$globalVariables['data']['error'] = getLabel("error-mobile-application-not-allowed");

						return $globalVariables;
					}
				}
				/**
				 * @var umiObject $user
				 */
				$userType = 'user';
				$userInfo['@status'] = 'auth';
				$userInfo['@login'] = $user->login;
				$userInfo['xlink:href'] = $user->xlink;
				if ($permissions->isAdmin()) {
					$userType = 'admin';
					if ($permissions->isSv()) {
						$userType = 'sv';
					}
				}
			}
			$userInfo['@type'] = $userType;

			if ($geoip = $this->getModule("geoip")) {
				/**
				 * @var geoip $geoip
				 */
				$geoinfo = $geoip->lookupIp();
				if (!isset($geoinfo['special'])) {
					$userInfo['geo'] = [
							'country'   => $geoinfo['country'],
							'region'    => $geoinfo['region'],
							'city'      => $geoinfo['city'],
							'latitude'  => $geoinfo['lat'],
							'longitude' => $geoinfo['lon']
					];
				} else {
					$userInfo['geo'] = ['special' => $geoinfo['special']];
				}
			}

			$globalVariables['user'] = $userInfo;

			if ($elementId && ($element = $hierarchy->getElement($elementId))) {

				$parentElements = $hierarchy->getAllParents($elementId);

				$parentsInfo = [];
				foreach ($parentElements as $parentElementId) {
					if ($parentElementId == 0) {
						continue;
					}

					if ($parentElement = $hierarchy->getElement($parentElementId)) {
						$parentsInfo[] = $parentElement;
					}
				}
				$globalVariables += [
						'@pageId'   => $elementId,
						'parents'   => [
								'+page' => $parentsInfo
						],
						'full:page' => $element
				];

				def_module::pushEditable($currentModule, $currentMethod, $elementId);
			} elseif ($currentModule == 'content' && $currentMethod == 'content') {
				/** @var HttpOutputBuffer $buffer */
				$buffer = outputBuffer::current();
				$buffer->status("404 Not Found");
				$globalVariables['@method'] = "notfound";
			} elseif (!$notPermitted && $this->isAllowedExecuteMethod($currentModule, $currentMethod)) {
				// execute site mode method
				try {
					$path = getRequest('path');
					// secure path
					$pathParts = explode("/", $path);
					if (isset($pathParts[0]) && $pathParts[0] == $this->getCurrentLang()->getPrefix()) {
						$pathParts = array_slice($pathParts, 1);
					}
					if (count($pathParts) < 2) {
						throw new coreException("Invalid udata path");
					}
					$pathParts[0] = $currentModule;
					$pathParts[1] = $currentMethod;
					$path = 'udata://' . implode("/", $pathParts);

					$globalVariables['xml:data'] = $this->executeStream($path);
				} catch (publicException $e) {
					$globalVariables['data'] = $e;
				}
			}

			return $globalVariables;
		}

		/**
		 * Возвращает базовые глобальные переменные для клиентской части, то есть,
		 * те которые теоретически могут использоваться на каждой странице
		 * @return array
		 */
		private function getBaseClientVariables() {
			$variables = [
				'@module' => $this->getCurrentModule(),
				'@method' => $this->getCurrentMethod(),
				'@domain' => $this->getCurrentDomain()->getHost(),
				'@system-build' => regedit::getInstance()->getVal("//modules/autoupdate/system_build"),
				'@lang' => $this->getCurrentLang()->getPrefix(),
				'@pre-lang' => $this->getPreLang(),
				'@header' => def_module::parseTPLMacroses(macros_header()),
				'@title' => def_module::parseTPLMacroses(macros_title()),
				'@site-name' => def_module::parseTPLMacroses(macros_sitename()),
				'@csrf' => \UmiCms\Service::Session()->get('csrf_token'),
				'meta' => [
					'keywords' => macros_keywords(),
					'description' => macros_describtion()
				],

			];

			if ( isDemoMode() ) {
				$variables['@demo'] = 1;
			}

			if ( $this->getCurrentElementId() ) {
				$variables['@pageId'] = $this->getCurrentElementId();
			}

			return $variables;
		}

		/**
		 * Определяет нужно ли использовать только базовые глобальные переменные для клиентской части
		 * в рамках общих данных для шаблонизации
		 * @return bool
		 */
		private function useOnlyBaseClientVariables() {
			return (bool) mainConfiguration::getInstance()
				->get('system', 'use-only-base-client-variables');
		}

		private function init() {
			$this->loadLangs();
			$this->doSomething();
			$this->errorUrl = getServer('HTTP_REFERER');
			$this->calculateRefererUri();
		}

		/**
		 * Определяет текущий домен
		 * @return bool
		 * @throws coreException
		 */
		private function detectDomain() {
			$host = getServer('HTTP_HOST');
			$domainsCollection = domainsCollection::getInstance();
			$requestDomainId = $domainsCollection->getDomainId($host);
			$requestDomain = $domainsCollection->getDomain($requestDomainId);
			$defaultDomain = $domain = $domainsCollection->getDefaultDomain();

			switch (true) {
				case ($requestDomain instanceof iDomain) : {
					$currentDomain = $requestDomain;
					break;
				}
				case ($defaultDomain instanceof iDomain) : {
					$currentDomain = $defaultDomain;
					break;
				}
				default : {
					throw new coreException('Cannot detect current domain');
				}
			}

			if ($host != $domain->getHost()) {
				$this->handleRequestFromMirror($currentDomain, $requestDomain);
			}

			$this->setCurrentDomain($currentDomain);
		}

		/**
		 * Обрабатывает запрос с зеркала домена.
		 * В зависимости от настроек:
		 * @link http://dev.docs.umi-cms.ru/nastrojka_sistemy/dostupnye_sekcii/sekciya_seo/#sel=29:1,29:3
		 *
		 * Совершает одно из следующих действий:
		 *
		 * 1) Перенаправляет с зеркала на текущий домен;
		 * 2) Прерывает выполнение скрипта, если запрошено неизвестное зеркало;
		 * 3) Добавляет неизвестное зеркало в список зеркал текущего домена;
		 * 4) Ничего не делает
		 *
		 * @param iDomain $currentDomain текущий домен
		 * @param iDomain|bool $requestDomain запрошенный домен
		 * @throws coreException
		 */
		private function handleRequestFromMirror(iDomain $currentDomain, $requestDomain) {
			$config = mainConfiguration::getInstance();
			$primaryDomainRedirect = $config->get('seo', 'primary-domain-redirect');
			$requestUnknownDomain = !$requestDomain instanceof iDomain;

			if ($primaryDomainRedirect == 1) {
				/**
				 * @var HttpOutputBuffer $buffer
				 */
				$buffer = outputBuffer::current('HTTPOutputBuffer');
				$uri = getSelectedServerProtocol() . '://' . $currentDomain->getHost() . getServer('REQUEST_URI');
				$buffer->redirect($uri);
			}

			if ($primaryDomainRedirect == 2 && $requestUnknownDomain) {
				/**
				 * @var HttpOutputBuffer $buffer
				 */
				$buffer = outputBuffer::current('HTTPOutputBuffer');
				$buffer->status(500);
				$buffer->option('generation-time', false);
				$buffer->push(file_get_contents(CURRENT_WORKING_DIR . "/errors/invalid_domain.html"));
				$buffer->end();
			}

			if ($primaryDomainRedirect == 3 && $requestUnknownDomain) {
				$host = getServer('HTTP_HOST');
				$currentDomain->addMirror($host);
			}
		}

		/**
		 * Возвращает идентицикатор языка на основании данных запроса
		 * @return bool|int
		 */
		private function getRequestLanguageId() {
			$requestLanguageIdList = getRequest('lang_id');
			$languagesCollection = langsCollection::getInstance();
			$requestLanguageId = false;

			if ($requestLanguageIdList != null) {
				if (is_array($requestLanguageIdList)) {
					list($requestLanguageIdList) = $requestLanguageIdList;
				}

				$requestLanguageId = intval($requestLanguageIdList);

			} elseif (!is_null(getRequest('links')) && is_array($rel = getRequest('rel'))) {
				if (sizeof($rel) && ($elementId = array_pop($rel))) {
					$element = umiHierarchy::getInstance()->getElement($elementId, true);

					if ($element instanceof umiHierarchyElement) {
						$requestLanguageId = $element->getLangId();
					}
				}
			} else {
				list($sub_path) = $this->getPathArray();
				$requestLanguageId = $languagesCollection->getLangId($sub_path);
			}

			return $requestLanguageId;
		}

		/**
		 * Определяет текущий язык
		 * @throws coreException
		 */
		private function detectLang() {
			$languagesCollection = langsCollection::getInstance();
			$requestLanguageId = $this->getRequestLanguageId();
			$requestLanguage = $languagesCollection->getLang($requestLanguageId);
			$currentDomain = $this->getCurrentDomain();
			$currentDomainLanguage = $languagesCollection->getLang($currentDomain->getId());
			$defaultLanguage = $languagesCollection->getDefaultLang();

			switch (true) {
				case ($requestLanguage instanceof iLang) : {
					$currentLanguage = $requestLanguage;
					break;
				}
				case ($currentDomainLanguage instanceof iLang) : {
					$currentLanguage = $currentDomainLanguage;
					break;
				}
				case ($defaultLanguage instanceof ilang) : {
					$currentLanguage = $defaultLanguage;
					break;
				}
				default : {
					throw new coreException('Cannot detect current language');
				}
			}

			$this->setCurrentLang($currentLanguage);
		}

		/**
		 * Определяет текущий шаблонизатор.
		 * @return umiTemplater
		 */
		private function detectCurrentTemplater() {
			if (defined('VIA_HTTP_SCHEME') && VIA_HTTP_SCHEME) {
				return $this->current_templater = $this->initHTTPSchemeModeTemplater();
			} elseif ($this->getCurrentMode() == 'admin') {
				return $this->current_templater = $this->initAdminModeTemplater();
			} else {
				return $this->current_templater = $this->initSiteModeTemplater();
			}
		}

		/**
		 * Инициализируем шаблонизатор для режима работы VIA_HTTP_SCHEME
		 * @return umiTemplater
		 */
		private function initHTTPSchemeModeTemplater() {
			outputBuffer::contentGenerator('XSLT, HTTP SCHEME MODE');

			return umiTemplater::create('XSLT');
		}

		/**
		 * Инициализируем шаблонизатор для Site Mode,
		 * определяем шаблон и возвращаем инстанс соответсвующего шаблонизатора
		 * @return umiTemplater
		 */
		private function initSiteModeTemplater() {
			$template = $this->detectCurrentDesignTemplate();
			// шаблон не определен, выдаем ошибку, завершаем работу
			if (!$template instanceof template) {
				try {
					$buffer = outputBuffer::current();
				} catch (coreException $e) {
					$buffer = outputBuffer::current('HTTPOutputBuffer');
				}

				$buffer->clear();

				if ($buffer instanceof HTTPOutputBuffer) {
					$buffer->status(500);
				}

				$buffer->push(file_get_contents(SYS_ERRORS_PATH . 'no_design_template.html'));
				$buffer->end();
			}

			$templaterType = $template->getType();
			if ($templaterType == 'xsl') {
				$templaterType = 'XSLT';
			}
			if ($templaterType == 'tpls') {
				$templaterType = 'TPL';
			}

			$templaterType = strtoupper($templaterType);
			outputBuffer::contentGenerator($templaterType . ', SITE MODE');

			return umiTemplater::create($templaterType, $template->getFilePath());
		}

		/**
		 * Инициализируем шаблонизатор для Admin Mode
		 */
		private function initAdminModeTemplater() {
			$config = mainConfiguration::getInstance();
			$skinPath = $config->includeParam('templates.skins', ['skin' => system_get_skinName()]);
			$permissions = permissionsCollection::getInstance();
			$userId = UmiCms\Service::Auth()->getUserId();
			$isAllowed = $permissions->isAllowedMethod($userId, $this->getCurrentModule(), $this->getCurrentMethod());

			// TODO: вынести в конфиг все названия шаблонов
			$fileName = 'main.xsl';

			if (!$permissions->isAdmin(false, true) || !$isAllowed) {

				if ($permissions->isAuth()) {
					$sqlWhere = "owner_id = {$userId}";
					$userGroups = umiObjectsCollection::getInstance()->getObject($userId)->getValue('groups');

					if (is_array($userGroups)) {
						foreach ($userGroups as $userGroup) {
							$sqlWhere .= " or owner_id = {$userGroup}";
						}
					}

					$connection = ConnectionPool::getInstance()->getConnection();
					// TODO: убрать прямые запросы к БД
					$sql = "SELECT `module` FROM cms_permissions WHERE (" . $sqlWhere . ") and (method = '' or method is null)";
					$result = $connection->queryResult($sql);

					if ($result->length() !== 0) {
						$regedit = regedit::getInstance();
						$result->setFetchType(IQueryResult::FETCH_ARRAY);

						foreach ($result as $row) {
							$module = $row[0];
							$method = $regedit->getVal("//modules/{$module}/default_method_admin");
							if ($permissions->isAllowedMethod($userId, $module, $method)) {
								$host =  $this->getCurrentDomain()->getHost();
								def_module::simpleRedirect('http://' . $host . '/admin/' . $module . '/' . $method . "/");
								break;
							}
						}
					}
				}
				$fileName = 'main_login.xsl';
			}
			$templateSource = $skinPath . $fileName;

			if (!is_file($templateSource)) {
				throw new coreException('Template "' . $templateSource . '" not found.');
			}

			outputBuffer::contentGenerator('XSLT, ADMIN MODE');

			return umiTemplater::create('XSLT', $templateSource);
		}

		private function getPathArray() {
			$path = getRequest('path');
			$path = trim($path, "/");

			$urlSuffix = mainConfiguration::getInstance()->get('seo', 'url-suffix');
			$pos = strrpos($path, $urlSuffix);
			if ($pos && ($pos + strlen($urlSuffix) == strlen($path))) {
				$path = substr($path, 0, $pos);
			}

			return explode("/", $path);
		}

		/**
		 * Определяет текущий режим работы
		 */
		private function detectMode() {
			if (isset($_SERVER['argv']) && 1 <= count($_SERVER['argv'])
					&& !(isset($_SERVER['QUERY_STRING']) && $_SERVER['argv'][0] == $_SERVER['QUERY_STRING'])
			) {
				$this->setCurrentMode("cli");
				cacheFrontend::$cacheMode = true;

				return;
			}

			$path_arr = $this->getPathArray();

			if (sizeof($path_arr) < 2) {
				$path_arr[1] = null;
			}

			list($sub_path1, $sub_path2) = $path_arr;

			if ($sub_path1 == "admin" || $sub_path2 == "admin") {
				$this->setCurrentMode("admin");
				cacheFrontend::$adminMode = true;
			} else {
				$this->setCurrentMode("");
				cacheFrontend::$cacheMode = true;
				cacheFrontend::$adminMode = false;
			}
		}

		private function getSubPathType($sub_path) {
			$regedit = regedit::getInstance();

			if (!$this->getCurrentModule()) {

				if ($sub_path == "trash") {
					def_module::redirect($this->getPreLang() . "/admin/data/trash/");
				}

				if ($regedit->getVal("//modules/" . $sub_path, true)) {
					$this->setCurrentModule($sub_path);

					return "MODULE";
				}
			}

			if ($this->getCurrentModule() && !$this->getCurrentMethod()) {
				$this->setCurrentMethod($sub_path);

				return "METHOD";
			}

			if ($this->getCurrentModule() && $this->getCurrentMethod()) {
				return "PARAM";
			}

			return "UNKNOWN";
		}

		private function reset() {
			$this->setCurrentModule('');
			$this->setCurrentMethod('');

			for ($i = 0; $i < 10; $i++) {
				if (isset($_REQUEST['param' . $i])) {
					unset($_REQUEST['param' . $i]);
				} else {
					break;
				}
			}
		}

		/**
		 * Определяет текущий языковой префикс
		 */
		private function detectPreLang() {
			$currentLanguage = $this->getCurrentLang();
			$currentDomain = $this->getCurrentDomain();

			if ($currentLanguage->getId() != $currentDomain->getDefaultLangId()) {
				$languagePrefix = '/' . $currentLanguage->getPrefix();
			} else {
				$languagePrefix = '';
			}

			$_REQUEST['pre_lang'] = $languagePrefix;
			$this->setPreLang($languagePrefix);
		}

		/**
		 * @deprecated
		 * Выполняет проверку безопасности на наличие CSRF-атаки
		 * @return bool true если проверка безопасности пройдена успешно, false если проверка безопасности провалена
		 */
		public static function isCSRFTokenValid() {
			try {
				return \UmiCms\System\Protection\Security::getInstance()->checkCsrf();
			} catch (\UmiCms\System\Protection\CsrfException $e) {
				return false;
			}
		}

		/**
		 * @deprecated
		 */
		public function getLang() {
			return $this->getCurrentLang();
		}

		/**
		 * @deprecated
		 */
		public function setLang(iLang $lang) {
			$this->setCurrentLang($lang);
		}

		/**
		 * @deprecated
		 */
		public function getSkinPath() {
			return null;
		}
	}
