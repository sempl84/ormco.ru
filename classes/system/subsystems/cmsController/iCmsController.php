<?php
	interface iCmsController {
		/**
		 * @const string ADMIN_MODE_ID идентификатор административного режима
		 */
		const ADMIN_MODE_ID = 'admin';
		/**
		 * Проверяет работает ли система в административном режиме
		 * @return bool результат проверки
		 */
		public function isCurrentModeAdmin();
		/**
		 * Возвращает идентификатор административного режима
		 * @return string
		 */
		public function getAdminModeId();
		public function loadBuildInModule($moduleName);
		public function getModule($moduleName, $resetCache = false);
		public function installModule($installPath);
		/**
		 * Возвращает имя модуля, который обрабатывает текущий запрос
		 * @return string
		 */
		public function getCurrentModule();
		/**
		 * Устанавливает имя модуля, который обрабатывает текущий запрос
		 * @param string $moduleName имя модуля
		 * @return iCmsController
		 */
		public function setCurrentModule($moduleName);
		/**
		 * Возвращает имя метода модуля, который обрабатывет текущий запрос
		 * @return string
		 */
		public function getCurrentMethod();
		/**
		 * Устанавливает имя метода модуля, который обрабатывет текущий запрос
		 * @param string $methodName имя метода
		 * @return iCmsController
		 */
		public function setCurrentMethod($methodName);
		public function getCurrentElementId();
		/**
		 * Возвращает текущий режим работы системы ('admin', 'cli', '').
		 * Если он не задан - пытается его определить
		 * @return string
		 */
		public function getCurrentMode();
		/**
		 * Устанавливает текущий режим работы
		 * @param string $mode текущий режим работы
		 * @return iCmsController
		 */
		public function setCurrentMode($mode);

		/**
		 * Возвращает текущий домен.
		 * Если он не задан - пытается его определить
		 * @return iDomain
		 * @throws coreException
		 */
		public function getCurrentDomain();
		/**
		 * Устанавливает текущий домен
		 * @param iDomain $domain домен
		 * @return iCmsController
		 */
		public function setCurrentDomain(iDomain $domain);
		/**
		 * Возвращает текущий язык в системе.
		 * Если он не задан - пытается его определить.
		 * @return iLang
		 * @throws coreException
		 */
		public function getCurrentLang();
		/**
		 * Устанавливает текущий язык
		 * @param iLang $lang язык
		 * @return iCmsController
		 */
		public function setCurrentLang(iLang $lang);
		/**
		 * Возвращает текущий языковой префикс.
		 * Если он не задан - пытается его определить.
		 * @return string
		 */
		public function getPreLang();
		/**
		 * Устанавливает текущий языковой префикс
		 * @param string $prefix префикс
		 * @return iCmsController
		 */
		public function setPreLang($prefix);

		public function getCurrentTemplater($forceRefresh);
		public function getRequestId();
		public function calculateRefererUri();
		public function getCalculatedRefererUri();
	}

