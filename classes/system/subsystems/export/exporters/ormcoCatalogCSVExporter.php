<?php

class ormcoCatalogCSVExporter extends umiExporter
{
    /** @var string $encoding наименование кодировки, в которой будут экспортированы данные */
    private $encoding = 'windows-1251';
    /** @var string $delimiter разделитель полей */
    private $delimiter = ';';
    /** @const VALUE_LIMITER символ, обрамляющий значения полей */
    const VALUE_LIMITER = '"';
    /** @const SOURCE_ENCODING наименование исходной кодировки данных */
    const SOURCE_ENCODING = 'utf-8';
    /** @var array список поддерживаемых кодировок */
    protected static $supportedEncodings = array('utf-8', 'windows-1251', 'cp1251');
    
    public function setOutputBuffer()
    {
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset($this->encoding);
        $buffer->contentType("text/plain");
        return $buffer;
    }
    
    /**
     * Устанавливает кодировку, в которой будут экспортированы данные
     * @param string $encoding наименование устанавливаемой кодировки
     * @throws InvalidArgumentException если кодировка не поддерживается
     */
    public function setEncoding($encoding)
    {
        if (in_array(strtolower($encoding), self::$supportedEncodings)) {
            $this->encoding = $encoding;
        } else {
            throw new InvalidArgumentException("Encoding '${encoding}' is not supported");
        }
        
    }
    
    /**
     * Конструктор класса
     * @param string $type идентификатор формата экспорта
     */
    public function __construct($type)
    {
        parent::__construct($type);
        ini_set('mbstring.substitute_character', "none");
    }
    
    /**
     * Устанавливает символ, представляющий разделитель полей
     * @param string $delimiter символ-разделитель
     */
    public function setDelimiter($delimiter)
    {
        if ($delimiter) {
            $this->delimiter = (string)$delimiter;
        }
    }
    
    public function export($branches, $excludedBranches)
    {
        $temp_dir = SYS_TEMP_PATH . "/export/";
        $id = getRequest('param0');
        if (!is_dir($temp_dir)) mkdir($temp_dir, 0777, true);
        $cache_file_path = $temp_dir . $id . "." . $this->getFileExt();
        $sourceName = $id . "." . $this->getFileExt();
        
        if (file_exists($cache_file_path) && !file_exists($cache_file_path . 'array')) unlink($cache_file_path);
        
        $elementsToExport = array();
        
        if (!file_exists($cache_file_path . 'array')) {
            $sel = new selector('pages');
            $sel->types('hierarchy-type')->name(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
            $sel->where('is_active')->equals(1);
            $sel->group('obj_id');
            $sel->option('return', 'id');
            
            foreach ($sel as $row) {
                if(!isset($row['id'])) {
                    continue;
                }
                
                $elementsToExport[] = $row['id'];
            }
        } else {
            $elementsToExport = unserialize(file_get_contents($cache_file_path . 'array'));
        }
        
        $blockSize = false;
        if (getRequest('as_file') !== '0') {
            $blockSize = (int)mainConfiguration::getInstance()->get("modules", "exchange.export.limit");
            if ($blockSize <= 0) {
                $blockSize = 25;
            }
        }
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
        $sel->where('is_active')->equals(1);
        $sel->order('id')->asc();
        
        if (getRequest('as_file') !== '0') {
            $sel->where('id')->equals(array_slice($elementsToExport, 0, $blockSize + 1));
        }
    
        $umiLinksHelper = umiLinksHelper::getInstance();
    
        foreach($sel as $element) {
            if(!$element instanceof umiHierarchyElement) {
                continue;
            }
        
            $item = array();
            $article = trim($element->getValue(SiteCatalogObjectModel::field_article));
            if(!$article) {
                continue;
            }
        
            $article = str_replace('"', "'", $article);
            $item['id'] = $article;
        
            $title = trim($element->getValue('h1_alternative'));
            if(!$title) {
                $title = trim($element->getName());
            }
        
            $title = str_replace('"', "'", $title);
            $item['title'] = $title;
        
            $content = def_module::parseTPLMacroses($element->getValue('description'), $element->getId());
            $content = strip_tags($content);
            $content = str_replace('"', "'", $content);
            $content = str_replace(array("\t", PHP_EOL), '', $content);
            $item['content'] = trim($content);
        
            $availability = (intval($element->getValue('common_quantity')) > 0) ? 'in stock' : 'out of stock';
            $item['availability'] = $availability;
        
            $item['condition'] = 'new';
            $item['price'] = $element->getValue('price');
            $item['link'] = 'https://ormco.ru' . $umiLinksHelper->getLinkByParts($element);
            $item['image_link'] = 'https://ormco.ru/images/ormco-logo-blue-square.jpg';
            $item['brand'] = 'Ormco';
        
            $items[] = $item;
        }
    
        $handle = fopen($cache_file_path, 'a');
        $limiter = '"';
        $delimiter = "\t";
        $delimitingString = $limiter . $delimiter . $limiter;
    
        if(!file_get_contents($cache_file_path)) {
            fputs($handle, $limiter . implode($delimitingString, array_keys($items[0])) . $limiter . PHP_EOL);
        }
    
        foreach($items as $item) {
            $string = $limiter . implode($delimitingString, $item) . $limiter . PHP_EOL;
        
            if ($string = mb_convert_encoding($string, 'windows-1251', 'UTF-8')) {
                fputs($handle, $string);
            }
        }

        fclose($handle);
        
        if (getRequest('as_file') !== '0') {
            $exportedElements = array_slice($elementsToExport, 0, $blockSize + 1);
            $elementsToExport = array_diff($elementsToExport, $exportedElements);
            
            if (count($elementsToExport)) {
                $this->completed = false;
                file_put_contents($cache_file_path . 'array', serialize($elementsToExport));
            } else {
                if (file_exists($cache_file_path . 'array')) unlink($cache_file_path . 'array');
                $this->completed = true;
            }
        } else {
            $this->completed = true;
        }
        
        chmod($cache_file_path, 0777);
        return false;
    }
    
    public function getFileExt()
    {
        return "csv";
    }
}