<?php

	class HTTPOutputBuffer extends outputBuffer {
		protected $cleanEipAttributes = false;

		protected $charset = 'utf-8';

		protected $contentType = 'text/html';

		protected $headers = [];

		protected $headersSended = false;

		protected $status = '200 Ok';

		/**
		 * Опции буффера:
		 *   quick-edit: записывать в сессию информацию о редактируемых через EIP страницах, @see $this->end()
		 *   generation-time: выводить строку времени генерации страницы, @see $this->getCallTime()
		 */
		protected $options = [
			'quick-edit' => true,
			'generation-time' => true
		];

		public function send() {
			$buffer = $this->buffer;

			if ($this->getStatusCode() != 500) {
				$eventPoint = new umiEventPoint('systemBufferSend');
				$eventPoint->setMode('before');
				$eventPoint->addRef('buffer', $buffer);
				$eventPoint->call();
			}

			$this->buffer = $buffer;

			$this->sendHeaders();

			echo $this->buffer;
			$this->clear();
		}

		public function status($status = false) {
			if ($status) {
				$this->status = $status;
			}
			return $this->status;
		}

		/**
		 * Возвращает код статуса ответа
		 * @return int
		 */
		public function getStatusCode() {
			return (int) $this->status;
		}

		public function charset($charset = false) {
			if ($charset) {
				$this->charset = $charset;
			}
			return $this->charset;
		}

		public function contentType($contentType = false) {
			if ($contentType) {
				$this->contentType = $contentType;
			}
			return $this->contentType;
		}

		public function getHTTPRequestBody() {
			$putdata = fopen('php://input', 'r');
			$data = '';

			while (!feof($putdata)) {
				$data .= fread($putdata, 1024);
			}
			fclose($putdata);

			return $data;
		}

		public function sendHeaders() {
			if ($this->headersSended) {
				return true;
			} else {
				if (headers_sent()) {
					return false;
				}
			}

			$this->setDefaultHeaders();

			if ($this->getStatusCode() != 404 && defined('CALC_LAST_MODIFIED') && CALC_LAST_MODIFIED) {
				$this->setBrowserCacheHeaders();
			}

			if ($this->getStatusCode() != 500) {
				$eventPoint = new umiEventPoint('bufferHeadersSend');
				$eventPoint->setMode('before');
				$eventPoint->addRef('buffer', $this);
				$eventPoint->call();
			}

			$this->sendStatusHeader();

			foreach ($this->getHeaderList() as $header => $value) {
				$this->sendHeader($header, $value);
			}

			$this->sendCookies();
			$this->headersSended = true;
		}

		public function end() {
			$this->bufferDirectHeaders();
			$this->deleteDirectHeaders();

			if (!DEBUG) {
				@ob_clean();
			}

			if (getArrayKey($this->options, 'quick-edit')) {
				umiTemplater::prepareQuickEdit();
			}

			$this->push($this->getCallTime());
			$this->send();
			exit();
		}

		/**
		 * Возвращает значение опции или устанавливает значение, если передан второй параметр
		 * @param string $key имя опции
		 * @param null $value значение опции
		 * @return mixed|null
		 */
		public function option($key, $value = null) {
			if ($value === null) {
				return isset($this->options[$key]) ? $this->options[$key] : null;
			} else {
				$this->options[$key] = $value;
			}
		}

		public static function checkUrlSecurity($url) {
			return stripos($url, 'javascript:') === false && stripos($url, 'data:') === false && !preg_match('/^\/{2,}/i', $url);
		}

		public function redirect($url, $textStatus = '301 Moved Permanently', $numStatus = 301) {
			$maxLevels = 0;

			while (ob_get_level() && $maxLevels++ < 5) {
				@ob_end_clean();
			}

			$url = urldecode($url);

			if (!self::checkUrlSecurity($url)) {
				$this->status = '400 Bad Request';
				$this->sendStatusHeader();
				flush();
				exit('');
			}

			$this->status = $textStatus;
			$this->sendStatusHeader();

			$uriInfo = parse_url($url);

			if (!isset($uriInfo['scheme']) && !isset($uriInfo['host'])) {
				$url = '/' . ltrim($url, '/');
			}

			$this->sendCookies();
			header('Location: ' . $url, true, $numStatus);
			flush();
			exit('');
		}

		/**
		 * Был ли установлен заголовок
		 * @param string $name имя заголовка
		 * @return bool
		 * @throws wrongParamException
		 */
		public function issetHeader($name) {
			if (!is_string($name) || strlen($name) === 0) {
				throw new wrongParamException('Header name must be not empty string');
			}

			return isset($this->headers[$name]);
		}

		/**
		 * Устанавливает заголовок
		 * @param string $name имя заголовка
		 * @param string $value значение заголовка
		 * @return $this
		 * @throws wrongParamException
		 */
		public function setHeader($name, $value) {
			if (!is_string($name) || strlen($name) === 0) {
				throw new wrongParamException('Header name must be not empty string');
			}

			if (!is_string($value) || strlen($value) === 0) {
				throw new wrongParamException('Header value must be not empty string');
			}

			$this->headers[$name] = $value;
			return $this;
		}

		/**
		 * Удаляет заголовок из очереди на отправку
		 * @param string $name имя заголовка
		 * @return $this
		 * @throws wrongParamException
		 */
		public function unsetHeader($name) {
			if (!is_string($name) || strlen($name) === 0) {
				throw new wrongParamException('Header name must be not empty string');
			}

			unset($this->headers[$name]);
			return $this;
		}

		/**
		 * Возвращает очередь заголовков для отправки
		 * @return array
		 *
		 * [
		 * 		header_name => header_value
		 * ]
		 */
		public function getHeaderList() {
			return $this->headers;
		}

		/**
		 * Выводит в буффер данные в json формате
		 * @param mixed $data данные
		 */
		public function printJson($data) {
			$this->calltime();
			$this->contentType('text/javascript');
			$this->charset('utf-8');
			$this->option('generation-time', false);
			$this->push(json_encode($data));
			$this->end();
		}

		public function length() {
			ob_start();
			echo $this->buffer;
			$size = ob_get_length();
			ob_end_clean();
			return $size;
		}

		/**
		 * Устанавливает значения для заголовков по умолчанию, если они не были заданы
		 */
		protected function setDefaultHeaders() {
			if (!$this->issetHeader('Content-Type')) {
				$this->setHeader('Content-Type', $this->contentType . '; charset=' . $this->charset);
			}

			if (!$this->issetHeader('Content-Length')) {
				$length = $this->length();

				if ($length) {
					$this->setHeader('Content-Length', (string) $length);
				}
			}

			if (!$this->issetHeader('Date')) {
				$this->setHeader('Date', (gmdate('D, d M Y H:i:s') . ' GMT'));
			}

			if (!$this->issetHeader('X-Generated-By')) {
				$this->setHeader('X-Generated-By', 'UMI.CMS');
			}

			if (!$this->issetHeader('X-UA-Compatible') && strpos(getServer('HTTP_USER_AGENT'), 'MSIE')) {
				$this->setHeader('X-UA-Compatible', 'IE=edge');
			}

			$version = (string) regedit::getInstance()
				->getVal('//modules/autoupdate/system_version');

			if (!$this->issetHeader('X-CMS-Version') && !empty($version)) {
				$this->setHeader('X-CMS-Version', $version);
			}

			$config = mainConfiguration::getInstance();
			$cacheControlOption = $config->get('kernel', 'cache-control');
			$cacheControlAllowed = ['private', 'public'];
			$cacheControl = 'private';

			if (!is_null($cacheControlOption) && in_array($cacheControlOption, $cacheControlAllowed)) {
				$cacheControl = $cacheControlOption;
			}

			$this->setHeader('Cache-Control', "${cacheControl}, no-cache, must-revalidate, max-age=0");
			$this->setHeader('Pragma', 'no-cache');

			if (!$this->issetHeader('X-XSS-Protection')) {
				$this->setHeader('X-XSS-Protection', '0');
			}
		}

		protected function sendStatusHeader() {
			header('HTTP/1.1 ' . $this->status);
			// Some servers close connection when we duplicate status header
			if ((int) mainConfiguration::getInstance()->get('kernel', 'send-additional-status-header')) {
				header('Status: ' . $this->status, true);
			}
		}

		protected function sendHeader($header, $value) {
			header("{$header}: {$value}");
		}

		protected function setBrowserCacheHeaders() {
			$hierarchy = umiHierarchy::getInstance();
			$currentElementId = cmsController::getInstance()->getCurrentElementId();
			$elementsUpdateTime = $hierarchy->getElementsLastUpdateTime();
			$objectsUpdateTime = umiObjectsCollection::getInstance()->getObjectsLastUpdateTime();
			$updateTime = $elementsUpdateTime < $objectsUpdateTime ? $objectsUpdateTime : $elementsUpdateTime;

			$isAllowed = $hierarchy->isAllowed($currentElementId);

			if ($updateTime && $currentElementId && $isAllowed) {
				$this->setHeader('Last-Modified', (gmdate('D, d M Y H:i:s', $updateTime) . ' GMT'));
				$this->setHeader('Expires', (gmdate('D, d M Y H:i:s', time() + 24 * 3600) . ' GMT'));
				if (function_exists('apache_request_headers')) {
					$request = apache_request_headers();
					if (isset($request['If-Modified-Since']) && (strtotime($request['If-Modified-Since']) >= ($updateTime))) {
						$this->status('304 Not Modified');
						$this->setHeader('Connection', 'close');
					}
				}
			}

			if (defined('CALC_E_TAG') && CALC_E_TAG) {
				$this->setHeader('E-tag', sha1($this->content()));
			}
		}

		/**
		 * Возвращает строку времени генерации страницы, пример:
		 * <!– This page generated in 0.071788 secs by XSLT, SITE MODE –>
		 * @return string|void
		 */
		protected function getCallTime() {
			$generationTime = round(microtime(true) - $this->invokeTime, 6);
			$config = mainConfiguration::getInstance();
			$showGenerateTime = (string) $config->get('kernel', 'show-generate-time');

			if (!$this->option('generation-time') || $showGenerateTime === '0') {
				return;
			}

			$generatedBy = '';
			$contentGenerator = parent::contentGenerator();
			if (is_string($contentGenerator) && strlen($contentGenerator)) {
				$generatedBy = ' by ' . $contentGenerator;
			}

			switch ($this->contentType) {
				case 'text/html':
				case 'text/xml':
					return "<!-- This page generated in {$generationTime} secs{$generatedBy} -->";

				case 'application/javascript':
				case 'text/javascript':
					return "/* This page generated in {$generationTime} secs{$generatedBy} */";

				default:
					// no default
			}
		}

		/**
		 * Помещает заголовки, отправленные напрямую, в буффер
		 */
		private function bufferDirectHeaders() {
			foreach (headers_list() as $header) {
				list($name, $value) = explode(':', $header);
				$this->setHeader(trim($name), trim($value));
			}
		}

		/**
		 * Удаляет заголовки, отправленные напрямую, из очереди на отправку
		 */
		private function deleteDirectHeaders() {
			@header_remove();
		}

		/**
		 * Отправляет куки
		 */
		private function sendCookies() {
			$cookiesResponsePool = \UmiCms\Service::CookieJar()
				->getResponsePool();

			foreach ($cookiesResponsePool->getList() as $cookie) {
				setcookie(
					$cookie->getName(),
					$cookie->getValue(),
					$cookie->getExpirationTime(),
					$cookie->getPath(),
					$cookie->getDomain(),
					$cookie->isSecure(),
					$cookie->isForHttpOnly()
				);

				$cookiesResponsePool->remove($cookie->getName());
			}
		}

		/**
		 * @deprecated
		 */
		public function header($name, $value = false) {
			if ($value === false) {
				unset($this->headers[$name]);
				return null;
			} else {
				return $this->headers[$name] = $value;
			}
		}
	}
