<?php
interface iOutputBuffer {
	static public function current($bufferClassName = false);
	public function push($data);
	public function calltime();
	public function content();
	public function length();
	public function clear();
	public function send();
	public function end();
	public static function contentGenerator($generatorType = null);
}