<?php
	/**
	 * Карта констант коллекции шаблоном писем
	 */
	class mailTemplatesConstantMap extends baseUmiCollectionConstantMap {
		/**
		 * @const string имя таблицы, которая содержит данные о шаблонах
		 */
		const TABLE_NAME = 'cms3_mail_templates';
	}
?>
