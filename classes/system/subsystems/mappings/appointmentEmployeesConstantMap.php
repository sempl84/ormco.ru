<?php
	/**
	 * Карта констант коллекции сотрудников записи на прием
	 */
 	class appointmentEmployeesConstantMap extends baseUmiCollectionConstantMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся сотрудники
		 */
		const TABLE_NAME = 'cms3_appointment_employees';
	}
?>
