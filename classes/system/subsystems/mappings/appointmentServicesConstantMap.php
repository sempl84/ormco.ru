<?php
	/**
	 * Карта констант коллекции услуг записи на прием
	 */
	class appointmentServicesConstantMap extends baseUmiCollectionConstantMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся услуги
		 */
		const TABLE_NAME = 'cms3_appointment_services';
	}
?>
