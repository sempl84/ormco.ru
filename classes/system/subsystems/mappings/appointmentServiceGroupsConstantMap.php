<?php
	/**
	 * Карта констант коллекции групп услуг записи на прием
	 */
	class appointmentServiceGroupsConstantMap extends baseUmiCollectionConstantMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся группы услуг
		 */
		const TABLE_NAME = 'cms3_appointment_service_groups';
	}
?>
