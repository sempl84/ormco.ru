<?php
	namespace UmiCms\System\Protection;
	use UmiCms\System\Session\iSession;
	/**
	 * Класс защиты от CSRF
	 * CSRF - "Cross Site Request Forgery" — "Межсайтовая подделка запроса"
	 * Class CsrfProtection
	 * @example
	 *
	 * use UmiCms\System\Protection\CsrfProtection;
	 *
	 * try {
	 *    $csrfProtection = \UmiCms\Service::CsrfProtection();
	 *    $csrfProtection->checkOriginCorrect('example.com');
	 *    $csrfProtection->checkTokenMatch('some_token');
	 *
	 *    // Проверка пройдена успешно
	 *  } catch (\UmiCms\System\Protection\CsrfException $e) {
	 *    // Проверка провалена
	 *  }
	 *
	 * @package UmiCms\System\Protection
	 */
	class CsrfProtection {
		/** @const ключ в сессии, в значении которого хранится токен */
		const TOKEN_KEY = 'csrf_token';

		/** @var iSession объект работы с сессией */
		private $session;

		/** @var \iCmsController контроллер приложения CMS */
		private $cmsController;

		/** @var string токен, с которым будет производиться сравнение для защиты от CSRF */
		private $token;

		/**
		 * Конструктор
		 * @param iSession $session контейнер сессии
		 * @param \iCmsController $cmsController cms контроллер
		 */
		public function __construct(iSession $session, \iCmsController $cmsController) {
			$this->session = $session;
			$this->cmsController = $cmsController;
		}


		/**
		 * Возвращает новое значение csrf-токена
		 */
		public function generateToken() {
			return md5(mt_rand() . microtime());
		}

		/**
		 * Загружает токен из хранилища
		 * @return bool|mixed|null|string
		 */
		public function loadToken() {
			$session = $this->getSession();
			$this->token = $session->get(self::TOKEN_KEY);
			return $this->token;
		}

		/**
		 * Возвращает зависимость в виде объекта, который работает с сессией
		 * @return iSession
		 */
		private function getSession() {
			return $this->session;
		}

		/**
		 * Производит проверку совпадения токенов
		 * @param string $token проверяемый CSRF-токен
		 * @return bool true в случае успешного прохождения проверки
		 * @throws CsrfException в случае неудачного прохождения проверки
		 */
		public function checkTokenMatch($token) {
			return $this->checkCondition($this->getToken() === $token);
		}

		/**
		 * Возвращает текущий CSRF-токен
		 * @return string
		 * @throws \coreException если токен не определен
		 */
		private function getToken() {
			if (!$this->token) {
				throw new \coreException('CSRF token cannot be empty');
			}

			return $this->token;
		}

		/**
		 * Производит проверку заголовка Origin
		 * @param string $origin проверяемый заголовок Origin
		 * @return bool true в случае успешного прохождения проверки
		 * @throws \InvalidArgumentException если передан пустой заголовок
		 * @throws CsrfException в случае неудачного прохождения проверки
		 * @throws \coreException если невозможно получить текущий домен
		 */
		public function checkOriginCorrect($origin) {
			$this->checkNotEmpty($origin);
			$host = $this->getCurrentHost();
			$hasHostInHeader = (strpos($origin, $host) !== false);
			return $this->checkCondition($host && $hasHostInHeader);
		}

		/**
		 * Производит проверку заголовка Referer
		 * @param string $referrer проверяемый заголовок Referer
		 * @return bool true в случае успешного прохождения проверки
		 */
		public function checkReferrerCorrect($referrer) {
			$host = $this->getHostFromReferrer($referrer);
			$this->checkNotEmpty($host);

			$domainNames = [];
			$domainNames[] = $host;
			$domainNames[] = 'www.' . $host;
			$domainNames[] = (string) preg_replace('/(:\d+)/', '', $host);

			$domainsCollection = \domainsCollection::getInstance();

			foreach ($domainNames as $domainName) {
				if (is_numeric($domainsCollection->getDomainId($domainName))) {
					return true;
				}
			}

			$this->checkCondition(false);
		}

		/**
		 * Определяет домен из заголовка HTTP_REFERER
		 * @param string $referrer заголовок
		 * @return string
		 */
		private function getHostFromReferrer($referrer) {
			preg_match('|^http(?:s)?:\/\/(?:www\.)?([^\/]+)|ui', $referrer, $matches);

			if (isset($matches[1]) && count($matches[1]) == 1) {
				return $matches[1];
			}

			return '';
		}

		/**
		 * Возвращает текущий домен системы
		 * @return string текущий домен системы
		 * @throws \coreException если невозможно получить текущий домен
		 */
		private function getCurrentHost() {
			$domain = $this->getCmsController()->getCurrentDomain();

			if (!$domain instanceof \domain) {
				throw new \coreException('Current domain not found');
			}

			return $domain->getHost();
		}

		/**
		 * Возвращает зависимость в виде контроллера приложения CMS
		 * @return \iCmsController
		 * @throws \DependencyNotInjectedException
		 */
		private function getCmsController() {
			if (!($this->cmsController instanceof \iCmsController)) {
				throw new \DependencyNotInjectedException('CmsController not injected');
			}

			return $this->cmsController;
		}

		/**
		 * Производит проверку условия
		 * @param bool $condition условие
		 * @return bool true если условие истинно
		 * @throws CsrfException если условие ложно
		 */
		private function checkCondition($condition) {
			if (!$condition) {
				throw new CsrfException('Csrf protection check failed');
			}

			return true;
		}

		/**
		 * Проверяет, что переданный аргумент не пустой
		 * @param string $string строка
		 * @throws \InvalidArgumentException
		 */
		private function checkNotEmpty($string) {
			if (!$string) {
				throw new \InvalidArgumentException("Empty argument {$string}");
			}
		}

	}
