<?php
	interface iSearchModel {
		public function runSearch($searchString, $searchTypesArray = NULL, $hierarchy_rels = NULL, $orMode = false);
		public function getContext($elementId, $searchString);
		public function getIndexPages();
		public function getAllIndexablePages();
		public function getIndexWords();
		public function getIndexWordsUniq();
		public function getIndexLast();
		public function truncate_index();
		public function index_all($limit = false, $lastId = 0);
		public function index_item($elementId);

		public function index_items($elementId);
		public function unindex_items($elementId);
		
		public function suggestions($string, $limit = 10);
	};
?>