<?php
namespace UmiCms\System\Auth\AuthenticationRules;
use UmiCms\System\Auth\PasswordHash;
/**
 * Класс фабрики правил аутентификации
 * @package UmiCms\System\Auth\AuthenticationRules
 */
class Factory implements iFactory {
	/**
	 * @var PasswordHash\iAlgorithm $hashAlgorithm алгоритм хеширования паролей
	 */
	private $hashAlgorithm;
	/**
	 * @var \selector $queryBuilder конструктор запросов к бд
	 */
	private $queryBuilder;

	/**
	 * @inheritdoc
	 */
	public function __construct(PasswordHash\iAlgorithm $algorithm, \selector $queryBuilder) {
		$this->hashAlgorithm = $algorithm;
		$this->queryBuilder = $queryBuilder;
	}

	/**
	 * @inheritdoc
	 */
	public function createByLoginAndPassword($login, $password) {
		$hashAlgorithm = $this->getHashAlgorithm();
		$queryBuilder = $this->getQueryBuilder();
		return new LoginAndPassword($login, $password, $hashAlgorithm, $queryBuilder);
	}

	/**
	 * @inheritdoc
	 */
	public function createByActivationCode($activationCode) {
		$queryBuilder = $this->getQueryBuilder();
		return new ActivationCode($activationCode, $queryBuilder);
	}

	/**
	 * @inheritdoc
	 */
	public function createByLoginAndProvider($login, $provider) {
		$queryBuilder = $this->getQueryBuilder();
		return new LoginAndProvider($login, $provider, $queryBuilder);
	}

	/**
	 * @inheritdoc
	 */
	public function createByUserId($userId) {
		$queryBuilder = $this->getQueryBuilder();
		return new UserId($userId, $queryBuilder);
	}

	/**
	 * Возвращает алгоритм хеширования паролей
	 * @return PasswordHash\iAlgorithm
	 */
	private function getHashAlgorithm() {
		return $this->hashAlgorithm;
	}

	/**
	 * Возвращает конструктор запросов к бд
	 * @return \selector
	 */
	private function getQueryBuilder() {
		return $this->queryBuilder;
	}
}