<?php
namespace UmiCms\System\Auth\AuthenticationRules;
use UmiCms\System\Auth\PasswordHash;
/**
 * Класс абстрактного правила аутентификации пользователя
 * @package UmiCms\System\Auth\AuthenticationRules
 */
abstract class Rule implements iRule {
	/**
	 * @var \UmiCms\System\Auth\PasswordHash\Algorithm $hashAlgorithm алгоритм хеширования паролей
	 */
	protected $hashAlgorithm;
	/**
	 * @var \selector $queryBuilder конструктор запросов
	 */
	protected $queryBuilder;

	/**
	 * @inheritdoc
	 */
	abstract public function validate();

	/**
	 * Возвращает конструктор запросов, сконфигурированный для выборок пользователей
	 * @return \selector
	 * @throws \selectorException
	 */
	protected function getQueryBuilder() {
		$className = get_class($this->queryBuilder);
		/**
		 * @var \selector $queryBuilder
		 */
		$queryBuilder = new $className('objects');
		$queryBuilder->types('object-type')->name('users', 'user');
		$queryBuilder->option('return')->value('id');
		$queryBuilder->option('no-length')->value(true);
		return $queryBuilder;
	}
}