<?php
namespace UmiCms\System\Auth\AuthenticationRules;
use UmiCms\System\Auth\PasswordHash;
/**
 * Класс правила аутентификации пользователя по логину и паролю
 * @package UmiCms\System\Auth\AuthenticationRules
 */
class LoginAndPassword extends Rule implements iRule {
	/**
	 * @var string $login логин
	 */
	private $login;
	/**
	 * @var string $password пароль
	 */
	private $password;

	/**
	 * Конструктор
	 * @param string $login логин
	 * @param string $password пароль
	 * @param PasswordHash\iAlgorithm $algorithm алгоритм хеширования паролей
	 * @param \selector $queryBuilder конструктор запросов к бд
	 */
	public function __construct($login, $password, PasswordHash\iAlgorithm $algorithm, \selector $queryBuilder) {
		$this->login = $login;
		$this->password = $password;
		$this->hashAlgorithm = $algorithm;
		$queryBuilder->flush();
		$this->queryBuilder = $queryBuilder;
	}

	/**
	 * @inheritdoc
	 */
	public function validate() {
		$login = $this->getLogin();
		$password = $this->getPassword();
		$passwordList = $this->getPasswordList($password);

		try {
			$queryBuilder = $this->getQueryBuilder();
			$queryBuilder->option('or-mode')->fields('login', 'e-mail');
			$queryBuilder->where('login')->equals($login);
			$queryBuilder->where('e-mail')->equals($login);
			$queryBuilder->where('password')->equals($passwordList);
			$queryBuilder->where('is_activated')->equals(true);
			$queryBuilder->limit(0, 1);
			$queryResultSet = $queryBuilder->result();
		} catch (\Exception $e) {
			return false;
		}

		if (count($queryResultSet) === 0) {
			return false;
		}

		$queryResultItem = array_shift($queryResultSet);
		return (int) $queryResultItem['id'];
	}

	/**
	 * Возвращает список хешей пароля
	 * @param string $rawPassword пароль в явном виде
	 * @return array
	 * @throws PasswordHash\WrongAlgorithmException
	 */
	private function getPasswordList($rawPassword) {
		$algorithm = $this->getHashAlgorithm();

		return [
			$algorithm->hash($rawPassword, $algorithm::MD5),
			$algorithm->hash($rawPassword, $algorithm::SHA256)
		];
	}

	/**
	 * Возвращает алгоритм хеширования паролей
	 * @return PasswordHash\Algorithm
	 */
	private function getHashAlgorithm() {
		return $this->hashAlgorithm;
	}

	/**
	 * Возвращает логин
	 * @return string
	 */
	private function getLogin() {
		return (string) $this->login;
	}

	/**
	 * Возвращает пароль
	 * @return string
	 */
	private function getPassword() {
		return (string) $this->password;
	}
}