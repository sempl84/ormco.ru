<?php
namespace UmiCms\System\Auth\AuthenticationRules;
/**
 * Класс правила аутентификации пользователя по коду активации
 * @package UmiCms\System\Auth\AuthenticationRules
 */
class ActivationCode extends Rule implements iRule {
	/**
	 * @var string $activationCode код активации
	 */
	private $activationCode;

	/**
	 * Конструктор
	 * @param string $activationCode код активации
	 * @param \selector $queryBuilder конструктор запросов
	 */
	public function __construct($activationCode, \selector $queryBuilder) {
		$this->activationCode = $activationCode;
		$queryBuilder->flush();
		$this->queryBuilder = $queryBuilder;
	}

	/**
	 * @inheritdoc
	 */
	public function validate() {
		$activationCode = $this->getActivationCode();

		try {
			$queryBuilder = $this->getQueryBuilder();
			$queryBuilder->where('activate_code')->equals($activationCode);
			$queryBuilder->limit(0, 1);
			$queryResultSet = $queryBuilder->result();
		} catch (\Exception $e) {
			return false;
		}

		if (count($queryResultSet) === 0) {
			return false;
		}

		$queryResultItem = array_shift($queryResultSet);
		return (int) $queryResultItem['id'];
	}

	/**
	 * Возвращает код активации пользователя
	 * @return string
	 */
	private function getActivationCode() {
		return (string) $this->activationCode;
	}
}