<?php
namespace UmiCms\System\Auth\AuthenticationRules;
/**
 * Класс правила аутентификации пользователя по логину и названию провайдера данных пользователя (социальной сети)
 * @package UmiCms\System\Auth\AuthenticationRules
 */
class LoginAndProvider extends Rule implements iRule {
	/**
	 * @var string $login логин
	 */
	private $login;
	/**
	 * @var string $provider название провайдера данных пользователя (социальной сети)
	 */
	private $provider;

	/**
	 * Конструктор
	 * @param string $login логин
	 * @param string $provider название провайдера данных пользователя (социальной сети)
	 * @param \selector $queryBuilder конструктор запросов к бд
	 */
	public function __construct($login, $provider, \selector $queryBuilder) {
		$this->login = $login;
		$this->provider = $provider;
		$queryBuilder->flush();
		$this->queryBuilder = $queryBuilder;
	}

	/**
	 * @inheritdoc
	 */
	public function validate() {
		$login = $this->getLogin();
		$provider = $this->getProvider();

		try {
			$queryBuilder = $this->getQueryBuilder();
			$queryBuilder->where('login')->equals($login);
			$queryBuilder->where('loginza')->equals($provider);
			$queryBuilder->where('is_activated')->equals(true);
			$queryResultSet = $queryBuilder->result();
		} catch (\Exception $e) {
			return false;
		}

		if (count($queryResultSet) === 0) {
			return false;
		}

		$queryResultItem = array_shift($queryResultSet);
		return (int) $queryResultItem['id'];
	}

	/**
	 * Возвращает логин
	 * @return string
	 */
	private function getLogin() {
		return (string) $this->login;
	}

	/**
	 * Возвращает название провайдера данных пользователя (социальной сети)
	 * @return string
	 */
	private function getProvider() {
		return (string) $this->provider;
	}
}