<?php
namespace UmiCms\System\Auth\AuthenticationRules;
/**
 * Класс правила аутентификации пользователя по идентификатору
 * @package UmiCms\System\Auth\AuthenticationRules
 */
class UserId extends Rule implements iRule {
	/**
	 * @var int $userId идентификатор пользователя
	 */
	private $userId;

	/**
	 * Конструктор
	 * @param int $userId идентификатор пользователя
	 * @param \selector $queryBuilder конструктор запросов к бд
	 */
	public function __construct($userId, \selector $queryBuilder) {
		$this->userId = $userId;
		$queryBuilder->flush();
		$this->queryBuilder = $queryBuilder;
	}

	/**
	 * @inheritdoc
	 */
	public function validate() {
		$userId = $this->getUserId();

		try {
			$queryBuilder = $this->getQueryBuilder();
			$queryBuilder->where('id')->equals($userId);
			$queryBuilder->limit(0, 1);
			$queryResultSet = $queryBuilder->result();
		} catch (\Exception $e) {
			return false;
		}

		if (count($queryResultSet) === 0) {
			return false;
		}

		$queryResultItem = array_shift($queryResultSet);
		return (int) $queryResultItem['id'];
	}

	/**
	 * Возвращает идентификатор пользователя
	 * @return int
	 */
	private function getUserId() {
		return (int) $this->userId;
	}
}