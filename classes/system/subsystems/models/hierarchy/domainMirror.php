<?php
	/**
	 * Класс зеркала домена
	 */
	class domainMirror extends umiEntinty implements iDomainMirror {
		/**
		 * @var string $host хост зеркала
		 */
		private $host;
		/**
		 * @var int $domainId идентификатор домена, к которому принадлежит зеркало
		 */
		private $domainId;
		/**
		 * @var string $store_type тип сохраняемой сущности для кеширования
		 */
		protected $store_type = 'domain_mirror';

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getHost() {
			return $this->host;
		}

		/**
		 * @inheritdoc
		 * @throws wrongParamException
		 */
		public function setHost($host) {
			if (!is_string($host) || empty($host)) {
				throw new wrongParamException("Wrong domain mirror host given");
			}

			$host = domain::filterHostName($host);

			if ($this->getHost() != $host) {
				$this->host = $host;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return int
		 */
		public function getDomainId() {
			return $this->domainId;
		}

		/**
		 * @inheritdoc
		 * @throws coreException если домена с заданным идентификатором не существует
		 */
		public function setDomainId($id) {
			if (!domainsCollection::getInstance()->isExists($id)) {
				throw new coreException("Domain #{$id} doesn't exist");
			}

			if ($this->getDomainId() != $id) {
				$this->domainId = $id;
				$this->setIsUpdated();
			}

			return true;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		protected function loadInfo($row = false) {
			if ($row === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$escapedId = (int) $this->getId();
				$sql = "SELECT `id`, `host`, `rel` FROM `cms3_domain_mirrows` WHERE `id` = $escapedId";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();
			}

			if (list($id, $host, $rel) = $row) {
				$this->host = (string) $host;
				$this->domainId = (int) $rel;
				return true;
			}

			return false;
		}

		/**
		 * Сохранить внесенные изменения в БД
		 * @return bool
		 */
		protected function save() {
			if (!$this->getIsUpdated()) {
				return true;
			}

			$host = self::filterInputString($this->getHost());
			$escapedId = (int) $this->getId();
			$domainId = (int) $this->getDomainId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
UPDATE `cms3_domain_mirrows`
	SET `host` = '$host', `rel` = $domainId
		WHERE `id` = $escapedId
SQL;
			$connection->query($sql);

			return true;
		}
	}
