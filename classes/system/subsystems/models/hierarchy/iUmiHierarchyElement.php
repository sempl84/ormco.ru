<?php
interface iUmiHierarchyElement extends iUmiEntinty {
	public function getIsDeleted();

	public function setIsDeleted($isDeleted = false);

	public function getIsActive();

	public function setIsActive($isActive = true);

	public function getIsVisible();

	public function setIsVisible($isVisible = true);

	public function getTypeId();

	public function setTypeId($typeId);

	public function getLangId();

	public function setLangId($langId);

	public function getTplId();

	public function setTplId($tplId);

	public function getDomainId();

	public function setDomainId($domainId);

	public function getUpdateTime();

	public function setUpdateTime($timeStamp = 0);

	public function getOrd();

	public function setOrd($ord);

	public function getRel();

	public function setRel($relId);

	/**
	 * Получить объект (класс umiObject), который является источником данных для страницы
	 * @return umiObject объект страницы (ее источник данных)
	 */
	public function getObject();

	public function setObject(umiObject $object, $bNeedSetUpdated = true);

	public function setAltName($altName, $autoConvert = true);

	public function getAltName();

	public function setIsDefault($isDefault = true);

	public function getIsDefault();

	public function getParentId();

	public function getValue($propName, $params = null);

	public function setValue($propName, $propValue);

	public function getFieldId($FieldName);

	public function getName();

	public function setName($name);

	public function getObjectTypeId();

	public function getHierarchyType();

	public function getObjectId();

	public function getModule();

	public function getMethod();
	/**
	 * Проверяет есть ли у страницы виртуальные копии
	 * @return bool результат проверки
	 * @throws Exception
	 */
	public function hasVirtualCopy();
	/**
	 * Проверяет является ли страница "оригинальной" по отношению к ее виртуальным копиям,
	 * то есть является ли страница первой созданной с идентификатором его объекта.
	 * @return bool результат проверки
	 * @throws Exception
	 */
	public function isOriginal();
}
