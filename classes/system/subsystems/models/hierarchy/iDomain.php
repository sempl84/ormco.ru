<?php
/**
 * Интерфейс домена
 */
interface iDomain extends iUmiEntinty {

	/**
	 * Возвращает хост домена
	 * @return string
	 */
	public function getHost();

	/**
	 * Устанавливает хост
	 * @param string $host
	 * @throws wrongParamException если хост невалидный
	 */
	public function setHost($host);

	/**
	 * Проверяет является ли домен доменом по умолчанию
	 * @return bool
	 */
	public function getIsDefault();

	/**
	 * Устанавливает значение флага "по умолчанию" домена.
	 * Служебный метод, в прикладном коде стоит использовать:
	 * domainsCollection::setDefaultDomain()
	 * @param bool $flag значение флага
	 */
	public function setIsDefault($flag);

	/**
	 * Возвращает идентификатор языка по умолчанию
	 * @return int
	 */
	public function getDefaultLangId();

	/**
	 * Устанавливает язык по умолчанию
	 * @param int $id идентификатор языка
	 * @return bool true
	 * @throws coreException если домена с таким идентификатором не существует
	 */
	public function setDefaultLangId($id);

	/**
	 * Создает зеркало домена
	 * @param string $host хост зеркала
	 * @return int идентификатор созданного зеркала
	 * @throws coreException если зеркало с заданным хостом уже существует
	 */
	public function addMirror($host);

	/**
	 * Удаляет зеркало домена с заданным идентификатором
	 * @param int $id идентификатор зеркала
	 * @return bool
	 * @throws coreException если зеркало с заданным id не сущесвует
	 */
	public function delMirror($id);

	/**
	 * Удаляет все зеркала домена
	 * @return bool
	 */
	public function delAllMirrors();

	/**
	 * Возвращает идентификатор зеркала до его хосту
	 * @param string $host хост зеркала
	 * @param bool $checkIdn преобразовывать хост в punycode
	 * @return int|bool
	 */
	public function getMirrorId($host, $checkIdn = true);

	/**
	 * Возвращает зеркало домена по id
	 * @param int $id идентификатор зеркала домена
	 * @return iDomainMirror|bool
	 */
	public function getMirror($id);

	/**
	 * Проверяет существует ли зеркало с заданным идентификатором
	 * @param int $id идентификатор зеркала
	 * @return bool
	 */
	public function isMirrorExists($id);

	/**
	 * Возвращает список зеркал домена
	 * @return iDomainMirror[]
	 */
	public function getMirrorsList();

	/**
	 * Удаляет неподдерживаемые символы из хоста домена
	 * @param string $host
	 * @return string
	 */
	public static function filterHostName($host);

	/**
	 * Возврашает имя текущего домена или зеркала домена, если мы находимся на нем.
	 * @return string
	 */
	public function getCurrentHostName();
}