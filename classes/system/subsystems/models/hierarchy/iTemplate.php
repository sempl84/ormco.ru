<?php
interface iTemplate extends iUmiEntinty {
	/**
	 * Возвращает директорию с ресурсами для шаблона дизайна
	 * @return string
	 */
	public function getResourcesDirectory($httpMode = false);
	/**
	 * Возвращает тип шаблона дизайна
	 * @return string тип шаблона дизайна
	 */
	public function getType();

	/**
	 * Возвращает полный путь к шаблону дизайна
	 * @return string
	 */
	public function getFilePath();
	/**
	 * Возвращает расширение файла шаблона
	 * @return string
	 */
	public function getFileExtension();

	public function getFilename();

	public function setFilename($filename);

	public function getTitle();

	public function setTitle($title);

	public function getDomainId();

	public function setDomainId($domainId);

	public function getLangId();

	public function setLangId($langId);

	public function getIsDefault();

	public function setIsDefault($isDefault);

	public function getUsedPages($limit = 0, $offset = 0);

	public function setUsedPages($elementIdArray);

	public function getTotalUsedPages();

	public function getRelatedPages($limit = 0, $offset = 0);
}