<?php
interface iUmiHierarchy {
	/**
	 * @const int INCREMENT_NEW_PAGE_ORDER приращение порядка вывода новой страницы
	 */
	const INCREMENT_NEW_PAGE_ORDER = 1;

	public function addElement($parentId, $hierarchyTypeId, $name, $altName, $objectTypeId = false, $domainId = false, $langId = false, $templateId = false);

	public function getElement($elementId, $ignorePermissions = false, $ignoreDeleted = false, $row = false);

	public function delElement($elementId);

	public function copyElement($elementId, $newParentId, $copyChildren = false);

	public function cloneElement($elementId, $newParentId, $copySubPages = false);

	public function getDeletedList(&$total = 0, $limit = 20, $page = 0, $searchName = '');

	public function restoreElement($elementId);

	public function killElement($parentId);

	public function removeDeletedElement($parentId, &$removedSoFar = 0);

	public function removeDeletedAll();

	public function getParent($elementId);

	public function getAllParents($elementsId, $selfInclude = false, $ignoreCache = false);

	public function getChildrenTree($rootPageId, $allowInactive = true, $allowInvisible = true, $depth = 0, $hierarchyTypeId = false, $domainId = false, $languageId = false);

	public function getChildrenList($rootPageId, $allowInactive = true, $allowInvisible = true, $hierarchyTypeId = false, $domainId = false, $includeSelf = false, $languageId = false);

	public function getChildrenCount($rootPageId, $allowInactive = true, $allowInvisible = true, $depth = 0, $hierarchyTypeId = false, $domainId = false, $languageId = false, $allowPermissions = false);

	public function getPathById($elementId, $ignoreLang = false, $ignoreIsDefaultStatus = false, $ignoreCache = false, $ignoreUrlSuffix = false);

	public function getIdByPath($elementPath, $showDisabled = false, &$errorsCount = 0, $domainId = false, $langId = false);

	public static function compareStrings($string1, $string2);

	public static function convertAltName($altName, $separator = false);

	public static function getTimeStamp();

	public function getDefaultElementId($langId = false, $domainId = false);

	public function moveBefore($elementId, $parentId, $previousElementId = false);

	public function moveFirst($elementId, $parentId);

	public function getDominantTypeId($elementId, $depth = 1, $hierarchyTypeId = null);

	public function addUpdatedElementId($elementId);

	public function getUpdatedElements();

	public function unloadElement($elementId);

	public function getElementsCount($module, $method = "");

	public function forceAbsolutePath($isForced = true);

	public function getObjectInstances($objectId, $ignoreDomain = false, $ignoreLang = false, $ignoreDeleted = false);

	public function getLastUpdatedElements($limit, $updateTimeStamp = 0);

	public function checkIsVirtual($elements, $includeDeleted = false);

	public function loadElements($elementIds);

	public function getMaxDepth($rootPageId, $maxDepth);

	public function getCurrentLanguageId();

	public function getCurrentDomainId();

	public function sortByHierarchy(array $pageIds);
	/**
	 * Возвращает оригинальную (первую) страницу, связанную с заданным объектом
	 * @param int $objectId идентификатор объекта
	 * @return bool|umiHierarchyElement
	 * @throws Exception
	 */
	public function getOriginalPage($objectId);
}