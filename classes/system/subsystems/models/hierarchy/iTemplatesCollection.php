<?php
interface iTemplatesCollection {
	public function addTemplate($filename, $title, $domainId = false, $langId = false, $isDefault = false);

	public function delTemplate($templateId);

	public function getDefaultTemplate($domainId = false, $langId = false);

	public function setDefaultTemplate($templateId, $domainId = false, $langId = false);

	public function getTemplatesList($domainId, $langId);
	/**
	 * Возвращает список всех шаблонов системы
	 * @return template[]
	 */
	public function getFullTemplatesList();
	public function getTemplate($templateId);
}