<?php
	/**
	 * Коллекция доменов, singleton.
	 */
	class domainsCollection extends singleton implements iDomainsCollection {
		/**
		 * @var iDomain[] $domains список доменов системы
		 */
		private $domains = [];
		/**
		 * @var iDomain|false $defaultDomain домен по умолчанию
		 */
		private $defaultDomain;

		/**
		 * @inheritdoc
		 * @return iDomainsCollection
		 */
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * @inheritdoc
		 * @return int
		 * @throws coreException
		 * @throws wrongParamException
		 */
		public function addDomain($host, $languageId, $isDefault = false) {
			if ($domain_id = $this->getDomainId($host)) {
				throw new coreException("Domain #{$host} already exist.");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->startTransaction("Create domain {$host}");

			try {
				$sql = "INSERT INTO `cms3_domains` VALUES (null, '%s', %d, %d)";
				$sql = sprintf($sql, $host,(int) $isDefault, $languageId);
				$connection->query($sql);

				$domain_id = $connection->insertId();

				$domain = new domain($domain_id);
				$domain->setHost($host);
				$domain->setIsDefault($isDefault);
				$domain->setDefaultLangId($languageId);
				$domain->commit();

			} catch (Exception $e) {
				$connection->rollbackTransaction();
				throw $e;
			}

			$connection->commitTransaction();
			$this->setDomain($domain);

			if ($isDefault) {
				$this->setDefaultDomain($domain_id);
			}

			$cache = cacheFrontend::getInstance();
			$cache->save($domain, 'domain');
			$cache->deleteKey('domains_list');

			return $domain_id;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws coreException
		 */
		public function delDomain($id) {
			if (!$this->isExists($id)) {
				throw new coreException("Domain #{$id} doesn't exist.");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->startTransaction("Delete domain #{$id}");

			try {
				$domain = $this->getDomain($id);
				$domain->delAllMirrors();

				if ($domain->getIsDefault()) {
					$this->defaultDomain = false;
				}

				$escapedId = (int) $id;
				$sql = "DELETE FROM `cms3_hierarchy` WHERE `domain_id` = $escapedId";
				$connection->query($sql);

				$sql = "DELETE FROM `cms3_domains` WHERE `id` = $escapedId";
				$connection->query($sql);
			} catch (Exception $e) {
				$connection->rollbackTransaction();
				throw $e;
			}

			$connection->commitTransaction();
			$this->unsetDomain($domain);
			unset($domain);

			$cache = cacheFrontend::getInstance();
			$cache->del($escapedId, 'domain');
			$cache->deleteKey('domains_list');

			return true;
		}

		/**
		 * @inheritdoc
		 * @return iDomain|bool
		 */
		public function getDomain($id) {
			return $this->isExists($id) ? $this->domains[$id] : false;
		}

		/**
		 * @inheritdoc
		 * @return iDomain|bool
		 */
		public function getDefaultDomain() {
			return ($this->defaultDomain instanceof iDomain) ? $this->defaultDomain : false;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws coreException
		 */
		public function setDefaultDomain($id) {
			if (!$this->isExists($id)) {
				throw new coreException("Domain #{$id} doesn't exist.");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->startTransaction("Set default domain #{$id}");

			try {
				$sql = "UPDATE `cms3_domains` SET `is_default` = '0' WHERE `is_default` = '1'";
				$connection->query($sql);

				$oldDefaultDomain = $this->getDefaultDomain();

				if ($oldDefaultDomain instanceof iDomain) {
					$oldDefaultDomain->setIsDefault(false);
					$oldDefaultDomain->commit();
				}

				$newDefaultDomain = $this->getDomain($id);
				$newDefaultDomain->setIsDefault(true);
				$newDefaultDomain->commit();

				$this->defaultDomain = $newDefaultDomain;
			} catch (Exception $e) {
				$connection->rollbackTransaction();
				throw $e;
			}

			$connection->commitTransaction();

			$cache = cacheFrontend::getInstance();
			$cache->deleteKey('domains_list');

			return true;
		}


		/**
		 * @inheritdoc
		 * @return iDomain[]
		 */
		public function getList() {
			return $this->domains;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		public function isExists($id) {
			if (!is_string($id) && !is_int($id)) {
				return false;
			}

			return array_key_exists($id, $this->getList());
		}

		/**
		 * @inheritdoc
		 * @return int|bool
		 */
		public function getDomainId($host, $useMirrors = true, $checkIdn = true) {
			if (!is_string($host) || empty($host)) {
				return false;
			}

			foreach ($this->getList() as $domain) {
				if ($domain->getHost() == $host) {
					return $domain->getId();
				}

				if ($useMirrors) {
					$mirrorId = $domain->getMirrorId($host);

					if ($mirrorId !== false) {
						return $domain->getId();
					}
				}
			}

			if ($checkIdn && idn_to_ascii($host) != idn_to_utf8($host)) {
				$host = ($host == idn_to_ascii($host)) ? idn_to_utf8($host) : idn_to_ascii($host);
				return $this->getDomainId($host, $useMirrors, false);
			}

			return false;
		}

		/**
		 * @inheritdoc
		 * @return bool|int
		 */
		public function getDomainIdByUrl($url) {
			if (!is_string($url) || empty($url)) {
				return false;
			}

			if (!preg_match('/^https?:\/\/([^\/]*)/', $url, $matches)) {
				return cmsController::getInstance()
					->getCurrentDomain()
					->getId();
			}

			$host = $matches[1];

			return $this->getDomainId($host);
		}

		/**
		 * @inheritdoc
		 */
		public function clearCache() {
			$domains = $this->getList();

			foreach ($domains as $domain) {
				$this->unsetDomain($domain);
				unset($domain);
			}

			unset($domains);
			$this->loadDomains();
		}
		
		/**
		 *  @inheritdoc
		 *  @return bool
		 */
		public function isDefaultDomain($host = null) {
			$mainHost = idn_to_ascii($this->getDefaultDomain()->getHost());
			$host = ($host) ? $host : $_SERVER['HTTP_HOST'];
			$host = idn_to_ascii($host);

			$hostList = [$mainHost, 'www.' . $mainHost];

			if (in_array($host, $hostList)) {
				return true;
			}

			return false;
		}

		/**
		 * Конструктор
		 */
		protected function __construct() {
			$this->loadDomains();
		}

		/**
		 * Удаляет домен из списка загруженны доменов
		 * @param iDomain $domain
		 * @return $this
		 */
		private function unsetDomain(iDomain $domain) {
			unset($this->domains[$domain->getId()]);
			return $this;
		}

		/**
		 * Добавляет домен в список загруженных доменов
		 * @param iDomain $domain
		 * @return $this
		 */
		private function setDomain(iDomain $domain) {
			$this->domains[$domain->getId()] = $domain;
			return $this;
		}

		/**
		 * Загружает список доменов
		 * @return bool
		 * @throws Exception
		 */
		private function loadDomains() {
			$cacheFrontend = cacheFrontend::getInstance();
			$connection = ConnectionPool::getInstance()->getConnection();
			$domainIds = $cacheFrontend->loadData('domains_list');

			if (!is_array($domainIds)) {
				$sql = "SELECT `id`, `host`, `is_default`, `default_lang_id` FROM `cms3_domains`";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);

				$domainIds = array();

				foreach ($result as $row) {
					$domainIds[$row[0]] = $row;
				}

				$cacheFrontend->saveData('domains_list', $domainIds, 3600);
			}

			foreach ($domainIds as $domain_id => $row) {
				$domain = $cacheFrontend->load($domain_id, 'domain');

				if ($domain instanceof domain == false) {
					try {
						$domain = new domain($domain_id, $row);
					} catch(privateException $e) {
						continue;
					}

					$cacheFrontend->save($domain, 'domain');
				}

				$this->setDomain($domain);

				if ($domain->getIsDefault()) {
					$this->defaultDomain = $domain;
				}
			}

			return true;
		}
	}
