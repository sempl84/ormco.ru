<?php
	/**
	 * Класс домена, содержит список зеркал
	 */
	class domain extends umiEntinty implements iDomain {
		/**
		 * @var string $host хост домена
		 */
		private	$host;
		/**
		 * @var int $defaultLanguageId идентификатор языка по умолчанию
		 */
		private $defaultLanguageId;
		/**
		 * @var iDomainMirror[] $mirrors список зеркал домена
		 */
		private $mirrors = [];
		/**
		 * @var bool $isDefaultFlag является ли домен основным
		 */
		private $isDefaultFlag;
		/**
		 * @var string $store_type тип сохраняемой сущности для кеширования
		 */
		protected $store_type = "domain";

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getHost() {
			return $this->host;
		}

		/**
		 * @inheritdoc
		 * @throws wrongParamException
		 */
		public function setHost($host) {
			if (!is_string($host) || empty($host)) {
				throw new wrongParamException("Wrong domain host given");
			}

			$host = self::filterHostName($host);

			if ($this->getHost() != $host) {
				$this->host = $host;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		public function getIsDefault() {
			return $this->isDefaultFlag;
		}

		/**
		 * @inheritdoc
		 */
		public function setIsDefault($flag) {
			$flag = (bool) $flag;

			if ($this->getIsDefault() != $flag) {
				$this->isDefaultFlag = $flag;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return int
		 */
		public function getDefaultLangId() {
			return $this->defaultLanguageId;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws coreException
		 */
		public function setDefaultLangId($id) {
			if (!langsCollection::getInstance()->isExists($id)) {
				throw new coreException("Language #{$id} doesn't exist");
			}

			if ($this->getDefaultLangId() != $id) {
				$this->defaultLanguageId = $id;
				$this->setIsUpdated();
			}

			return true;
		}

		/**
		 * @inheritdoc
		 * @return int
		 * @throws coreException
		 */
		public function addMirror($host) {
			if ($mirrorId = $this->getMirrorId($host)) {
				throw new coreException("Domain mirror #{$host} already exist.");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->startTransaction("Create domain mirror {$host}");

			try {
				$escapedId = (int) $this->getId();
				$sql = "INSERT INTO `cms3_domain_mirrows` (`rel`) VALUES ($escapedId)";
				$connection->query($sql);

				$mirrorId = $connection->insertId();

				$mirror = new domainMirror($mirrorId);
				$mirror->setHost($host);
				$mirror->setDomainId($escapedId);
				$mirror->commit();
			} catch (Exception $e) {
				$connection->rollbackTransaction();
				throw $e;
			}

			$connection->commitTransaction();
			$this->mirrors[$mirrorId] = $mirror;

			return $mirrorId;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws coreException если зеркало с заданным id не сущесвует
		 */
		public function delMirror($id) {
			if (!$this->isMirrorExists($id)) {
				throw new coreException("Domain mirror #{$id} doesn't exist.");
			}

			$escapedId = (int) $id;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms3_domain_mirrows WHERE id = $escapedId";
			$connection->query($sql);

			unset($this->mirrors[$escapedId]);
			return true;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		public function delAllMirrors() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$escapedId = (int) $this->getId();
			$sql = "DELETE FROM cms3_domain_mirrows WHERE rel = $escapedId";
			$connection->query($sql);

			return true;
		}

		/**
		 * @inheritdoc
		 * @return int|bool
		 */
		public function getMirrorId($host, $checkIdn = true) {
			if (!is_string($host) || empty($host)) {
				return false;
			}

			foreach ($this->getMirrorsList() as $mirror) {
				if ($mirror->getHost() == $host) {
					return $mirror->getId();
				}
			}

			if ($checkIdn && idn_to_ascii($host) != idn_to_utf8($host)) {
				$host = ($host == idn_to_ascii($host)) ? idn_to_utf8($host) : idn_to_ascii($host);
				return $this->getMirrorId($host, false);
			}

			return false;
		}

		/**
		 * @inheritdoc
		 * @return iDomainMirror|bool
		 */
		public function getMirror($id) {
			return ($this->isMirrorExists($id)) ? $this->mirrors[$id] : false;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		public function isMirrorExists($id) {
			if (!is_string($id) && !is_int($id)) {
				return false;
			}

			return array_key_exists($id, $this->mirrors);
		}

		/**
		 * @inheritdoc
		 * @return iDomainMirror[]
		 */
		public function getMirrorsList() {
			return $this->mirrors;
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public static function filterHostName($host) {
			return preg_replace("/([^A-z0-9\-А-яёЁ\.:]+)|[\^_\\\\]/u", "", $host);
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getCurrentHostName() {
			$host = getServer('HTTP_HOST');

			if (strpos($host, 'xn--') === 0) {
				$convert = new idna_convert();
				$host = $convert->decode($host);
			}

			if ($this->getHost() != $host) {
				$mirrorId = $this->getMirrorId($host, false);

				if ($mirrorId !== false) {
					return $this->getMirror($mirrorId)
						->getHost();
				}
			}

			return $this->getHost();
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws Exception
		 */
		protected function save() {
			if (!$this->getIsUpdated()) {
				return true;
			}

			$host = self::filterInputString($this->host);
			$isDefaultFlag = (int) $this->isDefaultFlag;
			$defaultLanguageId = (int) $this->defaultLanguageId;
			$escapedId = (int) $this->getId();
			$connection = ConnectionPool::getInstance()->getConnection();

			$sql = <<<SQL
UPDATE `cms3_domains`
	SET `host` = '{$host}', `is_default` = $isDefaultFlag, `default_lang_id` = $defaultLanguageId
		WHERE `id` = $escapedId
SQL;

			$connection->query($sql);
			return true;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		protected function loadInfo($row = false) {
			if ($row === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$escapedId = (int) $this->getId();
				$sql = <<<SQL
SELECT `id`, `host`, `is_default`, `default_lang_id` FROM `cms3_domains` WHERE `id` = $escapedId
SQL;
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();
			}

			if (list($id, $host, $isDefaultFlag, $defaultLanguageId) = $row) {
				$this->host = (string) $host;
				$this->isDefaultFlag = (bool) $isDefaultFlag;
				$this->defaultLanguageId = (int) $defaultLanguageId;
				return $this->loadMirrors();
			}

			return false;
		}

		/**
		 * Загружает список зеркал домена
		 * @return bool
		 */
		private function loadMirrors() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$escapedId = (int) $this->getId();
			$sql = "SELECT `id`, `host`, `rel` FROM `cms3_domain_mirrows` WHERE `rel` = $escapedId";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($mirrorId) = $row;

				try {
					$this->mirrors[$mirrorId] = new domainMirror($mirrorId, $row);
				} catch (privateException $e) {
					continue;
				}
			}

			return true;
		}

		/**
		 * @deprecated
		 */
		public function addMirrow($host) {
			return $this->addMirror($host);
		}

		/**
		 * @deprecated
		 */
		public function delMirrow($id) {
			return $this->delMirror($id);
		}

		/**
		 * @deprecated
		 */
		public function delAllMirrows() {
			return $this->delAllMirrors();
		}

		/**
		 * @deprecated
		 */
		public function getMirrowId($host) {
			return $this->getMirrorId($host);
		}

		/**
		 * @deprecated
		 */
		public function getMirrow($id) {
			return $this->getMirror($id);
		}

		/**
		 * @deprecated
		 */
		public function isMirrowExists($id) {
			return $this->isMirrorExists($id);
		}

		/**
		 * @deprecated
		 */
		public function getMirrowsList() {
			return $this->getMirrorsList();
		}
	}
