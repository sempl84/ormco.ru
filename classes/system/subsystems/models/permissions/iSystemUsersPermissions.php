<?php
namespace UmiCms\System\Permissions;
/**
 * Интерфейс класса прав системных пользователей
 * @package UmiCms\System\Permissions
 */
interface iSystemUsersPermissions {
	/**
	 * @const string SV_USER_GUID гуид супервайзера
	 */
	const SV_USER_GUID = 'system-supervisor';
	/**
	 * @const string SV_GROUP_GUID гуид группы супервайзеров
	 */
	const SV_GROUP_GUID = 'users-users-15';
	/**
	 * @const string GUEST_USER_GUID гуид гостя
	 */
	const GUEST_USER_GUID = 'system-guest';
	/**
	 * Конструктор
	 * @param \iUmiObjectsCollection $umiObjects коллекция объектов
	 */
	public function __construct(\iUmiObjectsCollection $umiObjects);
	/**
	 * Возвращает идентификатор супервайзера
	 * @return int|bool
	 */
	public function getSvUserId();
	/**
	 * Возвращает идентификатор группы супервайзеров
	 * @return int|bool
	 */
	public function getSvGroupId();
	/**
	 * Возвращает идентификатор гостя
	 * @return int|bool
	 */
	public function getGuestUserId();
}