<?php
	interface iPermissionsCollection {
		public function getOwnerType($ownerId);
		public function makeSqlWhere($ownerId, $ignoreSelf = false);

		public function isAllowedModule($ownerId, $module);
		public function isAllowedMethod($ownerId, $module, $method, $ignoreSelf = false);
		public function isAllowedObject($ownerId, $objectId, $resetCache = false);
		public function isSv($userId = false);
		public function isAdmin($userId = false, $ignoreCache = false);
		public function isOwnerOfObject($objectId, $userId = false);

		public function resetElementPermissions($elementId, $ownerId = false);
		public function resetModulesPermissions($ownerId, $modules = NULL);
		
		public function setElementPermissions($ownerId, $elementId, $level);
		public function setModulesPermissions($ownerId, $module, $method = false, $cleanupPermissions = true);

		public function setDefaultPermissions($elementId);

		public function hasUserPermissions($ownerId);
		
		public function copyHierarchyPermissions($fromOwnerId, $toOwnerId);

		public function setAllElementsDefaultPermissions($ownerId);
		
		public function getUsersByElementPermissions($elementId, $level = 1);
		
		public function pushElementPermissions($elementId, $level = 1);
		
		public function cleanupBasePermissions();
		
		public function isAuth();
	};
?>