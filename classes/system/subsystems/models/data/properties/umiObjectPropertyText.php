<?php

	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Текст".
	 */
	class umiObjectPropertyText extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['text_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = (string) $val;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT text_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = (string) $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $val) {
				if ($val == "<p />" || $val == "&nbsp;") {
					$val = "";
				}

				$val = self::filterInputString($val);
				$sql = "INSERT INTO {$this->tableName} (
							obj_id, field_id, text_val
						) VALUES(
							'{$this->object_id}', '{$this->field_id}', '{$val}'
						)";
				$connection->query($sql);
			}
		}

		public function __wakeup() {
			foreach ($this->value as $i => $v) {
				if (is_string($v)) {
					$this->value[$i] = str_replace("&#037;", "%", $v);
				}
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = '';
			} else {
				$oldValue = strval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = '';
			} else {
				$newValue = ($newValue === "<p />" || $newValue === "&nbsp;") ? '' : $newValue;
				$newValue = strval($newValue[0]);
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
