<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Выпадающий список", т.е. свойства с использованием справочников.
	 */
	class umiObjectPropertyRelation extends umiObjectProperty {

		/** @const разделитель идентификторов в строке */
		const DELIMITER_ID = ',';

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['rel_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = $val;
				}
				return $res;
			}

			if ($this->getIsMultiple()) {
				$sql = "SELECT rel_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}'";
			} else {
				$sql = "SELECT rel_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();

			if (is_null($this->value)) {
				return;
			}

			$tmp = [];

			foreach ($this->value as $val) {
				if (!$val) {
					continue;
				}

				if (is_string($val) && strpos($val, "|") !== false) {
					$tmp1 = explode("|", $val);
					foreach ($tmp1 as $v) {
						$v = trim($v);
						if ($v) {
							$tmp[] = $v;
						}
						unset($v);
					}
					unset($tmp1);
					//Check, if we can use it without fieldTypeId
					$this->getField()->setFieldTypeId(umiFieldTypesCollection::getInstance()->getFieldTypeByDataType('relation', 1)->getId());
				} else {
					$tmp[] = $val;
				}
			}

			$this->value = $tmp;
			unset($tmp);
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $key => $val) {
				if ($val) {
					$val = $this->prepareRelationValue($val);
					$this->values[$key] = $val;
				}
				if (!$val) {
					continue;
				}

				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, rel_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			$oldValue = $this->normaliseValue($oldValue);
			$newValue = $this->normaliseValue($newValue);

			if (count($oldValue) !== count($newValue)) {
				return true;
			}

			if (!$this->getIsMultiple()) {
				if (!isset($oldValue[0])) {
					$oldValue = null;
				} else {
					$oldValue = $oldValue[0];
				}

				if (!isset($newValue[0])) {
					$newValue = null;
				} else {
					$newValue = $newValue[0];
				}

				return ($oldValue === $newValue) ? false : true;
			}

			foreach ($newValue as $newValueRel) {
				if (!in_array($newValueRel, $oldValue)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Приводит значение поля типа "выпадающий список" к определенному формату, для сравнения.
		 * Возвращает результат форматирования.
		 * @param array $values значение поля типа "выпадающий список"
		 * @return array
		 */
		private function normaliseValue(array $values) {
			if (count($values) == 0) {
				return $values;
			}

			$normalisedValues = [];

			foreach ($values as $value) {
				switch (true) {
					case $value instanceof umiEntinty: {
						$normalisedValues[] = (int) $value->getId();
						break;
					}
					case is_numeric($value): {
						$normalisedValues[] = intval($value);
						break;
					}
					case is_string($value): {
						$normalisedValues[] = strval($value);
						break;
					}
				}
			}

			return $normalisedValues;
		}
	}

?>
