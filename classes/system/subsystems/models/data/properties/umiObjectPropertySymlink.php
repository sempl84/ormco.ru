<?php

	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Ссылка на дерево".
	 */
	class umiObjectPropertySymlink extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;
			$umiHierarchy = umiHierarchy::getInstance();

			if ($data = $this->getPropData()) {
				$umiHierarchy->loadElements($data['tree_val']);

				foreach ($data['tree_val'] as $val) {
					if (is_null($val)) {
						continue;
					}

					$element = $umiHierarchy->getElement((int) $val);

					if ($element === false || !$element->getIsActive()) {
						continue;
					}

					$res[] = $element;
				}

				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT tree_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$element = $umiHierarchy->getElement((int) $val);

				if ($element === false || !$element->getIsActive()) {
					continue;
				}

				$res[] = $element;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();
			$hierarchy = umiHierarchy::getInstance();
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $i => $val) {
				if (is_object($val)) {
					$val = (int) $val->getId();
				} else {
					$val = intval($val);
				}

				if (!$val) {
					continue;
				}

				$this->value[$i] = $hierarchy->getElement($val);
				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, tree_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			$oldValue = $this->normaliseValue($oldValue);
			$newValue = $this->normaliseValue($newValue);

			if (count($oldValue) !== count($newValue)) {
				return true;
			}

			foreach ($newValue as $newValueTag) {
				if (!in_array($newValueTag, $oldValue)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Приводит значение поля типа "ссылка на дерево" к определенному формату, для сравнения.
		 * Возвращает результат форматирования.
		 * @param array $values значение поля типа "ссылка на дерево""
		 * @return array
		 */
		private function normaliseValue(array $values) {
			if (count($values) == 0) {
				return $values;
			}

			$normalisedValues = [];

			foreach ($values as $value) {
				switch (true) {
					case $value instanceof umiEntinty: {
						$normalisedValues[] = (int) $value->getId();
						break;
					}
					case is_numeric($value): {
						$normalisedValues[] = intval($value);
						break;
					}
				}
			}

			return $normalisedValues;
		}
	}

?>
