<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Дата"
	 */
	class umiObjectPropertyDate extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['int_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = new umiDate((int) $val);
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT int_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = new umiDate((int) $val);
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $val) {
				if ($val === false || $val === "") {
					continue;
				} else {
					$val = (is_object($val)) ? (int) $val->timestamp : (int) $val;
					if ($val == false) {
						continue;
					}
				}

				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, int_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inherit
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0]) || !$oldValue[0] instanceof umiDate) {
				$oldValue = new umiDate(0);
			} else {
				$oldValue = $oldValue[0]->getDateTimeStamp();
			}

			if (!isset($newValue[0]) || !$newValue[0] instanceof umiDate) {
				$newValue = new umiDate(0);
			} else {
				$newValue = $newValue[0]->getDateTimeStamp();
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
