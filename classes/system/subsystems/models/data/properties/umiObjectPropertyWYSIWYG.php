<?php

	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "WYSIWYG".
	 */
	class umiObjectPropertyWYSIWYG extends umiObjectPropertyText {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['text_val'] as $val) {
					if (is_null($val)) {
						continue;
					}

					if (str_replace("&nbsp;", "", trim($val)) == "") {
						continue;
					}

					$res[] = (string) $val;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT text_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				if (str_replace("&nbsp;", "", trim($val)) == "") {
					continue;
				}

				$res[] = (string) $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			foreach ($this->value as $i => $value) {
				$value = str_replace(['&lt;!--', '--&gt;'], ['<!--', '-->'], $value);
				$value = preg_replace('/<!--\[if(.*?)>(.*?)<!(-*)\[endif\][\s]*-->/mis', '', $value);
				$this->value[$i] = $value;
			}
			parent::saveValue();
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = '';
			} else {
				$oldValue = strval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = '';
			} else {
				$newValue = strval($newValue[0]);
				$newValue = str_replace(['&lt;!--', '--&gt;'], ['<!--', '-->'], $newValue);
				$newValue = preg_replace('/<!--\[if(.*?)>(.*?)<!(-*)\[endif\][\s]*-->/mis', '', $newValue);
				$newValue = ($newValue === "<p />" || $newValue === "&nbsp;") ? '' : $newValue;
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
