<?php

	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Ссылка на тип данных".
	 */
	class umiObjectPropertyLinkToObjectType extends umiObjectPropertyInt {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			return parent::loadValue();
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			return parent::saveValue();
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = 0;
			} else {
				$oldValue = intval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = 0;
			} else {
				$newValue = intval($newValue[0]);
			}

			$umiObjectsTypes = umiObjectTypesCollection::getInstance();

			if (!$umiObjectsTypes->getType($newValue) instanceof iUmiObjectType) {
				return false;
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
