<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Кнопка-флажок" (булевый тип)
	 */
	class umiObjectPropertyBoolean extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['int_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = (int) $val;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT int_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = (int) $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $val) {
				if (!$val) {
					continue;
				}

				$val = (int) $this->boolval($val, true);
				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, int_val) VALUES ('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * TODO PHPDoc
		 * Возврашает булевое значение входного параметра $in, распознавая, в том числе
		 * Yes, no, 'false' и т.д.
		 * @param Mixed $in проверяемое значение
		 * @param Boolean $strict
		 * @return boolean
		 */
		protected function boolval($in, $strict = false) {
			$out = null;
			// if not strict, we only have to check if something is false
			if (in_array($in, ['false', 'False', 'FALSE', 'no', 'No', 'n', 'N', '0', 'off', 'Off', 'OFF', false, 0, null], true)) {
				$out = false;
			} else {
				if ($strict) {
					// if strict, check the equivalent true values
					if (in_array($in, ['true', 'True', 'TRUE', 'yes', 'Yes', 'y', 'Y', '1', 'on', 'On', 'ON', true, 1], true)) {
						$out = true;
					}
				} else {
					// not strict? let the regular php bool check figure it out (will largely default to true)
					$out = ($in ? true : false);
				}
			}

			return $out;
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = false;
			} else {
				$oldValue = $this->boolval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = false;
			} else {
				$newValue = $this->boolval($newValue[0]);
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
