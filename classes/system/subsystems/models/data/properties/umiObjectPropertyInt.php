<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Число"
	 */
	class umiObjectPropertyInt extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['int_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = (int) $val;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT int_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = (int) $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();
			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $val) {
				if ($val === false || $val === "") {
					continue;
				}
				$val = (int) $val;
				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, int_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = null;
			} else {
				$oldValue = intval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = null;
			} else {
				$newValue = intval($newValue[0]);
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
