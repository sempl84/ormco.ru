<?php
	/**
	 * Общий класс для взаимодействия с объектами системы.
	 * TODO Check and format all PHPDoc's
	 */
	class umiObject extends umiEntinty implements iUmiEntinty, iUmiObject {

		private $name;
		private $type_id;
		private $is_locked;
		private $owner_id = false;
		private $properties = array();
		private $invertedProperties = array();
		private $type;
		private $prop_groups = array();
		private $guid = null;
		private $type_guid = null;
		private $updateTime = null;
		private $ord = null;
		protected $store_type = "object";

		/**
		 * Получить название объекта
		 * @param Boolean $translate_ignored = false
		 * @return String название объекта
		 */
		public function getName($translate_ignored = false) {
			$this->name = umiObjectProperty::filterOutputString($this->name);
			return $translate_ignored ? $this->name : $this->translateLabel($this->name);
		}

		/**
		 * Получить id типа объекта
		 * @return Integer id типа объекта (для класса umiObjectType)
		 */
		public function getTypeId() {
			return $this->type_id;
		}

		/**
		 * Получить guid типа объекта
		 * @return String guid типа объекта (для класса umiObjectType)
		 */
		public function getTypeGUID(){
			return $this->type_guid;
		}

		/**
		 * Возвращает тип объекта
		 * @return umiObjectType
		 * @throws coreException
		 */
		public function getType() {
			if (!$this->type) {
				$type = umiObjectTypesCollection::getInstance()->getType($this->type_id);
				if (!$type) {
					throw new coreException("Can't load type in object's init");
				}
				$this->type = $type;
			}
			return $this->type;
		}

		/**
		 * Узнать, заблокирован ли объект. Метод зарезервирован, но не используется. Предполагается, что этот флаг будет блокировать любое изменение объекта
		 * @return Boolean true если обект заблокирован
		 */
		public function getIsLocked() {
			return $this->is_locked;
		}

		/**
		 * Задать новое название объекта. Устанавливает флаг "Модифицирован".
		 * @param String $name
		 */
		public function setName($name) {
			if ($this->getName() != $name) {
				$name = preg_replace('/([\x01-\x08]|[\x0B-\x0C]|[\x0E-\x1F])/', '', $name);
				$name = $this->translateI18n($name, "object-");
				$this->name = $name;
				$this->setIsUpdated();
			}
		}

		/**
		 * Устанавливает время изменения объекта
		 * @param int $updateTime время в формате Unix timestamp
		 * @return bool
		 */
		public function setUpdateTime($updateTime) {
			$updateTimeStamp = (int) $updateTime;

			if ($this->getUpdateTime() !== $updateTimeStamp) {
				$this->updateTime = $updateTimeStamp;
				$this->setIsUpdated(true, false);
			}

			return true;
		}

		/**
		 * Возвращает время последнего изменения объекта
		 * @return mixed
		 */
		public function getUpdateTime() {
			return $this->updateTime;
		}

		/**
		 * Возвращает значение индекса сортировки объекта
		 * @return int
		 */
		public function getOrder() {
			return $this->ord;
		}

		/**
		 * Устанавливает значение индекса сортировки объекта
		 * @param int $order значение индекса сортировки
		 * @return bool
		 */
		public function setOrder($order) {
			$order = (int) $order;

			if ($this->getOrder() !== $order) {
				$this->ord = $order;
				$this->setIsUpdated();
			}

			return true;
		}

		/**
		 * Установить новый id типа данных (класс umiObjectType) для объекта.
		 * Используйте этот метод осторожно, потому что он просто переключает id типа данных.
		 * Уже заполненные значения остануться в БД, но станут недоступны через API, если не переключить тип данных для объекта назад.
		 * Устанавливает флаг "Модифицирован".
		 * @return Boolean true всегда
		 */
		public function setTypeId($type_id) {
			if ($this->getTypeId() !== $type_id) {
				$this->type_id = $type_id;
				$this->setIsUpdated();
			}
			return true;
		}

		/**
		 * Выставить объекту статус "Заблокирован". Этот метод зарезервирован, но в настоящее время не используется.
		 */
		public function setIsLocked($is_locked) {
			$is_locked = (bool) $is_locked;

			if ($this->getIsLocked() !== $is_locked) {
				$this->is_locked = $is_locked;
				$this->setIsUpdated(true, false);
			}
		}

		/**
		 * Установить id владельца объекта. Это означает, что пользователь с id $ownerId полностью владеет этим объектом:
		 * создал его, может модифицировать, либо удалить.
		 * @param Integer $ownerId id нового владельца. Обязательно действительный id объекта (каждый пользователь это объект в umi)
		 * @return Boolean true в случае успеха, false если $ownerId не является нормальным id для umiObject
		 */
		public function setOwnerId($ownerId) {
			if (!umiObjectsCollection::getInstance()->isExists($ownerId)) {
				return false;
			}

			if ($this->getOwnerId() !== $ownerId) {
				$this->owner_id = $ownerId;
				$this->setIsUpdated();
			}

			return true;
		}

		/**
		 * Получить id пользователя, который владеет этим объектом
		 * @return Integer id пользователя. Всегда действительный id для umiObject или NULL если не задан.
		 */
		public function getOwnerId() {
			return $this->owner_id;
		}

		/**
		 * Проверить, заполены ли все необходимые поля у объекта
		 * @return Boolean
		 */
		public function isFilled() {
			$fields = $this->getType()->getAllFields();
			/**
			 * @var iUmiField[] $fields
			 */
			foreach ($fields as $field)
				if ($field->getIsRequired() && is_null($this->getValue($field->getName())))
					return false;
			return true;
		}

		/**
		 * Сохраняет изменения объекта и его свойств
		 * @return bool
		 * @throws Exception
		 */
		protected function save() {
			if (!$this->getIsUpdated()) {
				return true;
			}

                        $this->log_order_change();

			$ignoreI18n = true;
			$name = umiObjectProperty::filterInputString($this->getName($ignoreI18n));
			$name = $name ? "'$name'" : "NULL";
			$typeId = (int) $this->getTypeId();
			$isLocked = (int) $this->getIsLocked();
			$ownerId = (int) $this->getOwnerId();
			$guid = umiObjectProperty::filterInputString($this->getGUID());
			$updateTime = is_null($this->getUpdateTime()) ? 'NULL' : $this->getUpdateTime();
			$ord = (int) $this->getOrder();
			$objectId = (int) $this->getId();

			$connection = ConnectionPool::getInstance()
				->getConnection();
			$connection->startTransaction("Saving object #$objectId");

			$sql = <<<QUERY
UPDATE `cms3_objects`
SET `name` = $name, `type_id` = $typeId, `is_locked` = $isLocked, `owner_id` = $ownerId, `guid` = '$guid',
`updatetime` = $updateTime, `ord` = $ord
WHERE `id` = $objectId
QUERY;
			try {
				$connection->query($sql);

				$transactionModeEnabled = umiObjectProperty::isTransactionModeEnabled();

				if ($transactionModeEnabled) {
					umiObjectProperty::disableTransactionMode();
				}

				foreach ($this->getProperties() as $prop) {
					if ($prop instanceof iUmiObjectProperty && $prop->getIsUpdated()) {
						$prop->commit();
					}
				}

				if ($transactionModeEnabled) {
					umiObjectProperty::enableTransactionMode();
				}

			} catch (Exception $e) {
				$connection->rollbackTransaction();
				throw $e;
			}

			$connection->commitTransaction();

			return $this->setIsUpdated(false, false);
		}

		/** @inheritdoc */
		public function __construct($id, $row = false) {
			parent::__construct($id, $row);
		}

		/**
		 * Загружает необходимые данные для формирования объекта. Этот метод не подгружает значения свойств.
		 * Значения свойств запрашиваются по требованию
		 * В случае нарушения целостности БД, когда с загружаемым объектом в базе не связан ни один тип данных, объект удаляется.
		 * @param array|bool $row массив с информацией о сущности
		 * @return bool true в случае успеха
		 * @throws coreException
		 */
		protected function loadInfo($row = false) {
			if ($row === false || count($row) < 6) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$sql = <<<QUERY
SELECT o.name,
       o.type_id,
       o.is_locked,
       o.owner_id,
       o.guid AS `guid`,
       t.guid AS `type_guid`,
       o.updatetime,
       o.ord
FROM   cms3_objects `o`,
       cms3_object_types `t`
WHERE  o.id = '{$this->id}'
       AND o.type_id = t.id
QUERY;
				$result = $connection->queryResult($sql, true);

				if ($connection->errorOccurred()) {
					cacheFrontend::getInstance()->del($this->id, "object");
					throw new coreException($connection->errorDescription($sql));
				}

				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();

				if (!$row) {
					throw new coreException("Object #{$this->id} doesn't exist");
				}
			}

			list($name, $type_id, $is_locked, $owner_id, $guid, $type_guid, $updateTime, $ord) = $row;
			if (!$type_id) {
				//Foregin keys check failed, or manual queries made. Delete this object.
				umiObjectsCollection::getInstance()->delObject($this->id);
				return false;
			}

			$this->name = $name;
			$this->type_id = (int) $type_id;
			$this->is_locked = (bool) $is_locked;
			$this->owner_id = (int) $owner_id;
			$this->guid = $guid;
			$this->type_guid = $type_guid;
			$this->updateTime = $updateTime;
			$this->ord = (int) $ord;
			return $this->loadFields();
		}

		/**
		 * Загружает поля объекта
		 * @return bool
		 */
		public function loadFields() {
			$umiTypesHelper = umiTypesHelper::getInstance();
			$typeId = $this->getTypeId();
			$fields = $umiTypesHelper->getFieldsByObjectTypeIds($typeId);

			if (isset($fields[$typeId])) {
				$fields = $fields[$typeId];
				$this->invertedProperties = $fields;
				$this->properties = array_flip($fields);
			}

			return true;
		}

		/**
		 * Получить свойство объекта по его строковому идентификатору
		 * @param String $prop_name строковой идентификатор свойства
		 * @return umiObjectProperty или NULL в случае неудачи
		 */
		public function getPropByName($prop_name) {
			$prop_name = strtolower($prop_name);

			if (!$this->isPropertyNameExist($prop_name)) {
				return null;
			}

			$propertyId = (int) $this->invertedProperties[$prop_name];
			return $this->getPropById($propertyId);
		}

		/**
		 * Получить свойство объекта по его числовому идентификатору (просто id)
		 * @param Integer $field_id id поля
		 * @return umiObjectProperty или NULL в случае неудачи
		 */
		public function getPropById($field_id) {
			if (!$this->isPropertyExists($field_id)) {
				return null;
			}

			if (!$this->properties[$field_id] instanceof umiObjectProperty) {
				$umiTypesHelper = umiTypesHelper::getInstance();
				$fieldTypeId = $umiTypesHelper->getFieldTypeIdByFieldId($field_id);
				$this->properties[$field_id] = umiObjectProperty::getProperty($this->id, $field_id, $this->type_id, $fieldTypeId);
			}

			return $this->properties[$field_id];
		}

		/**
		 * Узнать, существует ли свойство с id $field_id
		 * @param Integer $field_id id поля
		 * @return Boolean true, если поле существует
		 */
		public function isPropertyExists($field_id) {
			return (bool) isset($this->properties[$field_id]);
		}

		/**
		 * Узнать, существует ли свойство с guid $fieldName
		 * @param string $fieldName id guid поля
		 * @return Boolean true, если поле существует
		 */
		public function isPropertyNameExist($fieldName) {
			return (bool) isset($this->invertedProperties[$fieldName]);
		}

		/**
		 * Узнать, существует ли группа полей с id $prop_group_id у объекта
		 * @param Integer $prop_group_id id группы полей
		 * @return Boolean true, если группа существует
		 */
		public function isPropGroupExists($prop_group_id) {
			if (count($this->prop_groups) == 0) {
				$this->loadGroups();
			}
			return (bool) isset($this->prop_groups[$prop_group_id]);
		}

		/**
		 * Загружает группы (umiFieldsGroup) и поля (umiField) в объект
		 */
		private function loadGroups() {
			$groups = $this->getType()->getFieldsGroupsList();

			foreach ($groups as $group) {
				if (!$group instanceof umiFieldsGroup || $group->getIsActive() == false ) {
					continue;
				}

				$fields = $group->getFields();

				$this->prop_groups[$group->getId()] = Array();

				foreach ($fields as $field) {
					$this->prop_groups[$group->getId()][] = $field->getId();
				}
			}
		}

		/**
		 * Получить id группы полей по ее строковому идентификатору
		 * @param String $prop_group_name Строковой идентификатор группы полей
		 * @return Integer id группы полей, либо false, если такой группы не существует
		 */
		public function getPropGroupId($prop_group_name) {
			$groups_list = $this->getType()->getFieldsGroupsList();

			foreach ($groups_list as $group) {
				if ($group->getName() == $prop_group_name) {
					return $group->getId();
				}
			}
			return false;
		}

		/**
		 * Получить группу полей по ее строковому идентификатору
		 * @param String $prop_group_name Строковой идентификатор группы полей
		 * @return array|boolean список идентификаторов полей в группе,
		 * либо false если такой группы не существует
		 */
		public function getPropGroupByName($prop_group_name) {
			if ($group_id = $this->getPropGroupId($prop_group_name)) {
				return $this->getPropGroupById($group_id);
			} else {
				return false;
			}
		}

		/**
		 * Получить группу полей по ее id
		 * @param Integer $prop_group_id id группы полей
		 * @return array|boolean список идентификаторов полей в группе,
		 * либо false если такой группы не существует
		 */
		public function getPropGroupById($prop_group_id) {

			if ($this->isPropGroupExists($prop_group_id)) {
				return $this->prop_groups[$prop_group_id];
			}

			$type = $this->getType();
			$group = $type->getFieldsGroup($prop_group_id);

			if (!$group instanceof umiFieldsGroup) {
				return false;
			}

			$groupFields = $group->getFields();
			$fieldIds = array();
			foreach ($groupFields as $field) {
				if (!$field instanceof umiField) {
					continue;
				}
				$fieldIds[] = $field->getId();
			}

			return $this->prop_groups[$prop_group_id] = $fieldIds;


		}

		/**
		 * Получить значение свойства $prop_name объекта
		 * @param String $prop_name строковой идентификатор поля
		 * @param Array $params = NULL дополнительные параметры (обычно не используется)
		 * @return Mixed значение поле. Тип значения зависит от типа поля. Вернет false, если свойства не существует.
		 */
		public function getValue($prop_name, $params = NULL) {
			if ($prop = $this->getPropByName($prop_name)) {
				return $prop->getValue($params);
			} else {
				return false;
			}
		}

		/**
		 * @inheritdoc
		 */
		public function getValueById($fieldId, $params = null) {
			$property = $this->getPropById($fieldId);

			if (!$property instanceof iUmiObjectProperty) {
				return false;
			}

			return $property->getValue($params);
		}

		/**
		 * Установить значение свойства с $prop_name данными из $prop_value. Устанавливает флаг "Модифицирован".
		 * Значение в БД изменится только когда на объекте будет вызван темод commit(), либо в деструкторе объекта
		 * @param String $prop_name строковой идентификатор поля
		 * @param Mixed $prop_value новое значение для поля. Зависит от типа поля
		 * @return Boolean true если прошло успешно
		 */
		public function setValue($prop_name, $prop_value) {
			if ($prop = $this->getPropByName($prop_name)) {

				$prop->setValue($prop_value);

				if ($prop->getIsUpdated()) {
					$this->setIsUpdated(true, true);
				}

				return true;
			} else {
				return false;
			}
		}

		/**
		 * @deprecated
		 */
		public function checkSelf() {
			static $res;

			if ($res !== null) {
				return $res;
			}

			if (!cacheFrontend::getInstance()->getIsConnected()) {
				return $res = true;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_objects WHERE id = '{$this->id}'";
			$result = $connection->queryResult($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$res = (bool) $result->length();

			if (!$res) {
				cacheFrontend::getInstance()->flush();
			}

			return $res;
		}

		/**
		 * Устанавливает флаг модификации объекта
		 * @param bool $isUpdated флаг модификации
		 * @param bool $writeUpdateTime нужно ли записывать время последнего обновления объекта
		 */
		public function setIsUpdated($isUpdated = true) {
			$args = func_get_args();
			$isUpdated = array_shift($args);

			if (is_null($isUpdated)) {
				$isUpdated = true;
			}

			$writeUpdateTime = array_shift($args);

			if (is_null($writeUpdateTime)) {
				$writeUpdateTime = true;
			}

			umiObjectsCollection::getInstance()->addUpdatedObjectId($this->id);
			parent::setIsUpdated($isUpdated);

			if ($writeUpdateTime) {
				$this->setUpdateTime(time());
			}
		}

		/**
		 * Удалить объект
		 */
		public function delete() {
			umiObjectsCollection::getInstance()->delObject($this->id);
		}

		/**
		 * Возвращает значение свойства или поля
		 * @param string $varName имя свойства или поля
		 * @return int|Mixed|string
		 */
		public function __get($varName) {
			switch ($varName) {
				case "id":		return $this->id;
				case "name":	return $this->getName();
				case "ownerId":	return $this->getOwnerId();
				case "typeId":	return $this->getTypeId();
				case "GUID":	return $this->getGUID();
				case "typeGUID":return $this->getTypeGUID();
				case "xlink":	return 'uobject://' . $this->id;

				default:		return $this->getValue($varName);
			}
		}

		/**
		 * Проверяет наличие свойства
		 * @param string $prop имя свойства
		 * @return bool
		 */
		public function __isset($prop) {
			switch($prop) {
				case 'id':
				case 'name':
				case 'ownerId':
				case 'typeId':
				case 'GUID':
				case 'typeGUID':
				case 'xlink': {
					return true;
				}
				default : {
					return ($this->getPropByName($prop) instanceof umiObjectProperty);
				}
			}
		}

		/**
		 * Устанавливает значение свойства или поля
		 * @param string $varName имя свойства или поля
		 * @param mixed $value значение
		 * @return bool|void
		 * @throws coreException
		 */
		public function __set($varName, $value) {
			switch ($varName) {
				case "id":		throw new coreException("Object id could not be changed");
				case "name":	return $this->setName($value);
				case "ownerId":	return $this->setOwnerId($value);
				default:		return $this->setValue($varName, $value);
			}
		}

		/**
		 * TODO PHPDoc
		 * (non-PHPdoc)
		 * @see umiEntinty::beforeSerialize()
		 */
		public function beforeSerialize($reget = false) {
			static $types = array();
			if ($reget && isset($types[$this->type_id])) {
				$this->type = $types[$this->type_id];
			} else {
				$types[$this->type_id] = $this->type;
				$this->type = null;
			}
		}

		/**
		 * TODO PHPDoc
		 * (non-PHPdoc)
		 * @see umiEntinty::afterSerialize()
		 */
		public function afterSerialize() {
			$this->beforeSerialize(true);
		}

		/**
		 * TODO PHPDoc
		 * (non-PHPdoc)
		 * @see umiEntinty::afterUnSerialize()
		 */
		public function afterUnSerialize() {
			$this->getType();
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function getModule() {
			$hierarchyTypeId = umiObjectTypesCollection::getInstance()->getHierarchyTypeIdByObjectTypeId($this->getTypeId());
			$hierarchyType = umiHierarchyTypesCollection::getInstance()->getType($hierarchyTypeId);
			if ($hierarchyType instanceof umiHierarchyType) {
				return $hierarchyType->getName();
			} else {
				return false;
			}}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function getMethod() {
			$hierarchyTypeId = umiObjectTypesCollection::getInstance()->getHierarchyTypeIdByObjectTypeId($this->getTypeId());
			$hierarchyType = umiHierarchyTypesCollection::getInstance()->getType($hierarchyTypeId);
			if ($hierarchyType instanceof umiHierarchyType) {
				return $hierarchyType->getExt();
			} else {
				return false;
			}
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getGUID() {
			return $this->guid;
		}

		/**
		 * @inheritdoc
		 * @throws coreException если GUID уже используется
		 */
		public function setGUID($guid) {
			$id = umiObjectsCollection::getInstance()->getObjectIdByGUID($guid);
			if($id && $id != $this->id) {
				throw new coreException("GUID {$guid} already in use");
			}

			if ($this->getGUID() != $guid) {
				$this->guid = $guid;
				$this->setIsUpdated();
			}
		}

		/**
		 * Деструктор
		 */
		public function __destruct() {
			parent::__destruct();
			umiObjectProperty::unloadPropData($this->id);
		}

		/**
		 * Возвращает загруженные свойства объекта
		 * @return iUmiObjectProperty[]
		 */
		private function getProperties() {
			return $this->properties;
		}

		/**
		 * @deprecated
		 * Загрузить тип данных (класс umiObjectType), который описывает этот объект
		 */
		private function loadType() {
			$type = umiObjectTypesCollection::getInstance()->getType($this->type_id);

			if (!$type) {
				throw new coreException("Can't load type in object's init");
			}

			$this->type = $type;
		}

		/**
		 * @deprecated
		 * Подготовить внутреннеие массивы для свойств и групп свойств на основе структуры типа данных, с которым связан объект
		 */
		private function loadProperties() {
			$type = $this->getType();
			$groups_list = $type->getFieldsGroupsList();
			foreach ($groups_list as $group) {
				if ($group->getIsActive() == false) {
					continue;
				}

				$fields = $group->getFields();

				$this->prop_groups[$group->getId()] = Array();

				foreach ($fields as $field) {
					$this->properties[$field->getId()] = $field->getName();
					$this->prop_groups[$group->getId()][] = $field->getId();
				}
			}
		}


    /* CUSTOM ORDER SAVE LOG */
    private function log_order_change() {
        if ($this->getTypeId() != 51) { // 51 - заказ
            return true;
        }
        $old_serialize_object = $this->create_object_snapshot(new umiObject($this->getId(), true));
        $new_serialize_object = $this->create_object_snapshot($this);
        $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

		$current_user_id = permissionsCollection::getInstance()->getUserId();

        $this->save_orders_change_log($this->getId(), $current_user_id, $res['message'], $res['details']);
    }

    /**
     * Сохранение факта изменения данных пользователя
     * @param type $obj_id - идентификатор объекта пользователя
     * @param type $author - имя автора изменений, строка
     * @param type $details - текстовое описание события
     */
    private function save_orders_change_log($obj_id, $author, $message, $details) {
        $author = mysql_real_escape_string($author);
        $message = mysql_real_escape_string($message);
        $details = serialize($details);
        $connection = ConnectionPool::getInstance()->getConnection();
        $connection->query("INSERT INTO `orders_change_log`(`obj_id`, `author`, `message`, `details`) VALUES ({$obj_id}, '{$author}', '{$message}', '{$details}')");
    }

    /**
     * Превращаем объект в массив свойств
     * @param umiObject $object
     * @return массив со значениями свойств
     */
    private function create_object_snapshot(umiObject $object) {
        $result = array();
        $object_types_collection = umiObjectTypesCollection::getInstance();
        $object_type = $object_types_collection->getType($object->getTypeId());
        $object_fields = $object_type->getAllFields();
        foreach ($object_fields as $field) {
            $row = $this->create_property_snapshot($object->getPropByName($field->getName()));
            if ($row !== false) {
                $result = $result + $row;
            }
        }
        return $result;
    }

    /**
     * Превращаем одно свойство объекта в массив
     * @param umiObjectProperty $property
     * @return boolean false если значение пусто или массив из правильных значений
     */
    private function create_property_snapshot(umiObjectProperty $property) {
        try {
            $value = $property->getValue();
            if (!empty($value)) {
                $field = $property->getField();
                $res = array(
                    "id" => $field->getId(),
                    "type" => $field->getDataType(),
                    "name" => $property->getName(),
                    "title" => $property->getTitle()
                );
                if (is_array($value)) {
                    $res['value'] = $value;
                } else {
                    $res['value'] = mysql_real_escape_string($value);
                }
                return array($field->getId() => $res);
            }
        } catch (Exception $exc) {

        }
        return false;
    }

    /**
     * Получаем разницу между двумя сериализованными объектами и сообщение об этом с деталями
     * @param type $old_value
     * @param type $new_value
     * @return type
     */
    private function create_diff_details($old_value, $new_value) {
        $message = array();
        $details = array();
        $diff_props = array();

        // все ключи
        $keys = array_unique(array_merge(array_keys($old_value), array_keys($new_value)));
        foreach ($keys as $key) {
            // пропускаем пустой пароль
            if (key_exists($key, $old_value) && !key_exists($key, $new_value) && $old_value[$key]['title'] == 'Пароль') {
                continue;
            }

            // Дата 1970-01-01 03:00:00 - пустая, так как это стандарт пустого значения редактора дат. Чистим значение
            if (key_exists($key, $new_value) && $new_value[$key]['type'] == 'date' && $new_value[$key]['value'] == '1970-01-01 03:00:00') {
                unset($new_value[$key]);
            }

            // Файл, не являющийся файлом (директория) - пустой, так как это стандарт пустого значения выбора файла. Чистим значение
            if (key_exists($key, $new_value) && $new_value[$key]['type'] == 'file' && !is_file($_SERVER['DOCUMENT_ROOT'] . $new_value[$key]['value'])) {
                unset($new_value[$key]);
            }

            if (key_exists($key, $old_value) && !key_exists($key, $new_value)) {
                $diff_props[$key] = array(
                    "name" => $old_value[$key]['title'],
                    "old" => $old_value[$key],
                    "new" => false
                );
            } elseif (!key_exists($key, $old_value) && key_exists($key, $new_value)) {
                $diff_props[$key] = array(
                    "name" => $new_value[$key]['title'],
                    "old" => false,
                    "new" => $new_value[$key]
                );
            } elseif (key_exists($key, $old_value) && key_exists($key, $new_value)) {
                if ($old_value[$key]['value'] != $new_value[$key]['value']) {
                    $diff_props[$key] = array(
                        "name" => $old_value[$key]['title'],
                        "old" => $old_value[$key],
                        "new" => $new_value[$key]
                    );
                }
            }
        }

        foreach ($diff_props as $key => $values) {
            $message[] = 'Изменено свойство "' . $values['name'] . '". Старое значение: "' . ($values['old'] === false ? '' : $values['old']['value']) . '", новое значение: "' . ($values['new'] === false ? '' : $values['new']['value']) . '".';
            $details[$key] = $values;
        }

        return array(
            "message" => implode("<br>", $message),
            "details" => $details
        );
    }
}