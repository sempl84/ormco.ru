<?php
interface iUmiObjectTypesCollection {
	public function addType($parentId, $name, $isLocked = false, $ignoreParentGroups = false);
	public function delType($typeId);

	public function getType($typeId);
	public function getSubTypesList($typeId);

	public function getParentTypeId($typeId);
	public function getChildTypeIds($typeId, $children = false);

	public function getGuidesList($publicOnly = false, $parentTypeId = null);

	public function getTypesByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache = false);
	public function getTypeIdByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache = false);
	public function getTypeIdByHierarchyTypeName($module, $method = "");
}