<?php
interface iUmiField extends iUmiEntinty {
	public function getName();
	public function setName($name);

	public function getTitle();
	public function setTitle($title);

	public function getIsLocked();
	public function setIsLocked($isLocked);

	public function getIsInheritable();
	public function setIsInheritable($isInheritable);

	public function getIsVisible();
	public function setIsVisible($isVisible);

	public function getFieldTypeId();
	public function setFieldTypeId($fieldTypeId);

	public function getFieldType();

	public function getGuideId();
	public function setGuideId($guideId);
	/**
	 * Связано ли поле с каким-либо справочником
	 * @return bool
	 */
	public function hasGuide();

	public function getIsInSearch();
	public function setIsInSearch($isInSearch);

	public function getIsInFilter();
	public function setIsInFilter($isInFilter);

	public function getTip();
	public function setTip($tip);

	public function getIsRequired();
	public function setIsRequired($isRequired = false);

	public function getIsSortable();
	public function setIsSortable($sortable = false);

	public function getRestrictionId();
	public function setRestrictionId($restrictionId = false);

	public function getIsSystem();
	public function setIsSystem($isSystem = false);

	public function isImportant();
	public function setImportanceStatus($status = false);

	public function getDataType();
}