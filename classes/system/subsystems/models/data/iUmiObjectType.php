<?php
interface iUmiObjectType extends iUmiEntinty{
	public function addFieldsGroup($name, $title, $is_active = true, $is_visible = true, $tip = '');
	public function delFieldsGroup($fieldGroupId);

	public function getFieldsGroupByName($fieldGroupName, $allowDisabled = false);

	public function getFieldsGroup($fieldGroupId, $ignoreIsActive = false);
	public function getFieldsGroupsList($showDisabledGroups = false);

	public function getName();
	public function setName($name);

	public function setIsLocked($isLocked);
	public function getIsLocked();

	public function setIsGuidable($isGuidable);
	public function getIsGuidable();

	public function setIsPublic($isPublic);
	public function getIsPublic();

	public function setHierarchyTypeId($hierarchyTypeId);
	public function getHierarchyTypeId();

	public function getParentId();

	public function setFieldGroupOrd($groupId, $newOrd, $isLast);


	public function getFieldId($fieldName, $ignoreInactiveGroups = true);

	public function getAllFields($returnOnlyVisibleFields = false);

	public function getModule();
	public function getMethod();
}