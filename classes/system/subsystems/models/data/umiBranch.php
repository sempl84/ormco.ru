<?php

	/**
	 * Этот класс служит для управления разделением и объединением контентных таблиц
	 * Впервые появился в версии 2.7.0. Логика работы класса находится на уровне mysql-драйвера.
	 * Данный класс не следует использовать в прикладном коде модулей.
	 */
	class umiBranch {
		static protected $branchedObjectTypes = false;

		/**
		 * Проанализировать текущее состояние контентных таблиц и сохранить в кеш
		 * @return Array список типов данных, которых затронули изменения
		 * @throws coreException
		 */
		public static function saveBranchedTablesRelations() {
			$cacheDirPath = self::getCacheDirPath();
			$cacheFilePath = $cacheDirPath . self::getCacheFileName();
			$objectTypesCollection = umiObjectTypesCollection::getInstance();
			self::$branchedObjectTypes = [];

			clearstatcache();

			if (file_exists($cacheFilePath)) {
				unlink($cacheFilePath);
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SHOW TABLES LIKE 'cms3_object_content%'";
			$result = $connection->queryResult($sql, true);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$branchedHierarchyTypes = [];

			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				if (preg_match("/cms3_object_content_([0-9]+)/", array_shift($row), $out)) {
					$branchedHierarchyTypes[] = (int) $out[1];
				}
			}

			$branchedObjectTypes = [];
			foreach ($branchedHierarchyTypes as $hierarchyTypeId) {
				$objectTypes = array_keys($objectTypesCollection->getTypesByHierarchyTypeId($hierarchyTypeId));
				if (is_array($objectTypes)) {
					foreach ($objectTypes as $objectTypeId) {
						$branchedObjectTypes[$objectTypeId] = $hierarchyTypeId;
					}
				}
			}

			if (is_dir($cacheDirPath) && is_writable($cacheDirPath)) {
				file_put_contents($cacheFilePath, serialize($branchedObjectTypes));
			}

			if (is_file($cacheFilePath)) {
				chmod($cacheFilePath, 0777);
			}

			return self::$branchedObjectTypes = $branchedObjectTypes;
		}

		/**
		 * Узнать, таблице с каким названием лежат данные для типа данных $objectTypeId
		 * @param Integer $objectTypeId id типа данных
		 * @return String название mysql-таблицы
		 */
		public static function getBranchedTableByTypeId($objectTypeId) {
			$branchedObjectTypes = self::$branchedObjectTypes;

			if (!is_array($branchedObjectTypes)) {
				$branchedObjectTypes = self::getBranchedTablesRelations();
			}

			if (isset($branchedObjectTypes[$objectTypeId])) {
				$hierarchyTypeId = $branchedObjectTypes[$objectTypeId];
				return "cms3_object_content_" . $hierarchyTypeId;
			} else {
				return "cms3_object_content";
			}
		}

		public static function getBranchedTableByHierarchyTypeId($hierarchyTypeId) {
			$hierarchyTypeId = intval($hierarchyTypeId);
			$branchedObjectTypesIds = self::$branchedObjectTypes;

			if (is_array($branchedObjectTypesIds) && array_search($hierarchyTypeId, $branchedObjectTypesIds) !== false) {
				return "cms3_object_content_" . $hierarchyTypeId;
			}

			return "cms3_object_content";
		}

		/**
		 * Узнать, разделены ли данные с иерархическим типом $hierarchyTypeId
		 * @param Integer $hierarchyTypeId
		 * @return Boolean true, если разделены, false если данные лежат в общей таблице
		 */
		public static function checkIfBranchedByHierarchyTypeId($hierarchyTypeId) {
			$branchedObjectTypes = self::$branchedObjectTypes;

			if (!is_array($branchedObjectTypes)) {
				$branchedObjectTypes = self::getBranchedTablesRelations();
			}
			return (bool) in_array($hierarchyTypeId, $branchedObjectTypes);
		}

		/**
		 * Получить текущее состояние базы данных для принятия решения о необходимости branch/merge таблиц.
		 * @return Array ассоциативный массив с распределением объектов (count) по hierarchy-type-id.
		 */
		public static function getDatabaseStatus() {
			$umiHierarchyTypes = umiHierarchyTypesCollection::getInstance();
			$hierarchyTypesList = $umiHierarchyTypes->getTypesList();

			$result = [];

			/** @var umiHierarchyType $hierarchyType */
			foreach ($hierarchyTypesList as $hierarchyType) {
				$hierarchyTypeId = $hierarchyType->getId();
				$isBranched = self::checkIfBranchedByHierarchyTypeId($hierarchyTypeId);

				$sel = new selector('pages');
				$sel->types('hierarchy-type')->id($hierarchyTypeId);
				$count = $sel->length();

				$result[] = [
						'id' => $hierarchyTypeId,
						'isBranched' => $isBranched,
						'count' => $count
				];
			}

			return $result;
		}

		/**
		 * Загрузить из кеша данные о состоянии таблиц данных
		 * @return Array список типов данных, которых затронули изменения
		 */
		protected static function getBranchedTablesRelations() {
			$filePath = self::getCacheDirPath() . self::getCacheFileName();

			if (is_file($filePath)) {
				$branchedObjectTypes = unserialize(file_get_contents($filePath));
				if (is_array($branchedObjectTypes)) {
					return self::$branchedObjectTypes = $branchedObjectTypes;
				}
			}

			return self::saveBranchedTablesRelations();
		}

		/**
		 * Возвращает имя файла с кешем
		 * @return string
		 */
		protected static function getCacheFileName() {
			return 'branchedTablesRelations.rel';
		}

		/**
		 * Возвращает путь до директории с кешем
		 * @return string
		 */
		protected static function getCacheDirPath() {
			return mainConfiguration::getInstance()
				->includeParam('system.runtime-cache');
		}
	}
