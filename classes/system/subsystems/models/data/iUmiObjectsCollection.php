<?php
interface iUmiObjectsCollection {
	public function getObject($objectId, $row = false);
	public function addObject($name, $typeId, $isLocked = false);
	public function delObject($objectId);

	public function cloneObject($iObjectId);

	public function getGuidedItems($guideId);

	public function unloadObject($objectId);
	/**
	 * Проверяет существует ли объект с заданными идентификатором
	 * и идентификатором его типа данных (опционально)
	 * @param int $objectId проверяемый идентификатор объекта
	 * @param int|bool $typeId проверяемый идентификатор типа данных объекта или false
	 * @return bool результат проверки
	 */
	public function isExists($objectId, $typeId = false);
	/**
	 * Возвращает объект по его гуиду
	 * @param string $guid гуид
	 * @return iUmiObject|bool
	 */
	public function getObjectByGUID($guid);
	/**
	 * Возвращает идентификатор объекта по его гуиду
	 * @param string $guid гуид
	 * @return int|bool
	 */
	public function getObjectIdByGUID($guid);
}