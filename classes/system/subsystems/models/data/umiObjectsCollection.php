<?php
	/**
	 * Этот класс служит для управления/получения доступа к объектам.
	 * Класс является синглтоном, экземпляр класса можно получить через статический метод getInstance()
	 * TODO Check and format all PHPDoc's
	 */
	class umiObjectsCollection extends singleton implements iSingleton, iUmiObjectsCollection {
		private $objects = [];
		private $updatedObjects = [];
		private $objectsGuidsToId = [];

		/**
		 * Конструктор
		 */
		protected function __construct() {
			$this->preLoadObjectsIdsByGuid();
		}

		/**
		 * Получить экземпляр коллекции
		 * @return umiObjectsCollection экземпляр класса
		 */
		public static function getInstance($c = null) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * Сортировать ли элементы справочника по id?
		 * @return bool
		 */
		public static function isGuideItemsOrderedById() {
			$registrySort = regedit::getInstance()->getVal('//settings/ignore_guides_sort');

			if ($registrySort) {
				return true;
			}

			return (bool) mainConfiguration::getInstance()->get('kernel', 'order-guide-items-by-id');
		}

		/**
		 * Проверить, загружен ли в память объект с id $object_id
		 * @param int $object_id id объекта
		 * @throws coreException
		 * @return Boolean true, если объект загружен
		 */
		private function isLoaded($object_id) {
			if (gettype($object_id) == 'object') {
				throw new coreException('Object given!');
			}

			return (bool) array_key_exists($object_id, $this->objects);
		}

		/**
		 * {@inheritDoc}
		 */
		public function isExists($objectId, $typeId = false) {
			if (!is_numeric($objectId)) {
				return false;
			}

			$isTypeNumeric = is_numeric($typeId);

			if (!$isTypeNumeric && func_num_args() > 1) {
				return false;
			}

			$objectId = (int) $objectId;
			$condition = " `id` = $objectId";

			if ($isTypeNumeric) {
				$typeId = (int) $typeId;
				$condition .= " AND `type_id` = $typeId";
			}

			$query = <<<SQL
SELECT `id` FROM `cms3_objects` WHERE $condition
SQL;

			$result = ConnectionPool::getInstance()
				->getConnection()
				->queryResult($query);

			if ($result instanceof IQueryResult) {
				$row = $result->fetch();

				return (bool) ($row[0]);
			}

			return false;
		}

		/**
		 * Является ли $object объектом UMI
		 * @param mixed $object
		 * @return bool
		 */
		public function isUmiObject($object) {
			return ($object instanceof iUmiObject);
		}

		/**
		 * Получить объект по его имени
		 * @param string $name имя или языковая метка имени объекта
		 * @param mixed $typeId ID типа данных объекта
		 * @return umiObject|bool найденный объект или false, если объект не найден
		 */
		public function getObjectByName($name, $typeId = false) {
			if ($typeId !== false && !is_numeric($typeId)) {
				return false;
			}

			$translatedLabel = ulangStream::getI18n($name);
			$label = (null === $translatedLabel) ? $name : $translatedLabel;
			$dbConnection = ConnectionPool::getInstance()->getConnection();
			$labelAndNameDiff = ($name !== $label);

			$label = $dbConnection->escape($label);
			$name = $dbConnection->escape($name);
			$escapedTypeId = $dbConnection->escape($typeId);

			$namePart = ($labelAndNameDiff) ? "(`name` = '{$name}' or `name` = '{$label}')" : "`name` = '{$label}'";
			$typePart = $typeId !== false ? "AND `type_id` = '${escapedTypeId}'" : '';
			$query = <<<QUERY
SELECT id
FROM   `cms3_objects`
WHERE  ${namePart}
       ${typePart}
LIMIT  1
QUERY;
			$result = $dbConnection->queryResult($query);

			if ($result instanceof IQueryResult) {
				$result->setFetchType(IQueryResult::FETCH_ASSOC);
				$row = $result->fetch();
				if (isset($row['id']) && is_numeric($row['id'])) {
					return $this->getObject($row['id']);
				}

				return false;
			}

			return false;
		}

		/**
		 * Получить экземпляр объекта с id $object_id
		 * @param int $object_id id объекта
		 * @param bool $row
		 *
		 * @return umiObject|bool  экземпляр объекта $object_id, либо false в случае неудачи
		 */
		public function getObject($object_id, $row = false) {
			if (!is_numeric($object_id) || $object_id === 0) {
				return false;
			}

			$object_id = (int) $object_id;

			if ($this->isLoaded($object_id)) {
				return $this->objects[$object_id];
			}

			$cacheFrontend = cacheFrontend::getInstance();
			$object = $cacheFrontend->load($object_id, 'object');

			if (!$object instanceof iUmiObject) {
				try {
					$object = new umiObject($object_id, $row);
					$cacheFrontend->save($object, 'object');
				} catch (baseException $e) {
					return false;
				}
			}

			if (is_object($object)) {
				$this->objects[$object_id] = $object;

				return $this->objects[$object_id];
			} else {
				return false;
			}
		}

		/**
		 * @inheritdoc
		 */
		public function getObjectByGUID($guid) {
			$id = $this->getObjectIdByGUID($guid);

			return $this->getObject($id);
		}

		/**
		 * @inheritdoc
		 */
		public function getObjectIdByGUID($guid) {
			static $cache = [];

			if (!$guid) {
				return false;
			}

			if (isset($this->objectsGuidsToId[$guid])) {
				return $cache[$guid] = $this->objectsGuidsToId[$guid];
			}

			if (!cmsController::$IGNORE_MICROCACHE && isset($cache[$guid])) {
				return $cache[$guid];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$guid = $connection->escape($guid);
			$query = "SELECT `id` FROM `cms3_objects` WHERE `guid` = '{$guid}'";
			$result = $connection->queryResult($query, true);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($query));
			}

			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() == 0) {
				return $this->objectsGuidsToId[$guid] = $cache[$guid] = false;
			}

			$fetchResult = $result->fetch();

			return $this->objectsGuidsToId[$guid] = $cache[$guid] = array_shift($fetchResult);
		}

		/**
		 * Удалить объект с id $object_id. Если объект заблокирован, он не будет удален.
		 * При удалении принудительно вызывается commit() на удаляемом объекте
		 * Нельзя удалить системных пользователей (гостя, супервайзера и его группу).
		 * @param Integer $object_id id объекта
		 * @return Boolean true, если удаление удалось
		 * @throws coreException
		 */
		public function delObject($object_id) {
			if (!is_numeric($object_id) || $object_id === 0) {
				return false;
			}

			$object_id = (int) $object_id;
			$systemUsersPermissions = UmiCms\Service::SystemUsersPermissions();

			$systemUsersAndGroupsIds = [
				$systemUsersPermissions->getSvUserId(),
				$systemUsersPermissions->getSvGroupId(),
				$systemUsersPermissions->getGuestUserId()
			];

			if (in_array($object_id, $systemUsersAndGroupsIds)) {
				throw new coreException("You are not allowed to delete object #{$object_id}. Never. Don't even try.");
			}

			//Make sure, we don't will not try to commit it later
			$object = $this->getObject($object_id);

			if (!$object instanceof iUmiObject) {
				return false;
			}

			$object->commit();

			$event = new umiEventPoint('collectionDeleteObject');
			$event->setParam('object_id', $object_id);
			$event->setMode('before');
			$event->call();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms3_objects WHERE id = '{$object_id}' AND is_locked='0'";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$this->unloadObject($object_id);
			unset($object);

			cacheFrontend::getInstance()->del($object_id, 'object');

			$event->setMode('after');
			$event->call();

			return true;
		}

		/**
		 * Создать новый объект в БД
		 * @param String $name название объекта.
		 * @param Integer $type_id id типа данных (класс umiObjectType), которому будет принадлежать объект.
		 * @param Boolean $is_locked =false Состояние блокировки по умолчанию. Рекоммендуем этот параметр не указывать.
		 * @return Integer id созданного объекта, либо false в случае неудачи
		 * @throws coreException
		 */
		public function addObject($name, $type_id, $is_locked = false) {
			$type_id = (int) $type_id;

			if (!$type_id) {
				throw new coreException("Can't create object without object type id (null given)");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = 'INSERT INTO cms3_objects VALUES(NULL, NULL, NULL, NULL, %d, NULL, 0, NULL)';
			$sql = sprintf($sql, $type_id);
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$object_id = $connection->insertId();
			$object = new umiObject($object_id);

			$object->setName($name);
			$object->setIsLocked($is_locked);
			$object->setOwnerId(
				$this->getOwnerId()
			);

			$maxOrder = $this->getMaxOrderByTypeId(
				$object->getTypeId()
			);

			$object->setOrder($maxOrder + 1);
			$object->commit();

			$this->objects[$object_id] = $object;

			return $object_id;
		}

		/**
		 * Сделать копию объекта и всех его свойств
		 *
		 * @param int $iObjectId Id копируемого объекта
		 * @throws coreException Mysql ошибки
		 * @return int Id объекта-копии
		 */
		public function cloneObject($iObjectId) {
			$iObjectId = (int) $iObjectId;
			$vResult = false;

			$oObject = $this->getObject($iObjectId);

			if ($oObject instanceof umiObject) {
				$connection = ConnectionPool::getInstance()->getConnection();
				// clone object definition
				$sSql = "INSERT INTO cms3_objects (name, guid, is_locked, type_id, owner_id, ord, updatetime) SELECT name, guid, is_locked, type_id, owner_id, ord, updatetime FROM cms3_objects WHERE id = '{$iObjectId}'";
				$connection->query($sSql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sSql));
				}

				$iNewObjectId = $connection->insertId();
				// clone object content
				$sSql = "INSERT INTO cms3_object_content (obj_id, field_id, int_val, varchar_val, text_val, rel_val, tree_val,float_val)  SELECT '{$iNewObjectId}' as obj_id, field_id, int_val, varchar_val, text_val, rel_val, tree_val,float_val FROM cms3_object_content WHERE obj_id = '$iObjectId'";
				$connection->query($sSql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sSql));
				}

				$vResult = $iNewObjectId;
			}

			return $vResult;
		}

		/**
		 * Получить отсортированный по имени список всех объектов в справочнике $guide_id (id типа данных).
		 * Равнозначно если бы мы хотели получить этот список для всех объектов определенного типа данных
		 * @param int $guideId id справочника (id типа данных)
		 * @return Array массив, где ключи это id объектов, а значения - названия объектов
		 * @throws coreException
		 */
		public function getGuidedItems($guideId) {
			$connection = ConnectionPool::getInstance()->getConnection();

			if (is_numeric($guideId)) {
				$guideId = (int) $guideId;
			} else {
				$guideId = $connection->escape($guideId);
				$query = "SELECT `id` FROM `cms3_object_types` WHERE `guid`='" . $guideId . "' LIMIT 1";
				$result = $connection->queryResult($query);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$guideId = (int) $guideId;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$guideId = array_shift($fetchResult);
				}
			}

			$sql = "SELECT id, name FROM cms3_objects WHERE type_id = '{$guideId}'";

			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$guidedItems = [];

			foreach ($result as $row) {
				list($id, $name) = $row;
				$guidedItems[$id] = $this->translateLabel($name);
			}

			if (!self::isGuideItemsOrderedById()) {
				natsort($guidedItems);
			}

			return $guidedItems;
		}

		/**
		 * TODO PHPDoc
		 */
		public function getCountByTypeId($typeId) {
			$typeId = (int) $typeId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT COUNT(id) FROM cms3_objects WHERE type_id = '{$typeId}'";
			$result = $connection->queryResult($sql);

			if ($connection->errorOccurred()) {
				throw new databaseException($connection->errorDescription($sql));
			}

			list($count) = $result->fetch();

			return $count;
		}

		/**
		 * Выгрузить объект из коллекции
		 * @param int $object_id id объекта
		 * @return bool
		 */
		public function unloadObject($object_id) {
			if ($this->isLoaded($object_id)) {
				unset($this->objects[$object_id]);
				umiObjectProperty::unloadPropData($object_id);
			} else {
				return false;
			}
		}

		/**
		 * Выгрузить объекты из коллекции
		 */
		public function unloadAllObjects() {

			foreach ($this->objects as $object_id => $v) {
				unset($this->objects[$object_id]);
				umiObjectProperty::unloadPropData($object_id);
			}
		}

		/**
		 * Получить id всех объектов, загруженных в коллекцию
		 * @return Array массив, состоящий из id объектов
		 */
		public function getCollectedObjects() {
			return array_keys($this->objects);
		}

		/**
		 * Указать, что $object_id был изменен во время сессии. Используется внутри ядра.
		 * Явный вызов этого метода клиентским кодом не нужен.
		 * @param Integer $object_id id объекта
		 */
		public function addUpdatedObjectId($object_id) {
			if (!in_array($object_id, $this->updatedObjects)) {
				$this->updatedObjects[] = $object_id;
			}
		}

		/**
		 * Получить список измененных объектов за текущую сессию
		 * @return Array массив, состоящий из id измененных значений
		 */
		public function getUpdatedObjects() {
			return $this->updatedObjects;
		}

		/**
		 * Возвращает максимальное время последней модификации объектов, загруженных в текущей сессии
		 * @return int Unix timestamp
		 */
		public function getObjectsLastUpdateTime() {
			$maxUpdateTime = 0;

			/** @var iUmiObject $object */
			foreach ($this->objects as $object) {
				if (!$object instanceof iUmiObject) {
					continue;
				}

				if ($maxUpdateTime < $object->getUpdateTime()) {
					$maxUpdateTime = $object->getUpdateTime();
				}
			}

			return $maxUpdateTime;
		}

		/**
		 * Деструктор коллекции. Явно вызывать его не нужно никогда.
		 */
		public function __destruct() {
			if (count($this->updatedObjects) && function_exists('deleteObjectsRelatedPages')) {
				deleteObjectsRelatedPages();
			}
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function clearCache() {
			$keys = array_keys($this->objects);
			foreach ($keys as $key) {
				unset($this->objects[$key]);
			}
			$this->objects = [];
		}

		/**
		 * @deprecated
		 */
		public function checkObjectById($objectId) {
			static $res;

			if ($res !== null) {
				return $res;
			}

			if (!cacheFrontend::getInstance()->getIsConnected()) {
				return $res = true;
			}

			$objectId = (int) $objectId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_objects WHERE id = $objectId";
			$result = $connection->queryResult($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$res = (bool) $result->length();

			if (!$res) {
				cacheFrontend::getInstance()->flush();
			}

			return $res;
		}

		/**
		 * Меняет индекс сортировки двух объектов
		 * @param iUmiObject $firstObject первый объект
		 * @param iUmiObject $secondObject второй объект
		 * @param string $mode режим изменения сортировки
		 * @return bool
		 */
		public function changeOrder(iUmiObject $firstObject, iUmiObject $secondObject, $mode) {
			/* @var iUmiObject|umiEntinty $firstObject */
			$firstObjectOrder = $firstObject->getOrder();

			switch ($mode) {
				case 'child':
				case 'after': {
					/* @var iUmiObject|umiEntinty $secondObject */
					$secondObject->setOrder($firstObjectOrder + 1);
					$secondObject->commit();

					$this->reBuildOrder($secondObject, 'greed');
					break;
				}
				case 'before': {
					$this->reBuildOrder($firstObject, 'greed');

					/* @var iUmiObject|umiEntinty $secondObject */
					$secondObject->setOrder($firstObjectOrder);
					$secondObject->commit();
					break;
				}
				default: {
					return false;
				}
			}

			return true;
		}

		/**
		 * Перестраивает индекс сортировки у объектов, чей
		 * индекс больше индекса первого объекта
		 * @param iUmiObject $firstObject первый объект
		 * @param string $mode режим перестраивания
		 * @return bool
		 * @throws coreException
		 */
		public function reBuildOrder(iUmiObject $firstObject, $mode) {
			/* @var iUmiObject|umiEntinty $firstObject */
			$firstObjectOrder = (int) $firstObject->getOrder();
			$objectTypeId = (int) $firstObject->getTypeId();
			$objectTypeId = $this->getOrderTargetTypeId($objectTypeId);

			switch (true) {
				case is_numeric($objectTypeId): {
					$typeCondition = "`type_id` = $objectTypeId";
					break;
				}
				case is_array($objectTypeId): {
					$objectTypeId = implode(', ', $objectTypeId);
					$typeCondition = "`type_id` IN ($objectTypeId)";
					break;
				}
				default: {
					throw new coreException(getLabel('error-wrong-type-id-given'));
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();

			switch ($mode) {
				case 'greed': {
					$sql = <<<SQL
UPDATE `cms3_objects`
SET `ord` = `ord` + 1
WHERE $typeCondition
AND `ord` >= $firstObjectOrder;
SQL;
					break;
				}
				case 'normal':
				default: {
					$sql = <<<SQL
UPDATE `cms3_objects`
SET `ord` = `ord` + 1
WHERE $typeCondition
AND `ord` > $firstObjectOrder;
SQL;
				}
			}

			$connection->query($sql);
			return true;
		}

		/**
		 * Возвращает максимальное значение индекса сортировки
		 * среди объекто заданного типа данных
		 * @param int $objectTypeId идентификатор объектного типа данных
		 * @return int|bool
		 * @throws coreException
		 */
		public function getMaxOrderByTypeId($objectTypeId) {
            $arSkipObjectTypeId = array(
                51, 43
            );
            
            if(in_array($objectTypeId, $arSkipObjectTypeId)) {
                return 0;
            }
		    
			$objectTypeId = $this->getOrderTargetTypeId($objectTypeId);
			$connection = ConnectionPool::getInstance()->getConnection();

			switch (true) {
				case is_numeric($objectTypeId): {
					$sql = <<<SQL
SELECT max(`ord`) as ord FROM `cms3_objects` WHERE `type_id` = $objectTypeId;
SQL;
					break;
				}
				case is_array($objectTypeId): {
					$objectTypeId = implode(', ', $objectTypeId);
					$sql = <<<SQL
SELECT max(`ord`) as ord FROM `cms3_objects` WHERE `type_id` IN ($objectTypeId);
SQL;
					break;
				}
				default: {
					throw new coreException(getLabel('error-wrong-type-id-given'));
				}
			}

			/* @var mysqlQueryResult $result */
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			$result = $result->getIterator();
			/* @var mysqlQueryResultIterator $result */
			$row = $result->current();

			return (int) $row['ord'];
		}

		/**
		 * Возвращает идентификатор объектного типа данных,
		 * среди объекто которого нужно произвести перестройку
		 * индекса сортировки.
		 * @param int $objectTypeId идентификатор объектного типа данных
		 * @return int|array
		 */
		private function getOrderTargetTypeId($objectTypeId) {
			$umiObjectsTypes = umiObjectTypesCollection::getInstance();
			$objectTypeId = (int) $objectTypeId;
			$parentObjectTypeId = $umiObjectsTypes->getParentTypeId($objectTypeId);

			if (!is_numeric($parentObjectTypeId)) {
				return $objectTypeId;
			}

			$objectTypeChildren = $umiObjectsTypes->getChildTypeIds($objectTypeId);
			$parentObjectType = $umiObjectsTypes->getType($parentObjectTypeId);

			switch (true) {
				case !$parentObjectType instanceof iUmiObjectType && count($objectTypeChildren) == 0: {
					return $objectTypeId;
				}
				case !$parentObjectType instanceof iUmiObjectType: {
					$objectTypeChildren[] = $objectTypeId;

					return $objectTypeChildren;
				}
			}

			/* @var umiObjectType $parentObjectType */
			if ($parentObjectType->getGUID() == 'root-guides-type') {
				return $objectTypeId;
			}

			return $umiObjectsTypes->getChildTypeIds($parentObjectTypeId);
		}

		/**
		 * Загружает во внутренний кеш класса id объектов с определенным гуидами
		 * @return void
		 * @throws coreException
		 */
		private function preLoadObjectsIdsByGuid() {
			$mainConfigs = mainConfiguration::getInstance();
			$guids = $mainConfigs->get('kernel', 'objects-guids-preload');

			if (!is_array($guids) || count($guids) == 0) {
				return;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$guids = array_map([$connection, 'escape'], $guids);
			$sql = "SELECT `id`, `guid` FROM `cms3_objects` WHERE `guid` IN ('" . implode('\', \'', $guids) . "');";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$objectsGuidsToId = &$this->objectsGuidsToId;

			foreach ($result as $row) {
				$objectsGuidsToId[$row['guid']] = $row['id'];
			}
		}

		/**
		 * Возвращает идентификатор владельца создаваемых объектов
		 * @return int|null
		 */
		private function getOwnerId() {
			/**
			 * @var UmiCms\System\Auth\iAuth $auth
			 */
			try {
				$auth = UmiCms\Service::Auth();
			} catch (Exception $e) {
				return null;
			}

			$permissions = permissionsCollection::getInstance();
			return ($permissions->is_auth()) ? $auth->getUserId() : null;
		}
	}