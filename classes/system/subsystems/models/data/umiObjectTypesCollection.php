<?php
	/** Коллекция для работы с типами данных (umiObjectType), синглтон. */
	class umiObjectTypesCollection extends singleton implements iSingleton, iUmiObjectTypesCollection {
		/** кэш загруженных типов */
		private $types = [];

		/** кэш соответствий guid => id типов */
		private $guidsToIds = [];

		protected function __construct() {
			$this->loadTypeIdsByGuids();
		}

		/**
		 * Получить экземпляр коллекции
		 * @param null $c
		 * @return umiObjectTypesCollection экземпляр класса
		 */
		public static function getInstance($c = null) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * Получить тип по его id
		 * @param int|string $typeId id или guid типа данных
		 * @return umiObjectType|bool тип данных (класс umiObjectType), либо false
		 * @throws coreException Если не удалось загрузить тип
		 */
		public function getType($typeId) {
			if (!$typeId) {
				return false;
			}

			if (!is_numeric($typeId)) {
				$typeId = $this->getTypeIdByGUID($typeId);
			}

			if ($this->isLoaded($typeId)) {
				return $this->types[$typeId];
			} else {
				$this->loadType($typeId);
				return getArrayKey($this->types, $typeId);
			}
		}

		/**
		 * Получить тип по его GUID
		 * @param string $guid Global Umi Identifier
		 * @return umiObjectType тип данных (класс umiObjectType), либо false
		 */
		public function getTypeByGUID($guid) {
			$id = $this->getTypeIdByGUID($guid);
			return $this->getType($id);
		}

		/**
		 * Идентификатор типа, у которого есть поле $fieldId
		 * @param int $fieldId идентификатор поля
		 * @return bool|int
		 */
		public function getTypeIdByFieldId($fieldId) {
			static $cache = [];
			$fieldId = (int) $fieldId;

			if (isset($cache[$fieldId])) {
				return $cache[$fieldId];
			}

			$sql = <<<SQL
SELECT  MIN(fg.type_id)
	FROM cms3_fields_controller fc, cms3_object_field_groups fg
	WHERE fc.field_id = {$fieldId} AND fg.id = fc.group_id
SQL;
			if ($objectTypeId = cacheFrontend::getInstance()->loadSql($sql)) {
				return $cache[$fieldId] = $objectTypeId;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$objectTypeId = array_shift($fetchResult);
			} else {
				$objectTypeId = false;
			}

			$cache[$fieldId] = $objectTypeId;
			cacheFrontend::getInstance()->saveSql($sql, $objectTypeId, 60);

			return $objectTypeId;
		}

		/**
		 * Получить числовой идентификатор типа по его GUID
		 * @param string $guid Global Umi Identifier
		 * @param bool $ignoreCache игнорировать кеш
		 * @return integer/boolean(false)
		 * @throws coreException
		 */
		public function getTypeIdByGUID($guid, $ignoreCache = false) {
			if (!is_string($guid)) {
				return false;
			}

			if (isset($this->guidsToIds[$guid]) && !cmsController::$IGNORE_MICROCACHE && !$ignoreCache) {
				return $this->guidsToIds[$guid];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$guid = $connection->escape($guid);
			$query = "SELECT `id` FROM `cms3_object_types` WHERE `guid` = '{$guid}'";
			$result = $connection->queryResult($query, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($query));
			}

			$typeId = false;
			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$typeId = array_shift($fetchResult);
			}

			return $this->guidsToIds[$guid] = $typeId;
		}

		/**
		 * Создать тип данных с названием $name, дочерний от типа $parentId
		 * @param Integer $parentId id родительского типа данных, от которого будут унаследованы поля и группы полей
		 * @param String $name название создаваемого типа данных
		 * @param Boolean $isLocked =false статус блокировки. Этот параметр указывать не надо
		 * @param Boolean $ignoreParentGroups не наследовать группы и поля родительского типа данных
		 * @throws coreException
		 * @return Integer id созданного типа данных, либо false в случае неудачи
		 */
		public function addType($parentId, $name, $isLocked = false, $ignoreParentGroups = false) {
			$parentId = (int) $parentId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO cms3_object_types (parent_id) VALUES('{$parentId}')";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$typeId = $connection->insertId();

			if (!$ignoreParentGroups) {
				$sql = "SELECT * FROM cms3_object_field_groups WHERE type_id = '{$parentId}'";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ASSOC);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}

				foreach ($result as $row) {
					$sql = <<<SQL
INSERT INTO cms3_object_field_groups (name, title, type_id, is_active, is_visible, ord, is_locked)
VALUES (
		'{$connection->escape($row['name'])}',
		'{$connection->escape($row['title'])}',
		'{$typeId}',
		'{$row['is_active']}',
		'{$row['is_visible']}',
		'{$row['ord']}',
		'{$row['is_locked']}'
);
SQL;
					$connection->query($sql);

					if ($connection->errorOccurred()) {
						throw new coreException($connection->errorDescription($sql));
					}

					$oldGroupId = $row['id'];
					$newGroupId = $connection->insertId();

					$sql = <<<SQL
INSERT INTO cms3_fields_controller
SELECT ord, field_id, '{$newGroupId}' FROM cms3_fields_controller WHERE group_id = '{$oldGroupId}';
SQL;
					$connection->query($sql);

					if ($connection->errorOccurred()) {
						throw new coreException($connection->errorDescription($sql));
					}
				}
			}

			$parentHierarchyTypeId = false;

			if ($parentId) {
				$parentType = $this->getType($parentId);
				if ($parentType) {
					$parentHierarchyTypeId = $parentType->getHierarchyTypeId();
				}
			}

			$type = new umiObjectType($typeId);
			$type->setName($name);
			$type->setIsLocked($isLocked);

			if ($parentHierarchyTypeId) {
				$type->setHierarchyTypeId($parentHierarchyTypeId);
			}

			$type->commit();
			$this->types[$typeId] = $type;
			umiBranch::saveBranchedTablesRelations();
			return $typeId;
		}

		/**
		 * Удалить тип данных с идентификатором $typeId
		 * Все объекты этого типа будут автоматически удалены без возможности восстановления.
		 * Все дочерние типы от $typeId будут удалены рекурсивно.
		 * @param int $typeId идентификатор типа данных, который будет удален
		 * @return bool true, если удаление было успешным
		 * @throws publicAdminException
		 * @throws coreException
		 */
		public function delType($typeId) {
			$type = $this->getType($typeId);

			if (!$type instanceof umiObjectType) {
				return false;
			}

			if ($type->getIsLocked()) {
				throw new publicAdminException(getLabel('error-object-type-locked'));
			}

			$typeIds = $this->getChildTypeIds($typeId);
			$typeIds[] = (int) $typeId;

			foreach ($typeIds as $typeId) {
				$type = $this->getType($typeId);

				foreach ($type->getFieldsGroupsList(true) as $group) {
					$type->delFieldsGroup($group->getId());
				}

				unset($this->types[$typeId]);
			}

			$typeIds = implode(', ', $typeIds);
			$connection = ConnectionPool::getInstance()->getConnection();

			$sql = "DELETE FROM cms3_objects WHERE type_id IN ({$typeIds})";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$sql = "DELETE FROM cms3_object_types WHERE id IN ({$typeIds})";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$sql = "DELETE FROM cms3_import_types WHERE new_id IN ({$typeIds})";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			umiBranch::saveBranchedTablesRelations();
			return true;
		}

		/**
		 * Проверить, загружен ли тип данных $typeId в коллекцию
		 * @param Integer $typeId id типа данных
		 * @return Boolean true, если загружен
		 */
		private function isLoaded($typeId) {
			if ($typeId === false) {
				return false;
			}
			return (bool) array_key_exists($typeId, $this->types);
		}

		/**
		 * Загрузить тип данных в память
		 * @param Integer $typeId id типа данных
		 * @return Boolean true, если объект удалось загрузить
		 */
		private function loadType($typeId) {
			if ($this->isLoaded($typeId)) {
				return true;
			}

			$type = cacheFrontend::getInstance()->load($typeId, "object_type");
			if (!$type instanceof umiObjectType) {
				try {
					$type = new umiObjectType($typeId);
				} catch (privateException $e) {
					return false;
				}

				cacheFrontend::getInstance()->save($type, "object_type");
			}

			if (is_object($type)) {
				$this->types[$typeId] = $type;
				return true;
			}
			return false;
		}

		/**
		 * Получить список дочерних типов по отношению к типу $type_id
		 * @param Integer $typeId id родительского типа данных
		 * @return int[] массив, состоящий из id дочерних типов данных
		 * @throws coreException
		 */
		public function getSubTypesList($typeId) {
			if (!is_numeric($typeId)) {
				throw new coreException("Type id must be numeric");
			}

			$typeId = (int) $typeId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_object_types WHERE parent_id = '{$typeId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$typeIds = [];
			foreach ($result as $row) {
				$typeIds[] = (int) array_shift($row);
			}
			return $typeIds;
		}

		/**
		 * Получить id типа данных, который является непосредственным родителем типа $type_id
		 * @param Integer $typeId id типа данных
		 * @return Integer id родительского типа, либо false
		 * @throws coreException
		 */
		public function getParentTypeId($typeId) {
			if ($this->isLoaded($typeId)) {
				return $this->getType($typeId)->getParentId();
			}

			$typeId = (int) $typeId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT parent_id FROM cms3_object_types WHERE id = '{$typeId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			if ($result->length() == 0) {
				return false;
			}

			$fetchResult = $result->fetch();
			return (int) array_shift($fetchResult);
		}

		/**
		 * Получить список всех дочерних типов от $typeId на всю глубину наследования
		 * @param Integer $typeId id типа данных
		 * @param mixed $children этот параметр указывать не требуется
		 * @return Array массив, состоящий из $id типов данных
		 * @throws coreException
		 */
		public function getChildTypeIds($typeId, $children = false) {
			if (!$children) {
				$children = [];
			}

			$typeId = (int) $typeId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_object_types WHERE parent_id = '{$typeId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$childTypeIds = [];

			foreach ($result as $row) {
				$id = array_shift($row);
				$childTypeIds[] = $id;

				if (!in_array($id, $children)) {
					$childrenIds = $this->getChildTypeIds($id, $childTypeIds);

					foreach ($childrenIds as $childId) {
						$childTypeIds[] = $childId;
					}
				}
			}

			return array_unique($childTypeIds);
		}

		/**
		 * Получить список типов данных, которые можно использовать в качестве справочников
		 * @param Boolean $publicOnly = false искать только те типа данных, у которых стоит флаг "Публичный"
		 * @param Integer $parentTypeId = null искать только в этом родителе
		 * @return Array массив, где ключ это id типа данных, а значение это его название
		 */
		public function getGuidesList($publicOnly = false, $parentTypeId = null) {
			$publicClause = ($publicOnly) ? "AND is_public = '1'" : "";
			$sql = "SELECT id, name FROM cms3_object_types WHERE is_guidable = '1' {$publicClause}";
			if ($parentTypeId) {
				$parentTypeId = (int) $parentTypeId;
				$sql .= " AND parent_id = '{$parentTypeId}'";
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$guides = [];
			foreach ($result as $row) {
				list($id, $name) = $row;
				$guides[$id] = $this->translateLabel($name);
			}
			return $guides;
		}

		/**
		 * Получить список всех типов данных, связанных с базовым типом (umiHierarchyType) $hierarchyTypeId
		 * @param Integer $hierarchyTypeId id базового типа
		 * @param Boolean $ignoreMicroCache = false не использовать микрокеширование результата
		 * @return Array массив, где ключ это id типа данных, а значние - название типа данных
		 * @throws coreException
		 */
		public function getTypesByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache = false) {
			static $cache = [];
			$hierarchyTypeId = (int) $hierarchyTypeId;
			if (isset($cache[$hierarchyTypeId]) && !$ignoreMicroCache) {
				return $cache[$hierarchyTypeId];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id, name FROM cms3_object_types WHERE hierarchy_type_id = '{$hierarchyTypeId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$typeIds = [];
			foreach ($result as $row) {
				list($id, $name) = $row;
				$typeIds[$id] = $this->translateLabel($name);
			}
			return $cache[$hierarchyTypeId] = $typeIds;
		}

		/**
		 * Получить тип данных, связанный с базовым типом (umiHierarchyType) $hierarchy_type_id
		 * @param Integer $hierarchyTypeId id базового типа
		 * @param Boolean $ignoreMicroCache =false не использовать микрокеширование результата
		 * @return Integer id типа данных, либо false
		 * @throws coreException
		 */
		public function getTypeIdByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache = false) {
			static $cache = [];
			$hierarchyTypeId = (int) $hierarchyTypeId;
			if (isset($cache[$hierarchyTypeId]) && !$ignoreMicroCache && !cmsController::$IGNORE_MICROCACHE) {
				return $cache[$hierarchyTypeId];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_object_types WHERE hierarchy_type_id = '{$hierarchyTypeId}' LIMIT 1";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$typeId = false;
			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$typeId = array_shift($fetchResult);
			}
			return $cache[$hierarchyTypeId] = $typeId;
		}

		/**
		 * Получить все типы данных
		 * @return Array массив всех типов или false
		 * @throws coreException
		 */
		public function getAllTypes() {
			static $cache = [];
			if (!empty($cache)) {
				return $cache;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT id, name, guid, is_locked, parent_id, is_guidable, is_public, hierarchy_type_id, sortable
FROM cms3_object_types;
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$types = [];
			foreach ($result as $row) {
				$row['name'] = $this->translateLabel($row['name']);
				$types[$row['id']] = $row;
			}
			return $cache = $types;
		}

		/**
		 * Получить тип данных, связанный с базовым типом (umiHierarchyType) $module/$method
		 * @param String $module имя модуля
		 * @param String $method имя метода
		 * @return Integer id типа данных, либо false
		 */
		public function getTypeIdByHierarchyTypeName($module, $method = "") {
			$hierarchyType = selector::get('hierarchy-type')->name($module, $method);
			if (!$hierarchyType) {
				return false;
			}
			$hierarchyTypeId = $hierarchyType->getId();
			$typeId = $this->getTypeIdByHierarchyTypeId($hierarchyTypeId);
			return (int) $typeId;
		}

		/** Очистить внутренний кэш класса */
		public function clearCache() {
			$this->types = [];
		}

		/**
		 * Возвращает id иерархического типа данных, связанного с объектным типом данных.
		 * @param int $objectTypeId ид объектного типа данных
		 * @return bool|int
		 */
		public function getHierarchyTypeIdByObjectTypeId($objectTypeId) {
			static $cache = [];
			$objectTypeId = intval($objectTypeId);
			if (isset($cache[$objectTypeId])) {
				return $cache[$objectTypeId];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT `hierarchy_type_id` as id FROM `cms3_object_types` WHERE `id` = {$objectTypeId};";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			$typeId = false;
			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$typeId = array_shift($fetchResult);
			}
			return $cache[$objectTypeId] = $typeId;
		}

		/**
		 * Загружает во внутренний кеш класса id типов данных с определенным гуидами
		 * @throws coreException
		 */
		private function loadTypeIdsByGuids() {
			$mainConfiguration = mainConfiguration::getInstance();
			$guids = $mainConfiguration->get('kernel', 'objects-types-guids-preload');
			if (!is_array($guids) || count($guids) == 0) {
				return;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$guids = array_map([$connection, 'escape'], $guids);
			$guidClause = implode("', '", $guids);
			$sql = "SELECT `id`, `guid` FROM `cms3_object_types` WHERE `guid` IN ('{$guidClause}')";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			foreach ($result as $row) {
				$this->guidsToIds[$row['guid']] = $row['id'];
			}
		}

		/** @deprecated */
		public function getParentClassId($typeId) {
			return $this->getParentTypeId($typeId);
		}

		/** @deprecated */
		public function getChildClasses($typeId, $children = false) {
			return $this->getChildTypeIds($typeId, $children);
		}

		/** @deprecated */
		public function getTypeByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache = false) {
			return $this->getTypeIdByHierarchyTypeId($hierarchyTypeId, $ignoreMicroCache);
		}

		/** @deprecated */
		public function getBaseType($module, $method = "") {
			return $this->getTypeIdByHierarchyTypeName($module, $method);
		}

		/** @deprecated */
		public function isExists($type_id) {
			return true;
		}
	}
?>
