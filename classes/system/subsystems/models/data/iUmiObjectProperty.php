<?php
interface iUmiObjectProperty extends iUmiEntinty {
	public function getValue(array $params = NULL);
	public function setValue($value);
	public function resetValue();

	public function getName();
	public function getTitle();

	public function getIsMultiple();
	public function getIsUnsigned();
	public function getDataType();
	public function getIsLocked();
	public function getIsInheritable();
	public function getIsVisible();

	public static function filterOutputString($string);
	public static function filterCDATA($string);

	public function getObject();
	public function getField();

	/**
	 * Выключает использование транзакций
	 */
	public static function disableTransactionMode();
	/**
	 * Включает использование транзакций
	 */
	public static function enableTransactionMode();
	/**
	 * Проверяет включен ли режим транзакций
	 * @return bool
	 */
	public static function isTransactionModeEnabled();
}