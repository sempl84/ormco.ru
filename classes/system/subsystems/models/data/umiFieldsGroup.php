<?php

	/**
	 * Этот класс реализует объединение полей в именованные группы.
	 */
	class umiFieldsGroup extends umiEntinty implements iUmiEntinty, iUmiFieldsGroup {
		private $name,
				$title,
				$type_id,
				$ord,
				$is_active = true,
				$is_visible = true,
				$is_locked = false,
			/** @var string подсказка группы полей */
				$tip = '',
				$autoload_fields = false,
				$fields = [];

		protected $store_type = "fields_group";

		/**
		 * Получить строковой id группы
		 * @return String строковой id группы
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * Получить название группы
		 * @return String название группы в текущей языковой версии
		 */
		public function getTitle() {
			return $this->translateLabel($this->title);
		}

		/**
		 * Получить id типа данных, к которому относится группа полей
		 * @return Integer id типа данных (класс umiObjectType)
		 */
		public function getTypeId() {
			return $this->type_id;
		}

		/**
		 * Получить порядковый номер группы, по которому она сортируется в рамках типа данных
		 * @return Integer порядковый номер
		 */
		public function getOrd() {
			return $this->ord;
		}

		/**
		 * Узнать, активна ли группа полей
		 * @return Boolean значение флага активности
		 */
		public function getIsActive() {
			return $this->is_active;
		}

		/**
		 * Узнать, видима ли группа полей
		 * @return Boolean значение флага видимости
		 */
		public function getIsVisible() {
			return $this->is_visible;
		}

		/**
		 * Узнать, заблокирована ли группа полей (разработчиком)
		 * @return Boolean значение флага блокировка
		 */
		public function getIsLocked() {
			return $this->is_locked;
		}

		/**
		 * Возвращает подсказку группы полей
		 * @return string текст подсказки
		 */
		public function getTip() {
			return $this->translateLabel($this->tip);
		}

		/**
		 * Изменить строковой id группы на $name
		 * @param String $name новый строковой id группы полей
		 */
		public function setName($name) {
			if ($this->getName() != $name) {
				$name = umiHierarchy::convertAltName($name, '_');
				$name = umiObjectProperty::filterInputString($name);
				$name = (strlen($name)) ? $name : '_';
				$this->name = $name;
				$this->setIsUpdated();
			}
		}

		/**
		 * Изменить название группы полей
		 * @param string $title новое название группы полей
		 */
		public function setTitle($title) {
			if ($this->getTitle() != $title) {
				$title = $this->translateI18n($title, "fields-group");
				$title = umiObjectProperty::filterInputString($title);
				$this->title = $title;
				$this->setIsUpdated();
			}
		}

		/**
		 * Изменить тип данных, которому принадлежит группа полей
		 * @param Integer $type_id id нового типа данных (класс umiObjectType)
		 * @return Boolean true
		 */
		public function setTypeId($type_id) {
			$type_id = (int) $type_id;

			if ($this->getTypeId() != $type_id) {
				$this->type_id = $type_id;
				$this->setIsUpdated();
			}

			return true;
		}

		/**
		 * Установить новое значение порядка сортировки
		 * @param Integer $ord новый порядковый номер
		 */
		public function setOrd($ord) {
			$ord = (int) $ord;

			if ($this->getOrd() != $ord) {
				$this->ord = $ord;
				$this->setIsUpdated();
			}
		}

		/**
		 * Изменить активность группы полей
		 * @param Boolean $is_active новое значение флага активности
		 */
		public function setIsActive($is_active) {
			$is_active = (bool) $is_active;

			if ($this->getIsActive() != $is_active) {
				$this->is_active = $is_active;
				$this->setIsUpdated();
			}
		}

		/**
		 * Изменить видимость группы полей
		 * @param Boolean $is_visible новое значение флага видимости
		 */
		public function setIsVisible($is_visible) {
			$is_visible = (bool) $is_visible;

			if ($this->getIsVisible() != $is_visible) {
				$this->is_visible = $is_visible;
				$this->setIsUpdated();
			}
		}

		/**
		 * Изменить стостояние блокировки группы полей
		 * @param Boolean $is_locked новое значение флага блокировки
		 */
		public function setIsLocked($is_locked) {
			$is_locked = (bool) $is_locked;

			if ($this->getIsLocked() != $is_locked) {
				$this->is_locked = $is_locked;
				$this->setIsUpdated();
			}
		}

		/**
		 * Устанавливает новую подсказку для группы полей
		 * @param string $newTip текст новой подсказки
		 */
		public function setTip($newTip) {
			if ($this->getTip() != $newTip) {
				$tip = $this->translateI18n($newTip, "fields-group");
				$newTip = umiObjectProperty::filterInputString($tip);
				$this->tip = $newTip;
				$this->setIsUpdated();
			}
		}

		protected function loadInfo($row = false) {
			if ($row === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$sql = "SELECT id, name, title, type_id, is_active, is_visible, is_locked, tip, ord FROM cms3_object_field_groups WHERE id = '{$this->id}'";
				$result = $connection->queryResult($sql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}

				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();
			}

			if (list($id, $name, $title, $type_id, $is_active, $is_visible, $is_locked, $tip, $ord) = $row) {
				$this->name = $name;
				$this->title = $title;
				$this->type_id = $type_id;
				$this->is_active = (bool) $is_active;
				$this->is_visible = (bool) $is_visible;
				$this->is_locked = (bool) $is_locked;
				$this->tip = (string) $tip;
				$this->ord = (int) $ord;

				if ($this->autoload_fields) {
					return $this->loadFields();
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		protected function save() {
			if (!$this->getIsUpdated()) {
				return true;
			}

			$name = $this->name;
			$title = $this->title;
			$type_id = (int) $this->type_id;
			$is_active = (int) $this->is_active;
			$is_visible = (int) $this->is_visible;
			$ord = (int) $this->ord;
			$is_locked = (int) $this->is_locked;
			$tip = $this->tip;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<QUERY
UPDATE cms3_object_field_groups
SET    NAME = '{$name}',
       title = '{$title}',
       type_id = '{$type_id}',
       is_active = '{$is_active}',
       is_visible = '{$is_visible}',
       ord = '{$ord}',
       is_locked = '{$is_locked}',
       tip = '{$tip}'
WHERE  id = '{$this->id}'
QUERY;
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			return true;
		}

		/**
		 * Не используйте этот метод в прикладном коде.
		 */
		public function loadFields($rows = false) {
			$fields = umiFieldsCollection::getInstance();

			if ($rows === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$sql = "SELECT cof.id, cof.name, cof.title, cof.is_locked, cof.is_inheritable, cof.is_visible, cof.field_type_id, cof.guide_id, cof.in_search, cof.in_filter, cof.tip, cof.is_required, cof.sortable, cof.is_system, cof.restriction_id, cof.is_important FROM cms3_fields_controller cfc, cms3_object_fields cof WHERE cfc.group_id = '{$this->id}' AND cof.id = cfc.field_id ORDER BY cfc.ord ASC";
				$result = $connection->queryResult($sql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}

				$result->setFetchType(IQueryResult::FETCH_ROW);

				foreach ($result as $row) {
					list($field_id) = $row;
					$field = $fields->getField($field_id, $row);
					if ($field instanceof iUmiField) {
						$this->fields[$field_id] = $field;
					}
				}
			} else {
				foreach ($rows as $row) {
					list($field_id) = $row;
					if ($field = $fields->getField($field_id, $row)) {
						$this->fields[$field_id] = $field;
					}
				}
			}
		}

		/**
		 * Получить список всех полей в группе
		 * @return umiField[]
		 */
		public function getFields() {
			return $this->fields;
		}

		private function isLoaded($field_id) {
			return (bool) array_key_exists($field_id, $this->fields);
		}

		/**
		 * Присоединить к группе еще одно поле
		 * @param Integer $field_id id присоединяемого поля
		 * @param Boolean $ignor_loaded если true, то можно будет добавлять уже внесенные в эту группу поля
		 * @return bool
		 * @throws coreException
		 */
		public function attachField($field_id, $ignore_loaded = false) {
			if ($this->isLoaded($field_id) && !$ignore_loaded) {
				return true;
			}

			$field_id = (int) $field_id;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT MAX(ord) FROM cms3_fields_controller WHERE group_id = '{$this->id}'";
			$result = $connection->queryResult($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			$result->setFetchType(IQueryResult::FETCH_ROW);
			$ord = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$ord = array_shift($fetchResult);
			}

			$ord += 5;

			$sql = "INSERT INTO cms3_fields_controller (field_id, group_id, ord) VALUES('{$field_id}', '{$this->id}', '{$ord}')";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			cacheFrontend::getInstance()->flush();

			$fields = umiFieldsCollection::getInstance();
			$field = $fields->getField($field_id);
			$this->fields[$field_id] = $field;
			umiTypesHelper::getInstance()->unloadObjectType($this->getTypeId());

			return true;
		}

		/**
		 * Вывести поле из группы полей.
		 * При этом поле физически не удаляется, так как может одновременно фигурировать в разный группах полей разных
		 * типов данных.
		 * @param Integer $field_id id поля (класс umiField)
		 * @return Boolean результат операции
		 * @throws coreException
		 */
		public function detachField($field_id) {
			if (!$this->isLoaded($field_id)) {
				return false;
			}

			$field_id = (int) $field_id;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms3_fields_controller WHERE field_id = '{$field_id}' AND group_id = '{$this->id}'";
			$connection->query($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			unset($this->fields[$field_id]);
			umiTypesHelper::getInstance()->unloadObjectType($this->getTypeId());

			$sql = "SELECT COUNT(*) FROM cms3_fields_controller WHERE field_id = '{$field_id}'";
			$result = $connection->queryResult($sql);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			cacheFrontend::getInstance()->flush();
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$count = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$count = array_shift($fetchResult);
			}

			return ($count == 0) ? umiFieldsCollection::getInstance()->delField($field_id) : true;
		}

		/**
		 * Переместить поле $field_id после поля $after_field_id в группе $group_id
		 * @param Integer $field_id id перемещаемого поля
		 * @param Integer $after_field_id id поля, после которого нужно расположить перемещаемое поле
		 * @param Integer $group_id id группы полей, в которой производятся перемещения
		 * @param Boolean $is_last переместить в конец
		 * @return Boolean результат операции
		 * @throws coreException
		 */
		public function moveFieldAfter($field_id, $after_field_id, $group_id, $is_last) {
			$connection = ConnectionPool::getInstance()->getConnection();

			if ($after_field_id == 0) {
				$neword = 0;
			} else {
				$sql = "SELECT ord FROM cms3_fields_controller WHERE group_id = '{$group_id}' AND field_id = '{$after_field_id}'";
				$result = $connection->queryResult($sql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}

				$result->setFetchType(IQueryResult::FETCH_ROW);
				$neword = 0;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$neword = array_shift($fetchResult);
				}
			}

			if ($is_last) {
				$sql = "UPDATE cms3_fields_controller SET ord = (ord + 1) WHERE group_id = '{$this->id}' AND ord >= '{$neword}'";
				$connection->query($sql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}
			} else {
				$sql = "SELECT MAX(ord) FROM cms3_fields_controller WHERE group_id = '{$group_id}'";
				$result = $connection->queryResult($sql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($sql));
				}

				$result->setFetchType(IQueryResult::FETCH_ROW);
				$neword = 0;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$neword = array_shift($fetchResult);
				}
				++$neword;
			}

			$sql = "UPDATE cms3_fields_controller SET ord = '{$neword}', group_id = '$group_id' WHERE group_id = '{$this->id}' AND field_id = '{$field_id}'";
			$connection->query($sql);

			cacheFrontend::getInstance()->flush();

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			return true;
		}

		public function commit() {
			parent::commit();
			cacheFrontend::getInstance()->flush();
		}

		/**
		 * Получить список всех групп с названием $name вне зависимости от типа данных
		 * @param String $name название группы полей
		 * @return Array массив из объектов umiFieldsGroup
		 */
		public static function getAllGroupsByName($name) {
			$connection = ConnectionPool::getInstance()->getConnection();

			if ($name) {
				$name = $connection->escape($name);
			} else {
				return false;
			}

			$sql = "SELECT `id` FROM `cms3_object_field_groups` WHERE `name` = '{$name}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$groups = [];

			foreach ($result as $row) {
				$group = new umiFieldsGroup(array_shift($row));

				if (!$group instanceof umiFieldsGroup) {
					continue;
				}

				$groups[] = $group;
			}

			return $groups;
		}
	}

?>
