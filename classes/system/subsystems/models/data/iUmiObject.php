<?php
interface iUmiObject extends iUmiEntinty {
	public function getName($translate_ignored = false);
	public function setName($name);

	public function getIsLocked();
	public function setIsLocked($isLocked);

	public function getTypeId();
	public function getTypeGUID();
	public function setTypeId($typeId);

	public function getPropGroupId($groupName);
	public function getPropGroupByName($groupName);
	public function getPropGroupById($groupId);

	public function getPropByName($propName);
	public function getPropById($propId);

	public function isPropertyExists($id);

	public function isFilled();

	public function getValue($propName, $params = NULL);

	/**
	 * Возвращает значение свойства объекта по ID поля
	 * @param int $fieldId ID поля
	 * @param null|array $params дополнительные параметры
	 * @return bool
	 */
	public function getValueById($fieldId, $params = null);
	public function setValue($propName, $propValue);

	public function setOwnerId($ownerId);
	public function getOwnerId();

	public function getUpdateTime();
	public function setUpdateTime($updateTime);

	public function getOrder();
	public function setOrder($order);

	/**
	 * Возвращает GUID (Global Umi ID)
	 * @return string
	 */
	public function getGUID();
	/**
	 * Устанавливает GUID (Global Umi ID)
	 * @param string $guid
	 * @throws coreException если GUID уже используется
	 */
	public function setGUID($guid);
}