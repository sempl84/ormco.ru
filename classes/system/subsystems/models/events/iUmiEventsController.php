<?php
interface iUmiEventsController {
	public function callEvent(iUmiEventPoint $eventPoint);
	static public function registerEventListener(iUmiEventListener $eventListener);
}