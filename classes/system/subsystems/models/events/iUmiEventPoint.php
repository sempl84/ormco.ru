<?php
interface iUmiEventPoint {
	public function __construct($eventId);
	public function getEventId();

	public function setMode($eventPointMode = "process");
	public function getMode();

	public function setParam($paramName, $paramValue = NULL);
	public function addRef($paramName, &$paramValue);

	public function getParam($paramName);
	public function &getRef($refName);
	public function call();
}