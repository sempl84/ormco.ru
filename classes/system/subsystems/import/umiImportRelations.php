<?php
	class umiImportRelations extends singleton implements iUmiImportRelations {
		protected function __construct() {
		}

		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}


		public function getSourceId($source_name) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_name = $connection->escape($source_name);

			$sql = "SELECT id FROM cms3_import_sources WHERE source_name = '{$source_name}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$source_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$source_id = array_shift($fetchResult);
			}

			return ($source_id) ? $source_id : false;
		}


		public function addNewSource($source_name) {
			if ($source_id = $this->getSourceId($source_name)) {
				return $source_id;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$source_name = $connection->escape($source_name);

			$sql = "INSERT INTO cms3_import_sources (source_name) VALUES('{$source_name}')";
			$connection->query($sql, true);

			return $connection->insertId();
		}


		public function setIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			if (!$new_id) {
				return false;
			}

			$sql = "DELETE FROM cms3_import_relations WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_relations (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_relations WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id =  $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_relations WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

		public function setObjectIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			if (!$new_id) {
				return false;
			}

			$sql = "DELETE FROM cms3_import_objects WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_objects (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewObjectIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_objects WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldObjectIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_objects WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

		public function setTypeIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_types WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_types (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewTypeIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_types WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldTypeIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_types WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}


		public function setFieldIdRelation($source_id, $type_id, $old_field_name, $new_field_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$old_field_name = $connection->escape($old_field_name);
			$new_field_id = $connection->escape($new_field_id);


			$sql = "DELETE FROM cms3_import_fields WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND (field_name = '{$old_field_name}' OR new_id = '{$new_field_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_fields (source_id, type_id, field_name, new_id) VALUES('{$source_id}', '{$type_id}', '{$old_field_name}', '{$new_field_id}')";
			$connection->query($sql);

			return (string) $new_field_id;
		}


		public function getNewFieldId($source_id, $type_id, $old_field_name) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$old_field_name = $connection->escape($old_field_name);

			$sql = "SELECT new_id FROM cms3_import_fields WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND field_name = '{$old_field_name}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_field_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_field_id = array_shift($fetchResult);
			}

			return ($new_field_id) ? (string) $new_field_id : false;
		}

		public function getOldFieldName($source_id, $type_id, $new_field_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$new_field_id = $connection->escape($new_field_id);

			$sql = "SELECT field_name FROM cms3_import_fields WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND new_id = '{$new_field_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_field_name = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_field_name = array_shift($fetchResult);
			}

			return ($old_field_name) ? (string) $old_field_name : false;
		}

		public function setGroupIdRelation($source_id, $type_id, $old_group_name, $new_group_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$old_group_name = $connection->escape($old_group_name);
			$new_group_id = $connection->escape($new_group_id);


			$sql = "DELETE FROM cms3_import_groups WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND (group_name = '{$old_group_name}' OR new_id = '{$new_group_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_groups (source_id, type_id, group_name, new_id) VALUES('{$source_id}', '{$type_id}', '{$old_group_name}', '{$new_group_id}')";
			$connection->query($sql);


			return (string) $new_group_id;
		}

		public function getNewGroupId($source_id, $type_id, $old_group_name) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$old_group_name = $connection->escape($old_group_name);

			$sql = "SELECT new_id FROM cms3_import_groups WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND group_name = '{$old_group_name}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_group_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_group_id = array_shift($fetchResult);
			}

			return ($new_group_id) ? (string) $new_group_id : false;
		}

		public function getOldGroupName($source_id, $type_id, $new_group_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$type_id = $connection->escape($type_id);
			$new_group_id = $connection->escape($new_group_id);

			$sql = "SELECT group_name FROM cms3_import_groups WHERE source_id = '{$source_id}' AND type_id = '{$type_id}' AND new_id = '{$new_group_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_group_name = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_group_name = array_shift($fetchResult);
			}

			return ($old_group_name) ? $connection->escape($old_group_name) : false;
		}

		public function setDomainIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_domains WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_domains (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewDomainIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_domains WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldDomainIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_domains WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

		public function setDomainMirrorIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_domain_mirrors WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_domain_mirrors (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewDomainMirrorIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_domain_mirrors WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldDomainMirrorIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_domain_mirrors WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

		public function setLangIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_langs WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_langs (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewLangIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_langs WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldLangIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_langs WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}
		public function setTemplateIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_templates WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_templates (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewTemplateIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_templates WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldTemplateIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_templates WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

		public function setRestrictionIdRelation($source_id, $old_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);
			$new_id = $connection->escape($new_id);

			$sql = "DELETE FROM cms3_import_restrictions WHERE source_id = '{$source_id}' AND (new_id = '{$new_id}' OR old_id = '{$old_id}')";
			$connection->query($sql);

			$sql = "INSERT INTO cms3_import_restrictions (source_id, old_id, new_id) VALUES('{$source_id}', '{$old_id}', '{$new_id}')";
			$connection->query($sql);

			return true;
		}


		public function getNewRestrictionIdRelation($source_id, $old_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$old_id = $connection->escape($old_id);

			$sql = "SELECT new_id FROM cms3_import_restrictions WHERE old_id = '{$old_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$new_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$new_id = array_shift($fetchResult);
			}

			return ($new_id) ? (string) $new_id : false;
		}


		public function getOldRestrictionIdRelation($source_id, $new_id) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$source_id = $connection->escape($source_id);
			$new_id = $connection->escape($new_id);

			$sql = "SELECT old_id FROM cms3_import_restrictions WHERE new_id = '{$new_id}' AND source_id = '{$source_id}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$old_id = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$old_id = array_shift($fetchResult);
			}

			return ($old_id) ? (string) $old_id : false;
		}

	};
?>
