<?php
	class csvSplitter extends umiImportSplitter {
		/** @var string $encoding наименование кодировки, в которой находятся импортируемые данные */
		private $encoding = 'windows-1251';
		/** @var string $delimiter разделитель полей */
		private $delimiter = ';';
		/** @const VALUE_LIMITER символ, обрамляющий значения полей */
		const VALUE_LIMITER = '"';
		/** @const TARGET_ENCODING наименование кодировки, в которую будут преобразованы данные */
		const TARGET_ENCODING = 'utf-8';
		/** @const VERSION версия umiDump */
		const VERSION = "2.0";
		/** @var string $sourceName имя источника импорта */
		protected $sourceName;
		/** @var iUmiImportRelations $relations */
		protected $relations;
		/** @var array $names список имен полей */
		protected $names = [];
		/** @var array $types список кодовых названий типов полей */
		protected $types = [];
		/** @var array $titles список заголовков полей */
		protected $titles = [];
		/** @var array список поддерживаемых кодировок */
		protected static $supportedEncodings = ['utf-8', 'windows-1251', 'cp1251'];
		/** @var bool $autoGuideCreation создавать ли справочники автоматически */
		public $autoGuideCreation = true;
		/** @var bool сбрасывать ли значение параметра объекта при пустом значении */
		private $resetVal = false;

		/**
		 * Устанавливает кодировку, в которой находятся импортируемые данные
		 * @param string $encoding наименование устанавливаемой кодировки
		 * @throws InvalidArgumentException если кодировка не поддерживается
		 */
		public function setEncoding($encoding) {
			if (in_array(strtolower($encoding), self::$supportedEncodings)) {
				$this->encoding = $encoding;
			} else {
				throw new InvalidArgumentException("Encoding '${encoding}' is not supported");
			}
		}

		/**
		 * Конструктор класса
		 * @param string $type идентификатор формата экспорта
		 */
		public function __construct($type) {
			parent::__construct($type);
			ini_set('mbstring.substitute_character', "none");
		}

		protected function createGrid(DOMDocument $doc) {
			$root = $doc->createElement('umidump');
			$root->setAttribute('xmlns:xlink', 'http://www.w3.org/TR/xlink');

			$a = $doc->createAttribute('version');
			$a->appendChild($doc->createTextNode(self::VERSION));
			$root->appendChild($a);

			$doc->appendChild($root);

			$m = $doc->createElement('meta');

			$cmsController = cmsController::getInstance();
			$domain = $cmsController->getCurrentDomain();
			$lang = $cmsController->getCurrentLang();

			$n = $doc->createElement('site-name');
			$n->appendChild($doc->createCDATASection(def_module::parseTPLMacroses(macros_sitename())));
			$m->appendChild($n);

			$n = $doc->createElement('domain');
			$n->appendChild($doc->createCDATASection($domain->getHost()));
			$m->appendChild($n);

			$n = $doc->createElement('lang');
			$n->appendChild($doc->createCDATASection($lang->getPrefix()));
			$m->appendChild($n);

			$n = $doc->createElement('source-name');
			$val = strlen($this->sourceName) ? $this->sourceName : md5($domain->getId() . $lang->getId());
			$n->appendChild($doc->createCDATASection($val));
			$m->appendChild($n);

			$n = $doc->createElement('generate-time');

			$date = new umiDate(time());

			$t = $doc->createElement('timestamp');
			$t->appendChild($doc->createTextNode($date->getFormattedDate('U')));
			$n->appendChild($t);

			$t = $doc->createElement('rfc');
			$t->appendChild($doc->createTextNode($date->getFormattedDate('r')));
			$n->appendChild($t);

			$t = $doc->createElement('utc');
			$t->appendChild($doc->createTextNode($date->getFormattedDate(DATE_ATOM)));
			$n->appendChild($t);

			$m->appendChild($n);

			$root->appendChild($m);

			return $root;
		}

		public function readDataBlock() {
			$file = new umiFile ($this->file_path);
			if ($file) {
				$this->sourceName = $file->getFileName();
			}

			$this->relations = umiImportRelations::getInstance();
			$this->relations->addNewSource($this->sourceName);

			$doc = new DOMDocument('1.0', 'utf-8');
			$doc->formatOutput = XML_FORMAT_OUTPUT;

			$root = $this->createGrid($doc);

			$pages = $doc->createElement('pages');
			$root->appendChild($pages);

			$hierarchy = $doc->createElement('hierarchy');
			$root->appendChild($hierarchy);

			$handle = fopen($this->file_path, 'r');
			$continue = (bool) $handle;
			$position = 0;

			while ($continue && ($string = fgets($handle))) {
				$c = substr_count($string, self::VALUE_LIMITER);
				if ($c % 2 !== 0) {
					while (!feof($handle)) {
						$part = fgets($handle);
						$string .= $part;
						$c += substr_count($part, self::VALUE_LIMITER);

						if ($c % 2 === 0) {
							break;
						}
					}
				}

				++$position;

				$string = html_entity_decode($string, ENT_QUOTES, $this->encoding);
				$string = preg_replace("/([^{$this->delimiter}])" . str_repeat(self::VALUE_LIMITER, 2) . "/s", "$1'*//*'", $string);
				preg_match_all("/" . self::VALUE_LIMITER . "(.*?)" . self::VALUE_LIMITER . "/s", $string, $matches);

				foreach ($matches[0] as $quotes) {
					$newQuotes = str_replace($this->delimiter, "'////'", $quotes);
					$string = str_replace($quotes, $newQuotes, $string);
				}
				$string = preg_replace("/(.+)" . $this->delimiter . "$/s", "$1", trim($string));

				$buffer = explode($this->delimiter, $string);

				foreach ($buffer as &$value) {
					$clean = $value;
					$clean = mb_convert_encoding($clean, self::TARGET_ENCODING, $this->encoding);
					$clean = str_replace(["'////'", "'*//*'"], [$this->delimiter, self::VALUE_LIMITER], $clean);
					$clean = preg_replace("/^" . self::VALUE_LIMITER . "(.*)" . self::VALUE_LIMITER . "$/s", "$1", $clean);
					$value = trim($clean);
				}
				unset($value);

				if ($position < 4) {
					if ($position === 1) {
						foreach ($buffer as $key => $value) {
							$this->names[$key] = $value;
						}
					} else {
						if ($position === 2) {
							foreach ($buffer as $key => $value) {
								$this->titles[$key] = $value;
							}
						} else {
							if ($position === 3) {
								foreach ($buffer as $key => $value) {
									$this->types[$key] = $value;
								}

								if ($this->offset) {
									fseek($handle, $this->offset);
								}
							}
						}
					}
				} else {
					$this->addElementInfo($doc, $buffer);
					if (($position - 3) >= $this->block_size) {
						break;
					}
				}
			}

			if (feof($handle)) {
				$continue = false;
			}
			$this->offset = ftell($handle);
			if (!$continue) {
				$this->complete = true;
			}

			return $doc;
		}

		protected function addElementInfo($doc, $info) {
			$page = $doc->createElement('page');
			$pages = $doc->getElementsByTagName('pages')->item(0);
			$hierarchy = $doc->getElementsByTagName('hierarchy')->item(0);
			$pages->appendChild($page);

			$sourceId = $this->relations->getSourceId($this->sourceName);

			$typeId = false;

			$key = array_search('type-id', $this->names, true);
			if ($key !== false) {
				$typeIdentifier = $info[$key];
				if (is_numeric($typeIdentifier)) {
					$typeId = $this->relations->getNewTypeIdRelation($sourceId, $typeIdentifier);
					if (!$typeId) {
						$type = umiObjectTypesCollection::getInstance()->getType($typeIdentifier);
						if ($type instanceof umiObjectType) {
							$typeId = $typeIdentifier;
							$this->relations->setTypeIdRelation($sourceId, $typeId, $typeId);
						}
					}
				}
			}

			$parentId = 0;
			$importId = getRequest('param0');

			$key = array_search('parent-id', $this->names, true);
			if ($key !== false) {
				$parentId = $info[$key];
			}
			$page->setAttribute('parentId', $parentId);

			$idKey = array_search('id', $this->names, true);

			if ($idKey !== false && $info[$idKey]) {
				$h = $doc->createElement('relation');
				$h->setAttribute('id', $info[$idKey]);
				$h->setAttribute('parent-id', $parentId);
				$hierarchy->appendChild($h);
			}

			if (!$typeId) {
				if (!$parentId) {
					if ($importId) {
						$elements = umiObjectsCollection::getInstance()->getObject($importId)->elements;

						if (is_array($elements) && count($elements)) {
							$parentId = $elements[0]->getId();
						}
					}
				}

				if ($parentId) {
					$typeId = umiHierarchy::getInstance()->getDominantTypeId($parentId);
				}
			}

			if (!$typeId) {
				$typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('content');
			}

			if ($typeId != $this->relations->getNewTypeIdRelation($sourceId, $typeId)) {
				$this->relations->setTypeIdRelation($sourceId, $typeId, $typeId);
			}

			$page->setAttribute('type-id', $typeId);
			$this->addPropertiesInfo($info, $page, $doc);
		}

		protected function addPropertiesInfo($info, $entity, $doc) {
			$properties = $doc->createElement('properties');
			$entity->appendChild($properties);

			$group = $doc->createElement('group');
			$properties->appendChild($group);
			$group->setAttribute('name', 'newGroup');

			foreach ($info as $key => $value) {
				if (!isset($this->names[$key])) {
					continue;
				}

				$isEmpty = !strlen($value);

				if ($isEmpty && !$this->resetVal && !isset($this->types[$key])) {
					continue;
				}

				$value = strtr($value, [
								"&" => "&amp;",
								"<" => "&lt;",
								">" => "&gt;"]
				);

				if ($this->names[$key] === 'id') {
					$entity->setAttribute('id', $value);
					continue;
				}

				if ($this->names[$key] === 'is-active') {
					$entity->setAttribute('is-active', $value);
					continue;
				}

				if ($this->names[$key] === 'is-visible') {
					$entity->setAttribute('is-visible', $value);
					continue;
				}

				if ($this->names[$key] === 'is-deleted') {
					$entity->setAttribute('is-deleted', $value);
					continue;
				}

				if ($this->names[$key] === 'name') {
					$name = $doc->createElement('name', $value);
					$entity->appendChild($name);
					continue;
				}

				if ($this->names[$key] === 'type-id' || $this->names[$key] === 'parent-id') {
					continue;
				}

				if ($this->names[$key] === 'template-id') {
					$template = templatesCollection::getInstance()->getTemplate($value);
					if ($template instanceof template) {
						$tpl = $doc->createElement('template', $template->getFilename());
						$entity->appendChild($tpl);
						$tpl->setAttribute('id', $value);
					}
					continue;
				}

				$property = $doc->createElement('property');
				$group->appendChild($property);

				$dataType = $this->types[$key];
				$multiple = (in_array($dataType, $this->getMultipleDataTypes())) ? true : false;
				$dataType = ($dataType === 'multiple-relation') ? 'relation' : $dataType;

				$fieldType = umiFieldTypesCollection::getInstance()->getFieldTypeByDataType($dataType, $multiple);

				if (!$fieldType instanceof umiFieldType) {
					throw new coreException('Wrong datatype "' . $dataType . '" is given for property "' . $this->names[$key] . '"');
				}

				$typeName = $fieldType->getName();

				$property->setAttribute('name', $this->names[$key]);
				$property->setAttribute('title', $this->titles[$key]);
				$property->setAttribute('type', $dataType);
				if ($multiple) {
					$property->setAttribute('multiple', 'multiple');
				}
				$property->setAttribute('visible', '1');
				$property->setAttribute('allow-runtime-add', intval(!$isEmpty));

				$type = $doc->createElement('type');
				$property->appendChild($type);
				$type->setAttribute('data-type', $dataType);

				$type->setAttribute('name', $typeName);

				$title = $doc->createElement('title', $this->titles[$key]);
				$property->appendChild($title);

				if ($dataType === 'relation') {
					$propertyValue = $doc->createElement('value');
					$property->appendChild($propertyValue);
					$values = ($multiple) ? explode(',', $value) : [$value];

					foreach ($values as $valueItem) {
						$item = $doc->createElement('item');
						$item->setAttribute('name', $valueItem);
						$propertyValue->appendChild($item);
					}
				} elseif ($dataType === 'tags') {
					$values = explode(',', $value);
					foreach ($values as $valueItem) {
						$item = $doc->createElement('value', trim($valueItem));
						$property->appendChild($item);
					}
					$propertyValue = $doc->createElement('combined', $value);
					$property->appendChild($propertyValue);
				} else {
					$propertyValue = $doc->createElement('value', $value);
					$property->appendChild($propertyValue);
				}
			}
		}

		public function translate(DomDocument $doc) {
			// do nothing
			return $doc->saveXML();
		}

		public function setResetVal($val) {
			$this->resetVal = $val;
		}

		/**
		 * Возвращает список типов данных полей, которые могут иметь несколько значений
		 * @return array
		 */
		private function getMultipleDataTypes() {
			return [
					'multiple-relation',
					'tags',
					'multiple_image',
					'symlink',
					'optioned'
			];
		}
	}