<?php
class csvcatalogSplitter extends umiImportSplitter {

	protected function readDataBlock() {
		// xml-пустышка
		$doc = new DOMDocument("1.0", "utf-8");
		$doc -> formatOutput = XML_FORMAT_OUTPUT;
		$rootNode = $doc -> createElement("umidump");
		$doc -> appendChild($rootNode);
		$rootNode -> setAttribute('version', '2.0');
		// текущее смещение
		$index = ($this -> offset) ? $this -> offset : 0;
		
		//$this->offset = 0; exit('ggg');
		//Какие-то действия, в зависимости от текущего смещения
		$start = ($this -> offset) ? $this -> offset + 1 : 1;
		$end = ($this -> offset) ? $start + $this -> block_size - 1 : $this -> block_size;
		$handle = fopen($this -> file_path, "r");
		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$objectTypes = umiObjectTypesCollection::getInstance();
		$cmsController = cmsController::getInstance();

		$main_catalog_id = 4;
		$lost_catalog_id = 8450;
		$catalog_hierarchy_type_id = 55;
		//hierarchy type id - "Объект каталога"
		$catalog_object_type_id = 84;
		// object type - "Объект каталога"
		// id раздела "игрушки"
		$row = 1;

		//Общие параметры
		$hierarchy = umiHierarchy::getInstance();
		$relation = umiImportRelations::getInstance();
		$perm = permissionsCollection::getInstance();
		$guestId = $perm -> getGuestId();
		// получаем id гостя
		$permLevel = 1;

		//создание нового импорта
		//$relation->addNewSource('csvcatalog'); exit('ggg');

		$source_csvcatalog = $relation -> getSourceId('csvcatalog');
		session::getInstance();
		//clear 
		if(!($this -> offset)) {
			unset($_SESSION['cat1']);
			unset($_SESSION['cat2']);
			unset($_SESSION['cat3']);
			unset($_SESSION['cat4']);
			unset($_SESSION['cat_last']);
		}
		
		file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "start (".date('H:i:s').") \n", FILE_APPEND);
		while (($string = fgets($handle)) !== FALSE) {
			file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "row - ".$row." (".date('H:i:s').") \n" . $string. "\n\n", FILE_APPEND);	
			if ($row >= $start and $row <= $end) {
				//подготовка данных и разбивка их в массив data
				if (substr_count($string, '"') % 2 != 0) {
					$isRecord = false;
					while (!feof($handle) && !$isRecord) {
						$string .= fgets($handle);
						if (substr_count($string, '"') % 2 == 0) {
							$isRecord = true;
						}
					}
				}
				//$string = html_entity_decode($string, ENT_QUOTES, 'cp1251');
				$string = preg_replace("/([^;])\"\"/s", "$1'*//*'", $string);
				preg_match_all("/\"(.*?)\"/s", $string, $matches);
				foreach ($matches[0] as $quotes) {
					$newQuotes = str_replace(";", "'////'", $quotes);
					$string = str_replace($quotes, $newQuotes, $string);
				}
				$string = preg_replace("/(.+);$/s", "$1", trim($string));
				
				$data = explode(";", $string);
				foreach ($data as $key => $value) {
					//$value = iconv('windows-1251', 'utf-8//IGNORE', $value);
					$value = str_replace("'////'", ";", $value);
					$value = str_replace("'*//*'", '"', $value);
					$value = preg_replace("/^\"(.*)\"$/s", "$1", $value);
					$value = trim($value);
					$data[$key] = $value;
				}
				//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end prepare data (".date('H:i:s').") \n", FILE_APPEND);	
			
				//data
				$cname1 = isset($data[0]) ? $data[0] : false;
				$cname2 = isset($data[1]) ? $data[1] : false;
				$cname3 = isset($data[2]) ? $data[2] : false;
				$cname4 = isset($data[3]) ? $data[3] : false;
				$oname = isset($data[4]) ? $data[4] : false;
				$oartikul = isset($data[5]) ? $data[5] : false;

				if (isset($data[14]))
					$alias = $data[14];
				else
					$alias = NULL;

				

				// создаем раздел запоминаем его как родителя в куку
				if ($cname1) {
					//проверяем создавался ли уже этот материал в системе
					
                    $new_category_id = $relation->getNewIdRelation($source_csvcatalog, md5($cname1.$main_catalog_id));
					if(!$new_category_id) {	 
						//Creating page...
						$new_category_id = $hierarchy -> addElement($main_catalog_id, $catalog_hierarchy_type_id, $cname1, $cname1, $catalog_object_type_id);
						$perm -> setElementPermissions($guestId, $new_category_id, $permLevel);
						
						$new_category = $hierarchy->getElement($new_category_id, true);
						$new_category->setIsActive(true);
	                    $new_category->setIsVisible(false);
						// записываем соответсвие в таблице БД: cms3_import_relations
						$relation -> setIdRelation($source_csvcatalog, md5($cname1.$main_catalog_id), $new_category_id);
						$hierarchy -> moveBefore($new_category_id, $main_catalog_id);
						
						//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end create category - ".$new_category_id." (".date('H:i:s').") \n", FILE_APPEND);	
			
					}
					
					
					$_SESSION['cat1'] = $new_category_id;
					$_SESSION['cat_last'] = $new_category_id;
					//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end working with category - ".$new_category_id." (".date('H:i:s').") \n", FILE_APPEND);	
			
					$row++;
					continue;
				}

				// создаем подраздел если есть кука
				if ($cname2||$cname3||$cname4) {
					$name = 'нет названия на строчке '.$row;
					$parent_cat_id = $lost_catalog_id;
						
					
					
					if ($cname2) {
						if(isset($_SESSION['cat1'])) {
							$parent_cat_id = $_SESSION['cat1'];
						}
						$name = $cname2;
					}elseif ($cname3) {
						if(isset($_SESSION['cat2'])) {
							$parent_cat_id = $_SESSION['cat2'];
						}
						$name = $cname3;
					}elseif ($cname4) {
						if(isset($_SESSION['cat3'])) {
							$parent_cat_id = $_SESSION['cat3'];
						}
						$name = $cname4;
					}
					
					//проверяем создавался ли уже этот материал в системе
                    $new_category_id = $relation->getNewIdRelation($source_csvcatalog, md5($name.$parent_cat_id));
					if(!$new_category_id) {	
						//Creating page...
						$new_category_id = $hierarchy -> addElement($parent_cat_id, $catalog_hierarchy_type_id, $name, $name, $catalog_object_type_id);
						$perm -> setElementPermissions($guestId, $new_category_id, $permLevel);
						
						$new_category = $hierarchy->getElement($new_category_id, true);
						$new_category->setIsActive(true);
	                    $new_category->setIsVisible(false);
						
						// записываем соответсвие в таблице БД: cms3_import_relations
						$relation -> setIdRelation($source_csvcatalog, md5($name.$parent_cat_id), $new_category_id);
						$hierarchy -> moveBefore($new_category_id, $parent_cat_id);
						
						//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end create category - ".$new_category_id." (".date('H:i:s').") \n", FILE_APPEND);	
			
					}
					
					if ($cname2) {
						$_SESSION['cat2'] = $new_category_id;
					}elseif ($cname3) {
						$_SESSION['cat3'] = $new_category_id;
					}elseif ($cname4) {
						$_SESSION['cat4'] = $new_category_id;
					}
					$_SESSION['cat_last'] = $new_category_id;
					
					//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end working with category - ".$new_category_id." (".date('H:i:s').") \n", FILE_APPEND);	
			
					$row++;
					continue;
				}
				
				
				// переносим туда товар если это товар и есть кука
				if ($oname && $oartikul) {
					//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "start working with tovar (".date('H:i:s').") \n", FILE_APPEND);	
				
					$parent_cat_id = $lost_catalog_id;
						
					if(isset($_SESSION['cat_last'])) {
						$parent_cat_id = $_SESSION['cat_last'];
					}
					//поиск товара с заданным артикулом
					$sel = new selector('pages');
					$sel -> types('object-type') -> name('catalog', 'object');
					$sel -> where('hierarchy')->page($parent_cat_id)->childs(2);
					$sel -> where('artikul') -> equals($oartikul);
					$sel -> limit(0, 1);
					$total = $sel -> length;
					//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end search tovar - ".$oartikul." in his category - ".$parent_cat_id." (".date('H:i:s').") \n", FILE_APPEND);	
			
					if (!($catalogObject = $sel -> first)){
						//поиск товара с заданным артикулом по всем товарам и перенос
						$sel = new selector('pages');
						$sel -> types('object-type') -> name('catalog', 'object');
						$sel -> where('artikul') -> equals($oartikul);
						$sel -> limit(0, 1);
						$total = $sel -> length;
						//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "search tovar anywhere by - ".$oartikul." (".date('H:i:s').") \n", FILE_APPEND);	
			
						if ($catalogObject = $sel -> first) {
							$catalogObjectId = $catalogObject -> id;
							$hierarchy -> moveBefore($catalogObjectId, $parent_cat_id);
							//file_put_contents(CURRENT_WORKING_DIR .'/testajax.txt', "end move toavar - ".$oartikul." (".date('H:i:s').") \n", FILE_APPEND);	
			
						}
					}
						
						
					
					$row++;
					continue;
				}
				

			}
			// для теста, вырубаем импорт на 2 строчке файла
			/*  if($row > 2) {
			 $this->complete = true;
			 break;
			 }  */
			if ($row > $end)
				break;
			$row++;
		}
		fclose($handle);
		$this -> offset += $this -> block_size;

		// условие для остановки цикла, которое необходимо настроить
		if (!isset($data) || !$data)
			$this -> complete = true;
		return $doc;
	}


	public function translate(DomDocument $doc) {
		return $doc -> saveXML();
	}

}
?>