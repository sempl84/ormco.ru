<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class BaseExtension implements IPhpExtension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __CLASS__;
    }
    
    const link_webforms_dispatch = '/webforms/dispatch/';
    
    const assets_webforms_system_fields = 'assets/webforms/system_fields';
    const assets_webforms_system_fields_param_form = 'form';
    const assets_webforms_system_fields_param_on_success = 'on_success';
    
    private $templatesDirectory = '';
    
    public function __construct()
    {
        $this->templatesDirectory = cmsController::getInstance()
            ->getTemplatesDirectory();
    }
    
    private $module;
    
    public function setModule($module)
    {
        $this->module = $module;
    }
    
    public function getModule()
    {
        return $this->module;
    }
    
    private $method;
    
    public function setMethod($method)
    {
        $this->method = $method;
    }
    
    public function getMethod()
    {
        return $this->method;
    }
    
    private $templateResources;
    
    /**
     * @return mixed
     */
    public function getTemplateResources()
    {
        return $this->templateResources;
    }
    
    /**
     * @param mixed $templateResources
     */
    public function setTemplateResources($templateResources)
    {
        $this->templateResources = $templateResources;
    }
    
    private $templateVersion = '';
    
    /**
     * @return string
     */
    public function getTemplateVersion()
    {
        return $this->templateVersion;
    }
    
    /**
     * @param string $templateVersion
     */
    public function setTemplateVersion($templateVersion)
    {
        $this->templateVersion = $templateVersion;
    }
    
    public function redirect($link = false)
    {
        if (!$link) {
            return false;
        }
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        /* @var $buffer HTTPOutputBuffer */
        $buffer->redirect($link);
        
        return true;
    }
    
    public function getExtendedProperties($variables)
    {
        $properties = array();
        
        if (isset($variables['extended'])) {
            if (isset($variables['extended']['properties'])) {
                foreach ($variables['extended']['properties'] as $property) {
                    /* @var umiObjectProperty $property */
                    
                    $properties[$property->getName()] = $property->getValue();
                }
            } else {
                foreach ($variables['extended'] as $property) {
                    /* @var umiObjectProperty $property */
                    
                    $properties[$property->getName()] = $property->getValue();
                }
            }
        }
        
        return $properties;
    }
    
    public function getExtendedProperty($variables, $name)
    {
        $return = null;
        
        if (isset($variables['extended'])) {
            if (isset($variables['extended']['properties'])) {
                foreach ($variables['extended']['properties'] as $property) {
                    /* @var umiObjectProperty $property */
                    
                    if ($property->getName() == $name) {
                        $return = $property->getValue();
                        break;
                    }
                }
            } else {
                foreach ($variables['extended'] as $property) {
                    /* @var umiObjectProperty $property */
                    
                    if ($property->getName() == $name) {
                        $return = $property->getValue();
                        break;
                    }
                }
            }
        }
        
        return $return;
    }
    
    public function getArrayItem($array, $filter = array(), $multiple = false)
    {
        if (!is_array($array)) {
            return false;
        }
        
        $return = array();
        
        foreach ($array as $item) {
            if (!is_array($item)) {
                continue;
            }
            
            $valid = false;
            foreach ($filter as $field => $value) {
                if (isset($item[$field]) && $item[$field] == $value) {
                    $valid = true;
                } else {
                    $valid = false;
                }
            }
            
            if (!$valid) {
                continue;
            }
            
            $return[] = $item;
        }
        
        return count($return) ? $multiple ? $return : $return[0] : false;
    }
    
    public function getAllFormFields($form)
    {
        $fields = array();
        
        $groups = getArrayKey($form, 'group');
        
        if (!$groups) {
            $groups = getArrayKey(getArrayKey($form, 'groups'), 'group');
        }
        
        if ($groups) {
            foreach ($groups as $group) {
                $arGroupFields = getArrayKey($group, 'fields');
                if (!$arGroupFields) {
                    $arGroupFields = getArrayKey($group, 'field');
                }
                
                if ($arGroupFields) {
                    foreach ($arGroupFields as $field) {
                        $fields[$field['name']] = $field;
                    }
                }
            }
        }
        
        return count($fields) ? $fields : false;
    }
    
    public function prepareHtmlAttributes($array)
    {
        if (!is_array($array)) {
            return '';
        }
        
        $attributes = array_map(function ($value, $key) {
            return $key . '="' . $value . '"';
        }, array_values($array), array_keys($array));
        
        return implode(' ', $attributes);
    }
    
    public function prepareFieldArray($name, $title, $inputName, $type, $value = false)
    {
        return array(
            'name' => $name,
            'title' => $title,
            'input_name' => $inputName,
            'type' => $type,
            'value' => $value
        );
    }
    
    public function preparePhoneLink($phone, $convertPlusToEight = false)
    {
        $phone = trim($phone);
        
        $phone = str_replace(array(' ', '-', '–', '(', ')'), '', $phone);
        
        if ($convertPlusToEight) {
            if ($phone[0] == '+') {
                $phone = '8' . substr($phone, 2);
            }
        }
        
        return 'tel:' . $phone;
    }
    
    public function prepareWhatsappLink($phone)
    {
        $phone = trim($phone);
        
        $phone = str_replace(array('+', ' ', '-', '–', '(', ')'), '', $phone);
        
        return 'https://api.whatsapp.com/send?phone=' . $phone;
    }
    
    /**
     * @return system
     */
    private function loadSystem()
    {
        static $system;
        
        if (!$system) {
            $system = system_buildin_load('system');
        }
        
        return $system;
    }
    
    public function makeThumbnail($path, $width = 'auto', $height = 'auto', $flags = 0, $quality = 100)
    {
        if ($path instanceof umiImageFile) {
            $path = $path->getFilePath();
        } elseif ($path[0] != '.') {
            $path = '.' . $path;
        }
        
        if (!$path) {
            return false;
        }
        
        $system = $this->loadSystem();
        
        $thumb = $system->makeThumbnail($path, $width, $height, '', true, $flags, $quality);
        
        return is_array($thumb) ? getArrayKey($thumb, 'src') : false;
    }
    
    public function makeThumbnailFull($path, $width = 'auto', $height = 'auto', $crop = 1, $cropSide = 5, $isLogo = 0, $quality = 100)
    {
        if ($path instanceof umiImageFile) {
            $path = $path->getFilePath();
        }
        
        if (!$path) {
            return false;
        }
        
        if (in_array(pathinfo($path, PATHINFO_EXTENSION), array('svg'))) {
            return $path;
        }
        
        if (is_string($path) && ($path[0] != '.')) {
            $path = '.' . $path;
        }
        
        $system = $this->loadSystem();
        
        try {
            $thumb = $system->makeThumbnailFull($path, $width, $height, '', true, $crop, $cropSide, $isLogo, $quality);
        } catch (Exception $e) {
            return false;
        }
        
        return is_array($thumb) ? getArrayKey($thumb, 'src') : false;
    }
    
    public function isMenuItemActive($rel = false, $isActive = false)
    {
        return ($rel == 'page') ? ($isActive == 1) : true;
    }
    
    /**
     * @param $status
     * @param $link
     * @param $items
     * @return bool
     */
    public function isMenuItemSelected($status = null, $link = null, $items = null)
    {
        if ($link && ltrim($link, '/') === Service::Request()->Get()->get('path')) {
            return true;
        } elseif (!$link && $status == 'active') {
            return true;
        }
        
        $return = false;
        
        if (is_array($items)) {
            foreach ($items as $subItem) {
                if (getArrayKey($subItem, 'status') == 'active') {
                    $return = true;
                    break;
                }
            }
        }
        
        return $return;
    }
    
    private $scriptFiles = array();
    
    public function addScriptFile($file, $params = array())
    {
        if (!in_array($file, $this->scriptFiles)) {
            $this->scriptFiles[] = array(
                'file' => $file,
                'params' => $params
            );
        }
    }
    
    public function getScriptFiles()
    {
        $code = array();
        
        foreach ($this->scriptFiles as $file) {
            $params = '';
            
            if (isset($file['params'])) {
                if (is_array($file['params'])) {
                    foreach ($file['params'] as $key => $value) {
                        $params .= ' ' . $key;
                        
                        if ($value !== true) {
                            $params .= '="' . $value . '"';
                        }
                    }
                }
            }
            
            $code[] = '<script type="text/javascript" src="' . $file['file'] . '"' . $params . '></script>';
        }
        
        return implode(PHP_EOL, $code);
    }
    
    private $javascript = array();
    
    public function addJavascript($script)
    {
        $this->javascript[] = $script;
    }
    
    public function getJavascript()
    {
        return count($this->javascript) ? '<script type="text/javascript">' . implode(PHP_EOL, $this->javascript) . '</script>' : '';
    }
    
    private $css = array();
    
    public function addCss($css)
    {
        $this->css[] = $css;
    }
    
    public function getCss()
    {
        return count($this->css) ? '<style>' . implode(PHP_EOL, $this->css) . '</style>' : '';
    }
    
    private $footerCode = '';
    
    public function addFooterCode($code)
    {
        $this->footerCode .= $code;
    }
    
    public function getFooterCode()
    {
        return $this->footerCode;
    }
    
    private $cssFiles = array();
    
    public function addCssFile($file)
    {
        $this->cssFiles[] = $file;
    }
    
    public function getCssFiles()
    {
        $code = array();
        
        foreach ($this->cssFiles as $file) {
            $code[] = sprintf('<link rel="stylesheet" type="text/css" href="%s" />', $file);
        }
        
        return implode(PHP_EOL, $code);
    }
    
    public function getNumEnding($number, $one = '', $two = '', $five = '')
    {
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $five;
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $one;
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $two;
                    break;
                default:
                    $ending = $five;
            }
        }
        return $ending;
    }
    
    public function getI18nDate($date, $yearPostfix = '')
    {
        $return = date('d', $date) . ' ' . $this->getI18nMonth($date) . ' ' . date('Y', $date);
        if ($yearPostfix) {
            $return .= ' ' . $yearPostfix;
        }
        
        return $return;
    }
    
    public function getI18nMonth($date)
    {
        $arMonth = array(
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        );
        
        return $arMonth[date('m', $date) - 1];
    }
    
    private $title;
    
    /**
     * @return mixed
     */
    public function getSiteMetaTitle()
    {
        return $this->title;
    }
    
    /**
     * @param mixed $title
     */
    public function setSiteMetaTitle($title)
    {
        $this->title = strip_tags($title);
    }
    
    private $metaKeywords;
    private $metaDescription;
    
    public function parseMeta($meta)
    {
        if (!is_array($meta)) {
            return false;
        }
        
        $this->metaKeywords = getArrayKey($meta, 'keywords');
        $this->metaDescription = getArrayKey($meta, 'description');
        
        return true;
    }
    
    /**
     * @return mixed
     */
    public function getSiteMetaKeywords()
    {
        return $this->metaKeywords;
    }
    
    /**
     * @param mixed $metaKeywords
     */
    public function setSiteMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }
    
    /**
     * @return mixed
     */
    public function getSiteMetaDescription()
    {
        return $this->metaDescription;
    }
    
    /**
     * @param mixed $metaDescription
     */
    public function setSiteMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }
    
    private $header;
    
    /**
     * @return mixed
     */
    public function getSiteHeader()
    {
        return $this->header;
    }
    
    /**
     * @param mixed $header
     */
    public function siteSiteHeader($header)
    {
        $this->header = strip_tags($header);
    }
    
    /**
     * @var boolean
     */
    private $isDev;
    
    /**
     * @return boolean
     */
    public function isIsDev()
    {
        return $this->isDev;
    }
    
    /**
     * @param boolean $isDev
     */
    public function setIsDev($isDev)
    {
        $this->isDev = $isDev;
    }
    
    private $lang;
    
    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }
    
    /**
     * @param mixed $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }
    
    private $langId;
    
    /**
     * @return mixed
     */
    public function getLangId()
    {
        return $this->langId;
    }
    
    /**
     * @param mixed $langId
     */
    public function setLangId($langId)
    {
        $this->langId = $langId;
    }
    
    private $preLang;
    
    /**
     * @return mixed
     */
    public function getPreLang()
    {
        return $this->preLang;
    }
    
    /**
     * @param mixed $preLang
     */
    public function setPreLang($preLang)
    {
        $this->preLang = $preLang;
    }
    
    private $domain;
    
    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }
    
    /**
     * @param mixed $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }
    
    public function prepareBlockObjectValues(umiObject $object, $fields = array())
    {
        $return = array();
        
        foreach ($fields as $field) {
            $value = $object->getValue($field);
            
            if ($value === null || $value === '') {
                continue;
            }
            
            $return[$field] = $value;
        }
        
        return $return;
    }
    
    public function stripHtmlComments($string)
    {
        return preg_replace('/<!--(?!<!)[^\[>].*?-->/Uis', '', $string);
    }
    
    private $modal = array();
    
    public function addModal($id, $html)
    {
        $this->modal[$id] = $html;
    }
    
    public function renderModal()
    {
        return implode(PHP_EOL, $this->modal);
    }
    
    public function getFormFieldsGroup($form, $name)
    {
        $groups = getArrayKey($form, 'group');
        
        if (!$groups) {
            $groups = getArrayKey(getArrayKey($form, 'groups'), 'group');
        }
        
        $return = false;
        if ($groups) {
            foreach ($groups as $group) {
                if (getArrayKey($group, 'name') != $name) {
                    continue;
                }
                
                $return = $group;
                break;
            }
        }
        
        return $return;
    }
    
    public function getGroupFields($group)
    {
        $return = array();
        
        $fields = getArrayKey($group, 'fields');
        if (!$fields) {
            $fields = getArrayKey($group, 'field');
        }
        
        if ($fields) {
            foreach ($fields as $field) {
                $return[$field['name']] = $field;
            }
        }
        
        return $return;
    }
    
    public function prepareOuterLink($link)
    {
        
        if (strpos($link, '//') === false) {
            $link = 'http://' . $link;
        }
        
        return $link;
    }
    
    public function prepareInstagramLink($id)
    {
        $id = trim(str_replace('@', '', $id));
        
        return 'https://www.instagram.com/' . trim($id, '/') . '/';
    }
    
    public function getTemplateFilePath($file = '')
    {
        if (!$file) {
            return '';
        }
        
        $file = str_replace($this->templatesDirectory, '', $file);
        $file = str_replace('.' . pathinfo($file, PATHINFO_EXTENSION), '', $file);
        
        return $file;
    }
    
    public function formatFileSize($bytes, $precision = 2, $units = array('B', 'KB', 'MB', 'GB', 'TB'))
    {
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);
        
        return round($bytes, $precision) . ' ' . $units[$pow];
    }
    
    public function prepareJsonParams($params)
    {
        return htmlspecialchars(json_encode($params, ENT_QUOTES));
    }
    
    public function getElementId($element = null)
    {
        return $element instanceof umiHierarchyElement ? $element->getId() : false;
    }
    
    public function sortOptionsByName($options)
    {
        $items = array();
        
        foreach($options as $option) {
            $items[$option['id']] = $option['name'];
        }
    
        asort($items);

        $return = array();
        foreach($items as $itemId => $name) {
            $return[] = array(
                'id' => $itemId,
                'name' => $name
            );
        }
        
        return $return;
    }
    
    private $requestUri;
    
    /**
     * @return mixed
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }
    
    /**
     * @param mixed $requestUri
     */
    public function setRequestUri($requestUri)
    {
        $this->requestUri = $requestUri;
    }
}