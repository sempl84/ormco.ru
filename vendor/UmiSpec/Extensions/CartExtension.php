<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class CartExtension implements IPhpExtension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __CLASS__;
    }
    
    private $cart;
    private $amount;
    private $price;
    private $itemsAmount = array();
    
    public function setCart($cart = null)
    {
        if (!is_array($cart)) {
            return false;
        }
        
        $this->cart = $cart;
        
        $amount = 0;
        $actualPrice = 0;
        
        $summary = getArrayKey($cart, 'summary');
        if (is_array($summary)) {
            $amount = getArrayKey($summary, 'amount');
            $price = getArrayKey($summary, 'price');
            
            if (is_array($price)) {
                $actualPrice = (float)getArrayKey($price, 'actual');
            }
        }
        
        $items = getArrayKey($cart, 'items');
        if ($items) {
            foreach ($items as $item) {
                $page = getArrayKey($item, 'page');
                
                if ($page instanceof umiHierarchyElement) {
                    if (!isset($this->itemsAmount[$page->getId()])) {
                        $this->itemsAmount[$page->getId()] = 0;
                    }
                    
                    $this->itemsAmount[$page->getId()] += $item['amount'];
                }
            }
            
        }
        
        $this->amount = $amount;
        $this->price = $actualPrice;
        
        return true;
    }
    
    public function getCart()
    {
        return $this->cart;
    }
    
    public function getCartAmount()
    {
        return $this->amount;
    }
    
    public function getCartPrice()
    {
        return $this->price;
    }
    
    public function getCartItems()
    {
        $items = getArrayKey($this->cart, 'items');
        
        return is_array($items) ? $items : false;
    }
    
    public function getCartItemsCount()
    {
        $count = 0;
        
        $items = getArrayKey($this->cart, 'items');
        if (is_array($items)) {
            $count = count($items);
        }
        
        return $count;
    }
    
    public function getCartItemAmount($pageId, $default = 0)
    {
        $amount = getArrayKey($this->itemsAmount, $pageId);
        if (is_null($amount)) {
            $amount = $default;
        }
        
        return $amount;
    }
    
    public function getIsInCart($pageId)
    {
        return isset($this->itemsAmount[$pageId]) ? true : false;
    }
    
    public function renderCartDataAmount()
    {
        return ' data-cart="amount"';
    }
}