<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class PageExtension implements IPhpExtension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __CLASS__;
    }

    private $umiHierarchy;

    /**
     * Получить адрес страницы по id
     *
     * @return string
     */
    public function getPageLink($pageId, $isAbsolute = false)
    {
        if (!$this->umiHierarchy) {
            $this->umiHierarchy = umiHierarchy::getInstance();
        }

        if ($isAbsolute) {
            $forceAbsolutePath = $this->umiHierarchy->forceAbsolutePath(true);
            $link = $this->umiHierarchy->getPathById($pageId);
            $this->umiHierarchy->forceAbsolutePath($forceAbsolutePath);
        } else {
            $link = $this->umiHierarchy->getPathById($pageId);
        }

        return $link;
    }

    /**
     * @var $parents int[]
     */
    private $arParentId = array();

    public function setParents($parents = null)
    {
        if (!is_array($parents)) {
            return false;
        }

        foreach ($parents as $parent) {
            if (!$parent instanceof umiHierarchyElement) {
                continue;
            }

            $this->arParentId[] = $parent->getId();
        }

        return true;
    }

    public function getParents()
    {
        return $this->arParentId;
    }

    /**
     * @param $index
     * @return false|int
     */
    public function getParentId($index)
    {
        return isset($this->arParentId[$index]) ? $this->arParentId[$index] : false;
    }

    public function getParentsCount()
    {
        return count($this->arParentId);
    }

    public function isParent($id)
    {
        return in_array($id, $this->arParentId);
    }

    public function redirectIfNotPage($link = '/')
    {
        if (!$this->pageId) {
            outputBuffer::current()->redirect($link);
        }
    }

    private $pageId;
    private $pageIsIndex;
    private $pageObjectId;
    private $pageObjectTypeId;
    private $pageObjectTypeGUID;
    private $pageObjectGUID;
    private $pageParentId;
    private $pageLink;
    private $page;
    private $pageAltName;

    public function parsePage(umiHierarchyElement $page = null)
    {
        if (!$page instanceof umiHierarchyElement) {
            return false;
        }

        $this->pageId = $page->getId();
        $this->pageIsIndex = $page->getIsDefault();
        $this->pageObjectId = $page->getObjectId();
        $this->pageObjectTypeId = $page->getObjectTypeId();
        $this->pageObjectTypeGUID = $page->getObject()->getTypeGUID();
        $this->pageObjectGUID = $page->getObject()->getGUID();
        $this->pageParentId = $page->getParentId();
        $this->pageLink = umiLinksHelper::getInstance()->getLink($page);
        $this->pageAltName = $page->getAltName();
        $this->page = $page;

        return true;
    }

    /**
     * @return mixed
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @return mixed
     */
    public function getPageIsIndex()
    {
        return $this->pageIsIndex;
    }

    /**
     * @return mixed
     */
    public function getPageObjectId()
    {
        return $this->pageObjectId;
    }

    /**
     * @return mixed
     */
    public function getPageObjectTypeId()
    {
        return $this->pageObjectTypeId;
    }

    /**
     * @return mixed
     */
    public function getPageParentId()
    {
        return $this->pageParentId;
    }

    /**
     * @return mixed
     */
    public function getPageObjectTypeGUID()
    {
        return $this->pageObjectTypeGUID;
    }

    /**
     * @return umiHierarchyElement|null
     */
    public function getPage()
    {
        return $this->page;
    }

    public function getPageValue($field)
    {
        return $this->page instanceof umiHierarchyElement ? $this->page->getValue($field) : null;
    }

    public function getPageName()
    {
        return $this->page instanceof umiHierarchyElement ? $this->page->getName() : null;
    }

    /**
     * @return mixed
     */
    public function getPageAltName()
    {
        return $this->pageAltName;
    }

    /**
     * @return mixed
     */
    public function getPageObjectGUID()
    {
        return $this->pageObjectGUID;
    }
}