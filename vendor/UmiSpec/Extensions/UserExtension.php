<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UserExtension implements IPhpExtension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __CLASS__;
    }
    
    private $userId;
    private $userLogin;
    private $userIsAuth;
    private $userType;
    
    public function parseUser($user)
    {
        if (!is_array($user)) {
            return false;
        }
        
        $this->userId = getArrayKey($user, 'id');
        $this->userLogin = getArrayKey($user, 'login');
        $this->userIsAuth = getArrayKey($user, 'status') == 'auth';
        $this->userType = getArrayKey($user, 'type');
        
        return true;
    }
    
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * @return mixed
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }
    
    /**
     * @return mixed
     */
    public function getUserIsAuth()
    {
        return $this->userIsAuth;
    }
    
    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }
    
    public function getUserIsSv()
    {
        return $this->userType == 'sv';
    }
    
    private $object;
    
    public function getUserValue($field)
    {
        $user = $this->getUser();
        
        return $user instanceof umiObject ? $user->getValue($field) : false;
    }
    
    /**
     * @return umiObject
     */
    public function getUser()
    {
        if (is_null($this->object)) {
            $object = false;
            
            if ($this->userId) {
                $object = umiObjectsCollection::getInstance()->getObject($this->userId);
            }
            
            $this->object = $object;
        }
        
        return $this->object;
    }
    
    public function redirectIfNotAuth($link)
    {
        if(!$this->userIsAuth) {
            $buffer = outputBuffer::current('HTTPOutputBuffer');
            /* @var $buffer HTTPOutputBuffer */
            $buffer->redirect($link);
        }
    }
    
    public function redirectIfAuth($link)
    {
        if($this->userIsAuth) {
            $buffer = outputBuffer::current('HTTPOutputBuffer');
            /* @var $buffer HTTPOutputBuffer */
            $buffer->redirect($link);
        }
    }
}