<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */
class UmiSpecInstallerHierarchyType
{
    /**
     * @var string
     */
    private $module = '';
    /**
     * @var string
     */
    private $method = '';
    /**
     * @var string
     */
    private $title = '';

    /**
     * @param $module
     * @param string $method
     * @param string $title
     */
    function __construct($module, $method = '', $title = '')
    {
        $this->module = $module;
        $this->method = $method;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}