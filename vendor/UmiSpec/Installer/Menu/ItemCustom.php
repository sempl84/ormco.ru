<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2019, Evgenii Ioffe
 */
class UmiSpecInstallerMenuItemCustom extends UmiSpecInstallerMenuItem
{
    public function __construct($name, $link)
    {
        $this->isActive = 'true';
        $this->name = $name;
        $this->link = $link;
        $this->rel = 'custom';
    }
}