<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2019, Evgenii Ioffe
 */
class UmiSpecInstallerMenuItemPage extends UmiSpecInstallerMenuItem
{
    public function __construct($pageId, $name = null)
    {
        $page = umiHierarchy::getInstance()->getElement($pageId);
        if (!$page instanceof umiHierarchyElement) {
            throw new publicException('Не найдена страница ' . $pageId);
        }

        if (!$name) {
            $name = $page->getName();
        }

        $this->isActive = $page->getIsActive();
        $this->name = $name;
        $this->link = umiHierarchy::getInstance()->getPathById($page->getId());
        $this->rel = $page->getId();
        $this->additionalData['isdeleted'] = $page->getIsDeleted() ? 1 : 0;
    }

    public static function create($pageId)
    {
        return new self($pageId);
    }

    public static function getPageIdByObjectGUID($objectGUID)
    {
        $objectId = umiObjectsCollection::getInstance()->getObjectIdByGUID($objectGUID);
        if (!$objectId) {
            return false;
        }

        $arPageId = umiHierarchy::getInstance()->getObjectInstances($objectId);
        if (!$arPageId) {
            return false;
        }

        $page = umiHierarchy::getInstance()->getElement($arPageId[0]);
        if (!$page instanceof umiHierarchyElement) {
            return false;
        }

        return $page->getId();
    }
}