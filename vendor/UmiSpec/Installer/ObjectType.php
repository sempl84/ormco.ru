<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */
class UmiSpecInstallerObjectType
{
    /**
     * @var string
     */
    private $name = '';
    /**
     * @var string
     */
    private $guid = '';
    /**
     * @var int
     */
    private $parentId = 0;
    /**
     * @var bool
     */
    private $isGuidable = false;
    /**
     * @var int
     */
    private $hierarchyTypeId = 0;
    /**
     * @var bool
     */
    private $isPublic = false;

    /**
     * @param $name
     * @param $guid
     * @param $parentId
     * @param $isGuidable
     */
    function __construct($name, $guid, $parentId, $isGuidable = false)
    {
        $this->name = $name;
        $this->guid = $guid;
        $this->parentId = $parentId;
        $this->isGuidable = $isGuidable;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return boolean
     */
    public function isIsGuidable()
    {
        return $this->isGuidable;
    }

    /**
     * @param boolean $isGuidable
     */
    public function setIsGuidable($isGuidable)
    {
        $this->isGuidable = $isGuidable;
    }

    /**
     * @return int
     */
    public function getHierarchyTypeId()
    {
        return $this->hierarchyTypeId;
    }

    /**
     * @param int $hierarchyTypeId
     */
    public function setHierarchyTypeId($hierarchyTypeId)
    {
        $this->hierarchyTypeId = $hierarchyTypeId;
    }

    /**
     * @return boolean
     */
    public function isIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param boolean $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }
}