<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */
class UmiSpecInstallerField
{
    /**
     * @var string
     */
    private $name = '';
    /**
     * @var string
     */
    private $title = '';
    /**
     * @var int
     */
    private $typeId = 0;
    /**
     * @var bool
     */
    private $visible = true;
    /**
     * @var bool
     */
    private $locked = false;
    /**
     * @var bool
     */
    private $required = false;
    /**
     * @var string
     */
    private $tip = '';
    /**
     * @var bool
     */
    private $inFilter = null;
    /**
     * @var bool
     */
    private $inSearch = null;
    /**
     * @var int
     */
    private $guideId = 0;

    /**
     * @param $name
     * @param $title
     * @param $typeId
     * @param bool $required
     */
    function __construct($name, $title = null, $typeId, $required = false)
    {
        $this->name = $name;
        $this->title = $title;
        $this->typeId = $typeId;
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param boolean $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * @param string $tip
     */
    public function setTip($tip)
    {
        $this->tip = $tip;
    }

    /**
     * @return boolean
     */
    public function isInFilter()
    {
        return $this->inFilter;
    }

    /**
     * @param boolean $inFilter
     */
    public function setInFilter($inFilter)
    {
        $this->inFilter = $inFilter ? true : false;
    }

    /**
     * @return boolean
     */
    public function isInSearch()
    {
        return $this->inSearch;
    }

    /**
     * @param boolean $inSearch
     */
    public function setInSearch($inSearch)
    {
        $this->inSearch = $inSearch;
    }

    /**
     * @return int
     */
    public function getGuideId()
    {
        return $this->guideId;
    }

    /**
     * @param int $guideId
     */
    public function setGuideId($guideId)
    {
        $this->guideId = $guideId;
    }
}