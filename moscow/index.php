<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ORMCO в Москве и московской области</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<script src="js/jquery-last.min.js" type="text/javascript" charset="utf-8"></script>
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" type="text/css">
		<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
		<script>
			lnks = new Array("form", "products", "ormco");
			function chkform() {
				var foo = 0;
				$('.ness').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				}); 
				if (foo == 0) {
					if ($('#cap').val() != '') {
						return true;
					}
					else {
						$('#cap').addClass("redfld");
						return false;
					}
				}
				else
					return false;
			}
			var x1 = 55.7451544;
			var y1 = 37.6619781;
			var myLatlng1;
			var map;
			var marker1;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map_canvas"),
					mapOptions);
				myLatlng1 = new google.maps.LatLng(x1, y1);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "ORMCO Москва"
				});
			}
			$(document).ready(function() {
				initialize_map();
				var wdth = $(window).width();
				hghts = new Array();
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$('a[href*=#]:not([href=#])').click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
			});
			var scrolling = 0;
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
			}
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
		</script>
	</head>
	<body>
		<div class="container">
			<a name="about"></a>
			<div class="header">
				<img src="images/logo.png" />
				<div class="menu">
					<ul>
						<li id="miormco"><a href="#ormco">Ormco в Москве</a></li>
						<li id="miproducts"><a href="#products">Продукция</a></li>
						<li id="miform"><a href="#form">Связаться</a></li>
					</ul>
				</div>
				<div class="phone">8 (495) 664-75-55</div>
			</div>
			<div id="ormco" class="main group">
				<div class="action_left">
					<h1>ORMCO В МОСКВЕ</h1>
					<h2>И МОСКОВСКОЙ ОБЛАСТИ</h2>
					<h4>Обращайтесь напрямую в офис Ormco в Москве!</h4>
					<div class="txt">
						Хотите купить продукцию Ormco в Москве и Московской области? Работа с производителем позволит вам гарантированно получить оригинальную качественную продукцию по низким ценам! Свяжитесь с менеджерами Ormco, позвонив или написав в офис прямых продаж в Москве
					</div>
					<div class="att">
						<h4>Обратите внимание!</h4>
						Компания Дентал Комплекс в настоящее время входит в состав Ormco. Уточняйте всю информацию по телефону: 8 (495) 664-75-55
					</div>
				</div>
				<div class="action_right">
					<div class="action_right_">
						<h2>Преимущества сотрудничества напрямую с Ormco</h2>
						<ul>
							<li>Гарантия качества</li>
							<li>Отличные цены</li>
							<li>100% оригинальная продукция</li>
							<li>Прекрасный сервис</li>
							<li>Маркетинговая поддержка</li>
						</ul>
					</div>
					<div class="to_order">
						<div class="to_order_left">Для заказа<br />звоните</div>
						<div class="to_order_right">8 (495) 664-75-55</div>
					</div>
					<div class="to_order2" onClick="$('html,body').animate({scrollTop: $('#form').offset().top - 100}, 500);">Написать в Ormco в Москве</div>
				</div>
			</div>
			<div id="products">
				<h2>ПРОДУКЦИЯ ORMCO</h2>
				<div class="products group">
					<div class="product" onClick="document.location='http://ormco.ru/market/damon-system/brekety/kit/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i1.png" /></div></div>
						<div class="prodtxt">Самолигирующие брекеты<br />
						Damon System</div>
						<div class="price">от 7930 р.</div>
						<a href="http://ormco.ru/market/damon-system/brekety/kit/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/damon-system/brekety/kit/damon-clear/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i2.png" /></div></div>
						<div class="prodtxt">Керамические брекеты<br />
						Damon Clear</div>
						<div class="price">от 11970 р.</div>
						<a href="http://ormco.ru/market/damon-system/brekety/kit/damon-clear/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/vestibular/kit/aesthetic/inspire-ice/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i3.png" /></div></div>
						<div class="prodtxt">Сапфировые брекеты<br />
						Ormco</div>
						<div class="price">от 7750 р.</div>
						<a href="http://ormco.ru/market/vestibular/kit/aesthetic/inspire-ice/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/damon-system/brekety/kit/damon-3/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i4.png" /></div></div>
						<div class="prodtxt">Металлические брекеты<br />
						Damon 3</div>
						<div class="price">от 8480 р.</div>
						<a href="http://ormco.ru/market/damon-system/brekety/kit/damon-3/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/damon-system/brekety/kit/damon-q/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i5.png" /></div></div>
						<div class="prodtxt">Металлические брекеты<br />
						Damon Q</div>
						<div class="price">от 10360 р.</div>
						<a href="http://ormco.ru/market/damon-system/brekety/kit/damon-q/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/vestibular/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i6.png" /></div></div>
						<div class="prodtxt">Классические вестибулярные<br />
						брекеты Ormco</div>
						<div class="price">от 1200 р.</div>
						<a href="http://ormco.ru/market/vestibular/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/arc/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i7.png" /></div></div>
						<div class="prodtxt">Дуги<br />
						Ormco</div>
						<div class="price">от 130 р.</div>
						<a href="http://ormco.ru/market/arc/"><button>Купить</button></a>
					</div>
					<div class="product" onClick="document.location='http://ormco.ru/market/elastic/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i8.png" /></div></div>
						<div class="prodtxt">Эластики и лигатуры<br />
						Ormco</div>
						<div class="price">от 37 р.</div>
						<a href="http://ormco.ru/market/elastic/"><button>Купить</button></a>
					</div>
					<div class="product active" onClick="document.location='http://ormco.ru/market/'">
						<div class="prodimg"><div class="prodimg_"><img src="images/i9.png" /></div></div>
						<div class="prodtxt">Все брекеты и<br />
						инструменты Ormco</div>
						<a href="http://ormco.ru/market/"><button>Купить</button></a>
					</div>
				</div>
			</div>
			<div id="form">
				<div class="contacts group">
					<div class="contacts_left">
						<h2>КОНТАКТЫ:</h2>
						<h3>Московский офис ORMCO:</h3>
						109004, Москва, ул. Станиславского, д. 21, стр. 3, этаж 1, БЦ "Фабрика Станиславского"<br />
						Тел.: (495) 664-75-55; факс: (495) 664-75-56<br />
						E-mail: <a href="mailto:sales.msk@ormco.com">sales.msk@ormco.com</a>
						<h4>Режим работы</h4>
						Понедельник - Пятница: 9:00 - 17:30<br />
						Суббота - Воскресенье: выходные дни
						<div class="map_canvas" id="map_canvas"></div>
					</div>
					<div class="contacts_right">
						<form action="#form" method="post" onSubmit="return chkform();">
							<div class="formbox">
								<h2>НАПИШИТЕ НАМ:</h2>
								<div class="err" id="err">
									<?
									if (md5($_POST["cap"]) != $_SESSION["captcha"] && $_POST["cap"] != "")
										{
										?>
										<br />
										<span class="red">Код введен неверно или устарел.</span>
										<?
									}
									elseif ($_POST["name"] != "" && ($_POST["phone"] != "" || $_POST["email"] != ""))
										{
										$emailto = "Natalia.Kataeva@ormco.com, Roman.Fomichev@ormco.com";
										// $emailto = "taran1972@gmail.com";
										$email = "info@ormco.com";
										$message = "Здравствуйте!

С сайта ".$_SERVER["HTTP_HOST"]." поступила заявка по акции Москва:
----------------------------------------
Указанный EMail: ".$_POST["email"]."
Указанный телефон: ".$_POST["phone"]."
ИМЯ: ".$_POST["name"]."
Посетитель - ".$_POST["who"]."
Вопрос: ".$_POST["question"]."
Время: ".date("d.m.Y")."
";
									$header = "From:".$email."\nX-Mailer: PHP Auto-Mailer\nContent-Type: text/plain; charset=utf-8";
									$subj = "Message from dentalcomplex action ORMCO MOSCOW";
$mkfile = "records.txt";
$mk_bd=fopen($mkfile,"w") or exit("Невозможно открыть файл!");
flock($mk_bd,LOCK_EX);
fwrite($mk_bd,$message);
flock($mk_bd,LOCK_UN);
fclose($mk_bd);
										mail($emailto, $subj, $message, $header, "-f".$email) or die("Mailing failed 1");
										?>
										Спасибо! Ваше сообщение отправлено!
										<?
									}
									else
										{
										if ($_POST["cap"] != "")
											{
											?>
											<br />
											<span class="red">Вы не ввели имя или контактные данные!</span>
											<?
										}
									}
									?>
								</div>
								<div class="group">
									<div class="ttl">ФИО <sup>*</sup></div>
									<div class="fld"><input class="ness" type="text" size="30" name="name" id="name" /></div>
								</div>
								<div class="group">
									<div class="ttl">Email <sup>*</sup></div>
									<div class="fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
								</div>
								<div class="group">
									<div class="ttl">Телефон <sup>*</sup></div>
									<div class="fld"><input class="short ness" type="text" size="25" name="phone" id="phone" /></div>
								</div>
								<div class="group">
									<div class="ttl">Кто Вы <sup>*</sup></div>
									<div class="fld"><select class="short ness" name="who">
											<option value="Пациент">Пациент</option>
											<option value="Врач">Врач</option>
											<option value="Менеджер">Менеджер</option>
										</select>
									</div>
								</div>
								<div class="group">
									<div class="ttl high">Ваш вопрос <sup>*</sup></div>
									<div class="fld high"><textarea class="ness" name="question"></textarea></div>
								</div>
								<div class="group">
									<div class="ttlc">Введите код, указанный на картинке</div>
									<div class="fldc">
										<div class="group">
											<div class="captcha"><img id="captcha" name="captcha" src="captcha.php" />
												<br />&nbsp;<span class="smll" onClick="reload();">другие цифры</span>
											</div>
											<div class="code"><input placeholder="Код с картинки" type="text" size="10" name="cap" id="cap" /></div>
										</div>
									</div>
								</div>
								<div class="group">
									<div class="formagree">Нажимая «Отправить», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span></div>
									<input type="submit" value="Отправить" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="footer" id="footer">
				<div class="footer_left w1">
					<img src="images/logo_footer.png" />
				</div>
				<div class="footer_left w1">
					<a target="_blank" href="/company/">О компании</a><br />
					<a target="_blank" href="/novelties/">Новости</a><br />
					<a target="_blank" href="/contact/">Связаться</a>
				</div>
				<div class="footer_left w2">
					<a target="_blank" href="/market/">Интернет-магазин для ортодонтов</a><br />
					<a target="_blank" href="/seminary/">Образовательные мероприятия Ormco</a><br />
					<a href="/filemanager/download/2969">Скачать каталог Ormco</a>
				</div>
				<div class="footer_left w3">
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=10184890&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/10184890/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:10184890,lang:'ru'});return false}catch(e){}" /></a>
<!-- /Yandex.Metrika informer -->

				</div>
			</div>
		</div>
<div id="pol">
	<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
	<div id="inpol">
		<h2>Политика конфиденциальности</h2>
		<h4>1. Общие положения</h4>
		Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
		Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
		<h4>2. Сбор, использование и переработка персональных данных</h4>
		Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
		Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
		В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
		Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
		Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
		<h4>3. Безопасность данных</h4>
		Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
		<h4>4. Персональные данные несовершеннолетних потребителей</h4>
		Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
		<h4>5. Файлы Cookie</h4>
		Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
		Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
		Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
		<h4>6. Отслеживание через интернет</h4>
		На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
		Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
		Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
		<h4>7. Ваши пожелания и вопросы</h4>
		Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте sales.msk@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
		<h4>8. Законодательство по обработке персональных данных</h4>
		Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
		<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
		•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
		<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
		Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
		<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
		Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
		<h4>(4) Периодичность регулярного удаления данных:</h4>
		Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
		<h4>(5) Запланированная передача данных другим странам:</h4>
		В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
		<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
		На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»). 
		Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
		Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
		Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
		Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
	</div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter10184890 = new Ya.Metrika({
                    id:10184890,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/10184890" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67881160-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>
