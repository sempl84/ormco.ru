<?php
$permissions = array(
    'purchasing' => array(
        'registerEventOneClick',
        'eventInfo', 'eventOrderNum', 'buhgalt_order_number',
        'saveInfoCust',
        'receiptt', 'invoicee',
        'simplePaymentsList',
        'legalList', 'legalAdd',
        'premoderate',
        'addSeveralBrackets',
        'receiptPrint', 'invoiceePrint',
        'addCheckoutStep1', 'addCheckoutStep2',
        'order1csyn', 'showPaymentChoose', 'showDeliveryChoose',
        'test',
        'order1csynTest', 'order1csynSend',
        'ndsSize',
        'needUserInfo',
        'try_order_pay',
        'clear_empty_non_users',
        'clear_non_user_orders',
        'ga_send_ecommerce_purchase'
    ),
    'personal' => array(
        'legalList', 'legalAdd', 'legal_delete',
        'addressList', 'adressAdd', 'address_delete'
    )
);