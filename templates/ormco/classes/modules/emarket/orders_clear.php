<?php
class emarket_orders_clear extends def_module {

	/**
	 * Очистка устаревших незарегистрированных пользователей - аналог того, что по крону, только быстрее и без заморозки. По крону видимо не успевает
	 * Запускается через http://ormco-test1.dpromo.su/udata/emarket/clear_empty_non_users
	 */
	public function clear_empty_non_users() {
		$start = 0;
		$limit = 500;

		$customerTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByGUID('emarket-customer');
        $expiration = umiObjectsExpiration::getInstance();
		$customers = $expiration->getExpiredObjectsByTypeId($customerTypeId, 1000000);

		sort($customers);
		echo "Total customers = " . sizeof($customers);
		echo "\nStart = " . $start;
		echo "\nLimit = " . $limit;

		$customers = array_slice($customers, $start, $limit);

		$orders = array();
		foreach($customers as $customer_id){
			$orders[$customer_id]['customer_id'] = $customer_id;
		}

		echo "\n\nCustomers = " . sizeof($customers);
//		print_r($customers);

		$sql = "SELECT o.id as id, oc_126_lj.rel_val as user_id FROM cms3_object_types t, cms3_objects o LEFT JOIN cms3_object_content oc_126_lj ON oc_126_lj.obj_id=o.id AND oc_126_lj.field_id = '126' WHERE o.type_id IN (51) AND t.id = o.type_id AND oc_126_lj.rel_val in (" . implode(',', $customers) . ") group by o.id order by o.id ";

		$connection = ConnectionPool::getInstance()->getConnection();
		$result = $connection->queryResult($sql);
		$result->setFetchType(IQueryResult::FETCH_ASSOC);

		$count = 0;
		foreach($result as $row){
			$orders[$row['user_id']]['orders'][] = $row['id'];
			$count++;
//			echo $row['id'] . "=" . $row['user_id'] . "\n";
		}

		echo "\nOrders = " . $count . "\n";

		$count = 0;

		$collection = umiObjectsCollection::getInstance();
		foreach($orders as $id_user => $user_details){
            echo $id_user;
			if(!isset($user_details['orders']) || sizeof($user_details['orders']) == 0){
                echo " - DIE\n";
				$customer = $collection->getObject($id_user);
				if($customer instanceof umiObject){
					$orders[$id_user]['name'] = $customer->getName();
					$deliveryAddresses = $customer->delivery_addresses;
					if (!is_null($deliveryAddresses) && is_array($deliveryAddresses) && sizeof($deliveryAddresses) > 0) {
						foreach($deliveryAddresses as $addressId) {
							$objects->delObject($addressId);
						}
					}
					$customer->delete();

					$count++;
				}
            }else{
                echo " - FREEZE\n";
                $customer_customer = new customer($collection->getObject($id_user));
                $customer_customer->freeze();
                foreach($user_details['orders'] as $order_id) {
                    $order = $collection->getObject($order_id);
                    if($order instanceof umiObject){
                        if (is_null($order->status_id)) {
                            if (!$expiration->isExpirationExists($order->getId())) {
                                $expiration->add($order->getId());
                            }
                        }
                    }
                }
            }
		}
		echo "\nDeleted users = " . $count . "\n";

		die();
	}

	/**
	 * Очистка устаревших неоформленных заказов в корзине от незарегистрированных пользователей
	 */
	public function clear_non_user_orders() {
		$start = 0;
		$limit = 100;

		$orderTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByGUID('emarket-order');
		$expiration = umiObjectsExpiration::getInstance();
		$orders = $expiration->getExpiredObjectsByTypeId($orderTypeId, 1000000);

		sort($orders);
		echo "Total orders = " . sizeof($orders);
		echo "\nStart = " . $start;
		echo "\nLimit = " . $limit;

		$orders = array_slice($orders, $start, $limit);

		$count = 0;
		if (sizeof($orders) > 0) {
			$objects = umiObjectsCollection::getInstance();
			foreach($orders as $orderId) {
				$order = $objects->getObject($orderId);
				if (is_null($order->status_id)) {
					$order = order::get($orderId);
					$items = $order->getItems();
					foreach($items as $item) {
						$orderItem = orderItem::get($item->getId());
						$orderItem->remove();
					}
					$customerId = $order->customer_id;
                    if(!empty($customerId)){
                        if (!$expiration->isExpirationExists($customerId)) {
                            $expiration->add($customerId);
                        }
                    }
					$order->delete();
					$count++;
                }else{
                    $expiration->clear($orderId);
				}
			}
		}
		echo "\nDeleted orders = " . $count . "\n";
		die();
	}

	/**
	 * Очистка устаревших неоформленных заказов в корзине
	 */
	public function clear_user_orders() {

	}

	/**
	 * Перевод оформленных заказов в корзине обратно в статус "не в корзине"
	 */
	public function clear_user_ready_orders() {
	}

//	/**
//	 * ИСПОЛЬЗОВАЛАСЬ 1 раз и больше ни ни - УДАЛИТЬ ПОСЛЕ ВСЕГО
//	 *
//	 * Обновляем статусы заказов на те, что соответствуют статусам из 1С
//	 */
//	public function update_system_order_status() {
//		die();
////		$statuses = array(
////			"1595826" => array(id_1c => "1595826", name => "Отгружен", status_id => "117", status_name => "Готов", status_alt => "ready"),
////			"1606394" => array(id_1c => "1606394", name => "Принят", status_id => "242", status_name => "Принят", status_alt => "accepted"),
////			"1606396" => array(id_1c => "1606396", name => "Оплачен", status_id => "242", status_name => "Принят", status_alt => "accepted"),
////			"1606397" => array(id_1c => "1606397", name => "Готов", status_id => "117", status_name => "Готов", status_alt => "ready"),
////			"1606398" => array(id_1c => "1606398", name => "Резерв частичный", status_id => "184", status_name => "Доставляется", status_alt => "delivery"),
////			"1611892" => array(id_1c => "1611892", name => "Сборка", status_id => "184", status_name => "Доставляется", status_alt => "delivery"),
////			"1611893" => array(id_1c => "1611893", name => "Собран частично", status_id => "184", status_name => "Доставляется", status_alt => "delivery"),
////		);
////
//		$orders = new selector('objects');
//		$orders->types('object-type')->name('emarket', 'order');
//		$orders->where('number')->isnotnull(true);
////		$orders->where('status_1c')->isnotnull(true);
//		$orders->where('status_id')->equals(58);
//		$orders->order('id')->asc();
//		$orders->limit(0, 1);
//
//		echo $orders->length();
////		foreach ($orders as $order){
////			echo "\n" . $order->getId() . " = ";
////			echo $order->getValue('status_1c') . " = ";
////			echo $statuses[$order->getValue('status_1c')]['name'] . " = ";
////			echo $statuses[$order->getValue('status_1c')]['status_id'] . " = ";
////			echo $statuses[$order->getValue('status_1c')]['status_name'] . " = ";
////			if(isset($statuses[$order->getValue('status_1c')]['status_id'])){
////				$order->setValue('status_id', $statuses[$order->getValue('status_1c')]['status_id']);
////				$order->commit();
////			}
////		}
//		die();
//	}

	/**
	 * Изменение стандартного статуса заказа при изменении статуса заказа в 1С
	 * @staticvar type $old_serialize_object
	 * @param iUmiEventPoint $event
	 * @return boolean
	 */
	public function update_order_status_from_order_1c_status(iUmiEventPoint $event) {
        static $old_serialize_object;

        $object = $event->getRef('object');
        $objectTypeId = $object->getTypeId();
        $order_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('emarket', 'order');
        if ($objectTypeId !== $order_type_id) { // не заказ
            return true;
        }

        if ($event->getMode() == 'after') {
			$status_id = self::get_status_id_by_status_1c_id($object->getValue('status_1c'));
			if($status_id != null && $status_id != $object->getValue('status_id')){
				$object->setValue('status_id', $status_id);
				$object->commit();
			}
        }
        return true;
    }

	/**
	 * Приватная функция соответствия статусв в 1С и статусов в заказе
	 * @param type $status_1c_id
	 * @param type $field
	 * @return array
	 */
	private function get_status_id_by_status_1c_id($status_1c_id, $field = 'status_id'){
		$statuses = array(
			'1595826' => array('id_1c' => '1595826', 'name_1c' => 'Отгружен',			'status_id' => '117', 'status_name' => 'Готов',			'status_alt' => 'ready'),
			'1606394' => array('id_1c' => '1606394', 'name_1c' => 'Принят',				'status_id' => '242', 'status_name' => 'Принят',		'status_alt' => 'accepted'),
			'1606396' => array('id_1c' => '1606396', 'name_1c' => 'Оплачен',			'status_id' => '242', 'status_name' => 'Принят',		'status_alt' => 'accepted'),
			'1606397' => array('id_1c' => '1606397', 'name_1c' => 'Готов',				'status_id' => '117', 'status_name' => 'Готов',			'status_alt' => 'ready'),
			'1606398' => array('id_1c' => '1606398', 'name_1c' => 'Резерв частичный',	'status_id' => '184', 'status_name' => 'Доставляется',	'status_alt' => 'delivery'),
			'1611892' => array('id_1c' => '1611892', 'name_1c' => 'Сборка',				'status_id' => '184', 'status_name' => 'Доставляется',	'status_alt' => 'delivery'),
			'1611893' => array('id_1c' => '1611893', 'name_1c' => 'Собран частично',	'status_id' => '184', 'status_name' => 'Доставляется',	'status_alt' => 'delivery'),
			'3199584' => array('id_1c' => '3199584', 'name_1c' => 'Отменен',            'status_id' => '250', 'status_name' => 'Отменен',	'status_alt' => 'canceled'),
		);

		if(isset($statuses[$status_1c_id])){
			return $statuses[$status_1c_id][$field];
		}
		return null;
	}
}