<?php
class emarket_user_delivery_adresses extends def_module {
    /* add adress, or save it
     * * adress id take from request info
     * * for example new adress : <input name="adress-person" value="new" />
     * * or adress with id = 1112 <input name="adress-person" value="1112" />
     * * may add ref_onsuccess url (page id or page url) by this <input type="hidden" name="ref_onsuccess" value="" />
     */
    public function adressAdd($template = 'default') {
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types->getBaseType("emarket", "delivery_address");

        //$delivery_addressesId = getRequest('address-person');
        $delivery_addressesId = getRequest('param0');
        $isNew = ($delivery_addressesId == null || $delivery_addressesId == 'new');
        if ($isNew) {
            $typeId = $types->getBaseType("emarket", "delivery_address");
            $customer = customer::get();
            $delivery_addressesId = $collection->addObject("", $typeId);
            $customer->delivery_addresses = array_merge($customer->delivery_addresses, array($delivery_addressesId));
        }
        $controller = cmsController::getInstance();
        $data = getRequest('data');
        if ($data && $dataModule = $controller->getModule("data")) {
            $key = $isNew ? 'new' : $delivery_addressesId;
            $delivery_address = $collection->getObject($delivery_addressesId);

            $delivery_address->setName($data[$key]['name']);

            $dataModule->saveEditedObject($delivery_addressesId, $isNew, true);
        }

        $url = getRequest('ref_onsuccess');
        if ($url){
            $this->redirect($url);
        }

        $referer_url = $_SERVER['HTTP_REFERER'];
        $this->redirect($referer_url);
    }

    /**
     * Удаление адреса пользователя из личного кабинета
     */
    public function address_delete() {
        $collection = umiObjectsCollection::getInstance();

        $delivery_addressesId = getRequest('address_id');
        if ($collection->isExists($delivery_addressesId)) {
            $customer = customer::get();
            $customer->setValue('delivery_addresses', array_diff($customer->getValue('delivery_addresses'), array($delivery_addressesId)));
            $customer->commit();

            $collection->delObject($delivery_addressesId);
        }

        $url = getRequest('ref_onsuccess');
        if ($url){
            $this->redirect($url);
        }

        $referer_url = $_SERVER['HTTP_REFERER'];
        $this->redirect($referer_url);
    }

    /**
     * Получение списка адресов
     * @param type $template
     * @return type
     */
    public function addressList($template = 'default') {
        $tpl_block = $tpl_item = '';
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types->getBaseType("emarket", "delivery_address");
        $customer = customer::get();

        $items = array();
        $delivery_addresses = $customer->delivery_addresses;
        if (is_array($delivery_addresses))
            foreach ($delivery_addresses as $delivery_addressesId) {
                $delivery_address = $collection->getObject($delivery_addressesId);
                $item_arr = array('attribute:id' => $delivery_addressesId, 'attribute:name' => $delivery_address->name);
                $items[] = def_module::parseTemplate($tpl_item, $item_arr, false, $delivery_addressesId);
            }
        if ($tpl_block) {
            return def_module::parseTemplate($tpl_block, array('items' => $items, 'type_id' => $typeId));
        } else {
            return array('attribute:type-id' => $typeId, 'xlink:href' => 'udata://data/getCreateForm/' . $typeId, 'items' => array('nodes:item' => $items));
        }
    }
}
