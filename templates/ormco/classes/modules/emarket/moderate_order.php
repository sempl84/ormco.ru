<?php
class emarket_custom_moderate_order extends def_module {
    // apply order to choose payment
    public function applyOrder($orderId) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }

        $order = order::get($orderId);
        $orderObj = umiObjectsCollection::getInstance()->getObject($orderId);

        // оплачивается
        $orderObj->setValue('moderate', 0);
        $orderObj->setValue('choose_payment', 0);

        //Инициализирована
        if($orderObj->getValue('need_export_new') != 1){
            $orderObj->setValue("need_export", 1);
        }
        $orderObj->commit();

        // отправляем письмо клиенту для выбора способа оплаты
        $customer = umiObjectsCollection::getInstance()->getObject($order->getCustomerId());

        $customer->last_order_sys = $orderId;

        $customer->commit();
        $email = $customer->email ? $customer->email : $customer->getValue("e-mail");

        if ($email) {
            $name = $customer->lname . " " . $customer->fname . " " . $customer->father_name;

            //mail_user_choose_payment
            $subject = "Заказа №{$order->number} одобрен";
            list($template) = def_module::loadTemplatesForMail("emarket/default", 'order_approved');

            $regedit = regedit::getInstance();
            $cmsController = cmsController::getInstance();
            $domains = domainsCollection::getInstance();
            $domainId = $cmsController->getCurrentDomain()->getId();
            $defaultDomainId = $domains->getDefaultDomain()->getId();

            if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
                $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
                $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
            } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
                $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
                $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
            } else {
                $fromMail = $regedit->getVal("//modules/emarket/from-email");
                $fromName = $regedit->getVal("//modules/emarket/from-name");
            }

            $param = array();
            $param["order_id"] = $orderId;
            $param["order_number"] = $orderObj->number;
            $param["domain"] = cmsController::getInstance()->getCurrentDomain()->getHost();

            $extParams = array('order_lname', 'order_fname', 'order_father_name', 'order_phone', 'order_e-mail', 'order_city', 'event_name', 'event_gorod_in', 'event_data');

            foreach ($extParams as $extParam) {
                $param[$extParam] = $orderObj->getValue($extParam);
            }

            $content = def_module::parseTemplateForMail($template, $param);

            $letter = new umiMail();
            $letter->addRecipient($email, $name);
            $letter->setFrom($fromMail, $fromName);
            $letter->setSubject($subject);
            $letter->setContent($content);
            $letter->commit();
            $letter->send();
        }
        return '<h1>Заказ одобрен.</h1>';
    }

    // apply event discount
    public function applyEventDiscount($orderId, $discount) {
        if (!$orderId){
            $orderId = getRequest('orderId');
        }

        $order = order::get($orderId);
        $originPrice = $order->getOriginalPrice();

        $orderObj = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$discount){
            $discount = (float) $orderObj->event_discount_value;
        }

        $bonus = ($discount > 0) ? $originPrice - $discount : 0;

        if (!$orderObj){
            return 'no';
        }
        $orderObj->setValue('bonus', $bonus);
        $orderObj->commit();
        $order->refresh();
        return 'ok';
    }

    // apply event discount
    public function simplePaymentsList($orderId) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }

        $objects = umiObjectsCollection::getInstance();
        $order = $objects->getObject($orderId);

        if (!$order instanceof umiObject){
            return false;
        }
        if ($order->getTypeGUID() !== 'emarket-order'){
            return false;
        }

        $orderSys = order::get($orderId);
        // оплачивается
        $orderSys->setOrderStatus(308);
        // инициализированно
        $orderSys->setPaymentStatus(259);

        if($order->getValue('need_export_new') != 1){
            $order->setValue("need_export", 1);
        }
        $order->setValue('choose_payment', 1);
        $order->commit();
        $orderSys->refresh();

        $userId = $order->getValue('customer_id');
        $customer = customer::get($userId);
        $cmsController = cmsController::getInstance();
        $domain = $cmsController->getCurrentDomain();
        $domainId = $domain->getId();

        $customer->setLastOrder($orderId, $domainId);
        permissionsCollection::getInstance()->loginAsUser((int) $userId);
        //$test = UmiCms\Service::Auth()->loginUsingId((int)$userId);
        //$this -> actAsUserNew($order -> getValue('customer_id'), $orderId);

        $paymentId = $order->payment_id;

        /* if ($_SERVER['REMOTE_ADDR'] == '83.102.203.103') {
          var_dump($userId);
          var_dump($test);
          exit('ttt');
          } */

        $url = "/emarket/purchase/payment/choose/";
        if ($paymentId) {
            $payment = payment::get($paymentId, $orderSys);

            if ($payment instanceof payment) {
                $paymentName = $payment->getCodeName();
                if ($paymentName == 'invoice') {
                    $personId = $order->legal_person;
                    $url = "/emarket/purchase/payment/{$paymentName}/do?legal-person=" . $personId;
                } else {
                    $url = "/emarket/purchase/payment/{$paymentName}/";
                }
            }
        }
        if ($_SERVER['REMOTE_ADDR'] == '92.61.67.194') {
            var_dump($userId);
            var_dump($url);
            exit('ttt');
        }
        $this->redirect($url);
    }

    public function change_order_status_from_1c_status($order_id, $status_1c, $status_1c_payment) {
        $status_1c_list = array(
            "Принят" => array(
                "status_name" => "Принят",
                "template_name" => "status_1c_adopted",
                "subject" => "Ваш заказ №%number% поступил в обработку"
            ),
            "К оплате" => array(
                "status_name" => "К оплате",
                "template_name" => "status_1c_to_pay",
                "subject" => "Ваш заказ №%number% одобрен и ожидает оплаты"
            ),
            "Готов" => array(
                "status_name" => "Сборка",
                "template_name" => "status_1c_ready",
                "subject" => "Прогресс по заказу №%number%"
            ),
            "Резерв частичный" => array(
                "status_name" => "Собран частично",
                "template_name" => "status_1c_partially_assembled",
                "subject" => "Прогресс по заказу №%number%"
            ),
            "Отгружен" => array(
                "status_name" => "Отгружен",
                "template_name" => "status_1c_shipped",
                "subject" => "Прогресс по заказу №%number%"
            ),
            "Отменен" => array(
                "status_name" => "Отменен",
                "template_name" => "status_1c_cancel",
                "subject" => "Отмена заказа №%number%"
            )
        );
        $status_1c_payment_list = array(
            "Оплачен" => array(
                "status_name" => "Оплачен",
                "template_name" => "status_1c_paid",
                "subject" => "Заказ №%number% оплачен"
            ),
        );

        $collection = umiObjectsCollection::getInstance();

        $is_need_to_send_letter = false;

        $order_obj = $collection->getObject($order_id);
        $order_payment_status_1c = $order_obj->getValue('status_payment_1c');
        $order_status_1c = $order_obj->getValue('status_1c');

        // проверка статуса оплаты
        if (empty($order_payment_status_1c) && isset($status_1c_payment_list[$status_1c_payment])) {
            // установить статус оплаты заказа
            $order_obj->setValue('status_payment_1c', $status_1c_payment_list[$status_1c_payment]['status_name']);
            $order_obj->commit();
            // отправить письмо по статусу
            $letter_template_name = $status_1c_payment_list[$status_1c_payment]['template_name'];
            $letter_subject = $status_1c_payment_list[$status_1c_payment]['subject'];
            $is_need_to_send_letter = true;
        }

        // проверка статуса заказа
        if (isset($status_1c_list[$status_1c])) {
            if (isset($status_1c_list[$status_1c]['status_name']) && $status_1c_list[$status_1c]['status_name'] != $order_status_1c) {
                // установить статус оплаты заказа
                $order_obj->setValue('status_1c', $status_1c_list[$status_1c]['status_name']);
                $order_obj->commit();
                // отправить письмо по статусу
                $letter_template_name = $status_1c_list[$status_1c]['template_name'];
                $letter_subject = $status_1c_list[$status_1c]['subject'];
                $is_need_to_send_letter = true;
            }
        }

        if ($is_need_to_send_letter) {
            $order = order::get($order_id);
            $customer = $collection->getObject($order->getCustomerId());
            $email = $customer->email ? $customer->email : $customer->getValue("e-mail");
            if ($email) {
                $name = $customer->lname . " " . $customer->fname . " " . $customer->father_name;

                //mail_user_choose_payment
                $settings_page_id = 236;
                $settings_page = umiHierarchy::getInstance()->getElement($settings_page_id);
                if ($settings_page instanceof umiHierarchyElement) {
                    $subject = str_replace('%number%', $order->number, $letter_subject);
                    list($templateLine) = def_module::loadTemplatesForMail("emarket/status1c", $letter_template_name);

                    $regedit = regedit::getInstance();
                    $cmsController = cmsController::getInstance();
                    $domains = domainsCollection::getInstance();
                    $domainId = $cmsController->getCurrentDomain()->getId();
                    $defaultDomainId = $domains->getDefaultDomain()->getId();

                    if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
                        $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
                        $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
                    } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
                        $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
                        $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
                    } else {
                        $fromMail = $regedit->getVal("//modules/emarket/from-email");
                        $fromName = $regedit->getVal("//modules/emarket/from-name");
                    }

                    $param = array();
                    $param["order_id"] = $order_id;
                    $param["order_number"] = $order_obj->number;
                    $content = def_module::parseTemplateForMail($templateLine, $param);

                    $letter = new umiMail();
                    $letter->addRecipient($email, $name);
                    $letter->setFrom($fromMail, $fromName);
                    $letter->setSubject($subject);
                    $letter->setContent($content);
                    $letter->commit();
                    $letter->send();
                }
            }
        }
    }
}