<?php

class emarket_import_discounts extends def_module
{
    const custom_log_file_input = 'input';
    const custom_log_file_response = 'response';
    
    /* импорт скидко пользователей по uid из 1С */
    public function import_discounts()
    {
        //$str = '{"item": [ {   "uid": "17023", "discount": "20.50" }, {   "uid": "1218307", "discount": "10.00" }, {   "uid": "190099", "discount": "10.00" }, {   "uid": "209950", "discount": "20.00" }, {   "uid": "5459", "discount": "0" }]}';
        //$input = (array)json_decode($str,true);
        $input = (array)json_decode(file_get_contents('php://input'), true);
        //file_put_contents(CURRENT_WORKING_DIR . '/traceajax.txt', "\n\n" . print_r(file_get_contents('php://input'), true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajax_discount.txt', "\n\n" . print_r($input, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        
        $result = $this->_doImportDiscounts($input);
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset('utf-8');
        $buffer->contentType('application/json');
        $buffer->push(json_encode($this->_generateImportDiscountsResponse($result)));
        $buffer->end();
    }
    
    public function testImportDiscounts($file = '')
    {
        $file = trim($file);
        if (!$file) {
            throw new publicException('Не передан путь к файлу');
        }
        
        $file = CURRENT_WORKING_DIR . '/sys-temp/' . $file . '.json';
        if (!file_exists($file)) {
            throw new publicException('Не найден файл ' . $file);
        }
        
        $string = file_get_contents($file);
        $string = str_replace(' ', '', $string);
        
        $data = json_decode($string, true);
        return $this->_doImportDiscounts($data);
    }
    
    public function _doImportDiscounts($data)
    {
        $this->_importDiscountsLogId = time();
        $this->_clearImportDiscountsLog();
        $this->_addImportDiscountsLogMessage('Импорт скидок запущен');
        $this->_saveImportDiscountsLogCustomFileContent(self::custom_log_file_input, json_encode($data));
        
        $return = array();
        if (isset($data['item'])) {
            foreach ($data['item'] as $item) {
                //$item = (array)$item;
                $item = (array)$item;
                //$uid = $discount = false;
                $uid = (isset($item['uid']) && intval($item['uid']) > 0) ? intval($item['uid']) : false;
                
                
                //get user id by commonUID
                $commonUid = (isset($item['common_uid']) && intval($item['common_uid']) > 0) ? intval($item['common_uid']) : false;
                if ($commonUid !== false) {
                    $sel = new selector('objects');
                    $sel->types('object-type')->name('users', 'user');
                    $sel->where('common_uid')->equals($commonUid);
                    $sel->option('return')->value('id');
                    $sel->limit(0, 1);
                    
                    if ($user = $sel->first) {
                        $uid = isset($user["id"]) ? $user["id"] : false;
                    } else {
                        $this->_addImportDiscountsLogMessage('Не найден пользователь с common_uid ' . $commonUid);
                    }
                }
                
                $discount = (isset($item['discount']) && floatval($item['discount']) >= 0) ? floatval($item['discount']) : false;
                if ($uid !== false && $discount !== false) {
                    //save discount
                    //$user = umiObjectsCollection::getInstance() -> getObject($uid);
                    $status = 'notfound';
                    //if ($user instanceof umiObject) {
                    //$result = $this->setUserDiscounts($userId, $proc);
                    //$status = ($user -> getValue("disc") != '') ? 'change' : 'create';
                    $result = $this->setUserDiscounts($uid, $discount);
                    $result = (array)$result;
                    switch ($result['node:text']) {
                        case 'delete discount':
                            $status = 'delete_zero_discount';
                            break;
                        case 'skip zero discount':
                            $status = 'skip_zero_discount';
                            break;
                        case 'create discount':
                            $status = 'create';
                            break;
                        case 'error create discount':
                            $status = 'error_create_discount';
                            break;
                        case 'no user':
                            $status = 'notfound';
                            break;
                        case 'change discount':
                            $status = 'change';
                            break;
                        default:
                            $status = 'unknown_error';
                            break;
                    }
                    $this->_addImportDiscountsLogMessage('Обновлены данные о скидке для пользователя ' . $uid . ' (common_uid - ' . $commonUid . '). Статус - ' . $status);
                    //$user -> setValue("disc", $discount);
                    //$user -> commit();
                    //}
                    $return[] = array('uid' => $commonUid, 'discount' => $discount, 'status' => $status);
                } elseif($uid === false) {
                    $return[] = array('uid' => $commonUid ? $commonUid : $item['uid'], 'discount' => $discount, 'status' => 'not found UID');
                } elseif($discount === false) {
                    $return[] = array('uid' => $commonUid, 'discount' => $discount, 'status' => 'no discount provided');
                }
            }
        }
        
        $this->_addImportDiscountsLogMessage('Импорт скидок завершен');
        
        $this->_saveImportDiscountsLogCustomFileContent(self::custom_log_file_response, json_encode($this->_generateImportDiscountsResponse($return)));
        
        return $return;
    }
    
    public function _generateImportDiscountsResponse($result)
    {
        return array('status' => 'successfull', 'item' => $result);
    }
    
    //перебор пользователей и создать для низ скидку из поля disc
    public function testusersDiscount()
    {
        return 'switch off';
        //$userId = 10; // admin
        //$userId = 14126; // test@test.ru
        //$proc = 26;
        $users = new selector('objects');
        $users->types('object-type')->name('users', 'user');
        $users->where('disc')->isnotnull(true);
        //$users->limit(0, 2);
        foreach ($users as $user) {
            $userId = $user->id;
            $proc = $user->disc;
            
            $lines_arr[] = $result = $this->setUserDiscounts($userId, $proc);
            $result = (array)$result;
            var_dump($result);
            var_dump($result['node:text']);
            //var_dump($result);
            exit('ggg');
            
            $user->disc = '';
            $user->commit();
        }
        
        $block_arr = array('nodes:item' => $lines_arr);
        return array('items' => $block_arr);
    }
    
    //поиск или создание скидки для пользователя
    public function setUserDiscounts($userId = NULL, $proc = NULL)
    {
        if (!((int)$userId > 0 && (int)$proc > -1)) {
            return 'wrong params';
        }
        
        $objects = umiObjectsCollection::getInstance();
        
        // param for create discount
        // param for create discount
        $discountTypeId = 61; // Тип скидки - на товары
        $modificatorId = 138; // Модификатор цены - Процент от суммы
        $ruleId = 521593; // Правила валидации - На пользователя исключая некоторые товары
        $literatureId = 12909; //array(12909); // Раздел, литература, на который скидки не надо применять
        $gigiena_i_profilaktikaId = 12904; // Раздел, Гигиена и профилактика, на который скидки не надо применять
        
        $lines_arr = array();
        $user = $objects->getObject($userId);
        if ($user instanceof umiObject && $user->getTypeId() == 52) { // 52 - пользователь
            $discountName = $user->getValue('e-mail');
            
            // take discounts
            $discounts = new selector('objects');
            $discounts->types('object-type')->name('emarket', 'discount');
            $discounts->where('name')->equals($discountName);
            $discounts->limit(0, 1);
            
            if ($discount = $discounts->first) {
                if (!($proc > 0)) {
                    $discountId = $discount->id;
                    $discountObject = discount::get($discountId);
                    $discountObject->delete();
                    
                    return array(
                        'attribute:id' => $discountId,
                        'attribute:proc' => $proc,
                        'attribute:userid' => $userId,
                        'attribute:email' => $discountName,
                        'node:text' => 'delete discount'
                    );
                }
                
                //change discount proc
                $modificatorId = $discount->discount_modificator_id;
                $modificatorObject = $objects->getObject($modificatorId);
                if ($modificatorObject instanceof umiObject == false) {
                    // TODO: сделать нормальную реакцию на проблему
                    // continue;
                }
                
                $modificatorObject->proc = $proc;
                $modificatorObject->commit();
                
                //add category exception
                if ($rules = $discount->discount_rules_id) {
                    foreach ($rules as $ruleIdTmp) {
                        $ruleObjectTmp = $objects->getObject($ruleIdTmp);
                        
                        if ($ruleObjectTmp->rule_type_id == $ruleId) {
                            $ruleObjectTmp->catalog_items = array($literatureId, $gigiena_i_profilaktikaId);
                        }
                    }
                }
                $discount->is_active = true;
                $discount->commit();
                
                return array(
                    'attribute:id' => $discount->id,
                    'attribute:proc' => $modificatorObject->proc,
                    'attribute:userid' => $userId,
                    'attribute:email' => $discountName,
                    'node:text' => 'change discount'
                );
                //$lines_arr[] = $line;
                //$block_arr = array('nodes:item' => $lines_arr);
                //return array('items' => $block_arr);
            } else {
                if (!($proc > 0)) {
                    return array(
                        'attribute:id' => 'NULL',
                        'attribute:proc' => $proc,
                        'attribute:userid' => $userId,
                        'attribute:email' => $discountName,
                        'node:text' => 'skip zero discount'
                    );
                }
                //Create new discount
                $discount = discount::add($discountName, $discountTypeId);
                $modificatorTypeObject = $objects->getObject((int)$modificatorId);
                
                if ($modificatorTypeObject instanceof umiObject) {
                    $modificatorObject = discountModificator::create($discount, $modificatorTypeObject);
                    $discount->setDiscountModificator($modificatorObject);
                    
                    // устанавливаем процент скидки
                    $modificatorObject->proc = $proc;
                    $modificatorObject->commit();
                    
                    //Apply rules
                    $ruleTypeObject = $objects->getObject((int)$ruleId);
                    
                    if ($ruleTypeObject instanceof umiObject) {
                        $ruleObject = discountRule::create($discount, $ruleTypeObject);
                        if ($ruleObject instanceof discountRule) {
                            // сохраняем пользователя
                            $ruleObject->users = $userId;
                            
                            // сохраняем исключение на раздел
                            $ruleObject->catalog_items = array($literatureId, $gigiena_i_profilaktikaId);
                            
                            $ruleObject->commit();
                            
                            $discount->is_active = true;
                            $discount->appendDiscountRule($ruleObject);
                            $discount->commit();
                            
                            return array(
                                'attribute:id' => $discount->id,
                                'attribute:proc' => $modificatorObject->proc,
                                'attribute:userid' => $userId,
                                'attribute:email' => $discountName,
                                'node:text' => 'create discount'
                            );
                            
                            //$lines_arr[] = $line;
                            //$block_arr = array('nodes:item' => $lines_arr);
                            //return array('items' => $block_arr);
                        }
                    }
                }
                $discount->delete();
                
                return array(
                    'attribute:id' => $discount->id,
                    'attribute:proc' => $proc,
                    'attribute:userid' => $userId,
                    'attribute:email' => $discountName,
                    'node:text' => 'error create discount'
                );
            }
        }
        
        return array(
            'attribute:id' => 'NULL',
            'attribute:proc' => $proc,
            'attribute:userid' => $userId,
            'attribute:email' => 'NULL',
            'node:text' => 'no user'
        );
    }
    
    //тестирование создание скидки с новым правилом
    public function testSetUserDiscounts($userId = NULL, $proc = NULL)
    {
        $userId = 21014;
        $proc = 20;
        
        if (!((int)$userId > 0 && (int)$proc > -1)) {
            return 'wrong params';
        }
        
        $objects = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        
        // param for create discount
        $discountTypeId = 61; // Тип скидки - на товары
        $modificatorId = 138; // Модификатор цены - Процент от суммы
        $ruleId = 521593; // Правила валидации - На пользователя исключая некоторые товары
        $literatureId = 12909; //array(12909); // Раздел, литература, на который скидки не надо применять
        $gigiena_i_profilaktikaId = 12904; // Раздел, Гигиена и профилактика, на который скидки не надо применять
        $literature = $hierarchy->getElement($literatureId);
        
        $lines_arr = array();
        $user = $objects->getObject($userId);
        if ($user instanceof umiObject) {
            $discountName = $user->getValue('e-mail');
            
            // take discounts
            $discounts = new selector('objects');
            $discounts->types('object-type')->name('emarket', 'discount');
            $discounts->where('name')->equals($discountName);
            $discounts->limit(0, 1);
            
            if ($discount = $discounts->first) {
                if (!($proc > 0)) {
                    $discountId = $discount->id;
                    $discountObject = discount::get($discountId);
                    $discountObject->delete();
                    
                    return array(
                        'attribute:id' => $discountId,
                        'attribute:proc' => $proc,
                        'attribute:userid' => $userId,
                        'attribute:email' => $discountName,
                        'node:text' => 'delete discount'
                    );
                }
                
                //change discount proc
                $modificatorId = $discount->discount_modificator_id;
                $modificatorObject = $objects->getObject($modificatorId);
                if ($modificatorObject instanceof umiObject == false) {
                    // TODO: сделать нормальную реакцию на проблему
                    // continue;
                }
                
                $modificatorObject->proc = $proc;
                $modificatorObject->commit();
                
                //add category exception
                //$discount->getDiscountRules();
                if ($rules = $discount->discount_rules_id) {
                    foreach ($rules as $ruleIdTmp) {
                        $ruleObjectTmp = $objects->getObject($ruleIdTmp);
                        
                        if ($ruleObjectTmp->rule_type_id == $ruleId) {
                            $ruleObjectTmp->catalog_items = array($literatureId, $gigiena_i_profilaktikaId);
                        }
                    }
                }
                
                $discount->is_active = true;
                $discount->commit();
                
                return array(
                    'attribute:id' => $discount->id,
                    'attribute:proc' => $modificatorObject->proc,
                    'attribute:userid' => $userId,
                    'attribute:email' => $discountName,
                    'node:text' => 'change discount'
                );
            } else {
                if (!($proc > 0)) {
                    return array(
                        'attribute:id' => 'NULL',
                        'attribute:proc' => $proc,
                        'attribute:userid' => $userId,
                        'attribute:email' => $discountName,
                        'node:text' => 'skip zero discount'
                    );
                }
                //Create new discount
                $discount = discount::add($discountName, $discountTypeId);
                $modificatorTypeObject = $objects->getObject((int)$modificatorId);
                
                if ($modificatorTypeObject instanceof umiObject) {
                    $modificatorObject = discountModificator::create($discount, $modificatorTypeObject);
                    $discount->setDiscountModificator($modificatorObject);
                    
                    // устанавливаем процент скидки
                    $modificatorObject->proc = $proc;
                    $modificatorObject->commit();
                    
                    //Apply rules
                    $ruleTypeObject = $objects->getObject((int)$ruleId);
                    
                    if ($ruleTypeObject instanceof umiObject) {
                        $ruleObject = discountRule::create($discount, $ruleTypeObject);
                        if ($ruleObject instanceof discountRule) {
                            // сохраняем пользователя
                            $ruleObject->users = $userId;
                            
                            // сохраняем исключение на раздел
                            $ruleObject->catalog_items = array($literatureId, $gigiena_i_profilaktikaId);
                            
                            $ruleObject->commit();
                            
                            $discount->is_active = true;
                            $discount->appendDiscountRule($ruleObject);
                            $discount->commit();
                            
                            return array(
                                'attribute:id' => $discount->id,
                                'attribute:proc' => $modificatorObject->proc,
                                'attribute:userid' => $userId,
                                'attribute:email' => $discountName,
                                'node:text' => 'create discount'
                            );
                            
                            //$lines_arr[] = $line;
                            //$block_arr = array('nodes:item' => $lines_arr);
                            //return array('items' => $block_arr);
                        }
                    }
                }
                $discount->delete();
                
                return array(
                    'attribute:id' => $discount->id,
                    'attribute:proc' => $proc,
                    'attribute:userid' => $userId,
                    'attribute:email' => $discountName,
                    'node:text' => 'error create discount'
                );
            }
        }
        
        return array(
            'attribute:id' => 'NULL',
            'attribute:proc' => $proc,
            'attribute:userid' => $userId,
            'attribute:email' => 'NULL',
            'node:text' => 'no user'
        );
    }
    
    public $_importDiscountsLogId;
    
    public function _addImportDiscountsLogMessage($message)
    {
        if (!$this->_importDiscountsLogId) {
            return false;
        }
        
        file_put_contents($this->_getImportDiscountsLogDir() . '/' . $this->_importDiscountsLogId . '.log', date('d.m.Y H:i:s') . ' ' . $message . PHP_EOL, FILE_APPEND);
        
        return true;
    }
    
    public function _saveImportDiscountsLogCustomFileContent($fileName, $content = '')
    {
        if (!$this->_importDiscountsLogId) {
            return false;
        }
        
        $content = trim($content);
        if (!$content) {
            return false;
        }
        
        file_put_contents($this->_getImportDiscountsLogCustomFilePath($this->_importDiscountsLogId, $fileName), $content);
        
        return true;
    }
    
    public function _getImportDiscountsLogCustomFilePath($id, $fileName)
    {
        return $this->_getImportDiscountsLogDir() . '/' . $id . '_' . $fileName . '.json';
    }
    
    public function _getImportDiscountsLogDir($bCreate = true)
    {
        $dir = CURRENT_WORKING_DIR . '/sys-temp/logs/import_discounts';
        
        if ($bCreate && !is_dir($dir)) {
            mkdir($dir, 0775, true);
        }
        
        return $dir;
    }
    
    public function _clearImportDiscountsLog()
    {
        $dir = $this->_getImportDiscountsLogDir(false);
        if (!is_dir($dir)) {
            return false;
        }
        
        /* Логи хранятся 365 дней*/
        $time = time() - 86400 * 365;
        
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
            
            if ($file->getMTime() < $time) {
                unlink($file->getRealPath());
            }
        }
        
        return true;
    }
}