<?php
class emarket_custom extends def_module {
    public function __construct($self) {
        $self -> __loadLib("/userDeliveryAdressesList.php", (dirname(__FILE__)));
        $self -> __implement("emarket_user_delivery_adresses");

        $self -> __loadLib("/moderate_order.php", (dirname(__FILE__)));
        $self -> __implement("emarket_custom_moderate_order");

        $self -> __loadLib("/analytics.php", (dirname(__FILE__)));
        $self -> __implement("emarket_analytics_tags");

        $self -> __loadLib("/import_discounts.php", (dirname(__FILE__)));
        $self -> __implement("emarket_import_discounts");

        $self -> __loadLib("/import_orders.php", (dirname(__FILE__)));
        $self -> __implement("emarket_import_orders");

        $self -> __loadLib("/orders_clear.php", (dirname(__FILE__)));
        $self -> __implement("emarket_orders_clear");
    }

    public function registerEventOneClick($elementId = NULL) {
        //получить и проверить товар
        if (!$elementId){
            $elementId = getRequest('page_id');
        }
        if (!$elementId){
            $elementId = getRequest('param0');
        }

        $hierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();

        $element = $hierarchy -> getElement($elementId);
        if ($element instanceof umiHierarchyElement) {
            $dataModule = $controller -> getModule('data');
            // добавляем товар
            $_REQUEST['no-redirect'] = 1;
            $this -> basket('put', 'element', $elementId);

            //очищаем корзину от лишних товаров
            $order = $this -> getBasketOrder(false);
            foreach ($order->getItems() as $orderItem) {
                $orderItemElementId = $orderItem -> getItemElement() -> id;
                if ($orderItemElementId != $elementId) {
                    $order -> removeItem($orderItem);
                } else {
                    if ($orderItem -> getAmount() > 1) {
                        $orderItem -> setAmount(1);
                        $orderItem -> refresh();
                    }
                }
            }
            $order -> refresh();

            //сохраняем все сырые данные в заказ (в том числе название и id товара)
            $orderId = $order -> id;
            $dataModule -> saveEditedObjectWithIgnorePermissions($orderId, true, true);

            // пытаемся дополнить данные пользователя сырыми данными из формы
            $dataForm = getRequest('data');
            $dataForm = (isset($dataForm['new'])) ? $dataForm['new'] : array();
            $customer = customer::get();
            foreach ($dataForm as $fieldName => $fieldValue) {
                $fieldName = str_replace("order_", "", $fieldName);
                if ($customer -> getValue($fieldName) == '') {
                    $customer -> setValue($fieldName, $fieldValue);
                }
            }
            $customer -> commit();

            // сохраняем информацию о мероприятии
            $order -> order_date = time();
            $order -> page_id = $elementId;
            $order -> event_name = $element -> name;

            $gorod_in = $element -> gorod_in;
            $gorod_in_object = umiObjectsCollection::getInstance() -> getObject($gorod_in);
            if ($gorod_in_object) {
                $order -> event_gorod_in = $gorod_in_object -> name;
            }

            $publish_date = $element -> publish_date;
            if ($publish_date) {
                $publish_date = $publish_date -> getFormattedDate("U");
            }
            $finish_date = $element -> finish_date;
            if ($finish_date) {
                $finish_date = $finish_date -> getFormattedDate("U");
            }
            if ($finish_date && $publish_date) {
                $event_data = $dataModule -> dateruPeriod($publish_date, $finish_date);
                $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
            } else {
                $event_data_str = 'дата не определена';
            }
            $order -> event_data = $event_data_str;

            $order -> commit();

            // завершаем заказа или переходим к оплате
            // 1. переходим к оплате
            // if ($user -> yur_lico != 1 && !$event_discount_name && !$event_discount_value && ($element->price != 0 || $element->price != '')) {
            // $this -> redirect($this -> pre_lang . '/' . $controller -> getUrlPrefix() . 'emarket/purchase/payment/choose/');
            // }
            // 2. завершения заказа
            $order -> order();

            // увеличение кол-ва регистраций на мероприятие
            $element -> curr_reserv = $element -> curr_reserv + 1;
            $element -> commit();

            return $this -> redirect('/event_apply_successful/?o=' . $orderId);

            // скидки
            // $event_discount_name = getRequest('event_discount_name');
            // $order -> setValue('event_discount_name', $event_discount_name);
            // $event_discount_value = getRequest('event_discount_value');
            // $order -> setValue('event_discount_value', $event_discount_value);
        }

        return 'Мероприятие не найдено';
    }

    // вывод информацию о мероприятии на страницу с сообщением о проверке заказа менеджером
    public function eventInfo() {
        $orderId = getRequest('o');
        $order = umiObjectsCollection::getInstance() -> getObject($orderId);
        if (!$order){
            return;
        }
        $seminar_name = $order -> event_name;
        $data = $order -> event_data;
        $gorod_in = $order -> event_gorod_in;
        return "<br/><strong>'{$seminar_name}'</strong><br/>{$data},<br/>{$gorod_in}";
    }

    // вывод номера мероприятия
    public function eventOrderNum() {
        $orderId = getRequest('o');
        $order = umiObjectsCollection::getInstance() -> getObject($orderId);
        if (!$order){
            return;
        }
        return $order -> number;
    }

    // формирует новый номер для счета для бухгалтерии
    public function buhgalt_order_number($old_id = NULL) {
        if (!$old_id){
            $old_id = getRequest('param0');
        }
        $res = "СТ" . str_pad($old_id, 9, "0", STR_PAD_LEFT);
        return $res;
    }

    // для вывода квитка на печать
    public function receiptt($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-receipt-simple.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    // для вывода счета для юр лиц на печать
    public function invoicee($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-invoice.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    // для вывода квитка на печать в pdf
    public function receiptPrint($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-receipt-simple-print.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    // для вывода счета для юр лиц на печать в pdf
    public function invoiceePrint($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-invoice-print.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    // для вывода размера ндс с учетом ндс товаров счета для юр лиц на печать в pdf
    public function ndsSize($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $order = order::get($orderId);
        $orderItems = $order->getItems();

        $objects = umiObjectsCollection::getInstance();
        $tax_price = 0;

        foreach ($orderItems as $item){
            $element = $item->getItemElement();
            $discount = 1;
            // if($item->getTotalActualPrice()  != $item->getTotalOriginalPrice()){
                // $discount = ($item->getTotalActualPrice() / $item->getTotalOriginalPrice());
            // }

            $price = (double) ($item->getTotalActualPrice() / $item->getAmount() * $discount);
            $quantity = (double) $item->getAmount();

            $vat = $tax; // дефолтное значение НДС в 0%
            $nds_id = $element->nds;
            if($nds_id){
                $nds_obj = $objects->getObject($nds_id);
                if($nds_obj){
                    $element_vat = $nds_obj->vat;
                    if (!isset($element_vat) || $element_vat !== '') {
                        $vat = $element_vat;  // значение НДС из справочника
                        $taxMultiplier = (int) $nds_obj->name;
                        if($taxMultiplier == 18) {
                            $taxMultiplier = 20;
                        }
                        //if(!$taxMultiplier || $taxMultiplier = '0') $taxMultiplier = 0;
                        //var_dump($taxMultiplier);
                        $tax_price = $tax_price + (($price * $taxMultiplier) / (100 + $taxMultiplier));
                        //var_dump($price);
                        //var_dump($tax_price);
                    }
                    //var_dump('no vat');
                }
            }
        }
        // /exit('fff' . $tax_price);
        return $tax_price;
    }

    // сертификат для вывода на печать в pdf (для kerr)
    public function kerrPrint($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-kerr-print.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    public function saveInfoCust() {
        $order = $this->getBasketOrder(false);
        /* @var $order order*/

        if ($order->isEmpty()) {
            $this->redirect('/emarket/cart/');
        }

		$customer_id = customer::get()->getId();

        //сохранение регистрационных данных
        $cmsController = cmsController::getInstance();
        $data = $cmsController->getModule('data');
		$data->saveEditedObject($customer_id, false, true);

		//сохранение способа доставки
        $deliveryId = getRequest('delivery-id');
		if ($deliveryId) {
			if (strpos($deliveryId, 'self_') === 0) {
				// если это самовывоз
				$order->delivery_address = false;
				$deliveryId = substr($deliveryId, 5);
			} else {
				//сохранение адреса доставки если это не самовывоз
				$addressId = getRequest('delivery-address');
				if ($addressId == 'new') {
					$collection = umiObjectsCollection::getInstance();
					$types = umiObjectTypesCollection::getInstance();
					$typeId = $types->getTypeIdByHierarchyTypeName("emarket", "delivery_address");
					$customer = customer::get();
                    $addressId = $collection->addObject('Address for customer #' . $customer_id, $typeId);
//                    $addressId = $collection -> addObject($_REQUEST['data']['new']['name'], $typeId);
                    $dataModule = $cmsController->getModule("data");
					if ($dataModule) {
						if (!$dataModule->saveEditedObject($addressId, true, true)) {
							$dataModule->saveEditedObjectWithIgnorePermissions($addressId, true, true);
						}
						// начиная с версии 2.9.5
					}
					$customer->delivery_addresses = array_merge($customer->delivery_addresses, array($addressId));
				}
				$order->delivery_address = $addressId;
			}

			// сохранение желаемого времени и даты доставки
            $delivery_time = getRequest('prefer_time_delivery');
            $delivery_time_arr = explode('-', $delivery_time);
            if(isset($delivery_time_arr[0])){
                $order -> setValue('prefer_date_time_delivery_start', strtotime(getRequest('prefer_date_delivery') .' '. $delivery_time_arr[0] .':00'));
            }
            if(isset($delivery_time_arr[1])){
                $order -> setValue('prefer_date_time_delivery_finish', strtotime(getRequest('prefer_date_delivery') .' '. $delivery_time_arr[1].':00'));
            }

            $delivery = delivery::get($deliveryId);
            $deliveryPrice = (float)$delivery -> getDeliveryPrice($order);
            $order -> setValue('delivery_id', $deliveryId);
            $order -> setValue('delivery_price', $deliveryPrice);
        }

/*if ($_SERVER['REMOTE_ADDR'] == '5.18.249.80') {
            var_dump($_REQUEST);
            var_dump(getRequest('prefer_date_delivery'));
            var_dump(getRequest('prefer_time_delivery'));
            var_dump(strtotime(getRequest('prefer_date_delivery') .' '. getRequest('prefer_time_delivery')));
                exit('ggg');
        }*/

        $comment = getRequest('comment');
        $order -> setValue('comment', $comment);

        //сохранение способа оплаты и редирект на итоговую страницу
        $order -> setValue('moderate', 1);

        // сохранение юр адреса если он был выбран или он был новый
        $paymentId = getRequest('payment-id');
        if ($paymentId == '13911') { // способ оплаты для юр лиц
            $personId = getRequest('legal_item');
            if ($personId == 'new') {
                $collection = umiObjectsCollection::getInstance();
                $types = umiObjectTypesCollection::getInstance();
                $typeId = $types -> getTypeIdByHierarchyTypeName("emarket", "legal_person");
                $customer = customer::get();
                $personId = $collection -> addObject("Legal person for customer #" . $customer -> getId(), $typeId);
                $dataModule = $cmsController -> getModule("data");
                if ($dataModule) {
                    if (!$dataModule -> saveEditedObject($personId, true, true)){
                        $dataModule -> saveEditedObjectWithIgnorePermissions($personId, true, true);
                    }
                    // начиная с версии 2.9.5
                }
                $customer -> legal_persons = array_merge($customer -> legal_persons, array($personId));
            }
            $order->legal_person = $personId;
            $order->payment_document_num = $order->id;
        }
        $order -> setValue('payment_id', $paymentId);
        $order -> commit();
        $order -> refresh();
        $order -> order();

        $order->setValue('need_export', 0);
        $order->setValue('need_export_new', 1);
        
        $gaClientId = SiteGoogleAnalyticsHelper::getClientId();
        
        $order->setValue(SiteEmarketOrderModel::field_ga_cid, $gaClientId);
        $order->commit();
        
        $userId = $order->getCustomerId();
        if($userId) {
            $user = umiObjectsCollection::getInstance()->getObject($userId);
            if($user instanceof umiObject) {
                $user->setValue(SiteUsersUserModel::field_ga_cid, $gaClientId);
                $user->commit();
            }
        }

        //сброс скидки на товары
        if ($paymentId == '13911') { // способ оплаты для юр лиц
            $orderItems = $order->getItems();

            foreach ($orderItems as $item){
                $item->setDiscount();
                $item->setDiscountValue(0);
                $item->commit();
            }

            $order -> refresh();
        }

        $this->change_order_status_from_1c_status($order->getId(), "Принят", "false");

        $urlPrefix = cmsController::getInstance() -> getUrlPrefix() ? (cmsController::getInstance() -> getUrlPrefix() . '/') : '';
        $this -> redirect($this -> pre_lang . '/' . $urlPrefix . 'emarket/premoderate/'.$order -> id);

        /*
         $paymentId = getRequest('payment-id');
         if(!$paymentId) {
         $this->errorNewMessage(getLabel('error-emarket-choose-payment'));
         $this->errorPanic();
         }
         $payment = payment::get($paymentId, $order);

         if($payment instanceof payment) {
         $paymentName = $payment->getCodeName();
         $url = "{$this->pre_lang}/" . cmsController::getInstance()->getUrlPrefix()."emarket/purchase/payment/{$paymentName}/";
         } else {
         $url = "{$this->pre_lang}/" . cmsController::getInstance()->getUrlPrefix()."emarket/cart/";
         }
         $this->redirect($url);*/
    }

    // для вывода счета для юр лиц на печать
    public function premoderate($orderId = NULL) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }
        return array('orderId'=>$orderId);
    }
    /*add legal, or save it
     ** legal id take from request info
     ** for example new legal : <input name="legal-person" value="new" />
     ** or legal with id = 1112 <input name="legal-person" value="1112" />
     ** may add ref_onsuccess url (page id or page url) by this <input type="hidden" name="ref_onsuccess" value="" />
     */
    public function legalAdd($template = 'default') {
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types -> getBaseType("emarket", "legal_person");
        $customer = customer::get();

        //$personId = getRequest('legal-person');
        $personId = getRequest('param0');
        $isNew = ($personId == null || $personId == 'new');
        if ($isNew) {
            $typeId = $types -> getBaseType("emarket", "legal_person");
            $customer = customer::get();
            $personId = $collection -> addObject("", $typeId);
            $customer -> legal_persons = array_merge($customer -> legal_persons, array($personId));
        }
        $controller = cmsController::getInstance();
        $data = getRequest('data');
        if ($data && $dataModule = $controller -> getModule("data")) {
            $key = $isNew ? 'new' : $personId;
            $person = $collection -> getObject($personId);
            $person -> setName($data[$key]['name']);
            $dataModule -> saveEditedObject($personId, $isNew, true);
        }

        $url = getRequest('ref_onsuccess');
        if ($url){
            $this -> redirect($url);
        }

        $referer_url = $_SERVER['HTTP_REFERER'];
        $this -> redirect($referer_url);
    }

    /* del legal
     ** legal id take from request info
     ** for example legal with id = 1112 <input name="legal-person" value="1112" />
     ** may add ref_onsuccess url (page id or page url) by this <input type="hidden" name="ref_onsuccess" value="" />
     */
    public function legal_delete($template = 'default') {
		$collection = umiObjectsCollection::getInstance();
		$personId = getRequest('legal-person');
		if ($collection->isExists($personId)) {
			$customer = customer::get();
			if($customer instanceof customer){
				$customer->legal_persons = array_diff($customer->legal_persons, array($personId));
				$customer->commit();
				$collection->delObject($personId);
			}
		}

		$url = getRequest('ref_onsuccess');
		$referer_url = empty($url) ? $_SERVER['HTTP_REFERER'] : $url;
		$this->redirect($referer_url);
	}

	/*
	 * output legal list on template /tpls/emarket/payment/invoice/{template}.tpl
     */
    public function legalList($template = 'default') {
        $tpl_block = $tpl_item = '';
		$collection = umiObjectsCollection::getInstance();
		$types = umiObjectTypesCollection::getInstance();
		$typeId = $types->getBaseType("emarket", "legal_person");
		$customer = customer::get();

		$items = array();
		$persons = $customer->legal_persons;
		if (is_array($persons)) {
			foreach ($persons as $personId) {
				$person = $collection->getObject($personId);
				$item_arr = array('attribute:id' => $personId, 'attribute:name' => $person->name);
				$items[] = def_module::parseTemplate($tpl_item, $item_arr, false, $personId);
			}
		}
		if ($tpl_block) {
			return def_module::parseTemplate($tpl_block, array('items' => $items, 'type_id' => $typeId));
		} else {
			return array('attribute:type-id' => $typeId, 'xlink:href' => 'udata://data/getCreateForm/' . $typeId, 'items' => array('nodes:item' => $items));
		}
	}


    /**
     * TODO: Write documentation
     *
     * All these cases renders full basket order:
     * /udata/emarket/basket/ - do nothing
     * /udata/emarket/basket/add/element/9 - add element 9 into the basket
     * /udata/emarket/basket/add/element/9?amount=5 - add element 9 into the basket + amount
     * /udata/emarket/basket/add/element/9?option[option_name_1]=1&option=2&option[option_name_2]=3 - add element 9 using options
     * /udata/emarket/basket/modify/element/9?option[option_name_1]=1&option=2&option[option_name_2]=3 - add element 9 using options
     * /udata/emarket/basket/modify/item/9?option[option_name_1]=1&option=2&option[option_name_2]=3 - add element 9 using options
     * /udata/emarket/basket/remove/element/9 - remove element 9 from the basket
     * /udata/emarket/basket/remove/item/111 - remove orderItem 111 from the basket
     * /udata/emarket/basket/remove_all/ - remove all orderItems from basket
     */
    public function addSeveralBrackets($mode = false, $itemType = false, $itemIds = false) {
        $mode = $mode ? $mode : getRequest('param0');
        $order = $this->getBasketOrder(!in_array($mode, array('put', 'remove')));

        $itemType = $itemType ? $itemType : getRequest('param1');
        $itemIds =  ($itemIds ? $itemIds : getRequest('param2'));
        //$amount = (int) getRequest('amount');
        $options = getRequest('options');

        $pieces = explode("_", $itemIds);
        //exit('bbb');
        //var_dump($itemIds);
        //var_dump($pieces);
        //exit('qfaaa');
        foreach($pieces as $braketInfo){
            $braketInfo_arr = explode("|", $braketInfo);
            if(!isset($braketInfo_arr[0]) || !isset($braketInfo_arr[1])) continue;

            $itemId = $braketInfo_arr[0];
            $amount = $braketInfo_arr[1];


            if($mode == 'put') {
                $newElement = false;
                if ($itemType == 'element') {
                    $orderItem = $this->getBasketItem($itemId, false);
                    if (!$orderItem) {
                        $orderItem = $this->getBasketItem($itemId);
                        $newElement = true;
                    }
                } else {
                    $orderItem = $order->getItem($itemId);
                }

                if (!$orderItem) {
                    throw new publicException("Order item is not defined");
                }

                if(is_array($options)) {
                    if($itemType != 'element') {
                        throw new publicException("Put basket method required element id of optionedOrderItem");
                    }

                    // Get all orderItems
                    $orderItems = $order->getItems();
                    foreach($orderItems as $tOrderItem) {
                        if (!$tOrderItem instanceOf optionedOrderItem) {
                            $itemOptions = null;
                            $tOrderItem = null;
                            continue;
                        }

                        $itemOptions = $tOrderItem->getOptions();

                        if(sizeof($itemOptions) != sizeof($options)) {
                            $itemOptions = null;
                            $tOrderItem = null;
                            continue;
                        }

                        if($tOrderItem->getItemElement()->id != $orderItem->getItemElement()->id) {
                            $itemOptions = null;
                            $tOrderItem = null;
                            continue;
                        }

                        // Compare each tOrderItem with options list
                        foreach($options as $optionName => $optionId) {
                            $itemOption = getArrayKey($itemOptions, $optionName);

                            if(getArrayKey($itemOption, 'option-id') != $optionId) {
                                $tOrderItem = null;
                                continue 2;		// If does not match, create new item using options specified
                            }
                        }

                        break;	// If matches, stop loop and continue to amount change
                    }

                    if(!isset($tOrderItem) || is_null($tOrderItem)) {
                        $tOrderItem = orderItem::create($itemId);
                        $order->appendItem($tOrderItem);
                        if ($newElement) {
                            $orderItem->remove();
                        }
                    }

                    if($tOrderItem instanceof optionedOrderItem) {
                        foreach($options as $optionName => $optionId) {
                            if($optionId) {
                                $tOrderItem->appendOption($optionName, $optionId);
                            } else {
                                $tOrderItem->removeOption($optionName);
                            }
                        }
                    }

                    if($tOrderItem) {
                        $orderItem = $tOrderItem;
                    }
                }

                $amount = $amount ? $amount : ($orderItem->getAmount() + 1);
                $orderItem->setAmount($amount ? $amount : 1);
                $orderItem->refresh();

                if($itemType == 'element') {
                    $order->appendItem($orderItem);
                }
            }

            if($mode == 'remove') {
                $orderItem = ($itemType == 'element') ? $this->getBasketItem($itemId, false) : orderItem::get($itemId);
                if($orderItem instanceof orderItem) {
                    $order->removeItem($orderItem);
                }
            }

            if ($mode == 'remove_all') {
                foreach ($order->getItems() as $orderItem) {
                    $order->removeItem($orderItem);
                }
            }

            $order->refresh();
        }

        $referer = getServer('HTTP_REFERER');
        $noRedirect = getRequest('no-redirect');

        if($redirectUri = getRequest('redirect-uri')) {
            $this->redirect($redirectUri);
        } else if (!defined('VIA_HTTP_SCHEME') && !$noRedirect && $referer) {
            $current = $_SERVER['REQUEST_URI'];
            if(substr($referer, -strlen($current)) == $current) {
                if($itemType == 'element') {
                    $referer = umiHierarchy::getInstance()->getPathById($itemId);
                } else {
                    $referer = "/";
                }
            }
            $this->redirect($referer);
        }

        return $this->order($order->getId());
    }

    public function addCustomerToOrder() {
        return 'switch off';
        //return 'ok22';
        //$orderId = 19643;
        $orderId = 19342;
        $order = umiObjectsCollection::getInstance() -> getObject($orderId);

        //$order->password = "490ac4580473e83ddde9c1a2d5bbd46a";
        $order->customer_id = 20562;

        $order->commit();
        //$order->refresh();
        return 'ok';
    }

    public function test() {
        //return 'switch off';
        $customerId = 1390672;
        $customerObj = umiObjectsCollection::getInstance() -> getObject($customerId);
        $customerObj->lname = '';
    }
    public function cloneOrderTest() {
        return 'switch off';

        $orderId = 1248933;
        $newOrderId = umiObjectsCollection::getInstance()->cloneObject($orderId);

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->order('number')->desc();
        $sel->limit(0, 1);
        $number = $sel->first ? ($sel->first->number + 1) : 1;

        regedit::getInstance()->setVal('emarket/lastOrderNumber', $number);

        $order = umiObjectsCollection::getInstance() -> getObject($newOrderId);
        $order->name = getLabel('order-name-prefix', 'emarket', $number);
        $order->number = $number;
        $order->commit();
    }

    public function needUserInfo() {
        $customer = customer::get();
        // get type or type id
        $types = umiObjectTypesCollection::getInstance();
        $customerId = $customer->id;
        $customerObj = umiObjectsCollection::getInstance() -> getObject($customerId);
        //var_dump($customer);
        //exit('hhh');
        $typeId = $customerObj -> getTypeId();

        $type = $types->getType($typeId);
        if (!$type instanceof umiObjectType){
            return $needToFill = 1;
        }

        $allFields = $type->getAllFields();

        $errorFields = array();
        $needToFill = 0;
        foreach($allFields as $field) {
            if($field->getIsRequired()) {
                $fieldName = $field->getName();
                if ( !$customerObj->getValue($fieldName) || $customerObj->getValue($fieldName) == '' ) {
                    //$errorFields[] = $field->getId();
                    $needToFill = 1;
                }
            }
        }
        return $needToFill;
    }

    public function try_order_pay(){
        $payanyway_id = 1106995;
		$collection = umiObjectsCollection::getInstance();

        $order_number = getRequest('order_number');
        $order_id = getRequest('order_id');

        if(!empty($order_number)){
            $orders = new selector('objects');
            $orders->types('object-type')->name('emarket', 'order');
            $orders->where('number')->equals($order_number);
            foreach($orders as $order){
                $order_id = $order->id;
                break;
            }
        }

        $order_obj = $collection->getObject($order_id);
        if($order_obj instanceof umiObject && $order_obj->getTypeId() == 51){
            // 117 - Готов, 126 - Отклонен, 250 - Отменен
            if($order_obj->getValue('status_id') == 117 || $order_obj->getValue('status_id') == 126 || $order_obj->getValue('status_id') == 250 ){
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            // 1606397 - Готов, 1606396 - Оплачен, 1595826 - Отгружен
            if($order_obj->getValue('status_1c') == 1606397 || $order_obj->getValue('status_1c') == 1606396 || $order_obj->getValue('status_1c') == 1595826 ){
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            // 192 - принята
            if ($order_obj->getValue('payment_status_id') == 192) {
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            // 1595921 - оплачено
            if ($order_obj->getValue('status_payment_1c') == 1595921) {
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            $order_obj->setValue('status_id', 58); // ожидает проверки - чтобы не отправлялось уведомление
            $order_obj->setValue('payment_id', $payanyway_id);
            $order_obj->commit();

			// убираем у пользователя из списка последних заказов конкретно этот заказ
			$customer_id = $order_obj->getValue('customer_id');
			$customer = $collection->getObject($customer_id);
			if($customer instanceof umiObject){
				$last_order = $customer->getValue('last_order');
				// если есть - удаляем
				if($last_order[0]['rel'] == $order_obj->getId()){
					$customer->setValue('last_order', array());
					$customer->commit();
				}
			}

            $order = order::get($order_id);
            if($order instanceof order){
                $payment = payment::get($payanyway_id, $order);
                $result = array(
                    "purchasing" => $payment->process()
                );
                $result["purchasing"]["attribute:stage"] = 'payment';
                $result["purchasing"]["attribute:step"] = 'payanyway';
                $result['attribute:order_id'] = $order_id;
                $result['attribute:order_number'] = $order_number;
                return $result;
            }
        }
    }

    public function send_letter_on_payment_accept(iUmiEventPoint $event) {
        if ($event->getMode() == "after" && $event->getParam("old-status-id") != $event->getParam("new-status-id")) {
            $order = $event->getRef("order");
            if ($event->getParam("new-status-id") == order::getStatusByCode('accepted', 'order_payment_status')) {
                $this->change_order_status_from_1c_status($order->getId(), '', 'Оплачен');
            }
        }
    }

	/**
	 * Получение лучших клиентов за период
	 * @param type $start_date
	 * @param type $end_date
	 * @return type
	 */
	public function get_top_clients_for_stat($start_date, $end_date, $limit) {
		$result = array(
			"top_price" => array(
				"items" => array(
					"nodes:item" => array()
				),
				"total" => 0
			),
			"top_count" => array(
				"items" => array(
					"nodes:item" => array()
				),
				"total" => 0
			),
			"orders" => array(
				"items" => array(
					"nodes:item" => array()
				),
				"total" => 0
			),
			"all_customers" => array(
				"items" => array(
					"nodes:item" => array()
				),
				"total" => 0
			),
			"start" => $start_date,
			"end" => $end_date,
			"start_date" => date("d.m.Y", $start_date),
			"end_date" => date("d.m.Y", $end_date),
		);

		$orders_obj = array();
		$customers = array();

		$orders = new selector('objects');
		$orders->types('object-type')->name('emarket', 'order');
		$orders->where('order_date')->between($start_date, $end_date);
		$orders->where('number')->isnotnull(true);
//		$orders->limit(0, 10000);
		$result['orders']['total'] = $orders->length();
		foreach ($orders as $order) {
			$orders_obj[] = array(
				"attribute:id" => $order->getId(),
				"attribute:number" => $order->getValue('number'),
				"attribute:price" => $order->getValue('total_price'),
				"attribute:customer_id" => $order->getValue('customer_id'),
				"attribute:date" => date("d.m.Y", $order->getValue('order_date')->getDateTimeStamp()),
			);
			$customers[$order->getValue('customer_id')]['attribute:id'] = $order->getValue('customer_id');
			$customers[$order->getValue('customer_id')]['attribute:count'] += 1;
			$customers[$order->getValue('customer_id')]['attribute:price'] += $order->getValue('total_price');
		}
		$result['orders']['items']['nodes:item'] = $orders_obj;
		$result['all_customers']['items']['nodes:item'] = $customers;
		$result['all_customers']['total'] = sizeof($customers);


		$top_price = $top_count = $customers;

		$sv_users = new selector('objects');
		$sv_users->types('object-type')->name('users', 'user');
		$sv_users->where('groups')->equals(9); // 9 - супервизоры
		foreach($sv_users as $sv_user){
			if(isset($customers[$sv_user->getid()])){
				unset($top_price[$sv_user->getid()]);
				unset($top_count[$sv_user->getid()]);
			}
		}

		usort($top_price, function($a, $b) {
			return $a['attribute:price'] < $b['attribute:price'];
		});
		$top_price = array_slice($top_price, 0, $limit);
		$result['top_price']['items']['nodes:item'] = $top_price;
		$result['top_price']['total'] = sizeof($top_price);

		usort($top_count, function($a, $b) {
			return $a['attribute:count'] < $b['attribute:count'];
		});
		$top_count = array_slice($top_count, 0, $limit);
		$result['top_count']['items']['nodes:item'] = $top_count;
		$result['top_count']['total'] = sizeof($top_count);

		return $result;
	}
}