<?php
class emarket_import_orders extends def_module {
    /*импорт скидко пользователей по uid из 1С*/
    public function order1csynTmp() {
        //$str = '{"item": [ {   "uid": "17023", "discount": "20.50" }, {   "uid": "1218307", "discount": "10.00" }, {   "uid": "190099", "discount": "10.00" }, {   "uid": "209950", "discount": "20.00" }, {   "uid": "5459", "discount": "0" }]}';
        //$input = (array)json_decode($str,true);
        $input = (array)json_decode(file_get_contents('php://input'), true);
        //file_put_contents(CURRENT_WORKING_DIR . '/traceajaxOrders.txt', "\n\n" . print_r(file_get_contents('php://input'), true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        //file_put_contents(CURRENT_WORKING_DIR . '/traceajaxOrders.txt', "\n\n" . print_r($input, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        //$_REQUEST['data'] = 'fff';
        //var_dump(file_get_contents('php://input'));
        //var_dump((array)json_decode(file_get_contents('php://input'), true));
        //exit('ggg');
        $result = array('status' => 'successfull');//, 'item' => $item_arr);
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer -> charset('utf-8');
        $buffer -> contentType('application/json');
        $buffer -> push(json_encode($result));
        $buffer -> end();
    }
    
    /*вывод способов оплат для 1С*/
    public function showPaymentChoose() {
        $guide = new selector('objects');
        $guide->types('object-type')->id(46); // id справочника со способами оплаты
        //$guide->where('disc')->isnotnull(true);
        
        foreach($guide as $guideItem){
            $guideItemId = $guideItem->id;
            $name = $guideItem->name;
            $item = array('attribute:id'=>$guideItemId, 'attribute:name'=>$name) ;
            $item_arr[] = $item;
        }
        
        return array('nodes:item'=>$item_arr);
    }
    
    /*вывод способов доставки для 1С*/
    public function showDeliveryChoose() {
        $guide = new selector('objects');
        $guide->types('object-type')->id(49); // id справочника со способами оплаты
        //$guide->where('disc')->isnotnull(true);
        
        foreach($guide as $guideItem){
            $guideItemId = $guideItem->id;
            switch ($guideItemId) {
                case '165023':
                    $name = 'dostavkaspb';
                    break;
                case '165024':
                    $name = 'dostavkaspb';
                    break;
                case '165022':
                    $name = 'dostavkamsk';
                    break;
                case '1229974':
                    $name = 'dostavkamsk';
                    break;
                
                case '13919':
                    $name = 'dostavkarf';
                    break;
                
                default:
                    $name = 'notfound';
                    break;
            }
            
            $item = array(
                'attribute:id'=>$guideItemId,
                'attribute:name'=>$name,
            ) ;
            
            $item_arr[] = $item;
        }
        
        return array('nodes:item'=>$item_arr);
    }
    
    /*импорт скидко пользователей по uid из 1С*/
    public function order1csyn() {
        $resultInfo = $resultError = array();
        $input = json_decode(file_get_contents('php://input'), true);
        
        //$content = '{"Заказы": { "Заказ": { "СтатусЗаказа1С": "Сборка", "СтатусОплатыЗаказа1С": "Оплачен", "СтатусЗаказа": "К оплате", "ИД": "2419266", "Оплата": { "ИД": "13911", "Название": "Счет для юр.лиц", "Статус": "-" }, "Доставка": { "ИД": "165022", "Название": "Доставка по Москве и МО", "Стоимость": "500.00" }, "Товары": {"Товар": [  { "ИД": "b694414a-649a-11e2-4d9a-0015179e7064", "Количество": 3.00, "СтоимостьБезСкидки": 1240.00, "Стоимость": 1240.00, "ИтогоБезСкидки": 3720.00, "Итого": 3720.00 },  { "ИД": "a64abbd0-267a-11e3-93fe-005056b0350d", "Количество": 7.00, "СтоимостьБезСкидки": 1240.00, "Стоимость": 1240.00, "ИтогоБезСкидки": 8680.00, "Итого": 8680.00 },  { "ИД": "d27545ca-f3e3-469d-9a0a-fb59fbc3d069", "Количество": 2.00, "СтоимостьБезСкидки": 615.00, "Стоимость": 615.00, "ИтогоБезСкидки": 1230.00, "Итого": 1230.00 },  { "ИД": "be4aa5e2-7b54-4635-a3a9-e5bcaa1db7e2", "Количество": 2.00, "СтоимостьБезСкидки": 2418.00, "Стоимость": 2418.00, "ИтогоБезСкидки": 4836.00, "Итого": 4836.00 },  { "ИД": "f955b87e-b1f0-41c4-9ab2-e4735446c008", "Количество": 7.00, "СтоимостьБезСкидки": 665.00, "Стоимость": 665.00, "ИтогоБезСкидки": 4655.00, "Итого": 4655.00 } ] }, "Покупатель": { "email": "test@test.com", "lname": "Rumyantsev", "fname": "Pavel", "father_name": "Ivanovich", "ЮрЛицо": { "ИНН": "35533534646", "РасчетныйСчет": "1321234356578865", "НаименованиеБанка": "ПАО \"ПРОМСВЯЗЬБАНК\"", "КорСчет": "30101810400000000555", "БИК": "044525555", "ОГРН": "333222111", "КПП": "456457437", "ЮридическийАдрес": "Москва г, Ленинградский пр-кт, дом № 30, корпус 3", "ФактическийАдрес": "Москва г, Ленинградский пр-кт, дом № 30, корпус 3", "ПочтовыйАдрес": "Москва г, Ленинградский пр-кт, дом № 30, корпус 3", "НаименованиеОрганизации": "тестназвание3110-изменение в 1С", "КонтактноеЛицо": "Rumyantsev Pavel Ivanovich", "КонтактныйТелефон": "" } } } } }';
        //$input = json_decode($content, true);
    
        $logFile = CURRENT_WORKING_DIR . '/sys-temp/logs/order1csyn/' . date('Ymd') . '.log';
        if(!is_dir(dirname($logFile))) {
            mkdir(dirname($logFile), 0775, true);
        }
        file_put_contents($logFile, date('H:i:s') . PHP_EOL . print_r($input, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        
        //var_dump((array)json_decode(file_get_contents('php://input'), true));
        //exit('ggg');
        
        if(json_last_error() == JSON_ERROR_NONE){
            $input = (array) $input;
            if (isset($input['Заказы']['Заказ'])) {
                $orderData = $input['Заказы']['Заказ'];
                
                $umiObjectsCollection = umiObjectsCollection::getInstance();
                $hierarchy = umiHierarchy::getInstance();
                $orderData = (array) $orderData;
                $orderIdTmp = $orderData["ИД"];
                //$orderId = $orderIdTmp - 1200000;
                $orderId = $orderIdTmp;
                
                // check isset order
                $order = $umiObjectsCollection->getObject($orderId);
                //$status = 'notfound';
                if ($order instanceof umiObject) {
                    $resultInfo['order1CId'] = $orderIdTmp;
                    $resultInfo['orderId'] = $orderId;
                    $paymentId = false;
                    // способ оплаты
                    if (isset($orderData["Оплата"]["ИД"])) {
                        $paymentId = $orderData["Оплата"]["ИД"];
                        if ($order->payment_id == $paymentId) {
                            $resultInfo['paymentId'] = 'Without change. The same value';
                        } else {
                            $order->payment_id = $paymentId;
                            $resultInfo['paymentId'] = $paymentId;
                        }
                    }
                    // статус оплаты
                    if (isset($orderData["Оплата"]["Статус"])) {
                        $paymentStatus1C = $orderData["Оплата"]["Статус"];
                        if ($paymentStatus1C == '-') {
                            $paymentStatus1C = 0;
                        }
                        
                        if ($order->payment_status_id_1c == $paymentStatus1C) {
                            $resultInfo['paymentStatus1C'] = 'Without change. The same value';
                        } else {
                            $order->payment_status_id_1c = $paymentStatus1C;
                            $resultInfo['paymentStatus1C'] = $paymentStatus1C;
                        }
                    }
                    
                    // способ доставки
                    if (isset($orderData["Доставка"]["ИД"])) {
                        $deliveryId = $orderData["Доставка"]["ИД"];
                        if ($order->delivery_id == $deliveryId) {
                            $resultInfo['deliveryId'] = 'Without change. The same value';
                        } else {
                            $order->delivery_id = $deliveryId;
                            $resultInfo['deliveryId'] = $deliveryId;
                        }
                    }
                    
                    // стоимость доставки
                    if (isset($orderData["Доставка"]["Стоимость"])) {
                        $deliveryPrice = $orderData["Доставка"]["Стоимость"];
                        
                        if ($order->delivery_price == $deliveryPrice) {
                            $resultInfo['deliveryPrice'] = 'Without change. The same value';
                        } else {
                            $order->delivery_price = $deliveryPrice;
                            $resultInfo['deliveryPrice'] = $deliveryPrice;
                        }
                    }
                    
                    $couponId = false;
                    $couponCode = false;
                    
                    if(isset($orderData['Промокод'])) {
                        $coupon = SiteOrmcoCouponsCouponModel::getCouponObjectByCode($orderData['Промокод']);
                        
                        if($coupon instanceof umiObject) {
                            $couponId = $coupon->getId();
                            $couponCode = $coupon->getValue(SiteOrmcoCouponsCouponModel::field_code);
                        }
                    }
                    
                    $order->setValue(SiteEmarketOrderModel::field_ormco_coupon_id, $couponId);
                    $order->setValue(SiteEmarketOrderModel::field_ormco_coupon_code, $couponCode);
                    $order->commit();
                    
                    // change cart item
                    if (isset($orderData["Товары"]["Товар"])) {
                        // берем список всех товаров
                        $orderIns = order::get($orderId);
                        if ($orderIns instanceof order == false) {
                            $resultError['ErGetOrder'] = 'Error get Order instance with Id - ' . $orderId;
                        } else {
                            $cartItemsArr = array();
                            $orderItems = $orderIns->getItems();
                            foreach ($orderItems as $orderItem) {
                                $orderIns->removeItem($orderItem);
                                /* = $orderItem->getItemElement();
                                  if($element instanceof umiHierarchyElement) {
                                  $pageId = $element->getId();
                                  $cartItemsArr[$pageId] = $orderItem->getId();
                                  } */
                            }
                            $orderIns->refresh();
                            
                            $orderIns_add = order::get($orderId, true);
                            
                            // перебираем данные о товарах из 1С
                            $itemsData = (array) $orderData["Товары"]["Товар"];
                            
                            // добавить товары
                            foreach ($itemsData as $key => $itemData) {
                                $itemDataId = (isset($itemData['ИД'])) ? $itemData['ИД'] : false;
                                $itemDataAmount = (isset($itemData['Количество'])) ? intval($itemData['Количество']) : false;
                                
                                $arr_key = 'cartItem_' . $key;
                                if (!$itemDataId || !$itemDataAmount) {
                                    $resultInfo[$arr_key]['NotEnoughParams'] = 'Not enough params: $itemDataId-' . $itemDataId . ', $itemDataAmount-' . $itemDataAmount;
                                    break;
                                } else {
                                    // search item
                                    // get pageId by 1C id
                                    $pageId = self::getPageIdBy1CId($itemDataId);
                                    
                                    $resultInfo[$arr_key]['id_1c'] = $itemDataId;
                                    
                                    $page = $hierarchy->getElement($pageId);
                                    if ($page instanceof umiHierarchyElement) {
                                        $resultInfo[$arr_key]['cart_item_page_id'] = $pageId;
                                        $resultInfo[$arr_key]['name'] = $page->name;
                                        
                                        //if(!isset($cartItemsArr[$pageId])){
                                        //add cart item
                                        $orderItem = orderItem::create($pageId);
                                        $itemId = $orderItem->getId();
                                        $resultInfo[$arr_key]['method'] = 'Add';
                                        
                                        //}
                                        $resultInfo[$arr_key]['cart_item_id'] = $itemId;
                                        
                                        // set item params
                                        $orderItem->setAmount($itemDataAmount ? $itemDataAmount : 1);
                                        if($couponId && $couponCode) {
                                            $orderItem->setValue(SiteEmarketOrderItemModel::field_ormco_coupon_id, $couponId);
                                            $orderItem->setValue(SiteEmarketOrderItemModel::field_ormco_coupon_code, $couponCode);
                                            $orderItem->setValue(SiteEmarketOrderItemModel::field_ormco_coupon_disable_update_price, true);
                                        }
                                        $orderItem->refresh();
                                        
                                        //if(!isset($cartItemsArr[$pageId])){
                                        $orderIns_add->appendItem($orderItem);
                                        //}
                                        
                                        $cartItemsArr[$key] = $itemId;
                                    } else {
                                        $resultInfo[$arr_key]['NoPage'] = 'Not found page with pageId = ' . $pageId . ' related with 1C id = "' . $itemDataId . '"';
                                    }
                                }
                            }
                            
                            $orderIns_add->refresh();
                            
                            // изменение цен и скидок
                            foreach ($itemsData as $key => $itemData) {
                                $itemDataId = (isset($itemData['ИД'])) ? $itemData['ИД'] : false;
                                $itemDataAmount = (isset($itemData['Количество'])) ? intval($itemData['Количество']) : false;
                                $itemDataPrice = (isset($itemData['Стоимость'])) ? $itemData['Стоимость'] : false;
                                $itemDataTotalOriginalPrice = (isset($itemData['ИтогоБезСкидки'])) ? $itemData['ИтогоБезСкидки'] : false;
                                $itemDataTotalPrice = (isset($itemData['Итого'])) ? $itemData['Итого'] : false;
                                
                                $arr_key = 'cartItem_' . $key;
                                if (!$itemDataId || !$itemDataAmount || !$itemDataPrice || !$itemDataTotalOriginalPrice || !$itemDataTotalPrice) {
                                    $resultInfo[$arr_key]['NotEnougnParams'] = 'Not enough params: $itemDataId-' . $itemDataId . ', $itemDataAmount-' . $itemDataAmount . ', $itemDataPrice-' . $itemDataPrice . ', $itemDataTotalOriginalPrice-' . $itemDataTotalOriginalPrice . ', $itemDataTotalPrice-' . $itemDataTotalPrice;
                                    break;
                                } else {
                                    // search item
                                    // get pageId by 1C id
                                    $pageId = self::getPageIdBy1CId($itemDataId);
                                    
                                    $resultInfo[$arr_key]['id_1c2'] = $itemDataId;
                                    
                                    $page = $hierarchy->getElement($pageId);
                                    if ($page instanceof umiHierarchyElement) {
                                        $resultInfo[$arr_key]['cart_item_page_id2'] = $pageId;
                                        $resultInfo[$arr_key]['name2'] = $page->name;
                                        
                                        if (isset($cartItemsArr[$key])) {
                                            // change cart item
                                            $itemId = $cartItemsArr[$key];
                                            $orderItem = orderItem::get($itemId);
                                            //$resultInfo[$arr_key]['method'] = 'Change';
                                            
                                            $resultInfo[$arr_key]['cart_item_id2'] = $itemId;
                                            
                                            // reset item price
                                            $orderItemObject = $umiObjectsCollection->getObject($itemId);
                                            if ($orderItemObject instanceof iUmiObject) {
                                                $orderItemObject->item_price = $itemDataPrice;
                                                $orderItemObject->item_total_original_price = $itemDataTotalOriginalPrice;
                                                $orderItemObject->item_discount_id = 0;
                                                $orderItemObject->item_discount_value = 0;
                                                $orderItemObject->setValue(SiteEmarketOrderItemModel::field_ormco_coupon_id, $couponId);
                                                $orderItemObject->setValue(SiteEmarketOrderItemModel::field_ormco_coupon_code, $couponCode);
                                                $orderItemObject->commit();
                                                
                                                $orderItemNew = orderItem::get($itemId); // попытка переинициализировать цену товара
                                            }
                                        }
                                    } else {
                                        $resultInfo[$arr_key]['NoPage'] = 'Not found page with pageId = ' . $pageId . ' related with 1C id = "' . $itemDataId . '"';
                                    }
                                }
                            }
                            
                            $orderIns_change = order::get($orderId, true);
                            $orderIns_change->refresh();
                        }
                    }
                    
                    // добавление\изменение юр лица ($paymentId = 13911)
                    if ($paymentId == 13911) {
                        // проверить по email совпадает ли пользователь в заказе и в 1С
                        /* $customer_email = (isset($orderData["Покупатель"]['email'])) ? $orderData["Покупатель"]['email'] : false;
                          $sel = new selector('objects');
                          $sel->types('object-type')->name('users', 'user');
                          $sel->where('e-mail')->equals($customer_email);
                          $sel->limit(0, 1); */
                        $customerId = $order->customer_id;
                        $customer = $umiObjectsCollection->getObject($customerId);
                        
                        if ($customer) {
                            //if($customer = $sel->first) {
                            //if($order->customer_id != $customer->id){
                            //	$resultInfo['legal'][]='Site customre email not equal 1C customer email';
                            //}else{
                            // проверка наличия данных о юр лице
                            if (isset($orderData["Покупатель"]["ЮрЛицо"])) {
                                $urLicoData = $orderData["Покупатель"]["ЮрЛицо"];
                                
                                $fio = $customer->lname . ' ' . $customer->fname . ' ' . $customer->father_name;
                                
                                $inn = (isset($urLicoData['ИНН'])) ? $urLicoData['ИНН'] : false;
                                $account = (isset($urLicoData['РасчетныйСчет'])) ? $urLicoData['РасчетныйСчет'] : false;
                                $bank = (isset($urLicoData['НаименованиеБанка'])) ? $urLicoData['НаименованиеБанка'] : false;
                                $bank_account = (isset($urLicoData['КорСчет'])) ? $urLicoData['КорСчет'] : false;
                                $bik = (isset($urLicoData['БИК'])) ? $urLicoData['БИК'] : false;
                                $ogrn = (isset($urLicoData['ОГРН'])) ? $urLicoData['ОГРН'] : false;
                                $kpp = (isset($urLicoData['КПП'])) ? $urLicoData['КПП'] : false;
                                $legal_address = (isset($urLicoData['ЮридическийАдрес'])) ? $urLicoData['ЮридическийАдрес'] : false;
                                $defacto_address = (isset($urLicoData['ФактическийАдрес'])) ? $urLicoData['ФактическийАдрес'] : false;
                                $post_address = (isset($urLicoData['ПочтовыйАдрес'])) ? $urLicoData['ПочтовыйАдрес'] : false;
                                $name = (isset($urLicoData['НаименованиеОрганизации'])) ? $urLicoData['НаименованиеОрганизации'] : false;
                                $phone_number = (isset($urLicoData['КонтактныйТелефон']) && $urLicoData['КонтактныйТелефон'] != '') ? $urLicoData['КонтактныйТелефон'] : '-';
                                $contact_person = (isset($urLicoData['КонтактноеЛицо'])) ? $urLicoData['КонтактноеЛицо'] : $fio;
                                
                                if (
                                    !$inn ||
                                    !$account ||
                                    !$bank ||
                                    !$bank_account ||
                                    !$bik ||
                                    !$ogrn ||
                                    !$kpp ||
                                    !$legal_address ||
                                    !$defacto_address ||
                                    !$post_address ||
                                    !$name) {
                                    
                                    $resultInfo['legal']['NoEnoughParams'] = 'Not enough params $inn: ' . $inn . ', $account: ' . $account . ', $bank: ' . $bank . ', $bank_account: ' . $bank_account . ', $bik: ' . $bik . ', $ogrn: ' . $ogrn . ', $kpp: ' . $kpp . ', $legal_address: ' . $legal_address . ', $defacto_address: ' . $defacto_address . ', $post_address: ' . $post_address . ', $name: ' . $name;
                                } else {
                                    $collection = umiObjectsCollection::getInstance();
                                    $types = umiObjectTypesCollection::getInstance();
                                    
                                    $persons = $customer->legal_persons;
                                    $isPersonNew = true;
                                    if (is_array($persons)) {
                                        foreach ($persons as $personId) {
                                            $person = $collection->getObject($personId);
                                            // редактируем юр лицо
                                            if ($inn == $person->inn) {
                                                $person->account = $account;
                                                $person->bank = $bank;
                                                $person->bank_account = $bank_account;
                                                $person->bik = $bik;
                                                $person->ogrn = $ogrn;
                                                $person->kpp = $kpp;
                                                $person->legal_address = $legal_address;
                                                $person->defacto_address = $defacto_address;
                                                $person->post_address = $post_address;
                                                $person->name = $name;
                                                $person->phone_number = $phone_number;
                                                $person->contact_person = $contact_person;
                                                $person->commit();
                                                
                                                $resultInfo['legal']['method'] = 'change';
                                                $resultInfo['legal']['inn'] = $inn;
                                                $resultInfo['legal']['id'] = $personId;
                                                
                                                $order->legal_person = $personId;
                                                $isPersonNew = false;
                                                break;
                                            }
                                        }
                                    }
                                    
                                    if ($isPersonNew) {
                                        $typeId = $types->getBaseType("emarket", "legal_person");
                                        
                                        $personId = $collection->addObject("", $typeId);
                                        $customer->legal_persons = array_merge($customer->legal_persons, array($personId));
                                        
                                        $person = $collection->getObject($personId);
                                        $person->setName($name);
                                        $person->inn = $inn;
                                        $person->account = $account;
                                        $person->bank = $bank;
                                        $person->bank_account = $bank_account;
                                        $person->bik = $bik;
                                        $person->ogrn = $ogrn;
                                        $person->kpp = $kpp;
                                        $person->legal_address = $legal_address;
                                        $person->defacto_address = $defacto_address;
                                        $person->post_address = $post_address;
                                        $person->name = $name;
                                        $person->phone_number = $phone_number;
                                        $person->contact_person = $contact_person;
                                        $person->commit();
                                        
                                        $resultInfo['legal']['method'] = 'add';
                                        $resultInfo['legal']['inn'] = $inn;
                                        $resultInfo['legal']['id'] = $personId;
                                        
                                        $order->legal_person = $personId;
                                    }
                                }
                            }
                            //}
                        }
                    }
                    
                    if (!(sizeof($resultError) > 0)) {
                        $order->commit();
                    }
                    
                    try {
                        $this->change_order_status_from_1c_status($orderId, isset($orderData["СтатусЗаказа1С"]) ? $orderData["СтатусЗаказа1С"] : '', isset($orderData["СтатусОплатыЗаказа1С"]) ? $orderData["СтатусОплатыЗаказа1С"] : '');
                    } catch (Exception $ex) {
                    }
                    
                    // стоимость доставки
                    if (isset($orderData["СтатусЗаказа"]) && $orderData["СтатусЗаказа"] == "К оплате") {
                        $this->applyOrder($orderId);
                    }
                } else {
                    $resultError['NotFoundOrder'] = 'Not found order ID = ' . $orderId;
                }
            }
        } else {
            $resultError['JsonEr'] = 'Json format error';
        }
        
        if (sizeof($resultError) > 0) {
            $result = array('status' => 'error', 'msg' => $resultError);
        } else {
            $result = array('status' => 'successfull', 'msg' => $resultInfo);
        }
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer -> charset('utf-8');
        $buffer -> contentType('application/json');
        $buffer -> push(json_encode($result));
        $buffer -> end();
    }
    
    // получение page Id по ИД из 1С
    public function getPageIdBy1CId($oldId = NULL) {
        $relation = umiImportRelations::getInstance();
        $sourceId = $relation -> getSourceId('commerceML2');
        $pageId = $relation -> getNewIdRelation($sourceId, $oldId);
        
        if ($pageId) {
            return $pageId;
        }
        return 0;
    }
    
    /*импорт скидко пользователей по uid из 1С*/
    public function order1csynSend() {
        $url = "http://ormco-test2.dpromo.su/udata/emarket/order1csyn/";
        //$send_data = array('ww'=>'123');
        //$content = json_encode($send_data);
        $content = '{"Заказы": { "Заказ": { "СтатусЗаказа": "К оплате", "ИД": "2380946", "Оплата": { "ИД": "13911", "Название": "Счет для юр.лиц", "Статус": "-" }, "Доставка": { "ИД": "165022", "Название": "Доставка по Москве и МО", "Стоимость": "300.00" }, "Товары": {"Товар": [ { "ИД": "6099d402-546b-11e9-9105-00505681a99c", "Количество": 1.00, "СтоимостьБезСкидки": 10.01, "Стоимость": 10.01, "ИтогоБезСкидки": 10.01, "Итого": 10.01 }, { "ИД": "7e1bca4a-5514-11e9-9105-00505681a99c", "Количество": 1.00, "СтоимостьБезСкидки": 10.00, "Стоимость": 10.00, "ИтогоБезСкидки": 10.00, "Итого": 10.00 } ] }, "Покупатель": { "email": "stomat.p@mail.ru", "lname": "Чернова", "fname": "Екатерина", "father_name": "Петровна", "ЮрЛицо": { "ИНН": "7720310954", "РасчетныйСчет": "40702810400005000768", "НаименованиеБанка": "МОСКОВСКИЙ ФИЛИАЛ ПАО \"СОВКОМБАНК\"", "КорСчет": "30101810945250000967", "БИК": "044525967", "ОГРН": "1157746699395", "КПП": "772001001", "ЮридическийАдрес": "117148, Москва г, Маршала Савицкого ул, дом № 22, корпус 2", "ФактическийАдрес": "117623, Москва г, Маршала Савицкого ул, дом № 22, корпус 2", "ПочтовыйАдрес": "117148, Москва г, Маршала Савицкого ул, дом № 22, корпус 2", "НаименованиеОрганизации": "\"АЛЬФАДЕНТ\" ООО г. Москва", "КонтактноеЛицо": "Чернова Екатерина Петровна", "КонтактныйТелефон": "+7 (999) 199-39-05" } } } } }';
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        
        $json_response = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //if ( $status != 201 ) {
        //    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        //}
        curl_close($curl);
        
        $response = json_decode($json_response, true);
        var_dump($json_response);
        var_dump($status);
        var_dump($response);
        exit('aaaa');
    }
    
    /*подставление цен из 1С в товары при пересчете цен товаров(событии refresh) */
    public function set1Cprices(iUmiEventPoint $event) {
        //return 'switch off';
        if ($event->getMode() == 'after') {
            $umiObjectsCollection = umiObjectsCollection::getInstance();
            $orderItem = $event->getRef('orderItem');
            
            if (!$orderItem instanceof umiObject){
                return;
            }
            
            $orderItemId = $orderItem->getId();
            if ($orderItemId == 1201889) {
                //exit('ffff');
                $orderItemObject = $umiObjectsCollection->getObject($orderItemId);
                //$status = 'notfound';
                if ($orderItemObject instanceof umiObject) {
                    //$orderItemObject->item_price = $itemDataPrice;
                    //$orderItemObject->item_total_original_price = $itemDataTotalOriginalPrice;
                    //$orderItemObject->commit();
                }
            }
        }
    }
    
    /*проверка подставление цен из 1С в товары при пересчете цен товаров(событии refresh) */
    public function set1CpricesTest() {
        //return 'switch off';
        $umiObjectsCollection = umiObjectsCollection::getInstance();
        $orderItemId = 1201889;
        //exit('ffff');
        $orderItemObject = $umiObjectsCollection -> getObject($orderItemId);
        //$status = 'notfound';
        if ($orderItemObject instanceof umiObject) {
            // взять объект "Наименование в корзине"
            // поменять цену
            // проверить, что
            //$orderItemObject->item_price = $itemDataPrice;
            //$orderItemObject->item_total_original_price = $itemDataTotalOriginalPrice;
            //$orderItemObject->commit();
        }
    }
}