<?php

class emarket_analytics_tags extends def_module
{
    /* Отправляем в ga список товаров в корзине, если находимся на странице корзины */
    public function addCheckoutStep1()
    {
        $hierarchy = umiHierarchy::getInstance();
        $cmsController = cmsController::getInstance();
        
        if (!($cmsController->getCurrentModule() == 'emarket' && $cmsController->getCurrentMethod() == 'cart')) {
            return '';
        }
        $moduleCatalog = $cmsController->getModule('catalog');
        
        $order = $this->getBasketOrder(false);
        $pages_info = '';
        foreach ($order->getItems() as $orderItem) {
            $orderItemElementId = $orderItem->getItemElement()->id;
            $orderItemElement = $hierarchy->getElement($orderItemElementId);
            
            $dataCategory = $moduleCatalog->getNavibarCategory($orderItemElementId);
            $dataArtikul = $orderItemElement->artikul;
            $dataName = $orderItem->getName();
            $dataPrice = $orderItem->getTotalActualPrice();
            $dataQty = $orderItem->getAmount();
            $dataPricePerOne = round($dataPrice / $dataQty * 100) / 100;
            
            $pages_info .= <<<END
ga('ec:addProduct', {
    'id': '{$dataArtikul}',
    'name': '{$dataName}',
    'category': '{$dataCategory}',
    'brand': '',
    'price': '{$dataPrice}',
    'price_per_one': '{$dataPricePerOne}',
    'quantity': {$dataQty}
});
END;
        }
        
        if (strlen($pages_info) > 0) {
            $pages_info .= <<<END
ga('ec:setAction','checkout', {
    'step': 1,
});
END;
        }
        return (string)$pages_info;
    }
    
    /* Отправляем в ga список товаров в корзине, если находимся на странице выбора способа доставки */
    public function addCheckoutStep2()
    {
        return '';
        
        $hierarchy = umiHierarchy::getInstance();
        $cmsController = cmsController::getInstance();
        
        if (!($cmsController->getCurrentModule() == 'emarket' && $cmsController->getCurrentMethod() == 'purchasing_one_step')) {
            return '';
        }
        $moduleCatalog = $cmsController->getModule('catalog');
        
        $order = $this->getBasketOrder(false);
        $pages_info = 'jQuery(document).ready(function(){';
        $objects = 'var order_items = [';
        foreach ($order->getItems() as $orderItem) {
            $orderItemElementId = $orderItem->getItemElement()->id;
            $orderItemElement = $hierarchy->getElement($orderItemElementId);
            
            $dataCategory = $moduleCatalog->getNavibarCategory($orderItemElementId);
            $dataArtikul = $orderItemElement->artikul;
            $dataName = $orderItem->getName();
            $dataPrice = $orderItem->getTotalActualPrice();
            $dataQty = $orderItem->getAmount();
            $dataPricePerOne = round($dataPrice / $dataQty * 100) / 100;
            
            $pages_info .= <<<END
ga('ec:addProduct', {
    'id': '{$dataArtikul}',
    'name': '{$dataName}',
    'category': '{$dataCategory}',
    'brand': '',
    'price': '{$dataPrice}',
    'price_per_one': '{$dataPricePerOne}',
    'quantity': {$dataQty}
});
END;
            $objects .= <<<END
{
    'id': '{$dataArtikul}',
    'name': '{$dataName}',
    'category': '{$dataCategory}',
    'brand': '',
    'price': '{$dataPrice}',
    'price_per_one': '{$dataPricePerOne}',
    'quantity': {$dataQty}
},
END;
        }
        
        if (strlen($pages_info) > 0) {
            $pages_info .= <<<END
ga('ec:setAction','checkout', {
    'step': 2,
});
ga('send', 'pageview', '/virtual/deliverymethod');
});
END;
        }
        $objects .= "];";
        
        $orderId = $order->id;
        $order_obj = order::get($orderId);
        $actualPrice = $order_obj->getActualPrice();
        $common_params = <<<END
var order_id = {$orderId},
    order_revenue = {$actualPrice},
    order_shipping = 0;
END;
        
        return $common_params . "\n" . $objects . "\n" . $pages_info;
    }
    
    /**
     * Отправка события ecommercePurchase через бек при запросе по AJAX со страницы успешного оформления заказа
     * /emarket/premoderate/[ID ЗАКАЗА]/
     */
    public function ga_send_ecommerce_purchase()
    {
        // Получение служебных данных по вызвавшей скрипт странице
        $url = getRequest('url');
        $domain = getRequest('domain');
        $title = getRequest('title');
        
        // Получение данных по заказу
        $hierarhy = umiHierarchy::getInstance();
        $order_id = getRequest('order_id');
        if (!$order_id) {
            throw new publicException('Не передан id заказа');
        }
        
        $emarket = cmsController::getInstance()->getModule('emarket');
        if (!$emarket instanceof emarket) {
            throw new publicException('Не найден модуль emarket');
        }
        
        if (!class_exists('order')) {
            throw new publicException('Не найден класс order');
        }
        
        $order = order::get($order_id);
        if (!$order instanceof order) {
            throw new publicException('Не найден заказ ' . $order_id);
        }
        
        if ($order->isEmpty()) {
            return null;
        }
        
        $number = $order->getValue('number');
        if (!$number) {
            return null;
        }
        
        $settingsPage = SiteContentPageSettingsModel::getPage();
        if (!$settingsPage instanceof umiHierarchyElement) {
            return null;
        }
        
        $gtmId = trim($settingsPage->getValue(SiteContentPageSettingsModel::field_counters_gtm_id));
        if (!$gtmId) {
            return null;
        }
        
        $order_params = array();
        $count = 1;
        foreach ($order->getItems() as $item) {
            if (!$item instanceof optionedOrderItem) {
                continue;
            }
            
            $element = $item->getItemElement(true);
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }
            
            $order_params['pr' . $count . 'id'] = $element->getId();
            $order_params['pr' . $count . 'nm'] = $element->getName();
            $order_params['pr' . $count . 'pr'] = $item->getItemPrice();
            $order_params['pr' . $count . 'qt'] = $item->getAmount();
            $order_params['pr' . $count . 'ca'] = '';
            $order_params['pr' . $count . 'br'] = '';
            
            $parent = $hierarhy->getElement($element->getParentId());
            if ($parent instanceof umiHierarchyElement) {
                $order_params['pr' . $count . 'ca'] = $parent->getName();
            }
            
            $count++;
        }
        $order_params['ti'] = $number;
        $order_params['tr'] = $order->getActualPrice();
        $order_params['ts'] = $order->getDeliveryPrice();
        $order_params['tcc'] = $order->getValue('order_ormco_coupon_code');
        
        // Итоговые параметры
        $params = [
            'v' => '1',
            't' => 'event',
            'cid' => $order->getValue(SiteEmarketOrderModel::field_ga_cid),
            'tid' => $gtmId,
            'ni' => '1',
            'cu' => 'RUB',
            'dl' => $url,
            'ul' => 'ru',
            'de' => 'UTF-8',
            'dt' => $title,
            'dh' => $domain,
            'ea' => 'ecommercePurchase',
            'pa' => 'purchase',
            'uip' => $_SERVER['REMOTE_ADDR'],
            'wget' => getRequest('navigator'),
            'z' => rand(0, 999999999),

//            'pr1id' => '11760',
//            'pr1nm' => 'Крючки орт. для установки на брекет Insignia 180*',
//            'pr1pr' => '180',
//            'pr1qt' => '1',
//            'pr1ca' => 'Крючки, стопоры',
//            'pr2id' => '8686',
//            'pr2nm' => 'Адгезив Enlight (шприц)',
//            'pr2pr' => '3950',
//            'pr2qt' => '1',
//            'pr2ca' => 'Адгезивы',
//            'pr3id' => '8681',
//            'pr3nm' => 'Адгезив Blugloo (шприц)',
//            'pr3pr' => '5860',
//            'pr3qt' => '1',
//            'pr3ca' => 'Адгезивы',
//            'pr4id' => '8695',
//            'pr4nm' => 'Адгезив Grengloo (шприц)',
//            'pr4pr' => '5860',
//            'pr4qt' => '5',
//            'pr4ca' => 'Адгезивы',
//            'tr' => '39290',

//            'dr' => 'https://tagassistant.google.com/',
//            '_v' => 'j92d',
//            'a' => '1610784083',
//            '_s' => '1',
//            'sd' => '24-bit',
//            'sr' => '1920x1080',
//            'vp' => '758x937',
//            'je' => '0',
//            '_u' => 'SKCAgEAL~',
//            'jid' => '984214244',
//            'gjid' => '592574196',
//            '_gid' => '1959197747.1626602689',
//            'gtm' => '2wg7e0M8L987N',
//            'cd1' => '1626690884935.tmz5zscd',
//            'cd2' => '1765107134.1623307426',
//            'cd3' => date('c'),
//            'cd5' => 'https://tagassistant.google.com/',
        ];
        
        $params = array_merge($params, $order_params);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://www.google-analytics.com/collect');
//        curl_setopt($curl, CURLOPT_URL, 'https://www.google-analytics.com/debug/collect');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        $ret = curl_exec($curl);
        print_r($ret);
        curl_close($curl);
        die();
    }
}