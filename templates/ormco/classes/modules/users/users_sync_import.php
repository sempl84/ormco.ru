<?php
class users_sync_import extends def_module {

	const url = 'https://orthodontia.ru/';
    //const url = 'http://ortho-test2.dpromo.su/';
    const hash = '3gdtrfxcvfh';

    // есть ли такой пользователь на текущем сайте
    public function isUserExist($email = NULL) {
        if(!$email) $email = getRequest('param0');
        if(!$email) return 'noEmail';

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('e-mail')->equals($email);
        $sel->limit(0, 1);

        return ($user = $sel->first) ? $user : false;
    }

    // Создаем пользователя при регистрации на другом сайте
    public function importNewUser($email = NULL, $password = NULL) {
        $email = $_REQUEST['e-mail'];
        $password = $_REQUEST['ps'];
        $active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 0;
        $commonUid = isset($_REQUEST['common_uid']) ? $_REQUEST['common_uid'] : '';
        //return ;

        if(!$email || !$password) {
            return 'noRequiredData';
        }

        if($this->isUserExist($email)){
            return 'userExist';
        }

        //check hash
        $allowfields = array(
            'ps',
            'active',
            'common_uid',
            'referer',
            'lname',
            'fname',
            'father_name',
            'e-mail',
            'phone',
            'phone_valid',
            'phone_valide_code',
            'phone_office',
            'city',
            'bd',
            'company',
            'agree',
            'country',
            'region',
            'prof_status',
        );
        $post = $_REQUEST;
        foreach($post as $fieldName => $fieldValue){
            if(!in_array($fieldName, $allowfields)){
                unset($post[$fieldName]);
            }
        }

        if($_REQUEST['hash'] !==  hash('sha256', implode('|',$post) . self::hash)){
            return 'fakeData';
        }else{
            // rightData
            //return 'ready to creace User'; //TODO test check user exist

            $group_id = regedit::getInstance() -> getVal("//modules/users/def_group");
            $objectTypes = umiObjectTypesCollection::getInstance();
            $objectTypeId = $objectTypes -> getBaseType("users", "user");

            //Creating user...
            $object_id = umiObjectsCollection::getInstance() -> addObject($email, $objectTypeId);
            $object = umiObjectsCollection::getInstance() -> getObject($object_id);

            $object -> setValue("login", $email);
            $object -> setValue("password", $password);
            $object -> setValue("is_activated", $active);
            $object -> setValue("common_uid", $commonUid);
            $object -> setValue("register_date", time());
            $object -> setValue("groups", Array($group_id));
            //$object -> setValue("sync_new_2", 1);
            foreach($_REQUEST as $fieldName => $fieldValue){
                $object -> setValue($fieldName, $fieldValue);
            }
            $object->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($object->getValue(SiteUsersUserModel::field_phone)));
            $object -> commit();

            return $object_id;
        }
    }

    // Изменяем пользователя при изменениях в ЛК на другом сайте
    public function importModifyUser($email = NULL, $password = NULL) {
        $email = $_REQUEST['e-mail'];
        $password = $_REQUEST['ps'];
        $active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 0;
        $commonUid = isset($_REQUEST['common_uid']) ? $_REQUEST['common_uid'] : '';
        //return ;

        if(!$email || !$password) {
            return 'noRequiredData';
        }

        $user = $this->isUserExist($email);
        if(!($user)){
            // создаем нового пользователя
            return $this->importNewUser();
        }

        //check hash
        $allowfields = array(
            'ps',
            'active',
            'common_uid' ,
            'referer',
            'lname',
            'fname',
            'father_name',
            'e-mail',
            'phone',
            'phone_valid',
            'phone_valide_code',
            'phone_office',
            'city',
            'bd',
            'company',
            'agree',
            'country',
            'region',
            'prof_status',
        );
        $post = $_REQUEST;
        foreach($post as $fieldName => $fieldValue){
            if(!in_array($fieldName, $allowfields)){
                unset($post[$fieldName]);
            }
        }

        if($_REQUEST['hash'] !==  hash('sha256', implode('|',$post) . self::hash)){
            return 'fakeData';
        }else{
            // rightData
            //update user...
            $user_id = $user->id;

            $user -> setValue("login", $email);
            $user -> setValue("password", $password);
            $user -> setValue("is_activated", $active);
            $user -> setValue("common_uid", $commonUid);
            //$object -> setValue("sync_new_2", 1);
            foreach($_REQUEST as $fieldName => $fieldValue){
                //$old_value =  $user -> getValue($fieldName); // for first sync
                //if(!$old_value || $old_value == ''){
                    $user -> setValue($fieldName, $fieldValue);
                //}

            }
            $user -> commit();

            return $user_id;
        }
    }

    // Вывести CommonUID по user ID, который использовался раньше
    public function getCommonUIDbyOldID($userId = NULL) {
        if(!$userId) $userId = getRequest('param0');
        if(!$userId) {
            $result = array('status'=>'error', 'msg'=>'noOldId');
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyOldID - start \n " . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

            return $result;
        }

        $user = umiObjectsCollection::getInstance() -> getObject($userId);
        if(!$user) {
            $result = array('status'=>'error', 'msg'=>'noUser');
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyOldID - start \n " . $userId . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

            return $result;
        }
        $commonUID = $user->common_uid;
        if($commonUID == '') {
            $result = array('status'=>'error', 'msg'=>'noCommonUID');
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyOldID - start \n " . $userId . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

            return $result;
        }

        // отметка что пользователь есть в 1С
        $user->common_uid_1c_sync = 1;
        $user->commit();

        $result = array('status'=>'successful', 'commonUID'=>$commonUID);
        file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyOldID - start \n " . $userId . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        $this-> exportCommonUid1cSync($user);
        return $result;
    }

    // Вывести CommonUID по user email
    public function getCommonUIDbyEmail($email = NULL) {
        if(!$email) $email = getRequest('param0');
        if(!$email) {
            $result = array('status'=>'error', 'msg'=>'noEmail');
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyEmail - start \n " . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

            return $result;
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        //$sel->where('common_uid')->isnull(false);
        $sel->where('e-mail')->equals($email);
        $sel->limit(0,1);


        $commonUID = false;
        if($userTmp = $sel->first){
            $commonUID = $userTmp->common_uid;
            if(!$commonUID || $commonUID==''){
                $result = array('status'=>'error', 'msg'=>'noCommonUID');
                file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyEmail - start \n " . $email . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

                return $result;
            }

            // отметка что пользователь есть в 1С
            $userTmp->common_uid_1c_sync = 1;
            $userTmp->commit();

            $result = array('status'=>'successful', 'commonUID'=>$commonUID);
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyEmail - start \n " . $email . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
            $this-> exportCommonUid1cSync($userTmp);

            return $result;
        }else{
            $result = array('status'=>'error', 'msg'=>'noUser');
            file_put_contents(CURRENT_WORKING_DIR . '/us_1c_log.txt', "\n\n getCommonUIDbyEmail - start \n " . $email . "\n" . print_r($result, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

            return $result;
        }
    }

    // синхронизация отметки о существовании такого пользователя и commonUid в 1С
    public function importCommonUid1cSync($commonUid = NULL) {
        if(!$commonUid) $commonUid = getRequest('param0');
        if(!$commonUid) $commonUid = getRequest('common_uid');

        if($commonUid){
            $sel = new selector('objects');
            $sel->types('object-type')->name('users', 'user');
            $sel->where('common_uid')->equals($commonUid);
            $sel->limit(0, 1);

            if($user = $sel->first){
                $user->common_uid_1c_sync = 1;
                $user->commit();
                return 'successful';
            }
        }

        return ;
    }
}
