<?php
class users_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/phone_validator.php", (dirname(__FILE__)));
        $self->__implement("users_phone_validator");

        $self->__loadLib("/users_sync.php", (dirname(__FILE__)));
        $self->__implement("users_sync");

        $self->__loadLib("/users_sync_import.php", (dirname(__FILE__)));
        $self->__implement("users_sync_import");

//        $self->__loadLib("/users_sync_import.php", (dirname(__FILE__)));
//        $self->__implement("users_sync_import");
    }

    public function login_do_pre() {
        $email = $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'];
        //$_REQUEST['from_page'] = getServer('HTTP_REFERER') . '?sa='.$email;
        return $this->login_do();
    }

    public function checkauth() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'];

        $login = getRequest('login');
        $password = getRequest('password');
        $from_page = getRequest('from_page');

        $permissions = permissionsCollection::getInstance();
        $cmsController = $this->cmsController;

        $user = $permissions->checkLogin($login, $password);

        if ($user instanceof iUmiObject) {
            return array('status' => 'successful');
        } else {
            return array(
                'status' => 'error',
                'field' => 'email2',
                'result' => 'Неверный логин или пароль'
            );
        }
    }

    public function checkreg() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['data']['new']['e-mail'];

        $without_act = (bool) regedit::getInstance()->getVal("//modules/users/without_act");

        // проверка логина
        $login = getRequest('login');
        $userId = false;
        $public = true;
        $valid = false;
        //Filters
        $login = trim($login);
        $valid = $login ? $login : (bool) $login;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $login) && $login) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if ($public) {
            $minLength = 3;
            if (mb_strlen($login, 'utf-8') > 40) {
                return array(
                    'status' => 'error',
                    'field' => 'e-mail',
                    'result' => 'Слишком длинный e-mail'
                );
            }
        }
        if (mb_strlen($login, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Слишком короткий e-mail'
            );
        }
        if (!$this->checkIsUniqueLogin($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }
        if (!umiMail::checkEmail($login)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if (!$this->checkIsUniqueEmail($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }

        // проверка пароля
        $password = getRequest('password');
        $passwordConfirmation = getRequest('password_confirm');
        $login = getRequest('login');

        $valid = false;
        //Filters
        $password = trim($password);
        $valid = $password ? $password : (bool) $password;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $password) && $password) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Неверный формат пароля'
            );
        }
        if ($login && ($password == trim($login))) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Пароль совпадает с e-mail'
            );
        }
        if ($public) {
            $minLength = 3;
            if (!is_null($passwordConfirmation)) {
                if ($password != $passwordConfirmation) {
                    return array(
                        'status' => 'error',
                        'field' => 'password',
                        'result' => 'Ошибка ввода подтверждения пароля'
                    );
                }
            }
        }
        if (mb_strlen($password, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Слишком короткий пароль'
            );
        }

        if (!umiCaptcha::checkCaptcha()) {
            return array(
                'status' => 'error',
                'result' => 'Неверная капча'
            );
        }
        return array('status' => 'successful');
    }

    public function registrate_do_pre() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['data']['new']['e-mail'];
		if (isset($_REQUEST['data']['new']['phone1']) && !empty($_REQUEST['data']['new']['phone1'])) {
			$this->redirect($this->pre_lang . "/users/registrate_done/");
		}

        $without_act = (bool) regedit::getInstance()->getVal("//modules/users/without_act");

        $login = $this->validateLogin(getRequest('login'), false, true);
        $password = $this->validatePassword(getRequest('password'), getRequest('password_confirm'), getRequest('login'), true);
        $email = $this->validateEmail(getRequest('email'), false, !$without_act);

        if (!umiCaptcha::checkCaptcha()) {
            $this->errorAddErrors('errors_wrong_captcha');
        }
        if (!umiCaptcha::checkCaptcha() || !$login || !$password || !$email) {
            $this->errorSetErrorPage('/users/registrate/');
            $this->errorThrow('public');
        }
        return $this->registrate_do();
    }

    public function settings_do_pre() {
        $userId = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($userId);
        if ($user instanceof iUmiObject) {
            $email = $_REQUEST['data'][$userId]['e-mail'];

            if($user->login != $email){
				$email = $this->validateLoginNew($email, $userId, false);
                $_REQUEST['login'] = $_REQUEST['data'][$userId]['login'] = $_REQUEST['data'][$userId]['email'] = $_REQUEST['data'][$userId]['e-mail'] = $user->login;

                $user->setValue("email_dlya_izmeneniya", $email);
                $user->setValue("pole_koda_aktivaciii_novogo_email", sha1(time(true)));
                $user->setValue("data_popytki_izmeneniya_email", date('c'));
                $user->commit();

                $this->send_confirm_email_letter();

//                $_REQUEST['login'] = $_REQUEST['data'][$userId]['login'] = $_REQUEST['data'][$userId]['email'] = $email;
//
//                $oEventPoint = new umiEventPoint("users_settings_do");
//                $oEventPoint->setMode("before");
//                $oEventPoint->setParam("user_id", $userId);
//                $this->setEventPoint($oEventPoint);
//
//                $user->setValue("login", $email);
//                $user->commit();
//
//                $oEventPoint->setMode("after");
//                $this->setEventPoint($oEventPoint);
            }
        }

        return $this->settings_do();
    }

    public function validateLoginNew($login, $userId = false, $public = false) {
        $valid = false;
        //Filters
        $login = trim($login);
        $valid = $login ? $login : (bool) $login;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $login) && $login) {
            $this->errorAddErrors('error-login-wrong-format');
            $valid = false;
        }
        if ($public) {
            $minLength = 3;
            if (mb_strlen($login, 'utf-8') > 40) {
                $this->errorNewMessage("%error-login-long%");
                $this->errorPanic();
            }
        }
        if (mb_strlen($login, 'utf-8') < $minLength) {
            $this->errorNewMessage("%error-login-short%");
            $this->errorPanic();
        }
        if (!$this->checkIsUniqueLogin($login, $userId)) {
            $this->errorNewMessage("%error-email-exists%");
            $this->errorPanic();
        }
        if (!umiMail::checkEmail($login)) {
            $this->errorNewMessage('%error-email-wrong-format%');
            $this->errorPanic();
        }
        if (!$this->checkIsUniqueEmail($login, $userId)) {
            $this->errorNewMessage('%error-email-exists%');
            $this->errorPanic();
        }
        return $valid;
    }

    public static function getRandomPassword($length = 12) {
        $length = 8;
        // if(function_exists('openssl_random_pseudo_bytes')) {
        // $password = base64_encode(openssl_random_pseudo_bytes($length, $strong));
        // if($strong == TRUE)
        // return substr($password, 0, $length);
        // }
        $avLetters = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        $size = strlen($avLetters);

        $npass = "";
        for ($i = 0; $i < $length; $i++) {
            $c = rand(0, $size - 1);
            $npass .= $avLetters[$c];
        }
        return $npass;
    }

    public function update_password($activate_code = false) {
        $object = false;
        $block_arr = Array();
        if (!$activate_code) {
            $activate_code = (string) getRequest('param0');
            $activate_code = trim($activate_code);
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('activate_code')->equals($activate_code);
        $sel->limit(0, 1);
        if ($sel->first) {
            $object = $sel->first;
        }

        $block_arr['attribute:activate_code'] = $activate_code;
        $block_arr['attribute:header'] = 'Восстановление пароля';
        if ($object instanceof umiObject) {
            $block_arr['attribute:status'] = "success";
            $block_arr['login'] = $object->name;
            $block_arr['id'] = $object->id;
        } else {
            $block_arr['attribute:status'] = "fail";
        }

        return $block_arr;
    }

    public function set_new_password($activate_code = false, $template = "default") {
        $object = false;
        $user_id = false;
        $block_arr = Array();

        list(
                $template_restore_failed_block, $template_restore_ok_block, $template_mail_password, $template_mail_password_subject
                ) = def_module::loadTemplatesForMail("users/forget/" . $template, "restore_failed_block", "restore_ok_block", "mail_password", "mail_password_subject"
        );

        if (!$activate_code) {
            $activate_code = (string) getRequest('param0');
            $activate_code = trim($activate_code);
        }

        $password = (string) getRequest('new_password');
        $password = trim($password);

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('activate_code')->equals($activate_code);
        $sel->limit(0, 1);

        if ($sel->first) {
            $object = $sel->first;
            $user_id = $object->id;
        }

        $block_arr['attribute:header'] = 'Результат изменения пароля';

        if ($object instanceof umiObject && $activate_code && $password) {
//                $password = self::getRandomPassword();

            $login = $object->getValue("login");
            $email = $object->getValue("e-mail");
            $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

            $hashAlgorithm = UmiCms\Service::PasswordHashAlgorithm();
            $encodedPassword = $hashAlgorithm->hash($password);

            $oEventPoint = new umiEventPoint("users_restore_password_do");
            $oEventPoint->setMode("before");
            $oEventPoint->setParam("user_id", $object->id);
            $this->setEventPoint($oEventPoint);

            $object->setValue("password", $encodedPassword);
            $object->setValue("activate_code", "");
            $object->commit();

            $oEventPoint->setMode("after");
            $this->setEventPoint($oEventPoint);

            $email_from = regedit::getInstance()->getVal("//settings/email_from");
            $fio_from = regedit::getInstance()->getVal("//settings/fio_from");

            $mail_arr = Array();
            $mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
            $mail_arr['password'] = $password;
            $mail_arr['login'] = $login;

            $mail_subject = def_module::parseTemplateForMail($template_mail_password_subject, $mail_arr, false, $user_id);
            $mail_content = def_module::parseTemplateForMail($template_mail_password, $mail_arr, false, $user_id);

            $someMail = new umiMail();
            $someMail->addRecipient($email, $fio);
            $someMail->setFrom($email_from, $fio_from);
            $someMail->setSubject($mail_subject);
            $someMail->setContent($mail_content);
            $someMail->commit();
            $someMail->send();

            $block_arr['attribute:status'] = "success";
            $block_arr['login'] = $login;
            $block_arr['password'] = $password;
            $block_arr['restore_result'] = 'Ваш пароль успешно изменен.<br />';

            $eventPoint = new umiEventPoint('successfulPasswordRestoring');
            $eventPoint->setMode('after');
            $eventPoint->setParam('userId', $user_id);
            $eventPoint->call();

            return $result[$template] = def_module::parseTemplate($template_restore_ok_block, $block_arr, false, $user_id);
        } else {
            $block_arr['attribute:status'] = "fail";
            $block_arr['restore_result'] = 'В процессе изменения пароля произошла ошибка. Пожалуйста, обратитесь к администратору.';
            return $result[$template] = def_module::parseTemplate($template_restore_failed_block, $block_arr);
        }
    }


	public function get_user_discount_id($user_id){
		$collection = umiObjectsCollection::getInstance();
		$result = 'none';
		$modificator_id = false;

		$user = $collection->getObject($user_id);
		if($user instanceof umiObject){
			$modificators = new selector('objects');
			$modificators->types('object-type')->id(318); // 318 - модификатор скидки
			foreach ($modificators as $modificator){ // перебор, потом что так быстрее, чем строить странный и криво запрос
				$allowed_users = $modificator->getValue('users');
				if(is_array($allowed_users) && in_array($user_id, $allowed_users)){
					$modificator_id = $modificator->getId();
					break;
				}
			}
			if($modificator_id === false){
				return $result;
			}

			$discounts = new selector('objects');
			$discounts->types('object-type')->id(41); // 41 - тип данных скидка
			$discounts->where('discount_rules_id')->equals($modificator_id);
			foreach($discounts as $discount){
				return $discount->getId();
			}
		}
		return $result;
	}

	/**
	 * Отмена изменения e-mail адреса
	 * @param type $user_id
	 * @return type
	 */
	public function cancel_change_email() {
		$result = array("result" => "fail");
        $user_id = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if(!empty($user->getValue('email_dlya_izmeneniya'))){
				$user->setValue('email_dlya_izmeneniya', '');
				$user->setValue('pole_koda_aktivaciii_novogo_email', '');
				$user->setValue('data_popytki_izmeneniya_email', '');
				$user->commit();
			}
			$result["result"] = "ok";
		}

        $referer = getServer('HTTP_REFERER');
        $noRedirect = getRequest('no-redirect');

        if (!$noRedirect) {
            $this->redirect($referer);
        }

		return $result;
	}

	/**
	 * Отправка сообщения о смене контактного e-mail адреса
	 */
	public function send_confirm_email_letter() {
		$result = array("result" => "Ошибка");
        $user_id = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if(!empty($user->getValue('email_dlya_izmeneniya'))){
				$email_from = regedit::getInstance()->getVal("//settings/email_from");
				$fio_from = regedit::getInstance()->getVal("//settings/fio_from");

				list(
					$template_confirm_email_letter_content, $template_confirm_email_letter_subject
					) = def_module::loadTemplatesForMail("users/change/default", "confirm_email_letter_content", "confirm_email_letter_subject"
				);

				$mail_arr = Array();
				$mail_arr['domain'] = $_SERVER['HTTP_HOST'];
				$mail_arr['login'] = $user->getValue('login');
				$mail_arr['change_link'] = 'https://' . $_SERVER['HTTP_HOST'] . '/users/change_email_from_letter/' . $user_id . '/' . $user->getValue('pole_koda_aktivaciii_novogo_email');

				$mail_subject = def_module::parseTemplateForMail($template_confirm_email_letter_subject, $mail_arr, false, $user_id);
				$mail_content = def_module::parseTemplateForMail($template_confirm_email_letter_content, $mail_arr, false, $user_id);

	            $email = $user->getValue('email_dlya_izmeneniya');
	            $fio = trim($user->getValue("lname") . ' ' . $user->getValue("fname") . ' ' . $user->getValue("father_name"));

				$someMail = new umiMail();
				$someMail->addRecipient($email, $fio);
				$someMail->setFrom($email_from, $fio_from);
				$someMail->setSubject($mail_subject);
				$someMail->setContent($mail_content);
				$someMail->commit();
				$someMail->send();

				$result["result"] = "Письмо с подтверждением отправлено";
			}
		}

        $referer = getServer('HTTP_REFERER');
        $noRedirect = getRequest('no-redirect');

        if (!$noRedirect) {
            $this->redirect($referer);
        }

		return $result;
	}

	/**
	 * Функция, вызываемся при клике на ссылку в письме о смене e-mail адреса
	 * @param type $user_id
	 */
	public function change_email_from_letter($user_id, $confirm_code) {
		$result = array("attribute:status" => "error", "result" => "Ошибка");
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if($confirm_code == $user->getValue('pole_koda_aktivaciii_novogo_email')){
				$email = $user->getValue('email_dlya_izmeneniya');
				$email = $this->validateLoginNew($email, $user_id, false);

                $oEventPoint = new umiEventPoint("users_settings_do");
                $oEventPoint->setMode("before");
                $oEventPoint->setParam("user_id", $user_id);
                $this->setEventPoint($oEventPoint);

                $user->setValue("e-mail", $email);
                $user->setValue("login", $email);

				$user->setValue('email_dlya_izmeneniya', '');
				$user->setValue('pole_koda_aktivaciii_novogo_email', '');
				$user->setValue('data_popytki_izmeneniya_email', '');
                $user->commit();

                $oEventPoint->setMode("after");
                $this->setEventPoint($oEventPoint);
				$result["attribute:status"] = "complete";
				$result["result"] = "ok";
			}
		}

		return $result;
	}

    /**
     * Функция для проверки внешнего вида писем
     */
    public function test_email(){
        $mailto = 'aghigay@gmail.com';
//        $mailto = 'aghigay@gmail.com,aghigay@yandex.ru,aghigay@mail.ru';
//        $mailto = 'dm@dpromo.su';

        // Регистрация
        $user_id = 1554191;
        $user = umiObjectsCollection::getInstance()->getObject($user_id);
        list(
            $template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
        ) = def_module::loadTemplatesForMail("users/register/default",
            "mail_registrated", "mail_registrated_subject", "mail_registrated_noactivation", "mail_registrated_subject_noactivation"
        );

        $mailData = array(
            'user_id' => $user->getId(),
            'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
            'activate_link' => getSelectedServerProtocol() . "://" . $domain . $this->pre_lang . "/users/activate/" . $activationCode . "/",
            'login' => $user->getValue('login'),
            'password' => 'test pass',
            'lname' => $user->getValue('lname'),
            'fname' => $user->getValue('fname'),
            'father_name' => $user->getValue('father_name'),
        );

        $email_from = regedit::getInstance()->getVal("//settings/email_from");
        $fio_from = regedit::getInstance()->getVal("//settings/fio_from");

        // без активации
        $mailContent = def_module::parseTemplateForMail($template_mail, $mailData, false, $objectId);
        $mailSubject = def_module::parseTemplateForMail($template_mail_subject, $mailData, false, $objectId);

//        $registrationMail = new umiMail();
//        $registrationMail->addRecipient($mailto);
//        $registrationMail->setFrom($email_from, $fio_from);
//        $registrationMail->setSubject($mailSubject);
//        $registrationMail->setContent($mailContent);
//        $registrationMail->commit();
//        $registrationMail->send();

        // с активацией
        $mailContent = def_module::parseTemplateForMail($template_mail_noactivation, $mailData, false, $objectId);
        $mailSubject = def_module::parseTemplateForMail($template_mail_subject_noactivation, $mailData, false, $objectId);

//        $registrationMail = new umiMail();
//        $registrationMail->addRecipient($mailto);
//        $registrationMail->setFrom($email_from, $fio_from);
//        $registrationMail->setSubject($mailSubject);
//        $registrationMail->setContent($mailContent);
//        $registrationMail->commit();
//        $registrationMail->send();


        // Смена контактного email адреса
        list(
            $template_confirm_email_letter_content, $template_confirm_email_letter_subject
            ) = def_module::loadTemplatesForMail("users/change/default", "confirm_email_letter_content", "confirm_email_letter_subject"
        );

        $mail_arr = Array();
        $mail_arr['domain'] = $_SERVER['HTTP_HOST'];
        $mail_arr['login'] = $user->getValue('login');
        $mail_arr['change_link'] = 'https://' . $_SERVER['HTTP_HOST'] . '/users/change_email_from_letter/' . $user_id . '/' . $user->getValue('pole_koda_aktivaciii_novogo_email');

        $mail_subject = def_module::parseTemplateForMail($template_confirm_email_letter_subject, $mail_arr, false, $user_id);
        $mail_content = def_module::parseTemplateForMail($template_confirm_email_letter_content, $mail_arr, false, $user_id);

//        $someMail = new umiMail();
//        $someMail->addRecipient($mailto);
//        $someMail->setFrom($email_from, $fio_from);
//        $someMail->setSubject($mail_subject);
//        $someMail->setContent($mail_content);
//        $someMail->commit();
//        $someMail->send();

        list($template_mail_verification, $template_mail_verification_subject) = def_module::loadTemplatesForMail("users/forget/default", "mail_verification", "mail_verification_subject");

        $mail_arr = Array();
        $mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
        $mail_arr['restore_link'] = getSelectedServerProtocol() . "://" . $domain . $this->pre_lang . "/users/restore/" . $activate_code . "/";
        $mail_arr['login'] = $user->getValue('login');
        $mail_arr['email'] = $user->getValue('e-mail');

        $mail_subject = def_module::parseTemplateForMail($template_mail_verification_subject, $mail_arr, false, $user_id);
        $mail_content = def_module::parseTemplateForMail($template_mail_verification, $mail_arr, false, $user_id);

//        $someMail = new umiMail();
//        $someMail->addRecipient($mailto);
//        $someMail->setFrom($email_from, $fio_from);
//        $someMail->setSubject($mail_subject);
//        $someMail->setPriorityLevel("highest");
//        $someMail->setContent($mail_content);
//        $someMail->commit();
//        $someMail->send();

        list($template_restore_failed_block, $template_restore_ok_block, $template_mail_password, $template_mail_password_subject) = def_module::loadTemplatesForMail("users/forget/default", "restore_failed_block", "restore_ok_block", "mail_password", "mail_password_subject");

        $mail_arr = Array();
        $mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
        $mail_arr['password'] = 'test_pass';
        $mail_arr['login'] = 'test_login';

        $mail_subject = def_module::parseTemplateForMail($template_mail_password_subject, $mail_arr, false, $user_id);
        $mail_content = def_module::parseTemplateForMail($template_mail_password, $mail_arr, false, $user_id);

//        $someMail = new umiMail();
//        $someMail->addRecipient($mailto);
//        $someMail->setFrom($email_from, $fio_from);
//        $someMail->setSubject($mail_subject);
//        $someMail->setContent($mail_content);
//        $someMail->commit();
//        $someMail->send();


        list($templateLine) = def_module::loadTemplatesForMail("emarket/status1c", $letter_template_name);

        $param = array();
        $param["order_id"] = 111;
        $param["order_number"] = 222;
        $content = def_module::parseTemplateForMail($templateLine, $param);

//        $letter = new umiMail();
//        $letter->addRecipient($email, $name);
//        $letter->setFrom($email_from, $fio_from);
//        $letter->setSubject($subject);
//        $letter->setContent($content);
//        $letter->commit();
//        $letter->send();


         $status_1c_list = array(
//            "Принят" => array(
//                "status_name" => "Принят",
//                "template_name" => "status_1c_adopted",
//                "subject" => "Ваш заказ №%number% поступил в обработку"
//            ),

//            "К оплате" => array(
//                "status_name" => "К оплате",
//                "template_name" => "status_1c_to_pay",
//                "subject" => "Ваш заказ №%number% одобрен и ожидает оплату"
//            ),
//            "Готов" => array(
//                "status_name" => "Сборка",
//                "template_name" => "status_1c_ready",
//                "subject" => "Прогресс по заказу №%number%"
//            ),
//            "Резерв частичный" => array(
//                "status_name" => "Собран частично",
//                "template_name" => "status_1c_partially_assembled",
//                "subject" => "Прогресс по заказу №%number%"
//            ),
//            "Отгружен" => array(
//                "status_name" => "Отгружен",
//                "template_name" => "status_1c_shipped",
//                "subject" => "Прогресс по заказу №%number%"
//            ),
//            "Отменен" => array(
//                "status_name" => "Отменен",
//                "template_name" => "status_1c_cancel",
//                "subject" => "Отмена заказа №%number%"
//            ),
//            "Оплачен" => array(
//                "status_name" => "Оплачен",
//                "template_name" => "status_1c_paid",
//                "subject" => "Заказ №%number% оплачен"
//            ),
        );

        $order_id = 3218078;
        $order_obj = umiObjectsCollection::getInstance()->getObject($order_id);
        foreach($status_1c_list as $one_status_1c){
            $subject = str_replace('%number%', $order_obj->number, $one_status_1c['subject']);
            list($templateLine) = def_module::loadTemplatesForMail("emarket/status1c", $one_status_1c['template_name']);

            $param = array();
            $param["order_id"] = $order_id;
            $param["order_number"] = $order_obj->number;
            $content = def_module::parseTemplateForMail($templateLine, $param);

            $letter = new umiMail();
            $letter->addRecipient($mailto);
            $letter->setFrom($email_from, $fio_from);
            $letter->setSubject($subject);
            $letter->setContent($content);
            $letter->commit();
            $letter->send();
        }

        // Заказ одобрен
//        $emarket_module = cmsController::getInstance()->getModule('emarket');
//        $emarket_module->applyOrder(2683799);

        return 222;
    }
}