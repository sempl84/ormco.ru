<?php
     $permissions = Array(
        'login' => array(
			'login_do_pre','registrate_do_pre','settings_do_pre','checkreg','checkauth',
			'validateSync','parsephone',
			'importNewUser','importModifyUser', 'importCommonUid1cSync',
			'getCommonUIDbyOldID','getCommonUIDbyEmail',
			'outputUsersCommonUID',
			'isRemoteUserExist','createRemoteUser','sa',
			'createForeignUser',
			'update_password', 'set_new_password',
			'change_email_from_letter',
		),
        'settings' => Array(
			'is_phone_valid', 'send_code',
			'apply_code',
			'change_phone',
			'get_user_discount_id',
			'cancel_change_email',
			'send_confirm_email_letter',
		),
     );
?>