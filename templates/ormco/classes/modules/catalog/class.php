<?php

class catalog_custom extends def_module
{
    public function __construct($self)
    {
        $self->__loadLib("/analytics.php", (dirname(__FILE__)));
        $self->__implement("catalog_analytics_tags");
    }
    
    public function getCategoryListFill($id_parent = 0, $level = 5)
    {
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'category');
        $pages->where('hierarchy')->page($id_parent)->childs($level);
        $pages->where('is_active')->equals(1);
        $pages->order('ord');
        $res = $pages->result();
        $total_mpc = $pages->length();
        
        $this->pages = array();
        
        $hierarchy = umiHierarchy::getInstance();
        $arr_trees = $hierarchy->getChilds($id_parent, false, true, $level, 55);
        
        foreach ($res as $val) {
            $this->pages[$val->id] = array(
                "attribute:id" => $val->id,
                "xlink:href" => "upage://" . $val->id,
                "attribute:link" => $val->link,
                "attribute:name" => $val->name,
                "void:alt_name" => $val->name
            );
        }
        
        $udata = array();
        $udata['items']['nodes:item'] = $this->fillSubElements($arr_trees);
        $udata['total'] = $total_mpc;
        
        return $udata;
    }
    
    public function fillSubElements($cur_array)
    {
        $result = array();
        $row = array();
        // перебираем все элементы
        foreach ($cur_array as $l => $page) {
            $row = $this->pages[$l];
            if (sizeof($page) > 0) {
                $row['items']['nodes:item'] = $this->fillSubElements($page);
            }
            $result[] = $row;
        }
        return $result;
    }
    
    public function recommend_category()
    {
        $hierarchy = umiHierarchy::getInstance();
        
        $categoryId = cmsController::getInstance()->getCurrentElementId();
        //$categoryId = 12759;//
        $category = $hierarchy->getElement($categoryId);
        if (!$category) {
            return '';
        }
        $recommend_category = $category->recommend_category;
        
        // пробуем найти в родиетлях
        if (!(sizeof($recommend_category) > 0)) {
            $allParents = $hierarchy->getAllParents($categoryId, false);
            $allParents = array_reverse($allParents);
            
            foreach ($allParents as $parentId) {
                $parent = $hierarchy->getElement($parentId);
                if (!$parent)
                    continue;
                $recommend_category = $parent->recommend_category;
                if (sizeof($recommend_category) > 0) {
                    break;
                }
            }
        }
        if (!(sizeof($recommend_category) > 0)) {
            return;
        }
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        foreach ($recommend_category as $page) {
            $recommend_category_id = $page->id;
            $sel->where('hierarchy')->page($recommend_category_id)->childs(10);
        }
        $sel->order('rand');
        $sel->limit(0, 30);
        $total = $sel->length;
        
        $lines = $block_arr = array();
        foreach ($sel as $page) {
            $element_id = $page->id;
            $line_arr = array();
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:link'] = $hierarchy->getPathById($element_id);
            
            $line_arr['node:text'] = $page->name;
            $lines[] = $this->parseTemplate('', $line_arr, $element_id);
            $hierarchy->unloadElement($element_id);
        }
        
        $block_arr['items'] = array('nodes:item' => $lines);
        $block_arr['total'] = $total;
        $block_arr['category_id'] = $categoryId;
        
        return $this->parseTemplate('', $block_arr, $categoryId);
    }
    
    public function recommend_item()
    {
        $hierarchy = umiHierarchy::getInstance();
        
        $categoryId = cmsController::getInstance()->getCurrentElementId();
        $category = $hierarchy->getElement($categoryId);
        if (!$category) {
            return '';
        }
        
        $recommend_items = $category->recommend_items;
        
        // пробуем найти в родителях
        if (!(sizeof($recommend_items) > 0)) {
            $allParents = $hierarchy->getAllParents($categoryId, false);
            $allParents = array_reverse($allParents);
            
            foreach ($allParents as $parentId) {
                $parent = $hierarchy->getElement($parentId);
                if (!$parent)
                    continue;
                $recommend_items = $parent->recommend_items;
                if (sizeof($recommend_items) > 0) {
                    break;
                }
            }
        }
        if (!(sizeof($recommend_items) > 0)) {
            return;
        }
        $recommend_items_arr = array();
        
        foreach ($recommend_items as $page) {
            $recommend_items_arr[] = $page->id;
        }
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        $sel->where('id')->equals($recommend_items_arr);
        $sel->order('rand');
        $sel->limit(0, 30);
        $total = $sel->length;
        
        
        $lines = $block_arr = array();
        
        foreach ($sel as $page) {
            $element_id = $page->id;
            $line_arr = array();
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:link'] = $hierarchy->getPathById($element_id);
            
            $line_arr['node:text'] = $page->name;
            $lines[] = $this->parseTemplate('', $line_arr, $element_id);
            $hierarchy->unloadElement($element_id);
        }
        
        $block_arr['items'] = array('nodes:item' => $lines);
        $block_arr['total'] = $total;
        $block_arr['category_id'] = $categoryId;
        
        return $this->parseTemplate('', $block_arr, $categoryId);
    }
    
    public function catalog_item_from_symlink($field_name = NULL, $categoryId = NULL)
    {
        $hierarchy = umiHierarchy::getInstance();
        
        if (!$field_name)
            $field_name = getRequest('param0');
        if (!$categoryId)
            $categoryId = getRequest('param1');
        if (!$categoryId) {
            $categoryId = cmsController::getInstance()->getCurrentElementId();
        }
        
        $category = $hierarchy->getElement($categoryId);
        if (!$category)
            return '';
        
        $symlink_items = $category->getValue($field_name);
        
        
        $symlink_items_arr = array();
        
        if ($symlink_items) {
            foreach ($symlink_items as $page) {
                $symlink_items_arr[] = $page->id;
            }
        }
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        $sel->where('id')->equals($symlink_items_arr);
        //$sel->order('rand');
        //$sel->limit(0,30);
        $total = $sel->length;
        
        
        $lines = $block_arr = array();
        
        foreach ($sel as $page) {
            $element_id = $page->id;
            $line_arr = array();
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:link'] = $hierarchy->getPathById($element_id);
            
            $line_arr['node:text'] = $page->name;
            $lines[] = $this->parseTemplate('', $line_arr, $element_id);
            $hierarchy->unloadElement($element_id);
        }
        
        $block_arr['lines'] = array('nodes:item' => $lines);
        $block_arr['total'] = $total;
        $block_arr['category_id'] = $categoryId;
        
        return $this->parseTemplate('', $block_arr, $categoryId);
    }
    
    // разделяем строчку с номерами зубов для 1с импорта
    // udata/custom/explodeStr/34%2C44
    public function explodeStr($str)
    {
        $res = explode(',', $str);
        return array('nodes:item' => $res);
    }
    
    // set common amount flag
    public function sys_amount_flag()
    {
        return 'ffff';
        $categoryId = 4;
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($categoryId)->childs(15);
        //$sel->where('id')->equals($recommend_items_arr);
        //$sel->order('rand');
        //$sel->limit(0,30);
        $total = $sel->length();
        
        
        $lines = $block_arr = array();
        
        foreach ($sel as $page) {
            $page->common_quantity_flag = ($page->common_quantity > 0) ? 1 : 0;
        }
        return 'ok';
        $block_arr['items'] = array('nodes:item' => $lines);
        $block_arr['total'] = $total;
        $block_arr['category_id'] = $categoryId;
        
        return $this->parseTemplate('', $block_arr, $categoryId);
    }
    
    public function try_smart_search()
    {
        header("Content-type: text/json; charset=utf-8");
        $result = array('categories' => array(), 'items' => array(), 'all' => array());
        $search_string = getRequest('phrase');
        $search_branches = getRequest('search_branches');
        $limit_categories = 5;
        $limit_items = 20;
        
        $connection = ConnectionPool::getInstance()->getConnection();
        $hierarhy = umiHierarchy::getInstance();
        
        /* поиск по разделам */
        $categories = new selector('pages');
        $categories->types('hierarchy-type')->name('catalog', 'category');
        if ($search_branches) {
            $categories->where('hierarchy')->page($search_branches)->childs(10);
        }
//        $categories->where('h1')->like("%$search_string%");
        $categories->where('name')->like("%$search_string%");
        $categories->option('no-length');
        $categories->order('id');
        $categories->limit(0, $limit_categories);
//        $sql = str_replace('ORDER BY h.id ASC', ' ORDER BY CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(oc_3_lj.varchar_val, "№", ""), "\"", ""), ",", ""), "/", ""), ")", ""), "(", ""), "+", ""), "*", ""), "-", ""), ".", ""), " ", "")) ASC ', $categories->query());
        $sql = str_replace('ORDER BY h.id ASC', ' ORDER BY CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(o.name, "№", ""), "\"", ""), ",", ""), "/", ""), ")", ""), "(", ""), "+", ""), "*", ""), "-", ""), ".", ""), " ", "")) ASC ', $categories->query());
        
        $res = $connection->queryResult($sql);
        $res->setFetchType(IQueryResult::FETCH_ASSOC);
        foreach ($res as $row) {
            $category = $hierarhy->getElement($row['id']);
            if ($category instanceof umiHierarchyElement) {
                $photo = $category->getValue('header_pic');
                $row = array(
                    "id" => $category->getId(),
//                    "name" => $category->getValue('h1'),
                    "name" => $category->getName(),
                    "link" => $category->link,
                    "icon" => '/templates/ormco/img/nofoto.jpg'
                );
                
                if ($photo instanceof umiImageFile) {
                    $row["icon"] = substr($photo->getFilePath(), 1);
                }
                $result["categories"][] = $row;
            }
        }
        
        /* поиск по товарам */
        $items = new selector('pages');
        $items->types('hierarchy-type')->name('catalog', 'object');
        if ($search_branches) {
            $items->where('hierarchy')->page($search_branches)->childs(10);
        }
        $items->option('or-mode')->fields('h1', 'artikul');
        $items->where('h1')->like("%$search_string%");
        $items->where('artikul')->like("%$search_string%");
        $items->option('no-length');
        $items->order('poiskovyj_ves');
        $items->limit(0, $limit_items);
        $sql = str_replace('ORDER BY oc_671_lj.int_val ASC, h.ord ASC', ' ORDER BY oc_671_lj.int_val ASC, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(oc_3_lj.varchar_val, "№", ""), "\"", ""), ",", ""), "/", ""), ")", ""), "(", ""), "+", ""), "*", ""), "-", ""), ".", ""), " ", "")) ASC, h.ord ASC ', $items->query());
        $sql = str_replace('oc_3_lj.varchar_val', 'o.name', $sql);
        
        $res = $connection->queryResult($sql);
        $res->setFetchType(IQueryResult::FETCH_ASSOC);
        foreach ($res as $row) {
            $item = $hierarhy->getElement($row['id']);
            if ($item instanceof umiHierarchyElement) {
                $photo = $item->getValue('photo');
                $row = array(
                    "id" => $item->getId(),
                    "name" => $item->getValue('h1'),
                    "link" => $hierarhy->getPathById($row['id']),
                    "icon" => '/templates/ormco/img/nofoto.jpg'
                );
                
                if ($photo instanceof umiImageFile) {
                    $row["icon"] = substr($photo->getFilePath(), 1);
                }
                $result["items"][] = $row;
            }
        }
        
        $result["all"][] = array(
            "name" => 'Смотреть все результаты',
            "link" => '/search/search_do/?search_string=' . $search_string . '&search_branches=' . $search_branches,
            "class" => 'bold',
        );
        
        echo json_encode($result);
        die();
    }
    
    /**
     * Очистка кеша брекетов из административной зоны
     */
    public function clear_bracket_cache()
    {
        $data_module = cmsController::getInstance()->getModule('data');
        return $data_module->clear_cache('catalog', 'bracket');
    }
    
    public function hide_drafts_catalog($categoryId = NULL, $is_active = NULL)
    {
        return $this->change_page_active(8313, 0);
    }
    
    public function change_page_active($categoryId = NULL, $is_active = NULL)
    {
        if ($is_active == NULL) {
            return 'укажите второй параметр (включить или выключить активность)';
        }
        if (!in_array($is_active, array(0, 1))) {
            return 'укажите второй параметр корректно (включить или выключить активность). Он должен быть 1 или 0';
        }
        if (!$categoryId) $categoryId = getRequest('param0');
        $hierarchy = umiHierarchy::getInstance();
        $category = $hierarchy->getElement($categoryId);
        if (!($category instanceof umiHierarchyElement)) {
            return 'no category page';
        }
        
        $category->setIsActive($is_active);
        $category->commit();
        
        $lines = $line_arr = array();
        $line_arr['attribute:id'] = $categoryId;
        $line_arr['attribute:name'] = $category->getName();
        $lines[] = def_module::parseTemplate('', $line_arr, $categoryId);
        
        $pages = new selector('pages');
        $pages->where('is_active')->equals(array(0, 1));
        $pages->where('lang')->equals(false);
        $pages->option('no-permissions')->value(true);
        $pages->where('hierarchy')->page($categoryId)->childs(20);
        
        $total = $pages->length();
        
        foreach ($pages as $element) {
            $line_arr = array();
            
            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:name'] = $element->getName();
            //$line_arr['attribute:link'] = umiHierarchy::getInstance() -> getPathById($element_id);
            $lines[] = def_module::parseTemplate('', $line_arr, $element_id);
            
            
            $element->setIsActive($is_active);
            
            $element->commit();
            
            umiHierarchy::getInstance()->unloadElement($element_id);
        }
        $block_arr = array('nodes:item' => $lines);
        return array('total' => $total, 'categoryId' => $categoryId, 'is_active' => $is_active, 'items' => $block_arr);
    }
    
    public function get_main_catalog_list($category_id = false)
    {
        if (!$category_id) $category_id = 4;
        if ((string)$category_id != '0') $category_id = $this->analyzeRequiredPath($category_id);
        
        if ($category_id === false) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $category_id));
        }
        
        $categories = new selector('pages');
        $categories->types('object-type')->name('catalog', 'category');
        $categories->where('hierarchy')->page($category_id);
        
        
        $result = $categories->result();
        $total = $categories->length();
        
        if (($sz = sizeof($result)) > 0) {
            $block_arr = array();
            $lines = array();
            
            foreach ($result as $element) {
                if (!$element instanceof umiHierarchyElement) {
                    continue;
                }
                $element_id = $element->getId();
                
                $line_arr = array();
                $line_arr['attribute:id'] = $element_id;
                $line_arr['void:alt_name'] = $element->getAltName();
                $line_arr['attribute:link'] = $this->umiLinksHelper->getLinkByParts($element);
                $line_arr['xlink:href'] = "upage://" . $element_id;
                $line_arr['node:text'] = $element->getName();
                
                $object = $element->getObject();
                $property = $object->getPropByName('header_pic');
                if (!$property instanceof umiObjectProperty) continue;
                $line_arr['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
                
                $lines[] = self::parseTemplate('', $line_arr, $element_id);
            }
            
            $block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
            $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
            $block_arr['total'] = $total;
            return self::parseTemplate('', $block_arr, $category_id);
        } else {
            $block_arr = array();
            $block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
            return self::parseTemplate('', $block_arr, $category_id);
        }
    }
}
