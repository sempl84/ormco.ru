<?php
$permissions = Array(
    'tree' => Array(
        'clear_bracket_cache'
    ),
    'view' => Array(
        'getCategoryListFill', 'explodeStr',
        'bracket_paz', 'bracket',
        'recommend_category', 'recommend_item', 'catalog_item_from_symlink',
        'addGaData',
        'addImpression', 'addImpressionTest', 'getNavibarCategory',
        'addMetrikData', 'addDetail_metrik',
        'try_smart_search',
        'get_main_catalog_list',
    )
);