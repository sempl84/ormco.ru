<?php
class catalog_analytics_tags extends def_module {
    // apply ga analytics info
    public function addGaData() {
        $moduleEmarket = cmsController::getInstance()->getModule('emarket');
        return $this->addImpression() . $this->addDetail() . $moduleEmarket->addCheckoutStep1();
    }

    //добавить сптсок товаров в ga если это страница каталога
    public function addImpression() {
        $pages_info = '';
        $cmsController = cmsController::getInstance();
        $hierarchy = umiHierarchy::getInstance();

        // если это каталог с товарами
        if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'category')){
            return '';
        }

        $dataCategory = $parentName ='';
        $limit = $ignorePaging = false;

        // взять текущую страницу
        $currPageId = $cmsController -> getCurrentElementId();

        $currPage = $hierarchy->getElement($currPageId);
        $isChooseBrackets = $currPage->choose_brackets;

        if($isChooseBrackets){
            $limit = 10;
        }

        $limit = ($limit) ? $limit : $this->per_page;
        $currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
        $offset = $currentPage * $limit;

        // перебрать товары на данной странице
        $catalogPages = self::getSmartCatalogArray($currPageId,$limit,$ignorePaging);

        // получаем navibar для параметра category
        $parents = $hierarchy->getAllParents($currPageId);
        $parents[] = $currPageId;

        $firstItem = true;
        foreach($parents as $elementId) {
            if(!$elementId){
                continue;
            }

            $element = $hierarchy->getElement($elementId);
            if($element instanceof iUmiHierarchyElement) {
                $dataCategory .= (!$firstItem) ? '/' : '';
                $dataCategory .= $element->name;

                $parentName = $element->name;
                $firstItem = false;
            }
        }

        // создать массив данных
        foreach($catalogPages as $page){
            $dataArtikul = $page->artikul;
            $dataName = $page->name;
            $dataList = $parentName;
            $dataPosition = $offset;

            $pages_info .= <<<END
ga('ec:addImpression', {
 'id': '{$dataArtikul}',
 'name': '{$dataName}',
 'category': '{$dataCategory}',
 'brand': '',
 'list': '{$dataList}',
 'position': {$dataPosition}
});
END;
            $offset++;
        }

        return (string)$pages_info;//def_module::parseTemplate('%pages%', array('pages'=>$page_info));
    }

    /**
     * Выводит данные для формирования списка объектов каталога, с учетом параметров фильтрации
     * @param int $categoryId ид раздела каталога, объекты которого требуется вывести
     * @param int $limit ограничение количества выводимых объектов каталога
     * @param bool $ignorePaging игнорировать постраничную навигацию (то есть GET параметр 'p')
     * @param int $level уровень вложенности раздела каталога $categoryId, на котором размещены необходимые объекты каталога
     * @param bool $fieldName поле объекта каталога, по которому необходимо произвести сортировку
     * @param bool $isAsc порядок сортировки
     * @return mixed
     */
    public function getSmartCatalogArray($categoryId, $limit, $ignorePaging = false, $level = 1, $fieldName = false, $isAsc = true) {
        $umiHierarchy = umiHierarchy::getInstance();
        $category = $umiHierarchy->getElement($categoryId);

        if (!$category instanceof iUmiHierarchyElement) {
            return array();
        }

        $limit = ($limit) ? $limit : $this->per_page;
        $currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
        $offset = $currentPage * $limit;

        if (!is_numeric($level)) {
            $level = 1;
        }

        $filteredProductsIds = null;
        $queriesMaker = null;
        if (is_array(getRequest('filter'))) {
            $emptyItemsTemplate = $emptySearchTemplates;
            $queriesMaker = $this->getCatalogQueriesMaker($category, $level);

            if (!$queriesMaker instanceof FilterQueriesMaker) {
                return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
            }

            $filteredProductsIds = $queriesMaker->getFilteredEntitiesIds();

            if (count($filteredProductsIds) == 0) {
                return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
            }
        }

        $products = new selector('pages');
        $products->types('hierarchy-type')->name('catalog', 'object');

        if (is_null($filteredProductsIds)) {
            $products->where('hierarchy')->page($categoryId)->childs($level);
        } else {
            $products->where('id')->equals($filteredProductsIds);
        }

        if ($fieldName) {
            if ($isAsc) {
                $products->order($fieldName)->asc();
            } else {
                $products->order($fieldName)->desc();
            }
        } else {
            $products->order('ord')->asc();
        }

        if ($queriesMaker instanceof FilterQueriesMaker) {
            if (!$queriesMaker->isPermissionsIgnored()) {
                $products->option('no-permissions')->value(true);
            }
        }

        $products->option('load-all-props')->value(true);
        $products->limit($offset, $limit);
        $pages = $products->result();
        $total = $products->length();
        if ($total == 0) {
            return array();
        }
        return $pages;
    }

    //добавить информацию о товаре в ga если это страница товара
    public function addDetail() {
        $cmsController = cmsController::getInstance();
        $hierarchy = umiHierarchy::getInstance();

        // если это страница товара
        if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'object')){
            return '';
        }


        $dataCategory = $parentName ='';
        // взять текущую страницу
        $currPageId = $cmsController -> getCurrentElementId();
        //$currPageId = 12760; // TODEL

        $currPage = $hierarchy->getElement($currPageId);

        // получаем navibar для параметра category
        $parents = $hierarchy->getAllParents($currPageId);
        $parents[] = $currPageId;

        $firstItem = true;
        foreach($parents as $elementId) {
            if(!$elementId){
                continue;
            }

            $element = $hierarchy->getElement($elementId);
            if($element instanceof iUmiHierarchyElement) {
                $dataCategory .= (!$firstItem) ? '/' : '';
                $dataCategory .= $element->name;

                $parentName = $element->name;
                $firstItem = false;
            }
        }

        $dataArtikul = $currPage->artikul;
        $dataName = $currPage->name;

        $pages_info = <<<END
ga('ec:addProduct', {
   'id': '{$dataArtikul}',
   'name': '{$dataName}',
   'category': '{$dataCategory}',
   'brand': ''
});
ga('ec:setAction', 'detail');
END;

        return (string)$pages_info;
    }

    // получить путь до страницы $currPageId
    public function getNavibarCategory($currPageId, $isParent = true) {
        $hierarchy = umiHierarchy::getInstance();
        $dataCategory = $parentName ='';
        // если она каталог с товарами

        // получаем navibar для параметра category
        $parents = $hierarchy->getAllParents($currPageId);
        if($isParent){
            $parents[] = $currPageId;
        }

        $firstItem = true;
        foreach($parents as $elementId) {
            if(!$elementId){
                continue;
            }

            $element = $hierarchy->getElement($elementId);
            if($element instanceof iUmiHierarchyElement) {
                $dataCategory .= (!$firstItem) ? '/' : '';
                $dataCategory .= $element->name;

                $parentName = $element->name;
                $firstItem = false;
            }
        }

        return (string)$dataCategory;
    }

    // apply yandex analytics info
    public function addMetrikData() {
        $pages_info = <<<END
window.dataLayer = window.dataLayer || [];
END;
        return (string)$pages_info . $this->addDetail_metrik() ;
    }

    //добавить информацию о товаре в ga если это страница товара
    public function addDetail_metrik() {
        $cmsController = cmsController::getInstance();
        $hierarchy = umiHierarchy::getInstance();

        // если это страница товара
        if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'object')){
            return '';
        }

        $dataCategory = $parentName ='';

        // взять текущую страницу
        $currPageId = $cmsController -> getCurrentElementId();
        $currPage = $hierarchy->getElement($currPageId);

        $dataCategory = $this->getNavibarCategory($currPageId);

        $dataArtikul = $currPage->artikul;
        $dataName = $currPage->name;
        $originalPrice = $currPage->price;
        $discount = itemDiscount::search($currPage);
        $actualPrice = ($discount instanceof itemDiscount) ? $discount->recalcPrice($originalPrice) : $originalPrice;

        $pages_info = <<<END
dataLayer.push({
   "ecommerce": {
       "detail": {
           "products": [
               {
                   "id": "{$dataArtikul}",
                   "name" : "{$dataName}",
                   "price": "{$actualPrice}",
                   "brand": "",
                   "category": "{$dataCategory}",
                   }
           ]
       }
   }
});
END;

        return (string)$pages_info;
    }
}