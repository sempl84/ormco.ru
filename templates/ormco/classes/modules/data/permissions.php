<?php
$permissions = Array(
    'main' => Array(
        'dateru',
        'dateruPeriod',
        'currentTimestamp',
        'tmsDatepicker',
        'save_users_change_log',
        'get_users_change_log',
        'get_all_vuz_by_region',
    )
);