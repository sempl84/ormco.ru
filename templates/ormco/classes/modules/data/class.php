<?php
class data_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/__search.php", (dirname(__FILE__)));
        $self->__implement("data_custom_search");
    }

    public function getMonthRu($month, $year, $type = 0) {
        // Проверка существования месяца
        if (!checkdate($month, 1, $year)) {
            throw new publicException("Проверьте порядок ввода даты.");
        }
        $months_ru = Array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $months_ru1 = Array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
        if ($type == 1) {
            return $months_ru1[$month];
        }
        return $months_ru[$month];
    }

    public function dateru($time) {
        if (!$time){
            return;
        }
        $day = date('j', $time);
        $month = date('n', $time);
        $year = date('Y', $time);
        $hour = date('g', $time);
        $minutes = date('i', $time);
        $seconds = date('s', $time);

        $temp = Array(
            'month' => $this->getMonthRu($month, $year),
            'day' => $day,
            'year' => $year,
            'hour' => $hour,
            'minutes' => $minutes,
            'seconds' => $seconds
        );
        return def_module::parseTemplate('', $temp);
    }

    // test data:
    // 1352204856 Tue, 06 Nov 2012 12:27:36 GMT
    // 1352377656 Thu, 08 Nov 2012 12:27:36 GMT
    // 1355660856 Sun, 16 Dec 2012 12:27:36 GMT
    // 1357475256 Sun, 06 Jan 2013 12:27:36 GMT
    // 1383740856 Wed, 06 Nov 2013 12:27:36 GMT
    public function dateruPeriod($timeBegin = NULL, $timeEnd = NULL) {
        if (!$timeBegin || !$timeEnd){
            return "-";
        }
        if (!is_numeric($timeBegin) || !is_numeric($timeEnd)){
            return "=";
        }
        if ($timeEnd < $timeBegin) {
            $tmp = $timeEnd;
            $timeEnd = $timeBegin;
            $timeBegin = $tmp;
        }

        $dayBegin = date('d', $timeBegin);
        $monthBegin = date('n', $timeBegin);
        $yearBegin = date('Y', $timeBegin);
        $dayEnd = date('d', $timeEnd);
        $monthEnd = date('n', $timeEnd);
        $yearEnd = date('Y', $timeEnd);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd ");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin ");

        $yearCurrent = date('Y');

        if (($monthBegin != $monthEnd) || ($yearBegin != $yearEnd)) {
            $firstLine = $dayBegin . ' ' . $this->getMonthRu($monthBegin, $yearBegin) . ' ';
            $secondLine = $dayEnd . ' ' . $this->getMonthRu($monthEnd, $yearEnd);
            /* if($yearBegin != $yearEnd) {
              $secondLine .= ' '.$yearEnd.' г.';
              } */
            $firstLine .= '-';
        } else {
            $firstLine = ($dayBegin != $dayEnd) ? $dayBegin . '-' . $dayEnd : $dayBegin;
            $secondLine = $this->getMonthRu($monthBegin, $yearBegin);
        }

        //if(($yearEnd != $yearCurrent) && ($yearBegin == $yearEnd)) {
        $secondLine .= ' ' . $yearEnd . ' г.';
        //}
        $days = (int) (($timeEnd_forCalc - $timeBegin_forCalc) / 86400) + 1; // секунд в 24 часах
        $temp = Array(
            'first_line' => $firstLine,
            'second_line' => $secondLine,
            'days' => $days . ' ' . $this->wordDays($days)
        );
        return def_module::parseTemplate('', $temp);
    }

    public function wordDays($days) {
        $units = $days - (int) ($days / 10) * 10;
        $tens = $days - (int) ($days / 100) * 100 - $units;
        if ($tens == 10){
            return 'дней'; // 11-19 дней
        }

        if ($units == 1){
            return 'день'; // 1 21 31... день
        }
        if (($units > 1) && ($units < 5)){
            return 'дня'; // 2 3 4 22 23 24 дня
        }
        return 'дней'; // 5 6 7 8 9 20 дней
    }

    public function currentTimestamp() {
        $temp = Array(
            'timestamp' => time()
        );
        return def_module::parseTemplate('', $temp);
    }

    public function tmsDatepicker($timestp = NULL) {
        if (!$timestp){
            return '';
        }
        $day = date('d', $timestp);
        $month = date('n', $timestp);
        $year = date('Y', $timestp);
        $month = $this->getMonthRu($month, $year);
        return "$day $month, $year";
    }

    public function hideFromSearch($categoryId = NULL, $search_hide = NULL) {
        if ($search_hide == NULL) {
            return 'укажите второй параметр (включить или выключить галочку)';
        }
        if (!in_array($search_hide, array(0, 1))) {
            return 'укажите второй параметр корректно (включить или выключить галочку). Он должен быть 1 или 0';
        }
        if (!$categoryId){
            $categoryId = getRequest('param0');
        }
        $hierarchy = umiHierarchy::getInstance();
        $category = $hierarchy->getElement($categoryId);
        if (!($category instanceof umiHierarchyElement)) {
            return 'no category page';
        }

        $category->is_unindexed = $search_hide;
        $category->commit();

        $lines = $line_arr = array();
        $line_arr['attribute:id'] = $categoryId;
        $line_arr['attribute:name'] = $category->getName();
        $lines[] = def_module::parseTemplate('', $line_arr, $categoryId);

        $pages = new selector('pages');
        $pages->where('is_active')->equals(array(0, 1));
        $pages->where('hierarchy')->page($categoryId)->childs(20);

        $total = $pages->length;
        foreach ($pages as $element) {
            $line_arr = Array();

            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:name'] = $element->getName();
            //$line_arr['attribute:link'] = umiHierarchy::getInstance() -> getPathById($element_id);
            $lines[] = def_module::parseTemplate('', $line_arr, $element_id);

            $element->is_unindexed = $search_hide;

            $element->commit();

            umiHierarchy::getInstance()->unloadElement($element_id);
        }
        $block_arr = array('nodes:item' => $lines);
        return array('total' => $total, 'categoryId' => $categoryId, 'search_hide' => $search_hide, 'items' => $block_arr);
    }

    public function hideFromRobots($categoryId = NULL, $robots_deny_new = NULL) {
        if ($robots_deny_new == NULL) {
            return 'укажите второй параметр (включить или выключить галочку)';
        }
        if (!in_array($robots_deny_new, array(0, 1))) {
            return 'укажите второй параметр корректно (включить или выключить галочку). Он должен быть 1 или 0';
        }
        if (!$categoryId){
            $categoryId = getRequest('param0');
        }
        $hierarchy = umiHierarchy::getInstance();
        $category = $hierarchy->getElement($categoryId);
        if (!($category instanceof umiHierarchyElement)) {
            return 'no category page';
        }

        $category->robots_deny_new = $robots_deny_new;
        $category->commit();

        $lines = $line_arr = array();
        $line_arr['attribute:id'] = $categoryId;
        $line_arr['attribute:name'] = $category->getName();
        $lines[] = def_module::parseTemplate('', $line_arr, $categoryId);

        $pages = new selector('pages');
        $pages->where('is_active')->equals(array(0, 1));
        $pages->where('hierarchy')->page($categoryId)->childs(20);

        $total = $pages->length;
        foreach ($pages as $element) {
            $line_arr = Array();

            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:name'] = $element->getName();
            //$line_arr['attribute:link'] = umiHierarchy::getInstance() -> getPathById($element_id);
            $lines[] = def_module::parseTemplate('', $line_arr, $element_id);

            $element->robots_deny_new = $robots_deny_new;

            $element->commit();

            umiHierarchy::getInstance()->unloadElement($element_id);
        }
        $block_arr = array('nodes:item' => $lines);
        return array('total' => $total, 'categoryId' => $categoryId, 'robots_deny_new' => $robots_deny_new, 'items' => $block_arr);
    }

    public function hideFromSitemap($categoryId = NULL, $sitemap_hide = NULL) {
        if ($sitemap_hide == NULL) {
            return 'укажите второй параметр (включить или выключить галочку)';
        }
        if (!in_array($sitemap_hide, array(0, 1))) {
            return 'укажите второй параметр корректно (включить или выключить галочку). Он должен быть 1 или 0';
        }
        if (!$categoryId){
            $categoryId = getRequest('param0');
        }
        $hierarchy = umiHierarchy::getInstance();
        $category = $hierarchy->getElement($categoryId);
        if (!($category instanceof umiHierarchyElement)) {
            return 'no category page';
        }

        $category->robots_deny = $sitemap_hide;
        $category->commit();

        $lines = $line_arr = array();
        $line_arr['attribute:id'] = $categoryId;
        $line_arr['attribute:name'] = $category->getName();
        $lines[] = def_module::parseTemplate('', $line_arr, $categoryId);

        $pages = new selector('pages');
        $pages->where('is_active')->equals(array(0, 1));
        $pages->where('hierarchy')->page($categoryId)->childs(20);

        $total = $pages->length;
        foreach ($pages as $element) {
            $line_arr = Array();

            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:name'] = $element->getName();
            //$line_arr['attribute:link'] = umiHierarchy::getInstance() -> getPathById($element_id);
            $lines[] = def_module::parseTemplate('', $line_arr, $element_id);

            $element->robots_deny = $sitemap_hide;

            $element->commit();

            umiHierarchy::getInstance()->unloadElement($element_id);
        }
        $block_arr = array('nodes:item' => $lines);
        return array('total' => $total, 'categoryId' => $categoryId, 'sitemap_hide' => $sitemap_hide, 'items' => $block_arr);
    }

    /**
     * Сохранение факта изменения данных пользователя
     * @param type $obj_id - идентификатор объекта пользователя
     * @param type $author - имя автора изменений, строка
     * @param type $details - текстовое описание события
     */
    public function save_users_change_log($obj_id, $author, $message, $details) {
        $author = mysql_real_escape_string($author);
        $message = mysql_real_escape_string($message);
        $details = serialize($details);
        $connection = ConnectionPool::getInstance()->getConnection();
        $connection->query("INSERT INTO `users_change_log`(`obj_id`, `author`, `message`, `details`) VALUES ({$obj_id}, '{$author}', '{$message}', '{$details}')");
    }

    /**
     * Получение лога изменений данных пользователя по его идентификатору
     * @param type $obj_id - идентификатор объекта пользователя
     * @return type
     */
    public function get_users_change_log() {
        $obj_id = getRequest('obj_id');
        $res = array();
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult("SELECT * FROM `users_change_log` WHERE `obj_id` = '{$obj_id}' ORDER BY `created` DESC ");
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        foreach ($result as $row) {
            $res[] = $row;
        }
        return array("nodes:item" => $res);
    }

    // Логирование изменений в пользователе из административной зоны и при выгрузке из 1С
    public function update_user_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $object = $event->getRef('object');
        $objectTypeId = $object->getTypeId();
        $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
        if ($objectTypeId !== $user_type_id) { // не пользователь
            return true;
        }

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = '';
            if($event->getParam('source_id') !== false){
                $creater = "Обмен данными с 1С";
            }else{
                $permissions = permissionsCollection::getInstance();
                $current_user_id = $permissions->getUserId();
                $user = umiObjectsCollection::getInstance()->getObject($current_user_id);
                if ($user instanceof umiObject) {
                    $creater = $user->getValue('login') . ' (' . $current_user_id . ')';
                }
            }

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе из административной зоны на списке пользователей
    public function update_fast_property_user_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_property;

        $object = $event->getRef('entity');
        $property_name = $event->getParam('property');

        $objectTypeId = $object->getTypeId();
        $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
        if ($objectTypeId !== $user_type_id) { // не пользователь
            return true;
        }

        if ($event->getMode() == 'before') {
            $old_serialize_property = $this->create_property_snapshot($object->getPropByName($property_name));
        }elseif ($event->getMode() == 'after') {
            $permissions = permissionsCollection::getInstance();
            $current_user_id = $permissions->getUserId();
            $user_name = '';
            $user = umiObjectsCollection::getInstance()->getObject($current_user_id);
            if ($user instanceof umiObject) {
                $user_name = $user->getValue('login');
            }

            $new_serialize_property = $this->create_property_snapshot($object->getPropByName($property_name));
            $res = $this->create_diff_details($old_serialize_property, $new_serialize_property);

            $this->save_users_change_log($object->id, $user_name . ' (' . $current_user_id . ')', $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе из личного кабинета
    public function update_user_from_personal_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Личный кабинет';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование восстановления пароля пользователем
    public function update_user_restore_password_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Восстановление пароля';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    /**
     * Превращаем объект в массив свойств
     * @param umiObject $object
     * @return массив со значениями свойств
     */
    public function create_object_snapshot(umiObject $object) {
        $result = array();
        $object_types_collection = umiObjectTypesCollection::getInstance();
        $object_type = $object_types_collection->getType($object->getTypeId());
        $object_fields = $object_type->getAllFields();
        foreach ($object_fields as $field) {
            $row = $this->create_property_snapshot($object->getPropByName($field->getName()));
            if ($row !== false) {
                $result = $result + $row;
            }
        }
        return $result;
    }

    /**
     * Превращаем одно свойство объекта в массив
     * @param umiObjectProperty $property
     * @return boolean false если значение пусто или массив из правильных значений
     */
    public function create_property_snapshot(umiObjectProperty $property) {
        try {
            $value = $property->getValue();
            if (!empty($value)) {
                $field = $property->getField();
                $res = array(
                    "id" => $field->getId(),
                    "type" => $field->getDataType(),
                    "name" => $property->getName(),
                    "title" => $property->getTitle()
                );
                if (is_array($value)) {
                    $res['value'] = $value;
                } else {
                    $res['value'] = mysql_real_escape_string($value);
                }
                return array($field->getId() => $res);
            }
        } catch (Exception $exc) {
        }
        return false;
    }

    /**
     * Получаем разницу между двумя сериализованными объектами и сообщение об этом с деталями
     * @param type $old_value
     * @param type $new_value
     * @return type
     */
    public function create_diff_details($old_value, $new_value) {
        $message = array();
        $details = array();
        $diff_props = array();

        // все ключи
        $keys = array_unique(array_merge(array_keys($old_value), array_keys($new_value)));
        foreach ($keys as $key) {
            // пропускаем пустой пароль
            if (key_exists($key, $old_value) && !key_exists($key, $new_value) && $old_value[$key]['title'] == 'Пароль') {
                continue;
            }

            // Дата 1970-01-01 03:00:00 - пустая, так как это стандарт пустого значения редактора дат. Чистим значение
            if (key_exists($key, $new_value) && $new_value[$key]['type'] == 'date' && $new_value[$key]['value'] == '1970-01-01 03:00:00') {
                unset($new_value[$key]);
            }

            // Файл, не являющийся файлом (директория) - пустой, так как это стандарт пустого значения выбора файла. Чистим значение
            if (key_exists($key, $new_value) && $new_value[$key]['type'] == 'file' && !is_file($_SERVER['DOCUMENT_ROOT'] . $new_value[$key]['value'])) {
                unset($new_value[$key]);
            }

            if (key_exists($key, $old_value) && !key_exists($key, $new_value)) {
                $diff_props[$key] = array(
                    "name" => $old_value[$key]['title'],
                    "old" => $old_value[$key],
                    "new" => false
                );
            } elseif (!key_exists($key, $old_value) && key_exists($key, $new_value)) {
                $diff_props[$key] = array(
                    "name" => $new_value[$key]['title'],
                    "old" => false,
                    "new" => $new_value[$key]
                );
            } elseif (key_exists($key, $old_value) && key_exists($key, $new_value)) {
                if ($old_value[$key]['value'] != $new_value[$key]['value']) {
                    $diff_props[$key] = array(
                        "name" => $old_value[$key]['title'],
                        "old" => $old_value[$key],
                        "new" => $new_value[$key]
                    );
                }
            }
        }

        foreach ($diff_props as $key => $values) {
            $message[] = 'Изменено свойство "' . $values['name'] . '". Старое значение: "' . ($values['old'] === false ? '' : $values['old']['value']) . '", новое значение: "' . ($values['new'] === false ? '' : $values['new']['value']) . '".';
            $details[$key] = $values;
        }

        return array(
            "message" => implode("<br>", $message),
            "details" => $details
        );
    }

    /**
     * Пытаемся получить закешированные данные
     * @param string $module
     * @param string $method
     * @param string $file_name
     * @param string $expire
     * @return string
     */
    public function try_load_from_cache($module, $method, $file_name, $expire = 10800){ // 10800 = 60 * 60 * 3
        $path = $this->get_cache_path($module, $method);
        $full_file_name = $path . '/' . $file_name;

        if(is_file($full_file_name)){
            if(filemtime($full_file_name) + $expire > time()){
                return unserialize(file_get_contents($full_file_name));
            }
        }
        return null;
    }

    /**
     * Сохраняем данные в кеш
     * @param string $module
     * @param string $method
     * @param string $file_name
     * @param array $data
     */
    public function save_to_cache($module, $method, $file_name, $data){
        $path = $this->get_cache_path($module, $method);
        file_put_contents($path . '/' . $file_name, serialize($data));
    }

    /**
     * Получаем директорию для кеширования и сразу создаем её, если нужно
     * @param string $module
     * @param string $method
     * @return string
     */
    protected function get_cache_path($module, $method){
        $base_dir_path = CURRENT_WORKING_DIR . '/sys-temp/ormco_cache';
        if(!is_dir($base_dir_path)){
            mkdir($base_dir_path, 0777);
        }
        $cache_path = $base_dir_path . '/' . $module . '_' . $method;
        if(!file_exists($cache_path)){
            mkdir($cache_path, 0777);
        }
        return $cache_path;
    }
    /**
     * Очистка кеша конкретного модуля и метода
     * @param type $module
     * @param type $method
     */
    public function clear_cache($module, $method){
        $result = array(
            'module' => $module,
            'method' => $method,
            'count' => -1
        );

        $path = $this->get_cache_path($module, $method) . '/';
        if (file_exists($path)) {
            $result['count'] = 0;
            foreach (glob($path . '*') as $file) {
                unlink($file);
                $result['items']['nodes:item'][] = $file;
                $result['count']++;
            }
        }
        return $result;
    }

	/**
	 * Получение всех вузов с разделением по регионам
	 * @return type
	 */
	public function get_all_vuz_by_region(){
		$result = array();
		$items = new selector('objects');
		$items->types('object-type')->id(350);
		foreach ($items as $item){
			if(empty($item->getValue('region'))){
				$result[] = array(
					"attribute:id" => $item->getId(),
					"node:text" => $item->getValue('nazvanie_vuza')
				);
			}else{
				$result[$item->getValue('region')]["nodes:item"][] = array(
					"attribute:id" => $item->getId(),
					"node:text" => $item->getValue('nazvanie_vuza')
				);
			}
		}

		$collection = umiObjectsCollection::getInstance();
		foreach($result as $l=>$v){
			if(isset($result[$l]['nodes:item'])){
				$result[$l]['attribute:id'] = $l;
				$obj = $collection->getObject($l);
				if($obj instanceof umiObject){
					$result[$l]['attribute:name'] = $obj->getName();
				}
			}
		}

		return array("nodes:item" => $result);
	}
}