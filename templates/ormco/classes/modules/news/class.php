<?php
    class news_custom extends def_module {
		
		 
		/*public function load_tree_node() {
			$this->setDataType("list");
			$this->setActionType("view");

			$limit = 50;//getRequest('per_page_limit');
			$curr_page = getRequest('p');
			$offset = $curr_page * $limit;

			list($rel) = getRequest('rel');
			$sel = new selector('pages');
			if ($rel !== 0) {
				$sel->limit($offset, $limit);
			}
			selectorHelper::detectFilters($sel);

			$result = $sel->result;
			$length = $sel->length;
			$templatesData = getRequest('templates');

			if ($templatesData) {
				$templatesList = explode(',', $templatesData);
				$result = $this->getPagesByTemplatesIdList($templatesList, $limit, $offset);
				$length = $this->getTotalPagesByTemplates($templatesList);
			}

			$data = $this->prepareData($result, "pages");
			$this->setData($data, $length);
			$this->setDataRange($limit, $offset);

			if ($rel != 0) {
				$this->setDataRangeByPerPage($limit, $curr_page);
			}
			return $this->doData();
		}
		
		public function jsNoConflictMerge($array_files, $destination_dir, $dest_file_name, $newjQuery = NULL, $noCache = false) {
			if(!$newjQuery) $newjQuery = 'lastJQ';
			$prefix = "(function($,jQuery){\n";
			$suffix = "\n})({$newjQuery},{$newjQuery});\n\n";
			$array_files = explode(',', $array_files);
			//var_dump(is_file($dest_file_name));
			//var_dump($noCache);
			if(!is_file($dest_file_name) || (is_file($dest_file_name) && $noCache)){
				//exit('ggg');
				$content = "";
				foreach ($array_files as $file) {
					$content .= $prefix . file_get_contents($destination_dir . $file) . $suffix ;
				}
		
				$new_file = fopen($dest_file_name, "w+");
				fwrite($new_file, $content);
				fclose($new_file);
				
				$dest_file_name = str_replace('./', '/', $dest_file_name);
				return '<script src="' . $dest_file_name . '"></script>';
			}else{
				$dest_file_name = str_replace('./', '/', $dest_file_name);
				return '<script src="' . $dest_file_name.'"></script>'; //output combine file
			}
		}
		*/
		
		//перенос акции в архив
		public function discountAutoArchive() {
			$discount_pid = 293;
			$discount_archive_pid = 13570;
			$hierarchy = umiHierarchy::getInstance();
			//поиск товара с заданным артикулом по всем товарам и перенос
			$sel = new selector('pages');
			$sel -> types('object-type') -> name('news', 'item');
			$sel->where('hierarchy')->page($discount_pid)->childs(8);
			$sel->where('end_time')->less(time());
			$sel->order('end_time')->asc();
			foreach ($sel as $page) {
				$element_id = $page->id;
				$hierarchy->moveBefore($element_id, $discount_archive_pid); 
			}
	       
		}
		
};
?>