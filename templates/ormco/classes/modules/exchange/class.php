<?php
class exchange_custom extends def_module {
    protected static $importDirectory = "/1c_import/";

    public function auto_new_orders() {
        $timeOut = (int) mainConfiguration::getInstance()->get("modules", "exchange.commerceML.timeout");
        if ($timeOut < 0){
            $timeOut = 0;
        }

        sleep($timeOut);

        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset('utf-8');
        $buffer->contentType('text/plain');

        $type = getRequest("type");
        $mode = getRequest("mode");
        $instance1c = getRequest('param0') ? md5(getRequest('param0')) . "/" : '';
        self::$importDirectory = SYS_TEMP_PATH . "/1c_import/" . $instance1c;

        if (!permissionsCollection::getInstance()->isSv()) {
            $buffer->push("failure\nNot authorized as supervisor.");
            $buffer->end();
            exit();
        }

        $session = \UmiCms\Service::Session();
        $sessionId = $session->getId();
        $sessionName = $session->getName();

        switch ($type . "-" . $mode) {
//            case "catalog-checkauth":
//                // clear temp
//                removeDirectory(self::$importDirectory);
            case "sale-checkauth": {
                    $buffer->push("success\n$sessionName\n" . $sessionId);
                } break;
//            case "catalog-init":
            case "sale-init": {
                    removeDirectory(self::$importDirectory);
                    $config = mainConfiguration::getInstance();
                    $maxFileSize = (int) $config->get("modules", "exchange.commerceML.maxFileSize");
                    if ($maxFileSize <= 0) {
                        $maxFileSize = 102400;
                    }

                    $isZipAcceptable = $config->get("modules", "exchange.commerceML.accept-zip");
                    $zipResponse = $isZipAcceptable ? 'yes' : 'no';

                    $buffer->push("zip={$zipResponse}\nfile_limit={$maxFileSize}");
                } break;
//            case "catalog-file": {
//                    $buffer->push(self::saveIncomingFile());
//                } break;
//            case "catalog-import" : {
//                    $buffer->push(self::importCommerceML());
//                } break;
            case "sale-query" : {
                    $buffer->push(self::exportNewOrders());
                } break;

            case "sale-success" : {
                    $buffer->push(self::markExportedNewOrders());
                } break;
            case "sale-file" : {
                    $buffer->push(self::importOrders());
                } break;
            default:
                $buffer->push("failure\nUnknown import type ($type) or mode ($mode).");
        }
        $buffer->end();
    }

    protected function exportNewOrders() {
        $exporter = umiExporter::get("ordersNewCommerceML");
        $exporter->setOutputBuffer();
        $result = $exporter->export(array(), array());
        return $result;
    }

    protected function markExportedNewOrders() {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('need_export_new')->equals(1);

        if (mainConfiguration::getInstance()->get('modules', 'exchange.commerceML.ordersByDomains')) {
            $currentDomainId = cmsController::getInstance()->getCurrentDomain()->getId();
            $sel->where('domain_id')->equals($currentDomainId);
        }

        $orders = $sel->result;
        foreach ($orders as $order) {
            $order->setValue('need_export_new', 0);
            $order->commit();
        }

        return "success";
    }
    
    /**
     * Подготавливаем данные для количества, которое почему-то может быть указано с пробелом
     * @param type $original_items_count
     * @return type
     */
    public function prepare_wrong_items_count($original_items_count = null){
		if($original_items_count === null) {
            $original_items_count = getRequest('param0');
        }

        $result = preg_replace('/\s/', '', $original_items_count);

        return empty($result) ? 0 : $result;
    }
}