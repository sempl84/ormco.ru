<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xsl:extension-element-prefixes="php"
		exclude-result-prefixes="php">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="/udata">
		<xsl:apply-templates mode="print-reciept" />
	</xsl:template>

	<xsl:template match="/udata[@module='emarket' and @method='order']">
		<xsl:apply-templates select="document(concat('uobject://',/udata/@id))/udata/object" mode="print-reciept" />
	</xsl:template>

	<xsl:template match="/udata/object" mode="print-reciept">
		<xsl:variable name="payment" select="document(concat('uobject://',//property[@name='payment_id']/value/item/@id))/udata/object/properties" />
		<xsl:variable name="price" select="//property[@name='total_price']/value" />
		<xsl:variable name="customer" select="document(concat('uobject://',//property[@name='customer_id']/value/item/@id))/udata/object" />
		<!-- <xsl:variable name="name-string" select="concat($customer//property[@name='lname']/value, ' ', $customer//property[@name='fname']/value, ' ', $customer//property[@name='father_name']/value)" /> -->
		<xsl:variable name="name-string" select="concat($customer//property[@name='lname']/value, ' ', $customer//property[@name='fname']/value, ' ', $customer//property[@name='father_name']/value)" />
		<xsl:variable name="address"  select="document(concat('uobject://',//property[@name='delivery_address']/value/item[1]/@id))/udata/object" />
		<xsl:variable name="address-string">
			<xsl:choose>
				<xsl:when test="$address">
					 <xsl:value-of select="concat($address//property[@name='index']/value,', ',$address//property[@name='region']/value,', ',$address//property[@name='city']/value,', ',$address//property[@name='street']/value,', д. ',$address//property[@name='house']/value,', кв.',$address//property[@name='flat']/value)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select='""' />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<html>
			<head id="header">
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Квитанция для физических лиц</title>
				
				
			</head>
			<body id="receipt" style="font-size: 13px;">
				<div style="width: 180mm; font-size:12px; ">
					По данной квитанции Вы сможете произвести оплату в Сбербанке или любом другом банке, обслуживающем физических лиц
					<br />
				</div>
				
				
				
				<table border="0" style="width: 180mm; height: 145mm; border-collapse: collapse; empty-cells: show; font-family: Arial; border-spacing: 0; padding: 0;" class="table">
					<tr style="height: 70mm; font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
						<td class="center border width_50 strong" style="font-size: 11px; border-spacing: 0; text-align: center; font-weight: bold; width: 50mm; margin: 0;  border: 1px solid #000;" align="center">
							<div style="width:100%; font-size:2px; line-height:2px ;">&#160;</div>							
							<div>Извещение</div>
							<div style="width:100%; font-size:140px; line-height:140px ;">&#160;</div>
							<div>Кассир</div>
						</td>
						<td style="width:8px; font-size: 11px; border-spacing: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff; border-left: 1px solid #000; ">
						</td>
						<td class="border" style="width:461px; border-spacing: 0; margin: 0; padding: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000; border-left: 1px solid #fff;">
							<div style="width:100%; font-size:5px; line-height:5px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right" style="font-size: 9px; border-spacing: 0; text-align: right; margin: 0; " align="right"><i class="small" style="font-size: 9px; margin: 0; padding: 0; font-style: italic;">Форма № ПД-4</i></td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="underline" style="font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; width:447px">
<xsl:value-of select="$payment//property[@name='reciever']/value"  /> </td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование получателя платежа)</td></tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width:139px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_inn']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
									<td style="width:34px; font-size: 11px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0;  width:275px;">
<xsl:value-of select="$payment//property[@name='reciever_account']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;  width:139px;" align="center">(ИНН получателя платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0;   width:34px;"> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;   width:270px;" align="center">(номер счета получателя платежа)</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table  class="margin width_122" style="font-size: 0; border-spacing: 0; width: 461px; margin: 4px 0 0; padding: 0;">
								<tr >
									<td class="right" style=" width:13px; font-size: 11px; border-spacing: 0;  margin: 10px 0 0; " >в </td>
									<td  style="width:275px; font-size: 9px; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_bank']/value" /> </td>
									<td class="right" style=" width:45px; font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right">БИК </td>
									<td class="underline" style=" width: 114px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
&#160;<xsl:value-of select="$payment//property[@name='bik']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование банка получателя платежа)</td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:250px; font-size: 11px; border-spacing: 0;  margin: 0; white-space: nowrap;">Номер кор./сч. банка получателя платежа  </td>
									<td class="underline" style="width:200px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_bank_account']/value" /></td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width: 60mm; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:text style="font-size: 0; margin: 0; padding: 0;">Счет № </xsl:text><span id="py-order-name-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:text>И/</xsl:text><xsl:value-of select="@id"/><xsl:text>/Ф</xsl:text></span><xsl:text > от </xsl:text><xsl:value-of select="php:function('date', 'd.m.Y')"/>
</td>
									<td style="width: 7px; "> </td>
									<td style="width: 230px;border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid;"> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; "> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(номер лицевого счета (код) плательщика)</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:140px; font-size: 11px; border-spacing: 0; white-space: nowrap; margin: 0; ">Ф.И.О. плательщика  </td>
									<td style="width:310px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><span id="py-fio-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:value-of select="$name-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:140px; font-size: 11px; border-spacing: 0; white-space: nowrap; margin: 0; ">Адрес плательщика  </td>
									<td style="width:310px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><span id="py-address-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:value-of select="$address-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="width:195px; font-size: 11px; border-spacing: 0; margin: 0; ">Сумма платежа <span class="u" id="py-order-price-1" style="font-size: 11px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="floor($price)"/></span> руб. <span class="u" style="font-size: 11px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="round((number($price)-floor($price))*100)" /> </span> коп.</td>
									<td class="right" style="width:259px;font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right"> Сумма платы за услуги <span style="font-size: 8px; ">______</span> руб. <span style="font-size: 8px; ">______</span> коп.</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 11px; border-spacing: 0; margin: 0; ">Итого  <span style="font-size: 8px; ">____________</span> руб. <span style="font-size: 8px; ">______</span> коп.</td>
									<td class="right" style="width:230px; font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right">«<span style="font-size: 8px; ">____</span>»<span style="font-size: 8px; ">____________</span> 201<span style="font-size: 8px; ">___</span> г.</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; ">
										С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.
									</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 450px; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right strong" style="font-size: 11px; border-spacing: 0; text-align: right; font-weight: bold; margin: 0; " align="right">Подпись плательщика <span style="font-size: 8px; ">________________________</span></td>
								</tr>
							</table>
							<div style="width:100%; font-size:5px; line-height:5px ;">&#160;</div>
						</td>
					</tr>
					<tr style="height: 70mm; font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
						<td class="center border width_50 strong" style="font-size: 11px; border-spacing: 0; text-align: center; font-weight: bold; width: 50mm; margin: 0;  border: 1px solid #000;" align="center">
							<div style="width:100%; font-size:2px; line-height:2px ;">&#160;</div>							
							<div>Извещение</div>
							<div style="width:100%; font-size:140px; line-height:140px ;">&#160;</div>
							<div>Кассир</div>
						</td>
						<td style="width:8px; font-size: 11px; border-spacing: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff; border-left: 1px solid #000; ">
						</td>
						<td class="border" style="width:461px; border-spacing: 0; margin: 0; padding: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000; border-left: 1px solid #fff;">
							<div style="width:100%; font-size:5px; line-height:5px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right" style="font-size: 9px; border-spacing: 0; text-align: right; margin: 0; " align="right"><i class="small" style="font-size: 9px; margin: 0; padding: 0; font-style: italic;">Форма № ПД-4</i></td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="underline" style="font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; width:447px">
<xsl:value-of select="$payment//property[@name='reciever']/value"  /> </td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование получателя платежа)</td></tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width:139px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_inn']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
									<td style="width:34px; font-size: 11px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0;  width:275px;">
<xsl:value-of select="$payment//property[@name='reciever_account']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;  width:139px;" align="center">(ИНН получателя платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0;   width:34px;"> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;   width:270px;" align="center">(номер счета получателя платежа)</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table  class="margin width_122" style="font-size: 0; border-spacing: 0; width: 461px; margin: 4px 0 0; padding: 0;">
								<tr >
									<td class="right" style=" width:13px; font-size: 11px; border-spacing: 0;  margin: 10px 0 0; " >в </td>
									<td  style="width:275px; font-size: 9px; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_bank']/value" /> </td>
									<td class="right" style=" width:45px; font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right">БИК </td>
									<td class="underline" style=" width: 114px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
&#160;<xsl:value-of select="$payment//property[@name='bik']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование банка получателя платежа)</td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:250px; font-size: 11px; border-spacing: 0;  margin: 0; white-space: nowrap;">Номер кор./сч. банка получателя платежа  </td>
									<td class="underline" style="width:200px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_bank_account']/value" /></td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width: 60mm; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:text style="font-size: 0; margin: 0; padding: 0;">Счет № </xsl:text><span id="py-order-name-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:text>И/</xsl:text><xsl:value-of select="@id"/><xsl:text>/Ф</xsl:text></span><xsl:text > от </xsl:text><xsl:value-of select="php:function('date', 'd.m.Y')"/>
</td>
									<td style="width: 7px; "> </td>
									<td style="width: 230px;border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid;"> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; "> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(номер лицевого счета (код) плательщика)</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:140px; font-size: 11px; border-spacing: 0; white-space: nowrap; margin: 0; ">Ф.И.О. плательщика  </td>
									<td style="width:310px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><span id="py-fio-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:value-of select="$name-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="wrap" style="width:140px; font-size: 11px; border-spacing: 0; white-space: nowrap; margin: 0; ">Адрес плательщика  </td>
									<td style="width:310px; font-size: 11px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><span id="py-address-1" style="font-size: 11px; margin: 0; padding: 0;"><xsl:value-of select="$address-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="width:195px; font-size: 11px; border-spacing: 0; margin: 0; ">Сумма платежа <span class="u" id="py-order-price-1" style="font-size: 11px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="floor($price)"/></span> руб. <span class="u" style="font-size: 11px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="round((number($price)-floor($price))*100)" /> </span> коп.</td>
									<td class="right" style="width:259px;font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right"> Сумма платы за услуги <span style="font-size: 8px; ">______</span> руб. <span style="font-size: 8px; ">______</span> коп.</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 11px; border-spacing: 0; margin: 0; ">Итого  <span style="font-size: 8px; ">____________</span> руб. <span style="font-size: 8px; ">______</span> коп.</td>
									<td class="right" style="width:230px; font-size: 11px; border-spacing: 0; text-align: right; margin: 0; " align="right">«<span style="font-size: 8px; ">____</span>»<span style="font-size: 8px; ">____________</span> 201<span style="font-size: 8px; ">___</span> г.</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; ">
										С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.
									</td>
								</tr>
							</table>
							<div style="width:100%; font-size:10px; line-height:10px ;">&#160;</div>
							
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 450px; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right strong" style="font-size: 11px; border-spacing: 0; text-align: right; font-weight: bold; margin: 0; " align="right">Подпись плательщика <span style="font-size: 8px; ">________________________</span></td>
								</tr>
							</table>
							<div style="width:100%; font-size:5px; line-height:5px ;">&#160;</div>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>