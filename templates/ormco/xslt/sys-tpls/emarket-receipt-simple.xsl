<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xsl:extension-element-prefixes="php"
		exclude-result-prefixes="php">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="/udata">
		<xsl:apply-templates mode="print-reciept" />
	</xsl:template>

	<xsl:template match="/udata[@module='emarket' and @method='order']">
		<xsl:apply-templates select="document(concat('uobject://',/udata/@id))/udata/object" mode="print-reciept" />
	</xsl:template>

	<xsl:template match="/udata/object" mode="print-reciept">
		<xsl:variable name="payment" select="document(concat('uobject://',//property[@name='payment_id']/value/item/@id))/udata/object/properties" />
		<xsl:variable name="price" select="//property[@name='total_price']/value" />
		<xsl:variable name="customer" select="document(concat('uobject://',//property[@name='customer_id']/value/item/@id))/udata/object" />
		<!-- <xsl:variable name="name-string" select="concat($customer//property[@name='lname']/value, ' ', $customer//property[@name='fname']/value, ' ', $customer//property[@name='father_name']/value)" /> -->
		<xsl:variable name="name-string" select="concat($customer//property[@name='lname']/value, ' ', $customer//property[@name='fname']/value, ' ', $customer//property[@name='father_name']/value)" />
		<xsl:variable name="address"  select="document(concat('uobject://',//property[@name='delivery_address']/value/item[1]/@id))/udata/object" />
		<xsl:variable name="address-string">
			<xsl:choose>
				<xsl:when test="$address">
					 <xsl:value-of select="concat($address//property[@name='index']/value,', ',$address//property[@name='region']/value,', ',$address//property[@name='city']/value,', ',$address//property[@name='street']/value,', д. ',$address//property[@name='house']/value,', кв.',$address//property[@name='flat']/value)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select='""' />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<html>
			<head id="header">
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Квитанция для физических лиц</title>
				
				
			</head>
			<body id="receipt" style="font-size: 13px;">
				<div style="width: 180mm;">
					Для оплаты выбранных товаров, пожалуйста, распечатайте данную квитанцию <a href="#" onclick="window.print();">Печать</a>.<br />
					Вы сможете произвести по ней оплату в Сбербанке или любом другом банке, обслуживающем физических лиц.<br />
					
					<hr />
				</div>
				<table border="0" style="width: 180mm; height: 145mm; border-collapse: collapse; empty-cells: show; font-family: Arial; border-spacing: 0; padding: 0;" class="table">
					<tr style="height: 70mm; font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
						<td class="center border width_50 strong" style="font-size: 12px; border-spacing: 0; text-align: center; font-weight: bold; width: 50mm; margin: 0;  border: 1px solid #000;" align="center">
							Извещение<div style="height: 53mm; font-size: 0; margin: 0; padding: 0;"></div>Кассир
						</td>
						<td style="width:8px; font-size: 12px; border-spacing: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff; border-left: 1px solid #000; ">
						</td>
						<td class="border" style="width:461px; font-size: 12px; border-spacing: 0; margin: 0; padding: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000; border-left: 1px solid #fff;">
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right"><i class="small" style="font-size: 9px; margin: 0; padding: 0;">Форма № ПД-4</i></td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever']/value"  /> </td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование получателя платежа)</td></tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width:139px; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_inn']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
									<td style="width:34px; font-size: 12px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0;  width:276px;">
<xsl:value-of select="$payment//property[@name='reciever_account']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;  width:139px;" align="center">(ИНН получателя платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0;   width:34px;"> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0;   width:276px;" align="center">(номер счета получателя платежа)</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 461px; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="vartical-align:bottom; font-size: 12px; border-spacing: 0; margin: 0; width:13px">в </td>
									<td class="small underline" style="vartical-align:bottom;  width:275px; font-size: 9px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_bank']/value" style=" font-size: 0; margin: 0; padding: 0;"/> </td>
									<td class="right" style="vartical-align:bottom; width:45px; font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">БИК </td>
									<td class="underline" style="vartical-align:bottom; width: 124px; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
&#160;<xsl:value-of select="$payment//property[@name='bik']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование банка получателя платежа)</td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
									<td style="font-size: 9px; border-spacing: 0; margin: 0; "></td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Номер кор./сч. банка получателя платежа  </td>
									<td width="100%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_bank_account']/value" style="font-size: 0; margin: 0; padding: 0;"/></td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width: 60mm; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:text style="font-size: 0; margin: 0; padding: 0;">Счет № </xsl:text><span id="py-order-name-1" style="font-size: 12px; margin: 0; padding: 0;"><xsl:text style="font-size: 0; margin: 0; padding: 0;">И/</xsl:text><xsl:value-of select="@id" style="font-size: 0; margin: 0; padding: 0;"/><xsl:text style="font-size: 0; margin: 0; padding: 0;">/Ф</xsl:text></span><xsl:text style="font-size: 0; margin: 0; padding: 0;"> от </xsl:text><xsl:value-of select="php:function('date', 'd.m.Y')" style="font-size: 0; margin: 0; padding: 0;"/>
</td>
									<td style="width: 2mm; font-size: 12px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; "> </td>
									<td class="small center" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(номер лицевого счета (код) плательщика)</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Ф.И.О. плательщика  </td>
									<td width="99%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">  <span id="py-fio-1" style="font-size: 12px; margin: 0; padding: 0;"><xsl:value-of select="$name-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Адрес плательщика  </td>
									<td width="99%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">  <span id="py-address-1" style="font-size: 12px; margin: 0; padding: 0;"><xsl:value-of select="$address-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; ">Сумма платежа  <span class="u" id="py-order-price-1" style="font-size: 12px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="floor($price)" style="font-size: 0; margin: 0; padding: 0;"/></span> руб. <span class="u" style="font-size: 12px; text-decoration: underline; margin: 0 2px; padding: 0;"> <xsl:value-of select="round((number($price)-floor($price))*100)" style="font-size: 0; margin: 0; padding: 0;"/> </span> коп.</td>
									<td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">  Сумма платы за услуги  _____ руб. ____ коп.</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; ">Итого  _______ руб. ____ коп.</td>
									<td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">  «______»________________ 201____ г.</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; ">
										С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.
									</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right strong" style="font-size: 12px; border-spacing: 0; text-align: right; font-weight: bold; margin: 0; " align="right">Подпись плательщика _____________________</td>
								</tr>
							</table>
						</td>
					</tr>
					
					
					
					
					
					
					
					
					
					<tr valign="top" style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
						<td class="width_50 border center strong" style="height: 70mm; font-size: 12px; border-spacing: 0; text-align: center; font-weight: bold; width: 50mm; margin: 0;  border: 1px solid #000;" align="center">
							<div style="height: 58mm; font-size: 0; margin: 0; padding: 0;"></div>Квитанция<div style="height: 11px; font-size: 0; margin: 0; padding: 0;"></div>Кассир
						</td>
						<td style="width:8px; font-size: 12px; border-spacing: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff; border-left: 1px solid #000; ">
						</td>
						<td class="border" style="width:461px; font-size: 12px; border-spacing: 0; margin: 0; padding: 0; border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000; border-left: 1px solid #fff;">
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="right small" style="font-size: 9px; border-spacing: 0; text-align: right; margin: 0; " align="right"> </td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td></tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;"><td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование получателя платежа)</td></tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width: 37mm; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_inn']/value" style="font-size: 0; margin: 0; padding: 0;"/></td>
									<td style="width: 9mm; font-size: 12px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_account']/value" style="font-size: 0; margin: 0; padding: 0;"/></td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(ИНН получателя платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; "> </td>
									<td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(номер счета получателя платежа)</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; ">в </td>
									<td class="underline small" style="width: 73mm; font-size: 9px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='reciever_bank']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
									<td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">БИК  </td>
									<td class="underline" style="width: 33mm; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:value-of select="$payment//property[@name='bik']/value" style="font-size: 0; margin: 0; padding: 0;"/> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; "></td>
									<td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование банка получателя платежа)</td>
									<td style="font-size: 12px; border-spacing: 0; margin: 0; "></td>
									<td style="font-size: 12px; border-spacing: 0; margin: 0; "></td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Номер кор./сч. банка получателя платежа  </td>
									<td width="100%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "><xsl:value-of select="$payment//property[@name='reciever_bank_account']/value"  /></td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="underline" style="width: 60mm; font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">
<xsl:text style="font-size: 0; margin: 0; padding: 0;">Счет № </xsl:text><span id="py-order-name-2" style="font-size: 12px; margin: 0; padding: 0;"><xsl:text style="font-size: 0; margin: 0; padding: 0;">И/</xsl:text><xsl:value-of select="@id" style="font-size: 0; margin: 0; padding: 0;"/><xsl:text style="font-size: 0; margin: 0; padding: 0;">/Ф</xsl:text></span><xsl:text style="font-size: 0; margin: 0; padding: 0;"> от </xsl:text><xsl:value-of select="php:function('date', 'd.m.Y')" style="font-size: 0; margin: 0; padding: 0;"/>
</td>
									<td style="width: 2mm; font-size: 12px; border-spacing: 0; margin: 0; "> </td>
									<td class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; "> </td>
								</tr>
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(наименование платежа)</td>
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; "> </td>
									<td class="center small" style="font-size: 9px; border-spacing: 0; text-align: center; margin: 0; " align="center">(номер лицевого счета (код) плательщика)</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Ф.И.О. плательщика  </td>
									<td width="99%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">  <span id="py-fio-2" style="font-size: 12px; margin: 0; padding: 0;"><xsl:value-of select="$name-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td width="1%" class="wrap" style="font-size: 12px; border-spacing: 0; white-space: nowrap; margin: 0; ">Адрес плательщика  </td>
									<td width="99%" class="underline" style="font-size: 12px; border-spacing: 0; border-bottom-width: 1px; border-bottom-color: #000; border-bottom-style: solid; margin: 0; ">  <span id="py-address-2" style="font-size: 12px; margin: 0; padding: 0;"><xsl:value-of select="$address-string" style="font-size: 0; margin: 0; padding: 0;"/></span>  </td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; ">Сумма платежа <span class="u" id="py-order-price-2" style="font-size: 12px; text-decoration: underline; margin: 0 2px; padding: 0;"><xsl:value-of select="floor($price)" style="font-size: 0; margin: 0; padding: 0;"/></span> руб. <span class="u" style="font-size: 12px; text-decoration: underline; margin: 0 2px; padding: 0;"> <xsl:value-of select="round((number($price)-floor($price))*100)" style="font-size: 0; margin: 0; padding: 0;"/> </span> коп.</td>
									<td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">Сумма платы за услуги  _____ руб. ____ коп.</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td style="font-size: 12px; border-spacing: 0; margin: 0; ">Итого  _______ руб. ____ коп.</td>
									<td class="right" style="font-size: 12px; border-spacing: 0; text-align: right; margin: 0; " align="right">  «______»________________ 201____ г.</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="small" style="font-size: 9px; border-spacing: 0; margin: 0; ">
										С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.
									</td>
								</tr>
							</table>
							<table border="0" class="margin width_122" style="font-size: 0; border-spacing: 0; width: 122mm; margin: 4px 0 0; padding: 0;">
								<tr style="font-size: 0; border-spacing: 0; margin: 0; padding: 0;">
									<td class="right strong" style="font-size: 12px; border-spacing: 0; text-align: right; font-weight: bold; margin: 0; " align="right">Подпись плательщика _____________________</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>