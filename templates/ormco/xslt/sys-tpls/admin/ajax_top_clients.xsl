<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xsl:extension-element-prefixes="php"
		exclude-result-prefixes="php">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>
 	<xsl:decimal-format name="price" decimal-separator="." grouping-separator="&#160;"/>

	<xsl:template match="/">
		<h3 style="text-align:center;">
			Даты с
			<xsl:value-of select="udata/start_date" />
			по
			<xsl:value-of select="udata/end_date" />
		</h3>
		<h3 style="text-align:center;">
			Всего проанализировано заказов
			<xsl:value-of select="udata/orders/total" />
			на общую сумму
			<xsl:value-of select="format-number(sum(udata/orders/items/item/@price), '#&#160;###&#160;###.00', 'price')" />
			руб.
		</h3>
		<br />

		<xsl:apply-templates select="udata/top_price" mode="top_price" />

		<xsl:apply-templates select="udata/top_count" mode="top_count" />
	</xsl:template>

	<xsl:template match="top_price" mode="top_price" />
	<xsl:template match="top_price[items/item]" mode="top_price">
		<h1 style="text-align:center;">Лучшие клиенты по стоимости заказа</h1>

		<xsl:apply-templates select="items" mode="clients">
			<xsl:with-param name="class" select="'top_price'" />
		</xsl:apply-templates>
		<a href="#" class="export_to_csv" data-class="top_price">Выгрузить в CSV</a>
	</xsl:template>

	<xsl:template match="top_count" mode="top_count" />
	<xsl:template match="top_count[items/item]" mode="top_count">
		<h1 style="text-align:center;">Лучшие клиенты по количеству заказов</h1>

		<xsl:apply-templates select="items" mode="clients">
			<xsl:with-param name="class" select="'top_clients'" />
		</xsl:apply-templates>
		<a href="#" class="export_to_csv" data-class="top_clients">Выгрузить в CSV</a>
	</xsl:template>

	<xsl:template match="items" mode="clients">
		<xsl:param name="class" />

		<table class="tableContent btable btable-bordered btable-striped {$class}">
			<tr>
				<th>#</th>
				<th>ID</th>
				<th>Имя</th>
				<th>Заказов, шт</th>
				<th>Общая стоимость</th>
				<th>email</th>
				<th>Телефон</th>
				<th>Страна</th>
				<th>Регион</th>
			</tr>
			<xsl:apply-templates select="item" mode="clients" />
		</table>
	</xsl:template>

	<xsl:template match="item" mode="clients">
		<xsl:variable name="client" select="document(concat('uobject://', @id))" />
		<tr>
			<td><xsl:value-of select="position()" disable-output-escaping="yes" /></td>
			<td style="text-align:center"><a href="/admin/users/edit/{@id}/" target="_blank" title="Перейти к редактированию клиента"><xsl:value-of select="@id" disable-output-escaping="yes" /></a></td>
			<td>
				<xsl:value-of select="$client//property[@name = 'lname']/value" disable-output-escaping="yes" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="$client//property[@name = 'fname']/value" disable-output-escaping="yes" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="$client//property[@name = 'father_name']/value" disable-output-escaping="yes" />
			</td>
			<td style="text-align:center"><xsl:value-of select="@count" disable-output-escaping="yes" /></td>
			<td style="text-align:center"><xsl:value-of select="format-number(@price, '#&#160;###&#160;###.00', 'price')" /></td>
			<td style="text-align:center"><xsl:value-of select="$client//property[@name = 'e-mail']/value" disable-output-escaping="yes" /></td>
			<td style="text-align:center"><xsl:value-of select="$client//property[@name = 'phone']/value" disable-output-escaping="yes" /></td>
			<td style="text-align:center"><xsl:value-of select="$client//property[@name = 'country']/value/item/@name" disable-output-escaping="yes" /></td>
			<td style="text-align:center">
				<xsl:value-of select="$client//property[@name = 'region']/value/item/@name" disable-output-escaping="yes" />
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>