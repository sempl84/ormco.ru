<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xsl:extension-element-prefixes="php"
		exclude-result-prefixes="php">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>

	<xsl:template match="/">
		<xsl:variable name="payment" select="document(concat('uobject://', //property[@name='payment_id']/value/item/@id))/udata/object" />
		<xsl:variable name="person" select="document(concat('uobject://', //property[@name='legal_person']/value/item/@id))/udata/object" />
		<xsl:variable name="nds" select="document(concat('udata://emarket/ndsSize/', /udata/object/@id))/udata" />

		<html>
			<head>
			<!-- <style type="text/css">
				table td td{
					border: 1px solid black;
				}
				.table_caption {
					text-align: center;
					margin: 0;
					font-size: 13px;
				}
			</style> -->
			<style type="text/css" media="print">
				@page {
					size: auto;
					margin: 7mm;
				}
			</style>
			</head>
			<body id="invoice">
			<div style="width:620px;"><!--hr/--></div>

			<table bgcolor="#FFFFFF" width="620" cellpadding="1" cellspacing="0">
				<tr>
					<td align="left" valign="top" style="font-family:Arial;font-size:13px;">
						<u><b><xsl:value-of select="$payment//property[@name='name']/value"/></b></u>
						<br/><br/>
						<b>Адрес: <xsl:value-of select="$payment//property[@name='legal_address']/value"/>, тел.: <xsl:value-of select="$payment//property[@name='phone_number']/value"/></b>
						<br/><br/>
						<h4 class="table_caption" style="text-align: center;margin: 0;font-size: 13px">Образец заполнения платежного поручения</h4>
						<table class="tbl" width="620" cellpadding="2" cellspacing="0" border="0" bordercolor="#000000" style="font-family:Arial;font-size:13px;">
							<tr>
								<td width="175" align="left" valign="top" style="border: 1px solid black">ИНН <xsl:value-of select="$payment//property[@name='inn']/value"/></td>
								<td width="175" align="left" valign="top" style="border: 1px solid black">КПП <xsl:value-of select="$payment//property[@name='kpp']/value"/></td>
								<td width="54" align="center" valign="bottom" rowspan="2" style="border: 1px solid black">Сч. №</td>
								<td width="216" align="left" valign="bottom" rowspan="2" style="border: 1px solid black"><xsl:value-of select="$payment//property[@name='account']/value"/></td>
							</tr>
							<tr>
								<td width="350" align="left" valign="top" colspan="2" style="border: 1px solid black">
									Получатель
									<br/>
									<xsl:value-of select="$payment//property[@name='name']/value"/>
								</td>
							</tr>
							<tr>
								<td align="left" valign="top" rowspan="2" colspan="2" style="border: 1px solid black">
									Банк получателя
									<br/>
									<xsl:value-of select="$payment//property[@name='bank']/value"/>

								</td>
								<td align="center" valign="top" style="border: 1px solid black">БИК</td>
								<td align="left" valign="top" style="border-bottom-width: 0;border: 1px solid black"><xsl:value-of select="$payment//property[@name='bik']/value"/></td>
							</tr>
							<tr>
								<td align="center" valign="top" style="border: 1px solid black">Сч. №</td>
								<td align="left" valign="top" style="border-top-width: 0;border: 1px solid black"><xsl:value-of select="$payment//property[@name='bank_account']/value"/></td>
							</tr>
						</table>
						<div style="font-size:40px; line-height:40px;"></div>
						<h3 style="font-size:14px; text-align:center"><strong>СЧЕТ № <xsl:value-of select="document(concat('udata://custom/buhgalt_order_number/',/udata/object/@id))/udata"/> от <xsl:value-of select="php:function('dateToString', number(//property[@name='order_date']/value/@unix-timestamp))"/>.</strong></h3>

						<p>
							<text>Покупатель: ИНН </text><xsl:value-of select="$person//property[@name='inn']/value"/>
							<text>, КПП </text><xsl:value-of select="$person//property[@name='kpp']/value"/>
							<xsl:if test="$person//property[@name='name']/value">
							<text>, </text><xsl:value-of select="$person//property[@name='name']/value"/>
							</xsl:if>
							<xsl:if test="$person//property[@name='legal_address']/value">
							<text>, </text><xsl:value-of select="$person//property[@name='legal_address']/value"/>
							</xsl:if>
							<xsl:if test="$person//property[@name='phone_number']/value">
							<text>, тел: </text><xsl:value-of select="$person//property[@name='phone_number']/value"/>
							</xsl:if>
							<xsl:if test="$person//property[@name='fax']/value">
							<text>, факс: </text><xsl:value-of select="$person//property[@name='fax']/value"/>
							</xsl:if>
						</p>
						<table class="tbl" width="620" cellpadding="3" cellspacing="0" border="0" bordercolor="#000000" style="font-family:Arial;font-size:13px;">
							<tr>
								<td width="40" align="center" valign="middle" style="border: 1px solid black">№</td>
								<td width="280" align="center" valign="middle" style="border: 1px solid black">Наименование товара</td>
								<td width="65" align="center" valign="middle" style="border: 1px solid black">Кол-во</td>
								<td width="65" align="center" valign="middle" style="border: 1px solid black">Единица измерения</td>
								<td width="85" align="center" valign="middle" style="border: 1px solid black">Цена</td>
								<td width="85" align="center" valign="middle" style="border: 1px solid black">Сумма</td>
							</tr>

							<xsl:apply-templates select="//property[@name='order_items']/value/item" mode="order-items" />

							<xsl:variable name="total_original_price" select="//property[@name='total_original_price']/value"/>
							<xsl:variable name="total_price" select="//property[@name='total_price']/value"/>
							<xsl:variable name="delivery" select="//property[@name='delivery_price']/value"/>
							<xsl:variable name="discount" select="$total_original_price + $delivery - $total_price"/>

							<xsl:if test="$discount &gt; 0">
							<tr>
									<td align="right" valign="top" colspan="5" style="border: 1px solid black"><b>Скидка:</b></td>
									<td align="right" valign="top" style="border: 1px solid black">
											<xsl:value-of select="format-number($discount, '#.00')"/>
									</td>
								</tr>
							</xsl:if>

							<apply-templates select="//property[@name='delivery_price']/value" mode="delivery"/>

							<tr border="0" >
								<td align="right" valign="top" colspan="5" border="0"  style="border-left: none;border-bottom: none;border-top: none;border: 1px solid black"><b>Итого:</b></td>
								<td align="right" valign="bottom" style="border: 1px solid black"><xsl:value-of select="format-number(//property[@name='total_price']/value, '#.00')"/></td>
							</tr>


							<tr>
								<td colspan="5" align="right" valign="bottom" style="border-left: none;border-bottom: none;border-top: none;border: 1px solid black">

										<b>В том числе НДС:</b>


								</td>
								<td align="right" valign="bottom" style="border: 1px solid black">
									<xsl:if test="$nds &gt; 0">
										<xsl:value-of select="format-number($nds, '#.00')"/>
									</xsl:if>
									<!-- <xsl:variable name="nds" select=" (//property[@name='total_price']/value * 20) div 120"/>
									<xsl:variable name="nds_round" select="round($nds * 100) div 100"/>
									<xsl:value-of select="format-number($nds_round, '#.00')"/> -->
								</td>
							</tr>
							<tr>
								<td colspan="5" align="right" valign="bottom" style="border-left: none;border-bottom: none;border-top: none;border: 1px solid black"><b>Всего к оплате:</b></td>
								<td align="right" valign="bottom" style="border: 1px solid black"><xsl:value-of select="format-number(//property[@name='total_price']/value, '#.00')"/></td>
							</tr>
						</table>
						<br/><br/>
						<p style="font-family:Arial;font-size:13px;">
							Всего наименований <xsl:value-of select="//property[@name='total_amount']/value"/>, на сумму <xsl:value-of select="format-number(//property[@name='total_price']/value, '0.00')"/> руб.
							<br/>
							<b>(<xsl:value-of select="php:function('sumToString', number(//property[@name='total_price']/value), 1, 'рубль', 'рубля', 'рублей', 0)"/>)</b>
						</p>
						<table border="0">
							<tr>
								<td colspan="2">
									<xsl:if test="$payment//property[@name='sign_image']/value">
										<!-- <div style="text-align:right; margin-right:100px; position:relative; bottom:-40px">
											<img src="https://ormco.ru{$payment//property[@name='sign_image']/value}" style="text-align:right;"/>
										</div>  -->
										<table border="0" width="620">
											<tr>
												<td width="200"></td>
												<td>
													<img src="https://ormco.ru{$payment//property[@name='sign_image']/value}" style="text-align:right;"/>
												</td>
											</tr>
										</table>
									</xsl:if>
								</td>
							</tr>
							<tr>
								<td height="25px">
									<div>Генеральный директор</div>
								</td>
								<td height="25px" style="text-align:right;">
									<!-- <div style="text-align:right;">____________________ Машарина Е.Г.</div>   -->
									<div style="text-align:right;">____________________ Шавырин А.А.</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

			</body>
		</html>
	</xsl:template>

	<xsl:template match="item" mode="order-items">
		<xsl:variable name="object" select="document(concat('uobject://', @id))/udata/object" />

		<tr>
			<td width="40" align="center" valign="top" style="white-space:nowrap; border: 1px solid black"><xsl:value-of select="position()"/></td>
			<td width="280" align="left" valign="middle" style="border: 1px solid black"><xsl:value-of select="$object/@name"/></td>
			<td width="65" align="right" valign="bottom" style="border: 1px solid black"><xsl:value-of select="$object//property[@name='item_amount']/value"/></td>
			<td width="65" align="center" valign="bottom" style="border: 1px solid black">шт.</td>
			<td width="85" align="right" valign="bottom" style="border: 1px solid black"><xsl:value-of select="format-number($object//property[@name='item_price']/value, '#.00')"/></td>
			<td width="85" align="right" valign="bottom" style="border: 1px solid black"><xsl:value-of select="format-number($object//property[@name='item_total_price']/value, '#.00')"/></td>
		</tr>
	</xsl:template>

	<xsl:template match="value[.='0']" mode="delivery" />
	<xsl:template match="value" mode="delivery">
		<tr>
			<td align="right" valign="bottom" colspan="5" style="border-left: 0; border-bottom: 0"><b>Доставка:</b></td>
			<td align="right" valign="bottom">
					<xsl:value-of select="format-number(., '#.00')" />
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>