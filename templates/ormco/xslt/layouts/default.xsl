<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl umi">

    <xsl:template match="/" mode="layout">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html lang="ru">
            <head>
                <title>
                    <xsl:value-of select="$document-title" />
                </title>
                <xsl:if test="//meta/description and not(//meta/description='')">
                    <meta name="description" content="{//meta/description}" />
                </xsl:if>
                <xsl:if test="//meta/keywords and not(//meta/keywords='')">
                    <meta name="keywords" content="{//meta/keywords}" />
                </xsl:if>

                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />

                <meta property="og:title" content="{result/@header}"/>
                <meta property="og:description" content="{//meta/description}"/>
                <meta property="og:image" content="{concat('https://', $domain, '/templates/ormco/img/ormco-og.jpg')}"/>
                <meta property="og:image:width" content="594"/>
                <meta property="og:image:height" content="241"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content= "{concat('https://', $domain, result/@request-uri)}" />

				<link rel="preload" href="/templates/ormco/fonts/fontawesome-webfont.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/ormco/fonts/opensans-italic.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/ormco/fonts/opensans-regular.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/ormco/fonts/opensans-semibold.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/ormco/fonts/alsrubl.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/ormco/fonts/glyphicons-halflings-regular.woff2" as="font" crossorigin="" />

                <link href="/min/index.php?f={$template-resources}css/combine.css,{$template-resources}css/custom.css,{$template-resources}css/photoswipe.css,{$template-resources}css/default-skin.css,{$template-resources}js/fancybox/jquery.fancybox.min.css,{$template-resources}css/blueimp-gallery.min.css,{$template-resources}css/search-clinic.css,{$template-resources}css/datepicker.min.css,/templates/ormco/js/easy_autocomplete/easy-autocomplete.min.css,/templates/ormco/js/easy_autocomplete/easy-autocomplete.themes.min.css,{$template-resources}css/coupons.css" rel="stylesheet" />

                <!-- маркетинговые материалы -->
                <xsl:if test="result[@module = 'materials']">
                    <link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet" />
                </xsl:if>

                <script src="/min/index.php?f={$template-resources}js/jquery-2.2.4.min.js,/js/jquery/jquery.cookie.js"></script>

                <link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />

                <link rel="canonical" href="https://{concat(result/@domain, result/@request-uri)}" />

                <xsl:apply-templates select="document('udata://exchange/getExchangeEcommerceCodeBeforeGTM/')/udata" />

                <xsl:value-of select="$settings//property[@name='scripts_head']/value" disable-output-escaping="yes" />
                <script type="text/javascript">
                    <xsl:value-of select="document('udata://catalog/addMetrikData/')/udata" disable-output-escaping="yes" />
                </script>
            </head>

            <body>
				<xsl:if test="$top_information_string_close != 1 and $settings//property[@name = 'pokazyvat_informacionnuyu_stroku']/value = 1">
					<xsl:attribute name="class">with_top_info_string</xsl:attribute>
				</xsl:if>

                <xsl:comment> Google Tag Manager (noscript) </xsl:comment>
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M8L987N"
                                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <xsl:comment> End Google Tag Manager (noscript) </xsl:comment>

                <xsl:apply-templates select="result" mode="header" />
                <xsl:apply-templates select="result" />
                <xsl:apply-templates select="result" mode="footer" />

				<xsl:if test="$top_information_string_close != 1 and $settings//property[@name = 'pokazyvat_informacionnuyu_stroku']/value = 1">
					<div class="top_information_string" style="color:{$settings//property[@name = 'cvet_teksta_informacionnoj_stroki']/value};background-color:{$settings//property[@name = 'cvet_fona_informacionnoj_stroki']/value}">
						<div class="container">
							<noindex>
								<div class="top_info_string_content">
									<xsl:if test="$settings//property[@name = 'ikonka_informacionnoj_stroki']/value">
										<div class="top_info_string_icon" style="color:{$settings//property[@name = 'cvet_ikonki_informacionnoj_stroki']/value};">
											<i class="fa {$settings//property[@name = 'ikonka_informacionnoj_stroki']/value}"></i>
										</div>
									</xsl:if>
									<a class="top_info_string_close" href="#" title="Закрыть" style="color:{$settings//property[@name = 'cvet_teksta_informacionnoj_stroki']/value};">
										<i class="fa fa-close"></i>
									</a>
									<div class="top_info_string_data">
										<div class="top_info_string_data_long">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki']/value" disable-output-escaping="yes" />
										</div>
										<div class="top_info_string_data_middle">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki_srednij']/value" disable-output-escaping="yes" />
										</div>
										<div class="top_info_string_data_small">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki_korotkij']/value" disable-output-escaping="yes" />
										</div>
									</div>
								</div>
							</noindex>
						</div>
					</div>
				</xsl:if>

                <xsl:apply-templates select="result" mode="modals" />
                <xsl:apply-templates select="result" mode="scripts" />

                <xsl:apply-templates select="document('udata://exchange/getExchangeEcommerceCode/')/udata" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="udata[@method = 'getExchangeEcommerceCode' or @method = 'getExchangeEcommerceCodeBeforeGTM']" />
    <xsl:template match="udata[@method = 'getExchangeEcommerceCode' or @method = 'getExchangeEcommerceCodeBeforeGTM'][code]">
        <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            <xsl:value-of select="code" disable-output-escaping="yes" />
        </script>
    </xsl:template>
</xsl:stylesheet>