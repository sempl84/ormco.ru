<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                   xmlns="http://www.w3.org/1999/xhtml"
                   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                   xmlns:date="http://exslt.org/dates-and-times"
                   xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                   xmlns:xlink="http://www.w3.org/TR/xlink"
                   exclude-result-prefixes="xsl date udt xlink">

    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:include href="../../../../default.xsl" />

    <xsl:template match="/">
        <xsl:value-of select="document('udata://custom/setAjaxOutputBuffer/')/udata" />

        <xsl:apply-templates select="udata" />
    </xsl:template>
</xsl:stylesheet>