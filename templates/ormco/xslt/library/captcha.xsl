<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="udata[@module = 'system' and @method = 'captcha']" />
	<xsl:template match="udata[@module = 'system' and @method = 'captcha' and count(url)]">
			<div class="form-group">
                <label>Введите текст с картинки:</label>
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{url}{url/@random-string}" class="captcha_img" alt="captcha" /><br />
                        <a href="#" class="captcha_reset">обновить</a>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="captcha" name="captcha" />
                    </div>
                </div>
            </div>
	</xsl:template>
</xsl:stylesheet>