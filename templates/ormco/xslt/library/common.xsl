<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="errors.xsl" />
	<xsl:include href="date.xsl" />
	<xsl:include href="captcha.xsl" />
	
	<xsl:include href="thumbnails.xsl" />

	<xsl:template match="/">

		<xsl:choose>
			<xsl:when test="count(udata) = 0">
				<xsl:apply-templates select="." mode="layout" />
			</xsl:when>

			<xsl:otherwise>
				<xsl:apply-templates />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="result">
		<p>
			<xsl:text>Debug message for UC-pages.</xsl:text>
		</p>
	</xsl:template>

	<xsl:template match="udata[@method = 'addExchangeEcommercePromoView']" />
	<xsl:template match="udata[@method = 'addExchangeEcommercePromoView'][params]">
		<xsl:attribute name="data-gtm-event">ecommercePromoClick</xsl:attribute>
		<xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="udata[@method = 'addExchangeEcommerceImpression']" />
	<xsl:template match="udata[@method = 'addExchangeEcommerceImpression'][params]">
		<xsl:attribute name="data-gtm-event">ecommerceClick</xsl:attribute>
		<xsl:attribute name="data-gtm-page-id"><xsl:value-of select="@id" /></xsl:attribute>
		<xsl:attribute name="data-gtm-list"><xsl:value-of select="@list" /></xsl:attribute>
		<xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="udata[@method = 'getExchangeEcommerceDetailCode']" />
	<xsl:template match="udata[@method = 'getExchangeEcommerceDetailCode'][params]">
		<xsl:attribute name="data-gtm-event">ecommerceDetail</xsl:attribute>
		<xsl:attribute name="data-gtm-page-id"><xsl:value-of select="@id" /></xsl:attribute>
		<xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="udata[@method = 'getExchangeEcommerceProductData']" mode="ecommerce-add" />
	<xsl:template match="udata[@method = 'getExchangeEcommerceProductData'][params]" mode="ecommerce-add">
		<xsl:attribute name="data-gtm-event">ecommerceAdd</xsl:attribute>
		<xsl:attribute name="data-gtm-page-id"><xsl:value-of select="@id" /></xsl:attribute>
		<xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="udata[@method = 'addExchangeEcommerceCheckoutCode']" />
</xsl:stylesheet>