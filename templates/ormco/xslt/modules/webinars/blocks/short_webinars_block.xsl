<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- новости на главной -->
	<xsl:template match="udata[@module='webinars' and @method='listElements']" mode="short_webinars_block" />	
	<xsl:template match="udata[@module='webinars' and @method='listElements' and items/item]" mode="short_webinars_block">	
		<div class="videos content-block gray">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-md-12 col-xs-12"><div class="header2">Записи вебинаров</div></div>
	                <ul>
	                    <xsl:apply-templates select="items/item" mode="short_webinars_block"/>
	                </ul>
	                <div class="col-md-12 col-xs-12 text-center">
	                    <a class="btn btn-primary" href="{document('upage://&webinars_pid;')/udata/page/@link}">Все записи вебинаров</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="item" mode="short_webinars_block">
		<li class="col-md-3 col-xs-6">
            <div class="thumbnail">
            	<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
					<xsl:attribute name="class">thumbnail locked</xsl:attribute>
				</xsl:if>
				
                <a href="{@link}" class="video-link">
                    <div class="preview">
                        <xsl:choose>
							<xsl:when test=".//property[@name='video_thumb']/value">
								<img src="{.//property[@name='video_thumb']/value}" class="img-responsive" alt="{text()}" />
							</xsl:when>
							<xsl:otherwise>
								<img src="&empty-photo-news;" class="img-responsive" alt="{text()}" />
							</xsl:otherwise>
						</xsl:choose>
                     
                		
						<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
							<img class="lock" src="{$template-resources}img/lock-icon.png" />
						</xsl:if>
                    </div>
                    <div class="header3"><xsl:value-of select="text()" /></div>
                </a>
                <xsl:if test=".//property[@name='author_info']/value"> 
                	<div class="speaker">
                        <xsl:if test=".//property[@name='author_photo']/value">
							<img src="{.//property[@name='author_photo']/value}" alt="{.//property[@name='author_info']/value}"/>
						</xsl:if>
                        
                        <span><xsl:value-of select=".//property[@name='author_info']/value" /></span>
                    </div>
	            </xsl:if>
                
            </div>
        </li>
        
        <!-- <li class="item">
			
			<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
				<xsl:attribute name="class">item locked</xsl:attribute>
			</xsl:if>
            <div class="white-pnl clearfix">
                <div class="left">
                    <a class="video-link" href="{@link}">
                        <div class="preview">
                        	<xsl:choose>
								<xsl:when test=".//property[@name='video_thumb']/value">
									<img src="{.//property[@name='video_thumb']/value}" class="img-responsive" alt="{text()}" />
								</xsl:when>
								<xsl:otherwise>
									<img src="&empty-photo-news;" class="img-responsive" alt="{text()}" />
								</xsl:otherwise>
							</xsl:choose>
                         
                    		
							<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
								<img class="lock" src="{$template-resources}img/lock-icon.png" />
							</xsl:if>
                            
                        </div>
                    </a>
                </div>
                <div class="right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" /></a></h4>
                    <xsl:if test=".//property[@name='author_info']/value"> 
                    	<div class="speaker">
	                        <xsl:if test=".//property[@name='author_photo']/value">
								<img src="{.//property[@name='author_photo']/value}" alt="{.//property[@name='author_info']/value}"/>
							</xsl:if>
	                        
	                        <span><xsl:value-of select=".//property[@name='author_info']/value" /></span>
	                    </div>
	                    </xsl:if>
                    <xsl:if test=".//property[@name='publish_time']/value/@unix-timestamp">                       
                    	<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>Добавлен: <xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
                    </xsl:if>
                    <xsl:value-of select=".//property[@name='anons']/value" disable-output-escaping="yes" />
                </div>
            </div>
        </li> -->
	</xsl:template>

</xsl:stylesheet>