<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'webinars']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid webinars">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-play-circle" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                    <xsl:call-template name="header_social_link" />
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://catalog/search/&webinars_pid;')/udata" />
	                
	                <xsl:apply-templates select="document('udata://webinars/listElements/')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="user" mode="right_col_auth" />
	                
	                <xsl:apply-templates select="." mode="right_col" />
	                
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='webinars' and @method='listElements']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='webinars' and @method='listElements' and items/item]">
		<ul class="items videos">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="udata[@module='webinars' and @method='listElements']/items/item" >	
		<li class="item">
			
			<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
				<xsl:attribute name="class">item locked</xsl:attribute>
			</xsl:if>
            <div class="white-pnl clearfix">
                <div class="left">
                    <a class="video-link" href="{@link}">
                        <div class="preview">
                        	<xsl:choose>
								<xsl:when test=".//property[@name='video_thumb']/value">
									<img src="{.//property[@name='video_thumb']/value}" class="img-responsive" alt="{text()}" />
								</xsl:when>
								<xsl:otherwise>
									<img src="&empty-photo-news;" class="img-responsive" alt="{text()}" />
								</xsl:otherwise>
							</xsl:choose>
                         
                    		<!-- TODO права доступа -->
							<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
								<img class="lock" src="{$template-resources}img/lock-icon.png" />
							</xsl:if>
                            
                        </div>
                    </a>
                </div>
                <div class="right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" /></a></h4>
                    <xsl:if test=".//property[@name='author_info']/value"> 
                    	<div class="speaker">
	                        <xsl:if test=".//property[@name='author_photo']/value">
								<img src="{.//property[@name='author_photo']/value}" alt="{.//property[@name='author_info']/value}"/>
							</xsl:if>
	                        
	                        <span><xsl:value-of select=".//property[@name='author_info']/value" /></span>
	                    </div>
	                    </xsl:if>
                    <xsl:if test=".//property[@name='publish_time']/value/@unix-timestamp">                       
                    	<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>Добавлен: <xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
                    </xsl:if>
                    <div class="content_wrap">
                    	<xsl:value-of select=".//property[@name='anons']/value" disable-output-escaping="yes" />
                    </div>
                </div>
            </div>
        </li>
	</xsl:template>

</xsl:stylesheet>