<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'webinars' and @method='item_element']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid webinars-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-play-circle" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                    <xsl:call-template name="header_social_link" />
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <!-- <div class="col-xs-12">
	                <h2><i class="fa fa-play-circle" aria-hidden="true"></i><xsl:value-of select="@header" disable-output-escaping="yes" /></h2>
	            </div> -->
	            
	            
	                
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
	                	<article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
	                    </article>  
	                    <xsl:if test=".//property[@name = 'video']/value">
					
							<div class="webinar_video ">
								<xsl:value-of select=".//property[@name = 'video']/value" disable-output-escaping="yes" />
							</div>
						
						</xsl:if>  
	                </div>
	            </div>
	            
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="user" mode="right_col_auth" />
	                <xsl:apply-templates select="." mode="right_col" />	                
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	

</xsl:stylesheet>