<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'catalog' and @method = 'object']">
        <xsl:value-of select="document(concat('udata://content/addRecentPage/',$document-page-id))/udata"  />
        <xsl:variable name="item_name">
            <xsl:choose>
                <xsl:when test=".//property[@name='h1_alternative']/value">
                    <xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@header" disable-output-escaping="yes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="price" select="document(concat('udata://emarket/price/',@pageId,'//0/'))/udata" />


        <div class="product-page">
            <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceDetailCode/', @pageId, '/', $price/price/actual, '/'))/udata" />
            <!--Breadcrumbs -->
            <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-list-alt" aria-hidden="true">
                        <xsl:if test=".//property[@name='icon_style']/value">
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                            </xsl:attribute>
                        </xsl:if>
                    </i>
                    <h1 >
                        <xsl:value-of select="$item_name" disable-output-escaping="yes" />
                    </h1>
                    <xsl:call-template name="header_social_link" />
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid category">
                <div class="row">
                    <aside class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                        <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//',$parents/page[position() = last() - 1]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />
                        <xsl:apply-templates select="." mode="left_col" />
                    </aside>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product-card white-pnl " itemscope="itemscope" itemtype="http://schema.org/Product">
                                    <meta itemprop="name" content="{$item_name}" />


                                    <div class="row">
                                        <div class="col-xs-3 left">
                                            <a class="object_one_big_image" data-fancybox="object" href="{.//property[@name='photo']/value}" title="{text()}" data-caption="{text()}">
                                                <xsl:call-template name="makeThumbnailFull_ByPath">
                                                    <xsl:with-param name="source" select=".//property[@name='photo']/value" />
                                                    <xsl:with-param name="width" select="190" />
                                                    <xsl:with-param name="height" select="190" />
                                                    <xsl:with-param name="empty">&empty-photo;</xsl:with-param>
                                                    <xsl:with-param name="element-id" select="@id" />
                                                    <xsl:with-param name="field-name" select="'photo'" />
                                                    <xsl:with-param name="alt" select="text()" />
                                                    <xsl:with-param name="class" select="'img-responsive'" />
                                                    <xsl:with-param name="class" select="'img-responsive'" />
                                                </xsl:call-template>
                                            </a>

                                            <xsl:apply-templates select="//property[@name = 'photos']" mode="photos_on_object" />
                                        </div>
                                        <div class="col-xs-9 right">
                                            <!-- <h3>
                                                <xsl:choose>
                                                    <xsl:when test=".//property[@name='h1_alternative']/value">
                                                        <xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </h3> -->
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <xsl:if test=".//property[@name='artikul']/value">
                                                        <p>
                                                            <span class="blue">Артикул</span>&#160;
                                                            <xsl:value-of select=".//property[@name='artikul']/value" />
                                                        </p>
                                                    </xsl:if>
                                                    <xsl:if test=".//property[@name='price']/value &gt; 0">
                                                        <xsl:if test="$price/discount">
                                                            <div class="pf_amount">
                                                                <span class="pf_amount_name">Скидка:</span>
                                                                <xsl:text> </xsl:text>
                                                                <span class="pf_amount_val">
                                                                    <xsl:value-of select="$price/discount/total/@percent" />%,
                                                                    <xsl:text> </xsl:text>
                                                                    <xsl:value-of select="concat(format-number(number($price/discount/total/node()), '#&#160;###,##','price'), ' ', $price/price/@suffix)" />
                                                                </span>
                                                                <xsl:text> </xsl:text>
                                                                <xsl:apply-templates select="$price/discount" mode="default-assets-emarket-discount-info" />
                                                            </div>
                                                        </xsl:if>
                                                        <div class="pf_amount large" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
                                                            <span class="pf_amount_name">Цена:</span>
                                                            <xsl:text> </xsl:text>
                                                            <span class="pf_amount_val">
                                                                <xsl:apply-templates select="$price" mode="object-price" />
                                                            </span>

                                                            <meta itemprop="priceCurrency" content="RUR" />
                                                            <meta itemprop="price" content="{$price/price/actual}" />
                                                            <meta itemprop="url" content="https://{$domain}{$request-uri}" />

                                                            <xsl:choose>
                                                                <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                                                                    <meta itemprop="availability" content="https://schema.org/InStock" />
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                            <meta itemprop="priceValidUntil " content="{number($curr_year) + 1}-01-01" />

                                                        </div>

                                                    </xsl:if>
                                                    <!-- <p><span class="blue">Артикул</span> 000-0028</p>
                                                    <p><span class="blue">Цена</span> 7480 руб.</p> -->

                                                    <meta itemprop="description" content="{$document-meta-description}" />
                                                    <meta itemprop="sku" content="{.//property[@name='artikul']/value}" />

                                                    <div itemprop="brand" itemtype="https://schema.org/Brand" itemscope="itemscope">
                                                        <meta itemprop="name" content="Ormco" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="already-select" id="add_basket_mark_{page/@id}" style="display:none">
                                                        <xsl:if test="$cart/items/item/page/@id = page/@id">
                                                            <xsl:attribute name="style"></xsl:attribute>
                                                        </xsl:if>
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>Товар был добавлен в корзину<br/>
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        <a href="/emarket/cart">Оформить заказ</a>
                                                    </div>
                                                    <xsl:choose>
                                                        <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                                                            <div class="availability yes">
                                                                <i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <div class="availability no">
                                                                <i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </div>
                                            </div>

                                            <!-- прячем кнопку купить и поля для указания кол-ва, если товара нет в наличии -->
                                            <xsl:if test=".//property[@name='common_quantity']/value &gt; 0">
                                                <div class="input-group spinner forBuy">
                                                    <label for="amount_id_{page/@id}" class="blue">Количество</label>
                                                    <input id="amount_id_{page/@id}" type="text" class="form-control" value="1" />
                                                    <div class="input-group-btn-vertical">
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-play" aria-hidden="true"></i>
                                                        </button>
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-play" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <a id="add_basket_{page/@id}" href="{$lang-prefix}/emarket/basket/put/element/{page/@id}/"
                                                    class="btn-primary btn-buy basket_list ga_productbuy"
                                                    data-id="{.//property[@name='artikul']/value}"
                                                    data-name="{$item_name}"
                                                    data-category="{$ec_category}"
                                                    data-brand="{.//property[@name='breket_sistema']/value}"
                                                    data-price="{$price/price/actual}"
                                                    data-qty="1">
                                                    <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceProductData/', page/@id, '/', $price/price/actual, '/'))/udata" mode="ecommerce-add" />
                                                    Купить
                                                </a>
                                            </xsl:if>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <xsl:if test=".//property[@name='description']/value and not(.//property[@name='description']/value='&lt;p&gt;&lt;/p&gt;')">
                            <div class="row">
                                <div class="col-lg-12">
                                    <article class="white-pnl">
                                        <h3>Описание товара</h3>
                                        <div class="content_wrap" itemprop="description">
                                            <xsl:value-of select=".//property[@name='description']/value" disable-output-escaping="yes" />
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </xsl:if>

						<xsl:apply-templates select="document('udata://catalog/recommend_category/?extProps=h1_alternative,photo,common_quantity')/udata" mode="viewed">
							<xsl:with-param name="block_title" select="'&recommend_category_title;'"/>
						</xsl:apply-templates>
						<xsl:apply-templates select="document('udata://catalog/recommend_item/?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed">
							<xsl:with-param name="block_title" select="'&recommend_objects_title;'"/>
						</xsl:apply-templates>

                        <div class="row">
                            <div class="col-lg-12">
                                <xsl:call-template name="social_share" />
                            </div>
                        </div>
                        <xsl:apply-templates select="document('udata://content/getRecentPages?extProps=photo,common_quantity')/udata" mode="viewed" />
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="property" mode="photos_on_object" />
    <xsl:template match="property[value]" mode="photos_on_object">
        <div class="object_images">
            <xsl:apply-templates select="value" mode="photos_on_object" />
        </div>
    </xsl:template>
    <xsl:template match="value" mode="photos_on_object">
        <a class="object_one_image" data-fancybox="object" href="{text()}" title="{@alt}" data-caption="{@alt}">
            <img src="{document(concat('udata://system/makeThumbnailFull/(', @path, ')/200/200'))//src}" alt="{@alt}" class="img-responsive" />
        </a>
    </xsl:template>

    <xsl:template match="udata" mode="object-price">
        <xsl:if test="price/original">
            <del><xsl:value-of select="format-number(number(price/original), '#&#160;###,##','price')" /></del>
        </xsl:if>
        <xsl:text> </xsl:text>
        <ins><xsl:value-of select="concat(price/@prefix, ' ', format-number(number(price/actual), '#&#160;###,##','price'), ' ', price/@suffix)" /></ins>
    </xsl:template>

    <xsl:template match="udata" mode="object-price-box">
        <xsl:if test="price/original">
            <div class="pf_price">
                <del><xsl:value-of select="format-number(number(price/original), '#&#160;###,##','price')" /></del>&#160;
                <xsl:apply-templates select="discount" mode="default-assets-emarket-discount-info" />
            </div>
        </xsl:if>
        <div class="pf_price">
            <span>
                <xsl:value-of select="concat(price/@prefix, ' ', format-number(number(price/actual), '#&#160;###,##','price'))" />
                <small>
                    &#160;<xsl:value-of select="price/@suffix" />
                </small>
            </span>
        </div>
    </xsl:template>
</xsl:stylesheet>