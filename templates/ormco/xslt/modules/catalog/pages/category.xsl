<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="result[@module = 'catalog' and @method = 'category']">
        <xsl:variable name="childCategory" select="document(concat('udata://catalog/getCategoryList/',$document-page-id))/udata" />
        <xsl:variable name="isChooseBrackets" select=".//property[@name='choose_brackets']/value" />

        <div class="catalog-page" >
            <xsl:if test="$childCategory//item">
                <xsl:attribute name="class">category-page</xsl:attribute>
            </xsl:if>
            <xsl:if test="$isChooseBrackets">
                <xsl:attribute name="class">brackets-page</xsl:attribute>
            </xsl:if>
            <!--Breadcrumbs -->
            <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-list-alt" aria-hidden="true">
                        <xsl:if test=".//property[@name='icon_style']/value">
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                            </xsl:attribute>
                        </xsl:if>
                    </i>
                    <h1  itemprop="name">
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                    <xsl:call-template name="header_social_link" />
                    <!-- если это брекеты, выводим подсказку -->
                    <xsl:if test="page/@id = 283">
                        <a href="#descrlink" class="info-lnk" data-toggle="tooltip" data-placement="top" title="Прочитайте подробнее о брекетах Damon Clear">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </a>
                    </xsl:if>
                </div>
            </div>

            <!--Content -->
            <xsl:choose>
                <!-- вывод категории с подразделами -->
                <xsl:when test="$childCategory//item">
                    <div class="container-fluid category">
                        <div class="row">
                            <aside class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                                <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//',$parents/page[position() = last()]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />
                                <xsl:apply-templates select="." mode="left_col" />
                            </aside>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
                                <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList/',$document-page-id,'?extProps=header_pic'))/udata" />

                                <xsl:apply-templates select="document('udata://catalog/recommend_category/?extProps=h1_alternative,photo,common_quantity')/udata" mode="viewed">
                                    <xsl:with-param name="block_title" select="'&recommend_category_title;'"/>
                                </xsl:apply-templates>
                                <xsl:apply-templates select="document('udata://catalog/recommend_item/?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed">
                                    <xsl:with-param name="block_title" select="'&recommend_objects_title;'"/>
                                </xsl:apply-templates>
                                <xsl:apply-templates select="document('udata://content/getRecentPages?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed" />

                                <xsl:apply-templates select=".//property[@name='descr']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel1']/value/page[1]/@id and .//property[@name='kolvo_tovarov1']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel1']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov1']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" >
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel1']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_1']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>

                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_1/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie_2']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel2']/value/page[1]/@id and .//property[@name='kolvo_tovarov2']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel2']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov2']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" >
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel2']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_2']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_2/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie3']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel3']/value/page[1]/@id and .//property[@name='kolvo_tovarov3']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel3']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov3']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel3']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_3']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_3/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie4']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel4']/value/page[1]/@id and .//property[@name='kolvo_tovarov4']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel4']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov4']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel4']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_4']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_4/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie5']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel5']/value/page[1]/@id and .//property[@name='kolvo_tovarov5']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel5']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov5']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel5']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_5']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_5/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie6']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel6']/value/page[1]/@id and .//property[@name='kolvo_tovarov6']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel6']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov6']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel6']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_6']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_6/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie7']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel7']/value/page[1]/@id and .//property[@name='kolvo_tovarov7']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel7']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov7']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel7']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_7']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_7/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie8']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel8']/value/page[1]/@id and .//property[@name='kolvo_tovarov8']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel8']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov8']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel8']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_8']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_8/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie9']" mode="categorDescr" />

                                <!--сертификаты-->
                                <xsl:if test="//property[@name = 'galereya_1']">
                                    <div class="row certificates">
                                        <div class="col-lg-12">
                                            <article class="white-pnl content_wrap">
                                                <xsl:apply-templates select="//property[@name = 'galereya_1']" mode="galereya_1" />
                                            </article>
                                        </div>
                                    </div>
                                </xsl:if>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <xsl:call-template name="social_share" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </xsl:when>
                <!-- выбор брекетов -->
                <xsl:when test="$isChooseBrackets">
                    <xsl:apply-templates select="." mode="choose_brackets" />
                </xsl:when>
                <!-- вывод категории с товарами из других разделов -->
                <xsl:when test="page/@type-id = &category_other_items_tid;">
                    <div class="container-fluid category_other_items">
                        <div class="row">
                            <aside class="col-md-3 left-pnl">
                                <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//',$parents/page[position() = last()]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />
                                <xsl:apply-templates select="." mode="left_col" />
                            </aside>
                            <div class="col-md-9">
                                <xsl:apply-templates select=".//property[@name='descr']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel1']/value/page[1]/@id and .//property[@name='kolvo_tovarov1']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel1']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov1']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" >
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel1']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_1']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>

                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_1/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie_2']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel2']/value/page[1]/@id and .//property[@name='kolvo_tovarov2']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel2']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov2']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" >
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel2']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_2']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_2/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie3']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel3']/value/page[1]/@id and .//property[@name='kolvo_tovarov3']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel3']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov3']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel3']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_3']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_3/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie4']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel4']/value/page[1]/@id and .//property[@name='kolvo_tovarov4']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel4']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov4']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel4']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_4']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_4/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie5']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel5']/value/page[1]/@id and .//property[@name='kolvo_tovarov5']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel5']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov5']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel5']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_5']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_5/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie6']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel6']/value/page[1]/@id and .//property[@name='kolvo_tovarov6']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel6']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov6']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel6']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_6']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_6/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie7']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel7']/value/page[1]/@id and .//property[@name='kolvo_tovarov7']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel7']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov7']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel7']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_7']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_7/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie8']" mode="categorDescr" />
                                <xsl:if test=".//property[@name='razdel8']/value/page[1]/@id and .//property[@name='kolvo_tovarov8']/value">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',.//property[@name='razdel8']/value/page[1]/@id,'/',.//property[@name='kolvo_tovarov8']/value,'/1//common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging">
                                                <xsl:with-param name="parent_link" select=".//property[@name='razdel8']/value/page[1]/@link"/>
                                                <xsl:with-param name="show_all_title" select=".//property[@name='nazvanie_knopki_posmotret_vse_tovary_8']/value"/>
                                            </xsl:apply-templates>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:apply-templates select="document(concat('udata://catalog/catalog_item_from_symlink/vyvodit_ne_razdel_a_spisok_tovarov_8/',$document-page-id,'?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalogNoPaging" />

                                <xsl:apply-templates select=".//property[@name='opisanie9']" mode="categorDescr" />

                                <xsl:apply-templates select="document('udata://content/getRecentPages?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed" />

                                <!--сертификаты-->
                                <xsl:if test="//property[@name = 'galereya_1']">
                                    <div class="row certificates">
                                        <div class="col-lg-12">
                                            <article class="white-pnl content_wrap">
                                                <xsl:apply-templates select="//property[@name = 'galereya_1']" mode="galereya_1" />
                                            </article>
                                        </div>
                                    </div>
                                </xsl:if>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <xsl:call-template name="social_share" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </xsl:when>
                <!-- вывод категории с товарами -->
                <xsl:otherwise>
                    <div class="container-fluid">
                        <div class="row">
                            <aside class="col-md-3 left-pnl">
                                <xsl:apply-templates select="document(concat('udata://catalog/getCatalogOrmcoSmartFilters//',$document-page-id,'/1/10'))/udata" mode="filters" />
                                <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//',$parents/page[position() = last()]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />
                                <xsl:apply-templates select="." mode="left_col" />
                            </aside>
                            <div class="col-md-9">
                                <xsl:if test="not($p &gt; 0)">
                                    <xsl:apply-templates select=".//property[@name='top_descr']" mode="top_descr" />
                                </xsl:if>

                                <div class="row">
                                    <div class="col-lg-12 objects_list">
                                        <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',$document-page-id,'////common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))/udata" mode="smartCatalog" />
                                    </div>
                                </div>

                                <xsl:apply-templates select="document('udata://content/getRecentPages?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed" />

								<xsl:if test="not($p &gt; 0)">
									<xsl:apply-templates select="document('udata://catalog/recommend_category/?extProps=h1_alternative,photo,common_quantity')/udata" mode="viewed">
										<xsl:with-param name="block_title" select="'&recommend_category_title;'"/>
									</xsl:apply-templates>
									<xsl:apply-templates select="document('udata://catalog/recommend_item/?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed">
										<xsl:with-param name="block_title" select="'&recommend_objects_title;'"/>
									</xsl:apply-templates>

                                    <xsl:apply-templates select=".//property[@name='descr']" mode="categorDescr" >
                                        <xsl:with-param name="show_cetificates" select="1" />
                                    </xsl:apply-templates>
                                </xsl:if>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <xsl:call-template name="social_share" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="property" mode="top_descr" />
    <xsl:template match="property[value]" mode="top_descr">
    	<div class="row top_descr">
            <div class="col-lg-12">
                <article class="white-pnl content_wrap">
                    <xsl:value-of select="value" disable-output-escaping="yes" />
                </article>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="property" mode="categorDescr" />
    <xsl:template match="property[value]" mode="categorDescr">
        <xsl:param name="show_cetificates" />
    	<div class="row" id="descrlink">
            <div class="col-lg-12">
                <article class="white-pnl content_wrap">
                    <xsl:value-of select="value" disable-output-escaping="yes" />

                    <!--сертификаты-->
                    <xsl:if test="//property[@name = 'galereya_1'] and $show_cetificates = 1">
                        <xsl:apply-templates select="//property[@name = 'galereya_1']" mode="galereya_1" />

                    </xsl:if>
                </article>
            </div>
        </div>
    </xsl:template>

    <!-- список товаров с пагинацией -->
    <xsl:template match="udata" mode="smartCatalog">
        <div class="white-pnl products-list js-products-list">
            Ничего нет
        </div>
    </xsl:template>
    <xsl:template match="udata[lines/item]" mode="smartCatalog">
        <div class="pf_row">
            <xsl:apply-templates select="lines/item" mode="short_item_box" />
        </div>
        <xsl:apply-templates select="total" />
    </xsl:template>

    <xsl:template match="item" mode="short_item_box">
        <xsl:param name="is_options" select="false()" />
        <xsl:variable name="item_name">
            <xsl:choose>
                <xsl:when test=".//property[@name='h1_alternative']/value">
                    <xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="text()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="price" select="document(concat('udata://emarket/price/',@id,'//0/'))/udata" />

        <div class="pf_grid4 pf_tb6 pf_sm12 pf_product_grid" itemscope="itemscope" itemtype="http://schema.org/Product">
            <xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommerceImpression/', @id, '/', $price/price/actual, '/(', php:function('urlencode', 'Каталог'), ')/', position(), '/1/'))/udata" />

            <div class="pf_productbox">
                <div class="pf_productbox_top">
                    <div class="pf_productbox_img">
                        <a href="{@link}"
                            data-id="{.//property[@name='artikul']/value}"
                            data-name="{$item_name}"
                            data-category="{$ec_category}"
                            data-brand="{.//property[@name='breket_sistema']/value}"
                            data-position="{position()}"
                            data-list="{$document-header}">

                            <xsl:call-template name="makeThumbnailFull_ByPath">
                                <xsl:with-param name="source" select=".//property[@name='photo']/value" />
                                <xsl:with-param name="width" select="190" />
                                <xsl:with-param name="height" select="190" />
                                <xsl:with-param name="empty">&empty-photo;</xsl:with-param>
                                <xsl:with-param name="element-id" select="@id" />
                                <xsl:with-param name="field-name" select="'photo'" />
                                <xsl:with-param name="class" select="'img-responsive'" />
                            </xsl:call-template>
                        </a>
                    </div>

                    <xsl:choose>
                        <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                            <div class="pf_productbox_avail">В наличии</div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="pf_productbox_avail none">Нет в наличии</div>
                        </xsl:otherwise>
                    </xsl:choose>

                    <div class="pf_productbox_title">
                        <a href="{@link}" class="lnk-to-product ga_productclick"
                            data-id="{.//property[@name='artikul']/value}"
                            data-name="{$item_name}"
                            data-category="{$ec_category}"
                            data-brand="{.//property[@name='breket_sistema']/value}"
                            data-position="{position()}"
                            data-list="{$document-header}">
                            <span itemprop="name">
                                <xsl:value-of select="$item_name" />
                            </span>
                        </a>
                    </div>

                    <div class="pf_productbox_meta">
                        <xsl:if test=".//property[@name='price']/value &gt; 0">
                            <xsl:if test="$price/price/actual &gt; 0">
                                <div class="pf_productbox_price" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
                                    <xsl:apply-templates select="$price" mode="object-price-box" />

                                    <meta itemprop="priceCurrency" content="RUR" />
                                    <meta itemprop="price" content="{$price/price/actual}" />
                                    <meta itemprop="url" content="https://{$domain}{$request-uri}" />

                                    <xsl:choose>
                                        <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                                            <meta itemprop="availability" content="https://schema.org/InStock" />
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <meta itemprop="priceValidUntil " content="{number($curr_year) + 1}-01-01" />
                                </div>
                            </xsl:if>
                        </xsl:if>

                        <meta itemprop="description" content="{$document-meta-description}" />
                        <meta itemprop="sku" content="{.//property[@name='artikul']/value}" />

                        <div itemprop="brand" itemtype="https://schema.org/Brand" itemscope="itemscope">
                            <meta itemprop="name" content="Ormco" />
                        </div>

                        <xsl:if test=".//property[@name='artikul']/value">
                            <div class="pf_productbox_art">Артикул:&#160;<xsl:value-of select=".//property[@name='artikul']/value" /></div>
                        </xsl:if>
                    </div>

                    <xsl:if test="$price/discount/coupon">
                        <div class="pf_productbox_sale">
                            <span>
                                <xsl:value-of select="concat($price/discount/coupon/@percent, '%')" />
                            </span>
                            &#160;
                            <xsl:value-of select="$price/discount/coupon/active_to/@days-left" />
                        </div>
                    </xsl:if>

                    <div class="pf_productbox_hover_area">
                        <xsl:if test=".//property[@name='common_quantity']/value &gt; 0">
                            <a id="add_basket_{@id}" href="{$lang-prefix}/emarket/basket/put/element/{@id}/" class="basket_list options_{$is_options} ga_productbuy pf_btn pf_btn_buy"
                                data-id="{.//property[@name='artikul']/value}"
                                data-name="{$item_name}"
                                data-category="{$ec_category}"
                                data-brand="{.//property[@name='breket_sistema']/value}"
                                data-price="{$price/price/actual}"
                                data-qty="1">
                                <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceProductData/', @id, '/', $price/price/actual, '/'))/udata" mode="ecommerce-add" />
                                Купить
                            </a>

                            <div class="jq-number pf_number">
                                <div class="jq-number__field">
                                    <input id="amount_id_{@id}" type="number" class="pf_number" min="1" value="1" />
                                </div>
                                <div class="jq-number__spin minus"></div>
                                <div class="jq-number__spin plus"></div>
                            </div>
                        </xsl:if>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- список товаров без пагинации (для разделов с товарами из других разделов) -->
    <xsl:template match="udata" mode="smartCatalogNoPaging"></xsl:template>
    <xsl:template match="udata[lines/item]" mode="smartCatalogNoPaging">
        <xsl:param name="show_all_title" />
        <xsl:param name="parent_link" />
        <div class="products-list js-products-list">
            <div class="pf_row">
                <xsl:apply-templates select="lines/item" mode="short_item_box" />
            </div>
            <xsl:if test="$parent_link">
                <a href="{$parent_link}" class="btn btn-primary btn-show-details show_catalog">
                    <span></span>
                    <span>
                        <xsl:choose>
                            <xsl:when test="$show_all_title">
                                <xsl:value-of select="$show_all_title" />
                            </xsl:when>
                            <xsl:otherwise>Посмотреть все товары</xsl:otherwise>
                        </xsl:choose>
                    </span>
                </a>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>