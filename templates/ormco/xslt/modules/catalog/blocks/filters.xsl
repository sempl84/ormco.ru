<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:variable name="filter" select="'filter'" />
	<xsl:variable name="from" select="'from'" />
	<xsl:variable name="to" select="'to'" />

	<xsl:template match="udata" mode="filters" />
    <xsl:template match="udata[//field]" mode="filters">
        <xsl:param name="action" select="''" />

        <div class="left-nav filters collapse js-filter js-collapse-pnl" id="showFilters" data-category="{@category-id}">
            <form class="clearfix" action="{$action}" data-category="{@category-id}">
                <xsl:apply-templates select="//field"  mode="filter" />

                <div class="header4">
                    <button type="submit" class="btn btn-primary">Фильтровать</button>
                </div>
            </form>
        </div>
        <button class="btn btn-primary btn-show-details btn-show-filter collapsed" type="button" data-toggle="collapse" data-target="#showFilters" aria-expanded="false" aria-controls="showFilters">
            <span>Показать фильтры</span><span>Свернуть фильтры</span>
        </button>
    </xsl:template>

    <xsl:template match="field" mode="filter">
		<div class="header4">
			<xsl:value-of select="@title" />
			<a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
		</div>

        <ul>
        	<xsl:apply-templates select="." mode="filter_field"/>
        </ul>

	</xsl:template>

	<xsl:template match="field[@data-type = 'boolean' and not(item = 1)]" mode="filter" />

	<xsl:template match="field" mode="filter_field">
		<xsl:apply-templates select="item" mode="filter_field" />
	</xsl:template>

		<xsl:template match="field/item" mode="filter_field">
			<li>
				<label>

					<input type="checkbox" name="{$filter}[{../@name}][{position() - 1}]" value="{text()}" class="filter_values filter_system_name_{../@name}" data-name="{$filter}[{../@name}][{position() - 1}]" data-value="{text()}">
						<xsl:if test="@is-selected = '1'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<xsl:value-of select="text()" />
				</label>
			</li>
		</xsl:template>

	<xsl:template match="field[@data-type = 'boolean' or @data-type = 'file' or @data-type = 'img_file' or @data-type = 'swf_file' or @data-type = 'video_file']" mode="filter_field">
		<xsl:variable name="selected_value" select="item[@is-selected = '1']" />

		<li>
			<label>
				<input type="radio" name="{$filter}[{@name}]" value="1" class="filter_values filter_system_name_{@name}" data-name="{$filter}[{@name}]" data-value="1">
					<xsl:apply-templates select="$selected_value" mode="checked">
						<xsl:with-param name="compare" select="1"/>
					</xsl:apply-templates>
				</input>
				<xsl:text>Есть</xsl:text>
			</label>
		</li>
		<li>
			<label>
				<input type="radio" name="{$filter}[{@name}]" value="0" class="filter_values filter_system_name_{@name}" data-name="{$filter}[{@name}]" data-value="0">
					<xsl:apply-templates select="$selected_value" mode="checked">
						<xsl:with-param name="compare" select="0"/>
					</xsl:apply-templates>
				</input>
				<xsl:text>Нет</xsl:text>
			</label>
		</li>
		<li>
			<label>
				<input type="radio" name="{$filter}[{@name}]" value="" class="filter_values filter_system_name_{@name}" data-name="{$filter}[{@name}]" data-value="">
					<xsl:if test="not($selected_value)">
						<xsl:attribute name="checked">
							<xsl:text>checked</xsl:text>
						</xsl:attribute>
					</xsl:if>
				</input>
				<xsl:text>Неважно</xsl:text>
			</label>
		</li>
	</xsl:template>

		<xsl:template match="item" mode="checked">
			<xsl:param name="compare" />

			<xsl:if test="$compare = text()">
				<xsl:attribute name="checked">
					<xsl:text>checked</xsl:text>
				</xsl:attribute>
			</xsl:if>
		</xsl:template>

</xsl:stylesheet>