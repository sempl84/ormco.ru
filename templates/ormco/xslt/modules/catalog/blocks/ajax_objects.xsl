<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl"
				exclude-result-prefixes="xsl php">

    <xsl:include href="../../emarket/blocks/discount_info.xsl" />

	<xsl:output encoding="utf-8" method="html" indent="yes" />

 	<xsl:decimal-format name="price" decimal-separator="," grouping-separator="&#160;"/>

	<xsl:template match="/">
		<xsl:apply-templates select="udata" mode="smartCatalog"/>
	</xsl:template>

	 <xsl:template match="udata" mode="smartCatalog">
    	<div class="white-pnl products-list js-products-list">Ничего нет</div>
    </xsl:template>
	<xsl:template match="udata[lines/item]" mode="smartCatalog">
		<xsl:variable name="category_name" select="document(concat('upage://',category_id))/udata/page/name" />
		<xsl:variable name="ec_category" select="document(concat('udata://catalog/getNavibarCategory/',category_id))/udata" />

        <div class="products-list js-products-list">
            <div class="pf_row">
                <xsl:apply-templates select="lines/item" mode="short_item_box">
                    <xsl:with-param name="category_name" select="$category_name" />
                    <xsl:with-param name="ec_category" select="$ec_category" />
                </xsl:apply-templates>
            </div>
	    </div>
	    <xsl:apply-templates select="total" />
    </xsl:template>

    <xsl:template match="item" mode="short_item_box">
    	<xsl:param name="category_name" select="''" />
    	<xsl:param name="ec_category" select="''" />
    	<xsl:param name="is_options" select="false()" />
    	<xsl:param name="item_info" select="document(concat('upage://', @id))/udata" />

        <xsl:variable name="item_name">
            <xsl:choose>
                <xsl:when test="$item_info//property[@name='h1_alternative']/value">
                    <xsl:value-of select="$item_info//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="text()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="price" select="document(concat('udata://emarket/price/', @id,'//0/'))/udata" />

        <div class="pf_grid4 pf_tb6 pf_sm12 pf_product_grid" itemscope="itemscope" itemtype="http://schema.org/Product">
            <div class="pf_productbox">
                <xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommerceImpression/', @id, '/', $price/price/actual, '/(', php:function('urlencode', 'Каталог'), '/', position(), '/1/'))/udata" />

                <div class="pf_productbox_top">
                    <div class="pf_productbox_img">
                        <a href="{@link}"
                            data-id="{$item_info//property[@name='artikul']/value}"
                            data-name="{$item_name}"
                            data-category="{$ec_category}"
                            data-brand="{$item_info//property[@name='breket_sistema']/value}"
                            data-position="{position()}"
                            data-list="{$category_name}">

                            <xsl:call-template name="makeThumbnailFull_ByPath">
                                <xsl:with-param name="source" select="$item_info//property[@name='photo']/value" />
                                <xsl:with-param name="width" select="190" />
                                <xsl:with-param name="height" select="190" />
                                <xsl:with-param name="empty">/templates/ormco/img/logo-ormco-tovar.png</xsl:with-param>
                                <xsl:with-param name="element-id" select="@id" />
                                <xsl:with-param name="field-name" select="'photo'" />
                                <xsl:with-param name="class" select="'img-responsive'" />
                            </xsl:call-template>
                        </a>
                    </div>

                    <xsl:choose>
                        <xsl:when test="$item_info//property[@name='common_quantity']/value &gt; 0">
                            <div class="pf_productbox_avail">В наличии</div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="pf_productbox_avail none">Нет в наличии</div>
                        </xsl:otherwise>
                    </xsl:choose>

                    <div class="pf_productbox_title">
                        <a href="{@link}" class="lnk-to-product ga_productclick"
                            data-id="{$item_info//property[@name='artikul']/value}"
                            data-name="{$item_name}"
                            data-category="{$ec_category}"
                            data-brand="{$item_info//property[@name='breket_sistema']/value}"
                            data-position="{position()}"
                            data-list="{$category_name}">
                            <span itemprop="name">
                                <xsl:value-of select="$item_name" />
                            </span>
                        </a>
                    </div>

                    <div class="pf_productbox_meta">
                        <xsl:if test="$item_info//property[@name='price']/value &gt; 0">
                            <xsl:if test="$price/price/actual &gt; 0">
                                <div class="pf_productbox_price"  itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
                                    <xsl:apply-templates select="$price" mode="object-price-box" />

                                    <meta itemprop="priceCurrency" content="RUR" />
                                    <meta itemprop="price" content="{$price/price/actual}" />
                                    <meta itemprop="url" content="https://ormco.ru/{$item_info/page/@link}" />
                                </div>
                            </xsl:if>
                        </xsl:if>

                        <xsl:if test="$item_info//property[@name='artikul']/value">
                            <div class="pf_productbox_art">Артикул:&#160;<xsl:value-of select="$item_info//property[@name='artikul']/value" /></div>
                        </xsl:if>
                    </div>

                    <xsl:if test="$price/discount/coupon">
                        <div class="pf_productbox_sale">
                            <span>
                                <xsl:value-of select="concat($price/discount/coupon/@percent, '%')" />
                            </span>
                            &#160;
                            <xsl:value-of select="$price/discount/coupon/active_to/@days-left" />
                        </div>
                    </xsl:if>

                    <div class="pf_productbox_hover_area">
                        <xsl:if test="$item_info//property[@name='common_quantity']/value &gt; 0">
                            <a id="add_basket_{@id}" href="/emarket/basket/put/element/{@id}/" class="basket_list options_{$is_options} ga_productbuy pf_btn pf_btn_buy"
                                data-id="{$item_info//property[@name='artikul']/value}"
                                data-name="{$item_name}"
                                data-category="{$ec_category}"
                                data-brand="{$item_info//property[@name='breket_sistema']/value}"
                                data-price="{$price/price/actual}"
                                data-qty="1">
                                <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceProductData/', @id, '/', $price/price/actual, '/'))/udata" mode="ecommerce-add" />
                                Купить
                            </a>

                            <div class="jq-number pf_number">
                                <div class="jq-number__field">
                                    <input id="amount_id_{@id}" type="number" class="pf_number" min="1" value="1" />
                                </div>
                                <div class="jq-number__spin minus"></div>
                                <div class="jq-number__spin plus"></div>
                            </div>
                        </xsl:if>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata" mode="object-price-box">
        <xsl:if test="price/original">
            <div class="pf_price">
                <del><xsl:value-of select="format-number(number(price/original), '#&#160;###,##','price')" /></del>&#160;
                <xsl:apply-templates select="discount" mode="default-assets-emarket-discount-info" />
            </div>
        </xsl:if>
        <div class="pf_price">
            <span>
                <xsl:value-of select="concat(price/@prefix, ' ', format-number(number(price/actual), '#&#160;###,##','price'))" />
                <small>
                    &#160;<xsl:value-of select="price/@suffix" />
                </small>
            </span>
        </div>
    </xsl:template>


    <xsl:template match="price|total-price" mode="discounted-price">
		<xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###','price'), ' ', @suffix)" />
	</xsl:template>

	<xsl:template name="makeThumbnailFull_ByPath">
		<xsl:param name="element-id" />
		<xsl:param name="field-name" />
		<xsl:param name="source" />
		<xsl:param name="empty" select="'/templates/ormco/img/logo-ormco-tovar.png'" />
		<xsl:param name="width">auto</xsl:param>
		<xsl:param name="height">auto</xsl:param>
		<xsl:param name="item">1</xsl:param>
		<xsl:param name="alt" />
		<xsl:param name="class" />

		<xsl:variable name="src">
			<xsl:choose>
				<xsl:when test="$source">
					<xsl:value-of select="$source" />
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="$empty" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="thumb" select="document(concat('udata://system/makeThumbnailFull/(.', $src, ')/', $width, '/', $height, '/void/0/1///100'))/udata" />

		<img class="{$class}" src="{$thumb/src}">
			<xsl:if test="$element-id and $field-name">
				<xsl:attribute name="umi:element-id">
					<xsl:value-of select="$element-id" />
				</xsl:attribute>

				<xsl:attribute name="umi:field-name">
					<xsl:value-of select="$field-name" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$alt">
				<xsl:attribute name="alt">
					<xsl:value-of select="$alt" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$empty">
				<xsl:attribute name="umi:empty">
					<xsl:value-of select="$empty" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$item = 1">
				<xsl:attribute name="itemprop">image</xsl:attribute>
			</xsl:if>
		</img>
	</xsl:template>

	<xsl:template match="total" />
	<xsl:template match="total[. &gt; ../per_page]">
		<xsl:apply-templates select="document(concat('udata://system/numpages/', ., '/', ../per_page,'///3/'))/udata" />
	</xsl:template>

	<xsl:template match="udata[@method = 'numpages']" />
	<xsl:template match="udata[@method = 'numpages'][count(items)]">
		<nav class="pagination-wrapper">
            <ul class="pagination">
                <li class="page-item" style="display:none">
                	<xsl:apply-templates select="toprev_link" />
                </li>
                <xsl:if test="tobegin_link and tobegin_link/@page-num &lt; items/item[1]/@page-num">
                     <xsl:apply-templates select="tobegin_link" />
                     <xsl:if test="tobegin_link/@page-num &lt; (items/item[1]/@page-num)">
                     	<li class="page-item"><a class="page-link" href="#">...</a></li>
                     </xsl:if>
                </xsl:if>
                <xsl:apply-templates select="items/item" mode="numpages" />
                <xsl:if test="toend_link and toend_link/@page-num &gt; items/item[position()=last()]/@page-num">
                     <xsl:if test="toend_link/@page-num &gt; (items/item[position()=last()]/@page-num + 1)">
                     	<li class="page-item"><a class="page-link" href="#">...</a></li>
                     </xsl:if>
                     <xsl:apply-templates select="toend_link" />
                </xsl:if>
                <li class="page-item" style="display:none">
                	<xsl:apply-templates select="tonext_link" />
                </li>
            </ul>
        </nav>
	</xsl:template>

	<xsl:template match="item" mode="numpages">
		<li class="page-item"><a class="page-link" href="{@link}" pagenum = "{@page-num}"><xsl:value-of select="." /></a></li>
	</xsl:template>

	<xsl:template match="item[@is-active = '1']" mode="numpages">
		<li class="page-item active"><a class="page-link" href="{@link}" pagenum = "{@page-num}"><xsl:value-of select="." /></a></li>
	</xsl:template>

	<xsl:template match="toprev_link">
		<xsl:attribute name="style"></xsl:attribute>
        <a class="page-link" href="{.}" aria-label="Prev" pagenum = "{@page-num}">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <span class="sr-only">Prev</span>
        </a>
	</xsl:template>

	<xsl:template match="tonext_link">
		<xsl:attribute name="style"></xsl:attribute>
        <a class="page-link" href="{.}" aria-label="Next" pagenum = "{@page-num}">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
	</xsl:template>

	<xsl:template match="tobegin_link">
		<li class="page-item"><a class="page-link" href="{.}" pagenum = "{@page-num}"><xsl:value-of select="@page-num + 1"/></a></li>
    </xsl:template>

    <xsl:template match="toend_link">
		<li class="page-item"><a class="page-link" href="{.}" pagenum = "{@page-num}"><xsl:value-of select="@page-num + 1"/></a></li>
    </xsl:template>

    <xsl:template match="udata[@method = 'addExchangeEcommerceImpression']" />
    <xsl:template match="udata[@method = 'addExchangeEcommerceImpression'][params]">
        <xsl:attribute name="data-gtm-event">ecommerceClick</xsl:attribute>
        <xsl:attribute name="data-gtm-page-id"><xsl:value-of select="@id" /></xsl:attribute>
        <xsl:attribute name="data-gtm-list"><xsl:value-of select="@list" /></xsl:attribute>
        <xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
    </xsl:template>

    <xsl:template match="udata[@method = 'getExchangeEcommerceProductData']" mode="ecommerce-add" />
    <xsl:template match="udata[@method = 'getExchangeEcommerceProductData'][params]" mode="ecommerce-add">
        <xsl:attribute name="data-gtm-event">ecommerceAdd</xsl:attribute>
        <xsl:attribute name="data-gtm-page-id"><xsl:value-of select="@id" /></xsl:attribute>
        <xsl:attribute name="data-gtm-value"><xsl:value-of select="params" /></xsl:attribute>
    </xsl:template>
</xsl:stylesheet>