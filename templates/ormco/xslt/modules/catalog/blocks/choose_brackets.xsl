<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="result" mode="choose_brackets">
        <xsl:variable name="brackets" select="document(concat('udata://catalog/bracket/',$document-page-id))/udata" />

        <div class="container-fluid choice-brackets js-choice-brackets">
            <div class="row">
                <aside class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                    <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//',$parents/page[position() = last()]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />
                    <xsl:apply-templates select="." mode="left_col" />
                </aside>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
                    <xsl:variable name="items" select="document(concat('udata://catalog/getSmartCatalog//',$document-page-id,'/5///common_quantity_flag/0?extProps=h1_alternative,price,common_quantity,artikul,photo,breket_sistema'))" />
                    <xsl:variable name="first_item" select="$items//lines/item[position() = 1]" />

                    <div class="pf_product_line">
                        <div class="pf_product_line_left">
                            <xsl:if test="count($brackets/block[items_count &gt; 0]) &gt; 1">
                                <xsl:attribute name="class">pf_product_line_left wide</xsl:attribute>
                            </xsl:if>

                            <div class="pf_product_line_img">
                                <xsl:call-template name="makeThumbnailFull_ByPath">
                                    <xsl:with-param name="source" select="$first_item//property[@name='photo']/value" />
                                    <xsl:with-param name="width" select="190" />
                                    <xsl:with-param name="height" select="190" />
                                    <xsl:with-param name="empty">&empty-photo;</xsl:with-param>
                                    <xsl:with-param name="element-id" select="$first_item/@id" />
                                    <xsl:with-param name="field-name" select="'photo'" />
                                    <xsl:with-param name="class" select="'img-responsive'" />
                                </xsl:call-template>
                            </div>
                            <xsl:if test="not(current()//property[@name = 'category_choose_brackets_block_hide_button']/value = '1')">
                                <xsl:apply-templates select="$brackets" mode="bracket-block" />
                            </xsl:if>
                        </div>
                        <div class="pf_product_line_text">
                            <xsl:value-of select=".//property[@name = 'tekst_bannera']/value"  disable-output-escaping="yes" />
                        </div>
                    </div>

                    <xsl:if test=".//property[@name = 'zagolovok_pod_bannerom']/value">
                        <div class="bracket_sub_head"><xsl:value-of select=".//property[@name = 'zagolovok_pod_bannerom']/value"  disable-output-escaping="yes" /></div>
                        <p class="bracket_sub_sub_head"><xsl:value-of select=".//property[@name = 'podzagolovok_pod_bannerom']/value"  disable-output-escaping="yes" /></p>
                    </xsl:if>

                    <xsl:if test="not($p &gt; 0)">
                        <xsl:apply-templates select=".//property[@name='top_descr']" mode="top_descr" />
                    </xsl:if>

                    <xsl:apply-templates select="$brackets" mode="bracket_tables" />

                    <div class="row">
                        <div class="col-lg-12">
                            <xsl:apply-templates select="$items/udata" mode="smartCatalog_chooseBrakckets">
                            	<xsl:with-param name="objects_list_title_on_blue_bg" select=".//property[@name='objects_list_title_on_blue_bg']/value" />
                            </xsl:apply-templates>
                        </div>
                    </div>
                    <xsl:apply-templates select="document('udata://content/getRecentPages?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed" />

                    <xsl:if test="not($p &gt; 0)">
						<xsl:apply-templates select="document('udata://catalog/recommend_category/?extProps=h1_alternative,photo,common_quantity')/udata" mode="viewed">
							<xsl:with-param name="block_title" select="'&recommend_category_title;'"/>
						</xsl:apply-templates>
						<xsl:apply-templates select="document('udata://catalog/recommend_item/?extProps=h1_alternative,photo,common_quantity,breket_sistema')/udata" mode="viewed">
							<xsl:with-param name="block_title" select="'&recommend_objects_title;'"/>
						</xsl:apply-templates>

                        <xsl:apply-templates select=".//property[@name='descr']" mode="categorDescr" >
                            <xsl:with-param name="show_cetificates" select="1" />
                        </xsl:apply-templates>
                    </xsl:if>

                    <div class="row">
                        <div class="col-lg-12">
                            <xsl:call-template name="social_share" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="udata" mode="bracket-block">
        <xsl:apply-templates select="block[items_count &gt; 0][1]" mode="bracket-block-common" />
    </xsl:template>

    <xsl:template match="block" mode="bracket-block-common">
        <xsl:variable name="price" select="document(concat('udata://emarket/price/', .//st[position() = 1]/@id, '//0/'))/udata" />

        <div class="pf_product_line_entry" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
            <meta itemprop="priceCurrency" content="RUR" />
            <meta itemprop="price" content="{$price/price/actual}" />
            <meta itemprop="url" content="http://{$domain}{$request-uri}" />
            <xsl:choose>
                <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                    <meta itemprop="availability" content="https://schema.org/InStock" />
                </xsl:when>
                <xsl:otherwise>
                    <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                </xsl:otherwise>
            </xsl:choose>

            <div class="pf_productbox_price">
                <div class="pf_price">
                    <xsl:if test="$price/price/original">
                        <del><xsl:value-of select="concat(format-number(number($price/price/original), '#&#160;###,##','price'), ' ', $price/price/@suffix)" /></del>&#160;
                        <xsl:apply-templates select="$price/discount" mode="default-assets-emarket-discount-info" />
                    </xsl:if>
                </div>
                <div class="pf_price last">
                    <span>
                        <xsl:value-of select="format-number(number($price/price/actual), '#&#160;###,##','price')" />
                        <small>&#160;руб</small>
                    </span>
                </div>
            </div>
            <xsl:choose>
                <xsl:when test="count(../block[items_count &gt; 0]) &gt; 1">
                    <div class="pf_product_line_short">Стандартный торк <xsl:value-of select="current()/items_count" /> шт.</div>
                    <xsl:apply-templates select="../block[items_count &gt; 0]" mode="bracket-block" />
                </xsl:when>
                <xsl:otherwise>
                    <div class="pf_product_line_short">Стандартный торк, паз <xsl:value-of select="current()/paz/@name" />, <xsl:value-of select="current()/items_count" /> шт.</div>
                    <xsl:apply-templates select="../block[items_count &gt; 0]" mode="bracket-block-one" />
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="block" mode="bracket-block" />
    <xsl:template match="block[items_count]" mode="bracket-block">
        <button class="pf_btn pf_btn_more js-buy-paz" data-id="{current()/paz/@id}">Купить паз <xsl:value-of select="current()/paz/@name" />, <xsl:value-of select="format-number(number(current()/price/actual), '#&#160;###,##','price')" /> руб.</button>
    </xsl:template>

    <xsl:template match="block" mode="bracket-block-one" />
    <xsl:template match="block[items_count]" mode="bracket-block-one">
        <button class="pf_btn pf_btn_more js-buy-paz" data-id="{current()/paz/@id}">Купить <xsl:value-of select="format-number(number(current()/price/actual), '#&#160;###,##','price')" /> руб.</button>
    </xsl:template>


    <!--таблица брекетов-->
    <xsl:template match="udata" mode="bracket_tables" />
    <xsl:template match="udata[.//item]" mode="bracket_tables">
        <div class="row">
            <div class="col-lg-12">
                <div class="white-pnl brackets clearfix">
                    <div class="row">
                        <div class="col-xs-3">
                            <xsl:apply-templates select="." mode="paz_select" />

                            <div class="">
                                <span class="blue">Верхняя челюсть</span>
                            </div>
                            <div class="price">
                                <div>Цена</div>
                                <xsl:choose>
                                    <xsl:when test="not(.//item[1]//price = .//item[1]//price_origin)">
                                    	<div>
                                            за ед.: <span class="bracket_price"><xsl:value-of select=".//item//price_origin" /> руб.</span>
                                        </div>
                                        <div>
                                            ваша цена: <span class="bracket_price_origin"><xsl:value-of select=".//item//price" /> руб.</span>
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                    	<div>
                                            за ед.: <span class="bracket_price"><xsl:value-of select=".//item//price" /> руб.</span>
                                        </div>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <div>
                                    всего: <span class="bracket_price_sum">- </span>
                                </div>
                            </div>
                            <div class="">
                                <span class="blue">Нижняя челюсть</span>
                            </div>
                        </div>
                        <div class="col-xs-9 info">
                            <xsl:apply-templates select="block[.//item]" mode="paz_table" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 text-center">
                            <button class="btn btn-blue js-select-top-bottom" data-gtm-event="ButtonClicks" data-gtm-event-value="Выбрать верхнюю и нижнюю челюсть, стандартный токен">Выбрать верхнюю<br />и нижнюю челюсть,<br />стандартный торк</button>
                        </div>
                        <div class="col-sm-4 text-center">
                            <button class="btn btn-blue js-select-bottom" data-gtm-event="ButtonClicks" data-gtm-event-value="Выбрать нижнюю челюсть, стандартный токен">Выбрать нижнюю<br /> челюсть,<br />стандартный торк</button>
                        </div>
                        <div class="col-sm-4 text-center">
                            <button class="btn btn-blue js-select-top" data-gtm-event="ButtonClicks" data-gtm-event-value="Выбрать верхнюю челюсть, стандартный токен">Выбрать верхнюю<br /> челюсть,<br />стандартный торк</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn-primary btn-buy main">Купить выбранное</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>


    <!-- paz_select -->
    <xsl:template match="udata" mode="paz_select" />
    <xsl:template match="udata[block]" mode="paz_select">
        <label for="slot">Выбор паза</label>
        <select class="selectpicker small" id="slot">
            <xsl:if test="not(count(block[item]) &gt; 1)">
                <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="block[item]" mode="paz_select" />
        </select>
    </xsl:template>
    <xsl:template match="block" mode="paz_select">
        <option value="{paz/@id}">
            <xsl:if test="position()=1">
                <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="paz/@name" />
        </option>
    </xsl:template>


    <!-- Картинка зуба -->
    <xsl:template match="block" mode="tooth">
        <xsl:param name="tooth_number" />
        <td data-tooth="{$tooth_number}">
            <div class="tooth disable">
                <xsl:if test=".//item[@tooth=$tooth_number]">
                    <xsl:attribute name="class">tooth</xsl:attribute>
                </xsl:if>
                <img src="{$template-resources}img/tooth-disable-icon.png" alt="tooth">
                    <xsl:if test=".//item[@tooth=$tooth_number]">
                        <xsl:attribute name="src"><xsl:value-of select="concat($template-resources, 'img/tooth-icon.png')" /></xsl:attribute>
                    </xsl:if>
                </img>
                <span><xsl:value-of select="$tooth_number" /></span>
            </div>
        </td>
    </xsl:template>

    <!-- Крючок -->
    <xsl:template match="block" mode="hk">
        <xsl:param name="tooth_number" />
        <td data-tooth="{$tooth_number}">
            <!-- вывод торков -->
            <xsl:if test="item[@tooth=$tooth_number]/child::*/child::*/hk = 1">
                <!-- <input type="checkbox" class="hk">
                    <xsl:if test="not(item[@tooth=$tooth_number]/child::*/child::*/hk = 0)">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                        <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                </input> -->
                <xsl:choose>
                    <!-- заблокировать состояние, если нет ни одного торка без крючка -->
                    <xsl:when test="not(item[@tooth=$tooth_number]/child::*/child::*/hk = 0)">
                        <input type="hidden" class="hk" checked="checked" />
                        <input type="checkbox" class="hk_fake" checked="checked" readonly="readonly" disabled="disabled"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <input type="checkbox" class="hk" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </td>
    </xsl:template>

    <!-- смещение -->
    <xsl:template match="block" mode="go">
        <xsl:param name="tooth_number" />
        <td data-tooth="{$tooth_number}">
            <!-- вывод торков -->
            <xsl:if test="item[@tooth=$tooth_number]/child::*/child::*/go = 1">
                <!-- <input type="checkbox" class="go">
                    <xsl:if test="">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                        <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                </input> -->
                <xsl:choose>
                    <!-- заблокировать состояние, если нет ни одного торка без смещения -->
                    <xsl:when test="not(item[@tooth=$tooth_number]/child::*/child::*/go = 0)">
                        <input type="hidden" class="go" checked="checked" />
                        <input type="checkbox" class="go_fake" checked="checked" readonly="readonly" disabled="disabled"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <input type="checkbox" class="go" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </td>
    </xsl:template>

    <!-- вывод блока со полем для кол-ва торков -->
    <xsl:template match="block" mode="number">
        <xsl:param name="tooth_number" />

        <td data-tooth="{$tooth_number}">
            <input type="number" min="0">
                <xsl:if test="not(.//item[@tooth=$tooth_number])">
                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                </xsl:if>
            </input>
        </td>
    </xsl:template>

    <!-- вывод блока со списками торков -->
    <xsl:template match="block" mode="torc">
        <xsl:param name="tooth_number" />

        <td data-tooth="{$tooth_number}">
            <!-- вывод торков -->
            <xsl:apply-templates select=".//item[@tooth=$tooth_number]/child::*" mode="torc">
                <xsl:sort select="@name"/>
            </xsl:apply-templates>
        </td>
    </xsl:template>

    <!-- вывод списка торков -->
    <xsl:template match="*" mode="torc">
        <div>
            <xsl:attribute name="class">tork_selector<xsl:text> </xsl:text>
                <xsl:value-of select="@name" /><xsl:text> </xsl:text>hk<xsl:value-of select="child::*/hk" /><xsl:text> </xsl:text>go<xsl:value-of select="child::*/go" /><xsl:text> </xsl:text><xsl:if test="not(position()=1)">hide_it<xsl:text> </xsl:text></xsl:if>
            </xsl:attribute>

            <select class="selectpicker small torc">
                <xsl:choose>
                    <xsl:when test="count(child::*) &gt; 1">
                        <option value="" selected="selected">&#160;</option>
                        <xsl:apply-templates select="child::*" mode="torc_value">
                            <xsl:sort order = "ascending" select="tork/@id"/>
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="disabled">disabled</xsl:attribute>
                        <xsl:apply-templates select="child::*" mode="torc_value_single">
                            <xsl:sort order = "ascending" select="tork/@id"/>
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
                <!-- <option value="" selected="selected">&#160;</option>
                <xsl:apply-templates select="child::*" mode="torc_value">
                    <xsl:sort order = "ascending" select="tork/@id"/>
                </xsl:apply-templates> -->
            </select>
        </div>
    </xsl:template>

    <!-- вывод значения торка -->
    <xsl:template match="*" mode="torc_value">
        <option data-price="{price}" data-price-origin="{price_origin}" value="{@id}">
            <xsl:value-of select="tork/@name" disable-output-escaping="yes" />
        </option>
    </xsl:template>

    <xsl:template match="*" mode="torc_value_single">
        <option data-price="{price}" data-price-origin="{price_origin}" value="{@id}" selected="selected">
            <xsl:value-of select="tork/@name" disable-output-escaping="yes" />
        </option>
    </xsl:template>

    <!-- paz table -->
    <xsl:template match="block" mode="paz_table">
        <div class="table-responsive paz_table hide_it" id="paz_{paz/@id}">
            <xsl:if test="position()=1">
                <xsl:attribute name="class">table-responsive paz_table</xsl:attribute>
            </xsl:if>
            <table>
                <tr>
                    <td></td>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="15"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="14"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="13"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="12"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="11"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="21"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="22"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="23"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="24"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="25"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-top-torc js-torc">
                    <td class="title">
                        торк
                    </td>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="15"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="14"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="13"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="12"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="11"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="21"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="22"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="23"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="24"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="25"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-top-numbers js-numbers">
                    <td class="title">количество</td>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="15"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="14"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="13"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="12"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="11"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="21"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="22"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="23"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="24"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="25"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-hk" style="display:none">
                    <xsl:if test=" item[@tooth=15]/child::*/child::*/hk = 1
                                    or item[@tooth=14]/child::*/child::*/hk = 1
                                    or item[@tooth=13]/child::*/child::*/hk = 1
                                    or item[@tooth=12]/child::*/child::*/hk = 1
                                    or item[@tooth=11]/child::*/child::*/hk = 1
                                    or item[@tooth=21]/child::*/child::*/hk = 1
                                    or item[@tooth=22]/child::*/child::*/hk = 1
                                    or item[@tooth=23]/child::*/child::*/hk = 1
                                    or item[@tooth=24]/child::*/child::*/hk = 1
                                    or item[@tooth=25]/child::*/child::*/hk = 1">
                        <xsl:attribute name="style"></xsl:attribute>
                    </xsl:if>
                    <td class="title">
                        крючок
                    </td>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="15"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="14"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="13"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="12"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="11"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="21"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="22"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="23"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="24"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="25"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-go" style="display:none">
                	<xsl:if test=" item[@tooth=15]/child::*/child::*/go = 1
                                        or item[@tooth=14]/child::*/child::*/go = 1
                                        or item[@tooth=13]/child::*/child::*/go = 1
                                        or item[@tooth=12]/child::*/child::*/go = 1
                                        or item[@tooth=11]/child::*/child::*/go = 1
                                        or item[@tooth=21]/child::*/child::*/go = 1
                                        or item[@tooth=22]/child::*/child::*/go = 1
                                        or item[@tooth=23]/child::*/child::*/go = 1
                                        or item[@tooth=24]/child::*/child::*/go = 1
                                        or item[@tooth=25]/child::*/child::*/go = 1">
                            <xsl:attribute name="style"></xsl:attribute>
                	</xsl:if>
                    <td class="title">
                        смещение
                    </td>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="15"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="14"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="13"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="12"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="11"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="21"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="22"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="23"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="24"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="25"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="lower-jaw-torc js-bottom-torc js-torc">
                    <td class="title">
                        торк
                    </td>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="45"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="44"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="43"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="42"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="41"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="31"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="32"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="33"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="34"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="torc">
                        <xsl:with-param name="tooth_number" select="35"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-bottom-numbers js-numbers">
                    <td class="title">количество</td>
                     <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="45"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="44"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="43"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="42"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="41"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="31"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="32"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="33"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="34"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="number">
                        <xsl:with-param name="tooth_number" select="35"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-hk" style="display:none">
                	<xsl:if test=" item[@tooth=45]/child::*/child::*/hk = 1
                                        or item[@tooth=44]/child::*/child::*/hk = 1
                                        or item[@tooth=43]/child::*/child::*/hk = 1
                                        or item[@tooth=42]/child::*/child::*/hk = 1
                                        or item[@tooth=41]/child::*/child::*/hk = 1
                                        or item[@tooth=31]/child::*/child::*/hk = 1
                                        or item[@tooth=32]/child::*/child::*/hk = 1
                                        or item[@tooth=33]/child::*/child::*/hk = 1
                                        or item[@tooth=34]/child::*/child::*/hk = 1
                                        or item[@tooth=35]/child::*/child::*/hk = 1">
                            <xsl:attribute name="style"></xsl:attribute>
                	</xsl:if>
                    <td class="title">
                        крючок
                    </td>
                     <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="45"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="44"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="43"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="42"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="41"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="31"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="32"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="33"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="34"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="hk">
                        <xsl:with-param name="tooth_number" select="35"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="js-go" style="display:none">
                	<xsl:if test=" item[@tooth=45]/child::*/child::*/go = 1
                                        or item[@tooth=44]/child::*/child::*/go = 1
                                        or item[@tooth=43]/child::*/child::*/go = 1
                                        or item[@tooth=42]/child::*/child::*/go = 1
                                        or item[@tooth=41]/child::*/child::*/go = 1
                                        or item[@tooth=31]/child::*/child::*/go = 1
                                        or item[@tooth=32]/child::*/child::*/go = 1
                                        or item[@tooth=33]/child::*/child::*/go = 1
                                        or item[@tooth=34]/child::*/child::*/go = 1
                                        or item[@tooth=35]/child::*/child::*/go = 1">
                            <xsl:attribute name="style"></xsl:attribute>
                	</xsl:if>
                    <td class="title">
                        смещение
                    </td>
                     <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="45"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="44"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="43"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="42"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="41"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="31"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="32"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="33"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="34"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="go">
                        <xsl:with-param name="tooth_number" select="35"/>
                    </xsl:apply-templates>
                </tr>
                <tr class="lower-jaw-tooth">
                    <td></td>
                     <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="45"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="44"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="43"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="42"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="41"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="31"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="32"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="33"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="34"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="tooth">
                        <xsl:with-param name="tooth_number" select="35"/>
                    </xsl:apply-templates>
                </tr>
            </table>
        </div>

        <!-- Button trigger modal -->
        <div style="display:none;">
            <button type="button" class="btn btn-primary btn-lg empty_tork_modal_start" data-toggle="modal" data-target="#empty_tork_modal" />
        </div>

        <!-- Modal -->
        <div class="modal fade" id="empty_tork_modal" tabindex="-1" role="dialog" aria-labelledby="empty_tork_modal_label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="modal-title header4 text-center" id="empty_tork_modal_label">Выберите незаполненный торк</div>
                    </div>
                    <div class="modal-body text-center" id="empty_tork_modal_content" style="font-weight:bold"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>

    </xsl:template>

    <!--список товаров-->
    <xsl:template match="udata" mode="smartCatalog_chooseBrakckets">
    	Ничего нет
    </xsl:template>
    <xsl:template match="udata[lines/item]" mode="smartCatalog_chooseBrakckets">
    	<xsl:param name="objects_list_title_on_blue_bg"  />
    	<div class="row">
            <div class="col-lg-12">
                <div class="white-pnl products-list">
                    <div class="header2">
                        <xsl:choose>
                            <xsl:when test="$objects_list_title_on_blue_bg">
                                <xsl:value-of select="$objects_list_title_on_blue_bg" disable-output-escaping="yes" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$document-name" />: все товарные позиции
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                    <button class="btn btn-primary btn-show-details" type="button" data-toggle="collapse" data-show-when-expanded="true" data-target="#showProducts" aria-expanded="true" aria-controls="showProducts">
                        <span>Показать</span><span>Свернуть</span>
                    </button>
                    <!-- <button class="btn btn-primary btn-show-details " type="button" data-toggle="collapse" data-target="#showProducts" aria-expanded="true" aria-controls="showProducts">
                        <span>Показать</span><span>Свернуть</span>
                    </button> -->
                    <div id="showProducts" class="collapse in js-collapse-pnl">
                        <ul>
                            <xsl:apply-templates select="lines/item" mode="short_item_chooseBrakckets" />
                        </ul>
                        <xsl:apply-templates select="total" />
                    </div>
                    <button class="btn btn-primary btn-show-details" type="button" data-toggle="collapse" data-target="#showProducts" aria-expanded="true" aria-controls="showProducts">
                        <span>Показать</span><span>Свернуть</span>
                    </button>
                    <!-- <button class="btn btn-primary btn-show-details " type="button" data-toggle="collapse" data-target="#showProducts" aria-expanded="true" aria-controls="showProducts">
                        <span>Показать</span><span>Свернуть</span>
                    </button> -->
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- краткая карточка товара брекета -->
    <xsl:template match="item" mode="short_item_chooseBrakckets">
    	<xsl:param name="is_options" select="false()" />
    	<xsl:variable name="item_name">
            <xsl:choose>
                <xsl:when test=".//property[@name='h1_alternative']/value">
                    <xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="text()" />
                </xsl:otherwise>
            </xsl:choose>
    	</xsl:variable>
    	<xsl:variable name="price" select="document(concat('udata://emarket/price/',@id,'//0/'))/udata" />

    	<li>
            <xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommerceImpression/', @id, '/', $price/price/actual, '/(', php:function('urlencode', 'Каталог'), ')/', position(), '/1/'))/udata" />

            <div class="row">
                <div class="col-xs-3 left">
                    <a href="{@link}"  class="ga_productclick"
                        data-id="{.//property[@name='artikul']/value}"
                        data-name="{$item_name}"
                        data-category="{$ec_category}"
                        data-brand="{.//property[@name='breket_sistema']/value}"
                        data-position="{position()}"
                        data-list="{$document-header}">
                        <xsl:call-template name="makeThumbnailFull_ByPath">
                            <xsl:with-param name="source" select=".//property[@name='photo']/value" />
                            <xsl:with-param name="width" select="190" />
                            <xsl:with-param name="height" select="190" />

                            <xsl:with-param name="empty">&empty-photo;</xsl:with-param>
                            <xsl:with-param name="element-id" select="@id" />
                            <xsl:with-param name="field-name" select="'photo'" />

                            <!-- <xsl:with-param name="alt" select="text()" /> -->
                            <xsl:with-param name="class" select="'img-responsive'" />
                        </xsl:call-template>
                    </a>
                </div>
                <div class="col-xs-9 right">
                    <a href="{@link}" class="lnk-to-product ga_productclick"
                        data-id="{.//property[@name='artikul']/value}"
                        data-name="{$item_name}"
                        data-category="{$ec_category}"
                        data-brand="{.//property[@name='breket_sistema']/value}"
                        data-position="{position()}"
                        data-list="{$document-header}">
                        <xsl:value-of select="$item_name" />
                    </a>
                    <div class="row">
                        <div class="col-sm-4">
                            <xsl:if test=".//property[@name='artikul']/value">
                            <p><span class="blue">Артикул</span>&#160;<xsl:value-of select=".//property[@name='artikul']/value" /></p>
                        </xsl:if>
                            <xsl:if test="$price/price/actual &gt; 0">
                                <xsl:if test="$price/discount">
                                    <div class="pf_amount">
                                        <span class="pf_amount_name">Скидка:</span>
                                        <xsl:text> </xsl:text>
                                        <span class="pf_amount_val">
                                            <xsl:value-of select="$price/discount/total/@percent" />%,
                                            <xsl:text> </xsl:text>
                                            <xsl:value-of select="concat(format-number(number($price/discount/total/node()), '#&#160;###,##','price'), ' ', $price/price/@suffix)" />
                                        </span>
                                        <xsl:text> </xsl:text>
                                        <xsl:apply-templates select="$price/discount" mode="default-assets-emarket-discount-info" />
                                    </div>
                                </xsl:if>
                                <div class="pf_amount large" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
                                    <span class="pf_amount_name">Цена:</span>
                                    <xsl:text> </xsl:text>
                                    <span class="pf_amount_val">
                                        <xsl:apply-templates select="$price" mode="object-price" />
                                    </span>
                                </div>
                            </xsl:if>
                        </div>
                        <div class="col-sm-8">
                            <div class="already-select" id="add_basket_mark_{@id}" style="display:none">
                                <xsl:if test="$cart/items/item/page/@id = @id">
                                    <xsl:attribute name="style"></xsl:attribute>
                                </xsl:if>
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>Товар был добавлен в корзину<br/>
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i><a href="/emarket/cart">Оформить заказ</a>
                            </div>

                            <xsl:choose>
                                <xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
                                        <div class="availability yes"><i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
                                </xsl:when>
                                <xsl:otherwise>
                                        <div class="availability no"><i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </div>

                    <!-- прячем кнопку купить и поля для указания кол-ва, если товара нет в наличии -->
                    <xsl:if test=".//property[@name='common_quantity']/value &gt; 0">
                        <div class="input-group spinner forBuy">
                            <label for="amount_id_{@id}" class="blue">Количество</label>
                            <input id="amount_id_{@id}" type="text" class="form-control" value="1" />
                            <div class="input-group-btn-vertical">
                                <button class="btn btn-default" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                                <button class="btn btn-default" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <a id="add_basket_{@id}" href="{$lang-prefix}/emarket/basket/put/element/{@id}/" class="btn-primary btn-buy basket_list options_{$is_options} ga_productbuy"
                            data-id="{.//property[@name='artikul']/value}"
                            data-name="{$item_name}"
                            data-category="{$ec_category}"
                            data-brand="{.//property[@name='breket_sistema']/value}"
                            data-price="{$price/price/actual}"
                            data-qty="1">
                            <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceProductData/', @id, '/', $price/price/actual, '/'))/udata" mode="ecommerce-add" />
                            Купить</a>
                    </xsl:if>
                </div>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>
