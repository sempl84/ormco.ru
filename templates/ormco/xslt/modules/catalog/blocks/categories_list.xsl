<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	exclude-result-prefixes="xsl umi">

    <xsl:template match="udata[@method = 'getCategoryList']">
        <div class="categories">
            <div class="row">
                <ul umi:element-id="{@category-id}" umi:region="list" umi:module="catalog" umi:sortable="sortable" umi:button-position="top right">
                    <xsl:apply-templates select="//item" />
                </ul>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'getCategoryList']//item">
        <li class="col-sm-4 col-xs-6 item" umi:element-id="{@id}" umi:region="row">
            <a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
                <xsl:call-template name="makeThumbnailFull_ByPath">
					<xsl:with-param name="source" select=".//property[@name='header_pic']/value" />
					<xsl:with-param name="width" select="190" />
					<xsl:with-param name="height" select="190" />

					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="element-id" select="@id" />
					<xsl:with-param name="field-name" select="'header_pic'" />

                    <xsl:with-param name="alt" select="text()" />
					<xsl:with-param name="class" select="'img-responsive'" />
				</xsl:call-template>
                <div class="title">
                    <p>
                        <xsl:value-of select="text()" />
                    </p>
                </div>
            </a>
        </li>
    </xsl:template>


    <!-- категории в левой колонке -->
    <xsl:template match="udata[@method = 'getCategoryList']" mode="categoryListLeft">
        <div class="left-search">
            <form class="show-placeholder" action="/search/search_do/" method="get">
                <input type="text" placeholder="Поиск по каталогу" name="search_string" class="smart_filter_search" id="search_input_autocomplete" />
                <span></span>
                <input type="hidden" name="search_branches" value="4" class="smart_filter_search_search_branches"/>
                <!-- <input type="text" placeholder="Поиск по сайту" name="search_string"/><span></span> -->
            </form>
            <div class="smart_filter_search_dropdown_hint"></div>
        </div>

        <div class="left-nav filters collapse js-filter js-collapse-pnl" id="showFiltersCat" data-category="{@category-id}">
            <nav class="">
            	<ul umi:element-id="{@category-id}" umi:region="list" umi:module="catalog" umi:sortable="sortable" umi:button-position="top right">
                    <xsl:apply-templates select="//item" mode="categoryListLeft"/>
                </ul>
            </nav>
        </div>
        <button class="btn btn-primary btn-show-details btn-show-filter collapsed" type="button" data-toggle="collapse" data-target="#showFiltersCat" aria-expanded="false" aria-controls="showFilters">
            <span>Показать разделы</span><span>Свернуть разделы</span>
        </button>
    </xsl:template>


	<xsl:template match="udata[@method = 'getCategoryList']//item" mode="categoryListLeft">
		<li umi:element-id="{@id}" umi:region="row">
			<xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if>
			<a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="." />
			</a>
		</li>
	</xsl:template>


	<!-- все разделы в шапке сайта -->
    <xsl:template match="udata[@method = 'getCategoryListFill']" mode="getCategoryListFill">

        <!-- <xsl:apply-templates select="items/item" mode="getCategoryListFillCsv"/> -->
        <div class="dropdown js-product-menu">
            <a role="button" data-toggle="dropdown" class="btn btn-primary" href="#" data-gtm-event="ButtonClicks" data-gtm-event-value="Продукция">
                Продукция <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" umi:element-id="{@category-id}" umi:region="list" umi:sortable="sortable" umi:button-position="top right">
				<xsl:apply-templates select="items/item" mode="getCategoryListFill"/>

            </ul>
        </div>
	</xsl:template>


<!-- <xsl:template match="item" mode="getCategoryListFillCsv">
<xsl:value-of select="@id" />;<xsl:value-of select="@name" />;;;;
</xsl:template>
<xsl:template match="item[items/item]" mode="getCategoryListFillCsv">
<xsl:value-of select="@id" />;<xsl:value-of select="@name" />;;;;
<xsl:apply-templates select="items/item" mode="getCategoryListFillSubCsv" />
</xsl:template>

<xsl:template match="item" mode="getCategoryListFillSubCsv">
<xsl:value-of select="@id" />;;<xsl:value-of select="@name" />;;;
</xsl:template>

<xsl:template match="item[items/item]" mode="getCategoryListFillSubCsv">
<xsl:value-of select="@id" />;;<xsl:value-of select="@name" />;;;
<xsl:apply-templates select="items/item" mode="getCategoryListFillSub2Csv" />
</xsl:template>

<xsl:template match="item" mode="getCategoryListFillSub2Csv">
<xsl:value-of select="@id" />;;;<xsl:value-of select="@name" />;;
</xsl:template>

<xsl:template match="item[items/item]" mode="getCategoryListFillSub2Csv">
<xsl:value-of select="@id" />;;;<xsl:value-of select="@name" />;;
<xsl:apply-templates select="items/item" mode="getCategoryListFillSub3Csv" />
</xsl:template>

<xsl:template match="item" mode="getCategoryListFillSub3Csv">
<xsl:value-of select="@id" />;;;;<xsl:value-of select="@name" />;
</xsl:template>

<xsl:template match="item[items/item]" mode="getCategoryListFillSub3Csv">
<xsl:value-of select="@id" />;;;;<xsl:value-of select="@name" />;
<xsl:apply-templates select="items/item" mode="getCategoryListFillSub3Csv" />
</xsl:template> -->


	<xsl:template match="item" mode="getCategoryListFill">
		<li umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
		</li>
	</xsl:template>

	<xsl:template match="item[items/item]" mode="getCategoryListFill">
		<li class="dropdown-submenu second-level" umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">dropdown-submenu second-level active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" role="button" data-toggle="dropdown" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
			<xsl:apply-templates select="items[item]" mode="getCategoryListFillSub" />
        </li>
	</xsl:template>

	<xsl:template match="items" mode="getCategoryListFillSub">
		<ul class="dropdown-menu">
			<xsl:apply-templates select="item" mode="getCategoryListFillSub"/>
        </ul>
	</xsl:template>

	<xsl:template match="item" mode="getCategoryListFillSub">
		<li umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
		</li>
	</xsl:template>

	<xsl:template match="item[items/item]" mode="getCategoryListFillSub">
		<li class="dropdown-submenu" umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">dropdown-submenu active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" role="button" data-toggle="dropdown" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
			<xsl:apply-templates select="items[item]" mode="getCategoryListFillSub2" />
        </li>
	</xsl:template>

	<xsl:template match="items" mode="getCategoryListFillSub2">
		<ul class="dropdown-menu">
			<xsl:apply-templates select="item" mode="getCategoryListFillSub2"/>
        </ul>
	</xsl:template>

	<xsl:template match="item" mode="getCategoryListFillSub2">
		<li umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
		</li>
	</xsl:template>

	<xsl:template match="item[items/item]" mode="getCategoryListFillSub2">
		<li class="dropdown-submenu third-level" umi:element-id="{@id}" umi:region="row">
			<!-- <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
				<xsl:attribute name="class">dropdown-submenu active</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" role="button" data-toggle="dropdown" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<xsl:value-of select="@name" />
			</a>
			<xsl:apply-templates select="items[item]" mode="getCategoryListFillSub" />
        </li>
	</xsl:template>

</xsl:stylesheet>