<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- <xsl:template match="result" mode="header">
		<h1>
			<xsl:value-of select="@header" />
		</h1>
	</xsl:template>
	
	<xsl:template match="result[@pageId]" mode="header">
		<h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="&empty-page-name;">
			<xsl:value-of select="@header" />
		</h1>
	</xsl:template> -->
	
	<xsl:include href="pages/default.xsl" />
	<xsl:include href="pages/404.xsl" />
	<xsl:include href="pages/sitemap.xsl" />
	<xsl:include href="pages/main.xsl" />
	<xsl:include href="pages/contacts.xsl" />
	<xsl:include href="blocks/viewed.xsl" />

</xsl:stylesheet>