<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">


	<xsl:template match="/result[@method = 'notfound']" priority="1">
		<xsl:variable name="error-page" select="document(concat('upage://(', @pre-lang, '/notfound/)'))/udata/page" />
		<script>
			jQuery(document).ready(function(){
				ga('send', 'event', '404error', '{<xsl:value-of select="@request-uri" disable-output-escaping="yes" />}', {nonInteraction: true});
				ym(10184890, 'reachGoal', '404Error');
			});


		</script>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid news-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-building" aria-hidden="true">
	                    	<xsl:if test="$error-page//property[@name='icon_style']/value">
								<xsl:attribute name="class"><xsl:value-of select="concat('fa ',$error-page//property[@name='icon_style']/value)" /></xsl:attribute>
							</xsl:if>
	                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl-simple">
						<article class="notfound" umi:element-id="{$error-page/@id}" umi:field-name="content">
							<xsl:value-of select="$error-page//property[@name = 'content']/value" disable-output-escaping="yes" />

	                    </article>
	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

	                <xsl:apply-templates select="." mode="right_col" />

	            </div>
	        </div>
	    </div>
	</xsl:template>
</xsl:stylesheet>