<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'content']">
        <div class="article-page">
            <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-home" aria-hidden="true">
                        <xsl:if test=".//property[@name='icon_style']/value">
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                            </xsl:attribute>
                        </xsl:if>
                    </i>
                    <h1>
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                    <xsl:call-template name="header_social_link" />
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
                        <!--Страница оплаты-->
                        <xsl:if test="$document-page-id = 14011">
                            <xsl:apply-templates select="document('udata://emarket/try_order_pay')/udata" mode="payment_online" />
                        </xsl:if>

                        <article class="white-pnl content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
                        </article>
                        <xsl:call-template name="social_share" />
                    </div>
                    <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
                        <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                        <xsl:apply-templates select="." mode="right_col" />
                    </aside>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>