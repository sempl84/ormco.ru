<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'content' and page/@alt-name = 'contact']">
		<div class="contacts-page">
	        <!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="fa fa-home" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    <xsl:call-template name="header_social_link" />
	            </div>
	        </div>

	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <div class="row">
	                    	<div class="col-sm-6 col-xs-12">
	                            <div class="address">
	                                <div class="iframe-container">
	                                    <div class="map" data-title="Малоохтинский пр-т, д. 64, корп. 3" data-lat="{.//property[@name='lat2']/value}" data-lng="{.//property[@name='lng2']/value}" data-zoom="14">
	                                        <div class="hide js-description">
	                                            <div class="header3">Петербургский офис ORMCO</div>
	                                            <p>
	                                                <xsl:value-of select=".//property[@name='adres2']/value" disable-output-escaping="yes" /><br />
	                                                <b>Тел.:</b> <xsl:value-of select=".//property[@name='tel2']/value" disable-output-escaping="yes" /><br />
	                                                <b>E-mail:</b> <a href="mailto:{.//property[@name='email2']/value}"><xsl:value-of select=".//property[@name='email2']/value" disable-output-escaping="yes" /></a>
	                                            </p>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="bottom" >

	                                    <div class="header3" >Петербургский офис ORMCO</div>
	                                    <p >
											<xsl:value-of select=".//property[@name='adres2']/value" disable-output-escaping="yes" />
											<br />
											Тел.: <xsl:value-of select=".//property[@name='tel2']/value" disable-output-escaping="yes" /><br />
											E-mail: <a href="mailto:{.//property[@name='email2']/value}"><xsl:value-of select=".//property[@name='email2']/value" disable-output-escaping="yes" /></a>
	                                        <xsl:if test=".//property[@name='whatsapp2']/value">
	                                       		<br />Вы можете оформить заказ по WhatsApp
	                                       		<br /><i class="fa fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone={.//property[@name='whatsapp_link2']/value}"><xsl:value-of select=".//property[@name='whatsapp2']/value" disable-output-escaping="yes" /></a>
	                                    	</xsl:if>
	                                    </p>
	                                    <div>
	                                        <span class="blue">Режим работы</span>
	                                        <xsl:value-of select=".//property[@name='jobt2']/value" disable-output-escaping="yes" />
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-6 col-xs-12">
	                            <div class="address">
	                                <div class="iframe-container">
	                                    <div class="map" data-title="Ленинградский проспект, д. 37, корп. 9" data-lat="{.//property[@name='lat1']/value}" data-lng="{.//property[@name='lng1']/value}" data-zoom="14">
	                                        <div class="hide js-description">
	                                            <div class="header3">Московский офис ORMCO</div>
	                                            <p>
	                                                <xsl:value-of select=".//property[@name='adres1']/value" disable-output-escaping="yes" /><br />
	                                                <b>Тел.:</b> <xsl:value-of select=".//property[@name='tel1']/value" disable-output-escaping="yes" /><br />
	                                                <b>E-mail:</b> <a href="mailto:{.//property[@name='email1']/value}"><xsl:value-of select=".//property[@name='email1']/value" disable-output-escaping="yes" /></a>
	                                            </p>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="bottom" >
	                                    <div class="header3" >Московский офис ORMCO</div>
	                                    <p>
											<xsl:value-of select=".//property[@name='adres1']/value" disable-output-escaping="yes" />
											<br />
	                                        Тел.: <xsl:value-of select=".//property[@name='tel1']/value" disable-output-escaping="yes" /><br />
											E-mail: <a href="mailto:{.//property[@name='email1']/value}"><xsl:value-of select=".//property[@name='email1']/value" disable-output-escaping="yes" /></a>
	                                    	<xsl:if test=".//property[@name='whatsapp1']/value">
	                                       		<br />Вы можете оформить заказ по WhatsApp
	                                       		<br /><i class="fa fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone={.//property[@name='whatsapp_link1']/value}"><xsl:value-of select=".//property[@name='whatsapp1']/value" disable-output-escaping="yes" /></a>
	                                    	</xsl:if>
	                                    </p>
	                                    <div>
	                                        <span class="blue">Режим работы</span>
	                                        <xsl:value-of select=".//property[@name='jobt1']/value" disable-output-escaping="yes" />
	                                    </div>
	                                </div>
	                            </div>
	                        </div>

	                    </div>
	                    <div class="row">
	                        <div class="col-lg-12">
	                            <div class="dealers white-pnl">
                                    <div class="header_contacts_3">Для обращений из других регионов:</div>
	                                <p>
                                        E-mail:  <a href="mailto:dc-sales@ormco.com">dc-sales@ormco.com</a><br />
                                        Телефон (звонок бесплатный):  <a href="tel:88002227414">8 (800) 222-74-14</a><br />
                                    </p>
	                                <a href="&dealers_url;"><i class="fa fa-briefcase" aria-hidden="true"></i>Наши дилеры</a>
	                            </div>
	                        </div>
	                    </div>

	                    <xsl:apply-templates select="document('udata://news/lastlents/&contacts_managers_pid;/notemplate/100/1')/udata" mode="contacts_managers" />


	                    <div class="row">
	                        <div class="col-lg-12">
	                            <div class="social-links white-pnl clearfix">
	                                <div class="header3">Мы в социальных сетях</div>
	                                <ul>
	                                	<!--<li><a class="circled-icon" href="https://www.facebook.com/ormcorussia" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
					                    <li><a class="circled-icon" href="http://vk.com/ormcorussia" target="_blank" ><i class="fa fa-vk" aria-hidden="true"></i></a></li>
					                    <li><a class="circled-icon" href="https://twitter.com/ormcorussia" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					                    <!--<li><a href="https://instagram.com/ormcorussia" target="_blank" class="circled-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
	                                    <li><a class="circled-icon" href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
	                                    <li><a class="circled-icon" href="https://t.me/ormcorussia" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
	                                </ul>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-lg-12">
	                            <div class="contact-form white-pnl">

	                                <xsl:apply-templates select="$errors" />
	                                <div class="header3">Написать в Ormco</div>

									<xsl:apply-templates select="document(concat('udata://webforms/add/', &feedback_form_oid;))/udata" mode="feedback_form_contacts">
										<xsl:with-param name="submit_text" select="'Отправить'"/>
									</xsl:apply-templates>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="col-lg-12">
	                            <div class="social-share text-right">
	                                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
	                                <script src="//yastatic.net/share2/share.js"></script>
	                                <div class="ya-share2" data-services="odnoklassniki,gplus,vkontakte,twitter" data-counter=""></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                    <!-- <a href="#"><img src="{$template-resources}img/discount/vertical-banner.jpg" alt="banner"></a> -->
	                </aside>
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata" mode="contacts_managers"></xsl:template>
	<xsl:template match="udata[items/item]"  mode="contacts_managers">
		<div class="row">
            <div class="col-lg-12">
                <div class="sales-department white-pnl">
                    <div class="header3">Контакты отдела продаж</div>
                    <div id="showContacts" class="collapse js-collapse-pnl">
                        <xsl:apply-templates select="items/item" mode="contacts_managers"/>
                        <!-- <span class="header4">Менеджеры по работе с клиентами</span>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Регион</th>
                                        <th>ФИО менеджера</th>
                                        <th>Телефон</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Санкт-Петербург</td>
                                        <td>Оксана Калинина</td>
                                        <td>8 (921) 767-70-72</td>
                                        <td><a href="mailto:Oksana.Kalinina@ormco.com">Oksana.Kalinina@ormco.com</a></td>
                                    </tr>
                                    <tr>
                                        <td>Санкт-Петербург</td>
                                        <td>Оксана Калинина</td>
                                        <td>8 (921) 767-70-72</td>
                                        <td><a href="mailto:Oksana.Kalinina@ormco.com">Oksana.Kalinina@ormco.com</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span class="header4">Менеджеры по работе с клиентами</span>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ФИО менеджера</th>
                                        <th>Телефон</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Оксана Калинина</td>
                                        <td>8 (921) 767-70-72</td>
                                        <td><a href="mailto:Oksana.Kalinina@ormco.com">Oksana.Kalinina@ormco.com</a></td>
                                    </tr>
                                    <tr>
                                        <td>Оксана Калинина</td>
                                        <td>8 (921) 767-70-72</td>
                                        <td><a href="mailto:Oksana.Kalinina@ormco.com">Oksana.Kalinina@ormco.com</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->
                    </div>
                    <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#showContacts" aria-expanded="true" aria-controls="showContacts">
                        <span>Показать контакты отдела продаж</span><span>Свернуть</span>
                    </button>
                </div>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="udata[@module='news' and @method='lastlents']/items/item" mode="contacts_managers">
		<xsl:choose>
			<xsl:when test="position()=1">
				<xsl:apply-templates select="document(concat('udata://news/lastlist/',@id,'//1000/1?extProps=region,telefon,email'))/udata" mode="contacts_managers_block_first" >
					<xsl:with-param name="lent_name" select="text()"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="document(concat('udata://news/lastlist/',@id,'//1000/1?extProps=region,telefon,email'))/udata" mode="contacts_managers_block" >
					<xsl:with-param name="lent_name" select="text()"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>


	</xsl:template>

	<xsl:template match="udata" mode="contacts_managers_block_first" />
	<xsl:template match="udata[items/item]" mode="contacts_managers_block_first">
		<xsl:param name="lent_name" select="''" />

		<span class="header4"><xsl:value-of select="$lent_name" disable-output-escaping="yes" /></span>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Регион</th>
                        <th>ФИО менеджера</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:apply-templates select="items/item" mode="contacts_managers_block_first" />
                </tbody>
            </table>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="contacts_managers_block_first">
		<tr>
            <td><xsl:value-of select=".//property[@name='region']/value" /></td>
            <td><xsl:value-of select="text()"  disable-output-escaping="yes"/></td>
            <td><xsl:value-of select=".//property[@name='telefon']/value" /></td>
            <td><a href="mailto:{.//property[@name='email']/value}"><xsl:value-of select=".//property[@name='email']/value" /></a></td>
        </tr>
	</xsl:template>


	<xsl:template match="udata" mode="contacts_managers_block" />
	<xsl:template match="udata[items/item]" mode="contacts_managers_block">
		<xsl:param name="lent_name" select="''" />

		<span class="header4"><xsl:value-of select="$lent_name" disable-output-escaping="yes" /></span>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ФИО менеджера</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:apply-templates select="items/item" mode="contacts_managers_block" />
                </tbody>
            </table>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="contacts_managers_block">
		<tr>
            <td><xsl:value-of select="text()" disable-output-escaping="yes"/></td>
            <td><xsl:value-of select=".//property[@name='telefon']/value" /></td>
            <td><a href="mailto:{.//property[@name='email']/value}"><xsl:value-of select=".//property[@name='email']/value" /></a></td>
        </tr>
	</xsl:template>
</xsl:stylesheet>