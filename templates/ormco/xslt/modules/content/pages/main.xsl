<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM    "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:php="http://php.net/xsl">

	<xsl:template match="result[@module = 'content' and page/@is-default = 1]">
		<!-- Slider. -->
	    <div class="container-fluid top-container">
	        <div class="row">
	            <div class="col-md-8">
	                <xsl:apply-templates select="document('udata://slider/listElements/&slider_on_main_pid;?extProps=link_for_slide,right_img,content')/udata" mode="slider_block"/>
	            </div>
	            <div class="col-md-4">
	                <ul class="links row">
	                    <li class="col-xs-6 col-sm-6 col-md-12">
	                    	<xsl:apply-templates select="document('udata://banners/fastInsert/right_block_on_main1')/udata" mode="on_main_right_of_slider">
								<xsl:with-param name="position">1</xsl:with-param>
							</xsl:apply-templates>
	                    </li>
	                    <li class="col-xs-6 col-sm-6 col-md-12">
	                        <xsl:apply-templates select="document('udata://banners/fastInsert/right_block_on_main2')/udata" mode="on_main_right_of_slider">
								<xsl:with-param name="position">2</xsl:with-param>
							</xsl:apply-templates>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>

	    <!-- Discount -->
	    <div class="discount container-fluid">
	        <div class="row">
	            <div class="col-md-6 item">
	            	<xsl:apply-templates select="document('udata://banners/fastInsert/under_slider1')/udata" />
	            </div>
	            <div class="col-md-6 item">
	                <xsl:apply-templates select="document('udata://banners/fastInsert/under_slider2')/udata" />
	            </div>
	        </div>
	    </div>

		<!-- Категории на главной -->
		<xsl:apply-templates select=".//property[@name='category_on_main']" mode="category_on_main" />

	    <div class="container-fluid main-container">
	        <!-- Товары на главной -->
	        <xsl:apply-templates select=".//property[@name='objects_on_main']" mode="objects_on_main" />

	        <!-- News -->
	        <div class="row">
	            <div class="col-md-6 col-sm-12 col-xs-12 news">
	                <div class="white-pnl-border clearfix">
	                    <div class="header">Новости</div>
	                    <xsl:apply-templates select="document('udata://news/lastlist/&news_pid;//3/1/?extProps=publish_time,anons_pic,anons')/udata" mode="short_news_block2">
							<xsl:with-param name="title">Все новости</xsl:with-param>
						</xsl:apply-templates>
	                </div>
	            </div>
	            <div class="col-md-6 col-sm-12 col-xs-12 news">
                	<xsl:variable name="discounts_lists" select="document('udata://news/lastlist/&discounts_pid;//3/1/?extProps=publish_time,anons_pic,anons')/udata" />

                	<xsl:if test="$discounts_lists/items/item">
                    	<div class="white-pnl-border clearfix">
	                    	<div class="header">Акции</div>
	                    	<xsl:apply-templates select="$discounts_lists" mode="short_news_block2">
								<xsl:with-param name="title">Все акции</xsl:with-param>
							</xsl:apply-templates>
	                     </div>
                     </xsl:if>
	            </div>
	        </div>
	    </div>
	</xsl:template>


	<!-- category_on_main -->
	<xsl:template match="property" mode="category_on_main" />
	<xsl:template match="property[value]" mode="category_on_main">
		<div class="container-fluid categories">
	        <div class="row">
	            <ul>
	            	<xsl:apply-templates select="value/page" mode="category_on_main" />
	            </ul>
	            <div class="col-xs-12"><a href="&main_category_url;" class="lnk-go-to">Перейти в каталог<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="page" mode="category_on_main">
		<xsl:variable name="header_pic" select="document(concat('upage://',@id,'.menu_pic_a'))//value" />
		<xsl:variable name="header_pic_default" select="document(concat('upage://',@id,'.header_pic'))//value" />
		<li class="col-md-3 col-sm-4 col-xs-6 item">
			<a href="{@link}" data-gtm-event="ButtonClicks" data-gtm-event-value="{name}">
				<img src="&empty-photo;" alt="{name}" >
					<xsl:choose>
						<xsl:when test="$header_pic">
							<xsl:attribute name="src"><xsl:value-of select="$header_pic" /></xsl:attribute>
						</xsl:when>
						<xsl:when test="$header_pic_default">
							<xsl:attribute name="src"><xsl:value-of select="$header_pic_default" /></xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</img>
				<div class="title"><xsl:value-of select="name" /></div>
			</a>
		</li>
	</xsl:template>


	<!-- objects_on_main -->
	<xsl:template match="property" mode="objects_on_main" />
	<xsl:template match="property[value]" mode="objects_on_main">
		<div class="special-offers">
            <div class="header">Товары по акции</div>
            <ul class="row">
                <xsl:apply-templates select="value/page" mode="objects_on_main" />
            </ul>
        </div>
	</xsl:template>

	<xsl:template match="page" mode="objects_on_main">
		<xsl:variable name="item_info" select="document(concat('upage://',@id))/udata" />
		<xsl:variable name="photo" select="$item_info//property[@name='photo']/value" />
		<xsl:variable name="photo_for_main" select="$item_info//property[@name='menu_pic_a']/value" />

		<xsl:variable name="parentName" select="document(concat('upage://', @parentId))/udata/page/name" />
		<xsl:variable name="item_name">
			<xsl:choose>
				<xsl:when test=".//property[@name='h1_alternative']/value">
					<xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="text()" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="price" select="document(concat('udata://emarket/price/',@id,'//0/'))/udata" />

		<li class="col-md-3 col-sm-4 col-xs-6 item">
			<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommerceImpression/', @id, '/', $price/price/actual, '/(', php:function('urlencode', 'Товары по акции'), ')/', position(), '/1/'))/udata" />

			<xsl:choose>
				<xsl:when test="count(../page) = 3 and position() mod 3 = 0">
					<xsl:attribute name="class">col-md-6 col-sm-4 col-xs-6 item</xsl:attribute>
				</xsl:when>
				<xsl:when test="count(../page) = 2">
					<xsl:attribute name="class">col-md-6 col-sm-4 col-xs-6 item</xsl:attribute>
				</xsl:when>
				<xsl:when test="count(../page) = 1">
					<xsl:attribute name="class">col-md-12 col-sm-12 col-xs-12 item</xsl:attribute>
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
			<a href="{@link}" class="ga_productclick"
					data-id="{$item_info//property[@name='artikul']/value}"
					data-name="{$item_name}"
					data-category="{$ec_category}"
					data-brand="{$item_info//property[@name='breket_sistema']/value/item/@name}"
					data-position="{position()}"
					data-list="Товары по акции">
				<img src="&empty-photo;" >
					<xsl:choose>
						<xsl:when test="$photo_for_main">
							<xsl:attribute name="src"><xsl:value-of select="$photo_for_main" /></xsl:attribute>
						</xsl:when>
						<xsl:when test="$photo">
							<xsl:attribute name="src">
								<xsl:value-of select="document(concat('udata://system/makeThumbnailFull/(.', $photo, ')/190/190/void/0/1///100'))/udata/src" />
							</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</img>
				<div class="title"><xsl:value-of select="name" /></div>
				<div class="description"><xsl:value-of select="$parentName" /></div>
				<div class="price">
					<xsl:apply-templates select="$price/price" mode="price_on_main" />
				</div>
			</a>
		</li>
	</xsl:template>
</xsl:stylesheet>