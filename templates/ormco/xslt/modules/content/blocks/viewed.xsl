<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:php="http://php.net/xsl">

	
	<xsl:template match="udata" mode="viewed" />	
	<xsl:template match="udata[items/item]" mode="viewed">	
		<xsl:param name="block_title" select="'Вы уже смотрели'" />
		
		<div class="row">
            <div class="col-lg-12">
                <div class="white-pnl viewed-products">
                    <div class="header2"><xsl:value-of select="$block_title" /></div>
                    <div id="productSlider" class="multi-item-carousel carousel slide js-slider">
                    	<xsl:choose>
							<xsl:when test="count(items/item) &lt; 5">
								<xsl:attribute name="class">multi-item-carousel carousel slide</xsl:attribute>
								<div class="carousel-inner">
									<div class="item active">
			                            <xsl:apply-templates select="items/item" mode="viewed" >
			                            	<xsl:with-param name="block_title" select="$block_title" />
			                            </xsl:apply-templates>
			                        </div>
		                        </div>
							</xsl:when>
							<xsl:otherwise>
		                        <div class="carousel-inner">
		                            <xsl:apply-templates select="items/item" mode="viewed_pre" >
		                            	<xsl:with-param name="block_title" select="$block_title" />
		                            </xsl:apply-templates>
		                        </div>
	                    		<!-- Controls -->
		                        <a class="left carousel-control" href="#productSlider" role="button" data-slide="prev">
		                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		                        </a>
		                        <a class="right carousel-control" href="#productSlider" role="button" data-slide="next">
		                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		                        </a>
                    	
							</xsl:otherwise>
						</xsl:choose>
                    	
                    	
                        
                    </div>
                </div>
            </div>
        </div>
	</xsl:template>
	
	<xsl:template match="items/item" mode="viewed_pre">	
		<xsl:param name="block_title" select="'Вы уже смотрели'" />
		
		<div class="item">
			<xsl:if test="position() = 1">
				<xsl:attribute name="class">item active</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="." mode="viewed" >
            	<xsl:with-param name="block_title" select="$block_title" />
            </xsl:apply-templates>
		</div>
	</xsl:template>
	
	<xsl:template match="items/item" mode="viewed">
		<xsl:param name="block_title" select="'Вы уже смотрели'" />
		<xsl:variable name="item_name">
			<xsl:choose>
				<xsl:when test=".//property[@name='h1_alternative']/value">
					<xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="text()" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="ec_category" select="document(concat('udata://catalog/getNavibarCategory/',@id,'/0/'))/udata" />
		<xsl:variable name="price" select="document(concat('udata://emarket/price/',@id,'//0/'))/udata" />
		<xsl:variable name="artikul" select="document(concat('upage://',@id,'.artikul'))//value" />
			
        <div class="col-xs-12 col-sm-3" in="{@id}">
			<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommerceImpression/', @id, '/', $price/price/actual, '/(', php:function('urlencode', $block_title), ')/', position(), '/1/'))/udata" />

			<a href="{@link}"  class="ga_productclick"
                    	data-id="{$artikul}"
                    	data-name="{$item_name}"
                    	data-category="{$ec_category}"
                    	data-brand="{.//property[@name='breket_sistema']/value}"
                    	data-position="{position()}"
                    	data-list="{$block_title}">
                    	
            	<xsl:call-template name="makeThumbnailFull_ByPath">
					<xsl:with-param name="source" select=".//property[@name='photo']/value" />
					<xsl:with-param name="width" select="190" />
					<xsl:with-param name="height" select="190" />
					
					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="element-id" select="@id" />
					<xsl:with-param name="field-name" select="'photo'" />
					
					<!-- <xsl:with-param name="alt" select="text()" /> -->
					<xsl:with-param name="class" select="'img-responsive'" />
				</xsl:call-template>
                <div class="title">
                    <p>
                        <xsl:choose>
							<xsl:when test=".//property[@name='h1_alternative']/value">
								<xsl:value-of select=".//property[@name='h1_alternative']/value" disable-output-escaping="yes" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="text()" />
							</xsl:otherwise>
						</xsl:choose>
                    </p>
                </div>
                <div class="price">
					<xsl:if test="$price/price/actual &gt; 0">
						<xsl:if test="$price/discount">
							<div class="pf_amount">
								<span class="pf_amount_name">Скидка:</span>
								<xsl:text> </xsl:text>
								<span class="pf_amount_val">
									<xsl:value-of select="$price/discount/total/@percent" />%,
									<xsl:text> </xsl:text>
									<xsl:value-of select="concat(format-number(number($price/discount/total/node()), '#&#160;###,##','price'), ' ', $price/price/@suffix)" />
								</span>
								<xsl:text> </xsl:text>
								<xsl:apply-templates select="$price/discount" mode="default-assets-emarket-discount-info" />
							</div>
						</xsl:if>
						<div class="pf_amount large" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
							<span class="pf_amount_name">Цена:</span>
							<xsl:text> </xsl:text>
							<span class="pf_amount_val">
								<xsl:apply-templates select="$price" mode="object-price" />
							</span>
						</div>
					</xsl:if>
                </div>
                <xsl:choose>
					<xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
						<div class="availability yes"><i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
					</xsl:when>
					<xsl:otherwise>
						<div class="availability no"><i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
					</xsl:otherwise>
				</xsl:choose>
            </a>
            
            <div class="text-center">
            	<!-- прячем кнопку купить и поля для указания кол-ва, если товара нет в наличии -->
                <xsl:if test=".//property[@name='common_quantity']/value &gt; 0">
					<a id="add_basket_{@id}" href="{$lang-prefix}/emarket/basket/put/element/{@id}/" class="btn-primary btn-buy basket_list ga_productbuy" 
                    	data-id="{$artikul}"
                    	data-name="{$item_name}"
                    	data-category="{$ec_category}"
                    	data-brand="{.//property[@name='breket_sistema']/value}"
                    	data-price="{$price/price/actual}"
                    	data-qty="1">
						<xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceProductData/', @id, '/', $price/price/actual, '/'))/udata" mode="ecommerce-add" />
	                    Купить
	                </a>
                </xsl:if>
            	
            </div>
        </div>
        
	</xsl:template>
	
	<!-- <xsl:template match="items/item" mode="viewed">	

		<xsl:variable name="cur_pos" select="position()" />
		
		
		<div class="item">
			<xsl:if test="position()=1">
				<xsl:attribute name="class">item active</xsl:attribute>
			</xsl:if>
            <xsl:apply-templates select="." mode="viewed_item" />
            <xsl:apply-templates select="../item[position() = (($cur_pos - 1) * 4) + 2]" mode="viewed_item" />
            <xsl:apply-templates select="../item[position() = (($cur_pos - 1) * 4) + 3]" mode="viewed_item" />
            <xsl:apply-templates select="../item[position() = (($cur_pos - 1) * 4) + 4]" mode="viewed_item" />
        </div>
	</xsl:template>
	
	<xsl:template match="items/item" mode="viewed_item">	
        <div class="col-xs-12 col-sm-3" in="{@id}">
            <a href="{@link}">
            	<xsl:call-template name="makeThumbnailFull_ByPath">
					<xsl:with-param name="source" select=".//property[@name='photo']/value" />
					<xsl:with-param name="width" select="190" />
					<xsl:with-param name="height" select="190" />
					
					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="element-id" select="@id" />
					<xsl:with-param name="field-name" select="'photo'" />
					
					<xsl:with-param name="alt" select="text()" />
					<xsl:with-param name="class" select="'img-responsive'" />
				</xsl:call-template>
                <div class="title">
                    <p>
                        <xsl:value-of select="text()" />
                    </p>
                </div>
                <div class="price">
                    <xsl:apply-templates select="document(concat('udata://emarket/price/',@id,'//0/'))/udata/price" mode="discounted-price" />
                        
                </div>
                <xsl:choose>
					<xsl:when test=".//property[@name='common_quantity']/value &gt; 0">
						<div class="availability yes"><i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
					</xsl:when>
					<xsl:otherwise>
						<div class="availability no"><i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
					</xsl:otherwise>
				</xsl:choose>
            </a>
            <div class="text-center">
            	<a id="add_basket_{@id}" href="{$lang-prefix}/emarket/basket/put/element/{@id}/" class="btn-primary btn-buy basket_list ">Купить</a>
                
            </div>
        </div>
	</xsl:template> -->

</xsl:stylesheet>