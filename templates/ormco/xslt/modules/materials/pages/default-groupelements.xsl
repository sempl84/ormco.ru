<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'materials']">
		<xsl:variable name="childCategory" select="document(concat('udata://materials/getCategoryList/',$document-page-id))/udata" />
		<xsl:variable name="categoryPid">
			<xsl:choose>
				<xsl:when test="$document-page-id = &marketing_materials_pid;"></xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$parents/page[position() = last()]/@id" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div class="category-page" itemscope="itemscope" itemtype="http://schema.org/Product">
    		<xsl:choose>
				<xsl:when test="$childCategory//item">
    				<xsl:attribute name="class">category-page</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">brackets-page</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>

			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

			<div class="page-header">
	            <div class="container-fluid">
		            <h1><i class="fa fa-list-alt" aria-hidden="true"></i><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
		            <xsl:if test="$childCategory//item">
	    				<div class="info-lnk js-privacy-lnk" data-placement="top" title="Прочитайте подробнее политику конфиденциальности"><button class="btn btn-blue privacy-policy-modal-btn" data-toggle="modal" data-target="#privacyPolicyModal" data-gtm-event="ButtonClicks" data-gtm-event-value="Политика конфиденциальности">Политика конфиденциальности <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button></div>
					</xsl:if>

		        </div>
	        </div>

			<!--Content -->
		    <xsl:choose>
	        	<!-- вывод категории с подразделами -->
				<xsl:when test="$childCategory//item">
					<div class="container-fluid category">
				        <div class="row">
				        	<aside class="col-lg-3 col-md-3 hidden-sm hidden-xs left-pnl">
					            <xsl:apply-templates select="document(concat('udata://materials/getCategoryList//',$categoryPid,'/1000/1/'))/udata" mode="categoryListLeft" />
					            <xsl:apply-templates select="." mode="left_col" />
					        </aside>

			                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
			                    <xsl:apply-templates select="document(concat('udata://materials/getCategoryList/',$document-page-id,'?extProps=header_pic'))/udata" />

			                    <xsl:apply-templates select=".//property[@name='content']" mode="categorDescr" />
			                    <div class="row">
			                        <div class="col-lg-12">
			                            <xsl:call-template name="social_share" />
			                        </div>
			                    </div>
			                </div>
			            </div>
		            </div>
				</xsl:when>
				<xsl:otherwise>
					<div class="container-fluid download-materials-page">
			            <div class="row">
			                <aside class="col-lg-3 col-md-3 hidden-sm hidden-xs left-pnl">
			                    <xsl:apply-templates select="document(concat('udata://materials/getCategoryList//',$parents/page[position() = last()]/@id,'/1000/1/'))/udata" mode="categoryListLeft" />

			                	<xsl:apply-templates select="." mode="left_col" />
			                </aside>
			                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
			                    <div class="row">
			                        <div class="col-lg-12">
			                             <xsl:apply-templates select="document(concat('udata://materials/listElements//',$document-page-id,'?extProps=video,document_item,content,photo'))/udata"  />

			                        </div>
			                    </div>
			                    <xsl:if test=".//property[@name='content']/value">
				                    <div class="row">
				                        <div class="col-lg-12">
				                            <article class="white-pnl content_wrap">
				                                <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" />
				                            </article>
				                        </div>
				                    </div>
			                    </xsl:if>

			                </div>


			            </div>
			        </div>
				</xsl:otherwise>
			</xsl:choose>

	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='materials' and @method='listElements']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='materials' and @method='listElements' and items/item]">
		<div class="materials-list js-materials-list">
            <form action="/" method="post">
                <div class="header2">Материалы для скачивания</div>
                <ul>
		            <xsl:apply-templates select="items/item" />
		        </ul>

		        <div class="btns">
			        <button class="btn btn-default js-download-checked" type="button">Скачать выбранное</button>
			        <button class="btn btn-default js-download-all" type="button">Скачать все файлы</button>
			        <button class="btn btn-default js-reset-selected" type="button">Снять выделение</button>
			    </div>
			    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
			    <div id="blueimp-gallery" class="blueimp-gallery">
			        <div class="slides"></div>
			        <a class="close">×</a>
			    </div>
            </form>
        </div>
        <xsl:apply-templates select="total" />


	</xsl:template>

	<xsl:template match="udata[@module='materials' and @method='listElements']/items/item" >
		<xsl:variable name="href">
			<xsl:choose>
				<xsl:when test=".//property[@name='video']/value"><xsl:value-of select=".//property[@name='video']/value" /></xsl:when>
				<xsl:when test=".//property[@name='document_item']/value"><xsl:value-of select=".//property[@name='document_item']/value" /></xsl:when>
				<xsl:when test=".//property[@name='photo']/value"><xsl:value-of select=".//property[@name='photo']/value" /></xsl:when>
				<xsl:otherwise>#</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="filename">
			<xsl:choose>
				<xsl:when test=".//property[@name='video']/value"><xsl:value-of select="concat(.//property[@name='video']/value/@name,'.',.//property[@name='video']/value/@ext)" /></xsl:when>
				<xsl:when test=".//property[@name='document_item']/value"><xsl:value-of select="concat(.//property[@name='document_item']/value/@name,'.',.//property[@name='document_item']/value/@ext)" /></xsl:when>
				<xsl:when test=".//property[@name='photo']/value"><xsl:value-of select="concat(.//property[@name='photo']/value/@name,'.',.//property[@name='photo']/value/@ext)" /></xsl:when>
				<xsl:otherwise>noname</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="ext">
			<xsl:choose>
				<xsl:when test=".//property[@name='video']/value">video</xsl:when>
				<xsl:when test=".//property[@name='document_item']/value/@ext = 'pdf'">pdf</xsl:when>
				<xsl:when test=".//property[@name='photo']/value and contains($document-page-name,'оготип')">logo</xsl:when>
				<xsl:when test=".//property[@name='photo']/value">photo</xsl:when>
				<xsl:when test=".//property[@name='document_item']/value/@ext"><xsl:value-of select=".//property[@name='document_item']/value/@ext" /></xsl:when>
				<xsl:otherwise>noext</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<li >
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <a href="{$href}" class="js-preview" ext="{$ext}">

						<xsl:choose>
							<xsl:when test=".//property[@name='photo']/value">
								<img class="img-responsive" src="{.//property[@name='photo']/value}"  alt="{text()}"  itemprop="image" style="max-width: 100%; max-height: auto;" />
								<!-- <xsl:call-template name="makeThumbnail_ByPath">
									<xsl:with-param name="source" select=".//property[@name='photo']/value" />
									<xsl:with-param name="width" select="190" />
									<xsl:with-param name="height" select="auto" />

									<xsl:with-param name="empty">&empty-photo-speaker;</xsl:with-param>
									<xsl:with-param name="element-id" select="$document-page-id" />
									<xsl:with-param name="field-name" select="'photo'" />

									<xsl:with-param name="alt" select="text()" />
									<xsl:with-param name="class" select="'img-responsive'" />
								</xsl:call-template> -->
							</xsl:when>
							<xsl:when test=".//property[@name='video']/value">
								<div class="video-preview"></div>
							</xsl:when>
							<xsl:when test=".//property[@name='document_item']/value">
								<div class="pdf-preview"></div>
							</xsl:when>
							<xsl:otherwise><div class="pdf-preview"></div></xsl:otherwise>
						</xsl:choose>
                    </a>
                </div>
                <div class="col-sm-8 col-xs-7">
                    <a href="{$href}" class="lnk-to-product js-preview" ext="{$ext}">
                    	<xsl:value-of select="text()" disable-output-escaping="yes" />
                    	<xsl:if test=".//property[@name='document_item']/value/@ext = 'eps'">&#160;(EPS)</xsl:if>
                    	<xsl:if test="not(.//property[@name='document_item']/value/@ext = 'eps') and .//property[@name='photo']/value/@ext">&#160;<span>(<xsl:value-of select=".//property[@name='photo']/value/@ext" />)</span></xsl:if>
                    </a>
                    <div class="description"><xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" /></div>
                </div>
                <div class="col-sm-1 col-xs-2 text-center">
                    <input type="checkbox" />
                    <a href="{$href}" ext="{$ext}" download="{$filename}" pid = "{@id}" class="download-btn" title="Скачать"></a>
                </div>
            </div>
        </li>


	</xsl:template>

</xsl:stylesheet>