<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="udata[@module = 'emarket' and @method = 'cart']" mode="basket">
		<a href="{$lang-prefix}/emarket/cart/" class="basket" data-gtm-event="ButtonClicks">
			<xsl:if test="$method='cart' or $method='purchasing_one_step' or $method='purchase'">
				<xsl:attribute name="class">basket active</xsl:attribute>
			</xsl:if>
			<span class="basket_info_summary">
				<xsl:apply-templates select="summary" mode="basket" />
			</span> Корзина
		</a>
		<!-- <xsl:text> </xsl:text> -->
	</xsl:template>
	
	<xsl:template match="udata[@module = 'emarket' and @method = 'cart']" mode="basket_small">
		<a href="{$lang-prefix}/emarket/cart/" class="basket">
			<xsl:if test="$method='cart' or $method='purchasing_one_step' or $method='purchase'">
				<xsl:attribute name="class">basket active</xsl:attribute>
			</xsl:if>
			
			<span class="basket_info_summary">
				<xsl:apply-templates select="summary" mode="basket" />
			</span>
		</a>
	</xsl:template>
	
	<xsl:template match="summary" mode="basket">
		0
	</xsl:template>
	
	<xsl:template match="summary[amount &gt; 0]" mode="basket">
		<xsl:apply-templates select="amount" />
	</xsl:template>
</xsl:stylesheet>