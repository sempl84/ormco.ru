<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="udata" mode="payment_online">
        <article class="white-pnl content_wrap">
            <form method="GET" class="form-horizontal" action="">
                <div class="row">
                    <div class="form-group">
                        <xsl:if test="error">
                            <div class="col-xs-12">
                                <p class="bg-danger"><xsl:value-of select="error" /></p>
                            </div>
                        </xsl:if>
                        <div class="col-xs-12 col-md-4 control-label">Введите номер заказа</div>
                        <div class="col-xs-12 col-md-8">
                            <input type="text" paceholder="Номер заказа" name="order_number" value="{@order_number}" id="order_number" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-12 col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Оплатить</button>
                    </div>
                    </div>
                </div>
            </form>
        </article>
    </xsl:template>

    <xsl:template match="udata[@order_id]" mode="payment_online">
        <article class="white-pnl content_wrap">
            <p>Вы выбрали для оплаты заказ номер <xsl:value-of select="@order_id" /></p>
            <!--Вызываем стандартный механизм оплаты, эмулировав предварительно все данные для него-->
            <xsl:apply-templates select="purchasing" />
        </article>
    </xsl:template>
</xsl:stylesheet>