<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="udata[@method = 'price']">
		<xsl:apply-templates select="price" />
	</xsl:template>

	<xsl:template match="price">
		<span class="btn btn-blue"><xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" /></span>
	</xsl:template>

	<xsl:template match="price[original]">
		<span class="btn old-price"><xsl:value-of select="concat(@prefix, ' ', format-number(original, '#&#160;###,##','price'), ' ', @suffix)" /></span>
		<span class="btn btn-blue"><xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" /></span>
	</xsl:template>

	<xsl:template match="price[not(actual &gt; 0)]">
		<span class="btn btn-orange">Бесплатно</span>
	</xsl:template>


	<xsl:template match="udata[@method = 'price']" mode="short_event_price">
		<xsl:apply-templates select="price" mode="short_event_price"/>
	</xsl:template>

	<xsl:template match="price" mode="short_event_price">
		<span class="price">
			<xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" />
		</span>
	</xsl:template>

	<xsl:template match="price[original]" mode="short_event_price">
		<span class="price">
			<span class="old-price"><xsl:value-of select="concat(@prefix, ' ', format-number(original, '#&#160;###,##','price'), ' ', @suffix)" /></span>
			<span class="new-price"><xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" /></span>
		</span>
	</xsl:template>

	<xsl:template match="price[not(actual &gt; 0)]" mode="short_event_price">
		<span class="price">Бесплатно</span>
	</xsl:template>

    <!-- <xsl:template match="udata[@method = 'price']" mode="old_price">
        <xsl:param name="price" />
        <xsl:value-of select="concat(price/@prefix, ' ', $price, ' ', price/@suffix)" />
    </xsl:template>

	<xsl:template match="total-price">
		<xsl:value-of select="concat(@prefix, ' ', actual, ' ', @suffix)" />
	</xsl:template>

	<xsl:template match="price" mode="discounted-price">
		<xsl:value-of select="concat(@prefix, ' ', actual, ' ', @suffix)" />
	</xsl:template> -->

	<xsl:template match="price|total-price" mode="discounted-price">
		<xsl:choose>
			<xsl:when test="actual &gt; 0">
				<xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" />
				<meta itemprop="price" content="{actual}" />
				<meta itemprop="priceCurrency" content="RUB" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'Цена: - '" />
				<meta itemprop="price" content="-" />
				<meta itemprop="priceCurrency" content="RUB" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
	<xsl:template match="price[original]|total-price[original]" mode="discounted-price">
            <xsl:value-of select="concat(@prefix, ' ', format-number(original, '#&#160;###,##','price'), ' ', @suffix)" />
            <meta itemprop="price" content="{actual}" />
            <meta itemprop="priceCurrency" content="RUB" />
            <span class="old_price">Ваша цена: <span><xsl:value-of select="concat(format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" /></span></span>
	</xsl:template>

	<xsl:template match="price" mode="price_on_main">
		<span class="red">
			<xsl:value-of select="format-number(actual, '#&#160;###,##','price')" /> <span class="rub">q</span>
			<meta itemprop="price" content="{actual}" />
			<meta itemprop="priceCurrency" content="RUB" />
		</span>
	</xsl:template>
	<xsl:template match="price[original]" mode="price_on_main">
		<span class="red">
			<xsl:value-of select="format-number(actual, '#&#160;###,##','price')" /> <span class="rub">q</span>
			<meta itemprop="price" content="{actual}" />
			<meta itemprop="priceCurrency" content="RUB" />
		</span>
		<span class="line-through">
			<xsl:value-of select="format-number(original, '#&#160;###,##','price')" /> <span class="rub">q</span>
		</span>
	</xsl:template>

</xsl:stylesheet>
