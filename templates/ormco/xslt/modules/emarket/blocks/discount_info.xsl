<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xsl">

    <xsl:template match="discount" mode="default-assets-emarket-discount-info">
        <div class="pf_info_icon">
            <div class="pf_tooltip pf_tooltip_right">
                <h4>Детализация скидок</h4>
                <ul class="pf_saletext">
                    <xsl:if test="user">
                        <li>
                            <div class="pf_saletext_name">Персональная скидка (<xsl:value-of select="user/@percent" />%)</div>
                            <div class="pf_saletext_val">- <xsl:value-of select="user/node()" /> руб.</div>
                        </li>
                    </xsl:if>
                    <xsl:if test="coupon">
                        <xsl:variable name="class">
                            <xsl:choose>
                                <xsl:when test="coupon/@focus">blue</xsl:when>
                                <xsl:otherwise>red</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <li>
                            <div class="pf_saletext_name">Скидка по промокоду (<xsl:value-of select="coupon/@percent" />%)</div>
                            <div class="pf_saletext_val {$class}">&#160;- <xsl:value-of select="coupon/total/node()" />&#160;руб.</div>
                        </li>
                    </xsl:if>
                </ul>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>