<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="result[@method = 'cart']">
        <div class="order-page">
            <!--Breadcrumbs -->
            <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true">
                        <xsl:if test=".//property[@name='icon_style']/value">
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                            </xsl:attribute>
                        </xsl:if>
                    </i>
                    <h1>
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-pnl basket">
                            <ul class="steps">
                                <li class="active">
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</li>
                                <li>
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</li>
                                <li>
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>Заказ оформлен</li>
                            </ul>
                            <div data-cart="cart-wrapper">
                                <xsl:apply-templates select="$cart" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'cart']">
        <div class="basket_empty">
            <div class="table">
                <h4 class="empty-content">&basket-empty;</h4>
                <p>&return-to-catalog;</p>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="udata[@method = 'cart'][count(items/item) &gt; 0]">
        <div class=" table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Название товара</th>
                        <th>Артикул</th>
                        <th>Количество</th>
                        <xsl:choose>
                            <xsl:when test="items/item/price/original">
                                <th>Цена</th>
                                <th>Ваша цена</th>
                            </xsl:when>
                            <xsl:otherwise>
                                <th>Цена</th>
                            </xsl:otherwise>
                        </xsl:choose>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:apply-templates select="document('udata://ormcoCoupons/getOrmcoCouponsMultipleCartContent/')/udata" mode="cart-coupons-multiple-content" />

                    <xsl:apply-templates select="items/item" />
                </tbody>
            </table>
        </div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-9"></div>
			<div class="col-md-3">
                <div class="pf_amount large">
                    <span class="pf_amount_name">Сумма:&#160;</span>
                    <span class="pf_amount_val">
                        <xsl:if test="summary/price/original">
                            <del><xsl:value-of select="format-number(summary/price/original, '#&#160;###,##','price')" /></del>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <ins><xsl:value-of select="concat(summary/price/@prefix, ' ', format-number(summary/price/actual, '#&#160;###,##','price'), ' ', summary/price/@suffix)" /></ins>
                    </span>
                </div>
                <xsl:if test="summary/price/discount">
                    <div class="pf_amount">
                        <span class="pf_amount_name">Скидка:&#160;</span> <span class="pf_amount_val"><xsl:value-of select="summary/price/discount/@percent" />%, <xsl:value-of select="concat(summary/price/@prefix, ' ', format-number(summary/price/discount, '#&#160;###,##','price'), ' ', summary/price/@suffix)" /></span>
                    </div>
                </xsl:if>
			</div>
		</div>

        <div class="row">
            <div class="col-sm-6 text-left">
                <a href="#" class="btn basket_remove_all" data-gtm-event="ButtonClicks">Очистить корзину</a>
            </div>
            <div class="col-sm-6 text-right">
                <form method="post" action="/emarket/purchasing_one_step/" class="no_ajax_form_waiting_show">
                    <button type="submit" class="btn btn-primary send-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Оформить заказ">Оформить заказ</button>
                </form>
            </div>
        </div>

        <xsl:apply-templates select="document('udata://exchange/addExchangeEcommerceCheckoutCode/1/')/udata" />
    </xsl:template>

    <xsl:template match="udata" mode="cart-coupons-multiple-content" />
    <xsl:template match="udata[content]" mode="cart-coupons-multiple-content">
        <tr>
            <td colspan="7" class="double_coupons_cart_text"><xsl:value-of select="content" disable-output-escaping="yes" /></td>
        </tr>
    </xsl:template>

    <xsl:template match="udata[@method = 'cart']//item">
        <xsl:variable name="page_info" select="document(concat('upage://',page/@id))/udata" />
        <xsl:variable name="productData" select="document(concat('udata://exchange/getExchangeEcommerceProductData/', page/@id, '/', price/actual, '/'))/udata" />

        <tr class="cart_item_{@id}" data-gtm-product="{$productData/params}">
            <td>
                <a href="{$lang-prefix}{page/@link}">
                    <!-- <img src="img/products/basket-img-1.jpg" alt="product 1" /> -->
                    <xsl:call-template name="catalog-thumbnail">
                        <xsl:with-param name="element-id" select="page/@id" />
                        <xsl:with-param name="field-name">photo</xsl:with-param>
                        <xsl:with-param name="empty">&empty-photo;</xsl:with-param>
                        <xsl:with-param name="width">137</xsl:with-param>
                        <xsl:with-param name="height">79</xsl:with-param>
                        <xsl:with-param name="align">middle</xsl:with-param>
                    </xsl:call-template>
                </a>
                <xsl:choose>
                    <xsl:when test="$page_info//property[@name='common_quantity']/value &gt; 0">
                        <div class="availability yes"><i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="availability no"><i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <a href="{$lang-prefix}{page/@link}">
                    <xsl:value-of select="@name" />
                </a>
            </td>
            <td>Артикул: <xsl:value-of select="$page_info//property[@name='artikul']/value" /></td>
            <td>
                <div class="input-group spinner">
                    <input type="text" value="{amount}" class="form-control amount" />
                    <input type="hidden" value="{amount}" class="amount_old"/>

                    <div class="input-group-btn-vertical change-amount">
                        <button class="btn btn-default top" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                        <button class="btn btn-default bottom" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                    </div>
                </div>
            </td>
            <xsl:choose>
                <xsl:when test="total-price/original">
                    <td class="cart_item_price_origin{@id}">
                        <div class="pf_price">
                            <del><xsl:value-of select="concat(total-price/@prefix, ' ', format-number(total-price/original, '#&#160;###,##','price'), ' ', total-price/@suffix)" /></del>
                        </div>
                    </td>
                    <td class="cart_item_price_{@id}">
                        <div class="pf_price">
                            <span><xsl:value-of select="concat(total-price/@prefix, ' ', format-number(total-price/actual, '#&#160;###,##','price'), ' ', total-price/@suffix)" /></span>
                            &nbsp;
                            <xsl:apply-templates select="discount" mode="default-assets-emarket-discount-info" />
                        </div>
                    </td>
                </xsl:when>
                <xsl:when test="../item/price/original">
                    <td class="cart_item_price_{@id}">
                        <xsl:apply-templates select="total-price" mode="discounted-price" />
                    </td>
                    <td class="cart_item_price_{@id}">
                        <xsl:apply-templates select="total-price" mode="discounted-price" />
                    </td>
                </xsl:when>
                <xsl:otherwise>
                    <td class="cart_item_price_{@id}">
                        <xsl:apply-templates select="total-price" mode="discounted-price" />
                    </td>
                </xsl:otherwise>
            </xsl:choose>

            <td>
                <a href="{$lang-prefix}/emarket/basket/remove/item/{@id}/" id="del_basket_{@id}" class="del delete-btn"
                   data-id="{$page_info//property[@name='artikul']/value}"
                   data-name="{$page_info/page/name}"
                   data-category="{document(concat('udata://catalog/getNavibarCategory/',$page_info/page/@parentId))/udata}"
                   data-brand="{$page_info//property[@name='breket_sistema']/value}"
                   data-price="{total-price/actual}"
                   data-qty="{amount}" data-gtm-event="ButtonClicks" data-gtm-event-value="Удалить">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </xsl:template>


    <xsl:template match="udata[@method = 'cart']/summary">
        <xsl:if test="price/bonus!=''">
            <div class="info">
                <xsl:text>&order-bonus;: </xsl:text>
                <span class="cart_discount">
                    <xsl:value-of select="$currency-prefix" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="price/bonus" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$currency-suffix" />
                </span>
            </div>
        </xsl:if>
        <div class="info">
            <xsl:text>&order-discount;: </xsl:text>
            <span class="cart_discount">
                <xsl:value-of select="$currency-prefix" />
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="price/discount!=''">
                        <xsl:value-of select="price/discount" />
                    </xsl:when>
                    <xsl:otherwise>
                        0
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$currency-suffix" />
            </span>
        </div>
        <xsl:apply-templates select="price/delivery[.!='']" mode="cart" />
        <div class="size2 tfoot">
            <xsl:text>&summary-price;: </xsl:text>
            <xsl:value-of select="$currency-prefix" />
            <xsl:text> </xsl:text>
            <span class="cart_summary size3">
                <xsl:apply-templates select="price/actual" />
            </span>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$currency-suffix" />
        </div>
    </xsl:template>

    <xsl:template match="delivery[.!='']" mode="cart">
        <div class="info">
            <xsl:text>&delivery;: </xsl:text>
            <xsl:value-of select="$currency-prefix" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="$currency-suffix" />
        </div>
    </xsl:template>
</xsl:stylesheet>