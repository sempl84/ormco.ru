<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl">

    <xsl:template match="/result[@method = 'ordersList']">
        <div class="my-orders-page">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                                <li class="breadcrumb-item"><a href="/users/settings/">Личный кабинет</a></li>
                                <li class="breadcrumb-item active"><xsl:value-of select="@header" disable-output-escaping="yes" /></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <aside class="col-lg-3 col-md-3 col-xs-12 right-pnl">
                        <xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata" mode="left_menu" />
                    </aside>
                    <div class="col-lg-9 col-md-9 col-xs-12 main-col main-col-mb">
                        <xsl:apply-templates select="document('udata://emarket/ordersList//desc/')/udata" />
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="udata[@method = 'ordersList']">
        <div class="my-orders-table">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Содержание</th>
                            <th>Статус</th>
                            <th>Сумма</th>
                            <th>Повторить</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:apply-templates select="items/item" mode="order" />
                    </tbody>
                </table>
            </div>
        </div>
        <xsl:apply-templates select="total" />
    </xsl:template>

    <xsl:template match="item" mode="order">
        <xsl:apply-templates select="document(concat('udata://emarket/order/', @id))/udata" mode="personal_order_details" />
    </xsl:template>

    <xsl:template match="udata[@module = 'emarket'][@method = 'order']" mode="personal_order_details" />
    <xsl:template match="udata[@module = 'emarket' and @method = 'order' and number]" mode="personal_order_details">
        <xsl:variable name="order" select="document(concat('uobject://', @id))/udata" />
        <xsl:variable name="status" select="document(concat('uobject://', $order//property[@name='status_id']/value/item/@id))/udata" />

        <tr>
            <td><xsl:value-of select="number" /></td>
            <td><xsl:apply-templates select="$order//property[@name='order_date']" /></td>
            <td>
                <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#showOrder{@id}" aria-expanded="true" aria-controls="showOrder1">
                    <span data-gtm-event="ButtonClicks" data-gtm-event-value="Показать детали">Показать детали <i class="fa fa-angle-down" aria-hidden="true"></i></span><span>Скрыть детали <i class="fa fa-angle-up" aria-hidden="true"></i></span>
                </button>
            </td>
            <td>
                <xsl:choose>
                    <!-- Статус заказа из 1С "К оплате" и статус оплаты из 1С "Оплачен" -->
                    <xsl:when test="$order//property[@name='status_1c']/value/item/@id = '1566620' and $order//property[@name='status_payment_1c']/value/item/@id = '1595921'">
                        Оплачен
                    </xsl:when>
                    <!-- Статус заказа из 1С "К оплате" и статус оплаты "Принята" -->
                    <xsl:when test="$order//property[@name='status_1c']/value/item/@id = '1566620' and $order//property[@name='payment_status_id']/value/item/@id = '192'">
                        Оплачен
                    </xsl:when>
                    <!-- Статус заказа из 1С пустой - еще не приходил, а статус оплаты "Принята" -->
                    <xsl:when test="not($order//property[@name='status_1c']/value/item/@id) and $order//property[@name='payment_status_id']/value/item/@id = '192'">
                        Оплачен
                    </xsl:when>
                    <xsl:when test="$order//property[@name = 'status_1c']/value">
                        <xsl:value-of select="$order//property[@name='status_1c']/value/item/@name" />
                    </xsl:when>
                    <xsl:when test="not($order//property[@name='moderate']/value = 1) and $order//property[@name='choose_payment']/value = '1' and $status//property[@name='codename']/value = 'waiting'">
                        Оплачен
                    </xsl:when>
                    <xsl:when test="$order//property[@name='moderate']/value = 1 and not($order//property[@name='choose_payment']/value = 1)">
                        Ожидает проверки
                    </xsl:when>
                    <xsl:when test="not($order//property[@name='moderate']/value = 1) and not($order//property[@name='choose_payment']/value = 1)">
                        Оплачивается
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="$order//property[@name='status_id']" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td><xsl:apply-templates select="summary/price" mode="discounted-price"/></td>
            <td tt="{$order//property[@name='payment_id']/value/item/@id}">
                <a href="#" id="repeat_{@id}" data-toggle="tooltip" data-placement="top" title="У вас будет возможность отредактировать заказ" data-gtm-event="ButtonClicks">
                    <xsl:attribute name="onclick">site.basket.addSeveral('<xsl:apply-templates select="items/item" mode="old_items"/>','#repeat_<xsl:value-of select="@id" />'); return false; </xsl:attribute>
                    Повторить заказ
                </a>

                <xsl:if test="not($order//property[@name='moderate']/value = 1) and $order//property[@name='choose_payment']/value = '1' ">
                    <!-- квитанция в банк -->
                    <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 14044">
                        <br/><a href="/tcpdf/docs/receipt.php?oi={@id}" target="_blank">Открыть квитанцию в банк</a>
                    </xsl:if>
                    <!-- квитанция в банк -->
                    <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 13911">
                        <br/><a href="/tcpdf/docs/invoicee.php?oi={@id}" target="_blank">Открыть счет для юр.лиц</a>
                    </xsl:if>
                </xsl:if>
            </td>
        </tr>
        <tr class="small-table">
            <td colspan="6">
                <div class="collapse" id="showOrder{@id}">
                    <!--<xsl:if test="$user-type='sv'">-->
                        <!-- Статус заказа "К оплате" и статус оплаты НЕ "Оплачен" -->
                        <xsl:if test="$order//property[@name='status_1c']/value/item/@id = '1566620' and not($order//property[@name='status_payment_1c']/value/item/@id = '1595921')">
                            <a href="/delivery/oplatit-zakaz-po-nomeru/?order_id={@id}" class="btn btn-primary personal_payment_online_btn">Оплатить онлайн</a>
                        </xsl:if>
                        <ul class="order_params">
                            <li>
                                <span>Статус оплаты заказа</span>
                                <xsl:text>: </xsl:text>
                                <xsl:choose>
                                    <xsl:when test="$order//property[@name = 'status_payment_1c']/value/item">
                                        <xsl:value-of select="$order//property[@name = 'status_payment_1c']/value/item/@name" />
                                    </xsl:when>
                                    <xsl:when test="$order//property[@name = 'payment_status_id']/value/item/@id = 192">
                                        оплачено
                                    </xsl:when>
                                    <xsl:otherwise>
                                        не оплачен
                                    </xsl:otherwise>
                                </xsl:choose>
                            </li>
                            <xsl:if test="$order//property[@name='payment_id']/value/item/@name">
                                <li>
                                    <span>Способ оплаты</span>:
                                    <xsl:choose>
                                        <xsl:when test="not($order//property[@name='moderate']/value = 1) and $order//property[@name='choose_payment']/value = '1' and $order//property[@name='payment_id']/value/item/@id = 14044">
                                            <a href="/tcpdf/docs/receipt.php?oi={@id}" target="_blank">
                                                <xsl:value-of select="$order//property[@name='payment_id']/value/item/@name" />
                                            </a>
                                        </xsl:when>
                                        <xsl:when test="not($order//property[@name='moderate']/value = 1) and $order//property[@name='choose_payment']/value = '1' and $order//property[@name='payment_id']/value/item/@id = 13911">
                                            <a href="/tcpdf/docs/invoicee.php?oi={@id}" target="_blank">
                                                <xsl:value-of select="$order//property[@name='payment_id']/value/item/@name" />
                                            </a>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="$order//property[@name='payment_id']/value/item/@name" />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </li>
                            </xsl:if>
                            <xsl:if test="$order//property[@name='order_ormco_coupon_code']/value">
                                <li>
                                    <span>Промокод</span>: <xsl:value-of select="$order//property[@name='order_ormco_coupon_code']/value" disable-output-escaping="yes" />
                                </li>
                            </xsl:if>
                            <xsl:if test="$order//property[@name='delivery_id']/value/item/@name">
                                <li>
                                    <span>Способ доставки</span>: <xsl:value-of select="$order//property[@name='delivery_id']/value/item/@name" />
                                </li>
                            </xsl:if>
                            <xsl:if test="$order//property[@name='delivery_address']/value/item/@id">
                                <li>
                                    <span >Адрес доставки</span>: <xsl:apply-templates select="document(concat('uobject://', $order//property[@name='delivery_address']/value/item/@id))//property" mode="delivery-address" />
                                </li>
                            </xsl:if>
                            <xsl:if test="string($order//property[@name='prefer_date_time_delivery_start']/value/@unix-timestamp) and string($order//property[@name='prefer_date_time_delivery_finish']/value/@unix-timestamp)">
                                <li>
                                    <span >Время и дата доставки</span>:
                                    <xsl:value-of select="php:function('date', 'd.m.Y', string($order//property[@name='prefer_date_time_delivery_start']/value/@unix-timestamp))" />
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="php:function('date', 'H:i', string($order//property[@name='prefer_date_time_delivery_start']/value/@unix-timestamp))" />-<xsl:value-of select="php:function('date', 'H:i', string($order//property[@name='prefer_date_time_delivery_finish']/value/@unix-timestamp))" />
                                </li>
                            </xsl:if>
                        </ul>
                    <!--</xsl:if>-->
                    <table class="table small">
                        <thead>
                            <tr>
                                <th>Названия товара</th>
                                <th>Артикул товара</th>
                                <th>Количество</th>
                                <th>Стоимость</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="items/item" />
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Всего</th>
                                <th></th>
                                <th><xsl:value-of select="sum(items/item/amount)" /></th>
                                <th>
                                    <xsl:variable name="total_old_price" select="sum(items/item/total-price/original)" />
                                    <xsl:if test="$total_old_price &gt; 0">
                                        <xsl:value-of select="concat(items/item/total-price/@prefix, ' ', format-number(sum(items/item/total-price/original), '#&#160;###,##','price'), ' ', items/item/total-price/@suffix)" />
                                    </xsl:if>

                                    <span class="old_price">
                                        <xsl:text>Ваша цена: </xsl:text>
                                        <span>
                                            <xsl:value-of select="concat(format-number(sum(items/item/total-price/actual), '#&#160;###,##','price'), ' ', items/item/total-price/@suffix)" />
                                            <!--<xsl:value-of select="concat(format-number(actual, '#&#160;###,##','price'), ' ', @suffix)" />-->
                                        </span>
                                    </span>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="udata[@method = 'order']/items/item">
        <tr>
            <td>
                <a href="{$lang-prefix}{page/@link}">
                    <xsl:value-of select="@name" />
                </a>
            </td>
            <td><xsl:value-of select="document(concat('upage://', page/@id, '.artikul'))//value" /></td>
            <td><xsl:apply-templates select="amount" /></td>
            <td><xsl:apply-templates select="total-price" mode="discounted-price"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="udata[@method = 'order']/items/item" mode="old_items"><xsl:value-of select="concat(page/@id,'|',amount)" />_</xsl:template>
    <xsl:template match="udata[@method = 'order']/items/item[position() = last()]" mode="old_items"><xsl:value-of select="concat(page/@id,'|',amount)" /></xsl:template>

    <xsl:template match="delivery[.!='']">
        <tr>
            <td colspan="3" class="name">
                <strong>&delivery;</strong>
            </td>
            <td>
                <xsl:value-of select="." />
                <xsl:text> </xsl:text>
                <xsl:value-of select="../@suffix" />
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="discount[.!='']">
        <tr>
            <td colspan="3" class="name">
                <strong>&item-discount;</strong>
            </td>
            <td>
                <xsl:value-of select="." />
                <xsl:text> </xsl:text>
                <xsl:value-of select="../@suffix" />
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>