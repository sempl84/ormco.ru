<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']">
        <!-- <xsl:apply-templates select="document('udata://emarket/cart')" /> -->
        <div class="order-page">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                                <li class="breadcrumb-item"><a href="/emarket/cart/">Корзина товаров</a></li>
                                <li class="breadcrumb-item active">Оформление заказа</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h1>Оформление заказа</h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="white-pnl">
                            <ul class="steps">
                                <li><a href="/emarket/cart/"><i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</a></li>
                                <li class="active"><i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</li>
                                <li><i class="fa fa-play-circle" aria-hidden="true"></i>Заказ оформлен</li>
                            </ul>
                            <xsl:choose>
                                <xsl:when test="$user-type='guest'">
                                    <xsl:value-of select="$settings//property[@name='emarket_auth_text']/value" disable-output-escaping="yes" />
                                    <a href="/users/login/" data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in" class="btn btn-primary" data-gtm-event="ButtonClicks">Вход</a>
                                    &#160;
<!--                                    <a href="#registerModal" data-toggle="modal" data-target="#registerModal" class="btn btn-primary" data-gtm-event="ButtonClicks">Регистрация</a>-->
                                    <a href="#no_register" data-toggle="modal" data-target="#no_register" class="btn btn-primary" data-gtm-event="ButtonClicks">Регистрация</a>
                                    &#160;
                                    <a href="/users/forget/" class="btn btn-primary" data-gtm-event="ButtonClicks">Восстановить пароль</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <script>
                                        <xsl:value-of select="document(concat('udata://emarket/addCheckoutStep2/?v=',$build_version))/udata" disable-output-escaping="yes" />
                                    </script>
                                    <form action="/emarket/saveInfoCust/" method="post" class="without-steps order_purch">
                                        <xsl:apply-templates select="$errors" />
                                        <!-- <xsl:if test="$user/@id =10"> -->
                                        <xsl:if test="document('udata://emarket/needUserInfo/')/udata = 1">
                                            <div class="form-horizontal js-user-profile-form">
                                                <div class="header5">Персональная информация</div>
                                                <p class="required_text">У вас есть неуказанные личные данные. Заполните, пожалуйста, пустые поля отмеченные *</p>
                                                <xsl:apply-templates select="document(concat('udata://data/getEditForm/', $user/@id,'//(short_info)'))/udata/group/field" mode="form" />
                                            </div>
                                        </xsl:if>
                                        <!-- </xsl:if> -->
                                        <xsl:apply-templates select="udata/onestep/customer" />

                                        <xsl:apply-templates select="udata/onestep/delivery_choose" />

                                        <!-- <div class="delivery_address onestep address collapse-in js-collapse-pnl" id="collapseAddress"> -->
                                        <div class="delivery_address onestep address collapse" id="collapseAddress">
                                            <div class="header5">Адрес доставки</div>

                                            <xsl:call-template name="delivery_address">
                                                <xsl:with-param name="context" select="udata/onestep/delivery" />
                                                <xsl:with-param name="block" select="'.delivery_address.onestep'" />
                                            </xsl:call-template>

                                            <div class="form-group">
                                                <div class="header5 delivry_main_header">Желаемая дата и время доставки:</div>
                                                <div class="header5 delivry_russia">Время доставки: 10:00 - 18:00</div>

                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control" id="prefer_date_delivery" name="prefer_date_delivery"/>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select id="prefer_time_delivery" name="prefer_time_delivery" class="selectpicker">
                                                            <option>- Время доставки -</option>
                                                            <option value="10-18">С 10 до 18</option>
                                                            <option value="10-14">С 10 до 14</option>
                                                            <option value="12-18">С 12 до 18</option>
                                                            <option value="14-18">С 14 до 18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#collapseAddress" aria-expanded="true" aria-controls="collapseAddress">
                                            <span>Введите свой адрес</span><span>Свернуть</span>
                                        </button> -->
                                        <xsl:apply-templates select="udata/onestep/payment" />

                                        <div class="ur_data onestep collapse js-collapse-pnl" id="collapseLegalBlock">
                                            <div class="header5">Информация о юр.лице</div>
                                            <xsl:apply-templates select="document('udata://emarket/legalList')/udata" mode="purchase_legal"/>
                                        </div>
                                        <div class="form-group">
                                            <div class="header5">Комментарий к заказу:</div>
                                            <textarea class="form-control" rows="5" name="comment" id="comment"></textarea>
                                        </div>

                                        <div class="text-right total-price">Стоимость заказа: <span>
                                            <xsl:value-of select="$cart/summary/price/actual" disable-output-escaping="yes" /> руб.</span>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 text-right">
                                                <button type="submit" class="btn btn-primary send-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Оформить заказ">Оформить заказ</button>
                                                <span class="er_report"></span>
                                            </div>
                                        </div>
                                    </form>

                                    <xsl:apply-templates select="document('udata://exchange/addExchangeEcommerceCheckoutCode/2/')/udata" />
                                </xsl:otherwise>
                            </xsl:choose>

                            <!-- <div class="address collapse js-collapse-pnl" id="collapseAddress">
                                <div class="header5">Введите свой адрес</div>
                                <div class="form-group row">
                                    <label for="country" class="col-sm-4">Страна:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="country" name="country" placeholder="Страна" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="zip" class="col-sm-4">Почтовый индекс:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="zip" name="zip" placeholder="Почтовый индекс" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="region" class="col-sm-4">Регион:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="region" name="region" placeholder="Регион" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="city2" class="col-sm-4">Город:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="city2" name="city2" placeholder="Город" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="street" class="col-sm-4">Улица:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="street" name="street" placeholder="Улица" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="house" class="col-sm-4">Дом:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="house" name="house" placeholder="Дом" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="flat" class="col-sm-4">Квартира:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="flat" name="flat" placeholder="Квартира" />
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#collapseAddress" aria-expanded="true" aria-controls="collapseAddress">
                                <span>Введите свой адрес</span><span>Свернуть</span>
                            </button> -->

                            <!-- <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#collapsePaymentMethod" aria-expanded="false" aria-controls="collapsePaymentMethod">
                                <span>Выбрать способ оплаты</span><span>Свернуть</span>
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

<!-- <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step'][udata/onestep]">
        <form class="without-steps" action="/emarket/saveInfo" method="POST">
            <xsl:apply-templates select="udata/onestep/customer" />
            <div class="delivery_address onestep">
                <xsl:call-template name="delivery_address">
                    <xsl:with-param name="context" select="udata/onestep/delivery" />
                    <xsl:with-param name="block" select="'.delivery_address.onestep'" />
                </xsl:call-template>
            </div>

            <xsl:apply-templates select="udata/onestep/delivery_choose" />
            <xsl:apply-templates select="udata/onestep/payment" />
            <input type="submit" value="&continue;" class="button big" />
        </form>
    </xsl:template>-->

    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/customer">
        <div class="customer onestep">
            <xsl:apply-templates select="document(concat('udata://data/getEditForm/', @id))" mode="simple_form"/>
            <!-- TODO -->
            <!--
            <div class="form-group row legal">
                <label for="isLegal" class="col-sm-4">Являюсь юридическим лицом</label>
                <div class="col-sm-8">
                    <input type="checkbox" name="isLegal" id="isLegal" />
                </div>
            </div> -->
        </div>
    </xsl:template>

    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery">
        <div class="delivery onestep">
            <div class="header5">Адрес доставки</div>
            <xsl:apply-templates select="items" mode="delivery-address" />
        </div>
    </xsl:template>

    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose" />
    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose[items/item]">
        <div class="dychoose onestep">
            <div class="header5">Способ доставки</div>

            <ul class="form-group shipping-method">
                <!-- <xsl:apply-templates select="items/item[not(@type-class-name='courier')]" mode="delivery-choose" />
                <xsl:apply-templates select="items[item/@type-class-name='courier']" mode="delivery-choose-courier" /> -->
                <xsl:choose>
                    <xsl:when test="$user-type='sv' or $user/@id = 21014 or $user/@id = 752009  or $user/@id = 17024">
                        <!-- <xsl:apply-templates select="items/item[@type-class-name='courier'and not(@id = 165024)]" mode="delivery-choose" /> -->
                        <!-- <xsl:apply-templates select="items/item[not(@id = 165024)]" mode="delivery-choose" /> -->
                        <!--<xsl:apply-templates select="items/item[@id = 13918]" mode="delivery-choose" />-->
                        <xsl:apply-templates select="items/item[@id = 13919]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165023]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165024]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165022]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 1229974]" mode="delivery-choose" />
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- <xsl:apply-templates select="items[item/@type-class-name='courier']" mode="delivery-choose-courier" /> -->
                        <!--
                        <xsl:apply-templates select="items/item[not(@type-class-name='courier')]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 13919]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165023]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165024]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165022]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 1229974]" mode="delivery-choose" /> -->

                        <!--<xsl:apply-templates select="items/item[@id = 13918]" mode="delivery-choose" />-->
                        <xsl:apply-templates select="items/item[@id = 13919]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165023]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165024]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 165022]" mode="delivery-choose" />
                        <xsl:apply-templates select="items/item[@id = 1229974]" mode="delivery-choose" />
                    </xsl:otherwise>
                </xsl:choose>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment" />
    <xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment[items/item]">
        <!-- <div class="payment onestep collapse js-collapse-pnl" id="collapsePaymentMethod"> -->
        <div class="payment onestep" id="collapsePaymentMethod">
            <div>
                <div class="header5">Выбрать способ оплаты</div>
                <ul class="form-group shipping-method">
                    <xsl:choose>
                        <xsl:when test="$user-type='sv' or $user/@id = 21014  or $user/@id = 752009 or $user/@id = 17024">
                            <xsl:apply-templates select="items/item[@id = 1176117]" mode="payment_one_step" />
                            <xsl:apply-templates select="items/item[not(@id = 1176117)][not(@id = 1327116)]" mode="payment_one_step" />
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- <xsl:apply-templates select="items/item[not(@id = 1106960) and not(@id = 1106995)]" mode="payment_one_step" /> -->
                            <xsl:apply-templates select="items/item[@id = 1176117]" mode="payment_one_step" />
                            <xsl:apply-templates select="items/item[not(@id = 1176117)][not(@id = 1327116)]" mode="payment_one_step" />
                        </xsl:otherwise>
                    </xsl:choose>
                </ul>
            </div>
        </div>
        <!-- <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#collapsePaymentMethod" aria-expanded="false" aria-controls="collapsePaymentMethod">
            <span>Выбрать способ оплаты</span><span>Свернуть</span>
        </button> -->
    </xsl:template>

    <xsl:template match="udata" mode="purchase_legal">
        <xsl:apply-templates select="items/item" mode="purchase_legal" />
        <div class="form_element">
            <label>
                <input type="radio" name="legal_item" value="new" class="legal_item">
                    <xsl:if test="not(items/item)">
                        <xsl:attribute name="checked">
                            <xsl:text>checked</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                </input>
                Добавить юр.лицо
            </label>
        </div>

        <div id="new-legal" class="legal_form collapse js-collapse-pnl">
            <xsl:if test="items/item">
                <xsl:attribute name="class">legal_form collapse in js-collapse-pnl</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="document(@xlink:href)/udata" mode="simple_form" />
        </div>
    </xsl:template>

    <xsl:template match="item" mode="purchase_legal">
        <xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata" />
        <div class="form_element">
            <label>
                <input type="radio" name="legal_item" value="{@id}" class="legal_item">
                    <xsl:if test="@active = 'active' or position() = 1">
                        <xsl:attribute name="checked">
                            <xsl:text>checked</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                </input>
                <xsl:value-of select="$item_info//property[@name='name']/value" />
            </label>
        </div>
    </xsl:template>

    <!-- test templates  -->
    <xsl:template match="result[@module = 'emarket' and @method = 'test']" mode="scripts">
        <script>
            var lastJQ = jQuery.noConflict();
        </script>

        <link href="{$template-resources}css/datepicker.min.css" rel="stylesheet" type="text/css" />
        <script src="{$template-resources}js/datepicker.min.js"></script>
        <script src="{$template-resources}js/order1c.js"></script>
    </xsl:template>

    <xsl:template match="result[@module = 'emarket' and @method = 'test']">
        <!-- <xsl:apply-templates select="document('udata://emarket/cart')" /> -->
        <div class="order-page">
            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h1>Test Оформление заказа</h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="white-pnl">
                            <ul class="steps">
                                <li><a href="/emarket/cart/"><i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</a></li>
                                <li class="active"><i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</li>
                                <li><i class="fa fa-play-circle" aria-hidden="true"></i>Заказ оформлен</li>
                            </ul>

                            <form action="/emarket/saveInfoCust/" method="post" class="without-steps order_purch">
                                <xsl:apply-templates select="$errors" />
                                <xsl:apply-templates select="udata/onestep/customer" />
                                <xsl:apply-templates select="udata/onestep/delivery_choose" />
                                <div class="delivery_address onestep address collapse js-collapse-pnl" id="collapseAddress">
                                    <div class="header5">Адрес доставки</div>

                                    <xsl:call-template name="delivery_address">
                                        <xsl:with-param name="context" select="udata/onestep/delivery" />
                                        <xsl:with-param name="block" select="'.delivery_address.onestep'" />
                                    </xsl:call-template>

                                    <div class="form-group">
                                        <div class="header5">Желаемая дата и время доставки:</div>

                                        <div class="row">
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control hasDatepicker" id="prefer_date_delivery" name="prefer_date_delivery"/>
                                            </div>
                                            <div class="col-xs-4">
                                                <select id="prefer_time_delivery" name="prefer_time_delivery" class="selectpicker">
                                                    <option>- Время доставки -</option>
                                                    <option value="10-14">С 10 до 14</option>
                                                    <option value="12-18">С 12 до 18</option>
                                                    <option value="14-18">С 14 до 18</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#collapseAddress" aria-expanded="true" aria-controls="collapseAddress">
                                    <span>Введите свой адрес</span><span>Свернуть</span>
                                </button> -->
                                <xsl:apply-templates select="udata/onestep/payment" />

                                <div class="ur_data onestep collapse js-collapse-pnl" id="collapseLegalBlock">
                                    <div class="header5">Информация о юр.лице</div>
                                    <xsl:apply-templates select="document('udata://emarket/legalList')/udata" mode="purchase_legal"/>
                                </div>

                                <div class="form-group">
                                    <label for="comment">Комментарий к заказу:</label>
                                    <textarea class="form-control" rows="5" name="comment" id="comment"></textarea>
                                </div>

                                <div class="text-right total-price">Стоимость заказа: <span>
                                    <xsl:value-of select="$cart/summary/price/actual" disable-output-escaping="yes" /> руб.</span>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary send-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Оформить заказ">Оформить заказ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>