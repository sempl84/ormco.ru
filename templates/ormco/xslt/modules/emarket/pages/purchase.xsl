<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/result[@method = 'purchase']">
        <xsl:apply-templates select="document('udata://emarket/purchase')" />
    </xsl:template>

    <xsl:template match="purchasing">
        <h4>
            <xsl:text>Purchase is in progress: </xsl:text>
            <xsl:value-of select="concat(@stage, '::', @step, '()')" />
        </h4>
    </xsl:template>

    <xsl:template match="purchasing[@stage = 'result']">
        <div class="order-page">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="/">Главная</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/emarket/cart/">Корзина товаров</a>
                                </li>
                                <li class="breadcrumb-item active">Оформление заказа</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h1>Оформление заказа</h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="white-pnl">
                            <ul class="steps">
                                <li>
                                    <a href="/emarket/cart/"><i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</a>
                                </li>
                                <li >
                                    <a href="/emarket/purchasing_one_step/"><i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</a>
                                </li>
                                <li class="active"><i class="fa fa-play-circle" aria-hidden="true"></i>Заказ не оформлен</li>
                            </ul>
                            <p><xsl:text>&emarket-order-failed;</xsl:text></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="purchasing[@stage = 'result' and @step = 'successful']">
        <xsl:variable name="order" select="document(concat('uobject://', //order/@id))/udata" />
        <xsl:variable name="name" select="$order/object/@name" />

        <div class="order-page">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                                <li class="breadcrumb-item"><a href="/emarket/cart/">Корзина товаров</a></li>
                                <li class="breadcrumb-item active">Оформление заказа</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i><h1>Оформление заказа</h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="white-pnl">
                            <ul class="steps">
                                <li><a href="/emarket/cart/"><i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</a></li>
                                <li ><a href="/emarket/purchasing_one_step/"><i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</a></li>
                                <li class="active"><i class="fa fa-play-circle" aria-hidden="true"></i>Заказ оформлен</li>
                            </ul>

                            <div class="header4"><xsl:value-of select="$name" /> успешно оформлен</div>
                            <p>На e-mail, указанный при регистрации, отправлено подтверждающее письмо. Оплатите заказ в ближайшее время!</p>
                            <div class="header4">Детали заказа</div>
                            <p>Информация о состоянии <xsl:value-of select="$name" /> доступна в личном кабинете в разделе <a href="/emarket/ordersList/">"Мои заказы"</a></p>
                            <!-- <div class="header4"><xsl:value-of select="$name" /> оформлен</div>
                            <p><xsl:value-of select="$name" /> поступил в обработку. На электронный адрес, указанный при оформлении, было выслано письмо с подтверждением. В ближайшее время с вами свяжутся менеджеры Ormco.</p>
                            <div class="header4">Детали заказа</div>
                            <p>Информация о состоянии <xsl:value-of select="$name" /> доступна в личном кабинете в разделе <a href="/emarket/ordersList/">"Мои заказы"</a></p> -->
                            <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 14044 or invoice_link">
                                <p>Платежный документ сохранен в вашем личном кабинете в разделе <a href="/emarket/ordersList/">"Мои заказы"</a>.</p>
                            </xsl:if>

                            <!-- квитанция в банк -->
                            <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 14044">
                                <script>
                                    document.getElementsByTagName("BODY")[0].style.display = "none";
                                    window.location.href = 'http://<xsl:value-of select="$domain" />/tcpdf/docs/receipt.php?oi=<xsl:value-of select="$order/object/@id" />';
                                </script>
                                <br/><a href="/tcpdf/docs/receipt.php?oi={$order/object/@id}" target="_blank" class="btn btn-primary">Открыть квитанцию в банк</a>
                            </xsl:if>

                            <!-- квитанция в банк -->
                            <xsl:if test="invoice_link">
                                <script>
                                    document.getElementsByTagName("BODY")[0].style.display = "none";
                                    window.location.href = 'http://<xsl:value-of select="$domain" />/tcpdf/docs/invoicee.php?oi=<xsl:value-of select="$order/object/@id" />';
                                </script>
                                <br/><a href="/tcpdf/docs/invoicee.php?oi={$order/object/@id}" target="_blank" id="invoice_link_btn" class="btn btn-primary ">Открыть счет для юр.лиц</a>
                            </xsl:if>

                            <br/><br/><a href="&main_category_url;" target="_blank" >Вернуться в каталог</a>


                            <!-- <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 14044">
                                <a href="">Квитанция</a>
                                <script>
                                    jQuery(document).ready(function(){
                                        var url = "http://" + "<xsl:value-of select="$domain" />" + "/emarket/receiptt/<xsl:value-of select="$order/object/@id" />";
                                        var popupParams = "width=650,height=650,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no";
                                        var popup = window.open(url, 'Счет для юридических лиц', popupParams);
                                    });
                                </script>
                            </xsl:if>
                            <xsl:if test="invoice_link">
                                <script>
                                    jQuery(document).ready(function(){
                                        var url = "http://" + "<xsl:value-of select="$domain" />" + "<xsl:value-of select="invoice_link" />";
                                        var popupParams = "width=650,height=650,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no";
                                        var popup = window.open(url, 'Счет для юридических лиц', popupParams);
                                    });
                                </script>
                            </xsl:if> -->
                        </div>
                        <xsl:apply-templates select="document('uhttp://orthodontia.ru/udata/catalog/getSmartCatalogPro//4/3/1/2/publish_date/1/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,hide_reg_button')/udata" mode="events_block"/>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/result[@module = 'emarket' and @method = 'premoderate']">
        <xsl:variable name="order" select="document(concat('uobject://', //orderId))/udata" />
        <xsl:variable name="name" select="$order/object/@name" />

        <div class="order-page">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                                <li class="breadcrumb-item"><a href="/emarket/cart/">Корзина товаров</a></li>
                                <li class="breadcrumb-item active">Оформление заказа</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i><h1>Оформление заказа</h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="white-pnl">
                            <ul class="steps">
                                <li><a href="/emarket/cart/"><i class="fa fa-play-circle" aria-hidden="true"></i>Корзина</a></li>
                                <li ><a href="/emarket/purchasing_one_step/"><i class="fa fa-play-circle" aria-hidden="true"></i>Оформление заказа</a></li>
                                <li class="active"><i class="fa fa-play-circle" aria-hidden="true"></i>Заказ оформлен</li>
                            </ul>

                            <div class="header4"><xsl:value-of select="$name" /> поступил в обработку</div>
                                <p>На e-mail, указанный вами при регистрации, отправлено подтверждающее письмо.</p>
                                <p>После обработки заказа менеджером Ormco, на ваш e-mail будет отправлено еще одно письмо, содержащее ссылку для перехода к оплате заказа. Не пропустите его!</p>
                                <p>Спасибо за то, что выбираете Ormco!</p>
                                <div class="header4">Детали заказа</div>
                                <p>Информация о состоянии <xsl:value-of select="$name" /> доступна в личном кабинете в разделе <a href="/emarket/ordersList/">"Мои заказы"</a></p>
                                <p>Если вы выбирали оплату по счету или квитанции, то платежный документ будет доступен в разделе <a href="/emarket/ordersList/">"Мои заказы"</a> после обработки заказа менеджером Ormco.</p>
                                <!-- <div class="header4"><xsl:value-of select="$name" /> оформлен</div>
                                    <p><xsl:value-of select="$name" /> успешно оформлен. На электронный адрес, указанный вами при оформлении заказа, было выслано письмо с подтверждением. Пожалуйста, оплатите заказ в ближайшее время.</p>
                                    <p>Спасибо за то, что выбираете Ormco!</p>
                                    <div class="header4">Детали заказа</div>
                                    <p>Информация о состоянии <xsl:value-of select="$name" /> доступна в личном кабинете в разделе <a href="/emarket/ordersList/">"Мои заказы"</a></p> -->

                                    <!-- квитанция в банк -->
                                    <!-- <xsl:if test="$order//property[@name='payment_id']/value/item/@id = 14044">
                                <br/><a href="/emarket/receiptt/{$order/object/@id}" target="_blank" class="btn btn-primary">Открыть квитанцию в банк</a>
                            </xsl:if> -->

                            <!-- квитанция в банк -->
                            <!-- <xsl:if test="invoice_link">
                                <br/><a href="/emarket/invoicee/{$order/object/@id}" target="_blank" class="btn btn-primary">Открыть счет для юр.лиц</a>
                            </xsl:if> -->

                            <br/><br/><a href="&main_category_url;" target="_blank" >Вернуться в каталог</a>
                        </div>

                        <xsl:apply-templates select="document('udata://catalog/getSmartCatalogPro//4/3/1/2/publish_date/1/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,hide_reg_button')/udata" mode="events_block"/>
                        <!--<xsl:apply-templates select="document('uhttp://orthodontia.ru/udata/catalog/getSmartCatalogPro//4/3/1/2/publish_date/1/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,hide_reg_button')/udata" mode="events_block"/>-->
                    </div>
                </div>
            </div>
        </div>

        <xsl:apply-templates select="document(concat('udata://exchange/getExchangeEcommerceEmarketPurchaseCode/', //orderId, '/'))/udata" mode="ecommerce-code" />
    </xsl:template>

    <xsl:template match="udata" mode="events_block" />
    <xsl:template match="udata[lines/item]" mode="events_block">
        <div class="white-pnl">
            <div class="header3"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Посмотрите также образовательные мероприятия Ormco</div>
            <div class="row">
                <ul class="events">
                    <xsl:apply-templates select="lines/item" mode="events_block" />
                </ul>
            </div>
            <a class="all-events-lnk" href="http://orthodontia.ru/events/">Посмотреть все образовательные мероприятия</a>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="events_block">
        <xsl:variable name="dateru" select="document(concat('udata://data/dateru/',@publish_date))/udata" />

        <li class="col-sm-4 col-xs-12">
            <div class="date">
                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                <xsl:value-of select="concat($dateru/day,' ',$dateru/month,' ',$dateru/year)"/>
            </div>
            <a href="{@link}">
                <xsl:value-of select="text()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata" mode="ecommerce-code" />
    <xsl:template match="udata[params]" mode="ecommerce-code">
        <div data-gtm-event="ecommercePurchase" data-gtm-value="{params}" data-order_id="{order_id}" />
    </xsl:template>
</xsl:stylesheet>