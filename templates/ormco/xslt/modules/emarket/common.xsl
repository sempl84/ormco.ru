<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="blocks/price.xsl" />
    <xsl:include href="blocks/basket.xsl" />
    <xsl:include href="blocks/payment_online.xsl" />
    <xsl:include href="blocks/discount_info.xsl" />

    <xsl:include href="pages/personal.xsl" />
    <xsl:include href="pages/orders-list.xsl" />
    <xsl:include href="pages/cart.xsl" />
    <xsl:include href="pages/purchase.xsl" />
    <xsl:include href="pages/purchasing-one-step.xsl" />

    <xsl:include href="purchase/required.xsl" />
    <xsl:include href="purchase/delivery.xsl" />
    <xsl:include href="purchase/payment.xsl" />
</xsl:stylesheet>