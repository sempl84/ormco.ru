<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="xsl">
	<xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']" mode="simple_form">
		<xsl:param name="excluded_regions" />

		<xsl:apply-templates select="group" mode="simple_form">
			<xsl:with-param name="excluded_regions" select="$excluded_regions" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="group" mode="simple_form">
		<xsl:param name="excluded_regions" />

		<div class="header5"><xsl:value-of select="@title" /></div>
		<xsl:apply-templates select="field" mode="simple_form">
			<xsl:with-param name="excluded_regions" select="$excluded_regions" />
		</xsl:apply-templates>
	</xsl:template>


	<xsl:template match="field" mode="simple_form">
		<xsl:param name="excluded_regions" />

		<div class="form-group row">
            <label for="{@name}{/udata/@form_id}" class="col-sm-4">
            	<xsl:if test="@required = 'required'">
            		<xsl:attribute name="class">col-sm-4 required</xsl:attribute>
            	</xsl:if>

            	<xsl:value-of select="@title" />
            </label>
            <div class="col-sm-8">
                <xsl:apply-templates select="." mode="simple_form_type">
					<xsl:with-param name="excluded_regions" select="$excluded_regions" />
				</xsl:apply-templates>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="field" mode="simple_form_type">
		<xsl:variable name="cur_name" select="@name" />

		<input type="text" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
        	<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
        	<xsl:if test="not($user-type  = 'guest') and $user-info//property[@name=$cur_name]/value">
        		<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name=$cur_name]/value" /></xsl:attribute>
        		<!-- <xsl:attribute name="disabled">disabled</xsl:attribute> -->
        	</xsl:if>
        	<xsl:if test="not($user-type  = 'guest') and $cur_name = 'email' and $user-info//property[@name='e-mail']/value">
        		<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='e-mail']/value" /></xsl:attribute>
        		<!-- <xsl:attribute name="disabled">disabled</xsl:attribute> -->
        	</xsl:if>

        	<xsl:if test="$cur_name = 'phone'">
        		<xsl:attribute name="class">form-control js-phone</xsl:attribute>
        	</xsl:if>
        </input>
		<xsl:if test="$cur_name = 'e-mail'  and /udata/@method = 'getCreateForm'">
			<span>Это будет ваш логин</span>
		</xsl:if>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="simple_form_type">
		<textarea class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</textarea>
	</xsl:template>

	<xsl:template match="field[@type = 'boolean']" mode="simple_form_type">
		<xsl:attribute name="class">form-group agree</xsl:attribute>
		<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
        	<xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
        </input>
        <span><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" /></span>
	</xsl:template>


	<xsl:template match="field[@type = 'relation']" mode="simple_form_type">
		<xsl:param name="excluded_regions" />

        <select id="{@name}{/udata/@form_id}" name="{@input_name}" data-val="true" class="form-control selectpicker show-tick {@name}_field">
            <!-- <xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if> -->
			<xsl:if test="@required = 'required' or @name='agree'">
				<xsl:attribute name="data-val-required">обязательное поле</xsl:attribute>
				<xsl:attribute name="data-val">true</xsl:attribute>
			</xsl:if>
            <xsl:if test="@multiple">
				<xsl:attribute name="multiple">
					<xsl:text>multiple</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'vuz'">
				<xsl:attribute name="data-size">8</xsl:attribute>
			</xsl:if>

			<option value="" selected="selected" disabled="disabled">
				<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
			</option>
			<xsl:choose>
				<xsl:when test="@name = 'country'">
					<xsl:apply-templates select="values/item[@id != 14538]" mode="webforms_input_type_country" />
					<xsl:apply-templates select="values/item[@id = 14538]" mode="webforms_input_type_country" />
				</xsl:when>
				<xsl:when test="@name = 'vuz'">
					<xsl:apply-templates select="values" mode="webforms_input_type_vuz" />
				</xsl:when>
				<xsl:when test="@name = 'region'">
					<xsl:apply-templates select="values/item[not(@id = $excluded_regions)]" mode="webforms_input_type" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="values/item" mode="webforms_input_type" />
				</xsl:otherwise>
			</xsl:choose>
        </select>
		<div class="required_text" data-valmsg-for="{@input_name}" data-valmsg-replace="true"></div>
	</xsl:template>





	<!-- <xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="webforms_input_type">
		<xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb
		<input type="file" name="{@input_name}" class="textinputs"/>
	</xsl:template> -->



</xsl:stylesheet>