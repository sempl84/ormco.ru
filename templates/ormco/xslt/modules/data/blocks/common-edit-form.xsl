<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="xsl">
	<xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']">
		<xsl:apply-templates select="group" mode="form" />
	</xsl:template>

	<xsl:template match="group" mode="form">
		<h4>
			<xsl:value-of select="@title" />
		</h4>
		<xsl:apply-templates select="field" mode="form" />
		<hr />
	</xsl:template>


	<xsl:template match="field" mode="form">
		<div class="form-group">
            <label for="{@name}{@id}" class="col-xs-3 control-label">
            	<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
            </label>
            <div class="col-sm-9 col-xs-12">
            	<xsl:apply-templates select="." mode="form_field_type" />
            </div>
        </div>
	</xsl:template>

	<xsl:template match="field" mode="form_field_type">
		<input type="text" class="form-control" id="{@name}{@id}" placeholder="{@title}" value="{.}" name="{@input_name}" data-val="true">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'phone'">
				<xsl:attribute name="class">form-control js-phone</xsl:attribute>
			</xsl:if>
		</input>
		<xsl:if test="@name = 'e-mail'  and /udata/@method = 'getCreateForm'">
			<span>Это будет ваш логин</span>
		</xsl:if>
	</xsl:template>

	<xsl:template match="field[@type = 'boolean']" mode="form">
		<div class="form-group agree">
		    <div class="col-sm-offset-3 col-sm-9 col-xs-12">
				<div class="checkbox">
					<label>
						<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="false" >
							<xsl:if test="@checked">
								<xsl:attribute name="value">true</xsl:attribute>
								<xsl:copy-of select="@checked" />
							</xsl:if>
						</input>
				        <input type="checkbox" class="form-control checkbox" value="0" name="{@name}{/udata/@form_id}_fake" id="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
				        	<xsl:if test="@required = 'required' or @name='agree'">
								<xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
								<xsl:attribute name="data-val">true</xsl:attribute>
							</xsl:if>
							<xsl:if test="@checked">
								<xsl:attribute name="value">1</xsl:attribute>
								<xsl:copy-of select="@checked" />
							</xsl:if>
				        </input>
						<span data-t="data_cef">
				        	<xsl:choose>
								<xsl:when test="@name='agree'">&personal_data_agree_text;<xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:when>
								<xsl:otherwise><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:otherwise>
							</xsl:choose>
				        </span>
				        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
					</label>
				</div>
		    </div>
		</div>

		<!-- <xsl:attribute name="class">form-group agree</xsl:attribute>

		<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" class="form-control checkbox" value="0" name="{@name}{/udata/@form_id}_fake" id="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
        	<xsl:if test="@required = 'required' or @name='agree'">
				<xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
				<xsl:attribute name="data-val">true</xsl:attribute>
			</xsl:if>
        </input>
        <span>
        	<xsl:choose>
				<xsl:when test="@name='agree'">&personal_data_agree_text;<xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:otherwise>
			</xsl:choose>
        </span>
        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
        -->
	</xsl:template>

	<xsl:template match="field[@type = 'relation']" mode="form_field_type">
		<xsl:apply-templates select="." mode="simple_form_type" />
	</xsl:template>
	<!-- <xsl:template match="field[@type = 'relation']" mode="form">
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>
				<select type="text" name="{@input_name}">
					<xsl:if test="@multiple = 'multiple'">
						<xsl:attribute name="multiple">multiple</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="values/item" mode="form" />
				</select>
			</label>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="form">
		<option value="{@id}">
			<xsl:copy-of select="@selected" />
			<xsl:value-of select="." />
		</option>
	</xsl:template>


	<xsl:template match="field[@type = 'boolean']" mode="form">
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>
				<input type="hidden" name="{@input_name}" value="0" />
				<input type="checkbox" name="{@input_name}" value="1">
					<xsl:copy-of select="@checked" />
				</input>
			</label>
		</div>
	</xsl:template>


	<xsl:template match="field[@type = 'text' or @type = 'wysiwyg']" mode="form">
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>
				<textarea name="{@input_name}" class="textinputs">
					<xsl:value-of select="." />
				</textarea>
			</label>
		</div>
	</xsl:template>


	<xsl:template match="field[@type = 'file' or @type = 'img_file']" mode="form">
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>

				<input type="file" name="{@input_name}" class="textinputs" />
			</label>
		</div>
	</xsl:template>

	<xsl:template match="@required" mode="form">
		<xsl:attribute name="class">required</xsl:attribute>
	</xsl:template> -->
</xsl:stylesheet>