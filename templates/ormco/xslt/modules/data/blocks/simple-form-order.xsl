<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="xsl">
	<xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']" mode="simple_form_order">
		<xsl:apply-templates select="group" mode="simple_form_order" />
	</xsl:template>
	
	<xsl:template match="group" mode="simple_form_order">
		<xsl:apply-templates select="field" mode="simple_form_order" />
	</xsl:template>


	<xsl:template match="field" mode="simple_form_order">
		<div class="form-group" id="fblock_{@name}{/udata/@form_id}">
			<xsl:apply-templates select="." mode="simple_form_order_type" />
        </div>
	</xsl:template>

	<xsl:template match="field" mode="simple_form_order_type">
		<xsl:variable name="curr_field_name" select="@name" />
		<input type="text" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<!-- <xsl:if test="@required = 'required'"> -->
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			<!-- </xsl:if> -->
			<xsl:if test="@name = 'phone'">
				<xsl:attribute name="class">form-control js-phone</xsl:attribute>
			</xsl:if>
			<xsl:if test="$user-info//property[contains($curr_field_name,@name)]/value">
        		<xsl:attribute name="value"><xsl:value-of select="$user-info//property[contains($curr_field_name,@name)]/value" /></xsl:attribute>
        		<xsl:attribute name="readonly">readonly</xsl:attribute>
        	</xsl:if>
		</input>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="simple_form_order_type">
		<textarea class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true">
			<!-- <xsl:if test="@required = 'required'"> -->
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			<!-- </xsl:if> -->
		</textarea>
	</xsl:template>

	<xsl:template match="field[@type = 'boolean']" mode="simple_form_order_type">
		<xsl:attribute name="class">form-group agree</xsl:attribute>
		
		<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="false" >
			<xsl:if test="@checked">
				<xsl:attribute name="value">true</xsl:attribute>
				<xsl:copy-of select="@checked" />
			</xsl:if>
		</input>
        <input type="checkbox" class="form-control checkbox" value="0" name="{@name}{/udata/@form_id}_fake" id="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
        	<xsl:if test="@required = 'required' or @name='order_agree'">
				<xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
				<xsl:attribute name="data-val">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="@checked">
				<xsl:attribute name="value">1</xsl:attribute>
				<xsl:copy-of select="@checked" />
			</xsl:if>
        </input> 
        <span data-t="data_sfo">
        	<xsl:choose>
				<xsl:when test="@name='order_agree'">&agree_text;<xsl:apply-templates select="." mode="simple_form_order_ast" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:otherwise>
			</xsl:choose>
        	
        </span>
        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
       
	</xsl:template>


	<xsl:template match="field[@type = 'relation']" mode="simple_form_order_type">
		<select id="{@name}{/udata/@form_id}" name="{@input_name}" class="form-control" data-val="true">
            <!-- <xsl:if test="@required = 'required'"> -->
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			<!-- </xsl:if> -->
            <xsl:if test="@multiple">
				<xsl:attribute name="multiple">
					<xsl:text>multiple</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<option value="" selected="selected">
				<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
			</option>
			<xsl:apply-templates select="values/item" mode="webforms_input_type" />
        </select>
	</xsl:template>

	<xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="simple_form_order_type">
		<input type="file" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<!-- <xsl:if test="@required = 'required'"> -->
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			<!-- </xsl:if> -->
		</input>
		<span><xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb</span>
	</xsl:template>
	
	<!-- галочка "я ординатор" -->
	<xsl:template match="field[@type = 'boolean' and @name='ordinator']" mode="simple_form_order_type">
		<xsl:attribute name="class">form-group agree</xsl:attribute>
		<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" >
        	<xsl:attribute name="onclick">
        		<![CDATA[
        			jQuery('#ordinator').val((jQuery(this).prop('checked'))); 
					if (jQuery(this).prop('checked')) {
						$('#fblock_order_upload').fadeIn(200);
					}else{
						$('#fblock_order_upload').fadeOut(200);
					}
        		]]></xsl:attribute>
        	<xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</input>
        <span>
        	<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
	</xsl:template>
	
	<!-- поле для загрузки фотки ординатора -->
	<xsl:template match="field[(@type = 'file' or @type = 'img_file') and @name='order_upload']" mode="simple_form_order_type">
		<input type="file" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</input>
		<span>Загрузите скан либо фото подтверждающего документа <br/><xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb</span>
	</xsl:template>
	
	<!-- <div class="group">
									<div class="ttla double">Я ординатор</div>
									<div class="flda"><input type="checkbox" size="30" name="ordinator" id="ordinator" value="Ординатор" onClick="if (this.checked) {$('#price1').text('5000 руб.'); $('#price2').text('6000 руб.'); $('#finalprice').text('6000 р.'); $('#upload').fadeIn(200);$('#uploaded').fadeIn(200);}else{$('#price1').text('5000 руб.'); $('#price2').text('10000 руб.'); $('#finalprice').text('10000 р.'); $('#upload').fadeOut(200); $('#uploaded').fadeOut(200);}" /></div>
									<div class="flda"><input type="checkbox" size="30" name="ordinator" id="ordinator" value="Ординатор" onClick="if (this.checked) {$('#upload').fadeIn(200);$('#uploaded').fadeIn(200);}else{$('#upload').fadeOut(200); $('#uploaded').fadeOut(200);}" /></div>
								</div>
								<div class="group">
									<div class="fldu"><button class="upload" name="upload" id="upload">Загрузить удостоверение</button></div>
								</div>
								<div class="uploaded" id="uploaded">(форматы jpg, gif, png или pdf)</div>
	 -->
	<!-- <xsl:template match="field" mode="simple_form_order_ast" />
	<xsl:template match="field[@required = 'required']" mode="simple_form_order_ast">
		<xsl:text>*</xsl:text>
	</xsl:template> -->
	<xsl:template match="field" mode="simple_form_order_ast">*</xsl:template>


	
</xsl:stylesheet>