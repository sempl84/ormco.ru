<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="blocks/properties.xsl" />
	<xsl:include href="blocks/common-edit-form.xsl" />
	<xsl:include href="blocks/common-edit-form-simple.xsl" />
	<xsl:include href="blocks/simple-form-order.xsl" />
	<xsl:include href="blocks/common-edit-form-simple_no_title.xsl" />
	<!-- <xsl:include href="blocks/rss.xsl" /> -->
</xsl:stylesheet>