<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
   	xmlns:php="http://php.net/xsl">

	<xsl:template match="udata[@module='slider' and @method='listElements']" mode="slider_block"></xsl:template>
	<xsl:template match="udata[@module='slider' and @method='listElements' and items/item]" mode="slider_block">
        <div class="carousel-holder">
            <div id="slider" class="carousel slide top-slider white-pnl-border" data-ride="carousel">
                <ol class="carousel-indicators">
                    <xsl:apply-templates select="items/item" mode="carousel-indicators" />
                </ol>
                <div class="carousel-inner">
                    <xsl:apply-templates select="items/item" mode="slider_block" />
                </div>
                <a class="left carousel-control" href="#slider" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#slider" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='slider' and @method='listElements']/items/item" mode="slider_block">	
		
		<div class="item">
			<xsl:if test="position()=1">
				<xsl:attribute name="class">item active</xsl:attribute>
			</xsl:if>
			<a href="{.//property[@name='link_for_slide']/value}">
				<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommercePromoView/(slider_', @id, ')/(', php:function('urlencode', string(node())), ')/slider_', position(), '/slider_', position(), '/1/'))/udata" />

				<img class="img-responsive" src="{.//property[@name='right_img']/value}" alt="{text()}" />
			</a>
        </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='slider' and @method='listElements']/items/item" mode="carousel-indicators">	
		<li data-target="#slider" data-slide-to="{position()-1}">
			<xsl:if test="position()=1">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if>
		</li>
	</xsl:template>

</xsl:stylesheet>