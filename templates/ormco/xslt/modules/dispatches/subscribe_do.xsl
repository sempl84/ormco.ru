<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">

	<xsl:template match="result[@module = 'dispatches'][@method = 'subscribe_do']">
		<div class="article-page">
			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
			
			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="fa fa-home" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	            </div>
	        </div>
	        
	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <article class="white-pnl" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<!-- <xsl:copy-of select="document('udata://dispatches/subscribe_do/')/udata" /> -->
							<xsl:apply-templates select="document('udata://dispatches/subscribe_do/')/udata" />
	                    </article>
	                    <xsl:call-template name="social_share" />
	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
	            </div>
	        </div>
			<!--Content -->
		</div>
		
	</xsl:template>

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe_do']">
		<xsl:apply-templates select="result" mode="subscribe_do" />
	</xsl:template>

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe_do'][unsubscribe_link]">
		<xsl:if test="$user-type = 'guest'">
			<p>&dispatch-you-to;.</p>
			<!-- <p>&dispatch-unsubscribe; <a href="{unsubscribe_link}">&dispatch-unsubscribe-part;</a>.</p> -->
		</xsl:if>
	</xsl:template>

	<xsl:template match="result" mode="subscribe_do">
		<xsl:choose>
			<xsl:when test="$user-type != 'guest'">
				<p>&dispatch-you-from;.</p>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
				<!-- <p>Если Вы не хотите получать нашу рассылку, Вы можете отказаться от подписки в <a href="/dispatches/subscribe/">личном кабинете</a>.</p> -->
		
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="result[@class = 'error']" mode="subscribe_do">
		<p><xsl:value-of select="."/></p>
	</xsl:template>

	<xsl:template match="result[items]" mode="subscribe_do">
		<p>&dispatch-you-to;</p>
		<!-- <p>Если Вы не хотите получать нашу рассылку, Вы можете отказаться от подписки в <a href="/dispatches/subscribe/">личном кабинете</a>.</p> -->
		<!-- <ul><xsl:apply-templates select="items" mode="subscribe_do" /></ul> -->
	</xsl:template>

	<xsl:template match="items" mode="subscribe_do">
		<li><xsl:value-of select="." disable-output-escaping="yes" /></li>
	</xsl:template>

</xsl:stylesheet>