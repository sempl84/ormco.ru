<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe']" mode="right">
		<div class="subscription-pnl">
            <form class="show-placeholder" action="{$lang-prefix}/dispatches/subscribe_do/" name="sbs_frm" method="post" onsubmit="ga('send', 'event', 'form', 'subscribe');ym(10184890, 'reachGoal', 'Subscribe');">
				<input type="text" id="subscribe" name="sbs_mail" placeholder="Ваша Эл. почта" />
            	<button type="submit">Ок</button>
            	<span>— Подписаться на рассылку</span>
			</form>
            
        </div>
        <!-- 
		<div class="infoblock">
			<div class="title"><h2><xsl:text>&newsletters;</xsl:text></h2></div>
			<div class="body">
				<div class="in">
					<form action="{$lang-prefix}/dispatches/subscribe_do/" name="sbs_frm" method="post">
						<ul>
							<li><input	type="text"
									onblur="if(this.value == '') this.value = '&e-mail;';"
									onfocus="if(this.value == '&e-mail;') this.value = '';"
									value="&e-mail;"
									class="input"
									id="subscribe"
									name="sbs_mail" /></li>
						</ul>
						<input type="submit" class="button" value="&subscribe;" />
					</form>
				</div>
			</div>
		</div> -->
	</xsl:template>

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe'][subscriber_dispatches]" mode="right">
		<div class="subscription-pnl">
            <form class="show-placeholder" action="{$lang-prefix}/dispatches/subscribe_do/" name="sbs_frm" method="post" onsubmit="ga('send', 'event', 'form', 'subscribe');ym(10184890, 'reachGoal', 'Subscribe');">
				<input type="text" id="subscribe" name="sbs_mail" placeholder="Ваша Эл. почта" />
            	<button type="submit">Ок</button>
            	<span>— Подписаться на рассылку</span>
			</form>
            
        </div>
		<div style="display:none">
			<xsl:apply-templates select="subscriber_dispatches" mode="right" />
		</div>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'dispatches' and @method = 'subscribe' and subscriber_dispatches/items/@is_checked = '1']" mode="right" />

	<xsl:template match="subscriber_dispatches" mode="right" />

	<xsl:template match="subscriber_dispatches[items]" mode="right">
		<div class="infoblock">
			<div class="title"><h2><xsl:text>&newsletters;</xsl:text></h2></div>
			<div class="body">
				<div class="in">
					<form action="{$lang-prefix}/dispatches/subscribe_do/" name="sbs_frm" method="post" onsubmit="ga('send', 'event', 'form', 'subscribe');ym(10184890, 'reachGoal', 'Subscribe');">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<ul><xsl:apply-templates select="items[2]" mode="dispatches" /></ul>
							</xsl:when>
							<xsl:otherwise>
								<ul><xsl:apply-templates select="items[1]" mode="dispatches" /></ul>
							</xsl:otherwise>
						</xsl:choose>
						<input type="submit" class="button" value="&subscribe;" />
					</form>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="result[@module = 'dispatches'][@method = 'subscribe']">
		<div class="my-orders-page">
	        <!--Breadcrumbs -->
			<div class="breadcrumbs-wrapper">
		        <div class="container-fluid">
		            <div class="row">
		                <div class="col-xs-12">
		                    <ol class="breadcrumb">
		                        <li class="breadcrumb-item"><a href="/">Главная</a></li>
		                        <li class="breadcrumb-item"><a href="/users/settings/">Личный кабинет</a></li>
		                        <li class="breadcrumb-item active"><xsl:value-of select="@header" disable-output-escaping="yes" /></li>
		                    </ol>
		                </div>
		            </div>
		        </div>
		    </div>
	
	        <!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	            </div>
	        </div>
	
	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <aside class="col-lg-3 col-md-3 col-xs-12 right-pnl">
	                    <xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata" mode="left_menu" />
	                    
	                </aside>
	                <div class="col-lg-9 col-md-9 col-xs-12 main-col main-col-mb white-pnl-simple">
	                	<div style="display:none"><xsl:copy-of select="document(concat('udata://dispatches/unsubscribeLink/',$user-info//property[@name='e-mail']/value))/udata" /></div>
	                    <!-- <p>&dispatch-unsubscribe; <a href="#">&dispatch-unsubscribe-part;</a>.</p> -->
	                </div>
	            </div>
	        </div>
	    </div>
		
	</xsl:template>

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe']">
		<div>
			<input	type="text"
					onblur="javascript: if(this.value == '') this.value = '&e-mail;';"
					onfocus="javascript: if(this.value == '&e-mail;') this.value = '';"
					value="&e-mail;"
					class="input"
					id="subscribe"
					name="sbs_mail" />
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'dispatches'][@method = 'subscribe'][subscriber_dispatches]">
		<xsl:apply-templates select="subscriber_dispatches" />
	</xsl:template>

	<xsl:template match="subscriber_dispatches" />

	<xsl:template match="subscriber_dispatches[items]">
		<ul><xsl:apply-templates select="items" mode="dispatches" /></ul>
	</xsl:template>

	<xsl:template match="items" mode="dispatches">
		<li>
			<label>
				<input type="checkbox" name="subscriber_dispatches[{@id}]" value="{@id}">
					<xsl:if test="@is_checked = '1'">
						<xsl:attribute name="checked">
							<xsl:text>checked</xsl:text>
						</xsl:attribute>
					</xsl:if>
				</input>
				<xsl:value-of select="." />
			</label>
		</li>
	</xsl:template>

</xsl:stylesheet>