<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="user" mode="header_auth_link">
		<div class="sign-links dropdown js-product-menu">
			<a role="button" data-toggle="dropdown" href="#" class="link-btn round" data-gtm-event="ButtonClicks" data-gtm-event-value="Кабинет">
				Кабинет <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu">
				<xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata/item" mode="getCategoryListFill" />
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="user[@type = 'guest']" mode="header_auth_link">
		<div class="sign-links">
			<a href="/users/login/" data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in" class="link-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Вход">Вход</a>
<!--			<a href="#registerModal" data-toggle="modal" data-target="#registerModal" class="link-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Регистрация">Регистрация</a>-->
			<a href="#no_register" data-toggle="modal" data-target="#no_register" class="link-btn" data-gtm-event="ButtonClicks" data-gtm-event-value="Регистрация">Регистрация</a>
		</div>
	</xsl:template>


	<xsl:template match="user" mode="header_auth_link_mobile">
		<a href="/users/settings/" class="login cabinet"></a>
	</xsl:template>

	<xsl:template match="user[@type = 'guest']" mode="header_auth_link_mobile">
        <a href="{$lang-prefix}/users/login/" data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in" class="login"></a>
	</xsl:template>
</xsl:stylesheet>