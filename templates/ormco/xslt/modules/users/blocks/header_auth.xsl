<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/TR/xlink">
    <!-- для зарегистрированного пользователя
    modal - регистрация на мероприятие -->
    <xsl:template match="user" mode="header_auth">
        <div class="modal fade register-modal wide-modal buy-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="buyOneClick" action="/emarket/registerEventOneClick/" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <div class="modal-title header4" id="gridAuth">Регистрация на мероприятие</div>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="page_id_input" name="page_id" value="{$document-page-id}" />
                            <xsl:apply-templates select="document('udata://data/getCreateForm/&order-tid;//(form_params)')" mode="simple_form_order"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-green pull-left buyOneClickButton">Зарегистрироваться</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </xsl:template>

    <!--
    для гостя
    modal - регистрация на мероприятие с формой авторизации и пояснительным текстом
    modal - регистрация пользователя
    modal - авторизация пользователя
    -->
    <xsl:template match="user[@type = 'guest']" mode="header_auth">
        <xsl:variable name="registrate_type_id" select="document('udata://users/registrate')" />
        <xsl:variable name="reg_fields" select="document($registrate_type_id/udata/@xlink:href)" />


        <div class="modal fade register-modal wide-modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="js-user-profile-form show-placeholder" action="/users/registrate_do_pre/" method="post" data-gtm-event="formFilling" data-gtm-event-form="Регистрация">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <div class="modal-title header4" id="gridRegister">Регистрация</div>
                        </div>
                        <div class="modal-body">
                            <div class="info_block">
                                <xsl:value-of select="$settings//property[@name='site_reg_info']/value" disable-output-escaping="yes" />
                            </div>

                            <h5>Представьтесь, пожалуйста</h5>
							<div class="form-group fn_block_lname hid"><input type="text" class="form-control" id="phone1" name="data[new][phone1]" placeholder="Телефон*" /></div>
                            <xsl:apply-templates select="$reg_fields//field[@name='lname']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='fname']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='father_name']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='prof_status']" mode="simple_form_no_title"/>
                            <div class="ord_dop_fields collapse ord_dop_fields3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="city567" placeholder="Другая специализация*" name="data[new][other_specialization]" data-val="true" data-val-required="true"/>
                                </div>
                            </div>

							<div class="student_dop_field ord_dop_fields collapse">
								<xsl:apply-templates select="$reg_fields//field[@name='vuz']" mode="simple_form_no_title"/>
							</div>

							<hr />
                            <h5>Контакты для связи</h5>
                            <xsl:apply-templates select="$reg_fields//field[@name='e-mail']" mode="simple_form_no_title"/>

                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" value="" placeholder="Пароль" data-val="true" data-val-required="true"/>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password_confirm" name="password_confirm" value="" placeholder="Повторить Пароль" data-val="true" data-val-required="true"/>
                            </div>

                            <xsl:apply-templates select="$reg_fields//field[@name='country']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='region']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='city']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='phone']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="$reg_fields//field[@name='agree']" mode="simple_form_no_title"/>
                            <xsl:apply-templates select="document('udata://system/captcha')" />
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary pull-left">Зарегистрироваться</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade register-modal wide-modal" id="authModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="show-placeholder" action="/users/login_do_pre" method="post" data-gtm-event="formFilling" data-gtm-event-form="Авторизация">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-gtm-event="ButtonClicks" data-gtm-event-value="Закрыть окно авторизации">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <div class="modal-title header4" id="gridAuth">Авторизация</div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Пароль" />
                            </div>
							<p class="popup_auth_error_message"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary pull-left" data-gtm-event="ButtonClicks" data-gtm-event-value="Вход">Вход</button>
                            <a href="{$lang-prefix}/users/forget/" class="forget_pas" data-gtm-event="ButtonClicks">Забыли пароль?</a>
                            <br/>
                            <a href="/users/registrate/" data-gtm-event="ButtonClicks">Регистрация</a>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade register-modal wide-modal buy-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="show-placeholder" action="/users/login_do_pre" method="post" data-gtm-event="formFilling" data-gtm-event-form="Авторизация">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <div class="modal-title header4" id="gridAuth">Авторизация</div>
                        </div>
                        <div class="modal-body">
                            Для регистрации на мероприятие вам необходимо<br/>
<!--                            <a href="#registerModal" data-toggle="modal" data-target="#registerModal" data-dismiss="modal">зарегистрироваться</a> или авторизоваться-->
                            <a href="#no_register" data-toggle="modal" data-target="#no_register" data-dismiss="modal">зарегистрироваться</a> или авторизоваться
                            <div class="form-group">
                                <input type="text" class="form-control" id="email2" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Пароль" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary pull-left">Вход</button>
                            <a href="{$lang-prefix}/users/forget/" class="forget_pas">Забыли пароль?</a>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </xsl:template>
</xsl:stylesheet>
