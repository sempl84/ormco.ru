<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- шаблон для текстового поля в формах (для регистрации, авторизации и т.п.) -->
	<xsl:template name="form_field">	
		<xsl:param name="title"/>
		<xsl:param name="name"/>
		<xsl:param name="field_name" />

		<div class="form-group">
            <label for="inputName" class="col-xs-3 control-label"><xsl:value-of select="$title" /></label>
            <div class="col-xs-9">
                <input type="text" class="form-control" id="{$name}" placeholder="{$title}" name="{$field_name}" data-val="true" data-val-required="true" />
            </div>
        </div>
	</xsl:template>


</xsl:stylesheet>