<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="user" mode="right_col_auth">
	</xsl:template>

	<xsl:template match="user[@type = 'guest']" mode="right_col_auth">
		<div class="banner blue">
            <img src="{$template-resources}img/people-icon.png" alt="library" />
            <p>Как получить расширенный доступ к материалам?</p>
<!--            <a class="btn btn-white" href="#registerModal" data-toggle="modal" data-target="#registerModal">Зарегистрироваться</a>-->
            <a class="btn btn-white" href="#no_register" data-toggle="modal" data-target="#no_register">Зарегистрироваться</a>
            <p class="or-txt">или</p>
            <a class="btn btn-white" href="/users/login/" data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in">Войти</a>
        </div>
	</xsl:template>

</xsl:stylesheet>