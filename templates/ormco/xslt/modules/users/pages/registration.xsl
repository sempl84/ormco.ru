<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:param name="address" />

    <xsl:template match="result[@method = 'registrate']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="#" data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in">Открыть форму регистрации</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'registrate']">
        <xsl:variable name="reg_fields" select="document(@xlink:href)" />

        <form class="form-horizontal js-user-profile-form " id="registrate" enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/registrate_do_pre/">
            <h4>Представьтесь, пожалуйста</h4>
			<div class="form-group fn_block_lname hid"><input type="text" class="form-control" id="phone1" name="data[new][phone1]" placeholder="Телефон*" /></div>
            <xsl:apply-templates select="$reg_fields//field[@name='lname']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='fname']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='father_name']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='prof_status']" mode="form"/>
            <div  class="ord_dop_fields collapse ord_dop_fields3">
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Другая специализация*</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control"  placeholder="Другая специализация*" name="data[new][other_specialization]" data-val="true" data-val-required="true"/>
                    </div>
                </div>
            </div>

            <div class="student_dop_field ord_dop_fields collapse">
				<xsl:apply-templates select="$reg_fields//field[@name='vuz']" mode="form"/>
            </div>

            <hr />
            <h4>Контакты для связи</h4>
            <xsl:apply-templates select="$reg_fields//field[@name='e-mail']" mode="form"/>

            <div class="form-group">
                <label for="inputOldPassword" class="col-xs-3 control-label">Пароль</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" data-val="true" data-val-length-min="3" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="inputNewPassword" class="col-xs-3 control-label">Пароль ещё раз</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Новый пароль ещё раз" data-val="true" data-val-equalto="true" data-val-equalto-other="password" data-val-required="true"/>
                    <span>Придумайте пароль не короче 3 символов.</span>
                </div>
            </div>

<!--            <xsl:apply-templates select="$reg_fields//field[@name='country']" mode="form"/>-->
<!--            <xsl:apply-templates select="$reg_fields//field[@name='region']" mode="form"/>-->
<!--            <xsl:apply-templates select="$reg_fields//field[@name='city']" mode="form"/>-->
            <xsl:apply-templates select="$reg_fields//field[@name='phone']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='agree']" mode="form"/>
            <xsl:apply-templates select="document('udata://system/captcha')" />

            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-primary" data-gtm-event="ButtonClicks" data-gtm-event-value="Зарегистрироваться">Зарегистрироваться</button>
                </div>
            </div>
        </form>
    </xsl:template>

    <xsl:template match="result[@method = 'registrate_done']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:apply-templates select="document('udata://users/registrate_done')/udata"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'registrate_done']">
        <xsl:choose>
            <xsl:when test="result = 'without_activation'">
                <h4>
                    <xsl:text>&registration-done;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:when test="result = 'error'">
                <h4>
                    <xsl:text>&registration-error;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:when test="result = 'error_user_exists'">
                <h4>
                    <xsl:text>&registration-error-user-exists;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:otherwise>
                <h4>
                    <xsl:text>&registration-done;</xsl:text>
                </h4>
                <p>
                    <xsl:text>&registration-activation-note;</xsl:text>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="result[@method = 'activate']">
        <xsl:variable name="activation-errors" select="document('udata://users/activate')/udata/error" />

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:choose>
                                    <xsl:when test="count($activation-errors)">
                                        <xsl:apply-templates select="$activation-errors" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p>
                                            <xsl:text>&account-activated;</xsl:text>
                                        </p>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>


    <!-- User settings -->
    <xsl:template match="result[@method = 'settings']">
        <div class="my-orders-page1">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="/">Главная</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/users/settings/">Личный кабинет</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <xsl:value-of select="@header" disable-output-escaping="yes" />
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
                    <h1>
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <aside class="col-lg-3 col-md-3 col-xs-12 right-pnl">
                        <xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata" mode="left_menu" />

                    </aside>
                    <div class="col-lg-9 col-md-9 col-xs-12 main-col main-col-mb white-pnl-simple">
                        <xsl:apply-templates select="$errors" />
                        <!-- форма с данными -->
                        <xsl:apply-templates select="document('udata://users/settings')" />
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'settings']">
        <xsl:variable name="legalList" select="document('udata://emarket/legalList')" />
        <xsl:variable name="addressList" select="document('udata://emarket/addressList')" />
        <xsl:variable name="setting_fields" select="document(concat('udata://data/getEditForm/', $user-id))" />

        <xsl:if test="$address = 'ok'">
            <div class="text-success"><p>Данные успешно сохранены</p></div>
        </xsl:if>

        <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/settings_do_pre/" id="personal_information_form" class="form-horizontal js-user-profile-form">
            <h4>Персональная информация</h4>
            <xsl:apply-templates select="$setting_fields//field[@name='lname']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='fname']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='father_name']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='prof_status']" mode="form"/>
            <div  class="ord_dop_fields collapse ord_dop_fields3">
                <xsl:if test="not($user-type  = 'guest') and ($user-info//property[@name='other_specialization']/value or $user-info//property[@name='prof_status']/value/item/@id = 14645)">
                    <xsl:attribute name="class">ord_dop_fields collapse in ord_dop_fields3</xsl:attribute>
                </xsl:if>
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Другая специализация*</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control"  placeholder="Другая специализация*" name="data[{$user-id}][other_specialization]" data-val="true" data-val-required="true">
                            <xsl:if test="not($user-type  = 'guest') and $user-info//property[@name='other_specialization']/value">
                                <xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='other_specialization']/value" /></xsl:attribute>
                            </xsl:if>
                        </input>
                    </div>
                </div>
            </div>

            <hr />
            <h4>Контакты для связи</h4>
            <xsl:apply-templates select="$setting_fields//field[@name='e-mail']" mode="form"/>

			<xsl:if test="$setting_fields//field[@name='email_dlya_izmeneniya']/text() != ''">
				<div class="col-xs-offset-3 col-xs-9">
					<p>Вы запросили изменение e-mail адреса. Новый e-mail <a href="mailto:{$setting_fields//field[@name='email_dlya_izmeneniya']/text()}"><xsl:value-of select="$setting_fields//field[@name='email_dlya_izmeneniya']/text()" /></a>.</p>
					<p>Чтобы подтвердить изменение - перейдите по ссылке в письме, отправленном вам на новый e-mail. Если вы не получили ссылку - вы можете повторно отправить письмо на новый e-mail со ссылкой для подтвердения.</p>
					<p>
						<a href="/udata/users/send_confirm_email_letter/" class="btn btn-primary">Отправить повторное письмо</a>
						<xsl:text> </xsl:text>
						<a href="/udata/users/cancel_change_email/" class="btn btn-primary">Отменить изменение e-mail</a>
					</p>
				</div>
			</xsl:if>

            <xsl:choose>
                <xsl:when test="document('udata://ormcoSiteData/getSiteDataDadataToken/')/udata/token">
                    <div class="form-group">
                        <label for="address" class="col-xs-3 control-label">Адрес*</label>
                        <div class="col-xs-7"><input type="text" class="form-control" id="address" placeholder="Адрес*" value="{$setting_fields//field[@name = 'user_address_raw']/node()}" data-val="true" data-val-required="true" disabled="" style="color:#999" /></div>
                        <div class="col-xs-2"><a class="btn btn-primary" href="/users/address/">Изменить</a></div>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="$setting_fields//field[@name='country']" mode="form"/>
                    <xsl:apply-templates select="$setting_fields//field[@name='region']" mode="form"/>
                    <xsl:apply-templates select="$setting_fields//field[@name='city']" mode="form"/>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:apply-templates select="$setting_fields//field[@name='phone']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='agree']" mode="form"/>

            <h4>Сменить пароль</h4>
            <div class="form-group">
                <label for="inputOldPassword" class="col-xs-3 control-label">Новый пароль</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" data-val="true" data-val-length-min="3" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputNewPassword" class="col-xs-3 control-label">Новый пароль ещё раз</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Новый пароль ещё раз" data-val="true" data-val-equalto="true" data-val-equalto-other="password" />
                    <span>Придумайте пароль не короче 3 символов.</span>
                </div>
            </div>
            <hr />
            <div class="checkbox add-more">
                <label>
                    <input type="checkbox" data-toggle="collapse" data-target="#collapseLegalEntity1">
                        <xsl:if test="$legalList/udata/items/item">
                            <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                    </input>
                    Оплачивать от юр. лица
                </label>
            </div>

            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-primary settingsFormSubmit" data-gtm-event="ButtonClicks" data-gtm-event-value="Сохранить">Сохранить</button>
                </div>
            </div>
        </form>

        <div class="collapse" id="collapseLegalEntity1">
            <xsl:if test="$legalList/udata/items/item">
                <xsl:attribute name="class">collapse in</xsl:attribute>
            </xsl:if>

            <xsl:apply-templates select="$legalList" />
            <hr />
        </div>

        <!-- вывод адресовов доставки -->
        <xsl:apply-templates select="$addressList" />
        <hr />
    </xsl:template>

    <!-- legalList -->
    <xsl:template match="udata[@method = 'legalList']">
        <xsl:choose>
            <xsl:when test="@xlink:href">
                <div class="table-responsive legallist_wrap_first">
                    <table class="table legallist_wrap">
                        <tbody>
                            <tr>
                                <th colspan="2">Мои юр. лица</th>
                            </tr>
                            <xsl:apply-templates select="items/item" mode="legalList"/>
                            <tr>
                                <td colspan="2">
                                    <div class="button_develiry1">
                                        <a href="#" onclick="jQuery('#addLegal').show();return false">
                                            <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" onclick="jQuery('#addLegal').show();return false">
                                            Добавить еще юр. лицо
                                        </a>
                                    </div>

                                    <div id="addLegal" style="display:none">
                                        <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/legalAdd/" class="form-horizontal js-user-profile-form">
                                            <xsl:apply-templates select="document(@xlink:href)//field" mode="form" />
                                            <div class="form-group">
                                                <div class="col-xs-offset-3 col-xs-9">
                                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </xsl:when>
            <xsl:otherwise>
                Вам необходимо <a href="{$lang-prefix}/users/login">авторизоваться</a> или <a href="{$lang-prefix}/users/registrate">зарегистрироваться</a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="item" mode="legalList">
        <xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata" />
        <tr>
            <td class="text-left">
                <div class="item_develiry">
                    <xsl:value-of select="$item_info//property[@name='name']/value" />
                </div>
                <div id="editLegal{@id}" class="col-md-12 editLegal" style="display:none">
                    <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/legalAdd/{@id}" class="form-horizontal js-user-profile-form">
                        <xsl:apply-templates select="document(concat('udata://data/getEditForm/', @id))//field" mode="form" />
                        <div class="form-group">
                            <div class="col-xs-offset-3 col-xs-9">
                                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </div>
                    </form>
                </div>
            </td>
            <td class="but_list">
                <a href="#" class="btn btn-primary link_edit_legal" data-id="{@id}" title="Изменить">
                    <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                </a>
                <a href="/udata/emarket/legal_delete?legal-person={@id}" class="btn btn-danger" title="Удалить" onclick="return confirm('Вы уверены, что хотите удалить выбранное юридическое лицо? Отменить данное действие будет невозможно.');">
                    <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </xsl:template>


    <!-- addressList -->
    <xsl:template match="udata[@method = 'addressList']">
        <xsl:choose>
            <xsl:when test="@xlink:href">
                <div class="table-responsive legallist_wrap_first">
                    <table class="table legallist_wrap">
                        <tbody>
                            <tr>
                                <th colspan="2">Мои адреса доставки</th>
                            </tr>
                            <xsl:apply-templates select="items/item" mode="addressList"/>
                            <tr>
                                <td colspan="2">
                                    <div class="button_develiry1">
                                        <a href="javascript:void(0);" onclick="jQuery('#addressForm').show();return false">
                                            <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                                        </a>
                                        <a href="javascript:void(0);" onclick="jQuery('#addressForm').show();return false" data-gtm-event="ButtonClicks">
                                            Добавить еще адрес доставки
                                        </a>
                                    </div>

                                    <div id="addressForm" style="display:none">
                                        <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/adressAdd/" class="form-horizontal js-user-profile-form">
                                            <xsl:apply-templates select="document(@xlink:href)//field" mode="form" />
                                            <div class="form-group">
                                                <div class="col-xs-offset-3 col-xs-9">
                                                    <button type="submit" class="btn btn-primary" data-gtm-event="ButtonClicks">Добавить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </xsl:when>
            <xsl:otherwise>
                Вам необходимо <a href="{$lang-prefix}/users/login">авторизоваться</a> или <a href="{$lang-prefix}/users/registrate">зарегистрироваться</a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="item" mode="addressList">
        <xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata" />
        <tr>
            <td class="text-left">
                <div class="item_develiry">
                    <xsl:apply-templates select="$item_info//property" mode="delivery-address" />
                </div>
                <div id="editLegal{@id}" class="col-md-12 editLegal" style="display:none">
                    <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/adressAdd/{@id}" class="form-horizontal js-user-profile-form">
                        <xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field" mode="form" />
                        <div class="form-group">
                            <div class="col-xs-offset-3 col-xs-9">
                                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </div>
                    </form>
                </div>
            </td>
            <td class="but_list">
                <a href="#" class="btn btn-primary link_edit_legal" data-id="{@id}" title="Изменить" data-gtm-event="ButtonClicks" data-gtm-event-value="Изменить адрес доставки">
                    <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                </a>
                <a href="/udata/emarket/address_delete/?address_id={@id}" class="btn btn-danger link_delete_address" data-id="{@id}" title="Удалить" onclick="return confirm('Вы уверены, что хотите удалить выбранный адрес? Отменить данное действие будет невозможно.');"  data-gtm-event="ButtonClicks" data-gtm-event-value="Удалить адрес доставки">
                    <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>