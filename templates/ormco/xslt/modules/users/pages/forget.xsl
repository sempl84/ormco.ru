<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="result[@method = 'forget']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:apply-templates select="$errors" />
                                <form class="form-horizontal" id="forget" method="post" action="{$lang-prefix}/users/forget_do/">
                                    <input type="hidden" name="from_page" value="{from_page}" />
                                    <div class="form-group">
                                        <label for="e-mail" class="col-xs-4 control-label">
                                            <xsl:text>&forget-email;:</xsl:text>
                                        </label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" class="form-control" id="e-mail" placeholder="&e-mail;*" name="forget_email" data-val="true" data-val-required="true" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8  col-xs-12">
                                            <button type="submit" class="btn btn-primary" data-gtm-event="ButtonClicks" data-gtm-event-value="Отправить письмо">&forget-button;</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="result[@method = 'forget_do']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>
                                    <xsl:text>&restore-note;</xsl:text>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="result[@module = 'users' and @method = 'update_password']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata">
            <xsl:with-param name="header" select="udata/@header" />
        </xsl:apply-templates>

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="udata/@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:apply-templates select="$errors" />

                                <xsl:choose>
                                    <xsl:when test="udata/@status = 'success'">
                                        <form class="form-horizontal" id="forget" method="post" action="{$lang-prefix}/users/set_new_password/">
                                            <input type="hidden" name="from_page" value="{from_page}" />
                                            <div class="form-group">
                                                <label for="e-mail" class="col-xs-3 control-label">
                                                    <xsl:text>&new-password;:</xsl:text>
                                                </label>
                                                <div class="col-sm-9 col-xs-12">
                                                    <input type="hidden" name="param0" value="{udata/@activate_code}" />
                                                    <input type="text" class="form-control" placeholder="&new-password;*" name="new_password" data-val="true" data-val-required="true" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9  col-xs-12">
                                                    <button type="submit" class="btn btn-primary">&new-password-button;</button>
                                                </div>
                                            </div>
                                        </form>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p>&new-password-error;</p>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="result[@method = 'set_new_password']">
        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata">
            <xsl:with-param name="header" select="udata/@header" />
        </xsl:apply-templates>

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="udata/@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl-simple">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>
                                    <xsl:value-of select="udata/restore_result" disable-output-escaping="yes" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>