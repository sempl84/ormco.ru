<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
                   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                   xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'users' and @method = 'discounts']">
        <div class="my-orders-page1">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="/">Главная</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/users/settings/">Личный кабинет</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <xsl:value-of select="@header" disable-output-escaping="yes" />
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
                    <h1>
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <aside class="col-lg-3 col-md-3 col-xs-12 right-pnl">
                        <xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata" mode="left_menu" />
                    </aside>
                    <div class="col-lg-9 col-md-9 col-xs-12 main-col main-col-mb">
                        <xsl:apply-templates select="$errors" />

                        <xsl:apply-templates select="document('udata://users/getUsersPersonalDiscount/')/udata" mode="discounts-user" />

                        <div class="pf_module">
                            <div class="pf_module_title pf_blue">Активные промокоды</div>

                            <xsl:apply-templates select="document('udata://ormcoCoupons/getOrmcoCouponsActiveItems/')/udata" mode="discounts-coupon-active" />
                        </div>

<!--                        <xsl:apply-templates select="document('udata://ormcoCoupons/getOrmcoCouponsHistoryUsedItems/')/udata" mode="history-items">-->
<!--                            <xsl:with-param name="title">Вы уже использовали</xsl:with-param>-->
<!--                        </xsl:apply-templates>-->

<!--                        <xsl:apply-templates select="document('udata://ormcoCoupons/getOrmcoCouponsHistoryNotUsedItems/')/udata" mode="history-items">-->
<!--                            <xsl:with-param name="title">Просроченные промокоды</xsl:with-param>-->
<!--                        </xsl:apply-templates>-->
                        <xsl:if test="$settings//property[@name = 'tekst_stranicy_moi_skidki']/value">
                            <div class="white-pnl-simple text_with_dots">
                                <xsl:value-of select="$settings//property[@name = 'tekst_stranicy_moi_skidki']/value" disable-output-escaping="yes" />
                            </div>
                        </xsl:if>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata" mode="discounts-user" />
    <xsl:template match="udata[discount]" mode="discounts-user">
        <div class="pf_module">
            <div class="pf_module_title">Персональная скидка <span class="pf_blue"><xsl:value-of select="discount/node()" />%</span></div>
            <xsl:if test="$settings//property[@name = 'config_discounts_personal_title']">
                <div class="pf_info_text">
                    <xsl:if test="$settings//property[@name = 'config_discounts_personal_content']">
                        <div class="pf_info_icon">
                            <div class="pf_tooltip"><xsl:value-of select="$settings//property[@name = 'config_discounts_personal_content']/value" disable-output-escaping="yes" /></div>
                        </div>
                    </xsl:if>
                    <xsl:value-of select="$settings//property[@name = 'config_discounts_personal_title']/value" disable-output-escaping="yes" />
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="udata" mode="discounts-coupon-active">
        <p>У вас нет активных промокодов</p>
    </xsl:template>

    <xsl:template match="udata[items/item]" mode="discounts-coupon-active">
        <xsl:apply-templates select="items/item" mode="discounts-coupon-active-item" />
    </xsl:template>

    <xsl:template match="item" mode="discounts-coupon-active-item">
        <div class="pf_promotitle">
            <div class="pf_promotitle_name"><xsl:value-of select="name" /></div>
            <div class="pf_promotitle_codename">Промокод: <span class="pf_green"><xsl:value-of select="@code" /></span></div>
            <div class="pf_promotitle_span">Осталось <span class="pf_green"><xsl:value-of select="days-left/@string" /></span></div>
        </div>

        <div class="pf_promocode_row">
            <div class="pf_promocode">
                <div class="pf_promocode_title">
                    На всю
                    <xsl:text disable-output-escaping="yes">&lt;br/&gt;</xsl:text>
                    продукцию
                </div>
                <div class="pf_promocode_box"><xsl:value-of select="discount/@percent" /><small>%</small></div>
            </div>
            <xsl:if test="focus">
                <div class="pf_promocode">
                    <div class="pf_promocode_title">
                        <xsl:choose>
                            <xsl:when test="focus/categories">
                                <small>На категорию</small>
                                <xsl:apply-templates select="focus/categories/category" mode="discounts-coupon-active-item-category" />
                            </xsl:when>
                            <xsl:otherwise>
                                <small>На избранные товары</small>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                    <div class="pf_promocode_box pf_red"><xsl:value-of select="focus/@percent" /><small>%</small></div>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="category" mode="discounts-coupon-active-item-category">
        <p>
            <xsl:choose>
                <xsl:when test="@link">
                    <a href="{@link}"><xsl:value-of select="node()" /></a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="node()" />
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>

    <xsl:template match="udata" mode="history-items" />
    <xsl:template match="udata[items/item]" mode="history-items">
        <xsl:param name="title" />

        <div class="pf_module">
            <div class="pf_module_title pf_blue"><xsl:value-of select="$title" /></div>

            <ul class="pf_promocodes_list">
                <xsl:apply-templates select="items/item" mode="history-item" />
            </ul><!-- /pf_promocodes_list -->

            <xsl:apply-templates select="document(concat('udata://system/numpages/', total, '/', per_page, '//', @numpages-param, '/'))/udata" mode="history-numpages" />
        </div>
    </xsl:template>

    <xsl:template match="item" mode="history-item">
        <xsl:variable name="dateru" select="document(concat('udata://data/dateru/',@date))/udata" />

        <li>
            <span class="pf_promocodes_date">
                <xsl:if test="@used != '1'"><xsl:text>Истек </xsl:text></xsl:if>
                <xsl:value-of select="concat($dateru/day,' ',$dateru/month,' ',$dateru/year)"/>
            </span>
            <span class="pf_promocodes_text">
                <xsl:value-of select="node()" />
                <xsl:if test="@percent"><xsl:text> </xsl:text><span class="pf_blue"><xsl:value-of select="@percent" />%</span></xsl:if>
            </span>
        </li>
    </xsl:template>

    <xsl:template match="udata" mode="history-numpages" />
    <xsl:template match="udata[items/item]" mode="history-numpages">
        <div class="pf_pagination">
            <xsl:apply-templates select="items/item" mode="history-numpages" />
        </div>
    </xsl:template>

    <xsl:template match="item" mode="history-numpages">
        <a href="{@link}"><xsl:value-of select="node() "/></a>
    </xsl:template>

    <xsl:template match="item[@is-active = '1']" mode="history-numpages">
        <span><xsl:value-of select="node() "/></span>
    </xsl:template>
</xsl:stylesheet>