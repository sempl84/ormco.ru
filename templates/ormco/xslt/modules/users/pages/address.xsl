<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result[@method = 'address']">
        <xsl:variable name="user" select="document(concat('uobject://', user/@id))/udata/object" />
        <xsl:variable name="dadataToken" select="document('udata://ormcoSiteData/getSiteDataDadataToken/')/udata" />

        <div class="my-orders-page1">
            <!--Breadcrumbs -->
            <div class="breadcrumbs-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="/">Главная</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/users/settings/">Личный кабинет</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <xsl:value-of select="@header" disable-output-escaping="yes" />
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--Header -->
            <div class="page-header">
                <div class="container-fluid">
                    <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
                    <h1>
                        <xsl:value-of select="@header" disable-output-escaping="yes" />
                    </h1>
                </div>
            </div>

            <!--Content -->
            <div class="container-fluid">
                <div class="row">
                    <aside class="col-lg-3 col-md-3 col-xs-12 right-pnl">
                        <xsl:apply-templates select="document('udata://menu/draw/left_menu_lk')/udata" mode="left_menu" />
                    </aside>
                    <div class="col-lg-9 col-md-9 col-xs-12 main-col main-col-mb white-pnl-simple">
                        <xsl:apply-templates select="$errors" />

                        <form enctype="multipart/form-data" method="post" action="/users/address/do/" id="address_form" class="form-horizontal" novalidate="novalidate">
                            <h4>Редактирование адреса</h4>
                            <xsl:if test="$user//property[@name = 'user_address_raw']/value">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Текущий адрес</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" disabled="" value="{$user//property[@name = 'user_address_raw']/value}" />
                                    </div>
                                </div>
                            </xsl:if>
                            <div class="form-group">
                                <label for="address_input" class="col-xs-3 control-label">Адрес*</label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="address_input" data-token="{$dadataToken/token}" placeholder="Адрес*" value="" name="address" data-val="true" data-val-required="true" aria-required="true" aria-invalid="false" aria-describedby="address-error" />
                                </div>
                            </div>
                            <textarea name="data" id="address_data" style="display:none;"></textarea>
                            <div class="form-group"><div class="col-xs-offset-3 col-xs-9"><button type="submit" class="btn btn-primary" data-gtm-event="ButtonClicks" data-gtm-event-value="Сохранить">Сохранить</button></div></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>