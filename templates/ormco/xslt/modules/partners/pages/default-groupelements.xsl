<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'partners']">
		<div class="dealers-page">
			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
			
			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="fa fa-briefcase" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    <xsl:call-template name="header_social_link" />
	            </div>
	        </div>
	        
	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <xsl:apply-templates select="document('udata://partners/listElements///1000/1?extProps=country,partner_web,diler,adres,partner_phone,partner_email')/udata" />
	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
	            </div>
	        </div>
			<!--Content -->
		</div>
	</xsl:template>
	
	<xsl:template match="udata[@module='partners' and @method='listElements']">
		<div class="white-pnl clearfix">
            Ничего не найдено
        </div>
	</xsl:template>
	<xsl:template match="udata[@module='partners' and @method='listElements' and items/item]">
		<div class="select-on-map-pnl">
            <div class="selectors">
                <select id="coutries" class="selectpicker show-tick js-coutries">
                    <option selected="selected">Выбор страны</option>                            
                </select>
                <select id="cities" class="selectpicker show-tick js-cities">
                    <option selected="selected">Выбор города</option>                      
                </select>
            </div>
            <div class="map js-map-canvas">                          
            </div>
        </div>
        <div class="dealers-table table-responsive js-dealers-table">
            <table class="table">
                <thead>
                    <tr>
                        <th>Страна</th>
                        <th>Дилер</th>
                        <th>Адрес</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:apply-templates select="items/item" />
                </tbody>
            </table>
        </div>
        <!--Google map-->
    	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpGO1gZOJsEJ9FdcwHdvwAgXgLQ2M5H2E"></script>
	</xsl:template>
	
	<xsl:template match="udata[@module='partners' and @method='listElements']/items/item" >	
		<!-- <tr>
            <td>Россия</td>
            <td><a href="#">Арион-Юг</a></td>
            <td>
                350042, Краснодар,
                Московская ул., 47
            </td>
            <td>+7 (918) 497-85-93</td>
            <td><a href="mailto:shoulga.oksana@yandex.ru">shoulga.oksana@yandex.ru</a></td>
        </tr> -->
		
		<tr>
            <td><xsl:value-of select=".//property[@name='country']/value/item/@name" /></td>
            <td>
            	<xsl:choose>
					<xsl:when test=".//property[@name='partner_web']/value">
						<a href="{.//property[@name='partner_web']/value}"><xsl:value-of select=".//property[@name='diler']/value" disable-output-escaping="yes"/></a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select=".//property[@name='diler']/value" disable-output-escaping="yes" />
					</xsl:otherwise>
				</xsl:choose>
			</td>
            <td><xsl:value-of select=".//property[@name='adres']/value" disable-output-escaping="yes" /></td>
            <td><xsl:value-of select=".//property[@name='partner_phone']/value" disable-output-escaping="yes" /></td>
            <td>
            	<xsl:if test=".//property[@name='partner_email']/value">
					<a href="mailto:{.//property[@name='partner_email']/value}"><xsl:value-of select=".//property[@name='partner_email']/value" disable-output-escaping="yes"/></a>
				</xsl:if>
			</td>
        </tr>
	</xsl:template>

</xsl:stylesheet>