<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'photoalbum']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid photos-page">
	    	<div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-picture-o" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                    <xsl:call-template name="header_social_link" />
	                </div>
	            </div>
	        </div>
	        <div class="row">

	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://catalog/search/&photoalbums_pid;')/udata" />

	                <xsl:apply-templates select="document(concat('udata://photoalbum/albums////',$document-page-id, '?extProps=descr,publish_time,photo'))/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='photoalbum' and @method='albums']">
		<div class="white-pnl clearfix">
            Ничего не найдено
        </div>
	</xsl:template>
	<xsl:template match="udata[@module='photoalbum' and @method='albums' and items/item]">

		<ul class="items white-pnl clearfix">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />

        <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	        <div class="pswp__bg"></div><div class="pswp__scroll-wrap">
	            <div class="pswp__container">
	                <div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item">
	                </div>
	            </div><div class="pswp__ui pswp__ui--hidden">
	                <div class="pswp__top-bar">
	                    <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div>
	                </div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
	                    <div class="pswp__share-tooltip">
	                        <!--<a href="#" class="pswp__share- -facebook"></a>-->
	                        <a href="#" class="pswp__share--twitter"></a><a href="#" class="pswp__share--pinterest"></a>
	                        <a href="#" download="" class="pswp__share--download"></a>
	                    </div>
	                </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
	                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
	                <div class="pswp__caption"><div class="pswp__caption__center"></div></div>
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='photoalbum' and @method='albums']/items/item" >
		<xsl:variable name="photos" select="document(concat('udata://photoalbum/album/',@id, '//1000/1/?extProps=photo,photo_alt'))/udata" />

		<xsl:if test="$photos/items/item">
			<li class="col-sm-4 col-xs-6 js-photos">
		        <xsl:apply-templates select="$photos/items/item" >
					<xsl:with-param name="photo" select=".//property[@name='photo']/value"/>
				</xsl:apply-templates>
		        <h4><a href="#"><xsl:value-of select="text()" /></a></h4>

		        <span class="date"><xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
		        <div class="preview" umi:element-id="{id}" umi:field-name="descr">
					<xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes" />
		        </div>
		    </li>
		</xsl:if>
	</xsl:template>

	<xsl:template match="udata[@module='photoalbum' and @method='album']/items/item">
		<a href="{.//property[@name='photo']/value}" class="js-photo hidden" data-size="{.//property[@name='photo']/value/@width}x{.//property[@name='photo']/value/@height}" title="{.//property[@name='photo_alt']/value}"></a>
	</xsl:template>

	<xsl:template match="udata[@module='photoalbum' and @method='album']/items/item[position()=1]">
		<xsl:param name="photo" />

		<a href="{.//property[@name='photo']/value}" data-size="{.//property[@name='photo']/value/@width}x{.//property[@name='photo']/value/@height}" title="{.//property[@name='photo_alt']/value}" class="js-photo">
            <xsl:choose>
                <xsl:when test="$photo">
                	<img src="{$photo}" class="img-responsive" alt="{.//property[@name='photo_alt']/value}" />
                </xsl:when>
                <xsl:otherwise>
                	<xsl:call-template name="makeThumbnailFull_ByPath">
						<xsl:with-param name="source" select=".//property[@name='photo']/value" />
						<xsl:with-param name="width" select="165" />
						<xsl:with-param name="height" select="105" />

						<xsl:with-param name="empty">&empty-photo-news;</xsl:with-param>
						<xsl:with-param name="element-id" select="@id" />
						<xsl:with-param name="field-name" select="'photo'" />

						<xsl:with-param name="alt" select=".//property[@name='photo_alt']/value" />
						<xsl:with-param name="class" select="'img-responsive'" />
					</xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>

        </a>
    </xsl:template>
</xsl:stylesheet>