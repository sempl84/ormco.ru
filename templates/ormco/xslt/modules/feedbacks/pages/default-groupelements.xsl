<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'feedbacks']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid feedbacks">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://feedbacks/listGroup/?extProps=event_type,event_name,feedback_for_event')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	                
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='feedbacks' and @method='listGroup']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='feedbacks' and @method='listGroup' and items/item]">
		<ul class="items js-items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
		<xsl:template match="udata[@module='feedbacks' and @method='listGroup']/items/item" >	
			<!-- <xsl:variable name="feedbacks_list" select="document('udata://feedbacks/listElements/?extProps=content,author_info')/udata" />
			<xsl:variable name="event" select="document(concat('upage://',.//property[@name='feedback_for_event']/value/page[1]/@id))" /> -->
			<xsl:apply-templates select="document(concat('udata://feedbacks/listElements/',@id,'///1/?extProps=content,author_info'))/udata" >
				<xsl:with-param name="event" select="document(concat('upage://',.//property[@name='feedback_for_event']/value/page[1]/@id))/udata" />
				<xsl:with-param name="event_type" select=".//property[@name='event_type']/value/item" />
				<xsl:with-param name="event_name" select=".//property[@name='event_name']/value" />
				<xsl:with-param name="lent_id" select="@id" />
			</xsl:apply-templates>
		</xsl:template>
	
	
	<xsl:template match="udata[@module='feedbacks' and @method='listElements']"></xsl:template>
	<xsl:template match="udata[@module='feedbacks' and @method='listElements' and items/item]">
		<xsl:param name="event" />
		<xsl:param name="event_type" />
		<xsl:param name="event_name" />
		<xsl:param name="lent_id" />
		
		<li class="item">
            <div class="white-pnl seminar-type">
                <xsl:choose>
                    <xsl:when test="$event//property[@name='event_type']/value/item/@id">
                    	<xsl:call-template name="feedback_event_class">	
                    		<xsl:with-param name="event_type_id" select="$event//property[@name='event_type']/value/item/@id" />
                    		<xsl:with-param name="old_class" select="'white-pnl'" />
						</xsl:call-template>
                    	<h4><xsl:value-of select="$event//property[@name='event_type']/value/item/@name" />: <a href="{$event/page/@link}"><xsl:value-of select="$event/page/name" /></a></h4>
                	</xsl:when>
                    <xsl:when test="$event_type/@id and $event_type/@name">
                    	<xsl:call-template name="feedback_event_class">	
                    		<xsl:with-param name="event_type_id" select="$event_type/@id" />
                    		<xsl:with-param name="old_class" select="'white-pnl'" />
						</xsl:call-template>
						<h4><xsl:value-of select="$event_type/@name" />: <xsl:value-of select="$event_name" /></h4>
                	</xsl:when>
                </xsl:choose>
                <div class="feedback content_wrap">
                	<xsl:value-of select="items/item[1]//property[@name='content']/value" disable-output-escaping="yes" />
                </div>
                <span class="author">
                	<!-- TODO -->
                	<!-- <i class="fa fa-vk" aria-hidden="true"></i>
                	<img src="{$template-resources}img/videos/author-1.png" alt="author" /> -->
                	<xsl:value-of select="items/item[1]//property[@name='author_info']/value" disable-output-escaping="yes" />
                </span>
                
                
                <xsl:apply-templates select="items[count(item) &gt; 1]" mode="more_feedback" >
                	<xsl:with-param name="lent_id" select="$lent_id" />
                </xsl:apply-templates>
 
            </div>
        </li>
            
	</xsl:template>
	
	
	<xsl:template match="items" mode="more_feedback">	
		<xsl:param name="lent_id" />
		
		<div class="collapse" id="showFeedbacks{$lent_id}" aria-expanded="false" role="document">        
            <ul class="other-feedbacks">
               <xsl:apply-templates select="item[position() &gt; 1]" mode="more_feedback" /> 
            </ul>
        </div>
        <button class="btn btn-primary btn-show-details collapsed" type="button" data-toggle="collapse" data-target="#showFeedbacks{$lent_id}" aria-expanded="false">
            <span>Показать все отзывы о мероприятии</span><span>Свернуть</span>
        </button>
	</xsl:template>
	
	<xsl:template match="item" mode="more_feedback">	
		<li>
            <div class="feedback content_wrap">
                <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" />
            </div>
            <span class="author">
            	<!-- TODO -->
            	<!-- 
            	<i class="fa fa-vk" aria-hidden="true"></i>
            	<img src="{$template-resources}img/videos/author-1.png" alt="author" />-->
            	<xsl:value-of select=".//property[@name='author_info']/value" disable-output-escaping="yes" />
            </span>
        </li>
	</xsl:template>

</xsl:stylesheet>