<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">

	<xsl:template match="udata[@module = 'webforms'][@method = 'add']">
		<form class="show-placeholder" method="post" action="{$lang-prefix}/webforms/pre_send/" onsubmit="site.forms.data.save(this); return site.forms.check(this);" enctype="multipart/form-data">
			<xsl:apply-templates select="items" mode="address" />
			<xsl:apply-templates select="groups/group" mode="webforms" />
			<input type="text" name="data[new][phone_call]" value="" />
			<input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
			<input type="hidden" name="ref_onsuccess" value="{$lang-prefix}/webforms/posted/{/udata/@form_id}/" />
			<div class="form_element">
				<xsl:apply-templates select="document('udata://system/captcha/')/udata" />
			</div>
			<div class="form_element">
				<input type="submit" class="button" value="Отправить" />
			</div>
		</form>
	</xsl:template>

	<xsl:template match="group" mode="webforms">
		<xsl:apply-templates select="field" mode="webforms" />
	</xsl:template>

	<xsl:template match="field" mode="webforms">
		<div class="form-group">
			<xsl:apply-templates select="." mode="webforms_input_type" />
        </div>
	</xsl:template>

	<xsl:template match="field" mode="webforms_input_type">
		<xsl:param name="page" select="''"/>
		<xsl:variable name="cur_name" select="@name" />

		<input type="text" class="form-control" id="{@name}{$page}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="$cur_name = 'telefon'">
				<xsl:attribute name="class">form-control js-phone</xsl:attribute>
				<xsl:attribute name="data-val-length">true</xsl:attribute>
				<xsl:attribute name="data-val-length-min">10</xsl:attribute>
				<xsl:attribute name="data-mask">9999999999999999</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'email' or @name='e-mail'">
				<xsl:attribute name="data-val-email">true</xsl:attribute>
			</xsl:if>
			<!-- подстановка значений для формы обратной связи -->
			<xsl:if test="not($user-type  = 'guest') and $user-info//property[@name=$cur_name]/value">
        		<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name=$cur_name]/value" /></xsl:attribute>
        		<!-- <xsl:attribute name="disabled">disabled</xsl:attribute> -->
        	</xsl:if>
			<xsl:if test="not($user-type  = 'guest') and /udata/@form_id = &feedback_form_oid;">
        		<xsl:choose>
					<xsl:when test="$cur_name = 'first_name' and $user-info//property[@name='fname']/value">
						<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='fname']/value" /></xsl:attribute>
        			</xsl:when>
					<xsl:when test="$cur_name = 'last_name' and $user-info//property[@name='lname']/value">
						<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='lname']/value" /></xsl:attribute>
        			</xsl:when>
					<xsl:when test="$cur_name = 'email' and $user-info//property[@name='e-mail']/value">
						<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='e-mail']/value" /></xsl:attribute>
        			</xsl:when>
					<xsl:when test="$cur_name = 'fio'">
						<xsl:attribute name="value"><xsl:value-of select="concat($user-info//property[@name='lname']/value, ' ', $user-info//property[@name='fname']/value, ' ', $user-info//property[@name='father_name']/value)" /></xsl:attribute>
					</xsl:when>
					<xsl:when test="$cur_name = 'telefon'">
						<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='phone']/value" /></xsl:attribute>
					</xsl:when>
					<xsl:when test="$cur_name = 'gorod'">
						<xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='city']/value" /></xsl:attribute>
					</xsl:when>
				</xsl:choose>
        	</xsl:if>
		</input>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="webforms_input_type">
		<xsl:param name="page" select="''"/>

		<textarea class="form-control" id="{@name}{$page}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</textarea>
	</xsl:template>

	<!--
	<xsl:template match="field[@type = 'password']" mode="webforms_input_type">
		<input type="password" name="{@input_name}" value="" class="textinputs"/>
	</xsl:template> -->

	<xsl:template match="field[@type = 'boolean']" mode="webforms_input_type">
		<xsl:param name="page" select="''"/>

		<xsl:attribute name="class">form-group agree</xsl:attribute>
		<input type="hidden" id="{@name}{$page}{/udata/@form_id}" name="{@input_name}" value="false" />
        <input type="checkbox" class="form-control checkbox" id="{@name}{$page}{/udata/@form_id}_fake" name="{@name}{/udata/@form_id}_fake" value="1" onclick="javascript:document.getElementById('{@name}{$page}{/udata/@form_id}').value = this.checked;">
        	<xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
				<xsl:attribute name="data-val">true</xsl:attribute>
			</xsl:if>
        </input>
        <span  data-t="webf_add">
        	<xsl:choose>
				<xsl:when test="@name='allow_personal_data'">&agree_text_contacts;<xsl:apply-templates select="." mode="simple_form_order_ast" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" /></xsl:otherwise>
			</xsl:choose>
		</span>
        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
	</xsl:template>


	<xsl:template match="field[@type = 'relation']" mode="webforms_input_type">
		<xsl:param name="page" select="''"/>

		<select id="{@name}{$page}{/udata/@form_id}" name="{@input_name}" class="form-control" data-val="true">
            <xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
            <xsl:if test="@multiple">
				<xsl:attribute name="multiple">
					<xsl:text>multiple</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<option value="" selected="selected">
				<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
			</option>
			<xsl:apply-templates select="values/item" mode="webforms_input_type" />
        </select>
	</xsl:template>

	<!-- <xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="webforms_input_type">
		<xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb
		<input type="file" name="{@input_name}" class="textinputs"/>
	</xsl:template> -->

	<xsl:template match="item" mode="webforms_input_type">
		<option value="{@id}">
			<xsl:copy-of select="@selected" />
			<xsl:if test="../../@name = 'tema'">
        		<xsl:attribute name="data-tema-id"><xsl:value-of select="document(concat('uobject://', @id, '.identifikator_adresa'))//value" /></xsl:attribute>
			</xsl:if>

			<xsl:apply-templates />
		</option>
	</xsl:template>

	<xsl:template match="field" mode="webforms_required" />
	<xsl:template match="field[@required = 'required']" mode="webforms_required">
		<xsl:attribute name="class">
			<xsl:text>required</xsl:text>
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="field" mode="webforms_required_ast" />
	<xsl:template match="field[@required = 'required']" mode="webforms_required_ast">
		<xsl:text>*</xsl:text>
	</xsl:template>

	<xsl:template match="items" mode="address">
		<xsl:apply-templates select="item" mode="address" />
	</xsl:template>

	<xsl:template match="item" mode="address">
		<input type="hidden" name="system_email_to" class="system_email_to" value="{@id}" />
	</xsl:template>

	<xsl:template match="items[count(item) &gt; 1]" mode="address">
		<xsl:choose>
			<xsl:when test="count(item[@selected='selected']) != 1">
				<div class="form_element">
					<label class="required">
						<span><xsl:text>Кому отправить:</xsl:text></span>
						<select name="system_email_to">
							<option value=""></option>
							<xsl:apply-templates select="item" mode="address_select" />
						</select>
					</label>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="item[@selected='selected']" mode="address" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="item" mode="address_select">
		<option value="{@id}"><xsl:apply-templates /></option>
	</xsl:template>
</xsl:stylesheet>