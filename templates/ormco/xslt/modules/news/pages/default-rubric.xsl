<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'news']">
		<div class="news-page">
			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
			
			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="fa fa-calendar-o" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    <xsl:call-template name="header_social_link" />
	            </div>
	        </div>
	        
	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <xsl:apply-templates select="document('udata://news/lastlist/?extProps=publish_time,anons_pic,anons')/udata" />
	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
	            </div>
	        </div>
			<!--Content -->
		</div>

	</xsl:template>
	
	<xsl:template match="udata[@module='news' and @method='lastlist']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='news' and @method='lastlist' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="udata[@module='news' and @method='lastlist']/items/item" >	
		<xsl:variable name="dateru" select="document(concat('udata://data/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata" />
                    
		<li class="white-pnl item">
            <div class="row">
                <div class="col-xs-3 left">
                	<a href="{@link}">
                    	<xsl:choose>
							<xsl:when test=".//property[@name='anons_pic']/value">
								<img class="img-responsive" src="{.//property[@name='anons_pic']/value}" alt="{text()}" />
								
								<!-- <xsl:call-template name="makeThumbnailFull_ByPath">
									<xsl:with-param name="source" select=".//property[@name='anons_pic']/value" />
									<xsl:with-param name="width" select="174" />
									<xsl:with-param name="height" select="auto" />
									
									<xsl:with-param name="empty">&empty-photo-news;</xsl:with-param>
									<xsl:with-param name="element-id" select="@id" />
									<xsl:with-param name="field-name" select="'anons_pic'" />
									
									<xsl:with-param name="alt" select="text()" />
									<xsl:with-param name="class" select="'img-responsive'" />
								</xsl:call-template> -->
							</xsl:when>
							<xsl:otherwise>
								<div class="default-photo">
                                    <img class="img-responsive" src="&empty-photo-news;" alt="{text()}" />
                                </div>
							</xsl:otherwise>
						</xsl:choose>
                    </a>
                </div>
                <div class="col-xs-9 right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a></h4>
                    <span class="date">
                    	<!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->
                    	<xsl:value-of select="concat($dateru/day,' ',$dateru/month,' ',$dateru/year)"/>
                    </span>
                    <div class="preview content_wrap">
						<xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
					</div>
                </div>
            </div>
        </li>
	</xsl:template>

</xsl:stylesheet>