<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'news' and @method='item']">
		<xsl:variable name="dateru" select="document(concat('udata://data/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata" />

        <div class="article-page">
			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">

	                <i class="fa fa-calendar-o" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    <xsl:call-template name="header_social_link" />
	            </div>
	        </div>

	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <article class="white-pnl">

							<xsl:choose>
								<xsl:when test="//property[@name = 'galereya_1']">
									<xsl:apply-templates select="//property[@name = 'galereya_1']" mode="galereya_1" />
								</xsl:when>
								<xsl:otherwise>
									<span class="date">
										<xsl:value-of select="concat($dateru/day,' ',$dateru/month,' ',$dateru/year)"/>
									</span>
								</xsl:otherwise>
							</xsl:choose>
							<div class="content_wrap">
								<xsl:value-of select="document(concat('udata://custom/getPageContentWithAdaptiveTables/', $document-page-id, '/content/'))/udata" disable-output-escaping="yes" />
							</div>


	                    </article>
	                    <xsl:if test=".//property[@name='video']/value">
	                    	<div class="white-pnl">
		                        <div class="embed-responsive embed-responsive-16by9">
		                        	<xsl:value-of select=".//property[@name='video']/value" disable-output-escaping="yes" />
		                            <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/StvD7Muph8Q" allowfullscreen></iframe> -->
		                        </div>
		                    </div>
	                    </xsl:if>

	                    <!-- слайдер с текущей страницы  -->
	                    <xsl:apply-templates select=".//property[@name='photos' and value]" mode="photos" />


	                    <a class="show-more-lnk" href="../">
	                    	<xsl:choose>
								<xsl:when test="$parents/page/@id = &articles_pid;">Показать все статьи</xsl:when>
								<xsl:when test="$parents/page/@id = &discounts_pid;">Показать все акции</xsl:when>
								<xsl:otherwise>Показать все новости</xsl:otherwise>
							</xsl:choose>
	                    </a>
	                    <div class="social-share text-right">
	                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
	                        <script src="//yastatic.net/share2/share.js"></script>
	                        <div class="ya-share2" data-services="odnoklassniki,gplus,vkontakte,twitter" data-counter=""></div>
	                    </div>

	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
	            </div>
	        </div>
			<!--Content -->
		</div>


	</xsl:template>


	<!-- шаблон для вывода слайдер с текущей страницы -->
	<xsl:template match="property" mode="photos" />
	<xsl:template match="property[value]" mode="photos">
		<div class="white-pnl gallery">
            <ul class="row js-photos">
                <xsl:apply-templates select="value" mode="photos" />
            </ul>
        </div>
	</xsl:template>

		<xsl:template match="value" mode="photos">
			<li class="col-sm-3 col-xs-4">
                <a href="{text()}" data-size="{@width}x{@height}" title="{@alt}" class="js-photo">
                	<img  src="{document(concat('udata://system/makeThumbnail/(.', text(), ')/174/auto/'))/udata/src}" alt="{@alt}" />
                    <!-- <xsl:call-template name="makeThumbnailFull_ByPath">
						<xsl:with-param name="source" select="text()" />
						<xsl:with-param name="width" select="174" />
						<xsl:with-param name="height" select="108" />
						<xsl:with-param name="alt" select="@alt" />
					</xsl:call-template> -->
                </a>
            </li>
		</xsl:template>

	<!-- Верхняя галерея фото -->
	<xsl:template match="property" mode="galereya_1">
		<xsl:if test="//property[@name='zagolovok_galerei_1']/value">
			<div class="h2">
				<xsl:value-of select="//property[@name='zagolovok_galerei_1']/value" disable-output-escaping="yes" />
			</div>
		</xsl:if>

		<ul class="cert_gal">
			<xsl:apply-templates select="value " mode="galereya_1" />
		</ul>
	</xsl:template>

	<xsl:template match="value" mode="galereya_1">
		<xsl:variable name="pos" select="position()" />

		<li>
			<div class="name">
				<a data-fancybox="object" href="{text()}" >
					<xsl:if test="@alt">
						<xsl:attribute name="data-caption" ><xsl:value-of select="@alt" /></xsl:attribute>
					</xsl:if>
					<img class="img-responsive" src="{text()}">
						<xsl:if test="//property[@name='prevyu_galerei_1']/value[position() = $pos]">
							<xsl:attribute name="src" ><xsl:value-of select="//property[@name='prevyu_galerei_1']/value[position() = $pos]" /></xsl:attribute>
						</xsl:if>

					</img>
				</a>
			</div>
		</li>
	</xsl:template>

	<xsl:template match="value" mode="galereya_1_modal">
		<div class="modal modal-image" id="modal-doc-{@id}">
			<div class="img">
				<div><img src="{text()}" /></div>
			</div>
			<!-- end .img-->
			<xsl:if test="@alt">
				<div class="name"><xsl:value-of select="@alt" disable-output-escaping="yes" /></div>
			</xsl:if>
			<xsl:if test="@title">
				<div><xsl:value-of select="@title" disable-output-escaping="yes" /></div>
			</xsl:if>
		</div>
	</xsl:template>



</xsl:stylesheet>