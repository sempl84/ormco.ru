<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- новости на главной -->
	<xsl:template match="udata[@module='news' and @method='lastlist']" mode="short_news_block" />	
	<xsl:template match="udata[@module='news' and @method='lastlist' and items/item]" mode="short_news_block">	
		<div class="news content-block">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-xs-12"><div class="header2">Новости</div></div>
	                <ul>
	                    <xsl:apply-templates select="items/item" mode="short_news_block"/>
	                </ul>
	                <div class="col-xs-12 text-center">
	                    <a class="btn btn-primary" href="{document('upage://&news_pid;')/udata/page/@link}">Все новости</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="item" mode="short_news_block">
		<li class="col-sm-3">
            <a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a>
            <span><xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
            <div class="preview">
				<xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
			</div>
        </li>
	</xsl:template>
	
	<!-- новости на главной ver 2 -->
	<xsl:template match="udata[@module='news' and @method='lastlist']" mode="short_news_block2" />	
	<xsl:template match="udata[@module='news' and @method='lastlist' and items/item]" mode="short_news_block2">
		<xsl:param name="title" />

        <ul>
        	<xsl:apply-templates select="items/item" mode="short_news_block2"/>
        </ul>

        <a class="lnk-go-to mb0" href="{archive_link}" data-gtm-event="ButtonClicks" data-gtm-event-value="{$title}"><xsl:value-of select="$title" /><i class="fa fa-angle-right" aria-hidden="true"></i></a>
	</xsl:template>
	
	<xsl:template match="item" mode="short_news_block2">
		<xsl:variable name="dateru" select="document(concat('udata://data/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata" />
        
        <li>
            <span class="date"><xsl:value-of select="concat($dateru/day,' ',$dateru/month,' ',$dateru/year)"/></span>
            <a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a>
            <xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
        </li>
	</xsl:template>

</xsl:stylesheet>