<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl umi">

    <!-- Header menu -->
    <xsl:template match="udata[@module = 'menu']" mode="top_menu">
        <ul class="nav navbar-nav top js-top-menu">
            <li class="dropdown corporation js-corporation">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Корпорация Ormco<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <xsl:apply-templates select="item" mode="top_menu" />
                    <li class="kerr">
                        <a href="http://www.kerrdental.ru/">
                            <img src="{$template-resources}img/kerr.png" alt="Kerr" />
                        </a>
                    </li>
                    <li class="kavo">
                        <a href="http://www.kavodental.ru/">
                            <img src="{$template-resources}img/kavo.png" alt="Kavo" />
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']/item" mode="top_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:if test="contains(node(),'http') or contains(node(),'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <!-- main menu -->
    <xsl:template match="udata[@module = 'menu']" mode="main_menu">
        <ul class="nav navbar-nav">
            <xsl:apply-templates select="item" mode="main_menu" />

            <li class="visible-xs">
                <a href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal" class="email">Бесплатная консультация</a>
            </li>
            <xsl:if test="$settings//property[@name='catalog_file']/value">
                <li class="visible-xs">
                    <a href="{$settings//property[@name='catalog_file']/value}" target="_blank" class="document" onclick="ga('send', 'event', 'click', 'cataloguedownload');ym(10184890, 'reachGoal', 'CatalogueDownload');">Скачать каталог</a>
                </li>
            </xsl:if>

            <xsl:if test="$settings//property[@name='pricelist_file']/value">
                <li class="visible-xs">
                    <a href="{$settings//property[@name='pricelist_file']/value}" target="_blank" class="download" onclick="ga('send', 'event', 'click', 'pricedownload');ym(10184890, 'reachGoal', 'PriceDownload');">Скачать прайс-лист</a>
                </li>
            </xsl:if>
            <li>
                <a href="#internationalModal" data-toggle="modal" data-target="#internationalModal">
                    <i class="glyphicon glyphicon-globe"></i>
                </a>
            </li>

            <!-- <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Компания<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="about-company.html">О компании</a></li>
                    <li><a href="payment-and-delivery.html">Оплата и доставка</a></li>
                    <li><a href="dealers.html">Дилеры</a></li>
                    <li><a href="news.html">Новости</a></li>
                </ul>
            </li>
            <li><a href="special-offers.html">Акции</a></li>
            <li><a href="contacts.html">Контакты</a></li>
            <li class="instruct-center"><a href="instruct-center.html">Обучающий центр</a></li>
            <li><a href="research-and-articles.html">Исследования и статьи</a></li> -->
            <li class="search-pnl">
                <form class="show-placeholder" action="/search/search_do/" method="get">
                    <input type="text" placeholder="Поиск по сайту" name="search_string" />
                    <span></span>
                    <input type="hidden" name="search_branches" value="&search_branches_pids;"/>
                    <!-- <input type="text" placeholder="Поиск по сайту" name="search_string"/><span></span> -->
                </form>
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//item" mode="main_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}" data-gtm-event="ButtonClicks">
                <xsl:if test="contains(@link,'http') or contains(@link,'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//item[contains(@link,'orthodontia.ru')]" mode="main_menu">
        <li class="instruct-center">
            <a href="{@link}" targe="_blank" data-gtm-event="ButtonClicks">
                <xsl:if test="contains(@link,'http') or contains(@link,'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//item[items/item]" mode="main_menu">
        <li class="dropdown">
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or items/item/@id = $document-page-id or items/item/@id = $parents/page/@id">
                <xsl:attribute name="class">dropdown active</xsl:attribute>
            </xsl:if>
            <a href="{@link}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-gtm-event="ButtonClicks">
                <xsl:if test="contains(@link,'http') or contains(@link,'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
                <span class="caret"></span>
            </a>
            <xsl:apply-templates select="items[item]" mode="main_menu" />
        </li>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//items" mode="main_menu">
        <ul class="dropdown-menu">
            <xsl:apply-templates select="item" mode="main_menu" />
        </ul>
    </xsl:template>

    <!-- right_menu -->
    <xsl:template match="udata[@module = 'menu']" mode="right_menu">
        <nav class="right-nav">
            <ul>
                <xsl:apply-templates select="item" mode="right_menu" />
            </ul>
        </nav>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']/item" mode="right_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or (concat('/',$module,'/',$method,'/') = @link)">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:if test="contains(@link,'http') or contains(@link,'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <!-- left_menu -->
    <xsl:template match="udata[@module = 'menu']" mode="left_menu">
        <nav class="left-nav">
            <ul>
                <xsl:apply-templates select="item" mode="left_menu" />
            </ul>
        </nav>

    </xsl:template>

    <xsl:template match="udata[@module = 'menu']/item" mode="left_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or (concat('/',$module,'/',$method,'/') = @link)">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:if test="contains(@link,'http') or contains(@link,'www')">
                    <xsl:attribute name="rel">nofollow</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <!-- bottom_menu -->
    <xsl:template match="udata[@module = 'menu']" mode="bottom_menu">
        <ul>
            <xsl:apply-templates select="item" mode="right_menu" />
        </ul>
    </xsl:template>
</xsl:stylesheet>