<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:php="http://php.net/xsl"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink php">

	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert']" mode="right" />

	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert'][banner]" mode="right">
		<div class="im hidden-xs">
			<xsl:apply-templates select="banner" />
		</div>
	</xsl:template>
	
	

	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert']" />
	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert'][banner]">
		<xsl:apply-templates select="banner" />
	</xsl:template>

	<xsl:template match="banner" />

	
	<xsl:template match="banner[@type = 'image']">
		<xsl:choose>
			<xsl:when test="href">
				<a href="/banners/go_to/{/udata/@id}/">
					<xsl:if test="@target">
						<xsl:attribute name="target">
							<xsl:value-of select="@target" />
						</xsl:attribute>
					</xsl:if>

					<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommercePromoView/(banner_', /udata/@id, ')/(', php:function('urlencode', string(alt)), ')/banner_', position(), '/banner_', position(), '/1/'))/udata" />

					<img src="{source}" alt="{alt}" />
				</a>
			</xsl:when>
			<xsl:otherwise>
				<img src="{source}"  alt="{alt}"  />
				<!-- width="{@width}" height="{@height}" border="0" -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="banner[@type = 'swf']">
		<object	width="{@width}" height="{@height}" align="middle" 
				codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" 
				classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="movie" value="{source}" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<param name="wmode" value="opaque" />
			<xsl:if test="href">
				<param name="flashVars">
					<xsl:attribute name="value">
						<xsl:text>url=/banners/go_to/</xsl:text><xsl:value-of select="/udata/@id" /><xsl:text>/</xsl:text>
						<xsl:if test="@target">
							<xsl:text>&amp;target=</xsl:text><xsl:value-of select="@target" />
						</xsl:if>
					</xsl:attribute>
				</param>
			</xsl:if>
			<embed	width="{@width}" height="{@height}" align="middle" wmode="opaque" 
					pluginspage="http://www.macromedia.com/go/getflashplayer" 
					type="application/x-shockwave-flash" allowscriptaccess="sameDomain" 
					name="banner" bgcolor="#ffffff" quality="high" src="{source}">
				<xsl:if test="href">
					<xsl:attribute name="flashVars">
						<xsl:text>url=/banners/go_to/</xsl:text><xsl:value-of select="/udata/@id" /><xsl:text>/</xsl:text>
						<xsl:if test="@target">
							<xsl:text>&amp;target=</xsl:text><xsl:value-of select="@target" />
						</xsl:if>
					</xsl:attribute>
				</xsl:if>
			</embed>
		</object>

		<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommercePromoView/(banner_', /udata/@id, ')/(', php:function('urlencode', string(alt)), ')/banner_', position(), '/banner_', position(), '/'))/udata" />
	</xsl:template>

	<xsl:template match="banner[@type = 'html']">
		<xsl:value-of select="document(concat('uobject://',../@id,'.htmlkod'))//value" disable-output-escaping="yes" />
		<xsl:value-of select="source" disable-output-escaping="yes" />

		<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommercePromoView/(banner_', /udata/@id, ')/(', php:function('urlencode', string(alt)), ')/banner_', position(), '/banner_', position(), '/'))/udata" />
	</xsl:template>
	
	<!-- on_main_right_of_slider -->
	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert']" mode="on_main_right_of_slider" />
	<xsl:template match="udata[@module = 'banners'][@method = 'fastInsert'][banner]" mode="on_main_right_of_slider">
		<xsl:param name="position" />

		<xsl:choose>
			<xsl:when test="banner/@type = 'image'">
				<xsl:apply-templates select="banner" mode="on_main_right_of_slider">
					<xsl:with-param name="position" select="$position" />
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="banner" />
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template match="banner[@type = 'image']" mode="on_main_right_of_slider">
		<xsl:param name="position" />

		<a href="/banners/go_to/{/udata/@id}/" class="white-pnl-border">
			<xsl:if test="@target">
				<xsl:attribute name="target">
					<xsl:value-of select="@target" />
				</xsl:attribute>
			</xsl:if>

			<xsl:apply-templates select="document(concat('udata://exchange/addExchangeEcommercePromoView/(banner_index_', /udata/@id, ')/(', php:function('urlencode', string(alt)), ')/banner_index_', $position, '/banner_index_', $position, '/1/'))/udata" />

			<img src="{source}" alt="{alt}" />
			<!-- width="{@width}" height="{@height}"  border="0" -->
			<span><xsl:value-of select="alt" /></span>
		</a>
	</xsl:template>

</xsl:stylesheet>