<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'doclocator' and @method='item_element']">
		<xsl:value-of select="document(concat('udata://content/redirect/(',$parents/page[position() = last()]/@link,')'))/udata" />
	</xsl:template>
</xsl:stylesheet>