<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'doclocator']">
		<xsl:variable name="clinics" select="document(concat('udata://doclocator/listElements/', $document-page-id, '//1000/1/?extProps=adres_clinic,phone_clinic,url_clinic,lat,lng,tip_kliniki'))/udata" />
		<xsl:variable name="clinic_types" select="document('udata://doclocator/get_all_types')" />

		<div class="search-clinic-page">
	        <!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

			<div class="page-header">
	            <div class="container-fluid">
	            	<div class="row">
	                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                        <i class="fa fa-plus-circle" aria-hidden="true">
	                        	<xsl:if test=".//property[@name='icon_style']/value">
									<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
								</xsl:if>
	                        </i>
	                        <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                        <div class="info-lnk js-privacy-lnk" data-placement="top" data-original-title="Прочитайте подробнее политику конфиденциальности">
	                            <button class="btn btn-blue privacy-policy-modal-btn" data-toggle="modal" data-target="#privacyPolicyModal">Отказ от ответственности <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
	                        </div>
	                        <xsl:if test=".//property[@name = 'content']/value">
		                        <article class="white-pnl content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
									<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
			                    </article>
		                    </xsl:if>
	                    </div>

	                    <div class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                         <xsl:call-template name="header_social_link" />
	                    </div>
	                </div>
		        </div>
	        </div>

	        <div class="container-fluid">
	        	<div class="row">
		            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
		                <div class="select-on-map-pnl">
		                    <div class="selectors">
		                        <select id="coutries" class="selectpicker show-tick js--clinics-regions col-sm-5">
		                            <option value="none" selected="selected">Выбрать регион</option>
		                        </select>
		                        <select id="cities" class="selectpicker show-tick js--clinics-cities col-sm-5">
		                            <option value="none" selected="selected">Выбрать город</option>
		                        </select>
								<div class="clinics-helper js-privacy-lnk" data-placement="top" data-original-title="Выберите регион и город чтобы найти подходящую клинику">
									<i class="fa fa-question" aria-hidden="true"></i>
								</div>
							</div>
		                    <div class="row clinic_types_area">
								<xsl:apply-templates select="$clinic_types//item" mode="clinic_type" />
		                    </div>
		                    <div class="map js--clinics-map"></div>

		                    <div class="clinics-view-data js--clinics-view-data"></div>
		                    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
		                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpGO1gZOJsEJ9FdcwHdvwAgXgLQ2M5H2E"></script>

		                    <script>
		                      var regions = [
		                      	<xsl:apply-templates select="$clinics/regions/item" mode="regions" />
		                      ];
		                      var cities = [
		                      	<xsl:apply-templates select="$clinics/cities/item" mode="cities" />
		                      ];
		                      var clinics = [
		                      	<xsl:apply-templates select="$clinics/items/item" mode="clinics" />
		                      ];
		                    </script>
		                </div>
		            </div>

					<aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
		        </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="item" mode="clinic_type">
		<label class="col-sm-5 js--select-type"><input type="checkbox" class="one_clinic_type" name="clinic_type_{@id}" data-id="{@id}" checked="checked" /><xsl:value-of select="@title" /><div class="one_clinic_type_hint js-privacy-lnk" data-placement="top" data-original-title="{@description}"><i class="fa fa-question"></i></div></label>
	</xsl:template>

	<!-- regions -->
	<xsl:template match="item" mode="regions">{ id: <xsl:value-of select="@id" />, title: '<xsl:value-of select="text()" />' },</xsl:template>

	<!-- cities -->
	<xsl:template match="item" mode="cities">{ id: <xsl:value-of select="@id" />,region_id: <xsl:value-of select="@region_id" />,  title: '<xsl:value-of select="text()" />' },</xsl:template>

	<!-- clinics -->
	<xsl:template match="item" mode="clinics">
		<xsl:variable name="type" select="document(concat('uobject://', .//property[@name = 'tip_kliniki']/value/item/@id))" />
		<xsl:variable name="icon">
			<xsl:choose>
				<xsl:when test="$type//property[@name = 'ikonka_tipa']/value"><xsl:value-of select="$type//property[@name = 'ikonka_tipa']/value" /></xsl:when>
				<xsl:otherwise>https://ormco.ru/templates/ormco/img/marker-icon.png</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>{</xsl:text> id: <xsl:value-of select="@id" />, city_id: <xsl:value-of select="@city_id" />, title: '<xsl:value-of select="text()" />', address: '<xsl:value-of select=".//property[@name='adres_clinic']/value" />', phone: '<xsl:value-of select=".//property[@name='phone_clinic']/value" />', url: '<xsl:value-of select=".//property[@name='url_clinic']/value" />', lat: <xsl:value-of select=".//property[@name='lat']/value" />, lng: <xsl:value-of select=".//property[@name='lng']/value" />, type_id: '<xsl:value-of select=".//property[@name = 'tip_kliniki']/value/item/@id" />', type: '<xsl:value-of select="$type//property[@name = 'podpis_na_bable_karty']/value" />', type_icon: '<xsl:value-of select="$icon" />', type_hint: '<xsl:value-of select="$type//property[@name = 'podskazka_dlya_tipa']/value" />'},
	</xsl:template>
</xsl:stylesheet>