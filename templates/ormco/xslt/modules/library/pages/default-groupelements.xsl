<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'library']">
		<div class="news-page">
			<!--Breadcrumbs -->
			<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
			
			<!--Header -->
	        <div class="page-header">
	            <div class="container-fluid">
	                <i class="fa fa-calendar-o" aria-hidden="true">
	                	<xsl:if test=".//property[@name='icon_style']/value">
							<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
						</xsl:if>
                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    <xsl:call-template name="header_social_link" />
	            </div>
	        </div>
	        
	        <!--Content -->
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-col">
	                    <xsl:apply-templates select="document('udata://library/listElements/?extProps=file_item')/udata" />
	                </div>
	                <aside class="col-lg-3 col-md-3 right-pnl hidden-sm hidden-xs">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	               		<xsl:apply-templates select="." mode="right_col" />
	                </aside>
	            </div>
	        </div>
			<!--Content -->
		</div>
		
	</xsl:template>
	
	<xsl:template match="udata[@module='library' and @method='listElements']">
		<div class="white-pnl clearfix">
            Ничего не найдено
        </div>
	</xsl:template>
	<xsl:template match="udata[@module='library' and @method='listElements' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="udata[@module='library' and @method='listElements']/items/item" >	
		<li class="white-pnl item">
            <div class="row library_item">
                <div class="col-xs-1 left ">
                	<a href="{.//property[@name='file_item']/value}" target="_blank" class="js-count-download" data-count_url="{@id}/file_item">
                    	<i class="fa fa-file-text-o" aria-hidden="true">
                    		<xsl:choose>
								<xsl:when test=".//property[@name='file_item']/value/@ext = 'pdf'">
									<xsl:attribute name="class">fa fa-file-pdf-o</xsl:attribute>
								</xsl:when>
								<xsl:when test=".//property[@name='file_item']/value/@ext = 'doc' or .//property[@name='file_item']/value/@ext = 'docx'">
									<xsl:attribute name="class">fa fa-file-word-o</xsl:attribute>
								</xsl:when>
							</xsl:choose>
                    	</i>
                    </a>
                </div>
                <div class="col-xs-11 right">
                    <h4>
                    	
                    	<a href="{.//property[@name='file_item']/value}" target="_blank" class="js-count-download" data-count_url="{@id}/file_item">
                    		<xsl:value-of select="text()" disable-output-escaping="yes" />
                    	</a>
                    </h4>
                    
                    <!-- <div class="preview">
						<xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
					</div> -->
                </div>
            </div>
        </li>
	</xsl:template>

</xsl:stylesheet>