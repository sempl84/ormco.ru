<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result" mode="scripts">
        <script>
            var lastJQ = jQuery.noConflict();
        </script>

        <xsl:if test="@module = 'content' and page/@alt-name = 'contact'">
            <!--Google map-->
            <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDpGO1gZOJsEJ9FdcwHdvwAgXgLQ2M5H2E"></script>
        </xsl:if>

        <script src="/min/index.php?f={$template-resources}js/combine.js,{$template-resources}js/fancybox/jquery.fancybox.min.js,{$template-resources}js/jquery.blueimp-gallery.min.js,{$template-resources}js/phone-validator.js,{$template-resources}js/search-clinic.js,/templates/ormco/js/easy_autocomplete/jquery.easy-autocomplete.min.js,{$template-resources}js/custom.js,{$template-resources}js/datepicker.min.js,{$template-resources}js/order1c.js,{$template-resources}js/jsrender.min.js" type="text/javascript"></script>

        <!-- маркетинговые материалы -->
        <xsl:if test="@module = 'materials'">
            <script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
        </xsl:if>

        <xsl:if test="$user-type = 'sv'">
            <xsl:value-of select="document('udata://system/includeQuickEditJs')/udata" disable-output-escaping="yes" />
        </xsl:if>

        <!--<script src="{$template-resources}js/sys/i18n.{@lang}.js?v={$build_version}"></script>-->
        <!--<script src="{$template-resources}js/sys/__common.js"></script>-->

        <script src="/min/index.php?f={$template-resources}js/sys/i18n.{@lang}.js,{$template-resources}js/sys/__common.js,/templates/ormco/js/sys/underscore-min.js,/js/client/basket.js,/templates/ormco/js/sys/basket.js,/templates/ormco/js/sys/forms.js,/templates/ormco/js/sys/message.js,/templates/ormco/js/sys/captcha.js,/templates/ormco/js/gtm.js,{$template-resources}js/sys/__common_events.js"></script>

        <xsl:choose>
            <xsl:when test="user/@type = 'guest'">
                <script type="text/javascript" src="{$template-resources}js/site/auth.js"></script>
                <script type="text/javascript" src="{$template-resources}js/site/bootstrap-pincode-input.js"></script>
            </xsl:when>
            <xsl:when test="/result/@method != 'address' and document('udata://ormcoSiteData/getSiteDataIsUserDadataEmpty/')/udata/@is-empty = '1'">
                <script type="text/javascript">
                    lastJQ(document).ready(function() {
                        lastJQ('#dadataModal').modal('show');
                    });
                </script>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="/result[@module = 'users' and @method = 'address'] or user/@type = 'guest'">
            <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/css/suggestions.min.css" rel="stylesheet" />
            <script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/js/jquery.suggestions.min.js"></script>
            <xsl:if test="/result[@module = 'users' and @method = 'address']">
                <script src="/templates/ormco/js/site/address.js"></script>
            </xsl:if>
        </xsl:if>

        <script src="//code.jivosite.com/widget.js" data-jv-id="jzpQ8JJNjp" async=""></script>
    </xsl:template>
</xsl:stylesheet>