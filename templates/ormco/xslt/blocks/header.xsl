<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result" mode="header">
	    <header class="navbar-fixed-top">
	        <nav class="top navbar navbar-default">
	            <div class="container-fluid">
	                <div class="navbar-header visible-xs">
	                    <a class="navbar-brand" href="/">
	                        <img src="{$template-resources}img/logo.png" alt="Ormco" />
	                        <img src="{$template-resources}img/logo-min.png" class="logo-min" alt="Ormco" />
	                    </a>
	                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                        <span class="sr-only">Toggle navigation</span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>
	                    <a href="#" class="fa fa-phone top_phone"></a>
	                    <xsl:apply-templates select="$user" mode="header_auth_link_mobile" />
	                    <xsl:apply-templates select="$cart" mode="basket_small" />
	                    <a href="javascript:void(0)" class="search js-mobile-search-btn"></a>
	                </div>
	                <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" role="navigation">
	                    <xsl:apply-templates select="document('udata://menu/draw/main_menu')/udata" mode="main_menu" />
	                </div>
	            </div>
	        </nav>
	        <div class="middle hidden-xs">
	            <div class="container-fluid">
	                <a class="navbar-brand" href="/">
	                    <img src="{$template-resources}img/logo.png" alt="Ormco" />
	                </a>
	                <ul class="info">
	                    <!--<li><span>Официальный сайт компании</span><span>Ormco в России</span></li>-->
	                    <li>
							<a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_2']/value}" class="phone"><xsl:value-of select="$settings//property[@name='telefon_2']/value" /></a>
	                        <span class="address"><xsl:value-of select="$settings//property[@name='adres_2']/value" /></span>
	                    </li>
	                    <li>
	                        <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_1']/value}" class="phone"><xsl:value-of select="$settings//property[@name='telefon_1']/value" /></a>
	                        <span class="address"><xsl:value-of select="$settings//property[@name='adres_1']/value" /></span>
	                    </li>
                        <li>
	                        <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_3']/value}" class="phone"><xsl:value-of select="$settings//property[@name='telefon_3']/value" /></a>
	                        <span class="address"><xsl:value-of select="$settings//property[@name='adres_3']/value" /></span>
	                    </li>
<!--	                    <li>
	                        <span class="phone"><xsl:value-of select="$settings//property[@name='telefon_1']/value" /></span>
	                        <span class="address"><xsl:value-of select="$settings//property[@name='adres_1']/value" /></span>
	                    </li> -->
	                </ul>
					<xsl:value-of select="$settings//property[@name='microsheme_code']/value" disable-output-escaping="yes"/>
	                <xsl:apply-templates select="$cart" mode="basket" />
	            </div>
	        </div>
	        <div class="bottom">
	        	<div class="mobile-search-pnl js-mobile-search-pnl">
	                <form action="/search/search_do/" method="get"><input type="text" name="search_string" placeholder="Поиск по сайту" /><span></span><input type="hidden" name="search_branches" value="&search_branches_pids;"/></form>
	            </div>
	        	<div class="mobile-add-phones">
                    <div class="one_phone">
                        Санкт-Петербург
                        <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_2']/value}"><xsl:value-of select="$settings//property[@name='telefon_2']/value" /></a>
                    </div>
                    <div class="one_phone">
                        Москва
                        <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_1']/value}"><xsl:value-of select="$settings//property[@name='telefon_1']/value" /></a>
                    </div>
                    <div class="one_phone">
                        Из регионов России
                        <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_3']/value}"><xsl:value-of select="$settings//property[@name='telefon_3']/value" /></a>
                    </div>
	            </div>
	            <div class="container-fluid">
	                <xsl:apply-templates select="document('udata://catalog/getCategoryListFill/4/5')/udata" mode="getCategoryListFill" />
	                <div class="support-links">
	                    <a href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal" class="link-btn email" data-gtm-event="ButtonClicks" data-gtm-event-value="Бесплатная консультация">Бесплатная консультация</a>
	                    <xsl:if test="$settings//property[@name='catalog_file']/value">
	                    	<xsl:text> </xsl:text>
	                    	<a href="{$settings//property[@name='catalog_file']/value}" target="_blank" class="link-btn download" onclick="ga('send', 'event', 'click', 'cataloguedownload');ym(10184890, 'reachGoal', 'CatalogueDownload');" data-gtm-event="ButtonClicks" data-gtm-event-value="Скачать каталог">Скачать каталог</a>
	                    </xsl:if>
	                    <xsl:if test="$settings//property[@name='pricelist_file']/value">
	                    	<xsl:text> </xsl:text>
	                    	<a href="{$settings//property[@name='pricelist_file']/value}" target="_blank" class="link-btn download" onclick="ga('send', 'event', 'click', 'pricedownload');ym(10184890, 'reachGoal', 'PriceDownload');" data-gtm-event="ButtonClicks" data-gtm-event-value="Скачать прайс">Скачать прайс</a>
	                    </xsl:if>
	                </div>
	                <xsl:apply-templates select="$user" mode="header_auth_link" />
	            </div>
	        </div>
	    </header>
	</xsl:template>
</xsl:stylesheet>