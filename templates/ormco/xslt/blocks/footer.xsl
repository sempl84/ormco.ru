<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result" mode="footer">
        <footer>
            <div class="container-fluid">
                <div class="top clearfix">
                    <ul class="info">
                        <li>
                            <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_2']/value}" class="phone">
                                <xsl:value-of select="$settings//property[@name='telefon_2']/value" />
                            </a>
                            <span class="address">
                                <xsl:value-of select="$settings//property[@name='adres_2']/value" />
                            </span>
                        </li>
                        <li>
                            <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_1']/value}" class="phone">
                                <xsl:value-of select="$settings//property[@name='telefon_1']/value" />
                            </a>
                            <span class="address">
                                <xsl:value-of select="$settings//property[@name='adres_1']/value" />
                            </span>
                        </li>
                        <li>
                            <a onclick="ga('send', 'event', 'click', 'phone number');ym(10184890, 'reachGoal', 'PhoneClick');" href="tel:{$settings//property[@name='telefon_3']/value}" class="phone"><xsl:value-of select="$settings//property[@name='telefon_3']/value" /></a>
                            <span class="address">
                                <xsl:value-of select="$settings//property[@name='adres_3']/value" />
                            </span>
                        </li>
                    </ul>

                    <ul class="social-icons">
<!--                        <li>
                            <a class="circled-icon" href="https://facebook.com/ormcorussia" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>-->
                        <li>
                            <a class="circled-icon" href="https://vk.com/ormcorussia" target="_blank">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="circled-icon" href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="circled-icon" href="https://twitter.com/ormcorussia" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
<!--                        <li>
                            <a class="circled-icon" href="https://www.instagram.com/ormcorussia/" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>-->
                        <li>
                            <a class="circled-icon" href="https://t.me/ormcorussia" target="_blank">
                                <i class="fa fa-telegram" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="partners">
                        <li>
                            <a href="https://med-resurs.org/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/medresurs.png" alt="Союз Мед Ресурс" /></a>
                        </li>
                        <li>
                            <a href="https://www.kerrdental.com/ru-ru/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kerr.png" alt="Kerr" /></a>
                        </li>
                        <li>
                            <!-- <a href="https://www.kavo.com/ru-ru/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kavo.png" alt="KaVo Dental Excellence" /></a> -->
                            <a href="https://dexis.com/ru-ru?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kavo.png" alt="KaVo Dental Excellence" /></a>
                        </li>
                        <li>
                            <a href="https://www.nobelbiocare.com/ru/ru/home.html?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/nb.png" alt="Nobel Biocare" /></a>
                        </li>
                    </ul>
                </div>
                <div class="middle clearfix">
                    <xsl:if test="$user-type = 'guest'">
                        <div class="subscription-pnl">
<!--                            <a href="#registerModal" data-toggle="modal" data-target="#registerModal" class="link-btn" data-gtm-event="ButtonClicks">Зарегистрируйтесь, чтобы подписаться на рассылку</a>-->
                            <a href="#no_register" data-toggle="modal" data-target="#no_register" class="link-btn" data-gtm-event="ButtonClicks">Зарегистрируйтесь, чтобы подписаться на рассылку</a>
                        </div>
                    </xsl:if>

                    <a href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal" class="link-btn ask-question">Бесплатная консультация</a>
                    <ul class="partners">
                        <li>
                            <a href="https://med-resurs.org/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/medresurs.png" alt="Союз Мед Ресурс" /></a>
                        </li>
                        <li>
                            <a href="https://www.kerrdental.com/ru-ru/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kerr.png" alt="Kerr" /></a>
                        </li>
                        <li>
                            <!-- <a href="https://www.kavo.com/ru-ru/?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kavo.png" alt="KaVo Dental Excellence" /></a> -->
                            <a href="https://dexis.com/ru-ru?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/kavo.png" alt="KaVo Dental Excellence" /></a>
                        </li>
                        <li>
                            <a href="https://www.nobelbiocare.com/ru/ru/home.html?utm_source=ormco.ru&amp;utm_medium=footerlink&amp;utm_campaign=synergy" target="_blank"><img src="{$template-resources}img/nb.png" alt="Nobel Biocare" /></a>
                        </li>
                    </ul>
                    <ul class="links">
                        <li>
                            <a href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal" class="link-btn bottom_free_consultation" data-gtm-event="ButtonClicks">Бесплатная консультация</a>
                            <!--<a href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal" class="link-btn ask-question">Бесплатная консультация</a>-->
                            <!--<a href="/politika-konfidencial-nosti/" target="_blank">Политика конфиденциальности</a>-->
                        </li>
                    </ul>
                </div>
                <div class="menu-items">
                    <xsl:apply-templates select="document('udata://menu/draw/bottom_menu')/udata" mode="bottom_menu" />
                </div>
                <div class="bottom">
                    <div class="copyright">
                        <span>© ООО «Ормко», <xsl:value-of select="document('udata://system/convertDate/now/(Y)')/udata" /><br /><a href="/politika-konfidencial-nosti/" target="_blank">Политика конфиденциальности</a></span>
                        <span>Разработка сайта <a href="http://www.realred.ru/" target="_blank">RED</a></span>
                    </div>
                    <ul class="social-icons">
                        <li>
                            <a class="circled-icon" href="https://twitter.com/ormcorussia" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="circled-icon" href="https://vk.com/ormcorussia" target="_blank">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </a>
                        </li>
<!--                        <li>
                            <a class="circled-icon" href="https://www.facebook.com/ormcorussia" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>-->
                        <li>
                            <a class="circled-icon" href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="circled-icon" href="https://t.me/ormcorussia" target="_blank">
                                <i class="fa fa-telegram" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="payment-icons">
                        <li>
                            <xsl:value-of select="$settings//property[@name='metriks']/value" disable-output-escaping="yes" />
                        </li>
                    </ul>
                    <div class="info">
                        <!--<ul class="links">-->
                        <!--<ul>-->
                            <!--<li>-->
                                <!--<a href="/politika-konfidencial-nosti/" target="_blank">Политика конфиденциальности</a>-->
                                <!--<a href="#">Политика конфиденциальности</a>-->
                            <!--</li>-->
                        <!--</ul>-->
                        <span class="blue">Не является офертой. Имеются противопоказания. Проконсультируйтесь со специалистами</span>
                        <span>Сайт разработан в студии <a href="http://www.realred.ru/" target="_blank">RED</a></span>
                    </div>
                </div>
            </div>
        </footer>
        <div class="waiting" style="display:none;"></div>
    </xsl:template>
</xsl:stylesheet>