<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result" mode="right_col">  
	   	<xsl:apply-templates select="document('udata://banners/fastInsert/(right_block)')/udata" mode="right" />
	   	<!-- <xsl:apply-templates select="document('udata://banners/fastInsert/(right_block_2)')/udata" mode="right" />
	   	
        <div class="events">
            <h5 class="txt-recomend">Также рекомендуем</h5>
            <ul class="list">
                <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', &all_event_pid;, '/1/1/2/?extProps=speaker,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string'))/udata/lines/item" mode="short_event_block_right_col"/>
            </ul>
        </div> -->
	</xsl:template>
	
</xsl:stylesheet>