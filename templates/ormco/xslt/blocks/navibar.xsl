<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="udata[@method='navibar']">
        <xsl:param name="header" />
        <xsl:variable name="pos">
            <xsl:choose>
                <xsl:when test="not($document-page-id)">2</xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="breadcrumbs-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <li class="breadcrumb-item" itemscope=""  itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                                <a href="/{$lang-prefix}" itemprop="item">
                                    <span itemprop="name">Главная</span>
                                    <meta itemprop="position" content="1" />
                                </a>
                            </li>
                            <xsl:if test="not($document-page-id)">
                                <xsl:choose>
                                    <xsl:when test="$header">
                                        <li class="breadcrumb-item active">
                                            <span>
                                                <span ><xsl:value-of select="$header" /></span>
                                            </span>
                                        </li>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <li class="breadcrumb-item active" >
                                            <span >
                                                <span ><xsl:value-of select="$document-header" /></span>
                                            </span>
                                        </li>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:apply-templates select="items/item" >
                                <xsl:with-param name="pos" select="$pos" />
                            </xsl:apply-templates>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method='navibar']/items/item">
        <xsl:param name="pos"/>

        <li class="breadcrumb-item" itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="{@link}" itemprop="item">
                <span itemprop="name"><xsl:value-of select="node()" /></span>
                <meta itemprop="position" content="{$pos + position()}" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata[@method='navibar']/items/item[last()]">
        <xsl:param name="pos"/>
        <li class="breadcrumb-item active"  itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <span >
                <span itemprop="name"><xsl:value-of select="node()" /></span>
                <meta itemprop="position" content="{$pos + position()}" />
                <meta itemprop="item" content="https://{$domain}{@link}" />
            </span>
        </li>
    </xsl:template>

    <!-- ec_category_navibar -->
    <xsl:template match="udata[@method='navibar']" mode="ec_category_navibar">
       <xsl:if test="not($document-page-id)"><xsl:value-of select="$document-header" /></xsl:if>
       <xsl:apply-templates select="items/item" mode="ec_category_navibar"/>
    </xsl:template>

    <xsl:template match="udata[@method='navibar']/items/item" mode="ec_category_navibar"><xsl:if test="not(position() = 1)">/</xsl:if><xsl:value-of select="node()" /></xsl:template>

</xsl:stylesheet>