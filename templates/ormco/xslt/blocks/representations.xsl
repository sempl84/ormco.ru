<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result" mode="representations">  
	   	<div class="representations content-block blue hidden-xs">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-md-2 hidden-sm">
	                    <span class="info-icon">i</span>
	                </div>
	                <div class="col-md-4 col-sm-6">
	                    <div class="header4">Представительство в Москве</div>
	                    <p>
	                        Ленинградский проспект, д. 37, корп. 9<br />
	                        Тел. +7 (495) 664-75-55; Факс +7 (495) 664-75-56
	                    </p>
	                </div>
	                <div class="col-md-4 col-sm-6">
	                    <div class="header4">Представительство в Петербурге</div>
	                    <p>
	                        Малоохтинский пр-т, д. 64, корп. 3<br />
	                        Тел. +7 (812) 324-74-14; Факс +7 (812) 320-20-52
	                    </p>
	                </div>
	                <div class="col-md-2 col-sm-12 right">
	                    <a class="btn btn-white" href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal">Написать нам</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
</xsl:stylesheet>