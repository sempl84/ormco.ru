<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="total" />
	<xsl:template match="total[. &gt; ../per_page]">
		<xsl:apply-templates select="document(concat('udata://system/numpages/', ., '/', ../per_page,'///3/'))/udata" />
	</xsl:template>
	
	<xsl:template match="udata[@method = 'numpages']" />
	<xsl:template match="udata[@method = 'numpages'][count(items)]">
		
		<nav class="pagination-wrapper">
			
            <ul class="pagination">
                <li class="page-item" style="display:none">
                	<xsl:apply-templates select="toprev_link" />
                </li>
                <xsl:if test="tobegin_link and tobegin_link/@page-num &lt; items/item[1]/@page-num">
                     <xsl:apply-templates select="tobegin_link" />
                     <xsl:if test="tobegin_link/@page-num &lt; (items/item[1]/@page-num)">
                     	<li class="page-item"><a class="page-link" href="#">...</a></li>
                     </xsl:if>
                </xsl:if>
                <xsl:apply-templates select="items/item" mode="numpages" />
                <xsl:if test="toend_link and toend_link/@page-num &gt; items/item[position()=last()]/@page-num">
                     <xsl:if test="toend_link/@page-num &gt; (items/item[position()=last()]/@page-num + 1)">
                     	<li class="page-item"><a class="page-link" href="#">...</a></li>	
                     </xsl:if>
                     <xsl:apply-templates select="toend_link" />
                </xsl:if>
                <li class="page-item" style="display:none">
                	<xsl:apply-templates select="tonext_link" />
                </li>
            </ul>
        </nav>
	</xsl:template>
	
	<xsl:template match="item" mode="numpages">
		<li class="page-item"><a class="page-link" href="{@link}" pagenum = "{@page-num}"><xsl:value-of select="." /></a></li>
	</xsl:template>
	
	<xsl:template match="item[@is-active = '1']" mode="numpages">
		<li class="page-item active"><a class="page-link" href="{@link}" pagenum = "{@page-num}"><xsl:value-of select="." /></a></li>
	</xsl:template>
	
	<xsl:template match="toprev_link">
		<xsl:attribute name="style"></xsl:attribute>
        <a class="page-link" href="{.}" pagenum = "{@page-num}" aria-label="&previous-page;">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <span class="sr-only">&previous-page;</span>
        </a>
        
	</xsl:template>
	
	<xsl:template match="tonext_link">
		<xsl:attribute name="style"></xsl:attribute>
        <a class="page-link" href="{.}" pagenum = "{@page-num}" aria-label="&next-page;">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">&next-page;</span>
        </a>
        
	</xsl:template>
	
	<xsl:template match="tobegin_link">
		<li class="page-item"><a class="page-link" href="{.}" pagenum = "{@page-num}"><xsl:value-of select="@page-num + 1"/></a></li>
    </xsl:template>

    <xsl:template match="toend_link">
		<li class="page-item"><a class="page-link" href="{.}" pagenum = "{@page-num}"><xsl:value-of select="@page-num + 1"/></a></li>
    </xsl:template>

</xsl:stylesheet>