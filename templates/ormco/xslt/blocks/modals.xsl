<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                exclude-result-prefixes="xsl umi xlink">

    <xsl:template match="result" mode="modals">
        <!-- форма обратной связи -->
        <div class="modal fade register-modal" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <xsl:apply-templates select="document(concat('udata://webforms/add/', &feedback_form_oid;))/udata" mode="feedback_form">
                        <xsl:with-param name="submit_text" select="'Отправить'"/>
                    </xsl:apply-templates>
                </div>
            </div>
        </div>

        <!-- Результат отправки формы -->
        <div class="modal fade register-modal" id="formResultModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <h4 class="modal-title" id="gridRegister"> </h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- фотогалерея в новостях -->
        <xsl:if test=".//property[@name='photos']/value">
            <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="pswp__bg"></div>
                <div class="pswp__scroll-wrap">
                    <div class="pswp__container">
                        <div class="pswp__item"></div>
                        <div class="pswp__item"></div>
                        <div class="pswp__item">
                        </div>
                    </div>
                    <div class="pswp__ui pswp__ui--hidden">
                        <div class="pswp__top-bar">
                            <div class="pswp__counter"></div>
                            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                            <button class="pswp__button pswp__button--share" title="Share"></button>
                            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                            <div class="pswp__preloader">
                                <div class="pswp__preloader__icn">
                                    <div class="pswp__preloader__cut">
                                        <div class="pswp__preloader__donut"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                            <div class="pswp__share-tooltip">
                                <!--<a href="#" class="pswp__share- -facebook"></a>-->
                                <a href="#" class="pswp__share--twitter"></a>
                                <a href="#" class="pswp__share--pinterest"></a>
                                <a href="#" download="" class="pswp__share--download"></a>
                            </div>
                        </div>
                        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                        <div class="pswp__caption">
                            <div class="pswp__caption__center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>

        <!-- подгрузка блоков для гостей: авторизация, регистрация
        и блоков для зарегистрированных: форма регистрации на мероприятие -->
        <xsl:apply-templates select="user" mode="header_auth" />

        <!-- Добавление товара в корзину -->
        <div class="modal fade register-modal" id="addToBasketModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="modal-title header4" id="gridRegister"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Добавление товаров в корзину</div>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

        <xsl:variable name="couponsAddToCartContent" select="document('udata://ormcoCoupons/getOrmcoCouponsMultipleAddToCartContent/')/udata" />

        <!-- Результат добавления товара в корзину -->
        <div class="modal fade register-modal" id="addToBasketResultModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-gtm-event="ButtonClicks" data-gtm-event-value="Закрыть окно товары добавлены в корзину">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="modal-title header4" id="gridRegister">Товар(ы) добавлены в корзину</div>
                    </div>
                    <div class="modal-body">
                        <xsl:if test="$couponsAddToCartContent/content">
                            <div id="couponsMultipleAddToCartContent" class="double_coupons_popup_text" style="display:none;"></div>
                        </xsl:if>

                        <a href="/emarket/cart/" class="btn btn-green pull-right" data-gtm-event="ButtonClicks" data-gtm-event-value="Оформить заказ">Оформить заказ</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" data-gtm-event="ButtonClicks" data-gtm-event-value="Продолжить покупки">Продолжить покупки</button>
                        <!-- <i class="fa fa-check" aria-hidden="true"></i> -->
                    </div>
                </div>
            </div>
        </div>

        <xsl:if test="$couponsAddToCartContent/content">
            <script id="couponsMultipleAddToCartTemplate" type="text/x-jsrender"><xsl:value-of select="$couponsAddToCartContent/content" disable-output-escaping="yes" /></script>
        </xsl:if>

        <!-- Фильтрация -->
        <div class="modal fade register-modal" id="filteringModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="modal-title header4 " id="gridRegister"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Обновление списка товаров</div>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

        <!-- Privacy Policy modal -->
        <xsl:if test="@module = 'materials'">
            <div class="modal privacy-policy-modal fade" id="privacyPolicyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close js-close" data-dismiss="modal" aria-label="Close" data-gtm-event="ButtonClicks" data-gtm-event-value="Закрыть политику конфиденциальности">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="modal-title" id="myModalLabel">Политика конфиденциальности</div>
                        </div>
                        <div class="modal-body">
                            <article>
                                <xsl:value-of select="$settings//property[@name='mm_politic']/value" disable-output-escaping="yes" />
                            </article>
                            <div class="form-group text-right">
                                <a href="/"  class="btn js-out hidden">Я не врач, покинуть раздел</a>
                                <button type="button" class="btn btn-blue js-accept hidden">Подтверждаю, что я врач, ознакомился</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>

        <!-- Privacy Policy modal for doclocator -->
        <xsl:if test="@module = 'doclocator'">
            <div class="modal privacy-policy-modal fade" id="privacyPolicyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close js-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="modal-title" id="myModalLabel">Отказ от ответственности</div>
                        </div>
                        <div class="modal-body">
                            <article>
                                <xsl:value-of select="$settings//property[@name='doclocator_politic']/value" disable-output-escaping="yes" />
                            </article>
                            <div class="form-group text-right">
                                <a href="/"  class="btn js-out hidden">Не согласен, покинуть раздел</a>
                                <button type="button" class="btn btn-blue js-accept hidden" data-gtm-event="ButtonClicks" data-gtm-event-value="Согласен с условиями использования">Согласен с условиями использования</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>

        <!-- Privacy Policy modal for settingsChange -->
        <xsl:if test="@module = 'users'">
            <div class="modal privacy-policy-modal fade" id="settingsChangeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close js-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="modal-title" id="myModalLabel">Редактирование настроек</div>
                        </div>
                        <div class="modal-body">
                            <article>
                                <xsl:value-of select="$settings//property[@name='change_reg_settings_one_akk']/value" disable-output-escaping="yes" />
                            </article>
                            <div class="form-group text-right">
                                <button type="button" class="btn btn-blue js-accept" data-gtm-event="ButtonClicks" data-gtm-event-value="Понятно">Понятно</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>

        <!-- модальные окна для валидации телефона -->
        <div class="modal fade phone-validator-modal" id="phoneValidateModal" tabindex="-1" role="dialog" aria-labelledby="phoneValidateModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Подтверждение телефона</div>
                    </div>
                    <div class="modal-body">
                        <p>Ваш номер мобильного телефона <b><xsl:value-of select="$user-info//property[@name='phone']/value" /></b> не подтвержден</p>
                        <div class="form-group js-phone--wrapper ">
                            <input type="hidden" class="form-control js-phone js-phone--input" value="{$user-info//property[@name='phone']/value}" name="phone" />
                            <a class="btn btn-primary js-phone--send-code" href="#" data-gtm-event="ButtonClicks">Отправить код</a>
                            <a class="btn btn-primary js-phone--change-phone" href="#" data-gtm-event="ButtonClicks">Изменить номер</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade phone-validator-modal" id="phoneChangeModal" tabindex="-1" role="dialog" aria-labelledby="phoneChangeModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Изменение телефона</div>
                    </div>
                    <div class="modal-body">
                        <p class="phoneChangeText">Укажите актуальный номер мобильного телефона:</p>
                        <div class="form-group js-phone--wrapper">
                            <input type="text" class="form-control js-phone js-phone--input" value="{$user-info//property[@name='phone']/value}" name="phone" />
                            <br/>
                            <a class="btn btn-primary js-phone--change-phone-apply" href="#">Применить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END. модальные окна для валидации телефона -->

        <!-- модальное окно с региональными версиями сайта -->
        <div class="modal fade  " id="internationalModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="modal-title header4" id="gridRegister">Welcome to Ormco</div>
                    </div>
                    <div class="modal-body">
                        <p>Please select your country.</p>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="header2">Americas</div>
                                <ul>
                                    <li>
                                        <a href="https://ormco.com/"><i class="flag-icon flag-icon-ca"></i> Canada</a>
                                    </li>
                                    <li>
                                        <a href="http://ormco.com.mx/" target="_blank"><i class="flag-icon flag-icon-mx"></i> Mexico</a>
                                    </li>
                                    <li>
                                        <a href="https://ormco.com/"><i class="flag-icon flag-icon-us"></i> United States</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-9">
                                <div class="header2">EMEA</div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <ul class="xs-m-b-0">
                                            <li>
                                                <a href="https://ormco.at/" target="_blank" title="Visit Ormco Austria"><i class="flag-icon flag-icon-at"></i> Austria</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.dk/" target="_blank" title="Visit Ormco Denmark"><i class="flag-icon flag-icon-dk"></i> Denmark</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.be/" target="_blank" title="Visit Ormco Belgium"><i class="flag-icon flag-icon-be"></i> Belgium</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.fr/" target="_blank" title="Visit Ormco France"><i class="flag-icon flag-icon-fr"></i> France</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.de/" target="_blank" title="Visit Ormco Germany"><i class="flag-icon flag-icon-de"></i> Germany</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.ie/" target="_blank" title="Visit Ormco Ireland"><i class="flag-icon flag-icon-ie"></i> Ireland</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-4">
                                        <ul>
                                            <li>
                                                <a href="https://ormco.in/" target="_blank" title="Visit Ormco India"><i class="flag-icon flag-icon-in"></i> India</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.it/" target="_blank" title="Visit Ormco Italy"><i class="flag-icon flag-icon-it"></i> Italy</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.be/" target="_blank" title="Visit Ormco Luxembourg"><i class="flag-icon flag-icon-lu"></i> Luxembourg</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.nl/" target="_blank" title="Visit Ormco Netherlands"><i class="flag-icon flag-icon-nl"></i> Netherlands</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.pl/" target="_blank" title="Visit Ormco Poland"><i class="flag-icon flag-icon-pl"></i> Poland</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.pt/" target="_blank" title="Visit Ormco Portugal"><i class="flag-icon flag-icon-pt"></i> Portugal</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-4">
                                        <ul>
                                            <li>
                                                <a href="https://ormco.ru/" target="_blank" title="Visit Ormco Russia"><i class="flag-icon flag-icon-ru"></i> Russia</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.co.za/" target="_blank" title="Visit Ormco South Africa"><i class="flag-icon flag-icon-za"></i> South Africa</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.es/" target="_blank" title="Visit Ormco Spain"><i class="flag-icon flag-icon-es"></i> Spain</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.ch/" target="_blank" title="Visit Ormco Switzerland"><i class="flag-icon flag-icon-ch"></i> Switzerland</a>
                                            </li>
                                            <li>
                                                <a href="https://ormco.uk/" target="_blank" title="Visit Ormco United Kingdom"><i class="flag-icon flag-icon-gb"></i> United Kingdom</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <small>If your country is not listed, <a href="https://ormco.com/" target="_blank">click here</a></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade register-modal" id="no_register" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="modal-title header4" id="gridRegister">Регистрация приостановлена</div>
                    </div>
                    <div class="modal-body">
                        <p>Регистрация новых пользователей на сайте временно приостановлена</p>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>