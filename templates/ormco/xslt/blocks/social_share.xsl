<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template name="social_share">
	    <div class="social-share text-right">
            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,odnoklassniki,twitter,gplus" data-counter=""></div>
        </div>
	</xsl:template>

	<xsl:template name="header_social_link" />
<!--	<xsl:template name="header_social_link">  -->
<!--	    <div class="header_social_link pull-right">-->
<!--	    	<xsl:call-template name="social_share" />-->
<!--        </div>-->
<!--	</xsl:template>-->

</xsl:stylesheet>