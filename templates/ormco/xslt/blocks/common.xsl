<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">
	
	<xsl:include href="navibar.xsl" />
	<xsl:include href="thumbnails.xsl" />
	<xsl:include href="numpages.xsl" />
	
	<xsl:include href="header.xsl" />
	<xsl:include href="representations.xsl" />
	<xsl:include href="footer.xsl" />
	<xsl:include href="modals.xsl" />
	<xsl:include href="scripts.xsl" />
	
	<xsl:include href="right_col.xsl" />
	<xsl:include href="left_col.xsl" />
	
	<xsl:include href="social_share.xsl" />
	
	
	

</xsl:stylesheet>