<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" />

	<xsl:template match="body">
		<html>
			<head></head>
			<body>
                <table cellspacing="0" cellpadding="0" align="center" width="600" style="border: 0;">
                    <tr>
                        <td style="height: 60px; padding: 0; background: #07477D; text-align: center;">
                            <a href="https://ormco.ru/" style="border: 0;">
                                <img src="https://ormco.ru/templates/ormco/img/letters/logo.png" alt="" style="border: 0; width: 108px;" />
                            </a>
                        </td>
                    </tr>

                    <xsl:value-of select="content" disable-output-escaping="yes" />

                    <tr>
                        <td style="background: #07477D; padding: 38px 24px 24px;">
                            <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; margin: 0; font-family: Arial; color: #7F9DB1; font-size: 15px; line-height: 24px;">
                                <tr>
                                    <td style="padding: 0; vertical-align: top;">
                                        <p style="margin: 0; color: #fff;">
                                            <a href="tel:+78123247414" style="color: #ffffff; text-decoration: none;">
                                                <b>+7 (812) 324-74-14</b>
                                            </a>
                                        </p>
                                        <p style="margin: 0 0 20px;">
                                            <a href="https://yandex.ru/maps/-/CCUaR2gOXC?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" style="color: #7F9DB1; text-decoration: none;">СПб, Малоохтинский пр., д.64. к.3</a>
                                        </p>
                                        <p style="margin: 0; color: #fff;">
                                            <a href="tel:+74956647555" style="color: #ffffff; text-decoration: none;">
                                                <b>+7 (495) 664-75-55</b>
                                            </a>
                                        </p>
                                        <p style="margin: 0 0 30px;">
                                            <a href="https://yandex.ru/maps/-/CCUaR2s0pB?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" style="color: #7F9DB1; text-decoration: none;">Москва, ул. Станиславского, д.21, стр.3</a>
                                        </p>
                                        <a href="https://itunes.apple.com/us/app/%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D1%81-ormco/id1212861947?mt=8" style="text-decoration: none;">
                                            <img src="https://ormco.ru/templates/ormco/img/letters/appstore.png" alt="" style="border: 0; width: 140px; margin-right: 8px;" />
                                        </a>
                                        <a href="https://play.google.com/store/apps/details?id=dpromo.app.ormco" style="text-decoration: none;">
                                            <img src="https://ormco.ru/templates/ormco/img/letters/market.png" alt="" style="border: 0; width: 140px;" />
                                        </a>
                                    </td>
                                    <td style="padding: 0; vertical-align: top;">
                                        <p style="margin: 0 0 14px;">
                                            <a href="https://ormco.ru/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/logo2.png" alt="" style="width: 120px; border: 0;" />
                                            </a>
                                        </p>
                                        <p>
<!--                                            <a href="https://www.instagram.com/ormcorussia/" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/in.png" alt="" style="border: 0; width: 32px; margin-right: 10px;" />
                                            </a>-->
<!--                                            <a href="https://facebook.com/ormcorussia" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/fb.png" alt="" style="border: 0; width: 32px; margin-right: 10px;" />
                                            </a>-->
                                            <a href="https://twitter.com/ormcorussia" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/tw.png" alt="" style="border: 0; width: 32px; margin-right: 10px;" />
                                            </a>
                                            <a href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/yt.png" alt="" style="border: 0; width: 32px; margin-right: 10px;" />
                                            </a>
                                            <a href="https://vk.com/ormcorussia" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/vk.png" alt="" style="border: 0; width: 32px; margin: 0;" />
                                            </a>
                                            <a href="https://vk.com/ormcorussia" style="text-decoration: none;">
                                                <img src="https://ormco.ru/templates/ormco/img/letters/tg.png" alt="" style="border: 0; width: 32px; margin: 0;" />
                                            </a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>