<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:template match="mail_verification_subject">
        <xsl:text>Восстановление пароля</xsl:text>
    </xsl:template>

    <xsl:template match="mail_verification">
        <xsl:variable name="restore_link" select="concat(substring-before(restore_link, '/restore/'), '/update_password/', substring-after(restore_link, '/restore/'))" />

        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Восстановление пароля</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте!</xsl:text>
                    <br />
                    <xsl:text>Кто-то, возможно Вы, пытается восстановить пароль для пользователя </xsl:text>
                    <xsl:value-of select="login" />
                    <xsl:text> на сайте </xsl:text>
                    <a href="https://{domain}">
                        <xsl:value-of select="domain" />
                    </a>.
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Если это не Вы, просто проигнорируйте данное письмо. Не волнуйтесь, Ваши данные в безопасности!</xsl:text>
                </p>
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block; background: #E5EEF3; margin: 0 0 40px 0;">
                    <p style="font-size: 16px; line-height: 27px; color: #07477D; margin: 10px 0;">Если Вы хотите восстановить пароль, нажмите на кнопку:</p>
                    <p style="margin: 0;">
                        <a href="{$restore_link}" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 7px; text-align: center; min-width: 200px; margin-right: 12px;">Восстановить пароль</a>
                    </p>
                    <p style="font-size: 14px; line-height: 27px; color: #07477D; margin: 10px 0 0;">Или вставьте ссылку в браузер:</p>
                    <p style="font-size: 14px; line-height: 27px; color: #07477D; margin: 0 0 10px;"><a href="{$restore_link}" style="color: #2AA8EC;"><xsl:value-of select="$restore_link" /></a></p>
                </div>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="mail_password_subject">
        <xsl:text>Новый пароль для сайта</xsl:text>
    </xsl:template>

    <xsl:template match="mail_password">
        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Новый пароль</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте!</xsl:text>
                    <br />
                    <xsl:text>Отправляем Ваш новый пароль от сайта </xsl:text>
                    <a href="https://{domain}" style="color: #2AA8EC;">
                        <xsl:value-of select="domain" />
                    </a>.
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block; background: #E5EEF3; margin: 0 0 40px 0;">
                    <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 32px;">
                        <tr>
                            <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ЛОГИН</xsl:text></td>
                            <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ПАРОЛЬ</xsl:text></td>
                        </tr>
                        <tr>
                            <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="login" /></td>
                            <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="password" /></td>
                        </tr>
                    </table>
                    <p style="margin: 0;">
                        <a href="https://{domain}/users/settings/" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 7px; text-align: center; min-width: 200px; margin-right: 12px;">Войти в аккаунт</a>
                    </p>
                </div>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>