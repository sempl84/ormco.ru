<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="html" indent="yes" />

	<xsl:template match="mail_registrated_subject">
		Регистрация на сайте <xsl:value-of select="domain" />
	</xsl:template>

	<xsl:template match="mail_registrated_subject_noactivation">
		Регистрация на сайте <xsl:value-of select="domain" />
	</xsl:template>

	<xsl:template match="mail_registrated_noactivation">
        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Регистрация на сайте <xsl:value-of select="domain" /></b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="lname" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="fname" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="father_name" />
                    <xsl:text>,</xsl:text>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Вы зарегистрировались на сайте </xsl:text>
                    <a href="https://{domain}">
                        <xsl:value-of select="domain" />
                    </a>.
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 32px;">
                    <tr>
                        <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ЛОГИН</xsl:text></td>
                        <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ПАРОЛЬ</xsl:text></td>
                    </tr>
                    <tr>
                        <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="login" /></td>
                        <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="password" /></td>
                    </tr>
                </table>
            </td>
        </tr>
	</xsl:template>

	<xsl:template match="mail_registrated">
        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Регистрация на сайте <xsl:value-of select="domain" /></b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="lname" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="fname" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="father_name" />
                    <xsl:text>,</xsl:text>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Вы зарегистрировались на сайте </xsl:text>
                    <a href="https://{domain}">
                        <xsl:value-of select="domain" />
                    </a>.
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 32px;">
                    <tr>
                        <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ЛОГИН</xsl:text></td>
                        <td style="color: #2AA8EB; width: 220px; padding: 8px 0;" valign="top"><xsl:text>ПАРОЛЬ</xsl:text></td>
                    </tr>
                    <tr>
                        <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="login" /></td>
                        <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="password" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block; background: #E5EEF3; margin: 0 0 40px 0;">
                    <p style="font-size: 16px; line-height: 27px; color: #07477D; margin: 10px 0;">Для завершения регистрации нажмите на кнопку:</p>
                    <p style="margin: 0;">
                        <a href="{activate_link}" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 7px; text-align: center; min-width: 200px; margin-right: 12px;">Подтвердить регистрацию</a>
                    </p>
                    <p style="font-size: 14px; line-height: 27px; color: #07477D; margin: 10px 0 0;">Или вставьте ссылку в браузер:</p>
                    <p style="font-size: 14px; line-height: 27px; color: #07477D; margin: 0 0 10px;"><a href="{activate_link}" style="color: #2AA8EC;"><xsl:value-of select="activate_link" /></a></p>
                </div>
            </td>
        </tr>
	</xsl:template>

	<xsl:template match="mail_admin_registrated_subject">
		<xsl:text>Зарегистрировался новый пользователь</xsl:text>
	</xsl:template>

	<xsl:template match="mail_admin_registrated">
        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>
                        <xsl:text>Зарегистрировался новый пользователь "</xsl:text>
                        <xsl:value-of select="login" />
                        <xsl:text>".</xsl:text>
                    </b>
                </p>
            </td>
        </tr>
	</xsl:template>
</xsl:stylesheet>