<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:template match="confirm_email_letter_subject">
        <xsl:text>Подтверждение изменения e-mail адреса</xsl:text>
    </xsl:template>

    <xsl:template match="confirm_email_letter_content">
        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Подтверждение изменения e-mail адреса</b>
                </p>
                <p style="margin: 0 0 24px;">
                     <xsl:text>Здравствуйте!</xsl:text>
                    <br />
                    <xsl:text>Кто-то, возможно Вы, пытается изменить контактный e-mail для пользователя "</xsl:text>
                    <xsl:value-of select="login" />
                    <xsl:text>" на сайте </xsl:text>
                    <a href="https://{domain}">
                        <xsl:value-of select="domain" />
                    </a>.
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Если это не Вы, просто проигнорируйте данное письмо. Не волнуйтесь, Ваши данные в безопасности!</xsl:text>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Если Вы действительно хотите изменить свой контакнтый e-mail, кликните по этой ссылке:</xsl:text>
                    <br />
                    <a href="{change_link}">
                        <xsl:value-of select="change_link" />
                    </a>
                </p>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>