<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:template match="status_notification">
        <xsl:text>Ваш заказ #</xsl:text>
        <xsl:value-of select="order_number" />
        <xsl:text> </xsl:text>
        <xsl:value-of select="status" />
        <br/>
        <br/>
        <xsl:text>Посмотреть историю заказов вы можете в своем </xsl:text>
        <a href="https://{domain}/emarket/ordersList/">
            <xsl:text>личном кабинете</xsl:text>
        </a>.
    </xsl:template>

    <xsl:template match="status_notification_receipt">
        <xsl:text>Ваш заказ #</xsl:text>
        <xsl:value-of select="order_number" />
        <xsl:text> </xsl:text>
        <xsl:value-of select="status" />
        <br/>
        <br/>
        <xsl:text>Посмотреть историю заказов вы можете в своем </xsl:text>

        <a href="https://{domain}/emarket/ordersList/">
            <xsl:text>личном кабинете</xsl:text>
        </a>.

        <br/>
        <br/>
        <xsl:text>Квитанцию на оплату вы можете получить, перейдя по </xsl:text>
        <a href="https://{domain}/tcpdf/docs/receipt.php?oi={order_id}">
            <xsl:text>этой ссылке</xsl:text>
        </a>
    </xsl:template>

    <xsl:template match="neworder_notification">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <xsl:text>Поступил новый заказ #</xsl:text>
        <xsl:value-of select="order_number" />
        <xsl:text> (</xsl:text>
        <a href="https://{domain}/admin/emarket/order_edit/{order_id}/">
            <xsl:text>Просмотр</xsl:text>
        </a>
        <xsl:text>)</xsl:text>
        <br/>
        <br/>

        ФИО: <b><xsl:value-of select="$customer//property[@name = 'lname']//value" />&#160;<xsl:value-of select="$customer//property[@name = 'fname']//value" />&#160;<xsl:value-of select="$customer//property[@name = 'father_name']//value" /></b><br />
        <xsl:text>Способ оплаты: </xsl:text>
        <xsl:value-of select="payment_type" />
        <br/>
        <xsl:text>Статус оплаты: </xsl:text>
        <xsl:value-of select="payment_status" />
        <br/>
        <xsl:text>Сумма оплаты: </xsl:text>
        <xsl:value-of select="price" />
        <xsl:text>&#160;</xsl:text>
        <span class="rub">руб.</span>
        <br/><br/>
        <xsl:text>Состав заказа:</xsl:text>
        <br/>
        <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;width:100%;">
            <tr>
                <th style="font-weight:bold;text-align:center;">Название товара</th>
                <th>&#160;</th>
                <th style="font-weight:bold;text-align:center;">Цена</th>
                <th>&#160;</th>
                <th style="font-weight:bold;text-align:center;">Кол-во</th>
                <th>&#160;</th>
                <th style="font-weight:bold;text-align:center;">Сумма</th>
            </tr>
            <xsl:apply-templates select="$order//property[@name = 'order_items']/value/item" mode="one_order_item" />
        </table>
    </xsl:template>

    <xsl:template match="item" mode="one_order_item">
        <xsl:variable name="order_item" select="document(concat('uobject://', @id))" />
        <tr>
            <td>
                <a href="https://ormco.ru{$order_item//property[@name = 'item_link']/value/page/@link}" style="color:#1876c3;font:Arial, sans-serif;-webkit-text-size-adjust:none;" target="_blank">
                    <xsl:value-of select="@name" />
                </a>
            </td>
            <td>&#160;</td>
            <td style="text-align:right">
                <xsl:value-of select="$order_item//property[@name = 'item_price']/value" />
                <xsl:text>&#160;</xsl:text>
                <span class="rub">руб.</span>
            </td>
            <td>&#160;</td>
            <td style="text-align:center">
                <xsl:value-of select="$order_item//property[@name = 'item_amount']/value" />
            </td>
            <td>&#160;</td>
            <td style="text-align:right">
                <xsl:value-of select="$order_item//property[@name = 'item_total_price']/value" />
                <xsl:text>&#160;</xsl:text>
                <span class="rub">руб.</span>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="invoice_subject">
        <xsl:text>На сайте </xsl:text>
        <xsl:value-of select="domain" />
        <xsl:text> успешно сформирован счет</xsl:text>
    </xsl:template>

    <xsl:template match="invoice_content">
        Вы можете распечатать счет перейдя <a href="http://{domain}/tcpdf/docs/invoicee.php?oi={order_id}">по ссылке</a>
    </xsl:template>

    <xsl:template name="personal_link">
        <xsl:attribute name="href" select="concat('http://', domain, '/users/settings/')" />
    </xsl:template>


    <!--Заказ одобрен-->
    <xsl:template match="order_approved">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> одобрен</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!
                </p>

                <p style="margin: 0 0 24px;">Ваш заказ одобрен менеджером Ormco! Теперь, вы можете его оплатить.</p>
                <p style="margin: 0;">
                    <a href="{concat('https://ormco.ru/delivery/oplatit-zakaz-po-nomeru/?order_id=', order_id)}" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Оплатить заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;margin: 0 0 40px;">
                    <p style="padding: 0; font-size: 20px; line-height: 27px; color: #2AA8EC; margin: 0 0 24px;"><b>Обратите внимание</b></p>
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Пожалуйста, оплатите заказ в течение 5 дней во избежание его отмены</p>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                    <a href="https://orthodontia.ru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>

    <!-- Идентификация нового купона -->
    <xsl:template match="new_coupon_notification">
        <tr>
            <td style="padding: 48px 24px 48px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Вы получили промокод от Ormco</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="user_name" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="user_father_name" />!
                </p>
                <p style="margin: 0 0 24px;">Спасибо за участие в образовательном мероприятии Ormco Тестирование промокодов!<br />
                    Мы признательны, что вы делаете выбор в пользу нашего полезного обучения и дарим вам промокод на покупку любой продукции производства Ormco 5% (кроме категорий Литература и Гигиена) или 10% на покупку Брекет Damon Q2, VECTORTAS, Дуги Cu Ni-Ti Damon.</p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px 48px; font-family: Arial;">
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block;">
                    <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 24px;">
                        <tr>
                            <td style="padding: 0; width: 34px;">
                                <img src="https://ormco.ru/templates/ormco/img/letters/gift.png" alt="" style="border: 0; width: 24px; display: block; margin: 0 0 2px;" />
                            </td>
                            <td style="padding: 0; font-size: 20px; line-height: 27px; color: #2AA8EC; margin: 0 0 24px;">
                                <b>Ваш промокод: <xsl:value-of select="coupon_code" /></b>
                            </td>
                        </tr>
                    </table>
                    <p style="margin: 0; font-size: 16px; line-height: 24px; color: #07477D;">
                        <b>Как воспользоваться промокодом:</b>
                    </p>
                    <ol style="margin: 0 0 24px 20px; padding: 0; font-size: 16px; line-height: 24px; color: #333242;">
                        <li style="margin: 0;">Сообщите его менеджеру по работе с клиентами при оформлении заказа по телефону/WhatsApp/в чате на сайте.</li>
                        <li style="margin: 0;">Оформите заказ в нашем Интернет-магазине. Промокод уже применился к вашей учетной записи.</li>
                    </ol>
                    <p style="margin: 0;">
                        <a href="https://ormco.ru/market/" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 265px; margin-right: 12px;">Воспользоваться промокодом</a>
                        <a href="https://ormco.ru/users/discounts/" style="display: inline-block; color: #2AA8EC; border: 1px solid #2AA8EC; border-radius: 32px; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 134px; margin-right: 16px;">Мои скидки</a>
                    </p>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background:#2AA8EC; border-radius: 12px; padding: 24px; display: block;">
                    <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 24px;">
                        <tr>
                            <td style="padding: 0; width: 34px;">
                                <img src="https://ormco.ru/templates/ormco/img/letters/info.png" alt="" style="border: 0; width: 24px; display: block; margin: 0 0 2px;" />
                            </td>
                            <td style="padding: 0; font-size: 20px; line-height: 27px; color: #ffffff; margin: 0 0 24px;">
                                <b>Обратите внимание</b>
                            </td>
                        </tr>
                    </table>
                    <ul style="margin: 0 0 0 20px; padding: 0; font-size: 16px; line-height: 24px; color: #ffffff;">
                        <li style="margin: 0;">Скидки по промокоду 5% и 10% не суммируются между собой, но суммируются с вашей персональной скидкой. Итоговый размер скидки может составлять не более 35%.</li>
                        <li>Промокод действует 14 календарных дней, начиная с даты окончания образовательного мероприятия.</li>
                        <li>В одном заказе можно воспользоваться только одним промокодом.</li>
                    </ul>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также, вы можете заказать у нас все необходимые инструменты для практики в ортодонтии.</p>
                <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Сделать новый заказ</a>
                <a href="https://ormco.ru/emarket/ordersList/" style="display: inline-block; color: #2AA8EC; border: 1px solid #2AA8EC; border-radius: 32px; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 134px; margin-right: 16px;">Мои заказы</a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>