<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="html" indent="yes" />
    <xsl:decimal-format name="price" decimal-separator="," grouping-separator="&#160;"/>

    <!-- Принят -->
    <xsl:template match="status_1c_adopted">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;"><b>Ваш заказ №<xsl:value-of select="order_number" /> поступил в обработку</b></p>

                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!
                </p>

                <p style="margin: 0 0 24px;">Ваш заказ №<xsl:value-of select="order_number" /> принят и передан в обработку менеджером Ormco. Мы обработаем заказ в течение суток. Менеджер свяжется с вами, если потребуется уточнить детали.</p>

                <p>Вы можете отслеживать статус заказа в личном кабинете на сайте <a href="https://ormco.ru/users/settings/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" style="color: #2AA8EC;">ormco.ru</a></p>

                <p style="margin: 0;">
                    <a href="https://ormco.ru/emarket/ordersList/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Мои заказы</a>
                    <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; color: #2AA8EC; border: 1px solid #2AA8EC; border-radius: 32px; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Сделать новый заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block; margin-bottom:40px;">
                    <p style="font-size: 20px; line-height: 27px; color: #2AA8EC; margin: 0 0 20px;"><b>Информация о заказе</b></p>

                    <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 32px;">
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">ФИО</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$customer//property[@name = 'lname']//value" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$customer//property[@name = 'father_name']//value" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Способ оплаты</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'payment_id']//value/item/@name" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Способ доставки</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'delivery_id']//value/item/@name" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Время доставки</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:apply-templates select="$order//property[@name = 'prefer_date_time_delivery_start']" mode="start_time" />
                                <xsl:text> </xsl:text>
                                <xsl:apply-templates select="$order//property[@name = 'prefer_date_time_delivery_finish']" mode="end_time" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Комментарий к заказу</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'comment']//value" disable-output-escaping="yes" />
                            </td>
                        </tr>
                    </table>

                    <xsl:apply-templates select="$order//property[@name = 'order_items']" mode="order_items" />
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 96px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #868686;">Также, у нас есть программы на обучение ортодонтии.</p>
                <a href="https://orthodontia.ru/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px;
        line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Обучение</a>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="property" mode="start_time" />
    <xsl:template match="property[value]" mode="start_time">
        <xsl:text> от </xsl:text>
        <xsl:value-of select="document(concat('udata://system/convertDate/', ./value/@unix-timestamp, '/(d.m.Y)'))" />
		<xsl:text> </xsl:text>
        <xsl:value-of select="document(concat('udata://system/convertDate/', ./value/@unix-timestamp, '/(H:i)'))" />
    </xsl:template>

    <xsl:template match="property" mode="end_time" />
    <xsl:template match="property[value]" mode="end_time">
        <xsl:text> до </xsl:text>
        <xsl:value-of select="document(concat('udata://system/convertDate/', ./value/@unix-timestamp, '/(d.m.Y)'))" />
		<xsl:text> </xsl:text>
        <xsl:value-of select="document(concat('udata://system/convertDate/', ./value/@unix-timestamp, '/(H:i)'))" />
    </xsl:template>

    <xsl:template match="property" mode="order_items" />
    <xsl:template match="property[value/item]" mode="order_items">
        <p style="font-size: 20px; line-height: 27px; color: #2AA8EC; margin: 0 0 20px;">
            <b>Состав заказа</b>
        </p>
        <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0;">
            <tr>
                <td style="color: #07477D; width: 220px; padding: 8px 0; border-bottom: 1px solid #E0E0E0;" valign="top">Наименование</td>
                <td style="color: #868686; padding: 8px 0; min-width: 70px; border-bottom: 1px solid #E0E0E0;" valign="top">Цена</td>
                <td style="color: #868686; padding: 8px 0; border-bottom: 1px solid #E0E0E0;" valign="top">Кол-во</td>
                <td style="color: #868686; padding: 8px 0; text-align: right; border-bottom: 1px solid #E0E0E0;" valign="top">Стоимость</td>
            </tr>

            <xsl:apply-templates select="value/item" mode="order_items" />

            <xsl:if test="//property[@name = 'total_original_price']/value != //property[@name = 'total_price']/value">
                <tr>
                    <td style="color: #07477D; width: 220px; padding: 8px 0; border-top: 1px solid #E0E0E0;" valign="top">Подытог</td>
                    <td colspan="3" style="color: #868686; padding: 8px 0; text-align: right; border-top: 1px solid #E0E0E0;" valign="top"><xsl:value-of select="format-number(number(//property[@name = 'total_original_price']/value), '#&#160;###,##','price')" /> ₽</td>
                </tr>
            </xsl:if>

            <xsl:if test="//property[@name = 'delivery_price']/value &gt; 0">
                <tr>
                    <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Доставка</td>
                    <td colspan="3" style="color: #868686; padding: 8px 0; text-align: right;" valign="top"><xsl:value-of select="format-number(number(//property[@name = 'delivery_price']/value), '#&#160;###,##','price')" /> ₽</td>
                </tr>
            </xsl:if>

            <xsl:if test="//property[@name = 'order_discount_value']/value &gt; 0">
                <tr>
                    <td style="color: #07477D; width: 220px; padding: 8px 0; border-bottom: 1px solid #E0E0E0;" valign="top">Скидка</td>
                    <td colspan="3" style="color: #868686; padding: 8px 0; text-align: right; border-bottom: 1px solid #E0E0E0;" valign="top"><xsl:value-of select="format-number(number(//property[@name = 'order_discount_value']/value), '#&#160;###,##','price')" /> ₽</td>
                </tr>
            </xsl:if>

            <tr>
                <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">
                    <b>Итого</b>
                </td>
                <td colspan="3" style="color: #07477D; padding: 8px 0; text-align: right;" valign="top">
                    <b><xsl:value-of select="format-number(number(../property[@name = 'total_price']/value), '#&#160;###,##','price')" /> ₽</b>
                </td>
            </tr>
        </table>
    </xsl:template>

    <xsl:template match="item" mode="order_items">
        <xsl:variable name="item" select="document(concat('uobject://', @id))" />
        <tr>
            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top"><xsl:value-of select="$item/udata/object/@name" /></td>
            <td style="color: #868686; padding: 8px 0; min-width: 70px;" valign="top"><xsl:value-of select="format-number(number($item//property[@name = 'item_price']/value), '#&#160;###,##','price')" /> ₽</td>
            <td style="color: #868686; padding: 8px 0;" valign="top"><xsl:value-of select="$item//property[@name = 'item_amount']/value" /></td>
            <td style="color: #868686; padding: 8px 0; text-align: right;" valign="top"><xsl:value-of select="format-number(number($item//property[@name = 'item_total_price']/value), '#&#160;###,##','price')" /> ₽</td>
        </tr>
    </xsl:template>


    <!-- К оплате -->
    <xsl:template match="status_1c_to_pay">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> одобрен и ожидает оплаты</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!
                </p>

                <p style="margin: 0 0 24px;">При оформлении заказа вы выбрали способ оплаты: <xsl:value-of select="$order//property[@name = 'payment_id']//value/item/@name" /></p>

                <xsl:choose>
                    <xsl:when test="$order//property[@name = 'payment_id']//value/item/@id = 14044">
                        <p><a href="https://ormco.ru/tcpdf/docs/receipt.php?oi={order_id}" target="_blank" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 250px; margin-right: 12px;">Открыть квитанцию на оплату</a></p>
                    </xsl:when>
                    <xsl:when test="$order//property[@name = 'payment_id']//value/item/@id = 13911">
                        <p><a href="https://ormco.ru/tcpdf/docs/invoicee.php?oi={order_id}" target="_blank" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 250px; margin-right: 12px;">Открыть счет на оплату</a></p>
                    </xsl:when>
                    <xsl:when test="$order//property[@name = 'payment_id']//value/item/@id = 1106995">
                        <p><a href="https://ormco.ru/delivery/oplatit-zakaz-po-nomeru/?order_id={order_id}&amp;utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional" target="_blank" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 250px; margin-right: 12px;">Оплатить заказ онлайн</a></p>
                        <p>Прямая ссылка на страницу оплаты: <xsl:value-of select="concat('https://ormco.ru/delivery/oplatit-zakaz-po-nomeru/?order_id=', order_id)" /></p>
                    </xsl:when>
                    <xsl:when test="$order//property[@name = 'payment_id']//value/item/@id = 1327116">
                        <p>Чтобы оплатить заказ наличными, приезжайте в рабочие дни с 9:30 до 17:30 в офис Ormco в Москве по адресу БЦ "Фабрика Станиславского", ул.Станиславского, д.21, стр.3, этаж 1</p>
                    </xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                    <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>


    <!-- Оплачен -->
    <xsl:template match="status_1c_paid">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #07477D; margin: 0 0 24px;">
                    <b>Оплата прошла успешно!</b>
                </p>

                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!
                </p>

                <p style="margin: 0 0 24px;">Оплата Вашего заказа прошла успешно! Сейчас мы начнем его собирать и готовить к отгрузке. Будем держать Вас в курсе!</p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="border: 2px solid #2AA8EC; border-radius: 12px; padding: 24px; display: block; margin-bottom:40px;">
                    <p style="font-size: 20px; line-height: 27px; color: #2AA8EC; margin: 0 0 20px;">
                        <b>Информация о заказе</b>
                    </p>

                    <table cellspacing="0" cellpadding="0" width="100%" style="border: 0; font-size: 15px; line-height: 24px; margin: 0 0 32px;">
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Способ оплаты</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'payment_id']//value/item/@name" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Способ доставки</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'delivery_id']//value/item/@name" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Время доставки</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:apply-templates select="$order//property[@name = 'prefer_date_time_delivery_start']" mode="start_time" />
                                <xsl:text> </xsl:text>
                                <xsl:apply-templates select="$order//property[@name = 'prefer_date_time_delivery_finish']" mode="end_time" />
                            </td>
                        </tr>
                        <tr>
                            <td style="color: #07477D; width: 220px; padding: 8px 0;" valign="top">Комментарий к заказу</td>
                            <td style="color: #868686; padding: 8px 0;" valign="top">
                                <xsl:value-of select="$order//property[@name = 'comment']//value" disable-output-escaping="yes" />
                            </td>
                        </tr>
                    </table>

                    <xsl:apply-templates select="$order//property[@name = 'order_items']" mode="order_items" />
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 96px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #868686;">Также, у нас есть программы на обучение ортодонтии.</p>
                <a href="https://orthodontia.ru/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #2AA8EC; border-radius: 32px; color: #fff; text-decoration: none; font-size: 14px;
        line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Обучение</a>
            </td>
        </tr>
    </xsl:template>


    <!-- Готов -->
    <xsl:template match="status_1c_ready">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> поступил на сборку</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!<br />
                    Мы уже начали собирать ваш заказ и отгрузим его в течение суток.</p>
                <p style="margin: 0;">
                    <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; background: #FF5F30; border-radius: 32px; color: #ffffff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Сделать новый заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                    <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>


    <!-- Резерв частичный -->
    <xsl:template match="status_1c_partially_assembled">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> частично собран</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!<br />
                    Ваш заказ частично собран. С вами свяжется менеджер Ormco для уточнения деталей.</p>
                <p style="margin: 0;">
                    <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; background: #FF5F30; border-radius: 32px; color: #ffffff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Сделать новый заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                    <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>


    <!-- Отгружен -->
    <xsl:template match="status_1c_shipped">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />
        <xsl:variable name="delivery" select="document(concat('uobject://', $order//property[@name = 'delivery_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> уже едет к вам!</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!<br />
                    <xsl:choose>
                        <xsl:when test="$delivery//property[@name = 'delivery_type_id']//value/item/@id = 204">
                            <!--Если выбран способ доставки Самовывоз, то:-->
                            Ваш заказ №<xsl:value-of select="order_number" /> отгружен. До новых встреч!
                        </xsl:when>
                        <xsl:otherwise>
                            <!--<xsl:when test="$delivery//property[@name = 'delivery_type_id']//value/item/@id = 80">-->
                            <!--Если выбрана доставка, то:-->
                            Ваш заказ №<xsl:value-of select="order_number" /> отгружен со склада, передан курьерской службе и скоро будет у вас!
                        </xsl:otherwise>
                    </xsl:choose>
                </p>
                <p style="margin: 0;">
                    <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; background: #FF5F30; border-radius: 32px; color: #ffffff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Сделать новый заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                    <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>


    <!-- Отменен -->
    <xsl:template match="status_1c_cancel">
        <xsl:variable name="order" select="document(concat('uobject://', order_id))" />
        <xsl:variable name="customer" select="document(concat('uobject://', $order//property[@name = 'customer_id']/value/item/@id))" />

        <tr>
            <td style="padding: 48px 24px 96px; font-family: Arial; font-size: 16px; line-height: 24px; color: #333242;">
                <p style="font-size: 24px; line-height: 33px; color: #377BB5; margin: 0 0 24px;">
                    <b>Заказ №<xsl:value-of select="order_number" /> отменен</b>
                </p>
                <p style="margin: 0 0 24px;">
                    <xsl:text>Здравствуйте, </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'fname']//value" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$customer//property[@name = 'lname']//value" />!
                </p>
                <p style="margin: 0 0 24px;">Ваш заказ был отменен. Ничего страшного! Это могло произойти, если:<br />
                    1. Вы попросили наших менеджеров его отменить;<br />
                    2. Заказ не был оплачен в течение 14 дней с даты его создания.</p>

                <p style="margin: 0;">
                    <a href="https://ormco.ru/market/?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=sdelat_noviy_zakaz" style="display: inline-block; background: #FF5F30; border-radius: 32px; color: #ffffff; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 12px;">Сделать новый заказ</a>
                </p>
            </td>
        </tr>

        <tr>
            <td style="padding: 0 24px; font-family: Arial;">
                <div style="background: #E5EEF3; border-radius: 12px; padding: 24px;">
                    <p style="font-size: 16px; line-height: 24px; color: #377BB5; margin: 0 0 24px;">Если у Вас возникли сложности или вопросы, наши менеджеры с радостью вам помогут и дадут профессиональную консультацию:</p>
                    <table style="padding: 0; border-collapse: collapse; border: 0;">
                        <tr>
                            <td style="padding: 0 24px 0 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">Телефон</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="tel:88002227414" style="color: #377BB5; text-decoration: none;">8 (800) 222-74-14</a>
                                </p>
                            </td>
                            <td style="padding: 0;">
                                <p style="font-weight: 600; font-size: 14px; line-height: 19px; text-transform: uppercase; color: #2AA8EC; margin: 0 0 8px;">E-mail</p>
                                <p style="font-size: 16px; line-height: 24px; margin: 0;">
                                    <a href="mailto:dc-sales@ormco.com" style="color: #377BB5; text-decoration: none;">dc-sales@ormco.com</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td style="padding: 48px 0 48px; text-align: center; font-family: Arial;">
                <p style="margin: 0 0 24px; font-size: 14px; line-height: 22px; color: #7F9DB1;">Также у нас есть образовательный портал по обучению ортодонтии<br />
                        <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="color: #2AA8EC;">Orthodontia.ru</a>
                </p>
                <a href="https://orthodontia.ru?utm_source=triggers_ormcoru&amp;utm_medium=email&amp;utm_campaign=transactional&amp;utm_content=ssylka_orthodontiaru" style="display: inline-block; background: #fff; border: 1px solid #2AA8EC; border-radius: 32px; color: #2AA8EC; text-decoration: none; font-size: 14px; line-height: 20px; padding: 14px 0; text-align: center; min-width: 200px; margin-right: 16px;">Перейти на сайт</a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>