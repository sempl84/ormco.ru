(function ($, jQuery) {
	if($('.search-clinic-page').length > 0){
		var searchClinics = {
			$clinicsMapWrapper: null,
			$clinicsViewData: null,
			regionsData: [],
			citiesData: [],
			clinicsData: [],
			$regionsSelect: null,
			$citiesSelect: null,
			googleMap: null,
			markers: [],
			markerCluster: null,
			allCitiesValue: 'Выбрать город',

			init: function () {
				// Set defaults
				this.$clinicsMapWrapper = $('.js--clinics-map');
				this.$clinicsViewData = $('.js--clinics-view-data');

				// Set initial data
				this.regionsData = regions;
				this.citiesData = cities;
				this.clinicsData = clinics;

				// Selects & options
				this.$regionsSelect = $('.js--clinics-regions');
				this.$citiesSelect = $('.js--clinics-cities');
				searchClinics.setRegionsOptions();
				searchClinics.setCitiesOptions();

				// Init map
				if (this.$clinicsMapWrapper.length !== 0) {
					var mapOptions = {
						center: new google.maps.LatLng(-34.397, 150.644),
						zoom: 8,
						scrollwheel: false,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					this.googleMap = new google.maps.Map(this.$clinicsMapWrapper[0], mapOptions);

					this.$regionsSelect.change(function () {
						searchClinics.setCitiesOptions();
						searchClinics.setClinicsData();
					});

					this.$citiesSelect.change(function () {
						searchClinics.setClinicsData();
					});

					searchClinics.setClinicsData();
				}

				// Click handlers
				this.$clinicsViewData
					.on('click', '.js--select-region', function (e) {
						e.preventDefault();

						const val = $(this).data('clinic-region');
						searchClinics.$regionsSelect.selectpicker('val', val);
						searchClinics.setCitiesOptions();
						searchClinics.setClinicsData();
					})
					.on('click', '.js--clear-region', function (e) {
						e.preventDefault();

						searchClinics.$regionsSelect.selectpicker('val', 'none');
						searchClinics.setCitiesOptions();
						searchClinics.setClinicsData();
					})
					.on('click', '.js--clear-city', function (e) {
						e.preventDefault();

						searchClinics.$citiesSelect.selectpicker('val', 'none');
						searchClinics.setClinicsData();
					})
					.on('click', '.js--select-city', function (e) {
						e.preventDefault();

						const val = $(this).data('clinic-city');
						searchClinics.$citiesSelect.selectpicker('val', val);
						searchClinics.setClinicsData();
					});

				$(document).on('change', '.one_clinic_type', function (e) {
					e.preventDefault();
					searchClinics.setClinicsData();
				});

				google.maps.event.addDomListener(this.$clinicsMapWrapper, 'mousedown', function () {
					google.maps.event.addListenerOnce(searchClinics.googleMap, "dragstart", function () {
						isDragging = true;
					});
					isDragging = false;
				});
			},

			setRegionsOptions: function () {
				for (var i = 0; i < this.regionsData.length; i++) {
					var region = this.regionsData[i];
					$("<option></option>").appendTo(this.$regionsSelect).text(region.title).val(region.id);
				}
			},

			setCitiesOptions: function () {
				var selectedRegion = this.$regionsSelect.val();
				var selectedRegionIndex = this.$regionsSelect.children().index(this.$regionsSelect.find("option:selected"));

				// Clear cities select
				this.$citiesSelect.empty();

				// Set options
				$("<option></option>").appendTo(this.$citiesSelect).text(this.allCitiesValue).val('none');
				for (var i = 0; i < this.citiesData.length; i++) {
					var city = this.citiesData[i];
					if (selectedRegionIndex === 0) {
						$("<option></option>").appendTo(this.$citiesSelect).text(city.title).val(city.id);
					} else if (Number(city.region_id) === Number(selectedRegion)) {
						$("<option></option>").appendTo(this.$citiesSelect).text(city.title).val(city.id);
					}
				}

				// Refresh cities select
				this.$citiesSelect.selectpicker('refresh');
			},

			setClinicsData: function () {
				var countItem = 0;
				var lastLocation = null;
				var mapBounds = new google.maps.LatLngBounds();
				this.$clinicsViewData.empty();

				var selectedRegion = this.$regionsSelect.val();
				var selectedCity = this.$citiesSelect.val();
				var selectedRegionIndex = this.$regionsSelect.children().index(this.$regionsSelect.find("option:selected"));
				var selectedCityIndex = this.$citiesSelect.children().index(this.$citiesSelect.find("option:selected"));

				var selected_types = new Array();
				$('.js--select-type input:checked').each(function(){
					selected_types.push($(this).data('id')+'');
				});

				this.clearClinicsMarkers();

				// Markers & data
				this.markers = [];
				for (var i = 0; i < this.clinicsData.length; i++) {
					var item = this.clinicsData[i];
					var clinicCity = this.citiesData.filter(function (city) {
						return Number(city.id) === Number(item.city_id);
					});

					if ((Number(item.city_id) === Number(selectedCity) || selectedCityIndex === 0) &&
							(Number(clinicCity[0].region_id) === Number(selectedRegion) || selectedRegionIndex === 0)) {
						var location = new google.maps.LatLng(item.lat, item.lng);
						lastLocation = location;

						if(selected_types.indexOf(item.type_id) == -1){
							// skip
						}else{
							if (!item.marker) {
								item.marker = this.createClinicMarker(item, location, clinicCity);
							}

							this.markers.push(item.marker);
							mapBounds.extend(location);
							countItem++;
						}
					}
				}

				this.setViewData();

				if (countItem === 0) {
					mapBounds.extend(location);
				} else if (countItem === 1) {
					this.googleMap.setOptions({
						center: lastLocation,
						zoom: 10
					});
				} else {
					this.googleMap.fitBounds(mapBounds);
				}

				this.markerCluster = new MarkerClusterer(this.googleMap, this.markers, {
					imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
				});

				for (var i = 0; i < this.markers.length; i++) {
					this.markers[i].setVisible(true);
				}
			},

			createClinicMarker: function (clinic, location, clinicCity) {
				var marker = new google.maps.Marker({
					id: clinic.id,
					position: location,
					title: clinic.title,
					type: clinic.type_id,
					icon: clinic.type_icon ? clinic.type_icon : 'https://ormco.ru/templates/ormco/img/marker-icon.png',
					type_hint: clinic.type_hint,
	//				icon: 'https://ormco.ru/templates/ormco/img/marker-icon.png'
				});

				var infoWindow = new google.maps.InfoWindow({
					content: '<b>' + clinic.title + '</b><br/>г.' + clinicCity[0].title + ', ' + clinic.address + '<br/>' + clinic.phone + ' <i style="display:block"><b>' + clinic.type + '</b></i>'
				});

				google.maps.event.addListener(marker, 'mouseover', function () {
					infoWindow.open(searchClinics.googleMap, marker);
				});
				google.maps.event.addListener(marker, 'mouseout', function () {
					infoWindow.close();
				});

				google.maps.event.addListener(marker, 'click', function () {
					searchClinics.googleMap.setZoom(14);
					searchClinics.googleMap.setCenter(marker.getPosition());
				});

				this.$clinicsViewData
					.on('mouseover', '.js--show-info[data-clinic-id="' + clinic.id + '"]', function (e) {
						infoWindow.open(searchClinics.googleMap, marker);
					})
					.on('mouseout', '.js--show-info[data-clinic-id="' + clinic.id + '"]', function (e) {
						infoWindow.close();
					});

				return marker;
			},

			clearClinicsMarkers: function () {
				for (var i = 0; i < this.markers.length; i++) {
					this.markers[i].setMap(null);
				}
				if(this.markerCluster != null){
					this.markerCluster.clearMarkers();
				}
			},

			setViewData: function () {
				var selectedRegion = this.$regionsSelect.val();
				var selectedCity = this.$citiesSelect.val();
				var selectedRegionIndex = this.$regionsSelect.children().index(this.$regionsSelect.find("option:selected"));
				var selectedCityIndex = this.$citiesSelect.children().index(this.$citiesSelect.find("option:selected"));

				if (selectedRegionIndex === 0 && selectedCityIndex === 0) {
					// Row
					var $content = $("<div class='search-clinic'></div>").appendTo(this.$clinicsViewData);

					// Header
					$("<h2 class='search-clinic__header'></h2>").appendTo($content).text('Выберите интересующий регион');

					// Content
					var $row = $("<div class='search-clinic__content row'></div>").appendTo($content);
					this.regionsData.forEach(function (region) {
						var $col = $("<div class='search-clinic__col col-md-4 col-sm-6 col-xs-12'></div>").appendTo($row);
						$("<a class='search-clinic__link js--select-region'></a>")
							.appendTo($("<div class='search-clinic__item'></div>").appendTo($col))
							.attr("href", "#").attr("data-clinic-region", region.id).text(region.title);
					});
				} else if (selectedRegionIndex > 0 && selectedCityIndex === 0) {
					// Prepare data
					var currentRegion = this.regionsData.filter(function (region) {
						return Number(region.id) === Number(selectedRegion);
					});
					var currentRegionCities = this.citiesData.filter(function (city) {
						return Number(city.region_id) === Number(currentRegion[0].id);
					});
					var currentRegionCitiesChunked = this.arraysChunk(currentRegionCities, Math.ceil(currentRegionCities.length / 3));

					// Content
					var $content = $("<div class='search-clinic'></div>").appendTo(this.$clinicsViewData);

					// Header
					$("<h2 class='search-clinic__header'></h2>").appendTo($content).text(currentRegion[0].title);

					// Row
					var $row = $("<div class='search-clinic__content row'></div>").appendTo($content);
					currentRegionCitiesChunked.forEach(function (cityArray) {
						var $col = $("<div class='search-clinic__col col-md-4 col-sm-6 col-xs-12'></div>").appendTo($row);
						cityArray.forEach(function (city) {
							$("<a class='search-clinic__link js--select-city'></a>")
								.appendTo($("<div class='search-clinic__item'></div>").appendTo($col))
								.attr("href", "#").attr("data-clinic-city", city.id).text(city.title);
						})
					});
					$("<a class='search-clinic__back js--clear-region'></a>").appendTo($content).attr("href", "#").text("Сменить регион");
				} else if (selectedCityIndex > 0) {
					// Prepare data
					var currentCity = this.citiesData.filter(function (city) {
						return Number(city.id) === Number(selectedCity);
					});
					var currentClinics = this.clinicsData.filter(function (clinic) {
						return Number(clinic.city_id) === Number(currentCity[0].id);
					});

					// Wrapper
					var $wrapper = $("<div class='search-clinic'></div>").appendTo(this.$clinicsViewData);

					$("<a class='search-clinic__back js--clear-city'></a>").appendTo($wrapper).attr("href", "#").text("Сменить город");

					// Header
					$("<h2 class='search-clinic__header'></h2>").appendTo($wrapper).text(currentCity[0].title);

					// Content
					var $content = $("<div class='search-clinic__content'></div>").appendTo($wrapper);

					// Content
					currentClinics.forEach(function (clinic) {
						var $row = $("<div class='search-clinic__item-inner row'></div>")
							.appendTo(
								$("<div class='search-clinic__item js--show-info'></div>")
								.appendTo($content)
								.attr("data-clinic-id", clinic.id)
							);
						var $leftCol = $("<div class='search-clinic__item-left col-sm-7 col-xs-12'></div>").appendTo($row);
						var $rightCol = $("<div class='search-clinic__item-right col-sm-5 col-xs-12'></div>").appendTo($row);

						$("<h3 class='search-clinic__item-title'></h3>").appendTo($leftCol).text(clinic.title);
						$("<div class='search-clinic__item-address'></div>").appendTo($leftCol).html("<i class='fa fa-map-marker' aria-hidden='true'></i>" + clinic.address);
						$("<p class='search-clinic__item-phone'></p>").appendTo($rightCol).text(clinic.phone);
						$("<a class='search-clinic__item-site'></a>")
							.appendTo($rightCol)
							.attr("href", clinic.url)
							.attr("target", "_blank")
							.attr("rel", "noopener noreferrer nofollow")
							.text(clinic.url);
					});
				}
			},

			arraysChunk: function (array, length) {
				var chunks = [];
				var i = 0;
				var n = array.length;
				while (i < n) {
					chunks.push(array.slice(i, i += length));
				}
				return chunks;
			}
		};

		jQuery(document).ready(function () {
			searchClinics.init();
		});
	}
})(lastJQ, lastJQ);