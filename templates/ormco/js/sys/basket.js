(function($,jQuery){

site.basket = {};

site.basket.$couponsMultipleAddToCartContent = null;

site.basket.replace = function(id, bRenderCart) {
	if (id == 'all') {
		jQuery('a[id^="add_basket_"]').each(function() {
			$(this).text(i18n.basket_add_button)
			jQuery('.basket_info_summary').text(i18n.basket_empty);
		});
	}
	return function(e) {
		var text, discount, goods_discount_price, goods_discount, item_total_price, item_discount, cart_item, basket, i, item, related_goods,
			cart_summary = jQuery('.cart_summary'),
			cart_summary_origin = jQuery('.cart_summary_origin'),
			cart_discount = jQuery('.cart_discount'),
			goods_discount_price = jQuery('.cart_goods_discount'),
			rem_item = true,
			detect_options = {},
			summary_origin_price = 0;

		if(bRenderCart) {
			site.basket.renderCart();
		} else {
			if (e.summary.amount > 0) {
				text = (e.summary.price.actual).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ") + ' pуб';

				goods_discount = ((typeof e.summary.price.original == 'undefined') ? e.summary.price.actual : e.summary.price.original);
				discount = ((typeof e.summary.price.discount != 'undefined') ? e.summary.price.discount : '0');
				for (i in e.items.item) {
					item = e.items.item[i];
					summary_origin_price = summary_origin_price+ ((typeof item["total-price"].original != 'undefined') ? item["total-price"].original : 0);
					if (item.id == id) {
						rem_item = false;
						item_total_price = item["total-price"].actual;
						item_total_origin_price = (typeof item["total-price"].original != 'undefined') ? item["total-price"].original : false;
						item_discount = ((typeof item.discount != 'undefined') ? item.discount.amount : '0');
					}
					if (item.page.id == id) {
						if (detect_options.amount) {
							detect_options.amount = detect_options.amount + item.amount;
						}
						else detect_options = {'id':id, 'amount':item.amount};
					}
				}
				summary_origin_price = (summary_origin_price).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ") + ' pуб';
				if (detect_options.amount) {
					var add_basket_button = jQuery('#add_basket_' + detect_options.id),
						add_basket_mark = jQuery('#add_basket_mark_' + detect_options.id);
					if(add_basket_mark.length > 0){
						add_basket_mark.show();
					}

					// var add_basket_button = jQuery('#add_basket_' + detect_options.id);
					// if (add_basket_button[0].tagName.toUpperCase() == 'A' && !site.basket.is_cart) {
					// add_basket_button.text(i18n.basket_add_button + ' (' + detect_options.amount + ')');
					// }
					// if (add_basket_button[0].tagName.toUpperCase() == 'FORM') {
					// add_basket_button = jQuery('input:submit', add_basket_button);
					// add_basket_button.val(i18n.basket_add_button + ' (' + detect_options.amount + ')');
					// }
					// else add_basket_button.val(i18n.basket_add_button + ' (' + detect_options.amount + ')');
				}
				if (rem_item) {
					if (cart_item = jQuery('.cart_item_' + id)) {
						if(related_goods = jQuery('.cart_item_' + id + ' + tr.related-goods')) {
							related_goods.remove();
						}
						cart_item.remove();
						cart_summary.text(text);
						cart_summary_origin.text(summary_origin_price);
						cart_discount.text(discount);
						goods_discount_price.text(goods_discount);
						jQuery('.basket tr').removeClass('even');
						jQuery('tr[class^="cart_item_"]:odd').addClass('even');
						jQuery('tr[class^="cart_item_"]:odd + .related-goods').addClass('even');
					}
				}
				else {
					cart_summary.text(text);
					cart_summary_origin.text(summary_origin_price);
					cart_discount.text(discount);
					goods_discount_price.text(goods_discount);
				}
				text = text || 0;
				text = e.summary.amount;
			}
			else {
				//console.log(i18n.basket_empty_html);
				text = 0;
				if (basket = jQuery('.basket.white-pnl')) {
					basket.html(i18n.basket_empty_html);
				}
			}
			jQuery('.basket_info_summary').text(text);
			site.basket.modify.complete = true;

			if(typeof e['coupon'] !== 'undefined') {
                if(jQuery('#couponsMultipleAddToCartTemplate').length > 0){
                    site.basket.$couponsMultipleAddToCartContent.html(jQuery('#couponsMultipleAddToCartTemplate').render({
                        code: e['coupon']['code']
                    })).show();
                }
			} else {
				site.basket.$couponsMultipleAddToCartContent.html('').hide();
			}

			if(!site.basket.add.complete){
				jQuery('#addToBasketResultModal').modal('show');
				jQuery('.waiting').hide();
				/*setTimeout(function(){
                    jQuery('#addToBasketResultModal').modal('hide');
                    jQuery('body.modal-open').attr('class','').attr('style','');
                }, 4000);*/
				site.basket.add.complete = true;
			}
			if(!site.basket.addSeveralBrackets.complete){
				jQuery('#addToBasketModal').modal('hide');
				jQuery('body.modal-open').attr('class','').attr('style','');
				jQuery('#addToBasketResultModal').modal('show');
				jQuery('.waiting').hide();
				/*setTimeout(function(){
                    jQuery('#addToBasketResultModal').modal('hide');
                    jQuery('body.modal-open').attr('class','').attr('style','');
                }, 4000); */
				site.basket.addSeveralBrackets.complete = true;
			}
		}
	};
};

site.basket.add = function(id, form, popup) {
	var e_name, options = {};
	if (form) {
		var elements = jQuery(':radio:checked', form);
		for (var i = 0; i < elements.length; i++) {
			e_name = elements[i].name.replace(/^options\[/, '').replace(/\]$/, '');
			options[e_name] = elements[i].value;
		}
	}
	if(jQuery('#amount_id_'+id).val() > 0){
		options['amount'] = jQuery('#amount_id_'+id).val();
	}
	basket.putElement(id, options, this.replace(id));
	if (popup) {
		site.message.close(jQuery('#add_options_' + id), jQuery('.overlay'))
	}
};
site.basket.addSeveral = function(ids,_this) {
	site.basket.addSeveralBrackets.complete = false;
	jQuery('#addToBasketModal').modal('show');

	var e_name, options = {};
	var result = ids.split('_');
	var lastItem = result.pop();
	//console.log(lastItem);
	var id_last = lastItem.split('|');
	var amount_last = id_last.pop();
	id_last = parseInt(id_last);
	if(!(id_last > 0)) {
		lastItem = result.pop();
		id_last = lastItem.split('|');
		amount_last = id_last.pop();
		id_last = parseInt(id_last);
		if(!(id_last > 0)) {
			lastItem = result.pop();
			id_last = lastItem.split('|');
			amount_last = id_last.pop();
			id_last = parseInt(id_last);
		}
	}
	jQuery(_this).html(jQuery(_this).text() + '<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>');
	for (i in result) {
		options = {};
		//id = parseInt(result[i]);
		id = result[i].split('|');
		amount = id.pop();
		id = parseInt(id);
		if(id > 0) {
			options['amount'] = amount;
			basket.putElement(id, options, function(){});
		}

	}
	options = {};
	options['amount'] = amount_last;
	basket.putElement(id_last, options, function(){jQuery('.fa-spinner',jQuery(_this)).remove();site.basket.replace(id_last); location.href='/emarket/cart/'});
};

site.basket.addSeveralBrackets = function(ids,_this) {
	site.basket.addSeveralBrackets.complete = false;
	jQuery('#addToBasketModal').modal('show');

	var e_name, options = {};
	var result = ids.split('_');
	var lastItem = result.pop();
	//console.log(lastItem);
	var id_last = lastItem.split('|');
	id_last = parseInt(id_last);
	if(!(id_last > 0)) {
		lastItem = result.pop();
		id_last = lastItem.split('|');
		id_last = parseInt(id_last);
		if(!(id_last > 0)) {
			lastItem = result.pop();
			id_last = lastItem.split('|');
			id_last = parseInt(id_last);
		}
	}
	jQuery(_this).html(jQuery(_this).text() + '<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>');
	options = {};

	if(typeof gtm === 'object') {
		gtm.sendGtmEventEcommerceBrackets(ids);
	}

	basket.__request("/udata/emarket/addSeveralBrackets/put/element/" + ids + ".json", options, this.replace(id_last));

	//console.log(id_last);
	jQuery('.fa-spinner',jQuery(_this)).remove();
};

/*site.basket.addSeveralBrackets = function(ids,_this) {
	site.basket.addSeveralBrackets.complete = false;
	jQuery('#addToBasketModal').modal('show');

	var e_name, options = {};
	var result = ids.split('_');
	var lastItem = result.pop();
	//console.log(lastItem);
	var id_last = lastItem.split('|');
	var amount_last = id_last.pop();
	id_last = parseInt(id_last);
	if(!(id_last > 0)) {
		lastItem = result.pop();
		id_last = lastItem.split('|');
		amount_last = id_last.pop();
		id_last = parseInt(id_last);
		if(!(id_last > 0)) {
			lastItem = result.pop();
			id_last = lastItem.split('|');
			amount_last = id_last.pop();
			id_last = parseInt(id_last);
		}
	}
	jQuery(_this).html(jQuery(_this).text() + '<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>');
	for (i in result) {
		options = {};
		//id = parseInt(result[i]);
		id = result[i].split('|');
		amount = id.pop();
		id = parseInt(id);
		if(id > 0) {
			options['amount'] = amount;
			basket.putElement(id, options, this.replace(id_last));
		}

	}
	options = {};
	options['amount'] = amount_last;
	//console.log(id_last);
	jQuery('.fa-spinner',jQuery(_this)).remove();
	basket.putElement(id_last, options, this.replace(id_last));
};*/

site.basket.list = function(link) {
	var oneClick = false;
	var id = (link.id.indexOf('add_basket') != -1) ? link.id.replace(/^add_basket_/, '') : false;
	if (!id) {
		oneClick = true;
		id = (link.id.indexOf('one_click') != -1) ? link.id.replace(/^one_click_/, '') : link;
	}
	if (!id) return false;
	if (jQuery(link).hasClass('options_true')) {
		var url = '/upage//' + id + '?transform=modules/catalog/popup-add-options.xsl';
		if (oneClick) {
			url = '/upage//' + id + '?transform=modules/catalog/popup-add-options-oneclick.xsl';
		}
		if (jQuery('#add_options_' + id).length == 0) {
			jQuery.ajax({
				url: url,
				dataType: 'html',
				success: function (data) {
					var isMessageCreated = site.message({
						id: 'add_options_' + id,
						header: i18n.basket_options,
						width: 400,
						content: data,
						async: false
					});
					if (isMessageCreated == true) {
						var form = jQuery('form.options');
						form.append('<input type="submit" class="button" value="'+ i18n.basket_add_short +'" />');
						form.submit(function() {
							if (!site.basket.is_cart && !oneClick) {
								site.basket.add(id, this, true);
							} else if(!site.basket.is_cart && oneClick) {
								site.basket.oneClick(id, this);
								site.message.close(jQuery('#add_options_' + id), jQuery('.overlay'))
							} if( site.basket.is_cart ) {
								return true;
							}
							return false;
						});
					}
				}
			});
		}
	}
	else if(!oneClick) {
		this.add(id);
	} else {
		site.basket.oneClick(id);
	}
};

site.basket.modify = function(id, amount_new, amount_old) {
	if (amount_new.replace(/[\d]+/) == 'undefined' && amount_new != amount_old) {
		basket.modifyItem(id, {amount:amount_new}, this.replace(id, true));
	}
	else this.modify.complete = true;
};

site.basket.modify.complete = true;
site.basket.add.complete = true;
site.basket.addSeveralBrackets.complete = true;




site.basket.remove = function(id) {
	if (id === 'all') {
		$('*[data-gtm-product]').each(function() {
			let $item = $(this);

			gtm.sendGtmEventEcommerceRemove($item.data('gtm-product'), $item.find('input.amount').val());
		});

		basket.removeAll(this.replace(id, true));
	} else {
		let $item = $('.cart_item_' + id);

		if($item.data('gtm-product')) {
			gtm.sendGtmEventEcommerceRemove($item.data('gtm-product'), $item.find('input.amount').val());
		}

		basket.removeItem(id, this.replace(id, true));
	}
};

site.basket.renderCart = function() {
	$.ajax({
		url: '/udata/emarket/cart/?transform=layouts/ajax/modules/emarket/cart.xsl',
		dataType: 'html',
		success: function (data) {
			$('*[data-cart=cart-wrapper]').html(data);
			site.basket.modify.complete = true;
		}
	});
};

site.basket.oneClick = function(id, form) {
	var option = $(form).find('input[type="radio"]:checked');
	var optional;
	if (option.length > 0) {
		optional = '?' + option.attr('name') + '=' + $(form).find('input[type="radio"]:checked').val()
	} else {
		optional = '';
	}

	var locale = location.pathname;
	var lang = locale.substring(1,locale.substring(1).indexOf('/')+1);

	jQuery.ajax({
		url: '/udata/emarket/createForm/emarket-purchase-oneclick?transform=modules/catalog/one-click.xsl&lang=' + lang,
		dataType: 'html',
		success: function (data) {
			var isMessageCreated = site.message({
				id: 'one_click_popup_' + id,
				header: i18n.oneclick_checkout,
				width: 400,
				content: data,
				async: false
			});
			if (isMessageCreated == true) {
				var form = jQuery('form.buyer_data');
				form.attr('action', form.attr('action') + 'element/' + id + '.xml' + optional)
				form.append('<div><input type="submit" class="button big" value="' + i18n.checkout + '" /></div>');
				form.submit(function() {
					var xml = site.basket.sendForm(form).responseXML;

					form.closest('.content').find('.error').remove();
					$(xml).find('error').each(
						function() {
							form.closest('div').prepend('<div class="error">' + $(this).text() + '</div>');
						}
					);

					form.closest('.content').find('.success').remove();
					if ($(xml).find('orderId').length > 0) {
						site.basket.replace('all');
						form.closest('div').prepend('<div class="success"><div><p>' + i18n.finish_message_prefix + '<strong>#' + $(xml).find('orderId').eq(0).text() + '</strong>' + i18n.finish_message_postfix + '</p></div><a class="button big close">' + i18n.continue + '</a></div>');
						form.remove();
					}

					return false;
				});
			}
		}
	});
}

site.basket.sendForm = function(form) {
	return jQuery.ajax({
		url: form.attr('action'),
		dataType: 'xml',
		method: form.attr('method'),
		data: form.serialize(),
		async: false,
		success: function (data) {
			return data;
		}
	});
}

site.basket.init = function() {
	this.is_cart = (!!jQuery('.basket table').length);
	this.$couponsMultipleAddToCartContent = jQuery('#couponsMultipleAddToCartContent');
	jQuery('body').on('click','.basket_list', function(){
            if (!site.basket.is_cart || !jQuery(this).hasClass('options_false')) {
                $('.waiting').show();
                site.basket.add.complete = false;
                //jQuery('#addToBasketModal').modal('show');
                site.basket.list(this);
                return false;
            }
	});
	jQuery('form.options').submit(function(){
            $('.waiting').show();
            var id = (this.id.indexOf('add_basket') != -1) ? this.id.replace(/^add_basket_/, '') : this;
            site.basket.add(id, this);
            return false;
	});
	jQuery('body').on('click', 'div.basket a.del', function(){
		site.basket.remove(this.id.match(/\d+/).pop());

		var id = $(this).data('id'),
	 		name = $(this).data('name'),
	 		category = $(this).data('category'),
	 		brand = $(this).data('brand'),
	 		price = $(this).data('price'),
	 		qty = $(this).data('qty');

		try{
			ga('ec:addProduct', {
			   'id': id,
			   'name': name,
			   'category': category,
			   'brand': brand,
			   'price': price,
			   'quantity': qty
			});
			ga('ec:setAction', 'remove');

			ga('send', 'event', 'ET', 'removal from cart');

			// yandex add data
			dataLayer.push({
			   "ecommerce": {
				   "remove": {
					   "products": [
						   {
							   'id': id,
							   'name': name,
							   'price': price,
							   'brand': brand,
							   'category': category,
							   'quantity': qty
						   }
					   ]
				   }
			   }
			});

			ym(10184890, 'reachGoal', 'RemoveFromCart');
		}catch (err){
			console.log(err);
		}
		return false;
	});
	jQuery('body').on('click', 'a.basket_remove_all', function(){
		site.basket.remove('all');

		try{
			jQuery('div.basket a.del').each(function(){
				var id = $(this).data('id'),
					name = $(this).data('name'),
					category = $(this).data('category'),
					brand = $(this).data('brand'),
					price = $(this).data('price'),
					qty = $(this).data('qty');

				ga('ec:addProduct', {
				   'id': id,
				   'name': name,
				   'category': category,
				   'brand': brand,
				   'price': price,
				   'quantity': qty
				});
				ga('ec:setAction', 'remove');

				// yandex add data
				dataLayer.push({
				   "ecommerce": {
					   "remove": {
						   "products": [
							   {
								   'id': id,
								   'name': name,
								   'price': price,
								   'brand': brand,
								   'category': category,
								   'quantity': qty
							   }
						   ]
					   }
				   }
				});
			});

			ga('send', 'event', 'ET', 'removal from cart');
			ym(10184890, 'reachGoal', 'RemoveFromCart');
		}catch (err){
			console.log(err);
		}
		return false;
	});
	jQuery('body').on('keyup', 'div.basket input.amount', function() {
		if(0 > parseInt(this.value))
			this.value = Math.abs(this.value);
		if (site.basket.modify.complete) {
			site.basket.modify.complete = false;
			let amountThis = this;
			setTimeout(function() {
				let id = parseInt(amountThis.parentNode.parentNode.className.split('_').pop()),
					e = jQuery(amountThis).next('input'),
					old = e.val();
					e.val(amountThis.value);

				site.basket.modify(id, amountThis.value, old);
			}, 500)
		}
	});

	jQuery('body').on('click', '.change-amount .top, .change-amount .bottom', function(){
		if (site.basket.modify.complete) {
			site.basket.modify.complete = false;

			let $item = $(this).closest('*[data-gtm-product]'),
				id = parseInt(jQuery(this).parents('tr').attr('class').split('_').pop()),
				e = jQuery(this).parents('.input-group.spinner').find('.amount'),
				old = e.val();

			if (jQuery(this).hasClass('top')) {
				e.val(parseInt(old) + 1);
			} else {
				e.val(parseInt(old) - 1);
			}

			if($item.length && (parseInt(old) > parseInt(e.val()))) {
				gtm.sendGtmEventEcommerceRemove($item.data('gtm-product'), old - parseInt(e.val()));
			}

			jQuery(this).parents('.input-group.spinner').find('.amount_old').val(old);
			site.basket.modify(id, e.val(), old);
		}
	});
};

jQuery(document).ready(function(){site.basket.init()});

})(lastJQ,lastJQ);