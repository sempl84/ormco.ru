jQuery(document).ready(function () {
    let $input = jQuery('#address_input'),
        $data = jQuery('#address_data'),
        $form = jQuery('#address_form');

    console.log($input.data('token'));

    $input.suggestions({
        token: $input.data('token'),
        type: "ADDRESS",
        onSearchStart: function () {
            $input.data('suggestion', null);
        },
        onSelect: function (suggestion) {
            $input.data('suggestion', suggestion);
        }
    });

    $form.on('submit', function (e) {
        let suggestion = $input.data('suggestion');

        if(!suggestion) {
            e.preventDefault();
            alert('Введите адрес');
            return false;
        }

        if(!suggestion['data']['house']) {
            e.preventDefault();
            alert('Не указан дом');
            return false;
        }

        $data.val(JSON.stringify(suggestion));
    });
});