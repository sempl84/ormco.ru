(function($,jQuery){

var menuService = {
	init: function () {
        var $productsMenu = $('.js-product-menu');
        $('.js-product-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
        $('header').data('position', 0);
        $(window).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $('header').addClass('shrink');
            } else {
                $('header').removeClass('shrink');
            }
            if ($(document).width() > 767 && $productsMenu.hasClass('open')) {
                if (!$('header').data('fixed') && $('header').data('position') < $(document).scrollTop())
                    $('header').css({ 'position': 'absolute', 'top': $(document).scrollTop() }).data('fixed', true).data('position', $(document).scrollTop());
                else if ($('header').data('position') > $(document).scrollTop())
                    $('header').data('fixed', false).data('position', $(document).scrollTop());
                else if (($('header').data('position') + $productsMenu.find('> ul').height() + 107) < $(document).scrollTop()) {
                    $('header').data('fixed', false).data('position', $(document).scrollTop());
                    $productsMenu.siblings().removeClass('open');
                    $productsMenu.toggleClass('open');
                }
            } else
                $('header').data('fixed', false);

            if (!$('header').data('fixed')) {
                $('header').css({ 'position': '', 'top': '' });
            }
        });
    }
}

var topArrow = {
	init : function() {
		var $topLink = $('<a/>').attr('href', 'javascript:void(0)').addClass('top-arrow').appendTo($(document.body));
		$topLink.click(function(e) {
			e.preventDefault();

			$('body,html').stop().animate({
				scrollTop : 0
			}, 500);
		});

		$(window).scroll(function() {
			if ($(document).scrollTop() < 50)
				$topLink.hide();
			else
				$topLink.css('display', 'block');
		}).scroll();
	}
}

var slider = {
	init : function() {
		var numberInRow = -1;
		$('.js-slider').carousel({
			interval : 10000
		});
		function setSiderItems() {
			$('.js-slider .item').each(function() {
				var next = $(this).next();
				$(this).children().slice(1).remove();
				for (var i = 0; i < numberInRow; i++) {
					if (!next.length)
						next = $(this).siblings(':first');
					next.children(':first-child').clone().appendTo($(this));
					//set next element
					next = next.next();
				}
				$(this).children().css('width', 1 / (numberInRow + 1) * 100 + '%');
			});
		}


		$(window).resize(function() {

			if ($(window).width() > 752 && numberInRow != 3) {
				numberInRow = 3;
				setSiderItems();
			} else if ($(window).width() >= 400 && $(window).width() <= 752 && numberInRow != 1) {
				numberInRow = 1;
				setSiderItems();
			} else if ($(window).width() < 400 && numberInRow != 0) {
				numberInRow = 0;
				setSiderItems();
			}
		}).resize();
	}
}

    var order = {
        init: function () {
            var $mainPanel = $('.js-order-form');
            if ($mainPanel.length != 0) {
                $mainPanel.find('.js-phone').mask('+7 (000) 0000-0000', {
                    placeholder: "+7 (___) ____-____"
                });
            }

			// default check agree on registrate
			$('#registrate, #registermodal').each(function(){
				$(this).find('[name="agree_fake"]').trigger('click');
				$(this).find('[name="data[new][agree]"]').val(true);
			})


            var $mainPanel = $('.js-user-profile-form');
            if ($mainPanel.length != 0) {
				// change phone on country change
                $mainPanel.find('#country554,#country').change(function () {
                    var $option = $(this).find('option:selected');
                    $mainPanel.find('.js-phone').unmask().attr('placeholder', '');
                    if ($option.data('mask')) {
                        var mask_option = {placeholder: $option.data('mask-placeholder')};

                        //Азербайджан
                        if ($option.val() == 14532) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    if (cep.length > 1) {
                                        field.val('+9' + cep.substring(2));
                                    }
                                    if (cep.length > 2) {
                                        field.val('+99' + cep.substring(3));
                                    }
                                    if (cep.length > 3) {
                                        field.val('+994' + cep.substring(4));
                                    }
                                },
                                placeholder: $option.data('mask-placeholder')
                            };
                        }
                        //Армения
                        // if($option.val() == 14533){
                        // mask_option={onKeyPress: function(cep, e, field, options){
                        // var tmp_mask2 ='+374 (00) 0000000';
                        // if(cep.length>12){
                        // tmp_mask2 ='+374 (000) 0000000';
                        // }
                        // field.unmask();
                        // field.mask(tmp_mask2, options);
//
                        // },
                        // placeholder: $option.data('mask-placeholder')}
                        // }
                        //Грузия
                        if ($option.val() == 14535) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    if (cep.length > 1) {
                                        field.val('+9' + cep.substring(2));
                                    }
                                    if (cep.length > 2) {
                                        field.val('+99' + cep.substring(3));
                                    }
                                    if (cep.length > 3) {
                                        field.val('+995' + cep.substring(4));
                                    }
                                },
                                placeholder: $option.data('mask-placeholder')
                            };
                        }
                        //Украина
                        if ($option.val() == 14536) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    if (cep.length > 1) {
                                        field.val('+3' + cep.substring(2));
                                    }
                                    if (cep.length > 2) {
                                        field.val('+38' + cep.substring(3));
                                    }
                                    if (cep.length > 3) {
                                        field.val('+380' + cep.substring(4));
                                    }
                                },
                                placeholder: $option.data('mask-placeholder')
                            };
                        }
                        //Узбекистан
                        if ($option.val() == 2718540) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    if (cep.length > 1) {
                                        field.val('+9' + cep.substring(2));
                                    }
                                    if (cep.length > 2) {
                                        field.val('+99' + cep.substring(3));
                                    }
                                    if (cep.length > 3) {
                                        field.val('+998' + cep.substring(4));
                                    }
                                },
                                placeholder: $option.data('mask-placeholder')
                            };
                        }
                        //Киргизия
                        if ($option.val() == 2718542) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    if (cep.length > 1) {
                                        field.val('+9' + cep.substring(2));
                                    }
                                    if (cep.length > 2) {
                                        field.val('+99' + cep.substring(3));
                                    }
                                    if (cep.length > 3) {
                                        field.val('+996' + cep.substring(4));
                                    }
                                },
                                placeholder: $option.data('mask-placeholder')
                            };
                        }
                        //Другое
                        if ($option.val() == 14538) {
                            mask_option = {
                                onKeyPress: function (cep, e, field, options) {
                                    tmp_mask = '000-0000';
                                    if (cep.length > 7) {
                                        tmp_mask = '+0 (00) 000-0000';
                                    }
                                    if (cep.length > 10) {
                                        tmp_mask = '+0 (000) 000-0000';
                                    }
                                    if (cep.length > 11) {
                                        tmp_mask = '+000 (000) 000-0000';
                                    }
                                    field.mask(tmp_mask, options);

                                },
                                placeholder: $option.data('mask-placeholder'),
                                reverse: true
                            };
                        }
                        var Minlength = $option.data('mask').length;
                        setTimeout(function () {
                            $mainPanel.find('.js-phone').addClass('show-field-placeholder').mask($option.data('mask'), mask_option).attr('data-val-length-min', Minlength).rules('add', {minlength: Minlength,});
                        }, 0);
                        // $mainPanel.find('.js-phone').mask($option.data('mask'), { placeholder: $option.data('mask-placeholder') });
                    } else {
                        setTimeout(function () {
                            $mainPanel.find('.js-phone').removeClass('show-field-placeholder').attr('placeholder', 'Мобильный телефон*').attr('data-val-length-min', 11).rules('add', {minlength: 11});
                        }, 0);
                    }
                }).change();
                //$mainPanel.find('#clinic_phone').mask($option.data('mask'), { placeholder: $option.data('mask-placeholder') });
            }
        }
    }

var inputSpinner = {
	init : function() {
		$('.spinner.forBuy').each(function() {
			var $pnl = $(this);
			if ($pnl.find('input:enabled').length != 0) {
				$pnl.find('input').change(function() {
					if (isNaN($(this).val()) || parseInt($(this).val()) <= 0)
						$(this).val(1);
					else
						$(this).val(parseInt($(this).val()));
				});
				$pnl.find('.btn:first-of-type').on('click', function() {
					$pnl.find('input').val(parseInt($pnl.find('input').val()) + 1);
				});
				$pnl.find('.btn:last-of-type').on('click', function() {
					if (parseInt($pnl.find('input').val()) > 1) {
						$pnl.find('input').val(parseInt($pnl.find('input').val()) - 1);
					} else
						$pnl.find('input').val(1);
				});
			}
		});
	}
}

var initPhotoSwipeFromDOM = function(gallerySelector) {

	//add figure html tag from img alt tag
	$(gallerySelector).find('a').each(function() {
		var $lnk = $(this);
		var $img = $lnk.find('img:first');
		if ($lnk.find('figure').length == 0 && $img.length != 0) {
			var $figure = $('<figure/>').prependTo($lnk);
			if ($img.attr('alt'))
				$figure.text($img.attr('alt'));
		} else {
			var $figure = $('<figure/>').appendTo($lnk);
			if ($lnk.attr('title'))
				$figure.text($lnk.attr('title'));
		}
	});

	var parseThumbnailElements = function(el) {
		var thumbElements = $(el).find('a.js-photo').toArray(),
		    numNodes = thumbElements.length,
		    items = [],
		    el,
		    childElements,
		    thumbnailEl,
		    size,
		    item;

		for (var i = 0; i < numNodes; i++) {
			el = thumbElements[i];

			// include only element nodes
			if (el.nodeType !== 1) {
				continue;
			}

			childElements = el.children;

			size = el.getAttribute('data-size').split('x');

			// create slide object
			item = {
				src : el.getAttribute('href'),
				w : parseInt(size[0], 10),
				h : parseInt(size[1], 10),
				author : '',
			};

			item.el = el;
			// save link to element for getThumbBoundsFn

			if (childElements.length > 0) {
				item.msrc = childElements[0].getAttribute('src');
				// thumbnail url
				item.title = childElements[0].innerHTML;

			}

			var mediumSrc = el.getAttribute('data-med');
			if (mediumSrc) {
				size = el.getAttribute('data-med-size').split('x');
				// "medium-sized" image
				item.m = {
					src : mediumSrc,
					w : parseInt(size[0], 10),
					h : parseInt(size[1], 10)
				};
			}
			// original image
			item.o = {
				src : item.src,
				w : item.w,
				h : item.h
			};

			items.push(item);
		}

		return items;
	};

	var closest = function closest(el, fn) {
		return el && (fn(el) ? el : closest(el.parentNode, fn));
	};

	var onThumbnailsClick = function(e) {
		e = e || window.event;
		e.preventDefault ? e.preventDefault() : e.returnValue = false;

		var eTarget = e.target || e.srcElement;

		var clickedListItem = closest(eTarget, function(el) {
			return el.tagName === 'A';
		});

		if (!clickedListItem) {
			return;
		}

		var clickedGallery = $(clickedListItem).parents('.js-photos:first')[0];

		var childNodes = $(clickedGallery).find('a').toArray(),
		    numChildNodes = childNodes.length,
		    nodeIndex = 0,
		    index;

		for (var i = 0; i < numChildNodes; i++) {
			if (childNodes[i].nodeType !== 1) {
				continue;
			}

			if (childNodes[i] === clickedListItem) {
				index = nodeIndex;
				break;
			}
			nodeIndex++;
		}

		if (index >= 0) {
			openPhotoSwipe(index, clickedGallery);
		}
		return false;
	};

	var photoswipeParseHash = function() {
		var hash = window.location.hash.substring(1),
		    params = {};

		if (hash.length < 5) {// pid=1
			return params;
		}

		var vars = hash.split('&');
		for (var i = 0; i < vars.length; i++) {
			if (!vars[i]) {
				continue;
			}
			var pair = vars[i].split('=');
			if (pair.length < 2) {
				continue;
			}
			params[pair[0]] = pair[1];
		}

		if (params.gid) {
			params.gid = parseInt(params.gid, 10);
		}

		return params;
	};

	var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
		var pswpElement = document.querySelectorAll('.pswp')[0],
		    gallery,
		    options,
		    items;

		items = parseThumbnailElements(galleryElement);

		options = {

			galleryUID : galleryElement.getAttribute('data-pswp-uid'),

			getThumbBoundsFn : function(index) {
				// See Options->getThumbBoundsFn section of docs for more info
				var thumbnail = items[index].el.children[0],
				    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
				    rect = thumbnail.getBoundingClientRect();

				return {
					x : rect.left,
					y : rect.top + pageYScroll,
					w : rect.width
				};
			},

			addCaptionHTMLFn : function(item, captionEl, isFake) {
				if (!item.title) {
					captionEl.children[0].innerText = '';
					return false;
				}
				captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
				return true;
			}
		};

		if (fromURL) {
			if (options.galleryPIDs) {
				for (var j = 0; j < items.length; j++) {
					if (items[j].pid == index) {
						options.index = j;
						break;
					}
				}
			} else {
				options.index = parseInt(index, 10) - 1;
			}
		} else {
			options.index = parseInt(index, 10);
		}

		// exit if index not found
		if (isNaN(options.index)) {
			return;
		}

		var radios = document.getElementsByName('gallery-style');
		for (var i = 0,
		    length = radios.length; i < length; i++) {
			if (radios[i].checked) {
				if (radios[i].id == 'radio-all-controls') {

				} else if (radios[i].id == 'radio-minimal-black') {
					options.mainClass = 'pswp--minimal--dark';
					options.barsSize = {
						top : 0,
						bottom : 0
					};
					options.captionEl = false;
					options.fullscreenEl = false;
					options.shareEl = false;
					options.bgOpacity = 0.85;
					options.tapToClose = true;
					options.tapToToggleControls = false;
				}
				break;
			}
		}

		if (disableAnimation) {
			options.showAnimationDuration = 0;
		}

		gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

		var realViewportWidth,
		    firstResize = true;

		gallery.listen('beforeResize', function() {

			var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
			dpiRatio = Math.min(dpiRatio, 2.5);
			realViewportWidth = gallery.viewportSize.x * dpiRatio;

			if (!firstResize) {
				gallery.invalidateCurrItems();
			}

			if (firstResize) {
				firstResize = false;
			}
		});

		gallery.listen('gettingData', function(index, item) {
			item.src = item.o.src;
			item.w = item.o.w;
			item.h = item.o.h;
		});

		gallery.init();
	};

	var galleryElements = document.querySelectorAll(gallerySelector);
	for (var i = 0,
	    l = galleryElements.length; i < l; i++) {
		galleryElements[i].setAttribute('data-pswp-uid', i + 1);
		galleryElements[i].onclick = onThumbnailsClick;
	}

	var hashData = photoswipeParseHash();
	if (hashData.pid && hashData.gid) {
		openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
	}
};

var dealers = {
$map_canvas: null,
$delears_tables: null,
$ddl_cities: null,
$ddl_coutries: null,
dealers_data: [],
all_cities_value: "",
google_map: null,
init_controls: function () {
this.$map_canvas = $('.js-map-canvas');
if (this.$map_canvas.length != 0) {
var mapOptions = {
center: new google.maps.LatLng(-34.397, 150.644),
zoom: 8,
scrollwheel: false,
mapTypeId: google.maps.MapTypeId.ROADMAP
};
this.google_map = new google.maps.Map(this.$map_canvas[0], mapOptions);
this.$delears_tables = $('.js-dealers-table tbody');
this.$ddl_cities = $('.js-cities').change(function () {
dealers.set_dealers_data();
});
this.$ddl_coutries = $('.js-coutries').change(function () {
dealers.set_cities_ddl();
dealers.set_dealers_data();
});
this.all_cities_value = this.$ddl_cities.val();
this.load_dealers_data();
}
},
load_dealers_data: function () {
$.ajax({
type: "GET",
url: "/udata/partners/jsonlist/237//10000/1",
//url: "dealers.json",
dataType: "json",
contentType: "application/json; charset=utf-8",
processData: false,
success: function (data) {
dealers.dealers_data = data;
dealers.set_coutries_ddl();
dealers.set_cities_ddl(true);
dealers.set_dealers_data();
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
alert("Status: " + textStatus); alert("Error: " + errorThrown);
}
})
},
set_coutries_ddl: function () {
var coutries = {};
for (var i = 0; i < this.dealers_data.length; i++) {
var item = this.dealers_data[i];
if (!coutries.hasOwnProperty(item.country)) {
coutries[item.country] = 1;
$("<option></option>").appendTo(this.$ddl_coutries).text(item.country).val(item.country);
}
}
},
set_cities_ddl: function (first) {
	var coutries = {};
	var selected_country = this.$ddl_coutries.val();
	this.$ddl_cities.empty();
	$("<option></option>").appendTo(this.$ddl_cities).text(this.all_cities_value).val(this.all_cities_value);
	var cities = [];
	for (var i = 0; i < this.dealers_data.length; i++) {
		var item = this.dealers_data[i];
		if ((!coutries.hasOwnProperty(item.city) && selected_country == item.country) || first) {
			coutries[item.city] = 1;
            cities.push(item.city);
		}
	}
    cities = cities.filter(function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    });
	cities.sort();
	for (var i = 0; i < cities.length; i++) {
		$("<option></option>").appendTo(this.$ddl_cities).text(cities[i]).val(cities[i]);
	}

	this.$ddl_cities.selectpicker('refresh');
}, set_dealers_data: function () {
	var count_item = 0;
	var last_location = null;
	var map_bounds = new google.maps.LatLngBounds();
	this.$delears_tables.empty();
	var selected_country = this.$ddl_coutries.val();
	var selected_city = this.$ddl_cities.val();
	var selected_country_index = this.$ddl_coutries.children().index(this.$ddl_coutries.find("option:selected"));
	var selected_city_index = this.$ddl_cities.children().index(this.$ddl_cities.find("option:selected"));
	for (var i = 0; i < this.dealers_data.length; i++) {
		var item = this.dealers_data[i];
		if ((item.city == selected_city || selected_city_index == 0) && (item.country == selected_country || selected_country_index == 0)) {
			var $row = $("<tr></tr>").appendTo(this.$delears_tables);
			$("<td></td>").appendTo($row).text(item.country);
			//$("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.webaddress).attr("target", "_blank").attr("rel", "nofollow").text(item.name);
			if(typeof item.webaddress != 'undefined'){
            	$("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.webaddress).attr("target", "_blank").attr("rel", "nofollow").text(item.name);
            }else{
            	$("<td></td>").appendTo($row).text(item.name);
            }
			$("<td></td>").appendTo($row).text(item.address);
			$("<td></td>").appendTo($row).html(item.phone.split(',').join('<br/>'));
			console.log('item.email');
			$("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", 'mailto:' + item.email).text(item.email);

			var location = new google.maps.LatLng(item.lat, item.lng);
			last_location = location;
			if (!item.marker) {
				item.marker = this.create_dealer_marker(item, location);
			}
			item.marker.setMap(this.google_map);
			map_bounds.extend(location);
			count_item++;
		} else if (item.marker) {
			item.marker.setMap(null);
		}
	}

	if (count_item == 1) {
		this.google_map.setOptions({
			center : last_location,
			zoom : 10
		});
	} else {
		this.google_map.fitBounds(map_bounds);
	}

}, create_dealer_marker: function (dealer, location) {
	var marker = new google.maps.Marker({
		position : location,
		title : dealer.name,
		icon : '/templates/ormco/img/marker-icon.png'
	});
	var infowindow = new google.maps.InfoWindow({
		content : '<b>' + dealer.name + '</b><br/>' + dealer.address + '<br/>' + dealer.phone
	});
	google.maps.event.addListener(marker, 'mouseover', function() {
		infowindow.open(dealers.google_map, marker);
	});
	google.maps.event.addListener(marker, 'mouseout', function() {
		infowindow.close();
	});
	return marker;
}
}

var choiceBrackets = {
	init : function() {
		var $pnl = $('.js-choice-brackets');
		if ($pnl.length != 0) {
            function resetValues() {
                $pnl.find('.paz_table:not(.hide_it) .js-bottom-numbers input:enabled,.paz_table:not(.hide_it) .js-top-numbers input:enabled').val('');
                $pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select').each(function(){
                	$(this).val($('option:eq(0)', $(this)).attr('value'));
                	$(this).selectpicker('refresh');
                })
            }

            function commonSum() {
                var sum = 0;
                $pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select').each(function(){
                	var tooth = $(this).parents('td').data('tooth'),
            			amount = $('.paz_table:not(.hide_it) .js-numbers td[data-tooth="'+tooth+'"] input').val(),
                		price = $('option:selected',this).data('price');
                	sum = (parseFloat(price) > 0 && parseInt(amount) > 0) ? sum + (parseInt(amount) * parseFloat(price)) : sum;
                	//console.log(sum);
                })
                sum = (parseFloat(sum) > 0) ? parseFloat(sum.toFixed(2)) + ' руб.' : '-';
                $('.bracket_price_sum').text(sum);
                tb_flag = false;
                t_flag = false;
                b_flag = false;

				try{
					console.log('ga braces choice');
					ga('send', 'event', 'braces choice');
					ym(10184890, 'reachGoal', 'BracesChoice');
				} catch (err){
					console.log(err);
				}
            }

            $pnl.find('.js-select-top-bottom').click(function () {
                resetValues();
                $pnl.find('.paz_table:not(.hide_it) .js-bottom-numbers input:enabled, .paz_table:not(.hide_it) .js-top-numbers input:enabled').each(function(){
                	var amount = $(this).val();
                	if(!(parseInt(amount) > 0)){
                		$(this).val(1);
                	}
                });
                //$pnl.find('.paz_table:not(.hide_it) .js-bottom-torc select:enabled, .paz_table:not(.hide_it) .js-top-torc select:enabled').val('ст.').selectpicker('refresh');
            	$pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select').each(function(){
            		choiceBrackets.selectStandard($(this));

                	$(this).selectpicker('refresh');
                })
                commonSum();
                tb_flag = true;
            });

            $pnl.find('.js-buy-top-bottom').click(function () {
                resetValues();
                $pnl.find('.paz_table:not(.hide_it) .js-bottom-numbers input:enabled, .paz_table:not(.hide_it) .js-top-numbers input:enabled').each(function(){
                	var amount = $(this).val();
                	if(!(parseInt(amount) > 0)){
                		$(this).val(1);
                	}
                });
                //$pnl.find('.paz_table:not(.hide_it) .js-bottom-torc select:enabled, .paz_table:not(.hide_it) .js-top-torc select:enabled').val('ст.').selectpicker('refresh');
            	$pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select').each(function(){
                	choiceBrackets.selectStandard($(this));

                	$(this).selectpicker('refresh');
                })
                commonSum();
                tb_flag = true;
                $('.btn-primary.btn-buy.main').click();
            });

			$pnl.find('.js-buy-paz').click(function () {
				resetValues();

				$('#slot').val($(this).data('id')).trigger('change');

				$pnl.find('.paz_table:not(.hide_it) .js-bottom-numbers input:enabled, .paz_table:not(.hide_it) .js-top-numbers input:enabled').each(function(){
					var amount = $(this).val();
					if(!(parseInt(amount) > 0)){
						$(this).val(1);
					}
				});
				//$pnl.find('.paz_table:not(.hide_it) .js-bottom-torc select:enabled, .paz_table:not(.hide_it) .js-top-torc select:enabled').val('ст.').selectpicker('refresh');
				$pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select').each(function(){
					choiceBrackets.selectStandard($(this));

					$(this).selectpicker('refresh');
				})
				commonSum();

				tb_flag = true;
				$('.btn-primary.btn-buy.main').click();
			});

            $pnl.find('.js-select-top').click(function () {
                resetValues();
                $pnl.find('.paz_table:not(.hide_it) .js-top-numbers input:enabled').each(function(){
                	var amount = $(this).val();
                	if(!(parseInt(amount) > 0)){
                		$(this).val(1);
                	}
                });
                $pnl.find('.paz_table:not(.hide_it) .js-top-torc .tork_selector:not(.hide_it) select').each(function(){
                	choiceBrackets.selectStandard($(this));

                	$(this).selectpicker('refresh');
                })
                commonSum();
                t_flag = true;

            });

            $pnl.find('.js-select-bottom').click(function () {
                resetValues();
                $pnl.find('.paz_table:not(.hide_it) .js-bottom-numbers input:enabled').each(function(){
                	var amount = $(this).val();
                	if(!(parseInt(amount) > 0)){
                		$(this).val(1);
                	}
                });
                //$pnl.find('.js-bottom-torc select:enabled').val('ст.').selectpicker('refresh');
                $pnl.find('.paz_table:not(.hide_it) .js-bottom-torc .tork_selector:not(.hide_it) select').each(function(){
                	choiceBrackets.selectStandard($(this));

                	$(this).selectpicker('refresh');
                })
                commonSum();
                b_flag = true;

            });
        }

        $('.btn-buy.main').click(function() {
            var ids = '';
            var is_error = false;
            var error_numbers_list = new Array();
            $pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select option:selected').each(function () {
                var tork = $(this).val();
                var count = $('.paz_table:not(.hide_it) .js-numbers').find('td[data-tooth = ' + $(this).closest('td').data('tooth') + '] input').val();

                if(tork == '' && count != '' && count != '0'){
                    error_numbers_list.push($(this).closest('td').data('tooth'));
                    is_error = true;
                }
            });

            if(is_error){
//                    $([document.documentElement, document.body]).animate({
//                        scrollTop: $(this).closest('td').offset().top - 120
//                    }, 1000);
                if(error_numbers_list.length > 1){
                    $('#empty_tork_modal_content').html('Пожалуйста, выберите торк для зубов ' + error_numbers_list.join(', '));
                }else{
                    $('#empty_tork_modal_content').html('Пожалуйста, выберите торк для зуба ' + error_numbers_list.join(', '));
                }
                $('.empty_tork_modal_start').click();
                return false;
            }

            $pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select option:selected:not([value = ""])').each(function () {
                var id = $(this).val(),
                    amount = 0,
                    tooth = $(this).parents('td').data('tooth'),
                    amount = $('.paz_table:not(.hide_it) .js-numbers td[data-tooth="' + tooth + '"] input').val();
                //console.log($('.js-numbers td[data-tooth="'+tooth+'"] input'));
                //console.log(id);
                //console.log(amount);
                //console.log('====');
                if (parseInt(amount) > 0) {
                    var common_amount = 0;
                    //if($pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select:enabled option:selected[value = "'+id+'"]').length > 1){
                    if ($pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select option:selected[value = "' + id + '"]').length > 1) {
                        //$pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select:enabled option:selected[value = "'+id+'"]').each(function(){
                        $pnl.find('.paz_table:not(.hide_it) .js-torc .tork_selector:not(.hide_it) select option:selected[value = "' + id + '"]').each(function () {
                            var new_tooth = $(this).parents('td').data('tooth'),
                                new_amount = $('.paz_table:not(.hide_it) .js-numbers td[data-tooth="' + new_tooth + '"] input').val();
                            if (parseInt(new_amount) > 0) {
                                common_amount = common_amount + parseInt(new_amount);
                            }
                        });
                    }
                    if (common_amount > 0) {
                        amount = common_amount;
                    }

                    separate = (ids != '') ? '_' : '';
                    ids = ids + separate + id + '|' + amount;
                }
            });

            //console.log('----');
            //console.log(ids);
            if(ids === ''){
                alert('Выберите все необходимые данные для добавления в корзину');
                return false;
            }

            site.basket.addSeveralBrackets(ids,$(this));

			try{
				//ga events
				if(tb_flag){
					console.log('ga both jaws');
					ga('send', 'pageview', '/virtual/addtoshoppingcart');
					ga('send', 'event', 'ET', 'add to cart', 'both jaws');
					ym(10184890, 'reachGoal', 'AddToShoppingcart');
				}else if(t_flag){
					console.log('ga upjaw');
					ga('send', 'pageview', '/virtual/addtoshoppingcart');
					ga('send', 'event', 'ET', 'add to cart', 'upjaw');
					ym(10184890, 'reachGoal', 'AddToShoppingcart');
				}else if(b_flag){
					console.log('ga downjaw');
					ga('send', 'pageview', '/virtual/addtoshoppingcart');
					ga('send', 'event', 'ET', 'add to cart', 'downjaw');
					ym(10184890, 'reachGoal', 'AddToShoppingcart');
				}else{
					console.log('ga selected');
					ga('send', 'pageview', '/virtual/addtoshoppingcart');
					ga('send', 'event', 'ET', 'add to cart', 'selected');
					ym(10184890, 'reachGoal', 'AddToShoppingcart');
				}
				//\ga events
			}catch (err){
				console.log(err);
			}

            return false;
        });

        // смена торков при выборе крючка
        $('.js-hk .hk').change(function(){
        	console.log('change hk');
        	//вывести другой набор торков
        	// выяснить для какого зуба меняем торки и сменить их
        	var tooth = $(this).parents('td').data('tooth'),
        		hk_checked = ($(this).prop('checked')) ? 1 : 0,
        		go_checked = ($('.paz_table:not(.hide_it) .js-go td[data-tooth="'+tooth+'"] input').prop('checked')) ? 1 : 0,
        		tork_container = $('.paz_table:not(.hide_it) .js-torc td[data-tooth="'+tooth+'"]'),
        		curr_selected_option_text = $('.tork_selector:not(.hide_it) select option:selected',tork_container).text(),
        		curr_selected_option_value = '',
        		select_container = $('.hk'+hk_checked+'.go'+go_checked ,tork_container);

        	select_container.find('select option').each(function(){
        		if($(this).text() == curr_selected_option_text){
        			curr_selected_option_value = $(this).attr('value');
        		}
        	});
        	select_container.find('select').val(curr_selected_option_value).selectpicker('refresh');

        	var selected_option = $('.selectpicker option:selected', select_container),
        		price = (selected_option.data('price')) ? selected_option.data('price') + ' руб.' : '-';

    		$('.tork_selector' ,tork_container).addClass('hide_it');
    		select_container.removeClass('hide_it');
    		//$('.bracket_price').text(price);

    		commonSum();
		});

		$('.js-go .go').change(function(){
        	console.log('change go');
        	//вывести другой набор торков
        	// выяснить для какого зуба меняем торки и сменить их
        	var tooth = $(this).parents('td').data('tooth'),
        		go_checked = ($(this).prop('checked')) ? 1 : 0,
        		hk_checked = ($('.paz_table:not(.hide_it) .js-hk td[data-tooth="'+tooth+'"] input').prop('checked')) ? 1 : 0,
        		tork_container = $('.paz_table:not(.hide_it) .js-torc td[data-tooth="'+tooth+'"]'),
        		curr_selected_option_text = $('.tork_selector:not(.hide_it) select option:selected',tork_container).text(),
        		curr_selected_option_value = '',
        		select_container = $('.hk'+hk_checked+'.go'+go_checked ,tork_container);

        	select_container.find('select option').each(function(){
        		if($(this).text() == curr_selected_option_text){
        			curr_selected_option_value = $(this).attr('value');
        		}
        	});
        	select_container.find('select').val(curr_selected_option_value).selectpicker('refresh');

        	var selected_option = $('.selectpicker option:selected', select_container),
        		price = (selected_option.data('price')) ? selected_option.data('price') + ' руб.' : '-';

    		$('.tork_selector' ,tork_container).addClass('hide_it');
    		select_container.removeClass('hide_it');
    		//$('.bracket_price').text(price);

    		commonSum();
		});

        $('#slot').change(function(){
        	var paz = $('option:selected', this).val();

        	$('.paz_table').addClass('hide_it');
        	$('#paz_'+paz).removeClass('hide_it');

        	$('.complects').addClass('hide_it');
        	$('#complects_paz_'+paz).removeClass('hide_it');

        	commonSum();
        });
        /*$('#slot.one_paz').on('mousedown', function(e) {
		   e.preventDefault();
		   this.blur();
		   window.focus();
		});*/

        // смена торков
        $('.tork_selector .selectpicker').change(function(){
        	var tooth = $(this).parents('td').data('tooth'),
        		number_input = $pnl.find('.paz_table:not(.hide_it) .js-numbers td[data-tooth="'+tooth+'"] input:enabled'),
        		price = ($('option:selected', this).data('price'))? $('option:selected', this).data('price') + ' руб.' : '-';
        	if(number_input.val() == ''){
        		number_input.val(1);
        	}
            //$(this).selectpicker('refresh');
        	//$('.bracket_price').text(price);

        	commonSum();
        });

        // изменение кол-ва товара
        $(".paz_table:not(.hide_it) .js-numbers td input").bind('keyup mouseup', function () {
		    commonSum();
		});
	},

	selectStandard: function($select) {
		let $option = $('option', $select).filter(function() { return $(this).html() === 'ст.'});

		if(!$option.length) {
			$option = $('option:eq(1)', $select);
		}

		$select.val($option.attr('value'));
	}
}

var productFilter = {
	init : function() {
		var filters = [];
		var updater_started = false;

		var $filterPnl = $('.js-filter');
		var $filterResults = $('.js-filter-results');

		if ($filterPnl.length != 0) {
			/*$filterPnl.find('input[type="checkbox"]').change(function () {
			 var $ul = $(this).parents('ul');
			 $(this).parents('li').attr('class', $(this).is(':checked') ? 'checked' : '');
			 if ($ul.find('input:checked').length > 0)
			 $ul.prev().addClass('selected');
			 else
			 $ul.prev().removeClass('selected');
			 });*/
			$filterPnl.find('.close').click(function() {
				$(this).parent().removeClass('selected').next().find('input').attr('checked', false).removeClass('selected').parents('li').removeClass('checked');


				updateFilterFields();
				return false;
			});
			$filterResults.click(function() {
				$('html,body').animate({
					scrollTop : $('.js-products-list').offset().top
				}, 'slow');
				return false;
			});

			// пагинация через ajax (только для страниц с фильтром)
			if($('#showFilters').length > 0){
				jQuery('body').on('click','.objects_list .pagination a', function(){
					var pagenum = jQuery(this).attr('pagenum');
					//console.log(pagenum);
					//console.log('/udata/catalog/getSmartCatalog//' + $('#showFilters').data('category') + '/?' + getFilterString() + '&p=' + pagenum + '&extProps=price,common_quantity,artikul,photo&transform=modules/catalog/blocks/ajax_objects.xsl');
					//return false;
					$.ajax({
						url : '/udata/catalog/getSmartCatalog//' + $('#showFilters').data('category') + '////common_quantity_flag/0?' + getFilterString() + '&p=' + pagenum + '&extProps=price,common_quantity,artikul,photo&transform=modules/catalog/blocks/ajax_objects.xsl',
						dataType : 'html',
						success : function(data) {
							//console.log(data);
							$('.objects_list').html(data);
							inputSpinner.init();
						},
					});
					return false;
				});
			}

			$('.filters form').submit(function(){
				//console.log('ggg');
				jQuery('#filteringModal').modal('show');

				updateObjectsList();

				setTimeout(function() {
					jQuery('#filteringModal').modal('hide');
				jQuery('body.modal-open').attr('class','').attr('style','');
				}, 1000);


				return false;
			})

			function checkPosition() {
				if ($(window).width() < 977) {
					var top = $(window).scrollTop() + $(window).height() - 100;
					if ($filterPnl.offset().top < top && $filterPnl.offset().top + $filterPnl.height() > top)
						$filterResults.show();
					else
						$filterResults.hide();
				} else
					$filterResults.hide();
			}


			$(window).resize(function() {
				checkPosition();
			}).resize();

			$(window).scroll(function() {
				checkPosition();
			});


			// show or hide filters
			$('.filters ul li input[type="checkbox"]').change(function(event) {
				event.preventDefault();

				if ($(this).parents('li').hasClass('disabled')) {
					return false;
				}

				var $ul = $(this).parents('ul');
				$(this).parents('li').attr('class', $(this).is(':checked') ? 'checked' : '');
				if ($ul.find('input:checked').length > 0)
					$ul.prev().addClass('selected');
				else
					$ul.prev().removeClass('selected');

				$(this).toggleClass('selected');
				updateFilterFields();
			});

			function updateFilterFields() {
				filters = [];
				$('.filter_values.selected').each(function() {
					filters.push({
						'name' : $(this).data('name'),
						'value' : $(this).data('value')
					});
				});

				// filters.push({'name' : $("#slider-range").data('name') + '[from]', 'value' : $("#slider-range").data('current-min')});
				//filters.push({'name' : $("#slider-range").data('name') + '[to]', 'value' : $("#slider-range").data('current-max')});

				$('.filter_values').parents('li').removeClass('checked');
				$('.filter_values.selected').parents('li').addClass('checked');
				//console.log(filters);

				updater_started = false;

				updateFilterList();
			}

			// отключаем незадействованные поля
			function updateFilterList() {
				$.ajax({
					url : '/udata/catalog/getSmartFilters//' + $('#showFilters').data('category') + '/1/5/.json?' + getFilterString(),
					dataType : 'json',
					success : function(data) {
						$('.filter_values:not(.selected)').attr('disabled',true).addClass('disabled').parents('li').addClass('disabled');
						// перебираем все группы
						for (var i in data.group) {
							for (var j in data.group[i].field) {
								for (var k in data.group[i].field[j].item) {
									var $curr_input = $('.filter_values.filter_system_name_' + data.group[i].field[j].name + '[data-value = "' + data.group[i].field[j].item[k].value + '"]'),
										$ul = $curr_input.parents('ul');

									$curr_input.attr('disabled',false).removeClass('disabled').removeClass('checked').parents('li').removeClass('disabled');

									if ($ul.find('input:checked').length > 0)
										$ul.prev().addClass('selected');
									else
										$ul.prev().removeClass('selected');

								}
							}
						}
					},
				});
			}

			// обновление спискат оваров
			function updateObjectsList() {
				$.ajax({
					url : '/udata/catalog/getSmartCatalog//' + $('#showFilters').data('category') + '////common_quantity_flag/0?' + getFilterString() + '&extProps=price,common_quantity,artikul,photo&transform=modules/catalog/blocks/ajax_objects.xsl',
					dataType : 'html',
					success : function(data) {
						//console.log(data);
						$('.objects_list').html(data);
						inputSpinner.init();
					},
				});
			}

			function getFilterString() {
				var result = '';
				for (var i = 0; i < filters.length; i++) {
					result += '&' + filters[i].name + '=' + filters[i].value;
				}

				//            console.log(result);
				return result;
			}


		}

	}
}

// var collapseService = {
	// init : function() {
		// $('.js-collapse-pnl').on('hide.bs.collapse', function() {
			// $('html,body').animate({
				// scrollTop : $(this).offset().top - 100
			// }, 600);
		// });
	// }
// }
var collapseService = {
    init: function () {
        $('.js-collapse-pnl').on('hide.bs.collapse', function () {
        		console.log('hide.bs.collapse');
            $('[data-show-when-expanded][data-target="#' + $(this).attr('id') + '"]').hide();

            $('html,body').animate({
                scrollTop: $(this).offset().top - 100
            }, 600);
        });

        $('.js-collapse-pnl').on('show.bs.collapse', function () {
        		console.log('show.bs.collapse');
            $('[data-show-when-expanded][data-target="#' + $(this).attr('id') + '"]').show();
        });
    }
}

var googleMap = {
	init : function() {
		$('[data-lat]').each(function() {
			var lat = $(this).data('lat');
			var lng = $(this).data('lng');
			var zoom = $(this).data('zoom');
			var description = $(this).find('.js-description').html();
			var mapOptions = {
				center : new google.maps.LatLng(lat, lng),
				zoom : zoom,
				scrollwheel : false,
				mapTypeId : google.maps.MapTypeId.ROADMAP
			};
			var googleMap = new google.maps.Map($(this)[0], mapOptions);

			var marker = new google.maps.Marker({
				position : new google.maps.LatLng(lat, lng),
				title : $(this).data('title'),
				icon : '/templates/ormco/img/marker-icon.png'
			});
			var infowindow = new google.maps.InfoWindow({
				content : description
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(googleMap, marker);
			});

			marker.setMap(googleMap);
		})
	}
}

$('[data-ride="carousel"]').carousel({
    swipe: 50
});

$(function() {
	$('[data-toggle="tooltip"]').tooltip();

	var is_touch_device = 'ontouchstart' in document.documentElement;
    if (!is_touch_device) {
        $('.js-privacy-lnk').tooltip({trigger: 'hover'});
    }

	$('.link_edit_legal').click(function(){
		var id = $(this).data('id');

		if(jQuery('#editLegal'+id+':visible').length > 0){
			jQuery('#editLegal'+id).hide();
		}else{
			jQuery(this).closest('table').find('.editLegal').hide();
			jQuery('#editLegal'+id).show();
		}
		return false;
	});

	$("#invoice input[name='legal-person']").change(function(){
		console.log($(this).val());
		var _val = $(this).val(),
			_form = $(this).parents('form'),
			_submit = $('button.send-btn');
		//formnovalidate name="cancel"
		if(_val == 'new'){
			_submit.removeAttr('formnovalidate','formnovalidate');
			_submit.removeAttr('name','cancel');
			$('#new-legal-person').removeClass('hide_it');
		}else{
			_submit.attr('formnovalidate','formnovalidate');
			_submit.attr('name','cancel');
			$('#new-legal-person').addClass('hide_it');
		}
	});

});

var webform = {
	captcha: function () {
		var captcha_reset = jQuery('.captcha_reset');
		captcha_reset.click(function () {
			var d = new Date();
			jQuery('.captcha_img', captcha_reset.parent()).attr('src', '/captcha.php?reset&' + d.getTime());
		});
	},
	send: function () {
		jQuery('.ajaxSend').submit(function (event) {
			var _this = $(this),
				_this_form_id = jQuery('input[name="system_form_id"]', _this).val(),
				form_modal = _this.closest('.register-modal.in');

			if (jQuery('input.course_apply', form_modal).data('from_course')) {
				jQuery('input#from_course').val(jQuery('input.course_apply', form_modal).data('from_course'));
			}

			jQuery('button[type="submit"]', _this).prop("disabled", true);

			jQuery.ajax({
				url: '/udata/webforms/checkdata/.json',
				dataType: 'json',
				async: false,
				data: _this.serialize(), //data: _this.serializeArray(),
				success: function (data) {
					console.log(data);
					if (data.status == 'error') {
						if (data.code == 'errorFields') {
							for (i in data.er) {
								fieldName = data.er[i];
//								console.log(jQuery('#' + fieldName + _this_form_id));
//								console.log(jQuery('#' + fieldName + _this_form_id + '_fake'));
								jQuery('#' + fieldName + _this_form_id + ', #' + fieldName + _this_form_id + '_fake').addClass('input-validation-error');
							}
						}
						if (data.code == 'errorFormId') {
							// не найдена форма по заданному type_id
						}
						jQuery('button[type="submit"]', _this).prop("disabled", false);
					} else {
						console.log('start send');

						jQuery.ajax({
							url: '/udata/' + _this.attr('action') + '.json',
							dataType: 'json',
							async: false,
							data: _this.serialize(),
							success: function (answer) {
                                if (answer.status) {
									if (answer.status == 'error') {
										if (answer.code == 'errorFields') {
											for (i in data.er) {
												fieldName = data.er[i];
//												console.log(jQuery('#' + fieldName + _this_form_id));
//												console.log(jQuery('#' + fieldName + _this_form_id + '_fake'));
												jQuery('#' + fieldName + _this_form_id + ', #' + fieldName + _this_form_id + '_fake').addClass('input-validation-error');
											}
										}
										jQuery('.waiting').hide();
									}else{
										_this.trigger('reset');
										jQuery('.waiting').hide();
										form_modal.modal('hide');
										jQuery('#formResultModal .modal-content .modal-body').html(answer.message);
										jQuery('#formResultModal').modal('show');
									}
                                }
							},
							error: function(data){
								console.log('error');
								console.log(data);
							}
						});
						console.log('finish send');
						$('.input-validation-error', _this).removeClass('input-validation-error');

						try {
							// seng ga event
							if (_this.attr('id') == 'contacts_form') {
								console.log('ga form#contacts_form');
								ga('send', 'event', 'form', 'feedback');
								ym(10184890, 'reachGoal', 'Feedback');
							}
							if (_this.parents('#feedbackModal').length > 0) {
								console.log('ga form#feedbackModal');
								ga('send', 'event', 'form', 'question');
								ym(10184890, 'reachGoal', 'QuestionSent');
							}
							// \seng ga event
						} catch (err) {
							console.log(err);
						}

//						jQuery.ajax({
//							url: '/udata/webforms/posted/' + _this_form_id + '.json',
//							dataType: 'json',
//							async: false,
//							success: function (answer) {
//								console.log('start posted');
//								console.log(answer);
//								if (answer.result) {
//									_this.trigger('reset');
//									form_modal.modal('hide');
//									jQuery('#formResultModal .modal-content .modal-body').html(answer.result);
//									jQuery('#formResultModal').modal('show');
//								}
//							}
//						});
						jQuery('button[type="submit"]', _this).prop("disabled", false);
					}
				}
			});
			event.preventDefault();
		});
	}
};

var downloadMaterials = {
    init: function () {
        var $pnl = $('.js-materials-list');
        //var links = [];

		var links = [];
		function downloadAll(urls) {
		  var link = document.createElement('a');

		  link.setAttribute('download', null);
		  link.style.display = 'none';

		  document.body.appendChild(link);

		  for (var i = 0; i < urls.length; i++) {

		  	//cound downloads
		  	var pid = urls[i][3];
		    if(typeof pid !== 'undefined'){
		    	var url = '/udata/materials/countDownloads/'+pid+'.json';
	            jQuery.ajax({
	               url: url,
	               dataType: 'json',
	               async: true
	         	});
		    }

         	//cound downloads
		  	link.setAttribute('download', urls[i][0]);
		    link.setAttribute('href', urls[i][1]);
		    var ext = urls[i][2];
		    ext = typeof ext !== 'undefined' ? ext : 'noext';
		    link.click();
			try{
				console.log('ga filesdownload '+ext);
				ga('send', 'event', 'click', 'filesdownload', ext);
				ym(10184890, 'reachGoal', 'FilesDownload');
			} catch (err) {
				console.log(err);
			}
		  }

		  document.body.removeChild(link);
		}

        if ($pnl.length != 0) {
            $pnl.find('.js-reset-selected').click(function () {
                $(this).parents('form').find('input[type="checkbox"]').attr('checked', false);
            });

            $pnl.find('.js-download-checked').click(function () {
                links = [];
                $(this).parents('form').find('input[type="checkbox"]:checked').each(function(){
                	var downloadbtn = $(this).parent().find('.download-btn'),
                		item = [$(downloadbtn).attr('download'), $(downloadbtn).attr('href'),$(downloadbtn).attr('ext'),$(downloadbtn).attr('pid')];
                	links.push(item);
                });
                downloadAll(links);
            });

            $pnl.find('.js-download-all').click(function () {
                links = [];
                $(this).parents('form').find('input[type="checkbox"]').each(function(){
                	var downloadbtn = $(this).parent().find('.download-btn'),
                		item = [$(downloadbtn).attr('download'), $(downloadbtn).attr('href'),$(downloadbtn).attr('ext'),$(downloadbtn).attr('pid')];
                	links.push(item);
                });
                downloadAll(links);
            });

            $pnl.find('.download-btn').click(function () {
                links = [];

            	var downloadbtn = $(this),
            		item = [$(downloadbtn).attr('download'), $(downloadbtn).attr('href'),$(downloadbtn).attr('ext'),$(downloadbtn).attr('pid')];
            	links.push(item);

                downloadAll(links);
                return false;
            });

            $pnl.find('.js-preview').click(function () {
                var ext = $(this).attr('ext');
			    ext = typeof ext !== 'undefined' ? ext : 'noext';

				try{
					console.log('ga filesdownload '+ext);
					ga('send', 'event', 'click', 'filesdownload', ext);
					ym(10184890, 'reachGoal', 'FilesDownload');
				} catch (err) {
					console.log(err);
				}
            });


            blueimp.Gallery.prototype.textFactory = function (obj, callback) {
                var $element = $('<div id="videoContent"></div>');
                var videoId = 'video_' + new Date().getTime();
                setTimeout(function () {
                    $('<video id ="' + videoId + '" style="margin:0px auto;" class="video-js vjs-default-skin" width="' + $(window).width() +
                        '" height="' + $(window).height() + '"' +
                        'data-setup=\'{"controls" : true, "autoplay" : true, "preload" : "auto"}\'>' +
                        '<source src="' + obj.href + '" type="' + (obj.href.endsWith('.mp4') ? 'video/mp4' : 'video/x-flv') + '">' +
                        '<p class="vjs-no-js">' +
                        'To view this video please enable JavaScript, and consider upgrading to a web browser that' +
                        '<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                        '</video>').appendTo($element);
                    videojs(videoId, {}, function () { });
                    callback({
                        type: 'load',
                        target: $element[0]
                    });
                }, 100);


                return $element[0];
            };
            $pnl.find('.js-preview').each(function () {
                $link = $(this);
                if ($link.attr('href').endsWith('.pdf'))
                    $link.attr('target', '_blank');
                else
                    $link.click(function () {
                        if ($(this).attr('href').endsWith('.flv') || $(this).attr('href').endsWith('.mp4')) {
                            blueimp.Gallery([
                                {
                                    href: $(this).attr('href'),
                                    type: 'text/html'
                                }
                            ]);
                        }
                        else {
                            blueimp.Gallery($(this));
                        }
                        return false;
                    })

            });

        }
    }
}

var privacyPolicyManager = {
    cookieName: 'AcceptPolicy',
    init: function () {
        var me = this;
        if($('.search-clinic-page').length > 0){
        	this.cookieName = 'AcceptPolicyDocLocator';
        }else{
        	this.cookieName = 'AcceptPolicy';
        }
        if (!this.getCookie(this.cookieName)) {
            $('#privacyPolicyModal .js-read-more').hide();
            $('#privacyPolicyModal .js-close').hide();
            $('#privacyPolicyModal .js-full-text').show();
            $('#privacyPolicyModal .js-accept').removeClass('hidden');
            $('#privacyPolicyModal .js-out').removeClass('hidden');
            $('#privacyPolicyModal').modal({ show: true, backdrop: 'static' });
        }
        $('#privacyPolicyModal .js-accept').click(function () {
            $(this).hide();
            me.saveCookie();
            $('#privacyPolicyModal .js-read-more').show();
            $('#privacyPolicyModal .js-close').show();
            $('#privacyPolicyModal .js-full-text').hide();
            $('#privacyPolicyModal').modal('hide');
        });

        $('#privacyPolicyModal .js-read-more').click(function () {
            $(this).fadeOut('slow');
            $('#privacyPolicyModal .js-full-text').slideDown('slow');
        });
    },
    saveCookie: function () {
        this.setCookie(this.cookieName, true, 999);
    },
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
// var mobileSearch = {
    // init: function () {
        // var $btn = $('.js-mobile-search-btn');
        // var $searchPnl = $('.js-mobile-search-pnl');
//
        // $btn.click(function () {
            // if ($btn.hasClass('open')) {
                // $btn.removeClass('open');
                // $searchPnl.stop().slideUp(500);
            // }
            // else {
                // $btn.addClass('open');
                // $searchPnl.stop().slideDown(500);
            // }
        // });
//
        // $(window).resize(function () {
            // if ($(window).width() <= 767) {
                // if ($btn.hasClass('open')) {
                    // $searchPnl.show();
                // }
                // else {
                    // $searchPnl.hide();
                // }
            // }
            // else {
                // $searchPnl.hide();
            // }
        // }).resize();
    // }
// }
var mobileSearch = {
    init: function () {
        var $btn = $('.js-mobile-search-btn');
        var $searchPnl = $('.js-mobile-search-pnl');

        $btn.click(function () {
            if ($btn.hasClass('open')) {
                $btn.removeClass('open');
                $searchPnl.stop().slideUp(500);
            }
            else {
                $btn.addClass('open');
                $searchPnl.stop().slideDown(500);
            }
        });

        $(window).resize(function () {
            if ($(window).width() <= 767) {
                if ($btn.hasClass('open')) {
                    $searchPnl.show();
                }
                else {
                    $searchPnl.hide();
                }
            }
            else {
                $searchPnl.hide();
            }
        }).resize();
    }
}
var ga_events = {
    init: function () {
        $('#registerModal form, form#registrate').submit(function () {


        	var prof_status = $('#prof_status',$(this)).val(),
        		prof_status_val = '',
        		_this = $(this);
        	console.log($('.input-validation-error',$(this)).length);
			if($('.input-validation-error',$(this)).length > 0){
				console.log('ga register-false');
				console.log('registration false');
				return false;
			}else{

				// check reg datas
				jQuery.ajax({
	               url: '/udata/users/checkreg/.json',
	               dataType: 'json',
	               async: false,
	               data: _this.serialize(), //data: _this.serializeArray(),
	               success: function (data) {
	                    console.log(data);
	                    if(data.status == 'error'){
	                    	console.log(data.field);
	                    	console.log(jQuery('#'+data.field));

	                    	jQuery('#'+data.field).addClass('input-validation-error').removeClass('valid');
	                    	console.log('registration false');
	                    	return false;



	                    }else{
	                    	//console.log('start send');
	                        //console.log('ga register-true');
							switch(prof_status) {
							  //Ортодонт
							  case '14643':
							  	prof_status_val = 'orthodontist';
							    break
							  //Хирург
							  case '14644':
							  	prof_status_val = 'surgeon';
							    break
							  //Другая специализация
							  case '14645':
							  	prof_status_val = 'other';
							    break
							  //Не врач
							  case '14646':
							  	prof_status_val = 'not a doctor';
							    break
							  default:
							    prof_status_val = 'empty';
							    break
							}
							try{
								console.log('registration');
								ga('send', 'event', 'form', 'registration', prof_status_val );
								ym(10184890, 'reachGoal', 'UserRegistration');
							} catch (err) {
								console.log(err);
							}
	                        return true;
	                    }
					}
	         	});
			}
			return true;
        });

        $('#authModal form, form#auth').submit(function (event) {
			$('.waiting').show();
			$('.popup_auth_error_message').hide();
            if ($('.input-validation-error', $(this)).length > 0) {
                console.log('ga auth-false');
				$('.waiting').show();
				event.preventDefault();
            } else {
				// check reg datas
                $.ajax({
                    url: '/udata/users/checkauth/.json',
                    dataType: 'json',
                    async: false,
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.status == 'error') {
                            $('.popup_auth_error_message').text(data.result).show();
                            $('.waiting').hide(); // спрятать прелоалер если ajax ответил ошибкой
							event.preventDefault();
                        } else {
							try{
								console.log('ga auth-true');
								ga('send', 'event', 'form', 'authorization');
								ym(10184890, 'reachGoal', 'UserAuthorization');
							} catch (err) {
								console.log(err);
							}
                        }
                    }
                });
            }
        });
    },
    ga_productclick: function () {
    	$( "body" ).on( "click", ".ga_productclick", function() {
        	 var 	id = $(this).data('id'),
        	 		name = $(this).data('name'),
        	 		category = $(this).data('category'),
        	 		brand = $(this).data('brand'),
        	 		position = $(this).data('position'),
        	 		list = $(this).data('list'),
					number = $(".ga_productclick").index(this) + 1;

			try{
				ga('ec:addProduct', {
				   'id': id,
				   'name': name,
				   'category': category,
				   'brand': brand,
				   'position': position
				});
				ga('ec:setAction', 'click', {list: list});

				ga('send', 'event', 'ET', 'productclick', 'button' + number + '_id' + id);
				ym(10184890, 'reachGoal', 'ProductClick');
			} catch (err){
				console.log(err);
			}
        });
    },
    ga_productbuy: function () {
        $( "body" ).on( "click", ".ga_productbuy", function() {
			var 	id = $(this).data('id'),
        	 		name = $(this).data('name'),
        	 		category = $(this).data('category'),
        	 		brand = $(this).data('brand'),
        	 		price = $(this).data('price'),
        	 		qty = $(this).data('qty'),
					number = $(".ga_productbuy").index(this) + 1;

			try{
				ga('ec:addProduct', {
				   'id': id,
				   'name': name,
				   'category': category,
				   'brand': brand,
				   'price': price,
				   'quantity': qty
				});
				ga('ec:setAction', 'add');

				ga('send', 'pageview', '/virtual/addtoshoppingcart');
				ga('send', 'event', 'ET', 'add to cart', 'button' + number + '_id' + id);

				// Yandex add info
				dataLayer.push({
				   "ecommerce": {
					   "add": {
						   "products": [
							   {
								   "id": id,
								   "name": name,
								   "price": price,
								   "brand": brand,
								   "category": category,
								   "quantity": qty
							   }
						   ]
					   }
				   }
				});

				ym(10184890, 'reachGoal', 'AddToShoppingcart');
			} catch (err) {
				console.log(err);
			}
        });
    },
    ga_delivery_choose: function () {
		/*$('input[name="delivery-id"]').change(function () {
			//alert('jj');
			var delivery_id = $('input[name="delivery-id"]:checked').val();
			var courier = $('.courier.payment_item');
			delivery_id == '13919' ? courier.attr('disabled', true) : courier.attr('disabled', false);

		});*/

        // now send it on purchase
        /*$( "body" ).on( "click", '.btn-show-details[data-target="#collapsePaymentMethod"]', function() {
        	if(!(jQuery('#collapsePaymentMethod').hasClass('in'))){
        		//console.log('// (On "PaymentMethod" button click)');
	        	var 	delivery_id = $('input[name="delivery-id"]:checked').val(),
	        	 		delivery_name = 'Pickup SPb';

				switch(delivery_id) {
				  case 'self_13905':
				  	delivery_name = 'Pickup SPb';
				    break
				  case 'self_13918':
				  	delivery_name = 'Pickup MSK';
				    break
				  case '13919':
				  	delivery_name = 'Delivery';
				    break
				}

				// (On "PaymentMethod" button click)
				ga('ec:setAction', 'checkout_option', {
				'step': 2,
				'option': delivery_name
				});
				ga('send', 'event', 'ET', 'checkout', 'delivery_method');

				// уточнить что пользователь перешел на виртуальную страницу выбора способа оплаты
				for (i in order_items) {
				  var item = order_items[i];

				  ga('ec:addProduct', {
					 'id': item.id,
					 'name': item.name,
					 'category': item.category,
					 'brand': item.brand,
					 'price': item.price,
					 'quantity': item.quantity
				  });
				}

				ga('ec:setAction','checkout', {
				 'step': 3,
				});
				ga('send', 'pageview', '/virtual/paymentmethod');

			}
        })*/
    }
}

var countFile = {
    init: function () {
        $('.js-count-download').click(function () {
        	var url = '/udata/library/countDownloads/'+$(this).data('count_url')+'.json';
            jQuery.ajax({
               url: url,
               dataType: 'json',
               async: false
         	});
         	return true;
        });

    }
}

var settingsChange = {
    cookieName: 'settingsChange',
    init: function () {
        var me = this;
        this.cookieName = 'settingsChange';

        $('.settingsFormSubmit').click(function () {
        //if (!this.getCookie(this.cookieName)) {
            $('#settingsChangeModal').modal({ show: true, backdrop: 'static' });

            $('#settingsChangeModal .js-accept').click(function () {
	            me.saveCookie();
	            $('#settingsChangeModal').modal('hide');
	            $('.settingsFormSubmit').parents('form').submit();
	            return true;
	        });
	        return false;
        })

    },
    saveCookie: function () {
        this.setCookie(this.cookieName, true, 999);
    },
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}


	googleMap.init();
	collapseService.init();
	productFilter.init();
	choiceBrackets.init();
	dealers.init_controls();
	slider.init();
	menuService.init();
	topArrow.init();
	order.init();
	inputSpinner.init();
	initPhotoSwipeFromDOM('.js-photos');
	privacyPolicyManager.init();
	downloadMaterials.init();
	mobileSearch.init();

	countFile.init();
	settingsChange.init();


    jQuery(document).ready(function(){
		ga_events.init();
		ga_events.ga_productclick();
		ga_events.ga_productbuy();
		ga_events.ga_delivery_choose();
		webform.captcha();
		webform.send();
    });



    jQuery(document).ready(function(){
        if(jQuery('.smart_filter_search').length > 0){
            jQuery('#search_input_autocomplete').easyAutocomplete({
                url: function(phrase) {
                    return '/udata/catalog/try_smart_search/?phrase=' + phrase + '&search_branches=' + jQuery('.smart_filter_search_search_branches').val();
                },
                categories: [
                    {
                        listLocation: "categories",
                        maxNumberOfElements: 5,
                        header: "Разделы"
                    }, {
                        listLocation: "items",
                        maxNumberOfElements: 20,
                        header: "Товары"
                    }, {
                        listLocation: "all",
                        maxNumberOfElements: 5,
                    }
                ],
                minCharNumber: 3,
                getValue: function(element) {
                    return element.name;
                },
                ajaxSettings: {
                    dataType: "json",
                    method: "GET",
                    data: {
                        dataType: "json"
                    }
                },
                template: {
                    type: "custom",
                    method: function(value, item) {
                        return "<div class='each-link " + item.class + "' data-link='" + item.link + "'>" + ( item.icon ? "<img src='" + item.icon + "' class='each-icon' /> " : "" ) + item.name + "</div>";
                    },
                },
                list: {
                    onChooseEvent: function() {
                        var value = jQuery("#search_input_autocomplete").getSelectedItemData().link;
                        document.location.href = value;
                    },
                    maxNumberOfElements: 100,
                },
                requestDelay: 200
            });
        }
    });

    jQuery(document).ready(function(){
        jQuery('a[href*=\\#]').click(scroll_same_page_href);
    });

    function scroll_same_page_href(event){
        var target = event.target;
		try{
			var explode_href = jQuery(target).attr('href').split('#');
			if(explode_href.length > 1){
				var id = explode_href[1];
				var elem_top = 0;
				if(jQuery('#' + id).length > 0){
					event.preventDefault();
					elem_top = jQuery('#' + id).offset().top;
				}else if(jQuery('*[name = ' + id + ']').length > 0 ){
					event.preventDefault();
					elem_top = jQuery('*[name = ' + id + ']').offset().top;
				}
				var top = (elem_top - jQuery('header').height()) + 'px';
				jQuery('html, body').stop().animate({scrollTop:top}, 300, function(){
					var top = (elem_top - jQuery('header').height()) + 'px'; // коррекция на изменение высоты шапки после скролла
					jQuery('html, body').animate({scrollTop:top}, 100);
				});
			}
		} catch (err) {
			console.log(err);
		}
    }

	//js для формы регистрации
	// отображать текстовое поле при выборе в поле "кто вы" варианта "другая специальность"
	$('.prof_status_field').on('change', function() {
		var option_val = this.value;
		var this_form = $(this).parents('form');
		var targ = $('.prof_status_field option[value="'+option_val+'"]', this_form).attr('target');
		//console.log($('#prof_status option[value="'+option_val+'"]'));

		// другая специальность item id = 14645
		if(option_val == '14645'){
			targ = 'ord_dop_fields3';
		}

		// другая специальность item id = 14645
		if(option_val == '2737568'){
			targ = 'student_dop_field';
		}
		//console.log(targ);
		$('.ord_dop_fields', this_form).collapse('hide');
		$('.'+targ, this_form).collapse('show');
	});

    /*$('.is_student').on('change', function() {
        var this_form = $(this).closest('form');
		$('.student_dop_field', this_form).stop(true, true).slideToggle(200);
    });*/

	// блокировать определенные регионы, если в поле "Страна" выбрана не "Россия"
	$('.country_field').on('change', function() {
		console.log( 'country ch' );
		console.log( this.value );
		var option_val = this.value,
			this_form = $(this).parents('form'),
			this_region_select = $('.region_field',this_form),
			russia_val = 14531,
			not_russia_region_val = 1884760;

		if(option_val != russia_val && option_val != '' && option_val != 'Страна*'){
			this_region_select.val(not_russia_region_val);
			$('option',this_region_select).prop('disabled', true);
			$('option:last',this_region_select).prop('disabled', false);
			this_region_select.selectpicker('refresh');
		}else if(option_val == russia_val && this_region_select.val() == not_russia_region_val){
			console.log($('.region_field option:first'));
			$('option',this_region_select).prop('disabled', false);
			$('option:first',this_region_select).prop('selected', 'selected');
			$('option:last',this_region_select).prop('disabled', true);
			this_region_select.selectpicker('refresh');
		}
	});
	//\js для формы регистрации

    // show loading
    jQuery(document).ready(function(){
        $('#registerModal form, form#registrate, form#personal_information_form').submit(function(){
            $('.waiting').show();
            if ($('.input-validation-error', $(this)).length > 0) {
                $('.waiting').hide();
            }
        });
        $('.no_ajax_form_waiting_show').submit(function(){
            $('.waiting').show();
        });
    });

    jQuery(document).ready(function(){
		jQuery('#ya_vrach351_fake').change(function(){
			if(jQuery(this).prop("checked")){
				jQuery(this).closest('form').find('.doctor_only').addClass('is_doctor_selected');
			}else{
				jQuery(this).closest('form').find('.doctor_only').removeClass('is_doctor_selected');
			}
		});
		jQuery('#ya_vrach_contacts_351_fake').change(function(){
			if(jQuery(this).prop("checked")){
				jQuery(this).closest('form').find('.doctor_only').addClass('is_doctor_selected');
			}else{
				jQuery(this).closest('form').find('.doctor_only').removeClass('is_doctor_selected');
			}
		});

		jQuery('#tema351').change(function(){
			jQuery(this).closest('form').find('.system_email_to').val(jQuery(this).find('option:selected').data('tema-id'));
		});

		jQuery('#tema_contacts_351').change(function(){
			jQuery(this).closest('form').find('.system_email_to').val(jQuery(this).find('option:selected').data('tema-id'));
		});
    });

	jQuery(document).ready(function(){
		if(jQuery('.top_info_string_close').length > 0){
			jQuery('.top_info_string_close').click(function(event){
				event.preventDefault();
				jQuery(this).closest('body').removeClass('with_top_info_string');
				jQuery(this).closest('.top_information_string').hide();
				jQuery.cookie('top_information_string_close', 1, { expires: 14, path: "/" });
			});
		}

        jQuery('.top_phone').click(function(event){
            event.preventDefault();
            jQuery('.mobile-add-phones').slideToggle()
        });

        jQuery(document).on('click','.jq-number__spin', function(event){
            event.preventDefault();
            let input = jQuery(this).closest('div.jq-number').find('input.pf_number');
            let val = Number.parseInt(jQuery(input).val());
            if(jQuery(this).hasClass('plus')){
                jQuery(input).val(val + 1);
            }
            if(jQuery(this).hasClass('minus')){
                if(val > 1){
                    jQuery(input).val(val - 1);
                }
            }
        });
	});
})(lastJQ,lastJQ);