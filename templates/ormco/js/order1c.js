jQuery(document).ready(function(){
	let start_year_date = new Date(2021, 0, 12),
		today = new Date(),
		min_date;

	if(today < start_year_date) {
		min_date = start_year_date
	} else {
		let tomorrow = new Date();
		tomorrow.setDate(today.getDate() + 1)
		min_date = tomorrow;
	}

	jQuery('#prefer_date_delivery').datepicker({
		minDate: min_date,
		dateFormat: "dd-mm-yyyy",
		onRenderCell: function (date) {
			var dayOfWeek = date.getDay();
			if (dayOfWeek === 0 || dayOfWeek === 6) { // Вск:0, Сб:6
				return {
					classes: '-disabled-'
				};
			}
		}
	});
});