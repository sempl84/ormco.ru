<?php

$FORMS = Array();

$FORMS['category'] = <<<END
%catalog getCategoryList('default', '%category_id%', 100, 1)%
%catalog getObjectsList('default', '%category_id%')%

END;


$FORMS['category_block'] = <<<END
<h3>Подразделы</h3>
<ul>
	%lines%
</ul>


END;


$FORMS['category_block_empty'] = <<<END

END;


$FORMS['category_block_line'] = <<<END
<li>
	<a href="%link%">%text%</a>
</li>

END;




$FORMS['objects_block'] = <<<END
<div class="white-pnl products-list js-products-list">
    <div>
        <ul>
        	%lines%
        </ul>
    </div>
</div>
 
END;


$FORMS['objects_block_search_empty'] = <<<END

END;


$FORMS['objects_block_line'] = <<<END
<li>
    <div class="row">
        <div class="col-xs-3 left">
            <a href="%link%">
            
            	<img class="img-responsive" src="%photo%" alt="%text%" width="190px" height="190px">
            	
            </a>
        </div>
        <div class="col-xs-9 right">
            <a href="%link%" class="lnk-to-product">%text%</a>
            <div class="row">
                <div class="col-sm-4">
                   	<p><span class="blue">Артикул</span>&#160;%artikul%"</p>
                    
                    <p>
                    	<span class="blue">Цена</span> 
                    	%price%
                    </p>
                </div>
                <div class="col-sm-8">
             		<!--common_quantity
					<div class="availability yes"><i class="fa fa-check" aria-hidden="true"></i>Есть в наличии</div>
					<div class="availability no"><i class="fa fa-circle" aria-hidden="true"></i>Нет в наличии</div>
					-->
						
                    
                </div>
            </div>
            <div class="input-group spinner forBuy">
                <label for="amount_id_%id%" class="blue">Количество</label>
                <input id="amount_id_%id%" type="text" class="form-control" value="1" />
                <div class="input-group-btn-vertical">
                    <button class="btn btn-default" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                    <button class="btn btn-default" type="button"><i class="fa fa-play" aria-hidden="true"></i></button>
                </div>
            </div>
            <a id="add_basket_%id%" href="/emarket/basket/put/element/%id%/" class="btn-primary btn-buy basket_list options_false">Купить</a>
            
        </div>
    </div>
</li>

END;



$FORMS['view_block'] = <<<END
<h4>
	Тестирование макроса "emarket price(id)"
</h4>
%emarket price(%id%)%

<h4>
	Тестирование макроса "emarket stores(id)"
</h4>
%emarket stores(%id%)%

<form method="post" action="/emarket/basket/put/element/%id%/">
	<div class="buttons">
		<input type="submit" value="Положить в корзину" />
		<input type="button" value="Добавить к сравнению" />
	</div>
</form>

END;

$FORMS['search_block'] = <<<END

<form method="get" action="%content get_page_url(%category_id%)%">
<h3>Фильтр по товарам</h3>
%lines%
<p>
	<input type="submit" value="Подобрать" /><br />
	<input type="button" onclick="javascript: window.location = '%content get_page_url(%category_id%)%';" value="Сбросить" />
</p>
</form>


END;


$FORMS['search_block_line'] = <<<END
	<p>
		%selector%
	</p>
END;



$FORMS['search_block_line_relation'] = <<<END
%title% <select name="fields_filter[%name%]"><option />%items%</select>

END;

$FORMS['template_block_line_symlink'] = <<<END
%title% <select name="fields_filter[%name%]"><option />%items%</select>

END;

$FORMS['search_block_line_text'] = <<<END
%title% <input type="text" name="fields_filter[%name%]" class="textinputs" value="%value%" />

END;

$FORMS['search_block_line_price'] = <<<END
%title% от &nbsp;до 
<input type="text" name="fields_filter[%name%][0]" value="%value_from%" />
<input type="text" name="fields_filter[%name%][1]" value="%value_to%" />

END;

$FORMS['search_block_line_boolean'] = <<<END
%title% <input type="checkbox" name="fields_filter[%name%]" %checked% value="1" /> 

END;

?>