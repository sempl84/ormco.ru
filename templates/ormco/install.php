<?php
	/**
	 * Класс сценария установки демошаблона "demodizzy".
	 */
	class demodizzyInstallScenario extends siteInstallScenario implements iSiteInstallScenario{

		/**
		 * @inherit
		 */
		public function run() {
			$this->setCallOrderTemplate();
			$this->reIndexCategories();
			$this->setDomainForOrders();
			$this->addAppointmentData();
		}

		/**
		 * Заполняет модуль "Онлайн-запись" демонстрационными данными
		 */
		private function addAppointmentData() {
			$this->addAppointmentPage();
			$this->addAppointmentEntities();
			$this->addMailTemplates();
			$this->addRegistryData();
		}

		/**
		 * Меняет ключи реестра
		 */
		private function addRegistryData() {
			$umiRegistry = regedit::getInstance();
			$umiRegistry->setVal("//modules/appointment/new-record-admin-notify", true);
			$umiRegistry->setVal("//modules/appointment/new-record-user-notify", true);
			$umiRegistry->setVal("//modules/appointment/record-status-changed-user-notify", true);

			$fromQueryTemplate = '//modules/appointment/work-time-%d-from';
			$toQueryTemplate = '//modules/appointment/work-time-%d-to';
			$fromTime = '08:00';
			$toTime = '20:00';

			for ($dayNumber = 0; $dayNumber < 5; $dayNumber++) {
				$umiRegistry->setVal(sprintf($fromQueryTemplate, $dayNumber), $fromTime);
				$umiRegistry->setVal(sprintf($toQueryTemplate, $dayNumber), $toTime);
			}
		}

		/**
		 * Создает шаблоны писем
		 * @throws Exception
		 */
		private function addMailTemplates() {
			$adminNotifyNewContent = <<<HTML
<table style="max-width: 400px; width: 100%;">
	<tbody>
		<tr>
			<td>Имя</td>
			<td>%name%</td>
		</tr>
		<tr>
			<td>Телефон</td>
			<td>%phone%</td>
		</tr>
		<tr>
			<td>E-Mail</td>
			<td>%email%</td>
		</tr>
		<tr>
			<td>Комментарий</td>
			<td>%comment%</td>
		</tr>
		<tr>
			<td>Дата</td>
			<td>%date%</td>
		</tr>
		<tr>
			<td>Время</td>
			<td>%time%</td>
		</tr>
		<tr>
			<td>Услуга</td>
			<td>%service%</td>
		</tr>
		<tr>
			<td>Категория</td>
			<td>%category%</td>
		</tr>
		<tr>
			<td>Специалист</td>
			<td>%specialist%</td>
	</tr>
	</tbody>
</table>
HTML;

$userNotifyNewContent = <<<HTML
<table style="max-width: 400px; width: 100%;">
	<tbody>
		<tr>
			<td>Дата</td>
			<td>%date%</td>
		</tr>
		<tr>
			<td>Время</td>
			<td>%time%</td>
		</tr>
		<tr>
			<td>Услуга</td>
			<td>%service%</td>
		</tr>
		<tr>
			<td>Категория</td>
			<td>%category%</td>
		</tr>
		<tr>
			<td>Специалист</td>
			<td>%specialist%</td>
		</tr>
	</tbody>
</table>
HTML;

$userNotifyModifyContent = <<<HTML
<table style="max-width: 400px; width: 100%;">
	<tbody>
		<tr>
			<td>Группа</td>
			<td>%category%</td>
		</tr>
		<tr>
			<td>Услуга</td>
			<td>%service%</td>
		</tr>
		<tr>
			<td>Дата</td>
			<td>%date%&nbsp;</td>
		</tr>
		<tr>
			<td>Время</td>
			<td>&nbsp;%time%</td>
		</tr>
		<tr>
			<td>Специалист</td>
			<td>%specialist%</td>
		</tr>
		<tr>
			<td>Статус</td>
			<td>%new-status%</td>
		</tr>
	</tbody>
</table>
HTML;

			$serviceContainer = umiServiceContainers::getContainer();
			/** @var MailTemplatesCollection $mailTemplatesCollection */
			$mailTemplatesCollection = $serviceContainer->get('MailTemplates');
			$mailTemplatesMap = $mailTemplatesCollection->getMap();

			$fieldName = $mailTemplatesMap->get('NAME_FIELD_NAME');
			$fieldContent = $mailTemplatesMap->get('CONTENT_FIELD_NAME');

			$templates = [
				[
					$fieldName => 'new-record-admin-notify-content',
					$fieldContent => $adminNotifyNewContent

				],
				[
					$fieldName => 'new-record-user-notify-content',
					$fieldContent => $userNotifyNewContent
				],
				[
					$fieldName => 'record-status-changed-user-notify-content',
					$fieldContent => $userNotifyModifyContent
				],
				[
					$fieldName => 'new-record-admin-notify-subject',
					$fieldContent => 'Запись на приём \\ %category% \\ %service% \\'
				],
				[
					$fieldName => 'new-record-user-notify-subject',
					$fieldContent => 'Вы записались на приём \\ %category% \\ %service% \\'
				],
				[
					$fieldName => 'record-status-changed-user-notify-subject',
					$fieldContent => 'Ваша заявка на запись \\ %category% \\ %service% \\ была обновлена'
				]
			];

			foreach ($templates as $template) {
				$mailTemplatesCollection->create($template);
			}
		}

		/**
		 * Создает страницу с записью на прием
		 * @return bool
		 * @throws coreException
		 */
		private function addAppointmentPage() {
			$umiHierarchyTypes = umiHierarchyTypesCollection::getInstance();
			/**
			 * @var iUmiHierarchyType|iUmiEntinty $appointmentPageType
			 */
			$appointmentPageType = $umiHierarchyTypes->getTypeByName('appointment', 'page');

			if (!$appointmentPageType instanceof iUmiHierarchyType) {
				$this->addLogMessage('Не удалось получить иерархический тип страницы с записью на прием');
				$this->addEndMessage();
				return false;
			}

			$umiHierarchy = umiHierarchy::getInstance();
			$appointmentPageId = $umiHierarchy->addElement(
				0,
				$appointmentPageType->getId(),
				'Онлайн-запись',
				'appointment'
			);

			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $appointmentPage
			 */
			$appointmentPage = $umiHierarchy->getElement($appointmentPageId, true);

			if (!$appointmentPage instanceof iUmiHierarchyElement) {
				$this->addLogMessage('Не удалось создать страницу с записью на прием');
				$this->addEndMessage();
				return false;
			}

			$umiPermissions = permissionsCollection::getInstance();
			$guestId = $umiPermissions->getGuestId();
			$pageId = $appointmentPage->getId();
			$level = permissionsCollection::E_READ_ALLOWED_BIT;
			$umiPermissions->setElementPermissions($guestId, $pageId, $level);

			$appointmentPage->setValue('title', 'Запись на прием');
			$appointmentPage->setIsVisible(true);
			$appointmentPage->setIsActive(true);
			$appointmentPage->commit();

			return true;
		}

		/**
		 * Создает сущность модуля "Онлайн-запись"
		 * @throws Exception
		 */
		private function addAppointmentEntities() {
			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentServiceGroupsCollection $groupsCollection
			 */
			$groupsCollection = $serviceContainer->get('AppointmentServiceGroups');
			$defaultGroupData = [
				$groupsCollection->getMap()->get('NAME_FIELD_NAME') => 'Услуги'
			];
			/**
			 * @var AppointmentServiceGroup $group
			 */
			$group = $groupsCollection->create($defaultGroupData);

			/**
			 * @var AppointmentServicesCollection $servicesCollection
			 */
			$servicesCollection = $serviceContainer->get('AppointmentServices');
			$servicesCollectionMap = $servicesCollection->getMap();

			$defaultServiceData = [
				$servicesCollectionMap->get('GROUP_ID_FIELD_NAME') => $group->getId(),
				$servicesCollectionMap->get('NAME_FIELD_NAME') => 'Починка бытовой техники',
				$servicesCollectionMap->get('TIME_FIELD_NAME') => '03:00:00',
				$servicesCollectionMap->get('PRICE_FIELD_NAME') => 100.500
			];
			/**
			 * @var AppointmentService $service
			 */
			$service = $servicesCollection->create($defaultServiceData);

			/**
			 * @var AppointmentEmployeesCollection $employeesCollection
			 */
			$employeesCollection = $serviceContainer->get('AppointmentEmployees');
			$employeesCollectionMap = $employeesCollection->getMap();

			$defaultEmployeeData = [
				$employeesCollectionMap->get('NAME_FIELD_NAME') => 'Василий Зайцев',
				$employeesCollectionMap->get('PHOTO_FIELD_NAME') => new umiImageFile(CURRENT_WORKING_DIR . '/images/employee.jpg'),
				$employeesCollectionMap->get('DESCRIPTION_FIELD_NAME') => 'Ремонтник со стажем.'
			];
			/**
			 * @var AppointmentEmployee $employee
			 */
			$employee = $employeesCollection->create($defaultEmployeeData);

			/**
			 * @var AppointmentEmployeesServicesCollection $employeesServicesCollection
			 */
			$employeesServicesCollection = $serviceContainer->get('AppointmentEmployeesServices');
			$employeesServicesCollectionMap = $employeesServicesCollection->getMap();

			$defaultEmployeeServiceData = [
				$employeesServicesCollectionMap->get('EMPLOYEE_ID_FIELD_NAME') => $employee->getId(),
				$employeesServicesCollectionMap->get('SERVICE_ID_FIELD_NAME') => $service->getId()
			];

			$employeesServicesCollection->create($defaultEmployeeServiceData);

			/**
			 * @var AppointmentEmployeesSchedulesCollection $employeesSchedulesCollection
			 */
			$employeesSchedulesCollection = $serviceContainer->get('AppointmentEmployeesSchedules');
			$employeesSchedulesCollectionMap = $employeesSchedulesCollection->getMap();
			$employeeIdField = $employeesSchedulesCollectionMap->get('EMPLOYEE_ID_FIELD_NAME');
			$dayNumberField = $employeesSchedulesCollectionMap->get('DAY_NUMBER_FIELD_NAME');
			$timeStartField = $employeesSchedulesCollectionMap->get('TIME_START_FIELD_NAME');
			$timeEndField = $employeesSchedulesCollectionMap->get('TIME_END_FIELD_NAME');

			$defaultEmployeeScheduleData = [
				[
					$employeeIdField => $employee->getId(),
					$dayNumberField => 0,
					$timeStartField => '09:00:00',
					$timeEndField => '18:00:00',
				],
				[
					$employeeIdField => $employee->getId(),
					$dayNumberField => 1,
					$timeStartField => '09:00:00',
					$timeEndField => '18:00:00',
				],
				[
					$employeeIdField => $employee->getId(),
					$dayNumberField => 2,
					$timeStartField => '09:00:00',
					$timeEndField => '18:00:00',
				],
				[
					$employeeIdField => $employee->getId(),
					$dayNumberField => 3,
					$timeStartField => '09:00:00',
					$timeEndField => '18:00:00',
				],
				[
					$employeeIdField => $employee->getId(),
					$dayNumberField => 4,
					$timeStartField => '09:00:00',
					$timeEndField => '18:00:00',
				]
			];

			array_map([$employeesSchedulesCollection, 'create'], $defaultEmployeeScheduleData);

			/**
			 * @var AppointmentOrdersCollection $ordersCollection
			 */
			$ordersCollection = $serviceContainer->get('AppointmentOrders');
			$ordersCollectionMap = $ordersCollection->getMap();

			$defaultOrderDate = [
				$ordersCollectionMap->get('SERVICE_ID_FIELD_NAME') => $service->getId(),
				$ordersCollectionMap->get('EMPLOYEE_ID_FIELD_NAME') => $employee->getId(),
				$ordersCollectionMap->get('ORDER_DATE_FIELD_NAME') => new umiDate(rand(1277118393, 1750503993)),
				$ordersCollectionMap->get('DATE_FIELD_NAME') => new umiDate(rand(1277118393, 1750503993)),
				$ordersCollectionMap->get('TIME_FIELD_NAME') => '14:00:00',
				$ordersCollectionMap->get('NAME_FIELD_NAME') => 'Василий Пупкин',
				$ordersCollectionMap->get('PHONE_FIELD_NAME') => '+7 899 434 43 34',
				$ordersCollectionMap->get('EMAIL_FIELD_NAME') => 'tester@mail.ru',
				$ordersCollectionMap->get('COMMENT_FIELD_NAME') => 'На телефон отвечаю не позже 23:00',
				$ordersCollectionMap->get('STATUS_ID_FIELD_NAME') => 2,
			];

			$ordersCollection->create($defaultOrderDate);
		}

		/**
		 * Устанавливает шаблон писем для формы обратной связи "Заказать звонок"
		 * @throws coreException
		 */
		private function setCallOrderTemplate() {
			$objects = umiObjectsCollection::getInstance();
			$callOrderTemplate = $objects->getObjectByGUID('call-order-template');
			$types = umiObjectTypesCollection::getInstance();
			$callOrderFormId = $types->getTypeIdByGUID('call-order-form');

			if ($callOrderTemplate instanceof iUmiObject && is_numeric($callOrderFormId) && $callOrderFormId > 0) {
				$callOrderTemplate->setValue('form_id', $callOrderFormId);
				$callOrderTemplate->commit();
				$this->addLogMessage("Шаблон писем для формы с идентификатором ${callOrderFormId} установлен");
			}
		}

		/**
		 * Устанавливает домен для заказов в соответствующем поле
		 * @throws selectorException
		 */
		private function setDomainForOrders() {
			$orders = new selector('objects');
			$orders->types('hierarchy-type')->name('emarket', 'order');
			$result = $orders->result();

			$currentDomain = cmsController::getInstance()->getCurrentDomain();

			if (!$currentDomain instanceof domain) {
				return;
			}

			/** @var iUmiObject $order */
			foreach($result as $order) {
				$order->setValue('domain_id', $currentDomain->getId());
				$order->commit();
				$this->addLogMessage("Домен для заказа #{$order->getId()} успешно установлен");
			}
		}

		/**
		 * Запускает переиндексацию фильтров категорий
		 * @return bool
		 */
		private function reIndexCategories() {
			$this->addStartMessage();
			$indexedCategories = $this->getIndexedCategories();

			if (count($indexedCategories) == 0) {
				$this->addLogMessage('Не найдены категории, требующие переиндексации фильтров');
				$this->addEndMessage();
				return false;
			}

			$umiHierarchy = umiHierarchy::getInstance();

			/* @var iUmiHierarchyElement|umiEntinty $indexedCategory */
			foreach ($indexedCategories as $indexedCategory) {
				$this->reIndexCategory($indexedCategory);
				$categoryId = $indexedCategory->getId();
				$this->addLogMessage('Категория с id = ' . $categoryId . ' успешно переиндексирована');
				$umiHierarchy->unloadElement($categoryId);
			}

			$this->addEndMessage();
			return true;
		}

		/**
		 * Добавляет в лог сообщение о начале выполнения сценария
		 * @return void
		 */
		private function addStartMessage() {
			$this->addLogMessage('Начало переиндексации фильтров');
		}

		/**
		 * Возвращает категории разделов каталога, нуждающиеся в переиндексации
		 * @return array
		 */
		private function getIndexedCategories() {
			$categories = new selector('pages');
			$categories->types('hierarchy-type')->name('catalog', 'category');
			$categories->where('index_choose')->equals(true);
			return $categories->result();
		}

		/**
		 * Переиндексирует фильтры раздела каталога
		 * @param iUmiHierarchyElement $category объект раздела каталога
		 * @return true
		 */
		private function reIndexCategory(iUmiHierarchyElement $category) {
			$level = (int) $category->getValue('index_level');
			$parentId = $category->getId();
			$catalogObjectHierarchyTypeId = $this->getCatalogObjectHierarchyTypeId();

			$indexGenerator = new FilterIndexGenerator($catalogObjectHierarchyTypeId, 'pages');
			$indexGenerator->setHierarchyCondition($parentId, $level);
			$indexGenerator->run();

			$category->setValue('index_source', $parentId);
			$category->setValue('index_date', new umiDate());
			$category->setValue('index_state', 100);
			$category->commit();

			$this->markChildren($parentId, $level);
			return true;
		}

		/**
		 * Возвращает идентификатор иерархического типа данных объектов каталога,
		 * или false, если не удалось получить тип.
		 * @return bool|int
		 */
		private function getCatalogObjectHierarchyTypeId() {
			$umiHierarchyTypes = umiHierarchyTypesCollection::getInstance();
			$umiHierarchyType = $umiHierarchyTypes->getTypeByName('catalog', 'object');

			if (!$umiHierarchyType instanceof umiHierarchyType) {
				return false;
			}

			return $umiHierarchyType->getId();
		}

		/**
		 * Указывает у дочерних разделов каталога источник индекса фильтров
		 * @param int $parentId ид родительского раздела
		 * @param int $level уровень вложенности дочерних разделов
		 * @return bool
		 */
		private function markChildren($parentId, $level) {
			$childrenCategories = new selector('pages');
			$childrenCategories->types('hierarchy-type')->name('catalog', 'category');
			$childrenCategories->where('hierarchy')->page($parentId)->childs($level);
			$childrenCategories->order('id');
			$childrenCategories = $childrenCategories->result();

			if (count($childrenCategories) == 0) {
				false;
			}
			$umiHierarchy = umiHierarchy::getInstance();
			/* @var iUmiHierarchyElement|umiEntinty $childrenCategory */
			foreach ($childrenCategories as $childrenCategory) {
				$childrenCategory->setValue('index_source', $parentId);
				$childrenCategory->commit();
				$umiHierarchy->unloadElement($childrenCategory->getId());
			}
			return true;
		}

		/**
		 * Добавляет в лог сообщение о конце выполнения сценария
		 * @return void
		 */
		private function addEndMessage() {
			$this->addLogMessage('Конец переиндексации фильтров');
		}
	}
?>