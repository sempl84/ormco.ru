<?php
/** @var umiTemplaterPHP|BaseExtension|PageExtension|UserExtension|OrmcoExtension $this */
/** @var array $variables */

$this->setCommonVar(OrmcoExtension::contacts_spb_phone, $this->getOrmcoSettingsValue('telefon_2'));
$this->setCommonVar(OrmcoExtension::contacts_spb_address, $this->getOrmcoSettingsValue('adres_2'));
$this->setCommonVar(OrmcoExtension::contacts_msk_phone, $this->getOrmcoSettingsValue('telefon_1'));
$this->setCommonVar(OrmcoExtension::contacts_msk_address, $this->getOrmcoSettingsValue('adres_1'));
$this->setCommonVar(OrmcoExtension::contacts_rus_phone, $this->getOrmcoSettingsValue('telefon_3'));
$this->setCommonVar(OrmcoExtension::contacts_rus_address, $this->getOrmcoSettingsValue('adres_3'));
$this->setCommonVar(OrmcoExtension::contacts_email, $this->getOrmcoSettingsValue(SiteContentPageSettingsModel::field_contacts_email));

$this->setCommonVar(OrmcoExtension::social_fb, 'https://facebook.com/ormcorussia');
$this->setCommonVar(OrmcoExtension::social_vk, 'https://vk.com/ormcorussia');
$this->setCommonVar(OrmcoExtension::social_yt, 'https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg');
$this->setCommonVar(OrmcoExtension::social_tw, 'https://twitter.com/ormcorussia');
$this->setCommonVar(OrmcoExtension::social_ig, 'https://www.instagram.com/ormcorussia/');
$this->setCommonVar(OrmcoExtension::social_tg, ' https://t.me/ormcorussia');

if($catalogPageLink = $this->getPageLink(OrmcoHelper::catalog_page_id)) {
    $this->setCommonVar(OrmcoExtension::pages_catalog_link, $catalogPageLink);
}

$privacyPage = getArrayKey($this->getOrmcoSettingsValue(SiteContentPageSettingsModel::field_redesign_pages_policy), 0);
if($privacyPage instanceof umiHierarchyElement) {
    $this->setCommonVar(OrmcoExtension::pages_policy_link, $this->getPageLink($privacyPage->getId()));
}

$templateFilePath = $this->getTemplateFilePath(__DIR__);

if(!$this->getUserIsAuth()) {
    try {
        $modalAuthHtml = $this->render(null, $templateFilePath . '/assets/modal/auth');
        if($modalAuthHtml) {
            $this->addModal('auth', $modalAuthHtml);
        }
    } catch (Exception $e) {}
}

try {
    $modalCountryHtml = $this->render(null, $templateFilePath . '/assets/modal/country');
    if($modalCountryHtml) {
        $this->addModal('country', $modalCountryHtml);
        $this->setCommonVar(OrmcoExtension::modal_country_active, true);
    }
} catch (Exception $e) {}

try {
    $modalFeedbackHtml = $this->render(null, $templateFilePath . '/assets/modal/feedback');
    if($modalFeedbackHtml) {
        $this->addModal('feedback', $modalFeedbackHtml);
        $this->setCommonVar(OrmcoExtension::modal_feedback_active, true);
    }
} catch (Exception $e) {}