<?php
/** @var umiTemplaterPHP|BaseExtension|PageExtension|UserExtension|CartExtension|OrmcoExtension $this */
/** @var array $variables */

$templateResources = str_replace(CURRENT_WORKING_DIR, '', dirname(dirname(__DIR__)));

$extensionsDir = '/vendor/UmiSpec/Extensions/';

$this->loadExtension(array(
    $extensionsDir . 'BaseExtension',
    $extensionsDir . 'PageExtension',
    $extensionsDir . 'UserExtension',
    $extensionsDir . 'CartExtension',
    $templateResources . '/php/library/extensions/OrmcoExtension',
));

$this->setTemplateResources($templateResources);
$this->setTemplateVersion('?v=2021121401');

$this->setModule(getArrayKey($variables, 'module'));
$this->setMethod(getArrayKey($variables, 'method'));

$this->parsePage(getArrayKey($variables, 'page'));
$this->setParents(getArrayKey($variables, 'parents'));

$this->setSiteMetaTitle(getArrayKey($variables, 'title'));
$this->siteSiteHeader(getArrayKey($variables, 'header'));
$this->parseMeta(getArrayKey($variables, 'meta'));

$this->parseUser(getArrayKey($variables, 'user'));

try {
    $this->setCart($this->macros('emarket', 'cart'));
} catch (Exception $e) {}

$this->setRequestUri(getArrayKey($variables, 'request-uri'));