<?php

use UmiCms\Service;

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrmcoExtension implements IPhpExtension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __CLASS__;
    }

    const no_image = '/templates/ormco_2021/assets/images/logo-ormco-tovar.png';

    const assets_errors = 'layouts/default/assets/errors';
    const assets_errors_param_items = 'items';

    const assets_breadcrumbs_default = 'layouts/default/assets/breadcrumbs/default';
    const assets_breadcrumbs_param_items = 'items';
    const assets_breadcrumbs_param_last = 'last';
    const assets_breadcrumbs_param_h1 = 'h1';

    const assets_numpages_default = 'layouts/default/assets/numpages/default';
    const assets_numpages_param_total = 'total';
    const assets_numpages_param_per_page = 'per_page';

    const assets_field_param_field = 'field';
    const assets_field_param_title = 'title';
    const assets_field_param_required = 'required';
    const assets_field_param_attributes = 'attributes';
    const assets_field_param_input_type = 'input_type';
    const assets_field_param_input_name = 'input_name';
    const assets_field_param_input_id = 'input_id';
    const assets_field_param_value = 'value';
    const assets_field_param_options = 'options';
    const assets_field_param_options_data = 'options_data';

    const assets_field_default = 'layouts/default/assets/field/default';

    const link_search = '/search/search_do/';
    const link_search_param_query = 'search_string';

    const link_cart = '/emarket/cart/';
    const link_login = '/users/login/';
    const link_forget = '/users/forget/';
    const link_settings = '/users/settings/';
    const link_personal_orders = '/emarket/ordersList/';

    const contacts_spb_phone = 'contacts_spb_phone';
    const contacts_spb_address = 'contacts_spb_address';
    const contacts_msk_phone = 'contacts_msk_phone';
    const contacts_msk_address = 'contacts_msk_address';
    const contacts_rus_phone = 'contacts_rus_phone';
    const contacts_rus_address = 'contacts_rus_address';
    const contacts_email = 'contacts_email';

    const social_fb = 'social_fb';
    const social_ig = 'social_ig';
    const social_tw = 'social_tw';
    const social_vk = 'social_vk';
    const social_yt = 'social_yt';
    const social_tg = 'social_tg';

    const modal_feedback_active = 'modal_feedback_active';
    const modal_country_active = 'modal_country_active';

    const pages_catalog_link = 'pages_catalog_link';
    const pages_policy_link = 'pages_policy_link';

    public function getOrmcoSettingsValue($fieldName)
    {
        $page = SiteContentPageSettingsModel::getPage();

        return $page instanceof umiHierarchyElement ? $page->getValue($fieldName) : null;
    }

    public function formatPrice($price)
    {
        return number_format($price, 2, ',', ' ');
    }

    private $modalAuthLoginByPhoneEnabled = false;
    private $modalAuthLoginByEmailEnabled = false;
    private $modalAuthRegisterStep1Enabled = false;

    /**
     * @return bool
     */
    public function isModalAuthLoginByPhoneEnabled()
    {
        return $this->modalAuthLoginByPhoneEnabled;
    }

    /**
     * @param bool $modalAuthLoginByPhoneEnabled
     */
    public function setModalAuthLoginByPhoneEnabled($modalAuthLoginByPhoneEnabled)
    {
        $this->modalAuthLoginByPhoneEnabled = $modalAuthLoginByPhoneEnabled;
    }

    /**
     * @return bool
     */
    public function isModalAuthLoginByEmailEnabled()
    {
        return $this->modalAuthLoginByEmailEnabled;
    }

    /**
     * @param bool $modalAuthLoginByEmailEnabled
     */
    public function setModalAuthLoginByEmailEnabled($modalAuthLoginByEmailEnabled)
    {
        $this->modalAuthLoginByEmailEnabled = $modalAuthLoginByEmailEnabled;
    }

    /**
     * @return bool
     */
    public function isModalAuthRegisterStep1Enabled()
    {
        return $this->modalAuthRegisterStep1Enabled;
    }

    /**
     * @param bool $modalAuthRegisterStep1Enabled
     */
    public function setModalAuthRegisterStep1Enabled($modalAuthRegisterStep1Enabled)
    {
        $this->modalAuthRegisterStep1Enabled = $modalAuthRegisterStep1Enabled;
    }

    public function renderDataAuthLogin()
    {
        if ($this->modalAuthLoginByPhoneEnabled) {
            $return = $this->renderDataAuthLoginByPhone();
        } elseif ($this->modalAuthLoginByEmailEnabled) {
            $return = $this->renderDataAuthLoginByEmail();
        } else {
            $return = '';
        }

        return $return;
    }

    public function renderDataAuthLoginByPhone()
    {
        return ' data-auth="link" data-target="modal_sign_in_phone" data-auth-action="sign_in"';
    }

    public function renderDataAuthLoginByEmail()
    {
        return ' data-auth="link" data-target="modal_sign_in_email" data-auth-action="sign_in"';
    }

    public function renderDataGtmEventButtonClicks($value = null)
    {
        $return = ' data-gtm-event="ButtonClicks"';
        if ($value) {
            $return .= ' data-gtm-event-value="' . htmlspecialchars($value) . '"';
        }

        return $return;
    }

    public function renderDataGtmEventEcommercePromoClick($value = null)
    {
        if (!$value) {
            return '';
        }

        return ' data-gtm-event="ecommercePromoClick" data-gtm-value="' . htmlspecialchars($value) . '"';
    }

    public function renderDataGtmEventEcommerceClick($pageId, $list, $value = null)
    {
        if (!$value) {
            return '';
        }

        return ' data-gtm-event="ecommerceClick" data-gtm-page-id="' . $pageId . '" data-gtm-list="' . $list . '" data-gtm-value="' . htmlspecialchars($value) . '"';
    }

    public function sortProfStatusOptions($options)
    {
        $return = array();

        $optionNotDoctor = null;
        foreach ($options as $option) {
            if ($option['id'] != SiteUsersUserModel::prof_status_id_not_doctor) {
                $return[] = $option;
            } else {
                $optionNotDoctor = $option;
            }
        }

        if ($optionNotDoctor) {
            $return[] = $optionNotDoctor;
        }

        return $return;
    }

    public function sortUniversityOptions($options)
    {
        $return = array();

        $optionOther = null;
        foreach ($options as $option) {
            if ($option['id'] != SiteUsersUserModel::university_id_other) {
                $return[] = $option;
            } else {
                $optionOther = $option;
            }
        }

        if ($optionOther) {
            $return[] = $optionOther;
        }

        return $return;
    }

    public function sortCountryOptions($options)
    {
        $return = array();

        $optionRussia = null;
        $optionOther = null;
        foreach ($options as $option) {
            switch ($option['id']) {
                case SiteUsersUserModel::country_id_russia:
                {
                    $optionRussia = $option;
                    break;
                }
                case SiteUsersUserModel::country_id_other:
                {
                    $optionOther = $option;
                    break;
                }
                default:
                {
                    $return[] = $option;
                }
            }
        }

        if ($optionRussia) {
            array_unshift($return, $optionRussia);
        }

        if ($optionOther) {
            $return[] = $optionOther;
        }

        return $return;
    }

    public function sortFeedbackFields($fields)
    {
        $return = array();

        $fieldIsDoctor = null;
        $fieldClinicName = null;
        $fieldClinicAddress = null;

        foreach ($fields as $fieldName => $field) {
            switch ($fieldName) {
                case SiteWebformsFormFeedbackModel::field_is_doctor:
                {
                    $fieldIsDoctor = $field;
                    break;
                }
                case SiteWebformsFormFeedbackModel::field_clinic_name:
                {
                    $fieldClinicName = $field;
                    break;
                }
                case SiteWebformsFormFeedbackModel::field_clinic_address: {
                    $fieldClinicAddress = $field;
                    break;
                }
                default:
                {
                    $return[] = $field;
                }
            }
        }

        if($fieldIsDoctor) {
            $return[] = $fieldIsDoctor;
        }

        if($fieldClinicName) {
            $return[] = $fieldClinicName;
        }

        if($fieldClinicAddress) {
            $return[] = $fieldClinicAddress;
        }

        return $return;
    }
}