jQuery(document).ready(function () {
    gtm.init(jQuery);
});

let gtm = new function () {
    this.jQuery = null;

    this.init = function (jQuery) {
        this.jQuery = jQuery;

        window.dataLayer = window.dataLayer || [];

        gtm.jQuery('*[data-gtm-event=ButtonClicks]').on('click', function () {
            let value = gtm.jQuery(this).data('gtm-event-value');
            if (!value) {
                value = gtm.jQuery(this).text();
            }

            gtm.sendGtmEventButtonClicks(value);
        });

        gtm.jQuery('body').on('click', '.top-arrow', function () {
            gtm.sendGtmEventButtonClicks('Вверх');
        });

        gtm.jQuery('*[data-gtm-event=formFilling]').on('submit', function () {
            gtm.sendGtmEventFormFilling(gtm.jQuery(this).data('gtm-event-form'), true);
        });

        gtm.jQuery('*[data-gtm-event=virtualSearch]').each(function () {
            gtm.sendGtmEventVirtualSearch(gtm.jQuery(this).data('gtm-event-value'));
        });

        gtm.jQuery('*[data-gtm-event=ecommercePromoClick]').on('click', function (e) {
            gtm.sendGtmEventEcommercePromoClick(gtm.jQuery(this).data('gtm-value'));
        });

        gtm.jQuery('body').on('click', '*[data-gtm-event=ecommerceClick]', function (e) {
            gtm.sendGtmEventEcommerceLick(gtm.jQuery(this).data('gtm-value'));
            gtm.setEcommerceDetailList(gtm.jQuery(this).data('gtm-page-id'), gtm.jQuery(this).data('gtm-list'));
        });

        gtm.jQuery('*[data-gtm-event=ecommerceDetail]').each(function () {
            let pageId = gtm.jQuery(this).data('gtm-page-id'),
                list = gtm.getEcommerceDetailList(pageId);

            if (list) {
                gtm.removeEcommerceDetailList(pageId)
            } else {
                list = 'Каталог';
            }

            gtm.sendGtmEventEcommerceDetail(list, gtm.jQuery(this).data('gtm-value'));
        });

        gtm.jQuery('body').on('click', '*[data-gtm-event=ecommerceAdd]', function (e) {
            let product = gtm.jQuery(this).data('gtm-value'),
                $ecommerceClick = gtm.jQuery(this).closest('*[data-gtm-event=ecommerceClick]'),
                $amount = gtm.jQuery('#amount_id_' + gtm.jQuery(this).data('page-id')),
                quantity = 1;

            if ($ecommerceClick.length) {
                gtm.sendGtmEventEcommerceDetail($ecommerceClick.data('list'), product);
            }

            if ($amount.length) {
                quantity = $amount.val();
            }

            gtm.sendGtmEventEcommerceAdd(product, quantity);
        });

        gtm.jQuery('*[data-gtm-event=ecommercePurchase]').each(function () {
            gtm.sendGtmEventEcommercePurchase(gtm.jQuery(this).data('gtm-value'), gtm.jQuery(this).data('order_id'));
        });
    }

    this.sendGtmEventButtonClicks = function (value) {
        dataLayer.push({
            'event': 'ButtonClicks',
            'pageUrl': gtm._getPageUrl(),
            'clickText': value
        });
    }

    this.sendGtmEventFormFilling = function (formName, valid) {
        dataLayer.push({
            'event': 'formFilling',
            'formName': formName,
            'fillingStatus': valid ? 'ошибка' : 'успешно',
            'pageUrl': gtm._getPageUrl(),
        });
    }

    this.sendGtmEventVirtualSearch = function (value) {
        dataLayer.push({
            'event': 'VirtualSearch',
            'pageUrl': gtm._getPageUrl(),
            'virtualPageSearch': value,
        });
    }

    this.sendGtmEventEcommercePromoClick = function (data) {
        dataLayer.push({
            'event': 'ecommercePromoClick',
            'ecommerce': {
                'promoClick': {
                    'promotions': [data]
                }
            }
        });
    };

    this.sendGtmEventEcommerceLick = function (data) {
        dataLayer.push({
            'event': 'ecommerceClick',
            'ecommerce': {
                'click': data
            }
        });
    };

    this.sendGtmEventEcommerceDetail = function (list, product) {
        dataLayer.push({
            'event': 'ecommerceDetail',
            'ecommerce': {
                'detail': {
                    'actionField': {
                        'list': list
                    },
                    'products': [
                        product
                    ]
                }
            }
        });
    };

    this.sendGtmEventEcommerceBrackets = function (ids) {
        gtm.jQuery.ajax({
            url: '/udata/exchange/getExchangeEcommerceBracketsData/' + ids + '/',
            dataType: 'json',
            success: function(data) {
                if(!!data['products']) {
                    for(let product of data['products']) {
                        gtm.sendGtmEventEcommerceDetail('Каталог', product);
                        gtm.sendGtmEventEcommerceAdd(product, product['quantity']);
                    }
                }
            }
        });
    }

    this.sendGtmEventEcommerceAdd = function (product, quantity) {
        if (!quantity) {
            quantity = 1;
        }

        product['quantity'] = quantity;

        dataLayer.push({
            'event': 'ecommerceAdd',
            'ecommerce': {
                'add': {
                    'products': [
                        product
                    ]
                }
            }
        });
    };

    this.sendGtmEventEcommerceRemove = function (product, quantity) {
        if (!quantity) {
            quantity = 1;
        }

        product['quantity'] = quantity;

        dataLayer.push({
            'event': 'ecommerceRemove',
            'ecommerce': {
                'remove': {
                    'products': [
                        product
                    ]
                }
            }
        });
    };

    this.sendGtmEventEcommercePurchase = function (data, order_id) {
        jQuery.ajax({
            url: '/udata/emarket/ga_send_ecommerce_purchase',
            dataType: 'html',
            method: 'POST',
            data: {
                url: window.location.href,
                domain: window.location.host,
                title: gtm.jQuery('title').text(),
                order_id: order_id,
                navigator: navigator.appName,
            }
        });
    };

    this.setEcommerceDetailList = function (pageId, list) {
        window.localStorage.setItem(this._getLocalStorageEcommerceDetailListKey(pageId), list);
    }

    this.getEcommerceDetailList = function (pageId) {
        return window.localStorage.getItem(this._getLocalStorageEcommerceDetailListKey(pageId));
    }

    this.removeEcommerceDetailList = function (pageId) {
        window.localStorage.removeItem(this._getLocalStorageEcommerceDetailListKey(pageId));
    }

    this._getLocalStorageEcommerceDetailListKey = function (pageId) {
        return 'ecommerceDetailList_' + pageId;
    }

    this._getPageUrl = function () {
        return location.protocol + '//' + location.host + location.pathname;
    }
};