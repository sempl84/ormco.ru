let webforms = new function () {
    this.init = function () {
        $('#ya_vrach').on('change', function() {
            webforms._toggleFeedbackDoctor();
        });

        $('form[data-webforms=form]').on('submit', function(e) {
            e.preventDefault();

            let $form = $(this),
                formId = jQuery('input[name="system_form_id"]', $form).val();

            jQuery.ajax({
                url: '/udata/' + $form.attr('action') + '.json',
                type: 'POST',
                dataType: 'json',
                data: $form.serialize(),
                success: function (answer) {
                    if (!!answer['status']) {
                        if (answer['status'] === 'error') {
                            console.log(answer);
                        } else {
                            openFancybox('#modal_feedback_success');
                        }
                    }
                },
                error: function (data) {
                    console.log('error');
                    console.log(data);
                }
            });

            try {
                // seng ga event
                if (formId === '351') {
                    ga('send', 'event', 'form', 'question');
                    ym(10184890, 'reachGoal', 'QuestionSent');
                }
            } catch (err) {
                console.log(err);
            }
        });

        this._toggleFeedbackDoctor();
    };

    this._toggleFeedbackDoctor = function () {
        if($('#ya_vrach').is(':checked')) {
            $('#nazvanie_kliniki').closest('.input-field').show();
            $('#adres_kliniki').closest('.input-field').show();
        } else {
            $('#nazvanie_kliniki').closest('.input-field').hide();
            $('#adres_kliniki').closest('.input-field').hide();
        }
    }
};

$(document).ready(function () {
    webforms.init();
});