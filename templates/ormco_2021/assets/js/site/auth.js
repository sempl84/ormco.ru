let auth = new function () {
    this.$formSendCode = null;
    this.$formValidateCode = null;
    this.$inputCode = null;
    this.$phoneCodeTimer = null;
    this.$formRegisterStep1 = null;
    this.$inputProfStatus = null;
    this.$formRegisterStep2 = null;
    this.$inputCountry = null;
    this.$inputRegion = null;
    this.$inputAddress = null;
    this.signInLastStep = null;

    this.init = function () {
        this.$formSendCode = $('form[data-auth=sign-in-phone-send-code]');
        this.$formValidateCode = $('#modal_sign_in_validate_phone_code_form');
        this.$inputCode = $('#modal_sign_in_validate_phone_code_input_code');
        this.$phoneCodeTimer = $('#sign_in_phone_code_timer');
        this.$formRegisterStep1 = $('#modal_register_step_1_form');
        this.$inputProfStatus = $('input[data-register=prof-status]');
        this.$formRegisterStep2 = $('#modal_register_step_2_form');
        this.$inputCountry = $('#modal_register_step_2_form_input_country');
        this.$inputRegion = $('#modal_register_step_2_form_input_region');
        this.$inputAddress = $('#modal_register_step_2_form_input_address');

        let $body = $('body');

        $body.on('click', '*[data-auth=link]', function (e) {
            e.preventDefault();

            let target = '#' + $(this).data('target');

            if($(this).data('auth-action') === 'sign_in' && auth.signInLastStep) {
                target = auth.signInLastStep;
            }

            openFancybox(target);
        });

        $('form[data-auth=sign-in-email-form]').on('submit', function (e) {
            e.preventDefault();

            let $form = $(this);

            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $form.serialize(),
                success: function (response) {
                    if (!!response['success']) {
                        window.location.reload();
                    } else {
                        alert(response['error']);
                    }
                }
            });
        });

        this.$formSendCode.on('submit', function (e) {
            e.preventDefault();

            let phone = $('#modal_sign_in_phone_input_phone').val();

            auth._sendPhoneCode(function (response) {
                if (!!response['success']) {
                    $('*[data-auth=sign-in-phone-code-phone-span]').text(phone);
                    $('*[data-auth=sign-in-phone-code-phone-input]').val(phone);

                    openFancybox('#modal_sign_in_validate_phone_code');
                } else if(response['error'] == 'no_register'){
                    openFancybox('#modal_no_register');
                } else {
                    alert(response['error']);
                }
            });
        });

        this.$formValidateCode.on('submit', function (e) {
            e.preventDefault();

            let $errorContainer = $('#modal_sign_in_validate_phone_code_error');

            $.ajax({
                url: auth.$formValidateCode.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: auth.$formValidateCode.serialize(),
                success: function (response) {
                    let error = '';

                    if (!!response['error']) {
                        error = response['error'];
                    } else {
                        if (!!response['action']) {
                            if (response['action'] === 'auth') {
                                window.location.reload();
                            } else if (response['action'] === 'register') {
                                let target = auth._getRegisterModalTarget();

                                if(target) {
                                    openFancybox(target);
                                } else {
                                    window.location.href = '/users/registrate/';
                                }
                            } else {
                                error = 'Неизвестный ответ от сервера';
                            }
                        } else {
                            error = 'Неизвестный ответ от сервера';
                        }
                    }

                    if (error) {
                        $errorContainer.text(error).show();
                    }
                }
            });
        });

        this.$inputCode.pincodeInput({
            inputs: 4,
            complete: function () {
                if (auth.$inputCode.data('code') !== auth.$inputCode.val()) {
                    auth.$inputCode.data('code', auth.$inputCode.val());
                    auth.$formValidateCode.trigger('submit');
                }
            }
        });

        $(document).on('afterLoad.fb', function (e, instance) {
            auth.signInLastStep = '';

            if (instance['current']['src'] === '#modal_sign_in_validate_phone_code') {
                $('#modal_sign_in_validate_phone_code_error').hide().text('');
                auth.$inputCode.pincodeInput().data('plugin_pincodeInput').clear();
                auth.$inputCode.pincodeInput().data('plugin_pincodeInput').focus();
                auth._startPhoneCodeTimer();
            } else if(instance['current']['src'] === '#modal_register_step_1' || instance['current']['src'] === '#modal_register_step_2') {
                auth.signInLastStep = instance['current']['src'];
            }
        });

        $(document).on('beforeClose.fb', function (e, instance) {
            if (instance['current']['src'] === '#modal_sign_in_validate_phone_code') {
                auth._stopPhoneCodeTimer();
                auth._updatePhoneCodeTimerText(false);
            }
        });

        $body.on('click', '#sign_in_phone_code_timer > a', function (e) {
            e.preventDefault();

            auth._startPhoneCodeTimer();
        });

        this.$formRegisterStep1.on('submit', function (e) {
            e.preventDefault();

            openFancybox('#modal_register_step_2');
        });

        this.$inputProfStatus.on('change', function () {
            let $inputFieldHidden = $(this).closest('.input-field').find('.input-field-hidden');

            $(this).closest('form').find('.input-field-hidden').slideUp(200);

            if ($inputFieldHidden.length) {
                $inputFieldHidden.slideDown(200);
            }
        });

        if(this.$inputAddress) {
            this.$inputAddress.suggestions({
                token: this.$inputAddress.data('token'),
                type: "ADDRESS",
                onSearchStart: function () {
                    auth.$inputAddress.data('suggestion', null);
                },
                onSelect: function (suggestion) {
                    auth.$inputAddress.data('suggestion', suggestion);
                }
            });
        } else {
            this.$inputCountry.on('change', function () {
                auth._toggleRegionOptions();
            });

            this._toggleRegionOptions();
        }

        this.$formRegisterStep2.on('submit', function (e) {
            e.preventDefault();

            if(auth.$inputAddress) {
                let suggestion = auth.$inputAddress.data('suggestion');

                if(!suggestion) {
                    alert('Введите адрес');
                    return false;
                }

                if(!suggestion['data']['house']) {
                    alert('Не указан дом');
                    return false;
                }

                $('#modal_register_step_2_form_input_address_data').val(JSON.stringify(suggestion));
            }

            let data = [].concat(auth.$formValidateCode.serializeArray(), auth.$formRegisterStep1.serializeArray(), auth.$formRegisterStep2.serializeArray());

            $.ajax({
                url: '/users/sign_in/register/',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (response) {
                    let error = '';

                    if (!!response['error']) {
                        error = response['error'];
                    } else {
                        if (!!response['action'] && response['action'] === 'auth') {
                            window.location.reload();
                        } else {
                            error = 'Неизвестный ответ от сервера';
                        }
                    }

                    if (error) {
                        alert(error);
                    }
                }
            });
        });
    };

    this._sendPhoneCode = function (handler) {
        $.ajax({
            url: auth.$formSendCode.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: auth.$formSendCode.serialize(),
            success: handler
        });
    };

    this._startPhoneCodeTimer = function () {
        auth._stopPhoneCodeTimer();

        let timeLeft = 59;

        let intervalId = setInterval(function () {
            timeLeft--;

            if (timeLeft === 0) {
                auth._stopPhoneCodeTimer();
                auth.$phoneCodeTimer.removeData('interval-id');
            }

            auth._updatePhoneCodeTimerText(timeLeft);
        }, 1000);

        auth._updatePhoneCodeTimerText(timeLeft);
        auth.$phoneCodeTimer.data('interval-id', intervalId);
    }

    this._stopPhoneCodeTimer = function () {
        if (auth.$phoneCodeTimer.data('interval-id') !== 'undefined') {
            clearInterval(auth.$phoneCodeTimer.data('interval-id'));
        }
    };

    this._updatePhoneCodeTimerText = function (timeLeft) {
        if (timeLeft === false) {
            auth.$phoneCodeTimer.html('');
        } else if (timeLeft > 0) {
            auth.$phoneCodeTimer.text('Получить новый код можно через 00:' + timeLeft.toString().padStart(2, '0'));
        } else {
            auth.$phoneCodeTimer.html('<a href="#">Получить новый код</a>');
        }
    };

    this._toggleRegionOptions = function () {
        let $countryOption = auth.$inputCountry.find('option:selected'),
            bCountryOptionSelected = $countryOption.val() !== '',
            bDisableRegionNotRussia = !bCountryOptionSelected || ($countryOption.data('option') === 'country-russia'),
            bUpdateSelected = true;

        auth.$inputRegion.find('option').each(function () {
            $(this).attr('selected', false);

            if ($(this).data('option') === 'region-not-russia') {
                $(this).attr('disabled', bDisableRegionNotRussia);
                if (!bDisableRegionNotRussia) {
                    $(this).attr('selected', true);
                }
            } else {
                $(this).attr('disabled', !bDisableRegionNotRussia);

                if (bDisableRegionNotRussia && bCountryOptionSelected && bUpdateSelected && $(this).val()) {
                    $(this).attr('selected', true);

                    bUpdateSelected = false;
                }
            }
        });

        auth.$inputRegion.trigger('refresh');
    }

    this._getRegisterModalTarget = function () {
        let target = '';

        if(auth.$formRegisterStep1.length) {
            target = '#modal_register_step_1';
        } else if(auth.$formRegisterStep2.length) {
            target = '#modal_register_step_2';
        }

        return target;
    };
};

$(document).ready(function () {
    auth.init();
});