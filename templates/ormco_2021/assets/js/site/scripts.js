function openFancybox(target) {
    $.fancybox.close();

    $.fancybox.open({
        src: target,
        autoFocus: false,
        touch: false,
        closeBtn: false,
        btnTpl: {
            close: '',
            smallBtn: ''
        }
    });
}

$(document).ready(function() {
    $('a[data-action=submit]').on('click', function(e) {
        e.preventDefault();

        let target = $(this).data('target');

        if(target) {
            $(target).trigger('submit');
        } else {
            $(this).closest('form').trigger('submit');
        }
    });

    $('a[data-action=fancybox]').fancybox({
        autoFocus: false,
        touch: false,
        closeBtn: false,
        btnTpl: {
            close: '',
            smallBtn: ''
        }
    });

    $('.input-float').each(function() {
        if($(this).find('input').val()) {
            $(this).addClass('focused');
        }
    });
});