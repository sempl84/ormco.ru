let subscribe = new function () {
    this.init = function () {
        let $form = $('#subscribe_form');

        $form.on('submit', function (e) {
            e.preventDefault();

            $form.find('div.input-float').removeClass('notvalid');
            $form.find('input.input').removeClass('input-not-valid');
            $form.find('div.tooltip').text('').hide();

            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $form.serialize(),
                success: function (response) {
                    if(!!response['success']) {
                        let $result = $('#subscribe_result'),
                            success = $form.data('success');

                        $form.hide();

                        $result.find('div.subscribe_title').text(success['title']);
                        $result.find('p').text(success['text']);
                    } else {
                        let error = '';

                        if(!!response['error']) {
                            error = response['error'];
                        } else {
                            error = 'Неизвестная ошибка';
                        }

                        $form.find('div.input-float').addClass('notvalid');
                        $form.find('input.input').addClass('input-not-valid');
                        $form.find('div.tooltip').text(error).show();
                    }
                }
            })
        });
    };
};

$(document).ready(function () {
    subscribe.init();
});