<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration212CategoryChooseBrackets
{
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createCategoryObjectTypeFields();
    }
    
    private function createCategoryObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteCatalogCategoryModel::module, SiteCatalogCategoryModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteCatalogCategoryModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteCatalogCategoryModel::group_choose_brackets_block, 'Блок Выбор брекетов');
        $field = new UmiSpecInstallerField(SiteCatalogCategoryModel::field_choose_brackets_block_hide_button, 'Скрыть кнопку', $this->installer->getFieldTypeId('boolean'));
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration212CategoryChooseBrackets();
$migration->execute();

echo 'Готово';
exit;