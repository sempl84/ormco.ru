<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration342ExchangeGa {
    
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createOrderObjectTypeFields();
        $this->createUserObjectTypeFields();
        $this->createSettingsObjectTypeFields();
    }
    
    private function createOrderObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderModel::group_ga, 'Google Analytics');
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_ga_cid, 'GA: ClientId', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_ga_order_postfix, 'GA: Постфикс к номеру заказа', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_ga, 'Google Analytics');
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_ga_cid, 'Ga: ClientId', $this->installer->getFieldTypeId('string'));
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_counters);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_counters_gtm_account_id, 'GoogleTagManager: Id аккаунта', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $field->setInFilter(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_counters_gtm_refund_source_id, 'GoogleTagManager: Id источника для отмены заказа', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $field->setInFilter(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration342ExchangeGa();
$migration->execute();

echo 'Готово';
exit;