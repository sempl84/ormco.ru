<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration245OrmcoCouponsRemoveFieldActive
{
    /**
     * @var $installer UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->updateCouponObjectTypeFields();
    }
    
    /**
     * @throws publicException
     */
    private function updateCouponObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании объектного типа данных');
        }
        
        $group = $objectType->getFieldsGroupByName(SiteOrmcoCouponsCouponModel::group_properties);
        if(!$group instanceof umiFieldsGroup) {
            throw new publicException('Не найдена группа полей ' . SiteOrmcoCouponsCouponModel::group_properties);
        }
        
        $fieldId = $objectType->getFieldId('is_active');
        if(!$fieldId) {
            throw new publicException('Не найдено поле is_active');
        }
        
        $group->detachField($fieldId);
        $group->commit();
        $objectType->commit();
        
        umiFieldsCollection::getInstance()->delField($fieldId);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration245OrmcoCouponsRemoveFieldActive();
$migration->execute();

echo 'Готово';
exit;