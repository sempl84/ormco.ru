<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration287AbandonedOrdersTemplate2
{
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createOrderObjectTypeFields();
    }
    
    private function createOrderObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderModel::group_abandoned_orders);
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_abandoned_orders_message_date, 'Дата отправки последнего письма о брошенной корзине', $this->installer->getFieldTypeId('date'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration287AbandonedOrdersTemplate2();
$migration->execute();

echo 'Готово';
exit;