<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class SiteMigration355Redesign
{
    
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createTemplate();
        
        $this->createMenuHeader();
        $this->createMenuPanel();
        $this->createMenuFooter();
        
        $this->createSliderItemObjectTypeFields();
        
        $this->createPageSettingsObjectTypeFields();
        
        $this->createUserObjectTypeFields();
        
        $this->setOrmcoUniSenderSubscribePermissions();
        
        $this->installOrmcoSiteDataModule();
    }
    
    private function createTemplate()
    {
        $templateName = 'ormco_2021';
        
        $bAdd = true;
        
        $templatesCollection = templatesCollection::getInstance();
        
        foreach ($templatesCollection->getFullTemplatesList() as $template) {
            if (!$template instanceof template) {
                continue;
            }
            
            if ($template->getName() != $templateName) {
                continue;
            }
            
            $bAdd = false;
            break;
        }
        
        if ($bAdd) {
            $templateId = $templatesCollection->addTemplate('default.phtml', 'Редизайн', 1, 1);
            if (!$templateId) {
                throw new publicException('Ошибка при создании шаблона');
            }
            
            $template = $templatesCollection->getTemplate($templateId);
            if (!$template instanceof template) {
                throw new publicException('Не найден шаблон ' . $templateId);
            }
            
            $template->setName('ormco_2021');
            $template->setType('php');
            $template->commit();
        }
    }
    
    private function createMenuHeader()
    {
        $hierarchy = <<<HIERARCHY
[{"rel":293,"link":"\/discount\/","name":"\u0410\u043a\u0446\u0438\u0438","isactive":1,"isdeleted":0},{"rel":13574,"link":"\/market\/brekety\/damon-system\/metallicheskie\/damon-q2\/","name":"\u041a\u043e\u043d\u0444\u0438\u0433\u0443\u0440\u0430\u0442\u043e\u0440 \u0431\u0440\u0435\u043a\u0435\u0442 \u0441\u0438\u0441\u0442\u0435\u043c","isactive":1,"isdeleted":0},{"rel":"custom","link":"https:\/\/orthodontia.ru\/","name":"\u041e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u043e\u0440\u0442\u0430\u043b","isactive":true},{"rel":8,"link":"\/contact\/","name":"\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b","isactive":1,"isdeleted":0},{"rel":289,"link":"\/delivery\/","name":"\u041e\u043f\u043b\u0430\u0442\u0430 \u0438 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430","isactive":1,"isdeleted":0}]
HIERARCHY;
        
        $this->installer->createMenu('redesign_header', 'Редизайн: Шапка', $hierarchy, true);
    }
    
    private function createMenuPanel()
    {
        $hierarchy = <<<HIERARCHY
[{"rel":4,"link":"/market/","name":"Каталог","isactive":1},{"rel":293,"link":"/discount/","name":"Акции","isactive":1},{"rel":8,"link":"/contact/","name":"Контакты","isactive":1},{"rel":289,"link":"/delivery/","name":"Оплата и доставка","isactive":1},{"rel":296,"link":"/issledovaniya/","name":"Статьи и исследования","isactive":1},{"rel":5,"link":"/novelties/","name":"Новости","isactive":1},{"rel":"custom","link":"#","name":"Клиентам","isactive":true,"children":[{"rel":13010,"link":"/oformlenie-zakaza/","name":"Как оформить заказ","isactive":1},{"rel":9,"link":"/company/","name":"О компании","isactive":1},{"rel":237,"link":"/dealers/","name":"Дилеры","isactive":1},{"rel":"custom","link":"https://www.diadoc.ru/lp-ormco","name":"ЭДО","isactive":true}]}]
HIERARCHY;
        
        $this->installer->createMenu('redesign_panel', 'Редизайн: Панель сбоку', $hierarchy, true);
    }
    
    private function createMenuFooter()
    {
        $hierarchy = <<<HIERARCHY
[{"rel":9,"link":"/company/","name":"О компании","isactive":1},{"rel":13008,"link":"/politika-konfidencial-nosti/","name":"Политика конфиденциальности","isactive":1}]
HIERARCHY;
        
        $this->installer->createMenu('redesign_footer', 'Редизайн: Подвал', $hierarchy, true);
    }
    
    private function createSliderItemObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteSliderItemElement::module, SiteSliderItemElement::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteSliderItemElement::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteSliderItemElement::group_redesign, 'Редизайн');
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_is_active, 'Активность', $this->installer->getFieldTypeId('boolean'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_title, 'Заголовок', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_text, 'Текст', $this->installer->getFieldTypeId('wysiwyg'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_button_text, 'Текст кнопки', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_image, 'Изображение', $this->installer->getFieldTypeId('img_file'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteSliderItemElement::field_redesign_link, 'Ссылка', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createPageSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiOBjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_redesign_pages, 'Редизайн: Страницы');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_redesign_pages_policy, 'Политика конфиденциальности', $this->installer->getFieldTypeId('symlink', true));
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_redesign_warn, 'Редизайн: Предупреждение');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_redesign_warn_is_active, 'Активность', $this->installer->getFieldTypeId('boolean'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_redesign_warn_title, 'Заголовок', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_redesign_warn_content, 'Контент', $this->installer->getFieldTypeId('wysiwyg'));
        $field->setInSearch(false);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_redesign_warn_link, 'Ссылка', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_system, 'Системные свойства');
        $group->setVisible(false);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_system_normalized_phone, 'Телефон (нормализованный)', $this->installer->getFieldTypeId('string'));
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function setOrmcoUniSenderSubscribePermissions()
    {
        permissionsCollection::getInstance()->setModulesPermissions(Service::SystemUsersPermissions()->getGuestUserId(), 'ormcoUniSender', 'subscribe');
    }
    
    private function installOrmcoSiteDataModule()
    {
        cmsController::getInstance()->installModule('classes/modules/ormcoSiteData/install.php');
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration355Redesign();
$migration->execute();

echo 'Готово';
exit;