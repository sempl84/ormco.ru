<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration205OrmcoCoupons
{
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createCouponObjectType();
        $this->createOrderObjectTypeFields();
        $this->createOrderItemObjectTypeFields();
        
        $this->createHistoryObjectType();
        
        $this->createSettingsObjectTypeFields();
        
        $this->createUserObjectTypeFields();
        
        $this->setOrmcoCouponsPermissions();
    }
    
    /**
     * @throws publicException
     */
    private function createCouponObjectType()
    {
        $installerHierarchyType = new UmiSpecInstallerHierarchyType(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method, 'Промокоды: Промокод');
        $hierarchyType = $this->installer->createHierarchyType($installerHierarchyType);
        if (!$hierarchyType instanceof umiHierarchyType) {
            throw new publicException('Ошибка при создании иерархического типа данных');
        }
        
        $installerObjectType = new UmiSpecInstallerObjectType($installerHierarchyType->getTitle(), '', false);
        $installerObjectType->setHierarchyTypeId($hierarchyType->getId());
        $installerObjectType->setIsGuidable(true);
        $objectType = $this->installer->createObjectTypeByHierarchyTypeId($installerObjectType);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании объектного типа данных');
        }
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsCouponModel::group_properties, 'Свойства');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_is_active, 'Активность', $this->installer->getFieldTypeId('boolean'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_date_begin, 'Дата начала', $this->installer->getFieldTypeId('date'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_date_end, 'Дата окончания', $this->installer->getFieldTypeId('date'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_user_id, 'Пользователь', $this->installer->getFieldTypeId('relation'));
        $field->setGuideId($this->installer->getObjectTypeIdByGUID('users-user'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_code, 'Код', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsCouponModel::group_discount, 'Скидка');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_discount_base, 'Базовая скидка', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_discount_focus, 'Фокусная скидка', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_discount_focus_page_id, 'Товары для фокусной скидки', $this->installer->getFieldTypeId('symlink', true));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_discount_focus_categories, 'Разделы для фокусной скидки', $this->installer->getFieldTypeId('text'));
        $field->setTip('Названия разделов');
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsCouponModel::group_seminar, 'Семинар');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_seminar_id, 'Id семинара', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_seminar_name, 'Название семинара', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createOrderObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderModel::group_ormco_coupon, 'Промокод');
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_ormco_coupon_id, 'Id промокода', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_ormco_coupon_code, 'Промокод', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_ormco_coupon_discount_value, 'Скидка: Размер', $this->installer->getFieldTypeId('float'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createOrderItemObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderItemModel::module, SiteEmarketOrderItemModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderItemModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderItemModel::group_ormco_coupon, 'Промокод');
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_id, 'Id промокода', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_code, 'Промокод', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_discount_value, 'Скидка: Размер', $this->installer->getFieldTypeId('float'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_discount_percent, 'Скидка: Процент', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_focus_discount, 'Скидка на фокусный товар', $this->installer->getFieldTypeId('boolean'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    /**
     * @throws publicException
     */
    private function createHistoryObjectType()
    {
        $installerHierarchyType = new UmiSpecInstallerHierarchyType(SiteOrmcoCouponsHistoryModel::module, SiteOrmcoCouponsHistoryModel::method, 'Промокоды: История');
        $hierarchyType = $this->installer->createHierarchyType($installerHierarchyType);
        if (!$hierarchyType instanceof umiHierarchyType) {
            throw new publicException('Ошибка при создании иерархического типа данных');
        }
        
        $installerObjectType = new UmiSpecInstallerObjectType($installerHierarchyType->getTitle(), '', false);
        $installerObjectType->setHierarchyTypeId($hierarchyType->getId());
        $installerObjectType->setIsGuidable(true);
        $objectType = $this->installer->createObjectTypeByHierarchyTypeId($installerObjectType);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании объектного типа данных');
        }
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsHistoryModel::group_properties, 'Свойства');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_used, 'Использован', $this->installer->getFieldTypeId('boolean'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_date, 'Дата', $this->installer->getFieldTypeId('date'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_user_id, 'Пользователь', $this->installer->getFieldTypeId('relation'));
        $field->setGuideId($this->installer->getObjectTypeIdByGUID('users-user'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_1c_id, 'Id в 1С', $this->installer->getFieldTypeId('string'));
        $field->setGuideId($this->installer->getObjectTypeIdByGUID('users-user'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsHistoryModel::group_coupon, 'Промокод');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_coupon_name, 'Название', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsHistoryModel::field_coupon_percent, 'Скидка в процентах', $this->installer->getFieldTypeId('int'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_discounts, 'Страница: Мои скидки');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_discounts_personal_title, 'Персональная скидка: Заголовок', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_discounts_personal_content, 'Персональная скидка: Контент', $this->installer->getFieldTypeId('wysiwyg'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_coupons, 'Промокоды');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_multiple_add_to_cart_content, 'Несколько промокодов: Текст при добавлении в корзину', $this->installer->getFieldTypeId('wysiwyg'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_multiple_cart_content, 'Несколько промокодов: Текст в корзине', $this->installer->getFieldTypeId('wysiwyg'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_coupons_notification, 'Промокоды: Уведомление');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_notification_sender_name, 'Отправитель: Имя', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_notification_sender_email, 'Отправитель: E-mail', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_notification_subject, 'Тема письма', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_notification_content, 'Текст письма', $this->installer->getFieldTypeId('wysiwyg'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_coupons_notification_debug_emails, 'E-mail адреса для проверки', $this->installer->getFieldTypeId('text'));
        $field->setTip('Если поле заполнено, уведомление будет отправлено только тем пользователям, адреса которых указаны в поле');
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function createUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_coupons, 'Промокоды');
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_coupons_system_field, 'Информация по промокодам - системное поле', $this->installer->getFieldTypeId('string'));
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function setOrmcoCouponsPermissions()
    {
        $permissionsCollection = permissionsCollection::getInstance();
        $permissionsCollection->setModulesPermissions(334, 'ormcoCoupons', 'site');
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration205OrmcoCoupons();
$migration->execute();

echo 'Готово';
exit;