<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration341OrmcoCouponsDisableUpdatePrice
{
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createOrderItemObjectTypeFields();
    }
    
    private function createOrderItemObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderItemModel::module, SiteEmarketOrderItemModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderItemModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderItemModel::group_ormco_coupon);
        $field = new UmiSpecInstallerField(SiteEmarketOrderItemModel::field_ormco_coupon_disable_update_price, 'Не применять скидку по купона для расчета цены', $this->installer->getFieldTypeId('boolean'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration341OrmcoCouponsDisableUpdatePrice();
$migration->execute();

echo 'Готово';
exit;