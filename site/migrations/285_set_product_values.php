<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration285SetProductValues
{
    public function execute()
    {
        $this->setProductValues();
    }
    
    private function setProductValues()
    {
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
        $sel->where('hierarchy')->page(14130);
        $sel->where('is_active')->equals(1);
        $sel->order('id')->asc();
        
        foreach($sel as $element) {
            if(!$element instanceof umiHierarchyElement) {
                continue;
            }
            
            $element->setValue('photo', new umiImageFile('./images/cms/data/products/symetri_bracket_1-1_web_190.jpg'));
            $element->setValue('photos', array(new umiImageFile('./images/cms/data/products/symetri_bracket_1-1_web.png')));
            $element->commit();
        }
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration285SetProductValues();
$migration->execute();

echo 'Готово';
exit;