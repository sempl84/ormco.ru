<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration263ExchangeEcommerce
{
    public function execute()
    {
        $this->setExchangeEcommercePermissions();
    }
    
    private function setExchangeEcommercePermissions()
    {
        $permissions = permissionsCollection::getInstance();
        
        $permissions->setModulesPermissions(UmiCms\Service::SystemUsersPermissions()->getGuestUserId(), 'exchange', 'ecommerce');
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration263ExchangeEcommerce();
$migration->execute();

echo 'Готово';
exit;