<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration225OrmcoCouponsState
{
    /**
     * @var $installer UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createCouponStateObjectType();
        $this->createCouponStateGuideItems();
        $this->createCouponObjectTypeFields();
    }
    
    private $couponStateObjectTypeId;
    
    private function createCouponStateObjectType()
    {
        $installerHierarchyType = new UmiSpecInstallerHierarchyType(SiteOrmcoCouponsCouponStateModel::module, SiteOrmcoCouponsCouponStateModel::method, 'Промокоды: Статус промокода');
        $hierarchyType = $this->installer->createHierarchyType($installerHierarchyType);
        if (!$hierarchyType instanceof umiHierarchyType) {
            throw new publicException('Ошибка при создании иерархического типа данных');
        }
        
        $installerObjectType = new UmiSpecInstallerObjectType($installerHierarchyType->getTitle(), '', false);
        $installerObjectType->setHierarchyTypeId($hierarchyType->getId());
        $installerObjectType->setIsGuidable(true);
        $objectType = $this->installer->createObjectTypeByHierarchyTypeId($installerObjectType);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании объектного типа данных');
        }
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsCouponStateModel::group_properties, 'Свойства');
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponStateModel::field_code, 'Код', $this->installer->getFieldTypeId('string'));
        $field->setRequired(true);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
        
        $this->couponStateObjectTypeId = $objectType->getId();
    }
    
    private function createCouponStateGuideItems()
    {
        $arGuideItems = array(
            SiteOrmcoCouponsCouponStateModel::code_active => 'Активен',
            SiteOrmcoCouponsCouponStateModel::code_freeze => 'Заморожен',
            SiteOrmcoCouponsCouponStateModel::code_inactive => 'Неактивен',
        );
        
        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName(SiteOrmcoCouponsCouponStateModel::module, SiteOrmcoCouponsCouponStateModel::method);
        
        $sel = new selector('objects');
        $sel->types('object-type')->id($objectTypeId);
        $sel->where(SiteOrmcoCouponsCouponStateModel::field_code)->equals(array_keys($arGuideItems));
        $sel->order('id')->asc();
        
        foreach($sel as $object) {
            if(!$object instanceof umiObject) {
                continue;
            }
            
            $code = $object->getValue(SiteOrmcoCouponsCouponStateModel::field_code);
            $object->setName($arGuideItems[$code]);
            $object->commit();
            
            unset($arGuideItems[$code]);
        }
        
        if(count($arGuideItems)) {
            $umiObjectsCollection = umiObjectsCollection::getInstance();
            
            foreach($arGuideItems as $code => $name) {
                $objectId = $umiObjectsCollection->addObject($name, $objectTypeId);
                if(!$objectId) {
                    throw new publicException('Ошибка при создании объекта ' . $code);
                }
                
                $object = $umiObjectsCollection->getObject($objectId);
                if(!$object instanceof umiObject) {
                    throw new publicException('Ошибка при создании объекта ' . $code);
                }
                
                $object->setValue(SiteOrmcoCouponsCouponStateModel::field_code, $code);
                $object->commit();
            }
        }
    }
    
    /**
     * @throws publicException
     */
    private function createCouponObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteOrmcoCouponsCouponModel::module, SiteOrmcoCouponsCouponModel::method);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании объектного типа данных');
        }
        
        $group = new UmiSpecInstallerGroup(SiteOrmcoCouponsCouponModel::group_properties);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_state, 'Статус', $this->installer->getFieldTypeId('relation'));
        $field->setGuideId($this->couponStateObjectTypeId);
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteOrmcoCouponsCouponModel::field_log, 'Лог', $this->installer->getFieldTypeId('text'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration225OrmcoCouponsState();
$migration->execute();

echo 'Готово';
exit;