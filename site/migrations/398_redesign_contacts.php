<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

class SiteMigration398RedesignContacts
{
    
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createPageSettingsObjectTypeFields();
    }
    
    private function createPageSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiOBjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_contacts, 'Контактная информация');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_contacts_email, 'Email', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration398RedesignContacts();
$migration->execute();

echo 'Готово';
exit;