<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class SiteMigration313ConfigCountersGtmId
{
    /**
     * @var umiHierarchyElement
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createSettingsObjectTypeFields();
    }
    
    private function createSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_counters, 'Счётчики');
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_counters_gtm_id, 'GoogleTagManager: Id', $this->installer->getFieldTypeId('string'));
        $field->setInSearch(false);
        $field->setInFilter(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration313ConfigCountersGtmId();
$migration->execute();

echo 'Готово';
exit;