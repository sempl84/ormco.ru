<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

if(isset($_SERVER['HTTP_HOST'])) {
    echo 'Script should be executed via console' . PHP_EOL;
    exit;
}

define('CRON', 'CLI');
define('SKIP_CHECK_AUTH', true);

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$options = getopt('a::', array('action::'));
$action = getArrayKey($options, 'a');
if(!$action) {
    $action = getArrayKey($options, 'action');
}

$class = new OrmcoUniOneSendAbandonedOrdersMessage();

switch($action) {
    case 'init': {
        $class->init();
        $class->process();
        break;
    }
    case 'process': {
        $class->process();
        break;
    }
    default: {
        echo 'Неизвестное действие ' . $action;
    }
}