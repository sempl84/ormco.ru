<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

if(isset($_SERVER['HTTP_HOST'])) {
    echo 'Script should be executed via console' . PHP_EOL;
    exit;
}

define('CRON', 'CLI');
define('SKIP_CHECK_AUTH', true);

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$buffer = outputBuffer::current('CLIOutputBuffer');

$options = getopt('a::', array('action::'));
$action = getArrayKey($options, 'a');
if(!$action) {
    $action = getArrayKey($options, 'action');
}

$class = new OrmcoUniSendersSyncContacts();

try {
    switch($action) {
        case 'init': {
            $registry = regedit::getInstance();
            
            $emailListId = $registry->getVal(ormcoUniSender::registry_param_contacts_list_id);
            if(!$emailListId) {
                throw new publicException('Не выбран список контактов для импорта');
            }
    
            $testEmail = trim($registry->getVal(ormcoUniSender::registry_param_test_email));
            if($testEmail) {
                $testEmail = explode(',', $testEmail);
            }
    
            $class->init($emailListId, $testEmail);
            $class->process();
            break;
        }
        case 'process': {
            $class->process();
            break;
        }
        default: {
            throw new publicException('Неизвестное действие ' . $action);
        }
    }
} catch (Exception $e) {
    $buffer->push('Ошибка: ' . $e->getMessage());
}

$buffer->send();
