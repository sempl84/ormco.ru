<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2018, Evgenii Ioffe
 */

session_start();

chdir(dirname(dirname(__FILE__)));
require_once 'standalone.php';

if(!defined('SKIP_CHECK_AUTH')) {
    $auth = UmiCms\Service::Auth();
    
    try {
        $auth->loginByEnvironment();
    } catch (UmiCms\System\Auth\AuthenticationException $e) {
        $buffer->clear();
        $buffer->status('401 Unauthorized');
        $buffer->setHeader('WWW-Authenticate', 'Basic realm="UMI.CMS"');
        $buffer->push('HTTP Authenticate failed');
        $buffer->end();
    }
    
    if(!permissionsCollection::getInstance()->isSv()) {
        echo 'Не хватает прав доступа' . PHP_EOL;
        exit;
    }
}