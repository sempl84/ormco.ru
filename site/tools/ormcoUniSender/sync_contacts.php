<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new OrmcoUniSendersSyncContacts();
$bRefresh = false;
$message = '';
if (isset($_REQUEST['init'])) {
    try {
        if(!class_exists('ormcoUniSender')) {
            throw new publicException('Не найден модуль ormcoUniSender');
        }
        
        $registry = regedit::getInstance();
        
        $emailListId = $registry->getVal(ormcoUniSender::registry_param_contacts_list_id);
        if(!$emailListId) {
            throw new publicException('Не выбран список контактов для импорта');
        }
        
        $testEmail = trim($registry->getVal(ormcoUniSender::registry_param_test_email));
        if($testEmail) {
            $testEmail = explode(',', $testEmail);
        }
        
        $class->init($emailListId, $testEmail);
        $bRefresh = true;
    
        $message .= implode('<br />', $class->getLog());
    } catch (Exception $e) {
        $message = 'Ошибка: ' . $e->getMessage();
    }
} elseif($class->isActive()) {
    $class->process();
    
    if(!$class->isDone() && !$class->hasError()) {
        $bRefresh = true;
    }
    
    $message .= implode('<br />', $class->getLog());
} else {
    $bRefresh = false;
}
?>
<html>
<head>
    <title>Синхронизация контактов с UniSender</title>
    <?php if ($bRefresh): ?>
        <script type="text/javascript">setTimeout(function () {
                window.location.href = '<?php echo str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__)?>?t=<?php echo time();?>'
            }, 1000);</script>
    <?php endif; ?>
</head>
<body>
<?php if($message):?>
    <?php echo $message; ?>
<?php else:?>
    <a href="?init=true">Запустить синхронизацию</a>
<?php endif;?>
</body>
</html>