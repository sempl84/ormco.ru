<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

use UmiCms\Service;

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$bRefresh = false;
$message = '';
if (isset($_REQUEST['normalize'])) {
    $session = Service::Session();
    
    $offset = intval($session->get('normalize_phones_offset'));
    $limit = 1000;
    
    try {
        $sel = new selector('objects');
        $sel->types('object-type')->name(SiteUsersUserModel::module, SiteUsersUserModel::method);
        $sel->order('id')->asc();
        $sel->limit($offset, $limit);
        
        $result = $sel->result();
        if($result) {
            foreach($sel as $user) {
                if(!$user instanceof umiObject) {
                    continue;
                }
        
                $user->setValue(SiteUsersUserModel::field_system_normalized_phone, SiteUsersUserModel::normalizePhone($user->getValue(SiteUsersUserModel::field_phone)));
                $user->commit();
        
                $message .= 'Обновлен телефон для пользователя ' . $user->getId() . '<br />';
            }
            
            $bRefresh = true;
            
            $session->set('normalize_phones_offset', $offset + $limit);
        } else {
            $message = 'Нормализация завершена';
            $session->del('normalize_phones_offset');
        }
    } catch (Exception $e) {
        $message = 'Ошибка: ' . $e->getMessage();
        $bRefresh = false;
    }
}
?>
<html>
<head>
    <title>Нормализация телефонов</title>
    <?php if ($bRefresh): ?>
        <script type="text/javascript">setTimeout(function () {
                window.location.href = '<?php echo str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__)?>?normalize=true&t=<?php echo time();?>'
            }, 1000);</script>
    <?php endif; ?>
</head>
<body>
<?php if($message):?>
    <?php echo $message; ?>
<?php else:?>
    <a href="?normalize=true">Запустить нормализацию</a>
<?php endif;?>
</body>
</html>