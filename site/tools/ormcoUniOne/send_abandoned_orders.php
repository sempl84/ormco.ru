<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new OrmcoUniOneSendAbandonedOrdersMessage();
$bRefresh = false;
$message = '';
if (isset($_REQUEST['init'])) {
    $class->init();
    $bRefresh = true;
    
    $message .= implode('<br />', $class->getLog());
} elseif($class->isActive()) {
    $class->process();
    
    if(!$class->isDone() && !$class->hasError()) {
        $bRefresh = true;
    }
    
    $message .= implode('<br />', $class->getLog());
} else {
    $bRefresh = false;
}
?>
<html>
<head>
    <title>Обработка для отправки писем по брошенным корзинам</title>
    <?php if ($bRefresh): ?>
        <script type="text/javascript">setTimeout(function () {
                window.location.href = '<?php echo str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__)?>?t=<?php echo time();?>'
            }, 1000);</script>
    <?php endif; ?>
</head>
<body>
<?php if($message):?>
    <?php echo $message; ?>
<?php else:?>
    <a href="?init=true">Запустить обработку</a>
<?php endif;?>
</body>
</html>