<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

define('SKIP_CHECK_AUTH', true);

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new OrmcoUniOneSendAbandonedOrdersMessage();
$message = '';
if (isset($_REQUEST['init'])) {
    $class->init();
    $class->process();
    
    $message .= implode('<br />', $class->getLog());
} elseif($class->isActive()) {
    $class->process();
    
    $message .= implode('<br />', $class->getLog());
}
?>
<html>
<head>
    <title>Обработка для отправки писем по брошенным корзинам</title>
</head>
<body>
<?php if($message):?>
    <?php echo $message; ?>
<?php endif;?>
</body>
</html>