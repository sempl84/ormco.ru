<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

class SiteMaterialsImportPhotosTool
{
    public function execute($parentId, $dir)
    {
        if (!is_dir($dir)) {
            throw new publicException('Не найдена директория ' . $dir);
        }
        
        $arImages = array();
        
        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
            
            $realPath = $file->getRealPath();
            if (!umiImageFile::getIsImage($realPath)) {
                continue;
            }
            
            $arImages[] = str_replace(CURRENT_WORKING_DIR, '.', $realPath);
        }
        
        if (!count($arImages)) {
            throw new publicException('В директории не найдены изображения');
        }
        
        $arImages = array_unique($arImages);
        
        $hierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName('materials', 'item_element');
        if(!$hierarchyType instanceof umiHierarchyType) {
            throw new publicException('Не найден иерархический тип данных');
        }
        
        $hierarchyTypeId = $hierarchyType->getId();
        
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->id($hierarchyTypeId);
        $sel->where('hierarchy')->page($parentId)->level(1);
        $sel->where('is_active')->equals(array(0, 1));
        $sel->where('photo')->equals($arImages);
        
        $arCurrentImages = array();
        foreach ($sel as $element) {
            if(!$element instanceof umiHierarchyElement) {
                continue;
            }
            
            $photo = $element->getValue('photo');
            if(!$photo instanceof umiImageFile || $photo->getIsBroken()) {
                continue;
            }
            
            $arCurrentImages[] = $photo->getFilePath(true);
        }
        
        $arCreateImages = array_diff($arImages, $arCurrentImages);
        
        $umiHierarchy = umiHierarchy::getInstance();
        $permissionsCollection = permissionsCollection::getInstance();
        
        foreach($arCreateImages as $imagePath) {
            $name = pathinfo($imagePath, PATHINFO_FILENAME);
            $pageId = $umiHierarchy->addElement($parentId, $hierarchyTypeId, $name, '');
            if(!$pageId) {
                throw new publicException('Ошибка при создании страницы ' . $name);
            }
            
            $permissionsCollection->setDefaultPermissions($pageId);
            $page = $umiHierarchy->getElement($pageId);
            if(!$page instanceof umiHierarchyElement) {
                throw new publicException('Ошибка при создании страницы ' . $name);
            }
            
            $page->setIsActive();
            $page->setValue('h1', $page->getName());
            $page->setValue('photo', new umiImageFile($imagePath));
            $page->commit();
        }
    }
}

$tool = new SiteMaterialsImportPhotosTool();
$tool->execute(13045, CURRENT_WORKING_DIR . '/files/mm/photos');

echo 'Готово';
exit;