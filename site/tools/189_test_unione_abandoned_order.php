<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(__DIR__) . '/check_permissions.php';

$ormcoUniOne = cmsController::getInstance()->getModule('ormcoUniOne');
if(!$ormcoUniOne instanceof ormcoUniOne) {
    throw new publicException('Не найден модуль ormcoUniOne');
}
/* @var $ormcoUniOne ormcoUniOne|OrmcoUniOneMacrosAbandonedOrders */

$email = getRequest('email');

$message = '';

if(getRequest('send')) {
    $orderId = getRequest('order_id');
    if(!$orderId) {
        throw new publicException('Не передан id заказа');
    }
    
    cmsController::getInstance()->getModule('emarket');
    
    $order = order::get($orderId);
    if(!$order instanceof order) {
        throw new publicException('Не найден заказ ' . $orderId);
    }
    
    try {
        $result = $ormcoUniOne->_sendAbandonedOrdersEmail(array($order->getObject()), getRequest('template_id'), array(), $email, getRequest('new_server') ? 'go2.unisender.ru' : null);
        if($result) {
            $message = 'Сообщение успешно отправлено';
        } else {
            $message = 'Произошла ошибка';
        }
    } catch (Exception $e) {
        $message = $e->getMessage();
    }
} else {
    $orderId = '3607358';
}

if(!$email) {
    $email = 'test@ormcomail.ru';
}
?>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Тестирование отправки писем о брошенных корзинах в UniOne</title>
    </head>
    <body>
        <?php if($message):?>
            <div style="padding-bottom: 20px;"><?php echo $message;?></div>
        <?php endif;?>
        <form method="POST" action="<?php echo str_replace(CURRENT_WORKING_DIR, '', __FILE__);?>">
            <div>
                Id корзины: <input type="text" name="order_id" value="<?php echo htmlspecialchars($orderId);?>" />
            </div>
            <div>
                E-mail: <input type="text" name="email" value="<?php echo $email;?>" />
            </div>
            <div>
                Шаблон:
                <select name="template_id">
                    <option value="d549e812-251e-11ec-89c7-42e5cc3806e9">Брошенная корзина - тестовый</option>
                    <option value="3b4c3d32-c1ff-11eb-a2d2-36cb0fb04eb8">Брошенная корзина</option>
                </select>
            </div>
            <div>
                Новый сервер:
                <input type="checkbox" name="new_server" value="1" />
            </div>
            <div>
                <button type="submit" name="send" value="1">Отправить</button>
            </div>
        </form>
    </body>
</html>
