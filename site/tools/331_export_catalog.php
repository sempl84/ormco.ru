<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(__DIR__) . '/check_permissions.php';

$sel = new selector('pages');
$sel->types('hierarchy-type')->name(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
$sel->where('is_active')->equals(1);
$sel->group('obj_id');

$umiLinksHelper = umiLinksHelper::getInstance();

$items = array();
foreach($sel as $element) {
    if(!$element instanceof umiHierarchyElement) {
        continue;
    }
    
    $item = array();
    $article = trim($element->getValue(SiteCatalogObjectModel::field_article));
    if(!$article) {
        continue;
    }
    
    $article = str_replace('"', "'", $article);
    $item['id'] = $article;
    
    $title = trim($element->getValue('h1_alternative'));
    if(!$title) {
        $title = trim($element->getName());
    }
    
    $title = str_replace('"', "'", $title);
    $item['title'] = $title;
    
    $content = def_module::parseTPLMacroses($element->getValue('description'), $element->getId());
    $content = strip_tags($content);
    $content = str_replace('"', "'", $content);
    $content = str_replace(array("\t", PHP_EOL), '', $content);
    $item['content'] = trim($content);
    
    $availability = (intval($element->getValue('common_quantity')) > 0) ? 'in stock' : 'out of stock';
    $item['availability'] = $availability;
    
    $item['condition'] = 'new';
    $item['price'] = $element->getValue('price');
    $item['link'] = 'https://ormco.ru' . $umiLinksHelper->getLinkByParts($element);
    $item['image_link'] = 'https://ormco.ru/images/ormco-logo-blue-square.jpg';
    $item['brand'] = 'Ormco';
    
    $items[] = $item;
}

$file = CURRENT_WORKING_DIR . '/sys-temp/export_catalog_' . time(). '.csv';

$handler = fopen($file, 'w+');

$limiter = '"';
$delimiter = "\t";
$delimitingString = $limiter . $delimiter . $limiter;

fputs($handler, $limiter . implode($delimitingString, array_keys($items[0])) . $limiter . PHP_EOL);

foreach($items as $item) {
    $string = $limiter . implode($delimitingString, $item) . $limiter . PHP_EOL;
    
    if ($string = mb_convert_encoding($string, 'windows-1251', 'UTF-8')) {
        fputs($handler, $string);
    }
}

fclose($handler);

$oFile = new umiFile($file);
$oFile->download(true);