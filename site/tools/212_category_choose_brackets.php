<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

define('SKIP_CHECK_AUTH', true);

require_once dirname(__DIR__) . '/check_permissions.php';

$sel = new selector('pages');
$sel->types('hierarchy-type')->name('catalog', 'category');
$sel->where('is_active')->equals(1);
$sel->where('choose_brackets')->equals(1);

$umiLinksHelper = umiLinksHelper::getInstance();

foreach($sel as $element) {
    if(!$element instanceof umiHierarchyElement) {
        continue;
    }
    
    echo '<a href="' . $umiLinksHelper->getLinkByParts($element) . '" target="_blank">' . $element->getName() . '</a><br />';
}