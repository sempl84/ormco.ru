<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

require_once dirname(__DIR__) . '/check_permissions.php';

error_reporting(E_ALL);
ini_set('display_errors', true);

$ormcoUniOne = cmsController::getInstance()->getModule('ormcoUniOne');
if(!$ormcoUniOne instanceof ormcoUniOne) {
    throw new publicException('Не найден модуль ormcoUniOne');
}
/* @var $ormcoUniOne ormcoUniOne */

$message = '';

if(getRequest('send')) {
    $substitutions = array();
    
    $rawSubstitutions = getRequest('substitutions');
    if($rawSubstitutions) {
        foreach(explode(PHP_EOL, $rawSubstitutions) as $row) {
            $arRow = explode('|', trim($row));
            if(count($arRow) != 2) {
                continue;
            }
            
            $substitutions[$arRow[0]] = $arRow[1];
        }
    }
    
    $api = $ormcoUniOne->getApi();
    
    $request = new UnioneApiEmailSendRequest();
    $request->setTemplateId(getRequest('template_id'));
    $request->addRecipient(getRequest('email'), $substitutions);
    
    try {
        $api->emailSend($request);
        $message = 'Сообщение успешно отправлено';
    } catch (Exception $e) {
        $message = $e->getMessage();
    }
}
?>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Тестирование отправки писем в UniOne</title>
</head>
<body>
<?php if($message):?>
    <div style="padding-bottom: 20px;"><?php echo $message;?></div>
<?php endif;?>
<form method="POST" action="<?php echo str_replace(CURRENT_WORKING_DIR, '', __FILE__);?>">
    <div>
        E-mail: <input type="text" name="email" value="test@ormcomail.ru" />
    </div>
    <div>
        Шаблон:
        <select name="template_id">
            <option value="3b4c3d32-c1ff-11eb-a2d2-36cb0fb04eb8">Брошенная корзина - визуальный редактор + html</option>
            <option value="e6b6d00e-bfb3-11eb-ab43-961e0c6ced58">Брошенная корзина - без визуального редактора</option>
            <option value="5b348f5a-bc87-11eb-9937-36e2abf3275a">Брошенная корзина</option>
        </select>
    </div>
    <div>
        Данные для подстановки:<br />
        <textarea name="substitutions" rows="10" cols="50" placeholder="Ключ 1|Значение 1"></textarea>
    </div>
    <div>
        <button type="submit" name="send" value="1">Отправить</button>
    </div>
</form>
</body>
</html>