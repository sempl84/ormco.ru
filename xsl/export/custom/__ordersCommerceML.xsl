<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	extension-element-prefixes="php"
	exclude-result-prefixes="xsl php udt">

	<xsl:template match="object" mode="order">
		<xsl:param name="order_num" select="properties/group/property[@name='number']/value" />
		<xsl:param name="status_change_date" select="string(properties/group/property[@name='status_change_date']/value/@unix-timestamp)" />
		<xsl:param name="order_date" select="string(properties/group/property[@name='order_date']/value/@unix-timestamp)" />
		<xsl:param name="total_price" select="properties/group/property[@name='total_price']/value" />
		<xsl:param name="customer_id" select="properties/group/property[@name='customer_id']/value/item/@id" />
		<xsl:param name="customer_comments" select="properties/group/property[@name='comments']/value" />
	
		<xsl:param name="order_status_id" select="number(properties/group/property[@name='status_id']/value/item/@id)" />
		<xsl:param name="order_status" select="document(concat('uobject://', $order_status_id))/udata/object" />
		<xsl:param name="order_status_codename" select="string($order_status//property[@name='codename']/value)" />
	
		<xsl:param name="payment_date" select="string(properties/group/property[@name='payment_date']/value/@unix-timestamp)" />
		<xsl:param name="payment_document_num" select="string(properties/group/property[@name='payment_document_num']/value)" />
		<xsl:param name="payment_type" select="string(properties/group/property[@name='payment_id']/value)" />
		<xsl:param name="payment_status_id" select="number(properties/group/property[@name='payment_status_id']/value/item/@id)" />
		<xsl:param name="payment_status_codename" select="string(document(concat('uobject://', $payment_status_id))//property[@name='codename']/value)" />
		<xsl:param name="delivery_allow_date" select="string(properties/group/property[@name='delivery_allow_date']/value/@unix-timestamp)" />
		
		<xsl:param name="delivery_price" select="number(properties/group/property[@name='delivery_price']/value)" />
		<xsl:param name="total_amount" select="count(properties/group/property[@name='order_items']/value/item)" />
		<xsl:param name="delivery_price_per_item_mod" select="number($delivery_price mod $total_amount)" />
		<xsl:param name="delivery_price_without_mod" select="number($delivery_price - $delivery_price_per_item_mod)" />
		<xsl:param name="delivery_price_per_item" select="number($delivery_price_without_mod div  $total_amount))" />
		
		<xsl:param name="full_object" select="document(concat('uobject://', @id))/udata" />
		<xsl:param name="first_item_page" select="document(concat('uobject://', .//properties/group/property[@name='order_items']/value/item[1]/@id))//property[@name='item_link']/value/page/@id" />
		
		<xsl:param name="first_item_page_1C" select="document(concat('udata://custom/get1cRel/', $first_item_page))/udata" />
		<xsl:param name="payment_type_name" select="string($full_object//property[@name='payment_id']/value/item/@name)" />
		
		<!-- <xsl:if test="($first_item_page_1C and not($first_item_page_1C = 0)) or .//properties/group/property[@name='order_items']/value/item[1]/text() = 'Лечение патологии височно-нижнечелюстного сустава, Элизабет Менцель, 26-29.01.17, СПб'">
		 -->	
			<Документ rr="1">
				<Ид><xsl:value-of select="@id" /></Ид>
				<Номер><xsl:value-of select="document(concat('udata://custom/buhgalt_order_number/',@id))/udata" /><!-- СТ<xsl:value-of select="@id" /> --></Номер>
				<Дата>
					<xsl:if test="string-length($order_date)">
						<xsl:value-of select="php:function('date', 'Y-m-d', $order_date)" />
					</xsl:if>
				</Дата>
				<ХозОперация>Заказ товара</ХозОперация>
				<Роль>Продавец</Роль>
				<Валюта>руб</Валюта>
				<Курс>1</Курс>
				<Сумма><xsl:value-of select="$total_price + $delivery_price" /></Сумма>
				<СуммаБезДоставки><xsl:value-of select="$total_price" /></СуммаБезДоставки>
				<СуммаБезДоставки2><xsl:value-of select="$total_price" /></СуммаБезДоставки2>
				<СтоимостьДоставки><xsl:value-of select="$delivery_price" /></СтоимостьДоставки>
				<xsl:if test="string-length($order_date)">
					<Время><xsl:value-of select="php:function('date', 'H:i:s', $order_date)" /></Время>
				</xsl:if>
				<Комментарий>Заказ №<xsl:value-of select="$order_num" /></Комментарий>
				<xsl:if test="$customer_id">
					<Контрагенты>
						<xsl:apply-templates select="document(concat('uobject://', $customer_id))/udata/object" mode="customer" >
							<xsl:with-param name="full_object" select="$full_object" />
						</xsl:apply-templates>
					</Контрагенты>
				</xsl:if>
				
				<Товары pp="1">
					<xsl:apply-templates select="properties/group/property[@name='order_items']/value/item" mode="order-item" >
						<xsl:with-param name="one_click_purchase" select="1" />
						<xsl:with-param name="delivery_price_per_item" select="$delivery_price_per_item" />
						<xsl:with-param name="delivery_price_per_item_mod" select="$delivery_price_per_item_mod" />
					</xsl:apply-templates>
				</Товары>
				<СеминарИд1С>
					<xsl:value-of select="$first_item_page_1C" />
					<xsl:if test="not($first_item_page_1C) and .//properties/group/property[@name='order_items']/value/item[1]/text() = 'Лечение патологии височно-нижнечелюстного сустава, Элизабет Менцель, 26-29.01.17, СПб'">
						<xsl:text>8e0bddd1-9f7c-11e6-9467-005056b0350d</xsl:text>
					</xsl:if>
				</СеминарИд1С>
				<!-- <xsl:if test="$full_object//group[@name='events_params']/property">
					<СеминарИд><xsl:value-of select="$first_item_page" /></СеминарИд>
				</xsl:if>
				<xsl:if test="$first_item_page_1C">
					<СеминарИд1С><xsl:value-of select="$first_item_page_1C" /></СеминарИд1С>
				</xsl:if> -->
				
				<xsl:apply-templates select="$full_object//group[@name='events_params']/property" mode="order-item-event"/>
				<xsl:apply-templates select="$full_object//group[@name='form_params']/property" mode="order-item-event"/>
				<xsl:apply-templates select="$full_object//group[@name='events_discount']/property" mode="order-item-event"/>
				
				<ЗначенияРеквизитов>
					<xsl:if test="string-length($payment_date)">
						<ЗначениеРеквизита>
							<Наименование>Дата оплаты</Наименование>
							<Значение><xsl:value-of select="php:function('date', 'Y-m-d', $payment_date)" /></Значение>
						</ЗначениеРеквизита>
					</xsl:if>
					
					<!-- ФормаОплаты -->
					<xsl:if test="string-length($payment_type_name)">
						<ЗначениеРеквизита>
							<Наименование>ФормаОплаты</Наименование>
							<Значение><xsl:value-of select="$payment_type_name" /></Значение>
						</ЗначениеРеквизита>
					</xsl:if>
		
					<xsl:if test="string-length($payment_document_num)">
						<ЗначениеРеквизита>
							<Наименование>Номер платежного документа</Наименование>
							<Значение><xsl:value-of select="$payment_document_num" /></Значение>
						</ЗначениеРеквизита>
					</xsl:if>
		
					<xsl:if test="string-length($payment_type)">
						<ЗначениеРеквизита>
							<Наименование>Метод оплаты</Наименование>
							<Значение><xsl:value-of select="$payment_type" /></Значение>
						</ЗначениеРеквизита>
					</xsl:if>
		
					<xsl:if test="string-length($delivery_allow_date)">
						<ЗначениеРеквизита>
							<Наименование>Дата разрешения доставки</Наименование>
							<Значение><xsl:value-of select="php:function('date', 'Y-m-d', $delivery_allow_date)" /></Значение>
						</ЗначениеРеквизита>
						<ЗначениеРеквизита>
							<Наименование>Доставка разрешена</Наименование>
							<Значение>true</Значение>
						</ЗначениеРеквизита>
					</xsl:if>
		
					<ЗначениеРеквизита>
						<Наименование>Заказ оплачен</Наименование>
						<Значение>
							<xsl:choose>
								<xsl:when test="$payment_status_codename = 'accepted'">true</xsl:when>
								<xsl:otherwise>false</xsl:otherwise>
							</xsl:choose>
						</Значение>
					</ЗначениеРеквизита>
		
					<ЗначениеРеквизита>
						<Наименование>Отменен</Наименование>
						<Значение>
							<xsl:choose>
								<xsl:when test="$order_status_codename = 'canceled'">true</xsl:when>
								<xsl:otherwise>false</xsl:otherwise>
							</xsl:choose>
						</Значение>
					</ЗначениеРеквизита>
		
					<ЗначениеРеквизита>
						<Наименование>Финальный статус</Наименование>
						<Значение>
							<xsl:choose>
								<xsl:when test="$order_status_codename = 'ready'">true</xsl:when>
								<xsl:otherwise>false</xsl:otherwise>
							</xsl:choose>
						</Значение>
					</ЗначениеРеквизита>
		
					<ЗначениеРеквизита>
						<Наименование>Статус заказа</Наименование>
						<!-- <ЗначениеСтатусаЗаказа><xsl:value-of select="$order_status/@name" /></ЗначениеСтатусаЗаказа> -->
						<Значение><xsl:value-of select="$order_status/@name" /></Значение>
					</ЗначениеРеквизита>
		
					<xsl:if test="string-length($status_change_date)">
						<ЗначениеРеквизита>
							<Наименование>Дата изменения статуса</Наименование>
							<Значение><xsl:value-of select="php:function('date', 'Y-m-d H:i', $status_change_date)" /></Значение>
						</ЗначениеРеквизита>
					</xsl:if>
				</ЗначенияРеквизитов>
			</Документ>
		<!-- </xsl:if> -->
	</xsl:template>
	
	<xsl:template match="object" mode="customer">
		<xsl:param name="full_object" />
		
		<xsl:variable name="full_name">
			<xsl:choose>
				<xsl:when test=".//property[@name='nazvanie_organizacii']/value">
					<xsl:value-of select=".//property[@name='nazvanie_organizacii']/value" />
				</xsl:when>
				<xsl:when test="$full_object//property[@name='order_lname']/value or $full_object//property[@name='order_fname']/value or $full_object//property[@name='order_father_name']/value">
					<xsl:value-of select="$full_object//property[@name='order_lname']/value" />&#160;<xsl:value-of select="$full_object//property[@name='order_fname']/value" />&#160;<xsl:value-of select="$full_object//property[@name='order_father_name']/value" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select=".//property[@name='lname']/value"/>&#160;<xsl:value-of select=".//property[@name='fname']/value"/>&#160;<xsl:value-of select=".//property[@name='father_name']/value"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<Контрагент>
			<Ид><xsl:value-of select="@id" /></Ид>
			<xsl:if test="$full_name"><Наименование><xsl:value-of select="$full_name"/></Наименование></xsl:if>
			<xsl:if test="$full_name"><НаименованиеПолное><xsl:value-of select="$full_name"/></НаименованиеПолное></xsl:if>
			<Роль>Покупатель</Роль>
			<xsl:if test=".//property[@name='e-mail']/value"><Email><xsl:value-of select=".//property[@name='e-mail']/value"/></Email></xsl:if>
			<xsl:if test=".//property[@name='email']/value"><Email><xsl:value-of select=".//property[@name='email']/value"/></Email></xsl:if>
			<xsl:choose>
				<!-- TODO take ur data from $full_object params -->
				<xsl:when test=".//property[@name='inn']/value">
					<ЮрФизЛицо>Юр. лицо</ЮрФизЛицо>
					<xsl:if test=".//property[@name='inn']/value"><ИНН><xsl:value-of select="//property[@name='inn']/value"/></ИНН></xsl:if>
					<xsl:if test=".//property[@name='kpp']/value"><КПП><xsl:value-of select=".//property[@name='kpp']/value"/></КПП></xsl:if>
					<xsl:if test=".//property[@name='raschetnyj_cchet']/value"><РасчетныйСчет><xsl:value-of select=".//property[@name='raschetnyj_cchet']/value"/></РасчетныйСчет></xsl:if>
					<xsl:if test=".//property[@name='bik']/value"><БИК><xsl:value-of select=".//property[@name='bik']/value"/></БИК></xsl:if>
					<xsl:if test=".//property[@name='korrschet']/value"><КоррСчет><xsl:value-of select=".//property[@name='korrschet']/value"/></КоррСчет></xsl:if>
				
					<Orthodontia>
						<КонтактноеЛицоUID><xsl:value-of select="@id" /></КонтактноеЛицоUID>
						<КонтрагентUID><xsl:value-of select=".//property[@name='inn']/value" /></КонтрагентUID>
					</Orthodontia>
				</xsl:when>
				<xsl:otherwise>					
					<ЮрФизЛицо>Физ. лицо</ЮрФизЛицо>
					<Orthodontia>
						<КонтактноеЛицоUID><xsl:value-of select="@id" /></КонтактноеЛицоUID>
						<КонтрагентUID><xsl:value-of select="@id" /></КонтрагентUID>
					</Orthodontia>
				</xsl:otherwise>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="@type-guid='emarket-customer'">
					<customer_type>guest</customer_type>
				</xsl:when>
				<xsl:when test="@type-guid='users-user'">
					<customer_type>user</customer_type>
					<СтатусПользователя><xsl:value-of select=".//property[@name='prof_status']/value/item/@name"/></СтатусПользователя>
				</xsl:when>
			</xsl:choose>
			
			<xsl:if test=".//property[@name='yuridicheskij_adres']/value"><ЮрАдрес><xsl:value-of select=".//property[@name='yuridicheskij_adres']/value"/></ЮрАдрес></xsl:if>
			<xsl:if test=".//property[@name='fakticheskij_adres']/value"><ФактАдрес><xsl:value-of select=".//property[@name='fakticheskij_adres']/value"/></ФактАдрес></xsl:if>
			<xsl:if test=".//property[@name='telefon']/value"><Телефон><xsl:value-of select=".//property[@name='telefon']/value"/></Телефон></xsl:if>
			<xsl:if test=".//property[@name='phone']/value"><Телефон><xsl:value-of select=".//property[@name='phone']/value"/></Телефон></xsl:if>
			<xsl:if test=".//property[@name='fio_rukovoditelya']/value"><ФИОруководителя><xsl:value-of select=".//property[@name='fio_rukovoditelya']/value"/></ФИОруководителя></xsl:if>
			<xsl:if test=".//property[@name='dolzhnost']/value"><ДолжностьРуководителя><xsl:value-of select=".//property[@name='dolzhnost']/value"/></ДолжностьРуководителя></xsl:if>
			<xsl:if test=".//property[@name='fio_kntaktnogo_lica']/value"><ФИОКЛ><xsl:value-of select=".//property[@name='fio_kntaktnogo_lica']/value"/></ФИОКЛ></xsl:if>
			
			<ФамилияКЛ>
				<xsl:choose>
					<xsl:when test="$full_object//property[@name='order_lname']/value">
						<xsl:value-of select="$full_object//property[@name='order_lname']/value"/>
					</xsl:when>
					<xsl:when test=".//property[@name='lname']/value">
						<xsl:value-of select=".//property[@name='lname']/value"/>
					</xsl:when>
					<xsl:otherwise>-</xsl:otherwise>
				</xsl:choose>
			</ФамилияКЛ>
			<ИмяКЛ>
				<xsl:choose>
					<xsl:when test="$full_object//property[@name='order_fname']/value">
						<xsl:value-of select="$full_object//property[@name='order_fname']/value"/>
					</xsl:when>
					<xsl:when test=".//property[@name='fname']/value">
						<xsl:value-of select=".//property[@name='fname']/value"/>
					</xsl:when>
					<xsl:otherwise>-</xsl:otherwise>
				</xsl:choose>
			</ИмяКЛ>
			<ОтчествоКЛ>
				<xsl:choose>
					<xsl:when test="$full_object//property[@name='order_father_name']/value">
						<xsl:value-of select="$full_object//property[@name='order_father_name']/value"/>
					</xsl:when>
					<xsl:when test=".//property[@name='father_name']/value">
						<xsl:value-of select=".//property[@name='father_name']/value"/>
					</xsl:when>
					<xsl:otherwise>-</xsl:otherwise>
				</xsl:choose>
			</ОтчествоКЛ>
			<ВнеСанкционногоСписка>1</ВнеСанкционногоСписка>
			<Регион>
				<xsl:choose>
					<xsl:when test="$full_object//property[@name='order_city']/value">
						<xsl:value-of select="$full_object//property[@name='order_city']/value"/>
					</xsl:when>
					<xsl:when test=".//property[@name='gorod_from']/value">
						<xsl:value-of select=".//property[@name='gorod_from']/value"/>
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
				</xsl:choose>
			</Регион>
			<xsl:if test="$full_object//property[@name='order_region']/value/item">
				<РегионUID>
					<UID><xsl:value-of select="$full_object//property[@name='order_region']/value/item/@id"/></UID>
					<Название><xsl:value-of select="$full_object//property[@name='order_region']/value/item/@name"/></Название>
				</РегионUID>
			</xsl:if>
					
			<xsl:choose>
				<!-- инфа из заказа -->
				<xsl:when test="$full_object//property[@name='order_phone']/value and not($full_object//property[@name='order_phone']/value = '')">
					<ТелефонКЛ><xsl:value-of select="$full_object//property[@name='order_phone']/value"/></ТелефонКЛ>
				</xsl:when>
				<!-- инфа из спец поля для телефона контактного лица -->
				<xsl:when test=".//property[@name='tel_kontaktnogo_lica']/value and not(.//property[@name='tel_kontaktnogo_lica']/value = '')">
					<ТелефонКЛ><xsl:value-of select=".//property[@name='tel_kontaktnogo_lica']/value"/></ТелефонКЛ>
				</xsl:when>
				<!-- инфа из юридической информации из рег. пользователя -->
				<xsl:when test=".//property[@name='telefon']/value and not(.//property[@name='telefon']/value = '')">
					<ТелефонКЛ><xsl:value-of select=".//property[@name='telefon']/value"/></ТелефонКЛ>
				</xsl:when>
				<!-- инфа из общей информации из рег. пользователя -->
				<xsl:when test=".//property[@name='mobile_phone']/value and not(.//property[@name='mobile_phone']/value = '')">
					<ТелефонКЛ><xsl:value-of select=".//property[@name='mobile_phone']/value"/></ТелефонКЛ>
				</xsl:when>
				<!-- инфа из общей информации из незарег. пользователя -->
				<xsl:when test=".//property[@name='phone']/value and not(.//property[@name='phone']/value = '')">
					<ТелефонКЛ><xsl:value-of select=".//property[@name='phone']/value"/></ТелефонКЛ>
				</xsl:when>
			</xsl:choose>
			<xsl:if test=".//property[@name='mejl_kontaktnogo_lica']/value"><ЕмайлКЛ><xsl:value-of select=".//property[@name='mejl_kontaktnogo_lica']/value"/></ЕмайлКЛ></xsl:if>
		</Контрагент>
	</xsl:template>
	
	<xsl:template match="item" mode="order-item">
		<xsl:param name="one_click_purchase" />
		<xsl:param name="delivery_price_per_item" />
		<xsl:param name="delivery_price_per_item_mod" />
		<xsl:param name="is_last">
			<xsl:choose>
				<xsl:when test="position() = last()">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		
		<xsl:apply-templates select="document(concat('uobject://', @id))/udata/object" mode="order-item" >
			<xsl:with-param name="one_click_purchase" select="$one_click_purchase" />
			<xsl:with-param name="delivery_price_per_item" select="$delivery_price_per_item" />
			<xsl:with-param name="delivery_price_per_item_mod" select="$delivery_price_per_item_mod" />	
			<xsl:with-param name="is_last" select="$is_last" />	
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="object" mode="order-item">
		<xsl:param name="one_click_purchase" />
		<xsl:param name="delivery_price_per_item" />
		<xsl:param name="delivery_price_per_item_mod" />
		<xsl:param name="is_last" />
		
		<xsl:param name="good-id" select="properties/group/property[@name='item_link']/value/page/@id" />
		<xsl:param name="good" select="document(concat('upage://', $good-id))/udata/page" />
		<xsl:param name="item_price" select="properties/group/property[@name='item_price']/value" />
		<xsl:param name="item_priceWithDelivery">
			<xsl:choose>
				<xsl:when test="$is_last=1">
					<xsl:value-of select="$item_price + $delivery_price_per_item + $delivery_price_per_item_mod" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$item_price + $delivery_price_per_item" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:param name="item_amount" select="properties/group/property[@name='item_amount']/value" />
		<xsl:param name="item_total_price" select="properties/group/property[@name='item_total_price']/value" />
		<xsl:param name="item_total_priceWithDelivery">
			<xsl:choose>
				<xsl:when test="$is_last=1">
					<xsl:value-of select="$item_total_price + (($item_amount-1) * $delivery_price_per_item) + $delivery_price_per_item_mod" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$item_total_price + ($item_amount * $delivery_price_per_item)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		
		<xsl:variable name="one_click_purchase_flag">Услуга</xsl:variable>
		
		<Товар tt="1">
			<xsl:choose>
				<xsl:when test="not($good)">
					<Ид><xsl:value-of select="@id" /></Ид>
				</xsl:when>
				<xsl:when test="$good//property[@name = '1c_product_id']/value">
					<Ид><xsl:value-of select="$good//property[@name = '1c_product_id']/value" /></Ид>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="rel_1c" select="document(concat('udata://custom/get1cRel/',$good-id))/udata" />
					<xsl:choose>
						<xsl:when test="not($rel_1c=0)">
							<Ид><xsl:value-of select="$rel_1c" /></Ид>
						</xsl:when>
						<xsl:otherwise>
							<Ид><xsl:value-of select="$good-id" /></Ид>
						</xsl:otherwise>
					</xsl:choose>
					
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$good//property[@name = '1c_catalog_id']/value">
				<ИдКаталога><xsl:value-of select="$good//property[@name = '1c_catalog_id']/value" /></ИдКаталога>
			</xsl:if>
	
			<Наименование><xsl:value-of select="$good/name | @name" /></Наименование>
			<БазоваяЕдиница Код="796" НаименованиеПолное="Штука" МеждународноеСокращение="PCE">шт</БазоваяЕдиница>
	 
			<ЦенаЗаЕдиницуБезДоставки><xsl:value-of select="$item_price" /></ЦенаЗаЕдиницуБезДоставки>
			<ЦенаЗаЕдиницу><xsl:value-of select="$item_priceWithDelivery" /></ЦенаЗаЕдиницу>
			<СуммаБезДоставки><xsl:value-of select="$item_total_priceWithDelivery" /></СуммаБезДоставки>
			<Сумма><xsl:value-of select="$item_total_price" /></Сумма>
			<Количество><xsl:value-of select="$item_amount" /></Количество>
			<Единица>шт</Единица>
			<Коэффициент>1</Коэффициент>
	
			<ЗначенияРеквизитов>
				<ЗначениеРеквизита>
				<Наименование>ВидНоменклатуры</Наименование>
				<Значение><xsl:value-of select="$one_click_purchase_flag" /></Значение>
				</ЗначениеРеквизита>
				<ЗначениеРеквизита>
				<Наименование>ТипНоменклатуры</Наименование>
				<Значение><xsl:value-of select="$one_click_purchase_flag" /></Значение>
				</ЗначениеРеквизита>
			</ЗначенияРеквизитов>
		</Товар>
	</xsl:template>

	
	

	
	
	
	<xsl:template match="property" mode="order-item-event">
		<xsl:element name="{@name}">
	      	<xsl:value-of select="value"/>
	    </xsl:element>
	</xsl:template>
	<xsl:template match="property[@type='relation']" mode="order-item-event">
		<xsl:element name="{@name}">
	      	<xsl:value-of select="value/item/@name"/>
	    </xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
