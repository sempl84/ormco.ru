<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	extension-element-prefixes="php"
	exclude-result-prefixes="xsl php udt">

	<!-- временно отключаем импорт товаров, для синхронизацию с тестовой 1С базой -->
	<xsl:template match="/">

		<umidump xmlns:xlink="http://www.w3.org/TR/xlink" version="2.0">
			<xsl:apply-templates select="КоммерческаяИнформация/Классификатор" />
			<!-- <xsl:apply-templates select="КоммерческаяИнформация/Каталог" /> -->

			<!-- <xsl:apply-templates select="КоммерческаяИнформация/ПакетПредложений" /> -->

			<xsl:if test="count(КоммерческаяИнформация/Документ)">
				<xsl:apply-templates select="КоммерческаяИнформация" mode="document" />
			</xsl:if>
		</umidump>

	</xsl:template>
	
	
	<xsl:template match="Классификатор">
		<meta>
			<source-name>commerceML2</source-name>
		</meta>

		<types>
			<!-- root type catalog / object -->
			<type id="root-catalog-object-type" title="Объект каталога" parent-id="root-pages-type" locked="locked">
				<base module="catalog" method="object">Объекты каталога</base>
				<fieldgroups>
					<!-- common fields -->
					<group name="common" title="Основные параметры">
						<field name="title" title="Поле TITLE">
							<type name="Строка" data-type="string"/>
						</field>
						<field name="h1" title="Поле H1">
							<type name="Строка" data-type="string"/>
						</field>
					</group>
				</fieldgroups>
			</type>
			<!-- root type catalog / rubric -->
			<type id="root-catalog-category-type" title="Раздел каталога" parent-id="root-pages-type">
				<base module="catalog" method="category">Разделы каталога</base>
				<fieldgroups>
					<!-- common fields -->
					<group name="common" title="Основные параметры" locked="locked">
						<field name="title" title="Поле TITLE">
							<type name="Строка" data-type="string"/>
						</field>
						<field name="h1" title="Поле H1" visible="visible">
							<type name="Строка" data-type="string"/>
						</field>
					</group>
				</fieldgroups>
			</type>

		</types>

	</xsl:template>
	
	<xsl:template match="Товары/Товар">
		<xsl:param name="group_id" select="string(Группы/Ид)" />
		<xsl:param name="name">
			<xsl:choose>
				<xsl:when test="string-length(ПолноеНаименование)">
					<xsl:value-of select="ПолноеНаименование" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="Наименование" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<page id="{Ид}" parentId="{$group_id}" type-id="root-catalog-object-type">
			<xsl:if test="Статус = 'Удален'">
				<xsl:attribute name="is-deleted">1</xsl:attribute>
			</xsl:if>
			<xsl:if test="not(Группы/Ид)">
				<xsl:attribute name="parentId"><xsl:value-of select="$catalog-id" /></xsl:attribute>
				<xsl:attribute name="type-id">root-catalog-object-type</xsl:attribute>
			</xsl:if>

			<default-active><xsl:value-of select="$catalog_item_activity" /></default-active>
			<default-visible><xsl:value-of select="$catalog_item_visible" /></default-visible>

			<basetype module="catalog" method="object">Объекты каталога</basetype>

			<name><xsl:value-of select="$name" /></name>

			<xsl:if test="string-length($catalog_item_template)">
				<default-template><xsl:value-of select="$catalog_item_template" /></default-template>
			</xsl:if>

			<properties>
				<group name="common">
					<title>Основные параметры</title>
					<property name="title" type="string">
						<title>Поле TITLE</title>
						<default-value><xsl:value-of select="$name" /></default-value>
					</property>
					<property name="h1" type="string">
						<title>Поле H1</title>
						<default-value><xsl:value-of select="$name" /></default-value>
					</property>
				</group>

				<group name="product">
					<title>1C: Общие свойства</title>
					<xsl:if test="string-length(Описание)">
						<property name="description" title="Описание" type="wysiwyg" allow-runtime-add="1">
							<type data-type="wysiwyg" />
							<title>Описание</title>
							<value>
								<xsl:choose>
									<xsl:when test="Описание/@ФорматHTML = 'true'">
										<xsl:value-of select="Описание"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="php:function('nl2br', string(Описание))" disable-output-escaping="yes" />
									</xsl:otherwise>
								</xsl:choose>
							</value>
						</property>
					</xsl:if>
					<property name="1c_catalog_id" type="string">
						<title>Идентификатор каталога 1С</title>
						<value><xsl:value-of select="$catalog-id" /></value>
					</property>
					<property name="1c_product_id" type="string">
						<title>Идентификатор в 1С</title>
						<value><xsl:value-of select="Ид" /></value>
					</property>
					<property name="artikul" type="string">
						<title>Артикул</title>
						<value><xsl:value-of select="Артикул" /></value>
					</property>
					<property name="bar_code" type="string">
						<title>Штрих-код</title>
						<value><xsl:value-of select="Штрихкод" /></value>
					</property>
					<property name="weight" type="float">
						<title>Вес</title>
						<value><xsl:value-of select="ЗначенияРеквизитов/ЗначениеРеквизита[Наименование = 'Вес']/Значение"/></value>
					</property>

					<xsl:apply-templates select="Картинка" />

					<xsl:if test="not(ЗначенияСвойств/ЗначенияСвойства/Ид = '8bcc10a2-919d-11e2-e099-0015179e7064')">
						<property name="Торк" title="Торк"  is-public="1" visible="visible" allow-runtime-add="1">
							<type data-type="relation" />
							<title>Торк</title>
							<value>
								<item name="ст" />
							</value>
						</property>
					</xsl:if>
				</group>

				<xsl:apply-templates select="ЗначенияСвойств" />
				<!-- проверка на торк и если его нет ставим ему значение "ст" -->
				
			</properties>
		</page>
	</xsl:template>
	
	
	<xsl:template match="ЗначенияСвойств/ЗначенияСвойства">
		<xsl:param name="property" select="key('property', Ид)" />
		<xsl:param name="value-id" select="string(ИдЗначения)" />
		<xsl:param name="data-type">
			<xsl:choose>
				<xsl:when test="$property/Наименование = 'Брекет система' or $property/Наименование = 'Номер зуба' or $property/Наименование = 'Паз' or $property/Наименование = 'Материал' or $property/Наименование = 'Сечение дуги' or $property/Наименование = 'Форма' or $property/Наименование = 'Челюсть' or $property/Наименование = 'Расстояние между петлями' or $property/Наименование = 'Размер дуги' or $property/Наименование = 'Температура преформации' or $property/Наименование = 'Структура дуги' or $property/Наименование = 'Цвет' or $property/Наименование = 'Группа замков' or $property/Наименование = 'Торк'">spec_relation</xsl:when>
				<xsl:when test="$property/Наименование = 'Наличие петли на дуге' or $property/Наименование = 'Смещение брекета к десне' or $property/Наименование = 'Наличие крючка' or $property/Наименование = 'Смещение брекета к десне' or $property/Наименование = 'Низкое трение' or $property/Наименование = 'Лингвальная дуга'">boolean</xsl:when>
				
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Число'">float</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Булево'">boolean</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Дата'">date</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Справочник'">relation</xsl:when>
				<xsl:otherwise>string</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:if test="$property">
			<property name="{$property/Наименование}" title="{$property/Наименование}"  is-public="1" visible="visible" allow-runtime-add="1">
				<!-- type="{$data-type}" -->
				<type data-type="{$data-type}" >
					<xsl:if test="$data-type = 'spec_relation'"><xsl:attribute name="data-type">relation</xsl:attribute></xsl:if>
				</type>
				<title><xsl:value-of select="$property/Наименование"/></title>

				<value>
					<xsl:choose>
						<!-- преобразуем множественное значение для номера зуба -->
						<xsl:when test="$property/Наименование = 'Номер зуба'">
							<xsl:apply-templates select="document(concat('udata://custom/explodeStr/',php:function('urlencode', string(Значение))))//item" mode="explodeStr"/>
						</xsl:when>
						<!-- преобразуем spec_relation значение , то есть все те что строка но выводим как список -->
						<xsl:when test="$data-type = 'spec_relation'"> 
							<item name="{Значение}" />
						</xsl:when>
						<!-- преобразуем boolen значение -->
						<xsl:when test="$data-type = 'boolean'">
							<xsl:choose>
								<xsl:when test="Значение = 'ложь'">0</xsl:when>
								<xsl:otherwise>1</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$data-type = 'relation'">
							<xsl:apply-templates select="$property/ТипыЗначений/ТипЗначений/ВариантыЗначений/ВариантЗначения[Ид = $value-id]" mode="relation-value" />
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="Значение" /></xsl:otherwise>
					</xsl:choose>
				</value>
			</property>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="item" mode="explodeStr">
		<item name="{text()}" />
	</xsl:template>
	
	<!-- запрет на импорт цен -->
	<!-- <xsl:template match="Цены" /> -->
		<!-- <xsl:param name="default-price" select="Цена[ИдТипаЦены = string($settings//item[@key='exchange.translator.1c_price_type_id'])]" />
		<group name="cenovye_svojstva" title="Ценовые свойства">
			<xsl:choose>
				<xsl:when test="count(Цена) > 1 and $default-price">
					<xsl:apply-templates select="$default-price" />
				</xsl:when>
				<xsl:when test="count(Цена) > 1 and not($default-price)">
					<xsl:apply-templates select="Цена[position() = 1]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="Цена" />
				</xsl:otherwise>
			</xsl:choose>
		</group>
	</xsl:template> -->


</xsl:stylesheet>
