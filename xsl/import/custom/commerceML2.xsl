<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                extension-element-prefixes="php"
                exclude-result-prefixes="xsl php udt">

    <xsl:template match="Товары/Товар">
        <xsl:param name="group_id" select="string(Группы/Ид)" />

        <xsl:param name="name">
            <xsl:choose>
                <xsl:when test="string-length(ПолноеНаименование)">
                    <xsl:value-of select="ПолноеНаименование" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="Наименование" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:param>

        <page id="{Ид}" parentId="{$group_id}" type-id="root-catalog-object-type">
            <xsl:if test="Статус = 'Удален'">
                <xsl:attribute name="is-deleted">1</xsl:attribute>
            </xsl:if>

            <xsl:if test="not(Группы/Ид)">
                <xsl:attribute name="parentId">
                    <xsl:value-of select="$catalog-id" />
                </xsl:attribute>
                <xsl:attribute name="type-id">root-catalog-object-type</xsl:attribute>
            </xsl:if>

            <default-active>
                <xsl:value-of select="$catalog_item_activity" />
            </default-active>

            <default-visible>
                <xsl:value-of select="$catalog_item_visible" />
            </default-visible>

            <basetype module="catalog" method="object">Объекты каталога</basetype>

            <name>
                <xsl:value-of select="$name" />
            </name>

            <xsl:if test="string-length($catalog_item_template)">
                <default-template>
                    <xsl:value-of select="$catalog_item_template" />
                </default-template>
            </xsl:if>

            <properties>
                <group name="common">
                    <title>Основные параметры</title>

                    <property name="title" type="string">
                        <title>i18n::field-title</title>
                        <default-value>
                            <xsl:value-of select="$name" />
                        </default-value>
                    </property>

                    <property name="h1" type="string">
                        <title>i18n::field-h1</title>
                        <default-value>
                            <xsl:value-of select="$name" />
                        </default-value>
                    </property>
                </group>

                <group name="product">
                    <title>1C: Общие свойства</title>

                    <xsl:if test="string-length(Описание)">
                        <property name="description" title="Описание" type="wysiwyg" allow-runtime-add="1">
                            <type data-type="wysiwyg" />
                            <title>Описание</title>
                            <value>
                                <xsl:choose>
                                    <xsl:when test="Описание/@ФорматHTML = 'true'">
                                        <xsl:value-of select="Описание"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="php:function('nl2br', string(Описание))" disable-output-escaping="yes" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </value>
                        </property>
                    </xsl:if>

                    <property name="1c_catalog_id" type="string">
                        <title>Идентификатор каталога 1С</title>
                        <value>
                            <xsl:value-of select="$catalog-id" />
                        </value>
                    </property>

                    <property name="1c_product_id" type="string">
                        <title>Идентификатор в 1С</title>
                        <value>
                            <xsl:value-of select="Ид" />
                        </value>
                    </property>

                    <property name="artikul" type="string">
                        <title>Артикул</title>
                        <value>
                            <xsl:value-of select="Артикул" />
                        </value>
                    </property>

                    <property name="bar_code" type="string">
                        <title>Штрих-код</title>
                        <value>
                            <xsl:value-of select="Штрихкод" />
                        </value>
                    </property>

                    <property name="weight" type="float">
                        <title>Вес</title>
                        <value>
                            <xsl:value-of select="ЗначенияРеквизитов/ЗначениеРеквизита[Наименование = 'Вес']/Значение"/>
                        </value>
                    </property>

                    <!-- Номер зуба -->
                    <xsl:if test="ПараметрыБрекетовЗамковКолец/Параметр/НомерЗуба">
                        <property name="nomer_zuba" type="relation">
                            <title>Номер зуба</title>
                            <value>
                                <xsl:apply-templates select="ПараметрыБрекетовЗамковКолец/Параметр/НомерЗуба" mode="nomer_zuba" />
                            </value>
                        </property>
                    </xsl:if>
                    <!-- торк -->
                    <xsl:if test="ПараметрыБрекетовЗамковКолец/Параметр/Торк">
                        <property name="tork" type="relation">
                            <title>Торк</title>
                            <value>
                                <xsl:choose>
                                    <xsl:when test="ПараметрыБрекетовЗамковКолец/Параметр/Торк='Стандартный торк'">
                                        <item name="ст" />
                                    </xsl:when>
                                    <xsl:when test="ПараметрыБрекетовЗамковКолец/Параметр/Торк='Отрицательный торк'">
                                        <item name="-" />
                                    </xsl:when>
                                    <xsl:when test="ПараметрыБрекетовЗамковКолец/Параметр/Торк='Положительный торк'">
                                        <item name="+" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </value>
                        </property>
                    </xsl:if>
                    <!-- Паз -->
                    <xsl:if test="ПараметрыБрекетовЗамковКолец/Параметр/Паз">
                        <property name="paz" type="relation">
                            <title>Паз</title>
                            <value>
                                <xsl:choose>
                                    <xsl:when test="ПараметрыБрекетовЗамковКолец/Параметр/Паз = '018'">
                                        <item name="0,18" />
                                    </xsl:when>
                                    <xsl:when test="ПараметрыБрекетовЗамковКолец/Параметр/Паз = '022'">
                                        <item name="0,22" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <item name="{ПараметрыБрекетовЗамковКолец/Параметр/Паз}" />
                                    </xsl:otherwise>
                                </xsl:choose>
                                <!-- <xsl:value-of select="ПараметрыБрекетовЗамковКолец/Параметр/Паз" disable-output-escaping="yes" /> -->
                            </value>
                        </property>
                    </xsl:if>

                    <xsl:apply-templates select="Картинка" />
                </group>
                <xsl:apply-templates select="ЗначенияСвойств" />
            </properties>
        </page>
    </xsl:template>

    <xsl:template match="ЗначенияСвойств/ЗначенияСвойства">
        <xsl:param name="property" select="key('property', Ид)" />
        <xsl:param name="value-id" select="string(ИдЗначения)" />
        <xsl:param name="data-type">
            <xsl:choose>
                <xsl:when test="$property/Наименование = 'Брекет система' or $property/Наименование = 'Номер зуба' or $property/Наименование = 'Паз' or $property/Наименование = 'Материал' or $property/Наименование = 'Сечение дуги' or $property/Наименование = 'Форма' or $property/Наименование = 'Челюсть' or $property/Наименование = 'Расстояние между петлями' or $property/Наименование = 'Размер дуги' or $property/Наименование = 'Температура преформации' or $property/Наименование = 'Структура дуги' or $property/Наименование = 'Цвет' or $property/Наименование = 'Группа замков' or $property/Наименование = 'Торк'">spec_relation</xsl:when>
                <xsl:when test="$property/Наименование = 'Наличие петли на дуге' or $property/Наименование = 'Смещение брекета к десне' or $property/Наименование = 'Наличие крючка' or $property/Наименование = 'Смещение брекета к десне' or $property/Наименование = 'Низкое трение' or $property/Наименование = 'Лингвальная дуга'">boolean</xsl:when>
                <xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Число'">float</xsl:when>
                <xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Булево'">boolean</xsl:when>
                <xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Дата'">date</xsl:when>
                <xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Справочник'">relation</xsl:when>
                <xsl:otherwise>string</xsl:otherwise>
            </xsl:choose>
        </xsl:param>
        <xsl:if test="$property">
            <property name="{$property/Наименование}" title="{$property/Наименование}"  is-public="1" visible="visible" allow-runtime-add="1">
                <!-- type="{$data-type}" -->
                <type data-type="{$data-type}" >
                    <xsl:if test="$data-type = 'spec_relation'">
                        <xsl:attribute name="data-type">relation</xsl:attribute>
                    </xsl:if>
                </type>
                <title>
                    <xsl:value-of select="$property/Наименование"/>
                </title>
                <value>
                    <xsl:choose>
                        <!-- преобразуем множественное значение для номера зуба -->
                        <xsl:when test="$property/Наименование = 'Номер зуба'">
                            <xsl:apply-templates select="document(concat('udata://custom/explodeStr/',php:function('urlencode', string(Значение))))//item" mode="explodeStr"/>
                        </xsl:when>
                        <!-- преобразуем spec_relation значение , то есть все те что строка но выводим как список -->
                        <xsl:when test="$data-type = 'spec_relation'">
                            <item name="{Значение}" />
                        </xsl:when>
                        <!-- преобразуем boolen значение -->
                        <xsl:when test="$data-type = 'boolean'">
                            <xsl:choose>
                                <xsl:when test="Значение = 'ложь' or Значение = 'Нет'">0</xsl:when>
                                <xsl:otherwise>1</xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="$data-type = 'relation'">
                            <xsl:apply-templates select="$property/ТипыЗначений/ТипЗначений/ВариантыЗначений/ВариантЗначения[Ид = $value-id]" mode="relation-value" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="Значение" />
                        </xsl:otherwise>
                    </xsl:choose>
                </value>
            </property>
        </xsl:if>
    </xsl:template>

    <xsl:template match="item" mode="explodeStr">
        <item name="{text()}" />
    </xsl:template>

    <xsl:template match="НомерЗуба" mode="nomer_zuba">
        <item name="{text()}" />
    </xsl:template>


    <xsl:template match="Предложения/Предложение">
        <xsl:variable name="item_count" select="document(concat('udata://exchange/prepare_wrong_items_count/', php:function('urlencode', string(Количество))))/udata" />

        <page id="{Ид}" update-only="1">
            <properties>
                <xsl:apply-templates select="Цены" />

                <group name="catalog_stores_props" title="Склады">
                    <xsl:if test="NDS">
                        <property name="nds" title="НДС" type="relation" is-public="1" allow-runtime-add="1">
                            <type data-type="relation" />
                            <title>НДС</title>
                            <value>
                                <item name="{NDS}" />
                            </value>
                        </property>
                    </xsl:if>
                    <property name="common_quantity" title="Общее количество на складах" type="float" is-public="1" allow-runtime-add="1">
                        <type data-type="float" />
                        <title>Общее количество на складах</title>
                        <value>
                            <xsl:value-of select="$item_count"/>
                        </value>
                    </property>
                    <property name="common_quantity_flag" title="Наличие" type="boolean" is-public="1" allow-runtime-add="1">
                        <type data-type="boolean" />
                        <title>Наличие</title>
                        <value>
                            <xsl:choose>
                                <xsl:when test="$item_count &gt; 0">1</xsl:when>
                                <xsl:otherwise>0</xsl:otherwise>
                            </xsl:choose>
                        </value>
                    </property>
                </group>

                <xsl:if test="СмещениеКДесне">
                    <group name="special" title="1C: Специфические свойства">
                        <property name="smewenie_breketa_k_desne" title="Смещение брекета к десне" type="boolean" is-public="1" allow-runtime-add="1">
                            <type data-type="boolean" />
                            <title>Смещение брекета к десне</title>
                            <value>
                                <xsl:choose>
                                    <xsl:when test="СмещениеКДесне = 'Да'">1</xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                </xsl:choose>
                            </value>
                        </property>
                    </group>
                </xsl:if>
            </properties>
        </page>
    </xsl:template>
</xsl:stylesheet>