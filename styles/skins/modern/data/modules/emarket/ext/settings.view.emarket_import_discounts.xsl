<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/result[@method = 'listImportDiscountsLog']/data[@type = 'settings' and @action = 'view']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
						<xsl:apply-templates select="items/item" mode="discounts-log-item" />
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="discounts-log-item">
		<p><a href="/admin/emarket/viewImportDiscountsLog/{@id}/"><xsl:value-of select="node()" /></a></p>
	</xsl:template>

	<xsl:template match="/result[@method = 'viewImportDiscountsLog']/data[@type = 'settings' and @action = 'view']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
						<xsl:if test="files/file">
							<h3>Список файлов</h3>
							<xsl:apply-templates select="files/file" mode="discounts-log-file" />
						</xsl:if>

						<h3>Лог</h3>
						<p><xsl:value-of select="log" disable-output-escaping="yes" /></p>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="file" mode="discounts-log-file">
		<p><a href="/admin/emarket/viewImportDiscountsLogCustomFile/{ancestor::data/@id}/{@name}/" target="_blank"><xsl:value-of select="@title" /></a></p>
	</xsl:template>
</xsl:stylesheet>
