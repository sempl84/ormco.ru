<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="udata" mode="order-items">
		<xsl:variable name="order-info" select="document(concat('uobject://', @id))/udata"/>
		<div class="panel-settings" name="g_order_items">
			<summary class="group-tip">
				<xsl:text>Управление товарами в заказе.</xsl:text>
			</summary>
			<a data-name="{@name}" data-label="&label-order-items-group;"></a>
			<div class="title">
				<xsl:call-template name="group-tip">
					<xsl:with-param name="group" select="'order_items'"/>
					<xsl:with-param name="force-show" select="1"/>
				</xsl:call-template>
				<div class="round-toggle"></div>
				<h3>
					<xsl:text>&label-order-items-group;</xsl:text>
				</h3>
			</div>

			<div class="content">
				<div class="layout">
					<div class="column">
						<div class="row">

							<div class="col-md-12" style="margin-bottom:10px;">
								<a href="{$lang-prefix}/admin/emarket/editOrderAsUser/{@id}/"
								   class="btn color-blue btn-small">
									<xsl:attribute name="title">&label-edit-as-user-tip;</xsl:attribute>
									<xsl:text>&label-edit-as-user;</xsl:text>
								</a>
							</div>

							<div class="col-md-12">
								<table class="btable btable-bordered btable-striped">
									<thead>
										<tr>
											<th align="left">
												<xsl:text>&label-order-items-group;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-order-items-current-price;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-order-items-discount;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-order-items-original-price;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-weight;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-order-items-amount;</xsl:text>
											</th>

											<th align="left">
												<xsl:text>&label-order-items-summ;</xsl:text>
											</th>

											<th>
												<xsl:text>&label-delete;</xsl:text>
											</th>
										</tr>
									</thead>
									<tbody>
										<xsl:apply-templates select="items/item" mode="order-items"/>
										<tr>
											<td>
												<strong>
													<xsl:text>&label-order-discount;</xsl:text>
												</strong>
											</td>
											<td colspan="5">
												<a href="{$lang-prefix}/admin/emarket/discount_edit/{discount/@id}/">
													<xsl:value-of select="discount/@name"/>
												</a>
												<xsl:apply-templates
														select="document(concat('uobject://', discount/@id, '.discount_modificator_id'))//item"
														mode="discount-size"/>
												<xsl:apply-templates select="discount/description"/>
											</td>
											<td>
												<input type="text" class="default" name="order-discount-value"
													   value="{discount_value}" size="3">
													<xsl:attribute name="value">
														<xsl:choose>
															<xsl:when test="../summary/price/discount">
																<xsl:value-of
																		select="document(concat('udata://emarket/applyPriceCurrency/', ../summary/price/discount, '/'))/udata/price/actual"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="discount_value"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
												</input>

												<xsl:if test="summary/price/discount">
													<xsl:value-of select="concat(summary/price/discount/@percent, '% (', summary/price/discount/node(), ' руб.)')" />
												</xsl:if>
											</td>
											<td/>
										</tr>
										<xsl:apply-templates select="summary/price/bonus" mode="order-summary"/>
										<xsl:apply-templates select="$order-info//group[@name = 'order_delivery_props']"
															 mode="order_delivery"/>
										<tr>
											<td colspan="6">
												<strong>
													<xsl:text>&label-order-items-result;:</xsl:text>
												</strong>
											</td>

											<td>
												<strong>
													<xsl:apply-templates select="summary/price/actual" mode="price"/>
												</strong>
											</td>
											<td/>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="infoblock">
							<h3>
								<xsl:text>&type-edit-tip;</xsl:text>
							</h3>
							<div class="content">
							</div>
							<div class="group-tip-hide"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="order-items">
		<tr>
			<td>
				<xsl:apply-templates select="document(concat('uobject://',@id))/udata/object" mode="order-item-name"/>
			</td>

			<td>
				<xsl:choose>
					<xsl:when test="price/original &gt; 0">
						<xsl:apply-templates select="price/original" mode="price"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="price/actual" mode="price"/>
					</xsl:otherwise>
				</xsl:choose>
			</td>

			<td>
				<input type="text" class="default" name="item-discount-value[{@id}]" value="{discount_value}" size="3"/>
				<xsl:if test="discount/coupon">
					<p>
						Промокод:
						<a href="/admin/ormcoCoupons/viewCoupon/{discount/coupon/@id}/" target="_blank"><xsl:value-of select="discount/coupon/@code" /></a>
						<xsl:text>, </xsl:text>
						<xsl:value-of select="concat(discount/coupon/@percent, '% (', discount/coupon/@value, ' руб.)')" />
					</p>
				</xsl:if>
			</td>

			<td>
				<xsl:apply-templates select="price/actual" mode="price"/>
			</td>

			<td>
				<input type="number" class="default" name="order-weight-item[{@id}]" value="{weight}" size="3"/>
			</td>

			<td>
				<input type="number" min="1" class="default" name="order-amount-item[{@id}]" value="{amount}" size="3"/>
			</td>

			<td>
				<xsl:apply-templates select="total-price/actual" mode="price"/>
			</td>

			<td class="center">
				<div class="checkbox">
					<input type="checkbox" name="order-del-item[]" value="{@id}" class="check"/>
				</div>

			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>