<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl">

    <!-- Edit order -->
    <xsl:template match="/result[@method = 'order_edit']/data/object" mode="form-modify">
        <xsl:variable name="order-info" select="document(concat('udata://emarket/order/', @id))/udata" />
        <xsl:variable name="customer-id" select="$order-info/customer/object/@id" />
        <xsl:variable name="type-customer" select="$order-info/customer/object/@type-guid" />
        <xsl:variable name="one-click-order" select="//group[@name = 'purchase_one_click']" />

        <xsl:call-template name="notify">
                <xsl:with-param name="order-info" select="$order-info" />
                <xsl:with-param name="one-click-order" select="$one-click-order" />
        </xsl:call-template>

        <!-- Информация о заказе -->
        <xsl:apply-templates select=".//group[@name = 'order_props']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <xsl:apply-templates select="//payment" mode="payment-view" />

        <!-- Информация о заказчике -->
        <xsl:apply-templates select="$order-info/customer">
                <xsl:with-param name="customer-id" select="$customer-id" />
                <xsl:with-param name="type-customer" select="$type-customer" />
                <xsl:with-param name="one-click-order" select="$one-click-order" />
        </xsl:apply-templates>

        <!-- Сырые данные из формы -->
        <xsl:apply-templates select=".//group[@name = 'form_params']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <!-- Сырые данные из мероприятия -->
        <xsl:apply-templates select=".//group[@name = 'event_params']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <!-- Скидки -->
        <xsl:apply-templates select=".//group[@name = 'events_discount']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <xsl:apply-templates select=".//group[@name = 'order_payment_props' or @name = 'order_delivery_props']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <xsl:apply-templates select=".//group[@name = 'statistic_info']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <xsl:apply-templates select=".//group[@name = 'order_abandoned_orders']" mode="form-modify">
            <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <!-- Наименования заказа (с удалением) -->
        <xsl:apply-templates select="$order-info" mode="order-items" />

        <!-- Комментарий к заказу -->
        <xsl:apply-templates select=".//group[@name = 'dop_parametry']" mode="form-modify">
                <xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
        </xsl:apply-templates>

        <!-- Список всех заказов покупателя -->
        <xsl:if test="$customer-id">
                <xsl:apply-templates select="document(concat('udata://emarket/ordersList/', $customer-id, '?links'))/udata">
                        <xsl:with-param name="customer-id" select="$customer-id" />
                </xsl:apply-templates>
        </xsl:if>

        <script type="text/javascript">
            $(function() {
                $('.toggle_fields').slideToggle();
                $('.toggle_fields_expander').bind('click', function() {
                    $('.toggle_fields').slideToggle();
                });
            })
        </script>
    </xsl:template>


    <xsl:template match="group[@name = 'order_delivery_props']" mode="form-modify">
        <div class="panel-settings" name="g_{@name}">
            <a data-name="{@name}" data-label="{@title}"></a>
            <div class="title">
                <xsl:call-template name="group-tip">
                    <xsl:with-param name="group" select="@name"/>
                </xsl:call-template>
                <div class="round-toggle"></div>
                <h3>
                    <xsl:value-of select="@title"/>
                </h3>
            </div>

            <div class="content">
                <div class="layout">
                    <div class="column">
                        <div class="row">
                            <xsl:variable name="deliveryGuid" select="document(concat('uobject://',field[@name='delivery_id']/values/item[1]/@id))/udata/object/properties/group[@name='delivery_description_props']/property[@name='delivery_type_id']/value/item[1]/@guid"/>
                            <xsl:variable name="apiShipGUID" select="'emarket-deliverytype-27958'"/>
                            <xsl:apply-templates select="field[@name='delivery_id']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='delivery_status_id']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='delivery_address']" mode="form-modify"/>

                            <xsl:if test="$deliveryGuid=$apiShipGUID">
                                <xsl:apply-templates select="field[@name='delivery_date']" mode="form-modify"/>
                                <xsl:apply-templates select="field[@name='pickup_date']" mode="form-modify"/>
                                <xsl:apply-templates select="field[@name='pickup_type']" mode="form-modify"/>
                                <xsl:apply-templates select="field[@name='delivery_point_in']" mode="order_delivery_apiship_field"/>
                                <xsl:apply-templates select="." mode="order_delivery_apiship"/>
                            </xsl:if>

                            <xsl:apply-templates select="field[@name='delivery_price']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='total_weight']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='total_width']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='total_height']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='total_length']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='prefer_date_time_delivery_start']" mode="form-modify"/>
                            <xsl:apply-templates select="field[@name='prefer_date_time_delivery_finish']" mode="form-modify"/>
                        </div>
                    </div>
                    <div class="column">
                        <div class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="{$context-manul-url}">
                            </div>
                            <div class="group-tip-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="order-items">
        <xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata/object" />
        <tr>
            <td>
                <a href="/admin/catalog/edit/{$item_info//property[@name='item_link']/value/page/@id}/">
                    <xsl:apply-templates select="$item_info" mode="order-item-name" />
                </a>
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="price/original &gt; 0">
                        <xsl:apply-templates select="price/original" mode="price" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="price/actual" mode="price" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <input type="text" class="default" name="item-discount-value[{@id}]" value="{discount_value}" size="3" />
            </td>
            <td>
                <xsl:apply-templates select="price/actual" mode="price" />
            </td>
            <td>
                <input type="number" class="default" name="order-weight-item[{@id}]" value="{weight}" size="3"/>
            </td>
            <td>
                <input type="number" min="1" class="default" name="order-amount-item[{@id}]" value="{amount}" size="3" />
            </td>
            <td>
                <xsl:apply-templates select="total-price/actual" mode="price" />
            </td>
            <td class="center">
                <div class="checkbox">
                    <input type="checkbox" name="order-del-item[]" value="{@id}" class="check"/>
                </div>
            </td>
        </tr>
    </xsl:template>


    <!-- <xsl:template match="field[@name = 'payment_id']" mode="form-modify">
            <div class="col-md-6">
                    <div class="title-edit">
                            <acronym title="{@tip}">
                                    <xsl:apply-templates select="." mode="sys-tips" />
                                    <xsl:value-of select="@title" />
                            </acronym>
                            <xsl:apply-templates select="." mode="required_text" />
                    </div>
                    <span class="event_discount_wrap">
                            <input class="default event_discount" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                                    <xsl:apply-templates select="@type" mode="number" />
                            </input>
                            <div class="buttons">

                                    <input class="btn color-blue" id="apply_order" type="button" value="Одобрить заказ (письмо клиенту)" />

                            </div>
                    </span>
            </div>


            <script>
        <![CDATA[
        jQuery(document).ready(function () {

            jQuery("#apply_order").click(function() {
                            //console.log('apply_order');
                            //return false;
                            var order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
                                    this_form = jQuery(this).parents('form');
                    jQuery.ajax({
                         url: "/emarket/applyOrder/"+order_id+"/",
                         dataType: 'text',
                         async: false,
                         success: function(e){
                              location.reload();
                         }
                    });
                return false;
            });
            jQuery("#apply_event_discount").click(function() {

                            console.log('apply_event_discount');
                            //return false;

                            var new_price = jQuery('.event_discount_wrap input:first').val(),
                                    order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
                                    this_form = jQuery(this).parents('form');
                    jQuery.ajax({
                         url: "/udata/emarket/applyEventDiscount/"+order_id+"/"+new_price+"/",
                         dataType: 'text',
                         async: false,
                         success: function(e){
                              //if(e == 'ok'){
                                    console.log(jQuery('.list li input[name="save-mode"]:first',this_form));
                                    jQuery('.list li input[name="save-mode"]:first',this_form).trigger( "click" );
                              //}
                              //reload page
                         }
                    });
                return false;
            });

        });
        ]]>
   </script>

    </xsl:template> -->

    <!-- ссылка на платеж квитанции или счет подробнее -->
    <xsl:template match="field[@type = 'relation' and @name='payment_id']" mode="form-modify">
        <div class="col-md-6 relation clearfix" id="{generate-id()}" umi:type="{@type-id}">
            <xsl:if test="not(@required = 'required')">
                <xsl:attribute name="umi:empty"><xsl:text>empty</xsl:text></xsl:attribute>
            </xsl:if>
            <div class="title-edit">
                <span class="label">
                    <acronym title="{@tip}">
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                        <!-- квитанция -->
                        <xsl:if test="values/item[@selected]/@id=14044">
                            (<a href="/tcpdf/docs/receipt.php?oi={/result/data/object/@id}/" target="_blank">Открыть квитанцию</a>)
                        </xsl:if>
                        <!-- счет для юр лиц -->
                        <xsl:if test="values/item[@selected]/@id=13911">
                            (<a href="/tcpdf/docs/invoicee.php?oi={/result/data/object/@id}/" target="_blank">Открыть счет</a>)
                        </xsl:if>
                        (<a href="/delivery/oplatit-zakaz-po-nomeru/?order_id={/result/data/object/@id}" target="_blank">Оплата онлайн</a>)
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span></span>
            </div>
            <div class="layout-row-icon">
                <div class="layout-col-control">
                    <select autocomplete="off" name="{@input_name}" id="relationSelect{generate-id()}">
                        <xsl:apply-templates select="." mode="required_attr" />
                        <xsl:if test="@multiple = 'multiple'">
                            <xsl:attribute name="multiple">multiple</xsl:attribute>
                            <xsl:attribute name="style">height: 62px;</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="not(values/item/@selected)">
                            <option value=""></option>
                        </xsl:if>
                        <xsl:apply-templates select="values/item" />
                    </select>
                </div>
                <xsl:if test="@public-guide = '1'">
                    <div class="layout-col-icon">
                        <a id="relationButton{generate-id()}" class="icon-action relation-add">
                            <i class="small-ico i-add"></i>
                        </a>
                    </div>
                </xsl:if>
            </div>
            <xsl:if test="@public-guide = '1'">
                <div>
                    <a href="{$lang-prefix}/admin/data/guide_items/{@type-id}/"><xsl:text>&label-edit-guide-items;</xsl:text></a>
                </div>
            </xsl:if>
            <div class="buttons">
                <input class="btn color-blue" id="apply_order" type="button" value="Одобрить заказ (письмо клиенту)" />
            </div>
        </div>

        <script>
            <![CDATA[
            jQuery(document).ready(function () {
                jQuery("#apply_order").click(function() {
                    //console.log('apply_order');
                    //return false;
                    var order_id = ]]><xsl:value-of select="/result/data/object/@id" /><![CDATA[;
                        this_form = jQuery(this).parents('form');
                    jQuery.ajax({
                        url: "/emarket/applyOrder/"+order_id+"/",
                        dataType: 'text',
                        async: false,
                        success: function(e){
                             location.reload();
                        }
                    });
                    return false;
                });
                jQuery("#apply_event_discount").click(function() {
                    console.log('apply_event_discount');
                    //return false;

                    var new_price = jQuery('.event_discount_wrap input:first').val(),
                        order_id = ]]><xsl:value-of select="/result/data/object/@id" /><![CDATA[;
                        this_form = jQuery(this).parents('form');
                        jQuery.ajax({
                            url: "/udata/emarket/applyEventDiscount/"+order_id+"/"+new_price+"/",
                            dataType: 'text',
                            async: false,
                            success: function(e){
                                //if(e == 'ok'){
                                    console.log(jQuery('.list li input[name="save-mode"]:first',this_form));
                                    jQuery('.list li input[name="save-mode"]:first',this_form).trigger( "click" );
                                //}
                                //reload page
                            }
                        });
                    return false;
                });
            });]]>
        </script>
    </xsl:template>
    <!-- ссылка на юр лицо подробнее -->
    <xsl:template match="field[@type = 'relation' and @name='legal_person']" mode="form-modify">
        <div class="col-md-6 relation clearfix" id="{generate-id()}" umi:type="{@type-id}">
            <xsl:if test="not(@required = 'required')">
                <xsl:attribute name="umi:empty"><xsl:text>empty</xsl:text></xsl:attribute>
            </xsl:if>
            <div class="title-edit">
                <span class="label">
                    <acronym title="{@tip}">
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                        <xsl:if test="values/item[@selected]/@id">
                            (<a href="/admin/data/guide_item_edit/{values/item[@selected]/@id}/" target="_blank">Подробнее</a>)
                        </xsl:if>
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span></span>
            </div>
            <div class="layout-row-icon">
                <div class="layout-col-control">
                    <select autocomplete="off" name="{@input_name}" id="relationSelect{generate-id()}">
                        <xsl:apply-templates select="." mode="required_attr" />
                        <xsl:if test="@multiple = 'multiple'">
                            <xsl:attribute name="multiple">multiple</xsl:attribute>
                            <xsl:attribute name="style">height: 62px;</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="not(values/item/@selected)">
                            <option value=""></option>
                        </xsl:if>
                        <xsl:apply-templates select="values/item" />
                    </select>
                </div>
                <xsl:if test="@public-guide = '1'">
                    <div class="layout-col-icon">
                        <a id="relationButton{generate-id()}" class="icon-action relation-add">
                            <i class="small-ico i-add"></i>
                        </a>
                    </div>
                </xsl:if>
            </div>
            <xsl:if test="@public-guide = '1'">
                <div>
                    <a href="{$lang-prefix}/admin/data/guide_items/{@type-id}/"><xsl:text>&label-edit-guide-items;</xsl:text></a>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="properties/group[@name = 'statistic_info']" mode="form-modify">
        <div class="panel-settings" name="g_{@name}">
            <a data-name="{@name}" data-label="{@title}"></a>
            <div class="title">
                <xsl:call-template name="group-tip">
                    <xsl:with-param name="param" select="@name" />
                </xsl:call-template>
                <div class="round-toggle"></div>
                <h3>
                    <xsl:value-of select="@title" />
                </h3>
            </div>
            <div class="content">
                <div class="layout">
                    <div class="column">
                        <div class="row">
                            <xsl:apply-templates select="field" mode="form-modify" />

                            <xsl:apply-templates select="/" mode="form-modify-update-time" />
                        </div>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&type-edit-tip;</xsl:text>
                            </h3>
                            <div class="content" >
                            </div>
                            <div class="group-tip-hide"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/" mode="form-modify-update-time">
        <xsl:variable name="updateTime" select="document(concat('uobject://', /result/data/object/@id, '/'))/udata/object/@update-time" />

        <div class="col-md-6">
            <div class="title-edit">
                Дата последнего изменения заказа
            </div>

            <xsl:value-of select="document(concat('udata://system/convertDate/', $updateTime , '/(Y-m-d%20H:i:s)'))/udata" />
        </div>
    </xsl:template>

    <xsl:template match="properties/group[@name = 'order_abandoned_orders']/field" mode="form-modify">
        <div class="col-md-6">

            <div class="title-edit">
                <acronym title="{@tip}"><xsl:value-of select="./@title" /></acronym>
            </div>

            <xsl:value-of select="node()" />
        </div>
    </xsl:template>


    <xsl:template match="properties/group[@name = 'order_payment_props']" mode="form-modify">
		<xsl:param name="show-name"><xsl:text>1</xsl:text></xsl:param>
		<xsl:param name="show-type"><xsl:text>1</xsl:text></xsl:param>

        <div class="panel-settings" name="g_{@name}">
			<xsl:if test="@name = 'more_params'">
				<xsl:attribute name="class">panel-settings extended_fields</xsl:attribute>
			</xsl:if>
			<a data-name="{@name}" data-label="{@title}"></a>
            <div class="title">
				<xsl:call-template name="group-tip" />
                <div class="round-toggle"></div>
                <h3><xsl:value-of select="@title"/></h3>
            </div>

            <div class="content">
				<div class="layout">
					<div class="column">
						<div class="row">
							<xsl:apply-templates select="." mode="form-modify-group-fields">
								<xsl:with-param name="show-name" select="$show-name"/>
								<xsl:with-param name="show-type" select="$show-type"/>
							</xsl:apply-templates>
                            <div class="col-md-12">
                                <div class="title-edit">
                                    &nbsp;
                                </div>
                                <span style="font-size: 18px;">
                                    ID операции:
                                    &nbsp;
                                    <xsl:value-of select="/result/data/object/@id" />
                                </span>
                            </div>
						</div>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&type-edit-tip;</xsl:text>
							</h3>
							<div class="content"></div>
							<div class="group-tip-hide"></div>
						</div>
					</div>
				</div>
            </div>
        </div>
	</xsl:template>
</xsl:stylesheet>