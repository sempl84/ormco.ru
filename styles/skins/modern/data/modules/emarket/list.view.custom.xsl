<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common" [
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/TR/xlink">
    <xsl:template match="/result[@method = 'stats']/data[@type = 'list' and @action = 'view']">
        <div class="tabs editing notextselect">
            <div class="section selected"><a class="ordersStats">&stat-order;</a></div>
            <div class="section"><a class="topPopularProduct">&stat-popular;</a></div>
            <div class="section"><a class="commonStats">&stat-common;</a></div>
			<div class="section"><a class="clientsStats">&stat-client;</a></div>
        </div>
        <div class="location">
            <script type="text/javascript" src="/styles/skins/modern/design/js/emarketstat.js"></script>
            <div class="loc-left">
                <xsl:call-template name="date-picker-range">
                    <xsl:with-param name="fromDate" select="//data/@fromDate"/>
                    <xsl:with-param name="toDate" select="//data/@toDate"/>
                </xsl:call-template>
            </div>
            <a class="btn-action loc-right infoblock-show">
                <i class="small-ico i-info"></i>
                <xsl:text>&help;</xsl:text>
            </a>
        </div>

        <div class="layout">
            <div class="column">
                <div id="ordersStats" class="tabsStat">
                    <xsl:call-template name="orders-stats-custom" />
                </div>
                <div id="topPopularProduct" class="tabsStat" style="display:none;">
                    <p style="text-align:center;font-weight:bold;font-style:italic;color:#999;">Для более точной статистики рекомендуется сначала переиндексировать заказы в <a href="/admin/emarket/config/">настройках модуля Интернет-магазин</a></p>
                    <xsl:call-template name="top-popular-product" />
                </div>
                <div id="commonStats" class="tabsStat" style="display:none;">
                    <xsl:apply-templates select="//group[@name='stats']" mode="statsOrder"/>
                </div>
                <div id="clientsStats" class="tabsStat" style="display:none;">
					<div class="row">
						<div style="loc-left">
							<label>
								Количество клиентов
								<input name="clients_limit" type="text" class="clients_limit default" value="15" style="width:100px;display:inline-block;" />
							</label>
						</div>
					</div>
                    <div class="top_clients_result_area"></div>
                </div>
            </div>
            <div class="column">
                <div  class="infoblock">
                    <h3><xsl:text>&label-quick-help;</xsl:text></h3>
                    <div class="content" title="{$context-manul-url}"></div>
                    <div class="infoblock-hide"></div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="orders-stats-custom">
        <xsl:call-template name="ui-smc-table">
            <xsl:with-param name="content-type">objects</xsl:with-param>
            <xsl:with-param name="control-params">realpayments_custom</xsl:with-param>
            <xsl:with-param name="search-show">1</xsl:with-param>
            <xsl:with-param name="domains-show">0</xsl:with-param>
            <xsl:with-param name="hide-csv-import-button">1</xsl:with-param>
            <xsl:with-param name="show-toolbar">0</xsl:with-param>
            <xsl:with-param name="enable-edit">false</xsl:with-param>
            <xsl:with-param name="js-required-props-menu">['order_items','total_price','payment_id','customer_id','order_date','payment_date','http_target','source_domain']</xsl:with-param>
            <xsl:with-param name="js-visible-props-menu">'order_items[250px]|total_price[250px]|payment_id[250px]|customer_id[250px]|order_date[250px]|payment_date[250px]|http_target[250px]|source_domain[250px]'</xsl:with-param>
        </xsl:call-template>

        <style>
            .tabs-content .location:nth-of-type(1){min-height:0;}
            .tabs-content .location:nth-of-type(2){min-height:0;}
        </style>
    </xsl:template>
</xsl:stylesheet>