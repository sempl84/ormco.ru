<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl">

	<!-- Edit order -->
	<xsl:template match="/result[@method = 'order_edit']/data/object" mode="form-modify">
		<xsl:variable name="order-info" select="document(concat('udata://emarket/order/', @id))/udata" />
		<xsl:variable name="customer-id" select="$order-info/customer/object/@id" />
		<xsl:variable name="type-customer" select="$order-info/customer/object/@type-guid" />
		<xsl:variable name="one-click-order" select="//group[@name = 'purchase_one_click']" />

		<xsl:call-template name="notify">
			<xsl:with-param name="order-info" select="$order-info" />
			<xsl:with-param name="one-click-order" select="$one-click-order" />
		</xsl:call-template>

		<!-- Информация о заказе -->
		<xsl:apply-templates select=".//group[@name = 'order_props']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>

		<xsl:apply-templates select="//payment" mode="payment-view" />
		
		<!-- Информация о заказчике -->
		<xsl:apply-templates select="$order-info/customer">
			<xsl:with-param name="customer-id" select="$customer-id" />
			<xsl:with-param name="type-customer" select="$type-customer" />
			<xsl:with-param name="one-click-order" select="$one-click-order" />
		</xsl:apply-templates>
		
		<!-- Сырые данные из формы -->
		<xsl:apply-templates select=".//group[@name = 'form_params']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>
		
		<!-- Сырые данные из мероприятия -->
		<xsl:apply-templates select=".//group[@name = 'event_params']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>
		
		<!-- Скидки -->
		<xsl:apply-templates select=".//group[@name = 'events_discount']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>
		
		

		<xsl:apply-templates select=".//group[@name = 'order_payment_props' or @name = 'order_delivery_props']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>

		<xsl:apply-templates select=".//group[@name = 'statistic_info']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>

		<!-- Наименования заказа (с удалением) -->
		<xsl:apply-templates select="$order-info" mode="order-items" />
		
		<!-- Комментарий к заказу -->
		<xsl:apply-templates select=".//group[@name = 'dop_parametry']" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>

		<!-- Список всех заказов покупателя -->
		<xsl:if test="$customer-id">
			<xsl:apply-templates select="document(concat('udata://emarket/ordersList/', $customer-id, '?links'))/udata">
				<xsl:with-param name="customer-id" select="$customer-id" />
			</xsl:apply-templates>
		</xsl:if>

		<script type="text/javascript">
			$(function() {
				$('.toggle_fields').slideToggle();
				$('.toggle_fields_expander').bind('click', function() {
					$('.toggle_fields').slideToggle();
				});
			})
		</script>

	</xsl:template>
	
	<xsl:template match="item" mode="order-items">
		<xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata/object" />
		
		<tr>
			<td>
				<a href="/admin/catalog/edit/{$item_info//property[@name='item_link']/value/page/@id}/">
					<xsl:apply-templates select="$item_info" mode="order-item-name" />
				</a>
			</td>

			<td>
				<xsl:choose>
					<xsl:when test="price/original &gt; 0">
						<xsl:apply-templates select="price/original" mode="price" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="price/actual" mode="price" />
					</xsl:otherwise>
				</xsl:choose>
			</td>

			<td>
				<input type="text" class="default" name="item-discount-value[{@id}]" value="{discount_value}" size="3" />
			</td>

			<td>
				<xsl:apply-templates select="price/actual" mode="price" />
			</td>

			<td>
				<input type="number" min="1" class="default" name="order-amount-item[{@id}]" value="{amount}" size="3" />
			</td>

			<td>
				<xsl:apply-templates select="total-price/actual" mode="price" />
			</td>

			<td class="center">
				<div class="checkbox">
					<input type="checkbox" name="order-del-item[]" value="{@id}" class="check"/>
				</div>

			</td>
		</tr>
	</xsl:template>
	
	
	
	<!-- <xsl:template match="field[@name = 'payment_id']" mode="form-modify">
		<div class="col-md-6">
			<div class="title-edit">
				<acronym title="{@tip}">
					<xsl:apply-templates select="." mode="sys-tips" />
					<xsl:value-of select="@title" />
				</acronym>
				<xsl:apply-templates select="." mode="required_text" />
			</div>
			<span class="event_discount_wrap">
				<input class="default event_discount" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
					<xsl:apply-templates select="@type" mode="number" />
				</input>
				<div class="buttons">
					
					<input class="btn color-blue" id="apply_order" type="button" value="Одобрить заказ (письмо клиенту)" />
					
				</div>
			</span>
		</div>
		
		
		<script>
            <![CDATA[
            jQuery(document).ready(function () {

                jQuery("#apply_order").click(function() {
	         		//console.log('apply_order');
	         		//return false;
	         		var order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
	         			this_form = jQuery(this).parents('form');
	                jQuery.ajax({
	                     url: "/emarket/applyOrder/"+order_id+"/",
	                     dataType: 'text',
	                     async: false,
	                     success: function(e){
	                          location.reload();
	                     }
	              	});
                    return false;
                });
                jQuery("#apply_event_discount").click(function() {
	         		
	         		console.log('apply_event_discount');
	         		//return false;
	         		
	         		var new_price = jQuery('.event_discount_wrap input:first').val(),
	         			order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
	         			this_form = jQuery(this).parents('form');
	                jQuery.ajax({
	                     url: "/udata/emarket/applyEventDiscount/"+order_id+"/"+new_price+"/",
	                     dataType: 'text',
	                     async: false,
	                     success: function(e){
	                          //if(e == 'ok'){
	                          	console.log(jQuery('.list li input[name="save-mode"]:first',this_form));
	                          	jQuery('.list li input[name="save-mode"]:first',this_form).trigger( "click" );
	                          //}
	                          //reload page
	                     }
	              	});
                    return false;
                });

            });
            ]]>
       </script>

	</xsl:template> -->
	
	<!-- ссылка на платеж квитанции или счет подробнее -->
	<xsl:template match="field[@type = 'relation' and @name='payment_id']" mode="form-modify">
		<div class="col-md-6 relation clearfix" id="{generate-id()}" umi:type="{@type-id}">
			<xsl:if test="not(@required = 'required')">
				<xsl:attribute name="umi:empty"><xsl:text>empty</xsl:text></xsl:attribute>
			</xsl:if>
			<div class="title-edit">
				<span class="label">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
						<!-- квитанция -->
						<xsl:if test="values/item[@selected]/@id=14044">
							(<a href="/tcpdf/docs/receipt.php?oi={/result/data/object/@id}/" target="_blank">Открыть квитанцию</a>)
						</xsl:if>
						<!-- счет для юр лиц -->
						<xsl:if test="values/item[@selected]/@id=13911">
							(<a href="/tcpdf/docs/invoicee.php?oi={/result/data/object/@id}/" target="_blank">Открыть счет</a>)
						</xsl:if>
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
				<span>

				</span>
			</div>
			<div class="layout-row-icon">
				<div class="layout-col-control">
					<select autocomplete="off" name="{@input_name}" id="relationSelect{generate-id()}">
						<xsl:apply-templates select="." mode="required_attr" />
						<xsl:if test="@multiple = 'multiple'">
							<xsl:attribute name="multiple">multiple</xsl:attribute>
							<xsl:attribute name="style">height: 62px;</xsl:attribute>
						</xsl:if>
						<xsl:if test="not(values/item/@selected)">
							<option value=""></option>
						</xsl:if>
						<xsl:apply-templates select="values/item" />
					</select>
				</div>
				<xsl:if test="@public-guide = '1'">
					<div class="layout-col-icon">
						<a id="relationButton{generate-id()}" class="icon-action relation-add">
							<i class="small-ico i-add"></i>
						</a>
					</div>
				</xsl:if>
				
				

			</div>
			<xsl:if test="@public-guide = '1'">
				<div>
					<a href="{$lang-prefix}/admin/data/guide_items/{@type-id}/"><xsl:text>&label-edit-guide-items;</xsl:text></a>
				</div>
			</xsl:if>
			<div class="buttons">
				<input class="btn color-blue" id="apply_order" type="button" value="Одобрить заказ (письмо клиенту)" />
			</div>
		</div>
		
		<script>
            <![CDATA[
            jQuery(document).ready(function () {

                jQuery("#apply_order").click(function() {
	         		//console.log('apply_order');
	         		//return false;
	         		var order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
	         			this_form = jQuery(this).parents('form');
	                jQuery.ajax({
	                     url: "/emarket/applyOrder/"+order_id+"/",
	                     dataType: 'text',
	                     async: false,
	                     success: function(e){
	                          location.reload();
	                     }
	              	});
                    return false;
                });
                jQuery("#apply_event_discount").click(function() {
	         		
	         		console.log('apply_event_discount');
	         		//return false;
	         		
	         		var new_price = jQuery('.event_discount_wrap input:first').val(),
	         			order_id = ]]><xsl:value-of select="/result/data/object/@id" /> <![CDATA[;
	         			this_form = jQuery(this).parents('form');
	                jQuery.ajax({
	                     url: "/udata/emarket/applyEventDiscount/"+order_id+"/"+new_price+"/",
	                     dataType: 'text',
	                     async: false,
	                     success: function(e){
	                          //if(e == 'ok'){
	                          	console.log(jQuery('.list li input[name="save-mode"]:first',this_form));
	                          	jQuery('.list li input[name="save-mode"]:first',this_form).trigger( "click" );
	                          //}
	                          //reload page
	                     }
	              	});
                    return false;
                });

            });
            ]]>
       </script>
	</xsl:template>
	<!-- ссылка на юр лицо подробнее -->
	<xsl:template match="field[@type = 'relation' and @name='legal_person']" mode="form-modify">
		<div class="col-md-6 relation clearfix" id="{generate-id()}" umi:type="{@type-id}">
			<xsl:if test="not(@required = 'required')">
				<xsl:attribute name="umi:empty"><xsl:text>empty</xsl:text></xsl:attribute>
			</xsl:if>
			<div class="title-edit">
				<span class="label">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
						<xsl:if test="values/item[@selected]/@id">
							(<a href="/admin/data/guide_item_edit/{values/item[@selected]/@id}/" target="_blank">Подробнее</a>)
						</xsl:if>
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
				<span>

				</span>
			</div>
			<div class="layout-row-icon">
				<div class="layout-col-control">
					<select autocomplete="off" name="{@input_name}" id="relationSelect{generate-id()}">
						<xsl:apply-templates select="." mode="required_attr" />
						<xsl:if test="@multiple = 'multiple'">
							<xsl:attribute name="multiple">multiple</xsl:attribute>
							<xsl:attribute name="style">height: 62px;</xsl:attribute>
						</xsl:if>
						<xsl:if test="not(values/item/@selected)">
							<option value=""></option>
						</xsl:if>
						<xsl:apply-templates select="values/item" />
					</select>
				</div>
				<xsl:if test="@public-guide = '1'">
					<div class="layout-col-icon">
						<a id="relationButton{generate-id()}" class="icon-action relation-add">
							<i class="small-ico i-add"></i>
						</a>
					</div>
				</xsl:if>
				
				

			</div>
			<xsl:if test="@public-guide = '1'">
				<div>
					<a href="{$lang-prefix}/admin/data/guide_items/{@type-id}/"><xsl:text>&label-edit-guide-items;</xsl:text></a>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

</xsl:stylesheet>