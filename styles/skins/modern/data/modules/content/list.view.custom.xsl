<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/result[@method = 'sitetree']/data[@type = 'list' and @action = 'view']">
        <div class="tabs-content module">
            <div class="section selected">
                <div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>&help;</xsl:text>
                    </a>
                </div>

                <div class="layout">
					<a style="border:solid 1px #0887B7; padding:5px 10px; background: #0887B7; color:#fff; float: right; text-align:center" class="clear_cache_button" href="#">Очистка кеша</a>
					<script type="text/javascript"><![CDATA[
						$(document).ready(function (){
							$('.clear_cache_button').on('click',function (event){
								event.preventDefault();
								var csrf = window.parent.csrfProtection.token;
								$.ajax({
									url: '/udata/catalog/clear_bracket_cache/?csrf=' + csrf,
									dataType: 'xml',
									success: function(data){
										console.log(data);
										openDialog('', 'Очистка кеша', {
											cancelButton: true,
											cancelText: 'Закрыть',
											confirmButton: false,
											html: 'Очистка кеша успешно завершена. Очищено файлов: ' + $(data).find('count').text(),
										});
									}
								});
							});
						});
					]]></script>
                    <div class="column">
                        <xsl:apply-templates select="domain" mode="list-view"/>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="{$context-manul-url}">
                            </div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</xsl:template>
</xsl:stylesheet>