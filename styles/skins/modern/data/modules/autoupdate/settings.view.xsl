<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/autoupdate">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="data[@type = 'settings' and @action = 'view']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
						<form method="post" action="do/" enctype="multipart/form-data">
							<xsl:apply-templates select="group" mode="settings.view"/>
						</form>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="group" mode="settings.view">
		<xsl:call-template name="autoupdate" />

		<div class="panel-settings">
			<div class="title" style="cursor:default">
				<h3><xsl:value-of select="@label" /></h3>
			</div>
			<div class="content">
				<table class="tableContent">
					<tbody>
						<xsl:apply-templates select="option" mode="settings.view" />
					</tbody>
				</table>
				<div class="buttons">
					<div class="pull-right">
						<xsl:choose>
							<xsl:when test="option[@name='disabled-by-host']">
								<p>
									&label-updates-disabled-by-host;
									<a href="http://{option[@name='disabled-by-host']}/admin/autoupdate/versions/">http://<xsl:value-of select="option[@name='disabled-by-host']"/></a>
								</p>
							</xsl:when>
							<xsl:otherwise>
								<input type="button" class="btn color-blue" value="&label-check-updates;" id="update" />
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="option[@type = 'check' and @name = 'disabled-by-host']" mode="settings.view" />

	<xsl:template match="option[@type = 'boolean' and @name = 'disabled']" mode="settings.view" />

	<xsl:template name="autoupdate">
		<script src="/styles/skins/modern/design/js/autoupdate.js?{$system-build}" />
	</xsl:template>

</xsl:stylesheet>
