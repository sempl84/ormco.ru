<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="option[@name = 'megaindex-password']" mode="settings.modify-option">
		<input class="default" type="password" name="{@name}" value="{value}" id="{@name}" />
	</xsl:template>

	<xsl:template match="/result[@method = 'config']/data[@type = 'settings' and @action = 'modify']">
		<div class="tabs-content module">
		<div class="section selected">
		<div class="location">
			<div class="save_size"></div>
			<a class="btn-action loc-right infoblock-show">
				<i class="small-ico i-info"></i>
				<xsl:text>&help;</xsl:text>
			</a>
		</div>

		<div class="layout">
		<div class="column">
		<form method="post" action="do/" enctype="multipart/form-data">
			<div class="panel-settings">
				<div class="title">
					<div class="round-toggle"></div>
					<h3><xsl:text>&header-seo-domains;</xsl:text></h3>
				</div>
				<div class="content">
					<xsl:apply-templates select="group[@name != 'yandex']" mode="settings-modify" />

				</div>
			</div>
			<div class="row">
				<xsl:call-template name="std-form-buttons-settings" />
			</div>
		</form>
		</div>
			<div class="column">
				<div  class="infoblock">
					<h3>
						<xsl:text>&label-quick-help;</xsl:text>
					</h3>
					<div class="content" title="{$context-manul-url}">
					</div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
		</div>
		</div>
		<xsl:apply-templates select="/result/@demo" mode="stopdoItInDemo" />

		<xsl:call-template name="error-checker" />
		<script>
			var form = $('form').eq(0);
			jQuery(form).submit(function() {
				return checkErrors({
					form: form,
					check: {
						empty: 'input.required',
					}
				});
			});
		</script>
	</xsl:template>

	<xsl:template match="/result[@method = 'config']//group" mode="settings-modify">
		<xsl:variable name="seo-title" select="option[starts-with(./@name, 'title')]" />
		<xsl:variable name="seo-default-title" select="option[starts-with(./@name, 'default')]" />
		<xsl:variable name="seo-keywords" select="option[starts-with(./@name, 'keywords')]" />
		<xsl:variable name="seo-description" select="option[starts-with(./@name, 'description')]" />
		<div class="row">
			<div class="col-md-12" style="font-size: 18px; margin-bottom:15px;">
				<strong><xsl:value-of select="option[@name = 'domain']/value"/>
				</strong>
			</div>
			<div class="col-md-6">
				<div class="title-edit">
					<acronym>&option-seo-title;</acronym>
				</div>
				<span>
					<input class="default" type="text" name="{$seo-title/@name}" value="{$seo-title/value}" id="{$seo-title/@name}" />
				</span>
			</div>
			<div class="col-md-6">
				<div class="title-edit">
					<acronym>&option-seo-default-title;</acronym>
				</div>
				<span>
					<input class="default" type="text" name="{$seo-default-title/@name}" value="{$seo-default-title/value}" id="{$seo-default-title/@name}" />
				</span>
			</div>
			<div class="col-md-6">
				<div class="title-edit">
					<acronym>&option-seo-keywords;</acronym>
				</div>
				<span>
					<input class="default" type="text" name="{$seo-keywords/@name}" value="{$seo-keywords/value}" id="{$seo-keywords/@name}" />
				</span>
			</div>
			<div class="col-md-6">
				<div class="title-edit">
					<acronym>&option-seo-description;</acronym>
				</div>
				<span>
					<input class="default" type="text" name="{$seo-description/@name}" value="{$seo-description/value}" id="{$seo-description/@name}" />
				</span>
			</div>
		</div>
		<hr/>
	</xsl:template>


	<xsl:template match="/result[@method = 'yandex']/data[@type = 'settings' and @action = 'modify']">
        <div class="tabs-content module">
        <div class="section selected">
        <div class="location">
            <div class="save_size"></div>
            <a class="btn-action loc-right infoblock-show">
                <i class="small-ico i-info"></i>
                <xsl:text>&help;</xsl:text>
            </a>
        </div>

        <div class="layout">
        <div class="column">
		<form method="post" action="do/" enctype="multipart/form-data">
			<div class="panel-settings properties-group">
				<div class="round-toggle"></div>
				<div class="title">
					<h3><xsl:text>&header-seo-yandex;</xsl:text></h3>
				</div>
				<div class="content">
					<div class="row">
						<xsl:apply-templates select="group[@name = 'yandex']" mode="settings-modify" />
					</div>
					<table class="tableContent">
						<tbody>
							<tr>

							</tr>
						</tbody>
					</table>

				</div>
			</div>
			<div class="row">
				<xsl:call-template name="std-form-buttons-settings" />
			</div>
		</form>
        </div>
            <div class="column">
                <div  class="infoblock">
                    <h3>
                        <xsl:text>&label-quick-help;</xsl:text>
                    </h3>
                    <div class="content" title="{$context-manul-url}">
                    </div>
                    <div class="infoblock-hide"></div>
                </div>
            </div>
        </div>
        </div>
        </div>
		<xsl:apply-templates select="/result/@demo" mode="stopdoItInDemo" />
	</xsl:template>

	<xsl:template match="group[@name = 'yandex']/option[@name = 'code']" mode="settings-modify">
		<div class="col-md-6">
			<div class="title-edit">
				<xsl:value-of select="@label" />
			</div>
			<input class="default" type="text" name="{@name}" id="{@name}" />
			<a href="https://oauth.yandex.ru/authorize?response_type=code&#38;client_id=47fc30ca18e045cdb75f17c9779cfc36" target="_blank">Получить код</a>
		</div>
	</xsl:template>

	<xsl:template match="group[@name = 'yandex']/option[@name = 'token' and value]" mode="settings-modify">
		<div class="col-md-6">
			<div class="title-edit">
				<xsl:value-of select="@label" />
			</div>
			<input class="default" type="text" name="{@name}" value="{value}" id="{@name}" />
		</div>
	</xsl:template>

</xsl:stylesheet>
