(function($) {
	'use strict';

	$(function() {
		var $captcha = $('#captcha');

		/**
		 * Выводит настройки, актуальные для выбранной в данный момент капчи.
		 * Используется конвенция, что настройки для "Системной капчи" начинаются со слова "captcha",
		 * а настройки для Google reCaptcha начинаются со слова "recaptcha".
		 */
		var refreshOptions = function() {
			$('.panel-settings .row:has(input[name*="captcha"])').hide();
			var selected = $captcha.val();
			$('.panel-settings .row:has(input[name^="' + selected + '"])').show();
		};

		$captcha.on('change', function() {
			refreshOptions();
		});

		refreshOptions();
	});

}(jQuery));
