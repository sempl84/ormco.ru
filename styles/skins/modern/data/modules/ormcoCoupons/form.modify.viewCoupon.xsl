<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/ormcoCoupons">

<xsl:stylesheet
		version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:umi="http://www.umi-cms.ru/TR/umi"
		xmlns:php="http://php.net/xsl">

	<xsl:template match="/result[@method = 'viewCoupon']/data" priority="1">
        <div class="tabs-content module {$module}-module">
			<div class="section selected">
				<div class="row">
                    <div class="col-md-9">
                        <xsl:apply-templates select="object//group" mode="table-group" />
                    </div>
                </div>

				<div class="row">
					<div class="col-md-9">
						<table class="btable btable-striped bold-head" data-table-process="table">
							<thead>
								<th colspant="2">Действия</th>
							</thead>
							<tbody>
								<tr>
									<td style="width: 40%; vertical-align: middle">Отправить уведомление</td>
									<td style="width: 60%">
										<form method="POST" action="/admin/ormcoCoupons/couponSendNotification/{object/@id}/">
											<input type="text" name="email" placeholder="e-mail" />
											&nbsp;
											<input type="submit" class="btn color-blue" value="Отправить" />
										</form>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="field[@name = 'is_active']" mode="table-field" />

	<xsl:template match="field[@name = 'coupon_discount_focus_categories']" mode="table-field-value" priority="1">
		<xsl:apply-templates select="document(concat('udata://ormcoCoupons/getOrmcoCouponsCouponDiscountFocusCategoriesItems/', /result/data/object/@id, '/'))/udata/categories/category" mode="table-field-value-focus-categories-item" />
	</xsl:template>

	<xsl:template match="field[@name = 'coupon_log']" mode="table-field-value" priority="1">
		<xsl:value-of select="document(concat('udata://ormcoCoupons/renderOrmcoCouponsLog/', ancestor::data/object/@id, '/'))/udata" disable-output-escaping="yes" />
	</xsl:template>

	<xsl:template match="category" mode="table-field-value-focus-categories-item">
		<p>
			<xsl:choose>
				<xsl:when test="@link">
					<a href="{@link}"><xsl:value-of select="node()" /></a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="node()" />
				</xsl:otherwise>
			</xsl:choose>
		</p>
	</xsl:template>
</xsl:stylesheet>
