<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/ormcoCoupons">

<xsl:stylesheet
		version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:umi="http://www.umi-cms.ru/TR/umi"
		xmlns:php="http://php.net/xsl">

	<xsl:template match="/result[@method = 'viewHistory']/data" priority="1">
        <div class="tabs-content module {$module}-module">
			<div class="section selected">
				<div class="row">
                    <div class="col-md-6">
                        <xsl:apply-templates select="object//group" mode="table-group" />
                    </div>
                </div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
