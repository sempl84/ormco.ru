<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common/ormcoCoupons" []>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/result[@method = 'couponsList']//data" priority="1">
        <div class="tabs-content module">
            <div class="section selected">
                <div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
                    <div class="imgButtonWrapper" xmlns:umi="http://www.umi-cms.ru/TR/umi">
                        <a id="viewCoupon" class="btn color-blue loc-left" href="{$lang-prefix}/admin/{$module}/viewCoupon/{$param0}/">
                            <xsl:text>&label-viewCoupon;</xsl:text>
                        </a>
                    </div>
                    <a class="btn-action loc-right infoblock-show"><i class="small-ico i-info"></i><xsl:text>&help;</xsl:text></a>
                </div>

                <div class="layout">
                    <div class="column">
                        <xsl:call-template name="ui-smc-table">
                            <xsl:with-param name="control-params">couponsList</xsl:with-param>
                            <xsl:with-param name="content-type">objects</xsl:with-param>
                            <xsl:with-param name="show-toolbar">0</xsl:with-param>
                            <xsl:with-param name="enable-edit">0</xsl:with-param>
                            <xsl:with-param name="js-add-buttons">
                                createAddButton(
                                $('#viewCoupon')[0], oTable,
                                '<xsl:value-of select="$lang-prefix" />/admin/ormcoCoupons/viewCoupon/{id}/', ['*']
                                );
                            </xsl:with-param>
                        </xsl:call-template>
                    </div>
                    <div class="column">
                        <div class="infoblock">
                            <h3><xsl:text>&label-quick-help;</xsl:text></h3>
                            <div class="content" title="{$context-manul-url}"></div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>