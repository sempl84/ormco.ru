<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common/ormcoCoupons" []>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="form.modify.viewCoupon.xsl" />
    <xsl:include href="form.modify.viewHistory.xsl" />

    <xsl:template match="group" mode="table-group">
        <table class="btable btable-striped bold-head" data-table-process="table">
            <thead>
                <tr>
                    <th colspan="2"><xsl:value-of select="@title" /></th>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates select="field" mode="table-field" />
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="field" mode="table-field">
        <tr>
            <td style="width: 40%"><xsl:value-of select="@title" /></td>
            <td style="width: 60%"><xsl:apply-templates select="current()" mode="table-field-value" /></td>
        </tr>
    </xsl:template>

    <xsl:template match="field" mode="table-field-value">
        <xsl:value-of select="node()" />
    </xsl:template>

    <xsl:template match="field[@type = 'symlink']" mode="table-field-value">
        <xsl:for-each select="values/item">
            <xsl:if test="position() &gt; 1"><br /></xsl:if><a href="{@link}" target="_blank"><xsl:value-of select="name" /></a>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="field[@type = 'boolean']" mode="table-field-value">
        <xsl:choose>
            <xsl:when test="node() = '1'">да</xsl:when>
            <xsl:otherwise>нет</xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>