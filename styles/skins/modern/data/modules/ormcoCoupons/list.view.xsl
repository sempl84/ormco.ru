<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common/ormcoCoupons" []>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="list.view.coupons.xsl" />
    <xsl:include href="list.view.history.xsl" />
</xsl:stylesheet>