<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common/ormcoCoupons" []>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="settings.view.abandonedOrders.xsl" />

    <xsl:template match="/result[@method = 'sms']/data[@type = 'settings' and @action = 'view']">
        <div class="tabs-content module">
            <div class="section selected">
                <div class="location">
                    <div class="save_size"></div>
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>&help;</xsl:text>
                    </a>
                </div>

                <div class="layout">
                    <div class="column">
                        <form action="/admin/ormcoSiteData/sendSms/" method="POST">
                            <div class="panel-settings">
                                <div class="title"><h3>Отправить SMS</h3></div>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-4"><div class="title-edit">Телефон</div></div>
                                        <div class="col-md-6"><input type="text" class="default" name="phone" value="" /></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><div class="title-edit">Сообщение</div></div>
                                        <div class="col-md-6"><textarea name="message"></textarea></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">&nbsp;</div>
                                        <div class="col-md-4"><input type="submit">Отправить</input></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="{$context-manul-url}">
                            </div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>