<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/result[@method = 'syncContactsLogList']/data[@type = 'settings' and @action = 'view']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
                        <xsl:apply-templates select="current()" mode="syncContacts-log-list" />
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

    <xsl:template match="data" mode="syncContacts-log-list">
        <p>Данные не найдены</p>
    </xsl:template>

    <xsl:template match="data[items/item]" mode="syncContacts-log-list">
        <xsl:apply-templates select="items/item" mode="syncContacts-log-list-item" />
    </xsl:template>

    <xsl:template match="item" mode="syncContacts-log-list-item">
        <p><a href="{@link}"><xsl:value-of select="node()" /></a></p>
    </xsl:template>

    <xsl:template match="/result[@method = 'syncContactsLogView']/data[@type = 'settings' and @action = 'view']">
        <div class="tabs-content module">
            <div class="section selected">
                <div class="location">
                    <div class="save_size"></div>
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>&help;</xsl:text>
                    </a>
                </div>

                <div class="layout">
                    <div class="column">
                        <xsl:apply-templates select="current()" mode="syncContacts-log-view" />
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="{$context-manul-url}">
                            </div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="data" mode="syncContacts-log-view">
        <p>Данные не найдены</p>
    </xsl:template>

    <xsl:template match="data[log]" mode="syncContacts-log-view">
        <p>
            <xsl:choose>
                <xsl:when test="@reverse"><a href="/admin/ormcoUniSender/syncContactsLogView/{@id}/">В прямом порядке</a></xsl:when>
                <xsl:otherwise><a href="/admin/ormcoUniSender/syncContactsLogView/{@id}/?reverse=true">В обратном порядке</a></xsl:otherwise>
            </xsl:choose>
        </p>
        <p>&nbsp;</p>
        <p><xsl:value-of select="log" disable-output-escaping="yes" /></p>
    </xsl:template>

</xsl:stylesheet>