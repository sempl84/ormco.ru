<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="field[@name = 'user_coupons_system_field']" mode="form-modify">
        <div class="col-md-12">
            <xsl:apply-templates select="document(concat('udata://ormcoCoupons/getOrmcoCouponsUserActiveItems/', /result/data/object/@id, '/'))/udata" />
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'getOrmcoCouponsUserActiveItems']" />
    <xsl:template match="udata[@method = 'getOrmcoCouponsUserActiveItems'][items/item]">
		<table class="btable btable-striped bold-head" data-table-process="table">
			<thead>
				<tr>
					<th style="text-align: left;">Осталось дней</th>
					<th style="text-align: left;">Название семинара</th>
					<th style="text-align: left;">Купон</th>
				</tr>
			</thead>
			<tbody>
				<xsl:apply-templates select="items/item" />
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="udata[@method = 'getOrmcoCouponsUserActiveItems']/items/item">
		<tr>
			<td style="width: 20%">
				<xsl:value-of select="days-left/@string" />
			</td>
			<td style="width: 60%">
				<a href="/admin/ormcoCoupons/viewCoupon/{@id}/"><xsl:value-of select="name" /></a>
			</td>
			<td style="width: 20%">
				<xsl:value-of select="@code" />
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>