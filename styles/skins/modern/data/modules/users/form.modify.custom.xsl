<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="field[@name = 'logi_izmenenij_sistemnoe_pole']" mode="form-modify">
        <div class="col-md-12">
            <div class="users_change_log" data-object-id="{//object/@id}" />
            <script src="/styles/skins/modern/data/modules/users/users_change_log.js"></script>
            <style>
                .users_change_log table tr{border-bottom:solid 1px #aaa;}
                .users_change_log table tr:nth-last-child(1){border-bottom:none;}
                .users_change_log table td{text-align:center;border-right:solid 1px #aaa;padding:1px 10px;}
                .users_change_log table td:nth-last-child(1){border-right:none;text-align:left;}
            </style>
        </div>
    </xsl:template>

    <xsl:template match="field[@name = 'informaciya_po_skidke_sistemnoe_pole']" mode="form-modify">
		<xsl:variable name="discount_id" select="document(concat('udata://users/get_user_discount_id/', //object/@id))" />

		<xsl:choose>
			<xsl:when test="$discount_id = 'none'">
		        <div class="col-md-12">
					Скидка не задана
		        </div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="discount" select="document(concat('uobject://', $discount_id))" />
				<xsl:variable name="percent" select="document(concat('uobject://', $discount//property[@name = 'discount_modificator_id']/value/item/@id))" />
				<xsl:variable name="items" select="document(concat('uobject://', $discount//property[@name = 'discount_rules_id']/value/item/@id))" />

		        <div class="col-md-6">
					Процент скидки: <div><b><xsl:value-of select="$percent//property[@name = 'proc']/value" />%</b></div>
		        </div>
		        <div class="col-md-6">
					Исключая товары/разделы:
					<xsl:apply-templates select="$items//property[@name = 'catalog_items']/value/page" mode="excluded_item" />
		        </div>
		        <div class="col-md-6">
					<a href="/admin/emarket/discount_edit/{$discount_id}/" target="_blank" title="Перейти в скидку в новом окне">Перейти в скидку</a>
		        </div>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <xsl:template match="page" mode="excluded_item">
		<div>
			<a href="{@link}" target="_blank" title="Открыть страницу в новом окне"><xsl:value-of select="name" /></a>
		</div>
    </xsl:template>
</xsl:stylesheet>