/**
* editableCell
* Класс для создания редактируемых ячеек контрола Таблица
*/
var editableCell = function(_TableItem, _Cell, _PropInfo, _Button) {
	/**
	* (Private properties)
	*/
	var __self = this;
	var PropInfo = _PropInfo;
	var PropName = _PropInfo.fieldName || _PropInfo.name;
	var PropId = _PropInfo.id;
	var Item = _TableItem;
	var ObjectMode = (Item.control.contentType != "pages");
	var editControl = null;
	var OldValue = "";
	var OldCellContent = "";
	var DataType = _PropInfo['dataType'] || '';

	/* Public properties */
	this.isActive = false;
	this.element = _Cell;
	this.prepareSaveData = function(type) {
		if (editControl) {
			__self.save(editControl.val(), false, type);
		}
	};

	/**
	 * (Private methods)
	 */
	var __constructor = function() {
		if (jQuery.inArray(DataType, editableCell.ignoreDataTypes) != -1) return false;
		if (jQuery.inArray(PropName, editableCell.ignorePropNames) != -1) return false;

		if (editableCell.enableEdit == false){
			return false
		}

		if (editableCell.enableEdit) {
			$(_Button).on('click', function (e) {
				var hc = __self;
				var ac = editableCell.activeCell;
				if (hc) {
					// save last active cell
					if (ac) ac.prepareSaveData();
					hc.makeEditable();
				}
			});
		}
		__self.isActive = true;
	};

	this.makeEditable = function() {
		editableCell.activeCell = __self;
		editableCell.handleCell = false;
        
		__getData();
	};

	var __getData = function() {
		var sDataSrc = window.pre_lang + '/admin/content/get_editable_region/' + Item.id+"/" + PropName + '.xml';
		if (ObjectMode) sDataSrc += '?is_object=1';

		jQuery.ajax({
			type: "GET",
			url: sDataSrc,
			dataType: "xml",
			success: function(data, status){
				__onGetData(data);
			},
			error : function (rq, status, err) {
				__onGetDataError(err);
			}
		});
	};

	var __reportError = function(err) {
		editableCell.activeCell = false;
		editableCell.handleCell = false;
		jQuery.jGrowl(err, { header: getLabel('js-error-header') });
	};

	var __onGetData = function(oXMLResponse) {
		var arrEls = oXMLResponse.getElementsByTagName("property");

		if (typeof(arrEls[0]) !== 'undefined') {
			sType = arrEls[0].getAttribute('type') || "";
		}

		var errs = oXMLResponse.getElementsByTagName("error");

		if (errs.length) {
			__reportError(errs[0].firstChild.nodeValue);
			return false;
		}

		if (PropName == 'name') {
			sType = 'name';
		}

		var vals = oXMLResponse.getElementsByTagName('value');
		var val = false;

		switch (sType) {
			case 'wysiwyg' :
			case 'text' :
			case 'string':
			case 'int' :
			case 'price':
			case 'float':
			case 'tags':
			case 'link_to_object_type':
			case 'counter':
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}
				if(!val) {
					val = '';
				}
				__makeEditableStringFld(val, sType);
			break;
			case 'date':
				if (vals.length > 0) {
					val = vals[0].getAttribute('formatted-date');
				}
				if(!val) {
					val = '';
				}
				__makeEditableDateField(val);
				break;
			case 'boolean':
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}
				__makeEditablBooleanFld(val);
			break;
			case 'relation' :
				var vals = oXMLResponse.getElementsByTagName('item');
				__makeEditableRelationFld(vals, arrEls[0].getAttribute('multiple') == 'multiple', oXMLResponse);
			break;

			case 'name' :
				var vals = oXMLResponse.getElementsByTagName('name');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}
				__makeEditableStringFld(val);
			break;
			case 'color': {
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}
				if(!val) {
					val = '';
				}

				_makeEditableColorField(val);
				break;
			}
			case 'img_file' :
			case 'swf_file' :
			case 'video_file' :
			case 'file' : {
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}

				makeEditableFileField(val, sType);
				break;
			}
			default:
				__reportError(getLabel("js-edcell-unsupported-type"));
				return false;
			break;
		}
        $(window).on('click',__self.outSideClick);

	};
    
    this.outSideClick = function (e) {
        if ($(__self.element).prop('id')!=$(e.target).closest('td').prop('id')) {
            __self.restore();
        }   
    };

	var __onSetData = function(oXMLResponse, sResponseTxt) {
		var el = jQuery('div', __self.element);

		var errs = oXMLResponse.getElementsByTagName("error");
		if (errs.length) {
			__reportError(errs[0].firstChild.nodeValue);
			__self.restore();
			return false;
		}

		var arrEls = oXMLResponse.getElementsByTagName("property");
		if (typeof(arrEls[0]) !== 'undefined') {
			sType = arrEls[0].getAttribute('type') || "";
		}

		if (PropName == 'name') {
			sType = 'name';
		}

		var vals = oXMLResponse.getElementsByTagName('value');
		var val = '';

		switch (sType) {
			case 'wysiwyg' :
			case 'text' :
			case 'int' :
			case 'price':
			case 'float':
			case 'tags':
			case 'counter':
			case 'link_to_object_type':
				var vals = oXMLResponse.getElementsByTagName('value');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}
				el.html(val);
				OldValue = val;
			break;
			case 'date':
				var vals = oXMLResponse.getElementsByTagName('value');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].getAttribute('formatted-date') : '';
				}
				el.html(val);
				OldValue = val;
			break;
			case 'string':
				var property = oXMLResponse.getElementsByTagName('property')[0].getAttribute('restriction');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
					if (property == 'email') {
						val = "<a href='mailto:" + val + "' title='" + val + "' class='link'>" + val + "</a>";
					}
				}
				el.html(val);
				OldValue = val;
			break;
			case 'boolean':
				if (vals.length) {
					val = vals[0].firstChild ? parseInt(vals[0].firstChild.nodeValue) : 0;
				}
				
				el.html(val ? '<img alt="" style="width:13px;height:13px;" src="/images/cms/admin/mac/tree/checked.png" />' : '');
				OldValue = val;
			break;
			case 'relation' :
				var vals = oXMLResponse.getElementsByTagName('item');
				if (vals.length == 1) {
					var name = vals[0].getAttribute('name');
					var guid = 'relation-value';
					if (vals[0].getAttribute('guid') != undefined) guid = vals[0].getAttribute('guid');				
					val = '<span title="' + name + '" class="c-' + guid + '">' + name + '</span>';
				}
				
				if (vals.length > 1) {
					for (var i = 0; i < vals.length; i++) {
						val += vals[i].getAttribute('name');
						if (i < vals.length - 1) val += ', ';
					}
				}
				el.html(val);
				OldValue = val;
			break;
			case 'name' :
				var vals = oXMLResponse.getElementsByTagName('name');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}

				el.html(OldCellContent);
				el.find('.name_col').text(val);

				var checkbox = jQuery('div.checkbox', el).get(0);
				Item.checkBox = checkbox;

				var anchor = jQuery('a', __self.element);
				anchor.html(val);
				OldValue = val;
			break;
			case 'color': {
				var vals = oXMLResponse.getElementsByTagName('value');
				if (vals.length) {
					val = vals[0].firstChild ? vals[0].firstChild.nodeValue : '';
				}

				var newContent = '<div class="color table"><span class="value">' + val +
					'</span><span class="color-box"><i style="background-color: ' + val +
					'"></i></span></div>';

				if (OldValue.length !== 0) {
					el.html(OldCellContent);
				} else {
					el.html(newContent);
				}

				var $content = jQuery('.value', el);
				var colorBox = jQuery('.color-box i', el);

				$content.html(val);
				colorBox.css('background-color', val);

				OldValue = val;
				break;
			}
			case "video_file":
			case "swf_file":
			case "file":
			case "img_file": {
				if (vals.length) {
					var $image = $(vals[0]);
					var filePath = $image.attr('path');
					var filePathWithoutExt = filePath.substring(0, filePath.lastIndexOf('.'));
					var normalisedFilePath = filePath.substring(1);
					var ext = $image.attr('ext');
					var fileName = $image.attr('name') + '.' + ext;
					var isBroken = $image.attr('is_broken');

					var templateId = (sType == 'img_file') ? 'fast-edit-image-preview' : 'fast-edit-file-preview';
					var template = _.template($('#' + templateId).html());
					val =  template({
						filePath: normalisedFilePath,
						filePathWithoutExt: filePathWithoutExt,
						fileName: fileName,
						ext: ext,
						isBroken: isBroken
					});
				}

				el.html(val);
				OldValue = val;
				break;
			}
			default: 
				__reportError(getLabel("js-edcell-unsupported-type"));
				return false;
			break;
		}

		if(window['onAfterSetProperty']) onAfterSetProperty(Item.id, PropName, val);

		jQuery.jGrowl(getLabel('js-property-saved-success'));
		OldCellContent = jQuery('div', __self.element).html();
		Control.recalcItemsPosition();
	};

	var __makeEditableStringFld = function(sVal, type) {
		OldValue = sVal;
		var el = jQuery('div',__self.element);
		OldCellContent = jQuery('div',__self.element).html();

		editControl = document.createElement('input');
		editControl.setAttribute('type', 'text');
		editControl.value = sVal;
		editControl.className = 'editableCtrl default';

		editControl = $(editControl);

		editControl.on('blur',function() {
			if(editableCell.activeCell) {
				__self.prepareSaveData(type);
				return true;
			}
		});

		editControl.on('keyup',function(evnt) {
			var iKCode = window.event ? window.event.keyCode : evnt.which;
			if (iKCode == 27) __self.restore();
			if (iKCode == 13 && editableCell.activeCell) __self.prepareSaveData();
			return true;
		});

		el.html('');
		editControl.appendTo(el);
		editControl.focus();
	};

	var __makeEditableDateField = function(value) {
		OldValue = value;
		var el = jQuery('div',__self.element);
		OldCellContent = jQuery('div',__self.element).html();

		editControl = document.createElement('input');
		editControl.setAttribute('type', 'text');
		editControl.value = value;
		editControl.className = 'default';

		editControl = $(editControl);

		editControl.on('blur',function(event) {
			var relatedTarget = event.relatedTarget || event.toElement;

			if (!relatedTarget) {
				return;
			}

			var isDatePickerClicked = $(event.relatedTarget).closest('table').hasClass('ui-datepicker-calendar');

			if (editableCell.activeCell && !isDatePickerClicked) {
				__self.prepareSaveData();
				return true;
			}
		});

		editControl.on('keyup',function(evnt) {
			var iKCode = window.event ? window.event.keyCode : evnt.which;
			if (iKCode == 27) __self.restore();
			if (iKCode == 13 && editableCell.activeCell) __self.prepareSaveData();
			return true;
		});

		el.html('');
		editControl.appendTo(el);
		editControl.focus();

		editControl.datepicker({
			dateFormat : 'yy-mm-dd',
			onClose: function(dateText) {
				if(!/\d{1,2}:\d{1,2}(:\d{1,2})?$/.exec(dateText)) {
					dateText += " 00:00:00";
				}

				editControl.val(dateText);

				if(editableCell.activeCell) {
					__self.prepareSaveData();
					return true;
				}
			}
		});

		editControl.datepicker('show');
	};

	/**
	 * Инициализирует форму редактирования в табличном контроле для полей типов:
	 * "Файл", "Изображение", "Видео" и "swf"
	 * @param {String} fieldValue значения поля
	 * @param {String} fieldType тип поля
	 */
	var makeEditableFileField = function(fieldValue, fieldType) {
		OldValue = fieldValue;
		var $div = jQuery('div', __self.element);
		OldCellContent = $div.html();
		$div.html('');

		var template = _.template($('#fast-edit-file-control').html());
		var editControlHtml = template({
			id: __self.element.id + '_input_id',
			value: fieldValue
		});

		var container = jQuery(editControlHtml).appendTo($div);
		editControl = jQuery('input', container);

		editControl.on('keyup',function(event) {
			var iKCode = window.event ? window.event.keyCode : event.which;

			if (iKCode == 27) {
				__self.restore();
			}

			if (iKCode == 13) {
				__self.prepareSaveData(fieldType);
			}

			return true;
		});

		editControl.on('change',function(event) {
			editControl.focus();
			return true;
		});

		$('a.icon-action', container).on('click',function (e) {
			var fileBrowserParam = '';

			switch (fieldType) {
				case 'img_file' : {
					fileBrowserParam = 'image=1';
					break;
				}
				case 'swf_file' :
				case 'video_file' : {
					fileBrowserParam = 'video=1';
					break;
				}
			}

			showFileBrowser(editControl, fileBrowserParam);
		});

		editControl.focus();
	};

	/**
	 * Открывает файловый менеджер
	 * @param {Object} $input Jquery объект инпута, в который необходимо вставить выбранный файл в файловом менеджере
	 * @param {String} customParam кастомный параметр инициализации файлового менеджера
	 */
	var showFileBrowser = function($input, customParam) {
		var inputId = $input.attr('id');
		var filePath  = $input.val();
		var matchResult = filePath.match(/(.*\/)/);
		var folderPath = (matchResult) ? matchResult[0] : '';
		var queryString = [];

		queryString.push('id=' + inputId);
		queryString.push('file=' + filePath);
		queryString.push('folder=' + folderPath);
		queryString.push('lang=' + window.lang_id);

		if (customParam) {
			queryString.push(customParam);
		}

		$.openPopupLayer({
			name   : "Filemanager",
			title  : getLabel('js-file-manager'),
			width  : 660,
			height : 530,
			url    : "/styles/common/other/elfinder/umifilebrowser.html?" + queryString.join("&")
		});

		var $fileBrowser = jQuery('div#popupLayer_Filemanager div.popupBody');

		var template = _.template($('#file-browser-options').html());
		var options = template({
			watermarkMessage: getLabel('js-water-mark'),
			rememberMessage: getLabel('js-remember-last-dir'),
			remember: getCookie('remember_last_folder', true)
		});

		$fileBrowser.append(options);
	};

	var _makeEditableColorField = function(value) {
		OldValue = value;
		var $div = jQuery('div', __self.element);
		OldCellContent = $div.html();
		$div.html('');

		var fieldHTML = '<div class="color table"><input type="text" class="default"></div>';
		var container = jQuery(fieldHTML).appendTo($div);
		var $input = jQuery('input', container);

		if (value) {
			$input.val(value);
		}

		$input.bind('blur', function(event) {
			if(editableCell.activeCell) {
				__self.prepareSaveData();
				return true;
			}
		});

		new colorControl(container, {});

		jQuery(container).bind('hidePicker', function() {
			if(editableCell.activeCell) {
				__self.save($input.val());
			}
		});
	};

	var __makeEditableRelationFld = function(vals, multiple) {
		$(__self.element).addClass('hide-editable');
		var overflowWrapper = $(__self.element).closest('.overflow-block');
		overflowWrapper.css('overflow','visible');
		var el = jQuery('div', __self.element);
		el.removeClass('cell-item');
		el.css('overflow','visible');
		OldCellContent = el.html();

		var multipleValue = (multiple ? 'multiple' : '');
		var selectHtml = '<div class="layout-col-control quick">' +
				'<select id="" class="default" ' + multipleValue +' autocomplete="off"></select>' +
			'</div>';

		el.html('');
		var selectContainer = jQuery(selectHtml).appendTo(el);

		editControl = jQuery('select', selectContainer);

		if (multiple) {
			var addButtonHtml = '<div class="layout-col-icon">' +
				'<a id="" class="icon-action relation-add">' +
				'<i class="small-ico i-add"></i>' +
				'</a></div>';
			el.append(addButtonHtml);
		}

		var control = new ControlRelation({
			container: el,
			type: PropInfo.guideId,
			id: Item.id,
			empty: true,
            preload:false
		});

		control.loadItemsAll(selectItems);
		var selectizeObject = control.selectize;

		function selectItems() {
			if (vals.length > 0) {
				selectizeObject.lock();

				(function() {
					for (var i in vals) {
						if (!vals.hasOwnProperty(i)) {
							continue;
						}
						var item = vals[i];
						selectizeObject.addItem(item.getAttribute('id'), true);
					}
				})();

				selectizeObject.unlock();
			}
		}

        selectizeObject.on('change', function() {
                __self.prepareSaveData();
        });

		el.on('keyup', function (e) {
			var iKCode = window.event ? window.event.keyCode : e.which;
			if (iKCode == 27) __self.restore();
			if (iKCode == 13 && editableCell.activeCell) __self.prepareSaveData();
			return true;
		});

		__self.prepareSaveData = function() {
			$(__self.element).removeClass('hide-editable');
			overflowWrapper.css('overflow','hidden');
			if (!selectizeObject) {
				return;
			}

			var result = [];
			for (var i = 0; i < selectizeObject.items.length; i++) {
				var itemId = selectizeObject.items[i];
				result.push(itemId);
			}

			__self.save(result);
		};
	};

	var __makeEditablBooleanFld = function(val) {
		var el = jQuery('div', __self.element);
		OldCellContent = el.html();

		el.html('');
		var checkedValue = (val ? 'checked' : '');
		var checkBoxHtml = '<div class="checkbox ' + checkedValue + '"><input type="checkbox editableCtrl"></div>';
		editControl = jQuery(checkBoxHtml).appendTo(el);

		editControl.click(function () {
			$(this).toggleClass('checked');
		});

		editControl.bind('click', function() {
			var v = $(this).hasClass('checked') ? 1 : 0;
			__self.save(v, true);
			
			if(   __self.element.name == 'is_activated' || __self.element.name == 'is_active' || __self.element.name == 'active' ) {
				Item.update ( {'is-active':this.checked ? 1 : 0, 'id': Item.id} );
		    }   
			
			return true;
		});

		var checkBox = editControl.children('input').eq(0);
		checkBox.focus();

		checkBox.bind('keydown', function(event) {
			if (event.keyCode == 27) {
				__self.restore();
			}

			if (event.keyCode == 13)  {
				__self.prepareSaveData();
			}
			return true;
		});

		editControl.focus();
	};

	var __onGetDataError = function(oException) {
		__reportError(getLabel('js-edcell-get-error') + oException);
	};

	var __setData = function(content) {
		var sDataSrc = window.pre_lang + '/admin/content/save_editable_region/' + Item.id + "/" + PropName + '.xml';
		if (ObjectMode) sDataSrc += '?is_object=1';

		jQuery.ajax({
			type: "POST",
			url: sDataSrc,
			dataType: "xml",
			success: function(data, status){
				__onSetData(data, status);
			},
			data : ({
				'data[]' : content,
				'csrf'   : csrfProtection.getToken()
			}),
			error : function (rq, status, err) {
				__onSetDataError(err);
			},
			complete: function (rq, status) {
				if (editableCell.activeCell == __self) editableCell.activeCell = false;
				if (editableCell.handleCell == __self) editableCell.handleCell = false;
			}
		});
	};

	var __onSetDataError = function(oException) {
		__reportError(getLabel('js-edcell-save-error') + oException);
	};

	/**
	 * Проверяет введенное значение для поля элемента
	 * @param {String|Object|Number} value новое значение для поля
	 * @param {String} type тип поля
	 * @returns {boolean} результат проверки
	 */
	var validateValue = function(value, type) {
		var numberTypes = ['int', 'price', 'float', 'counter'];

		if (numberTypes.indexOf(type) !== -1) {
			return validateNumberValue(value);
		}

		return true;
	};

	/**
	 * Проверяет введенное значение на соответствие числу
	 * @param {String|Object|Number} value проверяемое значение
	 * @returns {boolean} результат проверки
	 */
	var validateNumberValue = function(value) {
		if (value == '') {
			return true;
		}

		var isNumber = !isNaN(parseFloat(value)) && value.match(/^[0-9eE.,]+$/);

		if (!isNumber) {
			jQuery.jGrowl(getLabel('js-error-validate-number'), { header: getLabel('js-error-header') });
			return false;
		}

		return true;
	};

	/**
	 * Сохраняет значение в поле элемента
	 * @param {String|Object|Number} newValue сохраняемое значение
	 * @param {Boolean} force сохранить при любых условиях
	 * @param {String} type тип поля
	 * @returns {boolean}
	 */
	this.save = function(newValue, force, type) {
		editableCell.activeCell = false;
		if (_.isArray(newValue)){
			if (newValue.length==0) {
				this.restore();
				return false;
			}
		} else {
			if (!force && (OldValue == newValue || !validateValue(newValue, type))) {
				this.restore();
				return false;
			}
		}

		__setData(newValue);
		return true;
	};

	this.restore = function() {
        $(window).off('click',__self.outSideClick);
		$(__self.element).removeClass('hide-editable');
		$(__self.element).closest('.overflow-block').css('overflow','hidden');
		editableCell.activeCell = false;
		var div = jQuery('div', __self.element);
		div.html(OldCellContent);
		Control.recalcItemsPosition();
	};

	this.getEditControl = function() {
		return editControl;
	};

	__constructor();
};

editableCell.activeCell = false;
editableCell.handleCell = false;
editableCell.helper = false;
editableCell.hideHelper = false;

editableCell.ignoreDataTypes = ['wysiwyg'];
editableCell.ignorePropNames = [];