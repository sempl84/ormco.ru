/**
 * TableItem
 * Класс осуществяляет визуализацию строки таблицы
 * @param {Object} _oControl - экземпляр класса Control
 * @param {Object} _oParent - экземпляр класса TableItem, указывающий на прямого предка элемента
 * @param {Object} _oData - информация о элементе
 * @param {Object} _oSiblingItem - экземпляр класса TableItem, указывающий на соседа элемента
 * @param {String} _sInsertMode - режим добавление элемента по отношению к соседу _oSiblingItem. Может быть "after" и "before"
 */
var TableItem = function(_oControl, _oParent, _oData, _oSiblingItem, _sInsertMode) {
	var id = parseInt(_oData.id);
	var __self = this;
	var Data = _oData; 									// Источник данных
	var Parent = _oParent; 								// Родитель куда вставляем
	var SiblingItem = _oSiblingItem || null; 			// Дочений элемент для вставок

	var InsertMode = _sInsertMode || 'after'; 			// Режим вствки
	var ForceDraw = typeof(_oData['force-draw']) !== 'undefined' ? parseInt(_oData['force-draw']) : 1;
	var CountChilds = typeof(_oData['childs']) !== 'undefined' ? parseInt(_oData['childs']) : 0;
	var baseModule = (typeof(_oData['basetype']) !== 'undefined' && typeof(_oData['basetype']['module']) === 'string') ? _oData['basetype']['module'] : "content";
	var baseMethod = (typeof(_oData['basetype']) !== 'undefined' && typeof(_oData['basetype']['method']) === 'string') ? _oData['basetype']['method'] : "";
	var typeName = (typeof(_oData['basetype']) !== 'undefined' && typeof(_oData['basetype']['_value']) === 'string') ? _oData['basetype']['_value'] : "";
	var IconsPath = _oControl.iconsPath;
	var iconSrc = typeof(_oData['iconbase']) !== 'undefined' ? _oData['iconbase'] : IconsPath + 'ico_' + baseModule + '_' + baseMethod + '.png';
	var toggleCtrl = null; 								// Контрол сворачивания/разворачивания
	var labelCtrl = null; 								// Контрол поля заголовка
	var labelText = null; 								// Заголовок текст
	var itemIcon = null;								// Иконка элемента заголовка
	var dropIndicator = null;							// Индикатор сброка драгидрока (вроде еще не тестил это дело)
	var Settings = SettingsStore.getInstance();			// Какие-то настройки
	var AutoexpandAllowed = true;						// Вроде как разрешение на авто разворачивание элементов дерева
	var oldClassName = null;						// старый класс для селекта строки
	var selected = false;						// атрибут селекта строки
	var flatMode = _oControl.flatMode;					// отключение режимма дерева
	var objectTypesMode = _oControl.objectTypesMode; 	// ХЗ
	var pagesBar = null;								// Какой-то неведомый погрессбар. Пока хз зачем он.
	var filterRow = null;								// Строка с фильтрами
	var activeColumn = null;							// Пока неведомая колонка но вроде как с контролами строки
	/** @var {Boolean} Нужно ли выводить чекбоксы для элементов */
	var hasCheckbox = typeof _oControl.hasCheckboxes == 'boolean' ? _oControl.hasCheckboxes : true;
	/**
	 * (Public properties)
	 */
	this.checkBox = null;
	this.element = null; 								// Строка таблицы которую мы сейчас креайтим
	this.control = _oControl;							// Ссылка на общий объект контроллер Control
	this.childsContainer = null;						// Контейнер для дочернего элемента дерева
	this.id = id;										// Поле ид объекта данных
	this.name = typeof(_oData['name']) !== 'undefined' ? _oData['name'] : getLabel('js-smc-noname-page'); // поле name объекта данных
	this.isRoot = Parent ? false : true;					// Индикатор корневой объект или нет
	this.loaded = false;								// Атрибут загруженности данных
	this.viewLink = typeof(_oData['link']) !== 'undefined' ? _oData['link'] : false; // Ссылка на просмотр страницы|объекта
	this.editLink = typeof(_oData['edit-link']) !== 'undefined' ? _oData['edit-link'] : false; // Ссылка на форму редактирования страницы|объекта
	this.createLink = typeof(_oData['create-link']) !== 'undefined' ? _oData['create-link'] : false; // Ссылка на форму создания страницы|объекта
	this.permissions = typeof(_oData['permissions']) !== 'undefined' ? parseInt(_oData['permissions']) : 0;
	this.isActive = typeof(_oData['is-active']) !== 'undefined' ? parseInt(_oData['is-active']) : 0;
	if (objectTypesMode) {
		this.isActive = true;
	}
	this.isVirtualCopy = typeof(_oData['has-virtual-copy']) !== 'undefined';                //Атрибут вирутуальной копии
	this.isOriginal = typeof(_oData['is-original']) !== 'undefined';
	this.lockedBy = typeof(_oData['locked-by']) === 'object' ? _oData['locked-by'] : null;		    // Атрибут блокировки
	this.expiration = typeof(_oData['expiration']) === 'object' ? _oData['expiration'] : null; 	    // ХЗ какойто срок протухания судя по названию

	this.templateId = typeof(_oData['template-id']) !== 'undefined' ? _oData['template-id'] : null; // ИД шаблона
	this.langId = typeof(_oData['lang-id']) !== 'undefined' ? _oData['lang-id'] : null;				// ИД языка
	this.domainId = typeof(_oData['domain-id']) !== 'undefined' ? _oData['domain-id'] : null;		// Ид домена

	this.allowCopy = typeof(_oData['allow-copy']) !== 'undefined' ? parseInt(_oData['allow-copy']) : true; // Поддержка копирования
	this.allowActivity = typeof(_oData['allow-activity']) !== 'undefined' ? parseInt(_oData['allow-activity']) : true; // Поддержка управления активностью
	this.allowDrag = typeof(_oData['allow-drag']) !== 'undefined' ? _oData['allow-drag'] : true; // Поддержка перетаскивания

	this.parent = Parent;                               // Ссылка на родителя из спрятанных свойств
	this.childs = [];                                   // Дети
	this.hasChilds = (CountChilds > 0 || id == 0);      // Атрибут наличия детей
	this.labelControl = null;                           // Дубль контрола с заголовком строки
	this.position = false;                              // Позиция
	this.isExpanded = false;                            // атрибут рахвернут или нет
	this.filter = new filter;                           // Фильтр
	this.filter.setParentElements(id);                  // Родитель для фильтра
	this.level = Parent ? Parent.level + 1 : 0;         // Уровень вложенности
	this.ignoreEmptyFilter = false;                     // сосбтвенно сабж
	this.pageLimits = this.control.PerPageLimits;       // Количество записей на страницу
	this.nextSibling = typeof _oData['nextSibling'] == 'object' ? _oData['nextSibling'] : null;
	this.row = null;
	/**
	 * @type {Array} editableFieldTypeList список типо полей, которые поддерживают быстрое редактирование
	 */
	this.editableFieldTypeList = new Array(
		'text',
		'string',
		'int',
		'price',
		'float',
		'tags',
		'link_to_object_type',
		'counter',
		'date',
		'string',
		'boolean',
		'relation',
		'name',
		'color',
		'video_file',
		'swf_file',
		'file',
		'img_file'
	);

	// Данные пагинации
	this.pageing = {
		'total': 0,
		'limit': 0,
		'offset': 0
	};

	this.baseModule = (typeof(_oData['basetype']) !== 'undefined' && typeof(_oData['basetype']['module']) === 'string') ? _oData['basetype']['module'] : "content";
	this.baseMethod = (typeof(_oData['basetype']) !== 'undefined' && typeof(_oData['basetype']['method']) === 'string') ? _oData['basetype']['method'] : "";

	/**
	 * (Private methods)
	 */
	/**
	 * Конструктор класса
	 * @access private
	 */
	var __constructor = function() {
		if (_oParent) {
			_oParent.childs.push(id);
			if (ForceDraw) {
				__draw(Parent.childsContainer);
			}
		} else {
			if (ForceDraw) {
				__drawRoot();
			}
		}
	};

	var isObjectActivated = function(item, defaultActivity) {
		if (flatMode && __self.control.contentType != 'pages') {
			if (__self.control.enableObjectsActivity) {
				return (defaultActivity
						|| item.getValue('is_activated')
						|| item.getValue('is_active')
						|| item.getValue('activated')) !== "";
			} else {
				return true;
			}
		} else {
			return defaultActivity;
		}
	};

	/**
	 * Добавляет колонку в контейнер
	 * @param {HTMLElement} container элемент контейнера
	 * @param {String} size размер добавляемой колонки
	 * @private
	 */
	var __appendCol = function(container, size) {
		var column = document.createElement('col');
		column.style.width = size;
		$(container).append(column);
	};

	/**
	 * Обновляет данные элемента colgroup таблицы
	 * @param {HTMLElement} columnGroups контейнеры обновляемых данных
	 * @private
	 */
	var __updateColGroup = function(columnGroups) {
		var $columnGroups = $(columnGroups);
		var usedColumns = __getUsedColumns();
		var columnsTable = __getColumnsTable();

		$columnGroups.html('');

		var nameColumnSize = parseInt(usedColumns['name']['params'][0]);
		__appendCol($columnGroups, nameColumnSize + 'px');
		var columnSize;

		for (var name in usedColumns) {
			if (!columnsTable.hasOwnProperty(name)) {
				continue;
			}

			columnSize = usedColumns[name]['params'][0];
			__appendCol($columnGroups, columnSize);
		}

		__appendCol($columnGroups, '100%');
	};

	/**
	 * "Рисует" элемент
	 * @access private
	 * @param {DOMElement} _oContainerEl контейнер для добавления элемента
	 */
	var __draw = function(_oContainerEl) {
		var that = this;
		__self.isActive = isObjectActivated(__self, __self.isActive);
		var usedColumns = __getUsedColumns();
		var columnsMenu = __getColumnsMenu();
		var columnsTable = __getColumnsTable();

		var element = document.createElement('tr');
		element.setAttribute('rel', id);
		element.className = 'table-row tollbar level-' + __self.level;
		element.rel = id;
		__self.row = element;

		labelCtrl = element;
		__self.labelControl = labelCtrl;

		var nameData = usedColumns['name'],
				labelCell = document.createElement('td');
		labelCell.classList.add('table-cell');
		labelCell.id = 'c_' + __self.control.id + '_' + id + '_name';

		var size = parseInt(nameData['params'][0]);

		labelCell.width = size;
		labelCell.name = 'name';

		if (editableCell.enableEdit !== false) {
			var edit = document.createElement('i');
			edit.className = 'small-ico i-change editable';
			edit.title = getLabel('js-table-control-fast-edit');
			edit.id = 'e_' + __self.control.id + '_' + __self.id + '_name';
			labelCell.appendChild(edit);
		}

		var labelWrapper = document.createElement('div');

		labelWrapper.style.owerflow = 'hidden';

		labelCell.appendChild(labelWrapper);

		var cell = null;
		if (!objectTypesMode) {
			cell = new editableCell(__self, labelCell, usedColumns['name'], edit);
		}

		if (cell instanceof editableCell && !cell.isActive) {
			$(edit).detach();
		}

		toggleCtrl = document.createElement('span');
		toggleCtrl.className = 'catalog-toggle';

		var toggleWrapper = document.createElement('span');
		toggleWrapper.classList.add('catalog-toggle-wrapper');

		toggleWrapper.onclick = function() {
			__self.toggle(this);
			return false;
		};

		if (__self.hasChilds) {
			toggleWrapper.appendChild(toggleCtrl);
		} else {
			toggleWrapper.className = 'catalog-toggle-off';
		}
		if (!__self.control.flatMode) {

			labelWrapper.appendChild(toggleWrapper);
		}

		var checkWrapper = document.createElement('div');
		checkWrapper.classList.add('checkbox');

		var checkControl = document.createElement('input');
		checkControl.setAttribute('type', 'checkbox');
		checkControl.value = id;
		checkControl.classList.add('row_selector');
		checkWrapper.appendChild(checkControl);

		var $element = jQuery(element);
		$element.bind('mousedown', function(event) {
			__self.control.handleMouseDown(event, id);
		});

		$element.bind('mouseup', function(event) {
			__self.control.handleMouseUp(event, __self);
		});

		if (hasCheckbox) {
			__self.checkBox = checkWrapper;
			labelWrapper.appendChild(checkWrapper);
		}

		itemIcon = document.createElement('img');
		itemIcon.style.border = '0px';
		itemIcon.setAttribute('alt', typeName);
		itemIcon.setAttribute('title', typeName);
		itemIcon.setAttribute('src', iconSrc);
		itemIcon.className = 'ti-icon';
		itemIcon.onmousedown = function() {
			return false;
		};

		if (__self.control.allowDrag && __self.allowDrag) {
			itemIcon.style.cursor = 'move';
		}

		if (!__self.control.flatMode && !__self.control.objectTypesMode) {
			labelWrapper.appendChild(itemIcon);
		} else {
			itemIcon = null;
		}

		if (__self.expiration) {
			var oStatus = __self.expiration['status'];
			if (oStatus) {
				var statusSID = oStatus['id'];
				var statusName = oStatus['_value'];
				if (statusSID) {
					var expInd = document.createElement('img');
					var ico = IconsPath + 'ico_' + statusSID + '.png';
					expInd.setAttribute("src", ico);
					expInd.setAttribute("alt", statusName);
					expInd.setAttribute("title", statusName);
					expInd.className = 'page-status';
					labelWrapper.appendChild(expInd);
				}
			}
		}

		labelText = document.createElement('a');
		var t = __self.getValue('name');
		var val = String((typeof(t) == 'string' && t.length) ? t : "");

		if (!val.length) {
			val = getLabel('js-smc-noname-page');
		}

		labelWrapper.title = (__self.viewLink) ? __self.viewLink : val.replace(/<[^>]+>/g, '');
		labelText.className = 'name_col';
		labelText.href = __self.editLink;
		labelText.innerHTML = val;

		if (!__self.editLink) {
			labelText.className = 'name_col unactive';
		}

		$(labelText).bind('mousedown click mouseup', function(event) {
			var middleMouseButton = 1;

			if (event.button !== middleMouseButton) {
				event.preventDefault();
			}

			return true;
		});

		labelWrapper.appendChild(labelText);

		var editControl = document.createElement('a');
		editControl.classList.add('small-ico');
		editControl.classList.add('i-edit');
		editControl.classList.add('stucktotext');
		editControl.classList.add('editable');
		editControl.setAttribute('href', __self.editLink);
		editControl.setAttribute('title', getLabel('js-goto-edit-page'));
		labelWrapper.appendChild(editControl);

		if (__self.isVirtualCopy) {
			var virtualLabel = document.createElement('span');
			virtualLabel.className = 'label-virtual';
			virtualLabel.innerHTML = (__self.isOriginal) ? getLabel('js-smc-original') : getLabel('js-smc-virtual-copy');
			labelWrapper.appendChild(virtualLabel);
		}

		if (!__self.isActive && !__self.control.objectTypesMode) {
			element.classList.add('disabled');
		}

		element.appendChild(labelCell);

		var colSpan = 2;

		for (var name in usedColumns) {
			var column = columnsTable[name];
			var params = usedColumns[name]['params'];

			if (params[1] == 'static') {
				column = usedColumns[name];
			}
			if (!column) {
				continue;
			}

			colSpan++;

			var col = document.createElement('td');
			col.id = 'c_' + __self.control.id + '_' + id + '_' + name;
			col.className = 'table-cell';
			col.style.width = params[0];
			col.style.maxWidth = params[0];
			col.name = name;

			val = __self.getValue(name);

			var t = document.createElement('div');
			t.style.cursor = 'text';
			t.style.width = '100%';
			t.classList.add('cell-item');
			t.innerHTML = val;

			cell = null;

			if (__self.editableFieldTypeList.indexOf(column.dataType) != -1 && editableCell.enableEdit !== false) {
				var edit_col = document.createElement('i');
				edit_col.className = 'small-ico i-change editable';
				edit_col.title = getLabel('js-table-control-fast-edit');
				edit_col.id = 'e_' + __self.control.id + '_' + __self.id + '_' + name;
				col.appendChild(edit_col);
				cell = new editableCell(__self, col, column, edit_col);
			}

			col.appendChild(t);

			if (cell instanceof editableCell && !cell.isActive) {
				$(edit_col).detach();
			}

			if (name == 'is_activated' || name == 'active' || name == 'is_active') {
				activeColumn = col;
			}

			element.appendChild(col);
		}

		var autoCol = document.createElement('td');
		autoCol.className = 'table-cell';
		autoCol.style.width = '100%';
		element.appendChild(autoCol);

		if (SiblingItem) {
			var prevEl = null;
			if (InsertMode.toLowerCase() == 'after') {
				prevEl = SiblingItem.element.nextSibling;
			} else {
				prevEl = SiblingItem.element;
			}

			if (prevEl) {
				__self.element = _oContainerEl.insertBefore(element, prevEl);
			} else {
				__self.element = _oContainerEl.appendChild(element);
			}
		} else {
			__self.element = _oContainerEl.appendChild(element);
		}

		if (__self.control.dragAllowed && __self.allowDrag) {
			var DropMode = 'child';
			var timeOutHandler;
			var mouseFlag = false;

			jQuery(labelCtrl).draggable({
				appendTo: 'body',
				distance: Control.DragSensitivity,
				handle: '.ti-icon, a',
				cursorAt: {right: -2},
				helper: function() {
					if (__self.control.contentType == 'objects') {
						var drag_el = document.createElement('div');

						drag_el.innerHTML = '<div>' + __self.name + '</div>';
						drag_el.className = 'ti-draggable';
						jQuery(drag_el).css({
							'position': 'absolute',
							'background': 'url(' + iconSrc + ') no-repeat 0 4px',
							'padding-left': '20px'
						});

						return drag_el;
					} else {
						__self.setSelected(true);
						__self.control.selectedList[__self.id] = __self;

						var drag_el = document.createElement('div');

						for (key in __self.control.selectedList) {
							drag_el.innerHTML += '<div>' + __self.control.selectedList[key].name + '</div>';
						}

						drag_el.className = 'ti-draggable';

						jQuery(drag_el).css({
							'position': 'absolute',
							'padding-left': '20px'
						});
					}
					return drag_el;
				},
				start: function() {
					Control.DraggableItem = __self;
					Control.DragMode = true;
					if (__self.control.toolbar) {
						__self.control.toolbar.hide();
					}
				},
				stop: function() {
					if (Control.HandleItem) {
						Control.HandleItem.deInitDroppable();
						if (Control.DraggableItem) {
							Control.DraggableItem.tryMoveTo(Control.HandleItem, DropMode);
						}
					}
					Control.DraggableItem = null;
					Control.DragMode = false;
					jQuery('.pages-bar a').off('mouseover');
					mouseFlag = false;
					window.clearTimeout(timeOutHandler);
				},
				drag: function(event, ui) {
					jQuery('.pages-bar a').on('mouseover', function() {
						if (!mouseFlag) {
							mouseFlag = true;
							var that = this;
							timeOutHandler = window.setTimeout(function() {
								that.click()
							}, 1000);
						}
					});
					jQuery('.pages-bar a').on('mouseout', function() {
						if (mouseFlag) {
							mouseFlag = false;
							window.clearTimeout(timeOutHandler);
						}
					});

					var x = event.pageX;
					var y = event.pageY;
					var hItem = Control.detectItemByMousePointer(x, y);
					var oldHItem = Control.HandleItem;
					if (oldHItem) {
						oldHItem.deInitDroppable();
					}

					Control.HandleItem = hItem;
					if (hItem) {
						var cpos = jQuery(hItem.control.initContainer).position();
						var itmHeight = hItem.position.bottom - hItem.position.top;
						// 310 - коэффициент подобранный вручную для табличного контрола
						var itmDelta = y - cpos.top - hItem.position.top - 310;

						if (itmDelta < itmHeight / 3) {
							DropMode = 'before';
						}
						if (itmDelta > itmHeight / 3 && itmDelta < 2 * itmHeight / 3) {
							DropMode = 'child';
						}
						if (itmDelta > 2 * itmHeight / 3) {
							DropMode = 'after';
						}

						if (hItem.isRoot) {
							DropMode = 'child';
						}

						hItem.initDroppable(DropMode);
					}
				}
			});
		}

		var childsRow = document.createElement('tr');
		var childsBar = document.createElement('td');

		if (!flatMode) {
			colSpan++;
		}

		childsBar.colSpan = colSpan;
		childsRow.appendChild(childsBar);

		var childsBody = document.createElement('table');
		childsBody.classList.add('table');
		childsBody.style.tableLayout = 'fixed';

		var columnGroup = document.createElement('colgroup');
		__updateColGroup(columnGroup);
		childsBody.appendChild(columnGroup);

		var pagesWrapper = document.createElement('tr');
		pagesWrapper.classList.add('table-row');
		pagesWrapper.classList.add('level-' + __self.level);
		pagesWrapper.style.display = 'none';

		pagesBar = document.createElement('td');
		pagesBar.classList.add('pages-bar');
		pagesBar.classList.add('table-cell');
		pagesBar.id = 'pb_' + __self.control.id + '_' + __self.id;
		pagesBar.colSpan = colSpan;

		pagesWrapper.appendChild(pagesBar);
		childsBar.appendChild(childsBody);
		childsBody.appendChild(pagesWrapper);

		__self.childsContainer = childsBody;

		switch (true) {
			case !element.nextSibling: {
				_oContainerEl.appendChild(childsRow);
				break;
			}
			case _sInsertMode == 'before': {
				element.parentNode.insertBefore(childsRow, element.nextSibling);
				break;
			}
			case _sInsertMode == 'after': {
				element.parentNode.insertBefore(childsRow, element);
				break;
			}
		}
	};

	/**
	 * Пытается отправить запрос на перемещение элемента
	 * @access public
	 * @param Object Item - элемент в который (или после которого) пытаемся переместить текущий
	 * @param Boolean asSibling - если true, перемещаем после элемента Item, если false, то делаем элемент первым ребенком Item'a
	 * @return False ,в вслучае если перемещение не возможно
	 */
	this.tryMoveTo = function(Item, MoveMode) {
		if (Item) {
			var before = this.control.getRootNodeId();
			var rel = Item.id;
			var asSibling = 1;
			var after = '';

			if (this.control.contentType == 'pages') {
				asSibling = MoveMode !== 'child' ? 1 : 0;
			}

			if (MoveMode == 'before') {
				if (this.control.contentType == 'pages') {
					before = Item.id;
					rel = Item.parent.id;
					after = rel;
				} else {
					before = Item.id;
					rel = Item.id
				}
			}
			if (MoveMode == 'after') {
				if (this.control.contentType == 'pages') {
					var s = Item.getNextSibling();
					rel = Item.parent.id;
					before = s ? s.id : this.control.getRootNodeId();
					after = Item.id;
				} else {
					rel = Item.id;
					after = Item.id;
				}
			}

			if (this.control.contentType == 'objects') {
				after = rel;
			}

			if (Item === this) {
				return false;
			}
			if (Item.checkIsChild(this)) {
				return false;
			}
			if (before == this.id) {
				return false;
			}

			var receiver = Item;

			if (asSibling == 1 && this.control.contentType == 'pages') {
				receiver = Item.parent ? Item.parent : Item.control.getRoot();
			}

			var selectedIds = [];
			var counter = 0;

			for (i in this.control.selectedList) {
				selectedIds[counter++] = i;
			}

			this.control.dataSet.execute('move', {
				'element': this.id,
				'before': before,
				'after': after,
				'rel': rel,
				'as-sibling': asSibling,
				'domain': Item.control.getRoot().name,
				'childs': 1,
				'links': 1,
				'virtuals': 1,
				'permissions': 1,
				'templates': 1,
				'receiver_item': receiver,
				'handle_item': this,
				'viewMode': 'full',
				'moveMode': MoveMode,
				'selected_list': selectedIds
			});
		}
	};

	/**
	 * Пытается подготовить элемент, как контейнер для перемещаемого
	 * @access public
	 * @param Boolean asSibling - если true, готовим для перемещения после текущего элемента, если false, то готвоим для перемещения в качестве первого ребенка
	 * @return False ,в вслучае если перемещение в этот элемент не возможно
	 */
	this.initDroppable = function(DropMode) {
		var DropMode = DropMode || 'child';
		var di = Control.DraggableItem;
		var cpos = jQuery(this.control.initContainer).offset();
		if (di) {
			if (di === this) {
				return false;
			}
			if (this.checkIsChild(di)) {
				return false;
			}

			var ind = Control.dropIndicator;

			if (oTable) {
				var table_w = jQuery(oTable.container).width();
				if (table_w < this.position.right) {
					this.position.right = table_w;
				}
			}

			if (DropMode == 'after') {
				//document.title = 'drop after ' + this.name;
				ind.style.top = this.position.bottom + cpos.top + 'px';
				ind.style.left = this.position.left + cpos.left + 'px';
				ind.style.width = this.position.right - this.position.left;
			}
			if (DropMode == 'before') {
				//document.title = 'drop before ' + this.name;
				ind.style.top = this.position.top + cpos.top + ind.offsetHeight + 'px';
				ind.style.left = this.position.left + cpos.left + 'px';
				ind.style.width = this.position.right - this.position.left + 'px';
			}
			if (DropMode == 'child') {
				//document.title = 'drop as child ' + this.name;
				ind.style.top = this.position.bottom + cpos.top + 'px';
				ind.style.left = this.position.left + cpos.left + 20 + 'px';
				ind.style.width = this.position.right + cpos.left - 20 - this.position.left + 'px';
			}

			ind.style.display = '';

			setTimeout(__autoExpandSelf, 3239); //MUF
		}
	};

	/**
	 * Восстанавливает состояние элемента из режима "контейнер для перемещаемого"
	 * @access public
	 */
	this.deInitDroppable = function() {
		if (!Control.dropIndicator) {
			return;
		}
		Control.dropIndicator.style.display = 'none';
		//this.labelControl.className = selected ? 'ti selected' : 'ti';
	};

	var __getUsedColumns = function() {
		if (__self.control.usedColumns) {
			return __self.control.usedColumns;
		}

		var usedColumns = {};

		var setColumns = Settings.get(__self.control.id, "used-columns");
		if (setColumns === false) {
			// get default columns
			setColumns = __self.control.visiblePropsMenu;
			if (setColumns.length == 0) {
				setColumns = __self.control.dataSet.getDefaultFields();
			}
			if (!setColumns) {
				setColumns = "";
			}
		}

		if (setColumns.length) {
			var arrCols = setColumns.split("|");
			for (var i = 0; i < arrCols.length; i++) {
				var info = arrCols[i];
				var colName = arrCols[i];
				var colParams = [];
				var offset = info.indexOf('[');
				if (offset) {
					colName = info.substring(0, offset);
					colParams = info.substring(offset + 1, info.length - 1).split(',');

				}
				usedColumns[colName] = {
					'name': colName,
					'params': colParams
				};
			}
		}

		if (!usedColumns['name']) {
			usedColumns['name'] = {
				'name': 'name',
				'params': ['250px']
			};
		}

		var sequenceProps = __self.control.sequencePropsMenu;

		usedColumns = objectSort(usedColumns, sequenceProps);

		__self.control.usedColumns = usedColumns;

		return usedColumns;
	};

	var objectSort = function(object, keys) {
		var property, sortedObject = {}, i = 0;

		for (property in object) {
			if (!object.hasOwnProperty(keys[i])) {
				delete keys[i];
				keys.splice(i);
			}
			if (object.hasOwnProperty(property)) {
				// Проверим наличие свойства в массиве ключей
				if (keys.indexOf(property) === -1) {
					keys.push(property);
				}
				sortedObject[keys[i]] = object[keys[i]] || "";
				i++;
			}
		}
		return sortedObject;
	};

	var __setUsedColumns = function() {
		var usedColumns = __getUsedColumns();
		var cols = [];
		for (name in usedColumns) {
			var col = usedColumns[name];
			if (!col) {
				continue;
			}
			cols[cols.length] = name + '[' + col.params.join(',') + "]";
		}

		Settings.set(__self.control.id, cols.join('|'), "used-columns");
	};

	var __getColumnsMenu = function() {
		if (__self.control.columnsMenu) {
			return __self.control.columnsMenu;
		}

		var commonGroups = __self.control.dataSet.getCommonFields();
		var usedColumns = __getUsedColumns();
		var stopList = __self.control.dataSet.getFieldsStoplist();

		var ColumnsMenu = {};
		__self.control.needColumnsMenu = false;

		var num = 1, usedNum = 0;

		for (var i = 0; i < commonGroups.length; i++) {
			num++;
			var groupFields = commonGroups[i].getElementsByTagName('field');
			var needSeparator = false;

			for (var j = 0; j < groupFields.length; j++) {
				var field = groupFields[j];
				var title = field.getAttribute('title');
				var fieldId = field.getAttribute('id');
				var name = field.getAttribute('name');

				if (jQuery.inArray(name, __self.control.requiredPropsMenu) != -1) {
					continue;
				}

				var tnodes = field.getElementsByTagName('type');
				var dataType = tnodes[0].getAttribute('data-type');
				var guideId = field.getAttribute('guide-id');

				if (dataType == "symlink" || dataType == "password") {
					continue;
				}

				var deny = false;
				for (var s = 0; s < stopList.length; s++) {
					if (stopList[s] == name) {
						deny = true;
						break;
					}
				}

				if (deny) {
					continue;
				}
				__self.control.needColumnsMenu = true;

				var used = false;
				if (usedColumns[name]) {
					used = true;
				}
				ColumnsMenu[name] = {
					'caption': title,
					'icon': used ? 'checked' : 'undefined',
					'id': fieldId,
					'title': title,
					'fieldName': name,
					'dataType': dataType,
					'guideId': guideId,
					'checked': used,
					'execute': function(item) {
						item.checked ? __self.removeColumn(item.fieldName) : __self.appendColumn(item.fieldName);
						Control.recalcItemsPosition();
					}
				};

				num++;
				if (i < groupFields.length - 1) {
					needSeparator = true;
				}
				if (used) {
					usedNum++;
				}
			}

			if (needSeparator) {
				// separator
				ColumnsMenu[num + '-sep'] = '-';
			}
		}

		__self.control.columnsMenu = ColumnsMenu;

		return ColumnsMenu;
	};

	var __getColumnsTable = function() {
		if (__self.control.columnsTable) {
			return __self.control.columnsTable;
		}

		var commonGroups = __self.control.dataSet.getCommonFields();
		var usedColumns = __getUsedColumns();
		var stopList = __self.control.dataSet.getFieldsStoplist();

		var ColumnsTable = {};
		__self.control.needColumnsTable = false;

		var num = 1, usedNum = 0;

		for (var i = 0; i < commonGroups.length; i++) {
			num++;
			var groupFields = commonGroups[i].getElementsByTagName('field');
			var needSeparator = false;

			for (var j = 0; j < groupFields.length; j++) {
				var field = groupFields[j];
				var title = field.getAttribute('title');
				var fieldId = field.getAttribute('id');
				var name = field.getAttribute('name');
				var tnodes = field.getElementsByTagName('type');
				var dataType = tnodes[0].getAttribute('data-type');
				var guideId = field.getAttribute('guide-id');

				if (dataType == "symlink" || dataType == "password") {
					continue;
				}

				var deny = false;
				for (var s = 0; s < stopList.length; s++) {
					if (stopList[s] == name) {
						deny = true;
						break;
					}
				}

				if (deny) {
					continue;
				}
				__self.control.needColumnsMenu = true;

				var used = false;
				if (usedColumns[name]) {
					used = true;
				}
				ColumnsTable[name] = {
					'caption': title,
					'icon': used ? 'checked' : 'undefined',
					'id': fieldId,
					'title': title,
					'fieldName': name,
					'dataType': dataType,
					'guideId': guideId,
					'checked': used,
					'execute': function(item) {
						var mitm = item;
						item.checked ? __self.removeColumn(item.fieldName) : __self.appendColumn(item.fieldName);
						Control.recalcItemsPosition();
					}
				};

				num++;
				if (i < groupFields.length - 1) {
					needSeparator = true;
				}
				if (used) {
					usedNum++;
				}
			}

			if (needSeparator) {
				// separator
				ColumnsTable[num + '-sep'] = '-';
			}
		}

		__self.control.columnsTable = ColumnsTable;

		return ColumnsTable;
	};

	this.resizeColumn = function(fieldName, size) {
		var usedColumns = __getUsedColumns();
		var columnsMenu = __getColumnsMenu();
		var column = usedColumns[fieldName];
		if (!column) {
			return false;
		}

		for (var j = 0; j < __self.childs.length; j++) {
			var ch = __self.control.items[__self.childs[j]];
			if (ch) {
				ch.resizeColumn(fieldName, size);
			}
		}

		if (__self.isRoot) {
			var el = document.getElementById('h_' + __self.control.id + '_' + fieldName);
			if (!el) {
				return false;
			}
			el.style.width = size + 'px';
			el.style.maxWidth = size + 'px';

			usedColumns[fieldName].params[0] = size + 'px';
			__self.control.usedColumns = usedColumns;
			__setUsedColumns();

		} else {
			var el = document.getElementById('c_' + __self.control.id + '_' + __self.id + '_' + fieldName);
			if (!el) {
				return false;
			}

			el.style.width = size + 'px';
			el.style.maxWidth = size + 'px';
		}

		var $colGroups = $('colgroup', __self.control.initContainer);
		__updateColGroup($colGroups);

		return true;
	};

	/**
	 * Обработчик события изменения размера колонки таблицы
	 * @param {String} fieldName имя поля, размер которого изменяется
	 * @param {Object} event
	 */
	this.startResizeColumn = function(fieldName, event) {
		var $floatResizing = $(document.createElement('div'));
		var $tableContainer = $(this.control.initContainer);
		var containerOffset = $tableContainer.offset();

		$floatResizing.addClass('resizer');

		$floatResizing.css({
			top: containerOffset.top,
			left: event.clientX,
			height: $tableContainer.outerHeight() + 'px'
		});

		/**
		 * Отключает выделение текста
		 */
		var disableSelection = function() {
			try {
				document.body.style['-moz-user-select'] = 'none';
				document.body.style['-khtml-user-select'] = 'none';
				document.body.onselectstart = function() {
					return false;
				};
				document.selection.empty();
			} catch (err) {
			}
		};

		/**
		 * Обработчик события изменения размера
		 * @param {Object} event
		 */
		var onResize = function(event) {
			disableSelection();
			$floatResizing.css('left', event.clientX);
		};

		/**
		 * Обработчик события окончания изменения размера
		 */
		var onStopResize = function() {
			$(document).unbind('mouseup');
			$(document).unbind('mousemove');

			var $columnElement = $('#h_' + __self.control.id + '_' + fieldName);
			var newColumnSize = $floatResizing.position().left - $columnElement.offset().left;

			newColumnSize = Math.min(newColumnSize, TableItem.maxColumnWidth);
			newColumnSize = Math.max(newColumnSize, TableItem.minColumnWidth);

			var oldColumnSize = $columnElement.outerWidth();

			if (newColumnSize !== oldColumnSize) {
				__self.resizeColumn(fieldName, newColumnSize);
				Control.recalcItemsPosition();
			}

			$floatResizing.detach();
		};

		$(document).bind('mouseup', onStopResize);
		$(document).bind('mousemove', onResize);

		$('body').append($floatResizing);
	};

	/**
	 * добавлет колонку в таблицу по менюшке колонок
	 * @param fieldName
	 * @returns {boolean}
	 */
	this.appendColumn = function(fieldName) {
		var usedColumns = __getUsedColumns();
		var columnsMenu = __getColumnsMenu();
		var column = columnsMenu[fieldName];
		if (!column || usedColumns[fieldName]) {
			return false;
		}

		for (var j = 0; j < __self.childs.length; j++) {
			var ch = __self.control.items[__self.childs[j]];
			if (ch) {
				ch.appendColumn(fieldName);
			}
		}

		if (__self.isRoot) {
			// root el
			var col = document.createElement('th');
			col.className = 'table-cell';
			col.setAttribute("id", 'h_' + __self.control.id + '_' + fieldName);
			col.setAttribute("name", name);
			col.name = fieldName;
			col.style.width = '200px';

			var resizer = document.createElement('span');
			resizer.onmousedown = function(event) {
				if (!event) {
					event = window.event;
				}
				__self.startResizeColumn(fieldName, event);
			};

			col.appendChild(resizer);

			var header = document.createElement('div');
			header.classList.add('table-title');
			header.title = column.title;
			header.innerHTML = column.title;
			col.appendChild(header);

			__self.element.insertBefore(col, __self.element.lastChild);

			usedColumns[fieldName] = {
				'name': fieldName,
				'params': ['200px']
			};
			__self.control.usedColumns = usedColumns;

			columnsMenu[fieldName].checked = true;
			columnsMenu[fieldName].icon = 'checked';
			__self.control.columnsMenu = columnsMenu;

			// column filter
			var colFltr = document.createElement('div');
			colFltr.setAttribute("id", 'f_' + __self.control.id + '_' + fieldName);
			colFltr.style.width = '100%';
			__self.control.onDrawFieldFilter(columnsMenu[fieldName], colFltr, usedColumns[fieldName].params);
			//filterRow.insertBefore(colFltr, filterRow.lastChild);
			col.appendChild(colFltr);

			// change pgBar colspan
			var pgBar = document.getElementById('pb_' + __self.control.id + '_' + __self.id);
			if (pgBar) {
				pgBar.colSpan += 1;
			}

			__setUsedColumns();
			__toggleFilterRow();
		} else {
			var col = document.createElement('td');
			col.id = 'c_' + __self.control.id + '_' + __self.id + '_' + fieldName;
			col.style.width = '280px';
			col.style.maxWidth = '200px';
			col.className = 'table-cell';
			col.name = fieldName;

			if (document.getElementById(col.id)) {
				return true;
			}

			var val = __self.getValue(fieldName);

			//col.innerHTML = val;
			__self.element.insertBefore(col, __self.element.lastChild);
			// change colspan
			__self.childsContainer.parentNode.colSpan += 1;

			var edit_col = document.createElement('i');
			edit_col.className = 'small-ico i-change editable';
			edit_col.title = getLabel('js-table-control-fast-edit');
			edit_col.id = 'e_' + __self.control.id + '_' + __self.id + '_' + fieldName;
			col.appendChild(edit_col);

			var content_col = document.createElement('div');
			content_col.style.cursor = 'text';
			content_col.style.width = '100%';
			content_col.classList.add('cell-item');
			content_col.innerHTML = val;

			col.appendChild(content_col);

			var cell = new editableCell(__self, col, column, edit_col);

			if (cell instanceof editableCell && !cell.isActive) {
				$(edit_col).detach();
			}
		}

		var $colGroups = $('colgroup', __self.control.initContainer);
		__updateColGroup($colGroups);

		return true;
	};

	this.removeColumn = function(fieldName) {
		var usedColumns = __getUsedColumns();
		var columnsMenu = __getColumnsMenu();
		var column = columnsMenu[fieldName];
		if (!column || !usedColumns[fieldName]) {
			return false;
		}

		for (var j = 0; j < __self.childs.length; j++) {
			var ch = __self.control.items[__self.childs[j]];
			if (ch) {
				ch.removeColumn(fieldName);
			}
		}

		if (__self.isRoot) {
			// root el
			var el = document.getElementById('h_' + __self.control.id + '_' + fieldName);
			if (!el) {
				return false;
			}
			el.parentNode.removeChild(el);

			var fCell = document.getElementById('f_' + __self.control.id + '_' + fieldName);
			if (fCell) {
				fCell.parentNode.removeChild(fCell);
			}

			var usedCols = {};
			for (var name in usedColumns) {
				if (name == fieldName) {
					continue;
				}
				usedCols[name] = usedColumns[name];
			}
			__self.control.usedColumns = usedCols;

			columnsMenu[fieldName].checked = false;
			columnsMenu[fieldName].icon = 'undefined';
			__self.control.columnsMenu = columnsMenu;

			__self.control.onRemoveColumn(column);

			// change pgBar colspan
			__self.childsContainer.parentNode.parentNode.colSpan -= 1;
			var pgBar = document.getElementById('pb_' + __self.control.id + '_' + __self.id);
			if (pgBar) {
				pgBar.colSpan -= 1;
			}

			__setUsedColumns();
			__toggleFilterRow();
		} else {
			var el = document.getElementById('c_' + __self.control.id + '_' + __self.id + '_' + fieldName);
			if (!el) {
				return false;
			}
			el.parentNode.removeChild(el);
			// change colspan
			__self.childsContainer.parentNode.parentNode.colSpan -= 1;
		}

		__updateColGroup($('colgroup', __self.control.initContainer));

		return true;
	};

	/**
	 * Обработчик события выполнения экспорта списка в CSV
	 */
	this.exportCallback = function(elementId) {
		var elementId = typeof elementId == 'number' ? elementId : null;

		var callback = function(elementId) {
			var selectElement = document.querySelector('select[name=encoding]');

			var selectedOptionIndex = null;
			var selectedEncoding = null;

			if (selectElement && typeof selectElement.selectedIndex == 'number') {
				selectedOptionIndex = selectElement.selectedIndex;
				selectedEncoding = selectElement.item(selectedOptionIndex).text;
			}

			var link = __self.getExportLink(elementId, selectedEncoding);
			window.location = link;
		};

		jQuery.get('/styles/skins/modern/design/js/smc/html/quickExportDialog.html', function(html) {
			html = html.replace(/{{label:(.+)}}/gi, function(match, label) {
				return getLabel(label);
			});

			openDialog('', getLabel('js-csv-export'), {
				confirmText: getLabel('js-csv-export-button'),
				html: html,
				confirmCallback: function(popupName) {
					callback(elementId);
					closeDialog(popupName);
				},
				openCallback: function() {
					initSelectizer(true);
					$('#quick_csv_encoding .checkbox').click(function() {
						$(this).toggleClass('checked');
					});
				}
			});
		});
	};

	/**
	 * Обработчик события выполнения импорта списка из CSV
	 */
	this.importCallback = function(elementId) {
		var elementId = typeof elementId == 'number' ? elementId : null;
		var link = __self.getImportLink(elementId);
		if (__self.control.contentType == 'pages' && curent_module != undefined && curent_module != 'catalog') {
			link += "&force-hierarchy=1";
		}
		link += "&rnd=" + Math.round(Math.random() * 1000 % 1000);
		__self.callCsvImport(link);
	};

	var __toggleFilterRow = function() {
		var usedColumns = __getUsedColumns();
		var columnsTable = __getColumnsTable();

		var i = 0;
		for (var name in usedColumns) {
			var column = columnsTable[name];
			var params = usedColumns[name]['params'];

			if (params[1] == 'static') {
				column = usedColumns[name];
				column.title = getLabel('js-smc-' + name);
			}

			if (!column) {
				continue;
			}
			i++;
		}

		if (filterRow) {
			filterRow.style.display = (i > 0) ? '' : 'none';
		}
	};

	var resizeHandler = function(fieldName) {
		return function(event) {
			if (!event) {
				event = window.event;
			}
			__self.startResizeColumn(fieldName, event);
			return true;
		};
	};

	/**
	 * Рисует корневой элемент
	 * @access private
	 */
	var __drawRoot = function() {
		if (!Control.dropIndicator) {
			var dropIndicator = document.createElement('div');
			dropIndicator.className = 'ti-drop';
			Control.dropIndicator = document.body.appendChild(dropIndicator);
		}

		//Получаем информацию о столбцах
		var usedColumns = __getUsedColumns();   // Активные колонки
		var columnsMenu = __getColumnsMenu();   // Меню колонок
		var columnsTable = __getColumnsTable(); // Список всех колонок

		var possibleColumnsCount = 0, usedColumnsCount = 0;
		for (var i in columnsMenu) {
			++possibleColumnsCount;
		}

		//рисуем заголовок
		var tHead = document.createElement('tr');
		tHead.setAttribute('name', __self.control.id);
		tHead.className = "table-row title";

		//Колонка имен вобщем первая с титулником
		var nameData = usedColumns['name'],
				nameCell = document.createElement('th');

		nameCell.className = 'table-cell';
		nameCell.id = 'h_' + __self.control.id + '_name';
		nameCell.setAttribute('name', 'name');
		nameCell.name = name;
		nameCell.width = parseInt(nameData['params'][0]);

		var header = document.createElement('div');
		//header.style.width = nameData['params'][0];
		header.classList.add('table-title');
		header.classList.add('disabled');

		var firstColumnTitle = getLabel('js-smc-name-column');
		if (__self.control.labelFirstColumn.length > 0) {
			firstColumnTitle = __self.control.labelFirstColumn;
		}

		header.innerHTML = firstColumnTitle;
		header.title = firstColumnTitle;

		var resizer = document.createElement('span');
		resizer.onmousedown = resizeHandler('name');

		nameCell.appendChild(resizer);
		nameCell.appendChild(header);

		// добавлем поле поиска для имен если надо
		if (__self.control.enableEdit) {
			var inputSearch = document.createElement('div');
			inputSearch.className = 'input-search';
			inputSearch.id = 'f_' + __self.control.id + '_name';
			__self.control.onDrawFieldFilter('name', inputSearch, nameData.params);
			nameCell.appendChild(inputSearch);
		}

		tHead.appendChild(nameCell);

		var colSpan = 2;

		for (var name in usedColumns) {
			var column = columnsTable[name];
			var params = usedColumns[name]['params'];
			if (params[1] == 'static') {
				column = usedColumns[name];
				column.title = getLabel('js-smc-' + name);
			}
			if (!column) {
				continue;
			}

			colSpan++;

			var col = document.createElement('th');
			col.classList.add('table-cell');
			col.id = 'h_' + __self.control.id + '_' + name;
			col.setAttribute('name', name);
			col.style.width = params[0];

			header = document.createElement('div');
			header.classList.add('table-title');
			header.classList.add('disabled');
			header.title = column.title;
			header.innerHTML = column.title;

			col.appendChild(header);

			var resizer = document.createElement('span');
			resizer.onmousedown = resizeHandler(name);
			col.appendChild(resizer);

			var filterField = document.createElement('div');
			filterField.id = 'f_' + __self.control.id + '_' + name;
			filterField.style.width = '100%';
			__self.control.onDrawFieldFilter(column, filterField, params);

			col.appendChild(filterField);

			tHead.appendChild(col);

			++usedColumnsCount;
		}

		var autocol = document.createElement('th');
		autocol.classList.add('table-cell');
		autocol.classList.add('plus');
		autocol.style.width = '100%';
		autocol.style.textAlign = 'left';
		autocol.style.verticalAlign = 'middle';

		if (!__self.control.objectTypesMode && __self.control.needColumnsMenu) {
			var invokeBtn = document.createElement('i');
			invokeBtn.classList.add('small-ico');
			invokeBtn.classList.add('i-add');
			invokeBtn.classList.add('pointer');
			jQuery(invokeBtn).bind("click", function(event) {
				jQuery.cmenu.lockHiding = true;
				jQuery.cmenu.show(jQuery.cmenu.getMenu(__self.control.columnsMenu), __self.control.initContainer.offsetParent, event);
			});

			jQuery(invokeBtn).bind('mouseout', function() {
				jQuery.cmenu.lockHiding = false;
			});
			autocol.oncontextmenu = function() {
				return false;
			};
			autocol.appendChild(invokeBtn);
		}

		tHead.appendChild(autocol);

		jQuery(tHead).bind("click", function(event) {
			var el = event.target;

			if (el !== autocol) {
				TableItem.orderByColumn(el.parentNode.getAttribute('name'), __self.control, el);
			}
		});

		//todo: Хз что ниже происходит, надо разобраться, что-то вроде выключения фильров по какойто опции.
		//if (!__self.control.objectTypesMode) tHead.appendChild(filterRow);

		__toggleFilterRow();

		// Контейнер для данных таблицы ниже уже новый код
		var pagesWrapper = document.createElement('tBody');
		//pagesWrapper.classList.add('table-row');
		pagesWrapper.classList.add('level-' + __self.level);
		pagesWrapper.style.display = 'none';

		var pagesRow = document.createElement('tr');
		pagesRow.style.display = 'none';
		pagesRow.classList.add('table-row');

		pagesBar = document.createElement('td');
		pagesBar.classList.add('pages-bar');
		pagesBar.classList.add('table-cell');
		pagesBar.id = 'pb_' + __self.control.id + '_' + __self.id;
		if (!flatMode) {
			colSpan++;
		}
		pagesBar.colSpan = colSpan;

		pagesRow.appendChild(pagesBar);

		pagesWrapper.appendChild(pagesRow);

		__self.element = __self.control.container.appendChild(tHead);
		__self.childsContainer = __self.control.container.appendChild(pagesWrapper);
		//__self.control.container.appendChild(pagesWrapper);
		/*//__self.childsContainer  = __self.control.container.appendChild(tBody);
		 __self.childsContainer  = __self.control.container;
		 __self.control.initContainer = __self.control.container;
		 __self.control.container = __self.childsContainer;*/
		__self.control.initContainer = __self.control.container;
		__self.control.container = __self.childsContainer;

		//Добавить ссылку на экспорт CSV в том случае, если мы имеем дело с таблицами
		if (flatMode && !objectTypesMode) {
			__self.showCsvButtons(__self.exportCallback, __self.importCallback);
		}

		__self.expand();
	};

	/**
	 * Метод рисующий кнопки испорта экспорта в csv
	 * @param exportCallback
	 * @param importCallback
	 */
	this.showCsvButtons = function(exportCallback, importCallback) {
		if (this.control.disableCSVButtons) {
			return;
		}
		__self.hideCsvButtons();

		var csvButtons = document.createElement('div');
		csvButtons.id = 'csv-buttons';

		var aExportCsv = document.createElement('a');
		var exImg = document.createElement('i');
		exImg.className = 'small-ico i-csv-export';
		aExportCsv.appendChild(exImg);
		aExportCsv.appendChild(document.createTextNode(getLabel('js-csv-export')));
		aExportCsv.href = "#";
		aExportCsv.className = "csvLink csvExport btn-action";

		aExportCsv.onclick = exportCallback;
		csvButtons.appendChild(aExportCsv);

		if (!this.control.hideCsvImportButton) {
			var aImportCsv = document.createElement('a');
			var imImg = document.createElement('i');
			imImg.className = 'small-ico i-csv-import';
			aImportCsv.appendChild(imImg);
			aImportCsv.appendChild(document.createTextNode(getLabel('js-csv-import')));
			aImportCsv.href = "#";
			aImportCsv.className = "csvLink csvImport btn-action";

			aImportCsv.onclick = importCallback;
			csvButtons.appendChild(aImportCsv);
		}

		var parent = document.getElementById('csv-buttons-zone');
		parent.appendChild(csvButtons);
		//__self.control.container.parentNode.parentNode.appendChild(csvButtons);
		__self.csvButtons = csvButtons;
	};

	/**
	 * Соотверственно прячем кнопочки
	 */
	this.hideCsvButtons = function() {
		if (__self.csvButtons) {
			jQuery(__self.csvButtons).remove();
			__self.csvButtons = false;
		}

		jQuery('#csv-buttons-zone').html('');
	};

	/**
	 * Получаем линки на действие экспорта
	 * @param elementId
	 * @param encoding
	 * @returns {string}
	 */
	this.getExportLink = function(elementId, encoding) {
		var usedColumns = __getUsedColumns();
		var filterQueryString = this.control.getCurrentFilter().getQueryString();

		var path = document.location['pathname'];
		try { // Обработка вариантов, в которых document.location не совпадает с заданным адресом
			var cleanPath = __self.control.dataSet.getPathModuleMethod();
		} catch (err) {
		}

		if (path !== cleanPath && path.indexOf(cleanPath) == 0) {
			var pathDifference = path.substring(cleanPath.length);
			var expr = new RegExp('/([\/0-9]*)/');
			path = (expr.test(pathDifference)) ? path : cleanPath;
		} else {
			path = cleanPath;
		}

		var link = path + filterQueryString + "&xmlMode=force&export=csv";
		for (var i in usedColumns) {
			var columnName = usedColumns[i]['name'];
			if (columnName != "name") {
				link += "&used-fields[]=" + columnName;
			}
		}
		if (elementId) {
			link += "&rel[]=" + elementId;
		}

		if (typeof encoding == 'string' && encoding.length > 0) {
			link += "&encoding=" + encoding;
		}

		return link;
	};

	/**
	 * Линк на импорт
	 * @param elementId
	 * @returns {*}
	 */
	this.getImportLink = function(elementId) {
		var filterQueryString = this.control.getCurrentFilter().getQueryString();
		var link = document.location['pathname'];
		link += filterQueryString + "&xmlMode=force&import=csv";
		if (elementId) {
			link += "&rel[]=" + elementId;
		}
		return link;
	};

	/**
	 * Рисуем диалог импорта
	 * @param link
	 */
	this.callCsvImport = function(link) {
		window.csvQuickImportCallback = function() {
			closeDialog();
			document.location.reload();
		};

		jQuery.get('/styles/skins/modern/design/js/smc/html/quickImportDialog.html', function(html) {
			html = html.replace(/{{label:(.+)}}/gi, function(match, label) {
				return getLabel(label);
			});

			openDialog('', getLabel('js-csv-import'), {
				html: html,
				width: 400,
				confirmText: getLabel('js-csv-import-button'),
				confirmCallback: function(popupName) {
					jQuery("#import-csv-form input[name='csrf']").val(csrfProtection.getToken());
					jQuery("#import-csv-form").submit();
					return false;
				},
				openCallback: function() {
					$('#import-csv-form').attr('action', link);

					initSelectizer();
					$('#import-csv-form .checkbox').click(function() {
						$(this).toggleClass('checked');
					});
				}
			});
		});
	};

	/**
	 * Разворачивает элемент, если он находится под курсором
	 * Используется в режиме drag&drop
	 * @access private
	 */
	var __autoExpandSelf = function() {
		if (AutoexpandAllowed && __self === Control.HandleItem) {
			__self.expand();
		}
	};

	this.draw = function() {
		if (_oParent) {
			if (!ForceDraw) {
				__draw(Parent.childsContainer);
			}
		} else {
			if (!ForceDraw) {
				__drawRoot();
			}
		}
	};

	/**
	 * Добавляет дочерний элемент последним в списке
	 * @access public
	 * @param {Array} _oChildData - массив с информацией о новом элементе
	 * @return {Object} - новый элемент
	 */
	this.appendChild = function(_oChildData) {
		return new TableItem(this.control, __self, _oChildData);
	};

	/**
	 * Добавляет дочерний элемент после указанного элемента
	 * @access public
	 * @param {Array} _oChildData - массив с информацией о новом элементе
	 * @param {Object} oItem - элемент, после которого добавится новый
	 * @return {Object} - новый элемент
	 */
	this.appendAfter = function(_oChildData, oItem) {
		return new TableItem(this.control, __self, _oChildData, oItem, 'after');
	};

	/**
	 * Добавляет дочерний элемент перед указанным элементом
	 * @access public
	 * @param {Array} _oChildData - массив с информацией о новом элементе
	 * @param {Object} oItem - элемент, перед которым добавится новый
	 * @return {Object} - новый элемент
	 */
	this.appendBefore = function(_oChildData, oItem) {
		return new TableItem(this.control, __self, _oChildData, oItem, 'before');
	};

	/**
	 * Добавляет дочерний элемент в начало списка
	 * @access public
	 * @param {Array} _oChildData - массив с информацией о новом элементе
	 * @return {Object} - новый элемент
	 */
	this.appendFirst = function(_oChildData) {
		if (this.childsContainer.childNodes.length == 2) {
			return this.appendChild(_oChildData);
		} else if (typeof(this.childsContainer.childNodes[2].rel) != 'undefined') {
			return this.appendBefore(_oChildData, this.control.getItem(this.childsContainer.childNodes[1].rel));
		} else {
			return false;
		}
	};

	/**
	 * Возвращает предыдущего соседа элемента
	 * @access public
	 * @return {Object} предыдущий сосед, либо null
	 */
	this.getPreviousSibling = function() {
		var prevEl = this.element.previousSibling;
		if (prevEl && prevEl.rel) {
			return this.control.getItem(prevEl.rel);
		}
		return null;
	};

	/**
	 * Возвращает последующего соседа элемента
	 * @access public
	 * @return {Object} последующий сосед, либо null
	 */
	this.getNextSibling = function() {
		var prevEl = this.element.nextSibling;
		if (prevEl && prevEl.rel) {
			return this.control.getItem(prevEl.rel);
		}
		return null;
	};

	/**
	 * Удаляет элемент из DOM
	 * @access public
	 */
	this.clear = function() {
		if (this.isRoot) {
			var parent = this.element.parentNode;
			while (parent.firstChild) {
				parent.removeChild(parent.firstChild);
			}
		} else if (this.element.parentNode) {
			this.element.parentNode.removeChild(this.element.nextSibling);
			this.element.parentNode.removeChild(this.element);
		}
	};

	/**
	 * Проверяет, является ли текущий элемент потомком указанного (на всю глубину)
	 * @access public
	 * @param {Object} oItem - элемент
	 * @return {Boolean} true, если является
	 */
	this.checkIsChild = function(oItem) {
		var parent = this.parent;
		while (parent) {
			if (oItem === parent) {
				return true;
			}
			parent = parent.parent;
		}
		return false;
	};

	/**
	 * Возвращает координаты DOM-представления элемента
	 * Метод является обязательным, вызывается Control'ом.
	 * Служит для определения элемента под курсором мыши
	 * @access public
	 */
	this.recalcPosition = function() {
		if (__self.isRoot) {
			return false;
		}
		try {
			var parent = this.parent;
			while (parent) {
				if (!parent.isExpanded) {
					this.position = false;
					return false;
				}
				parent = parent.parent;
			}

			var container = this.control.getRoot().childsContainer;
			var pos = jQuery(labelCtrl).position();

			this.position = {
				'left': pos.left,
				'top': pos.top,
				'right': pos.left + container.offsetWidth,
				'bottom': pos.top + jQuery(labelCtrl).height()
			};

			return this.position;
		} catch (e) {
			this.position = false;
			return false;
		}
	};

	/**
	 * Выставляет статус загружены/не загружены дети элемента
	 * Метод является обязательным, вызывается Control'ом!
	 * @param {Boolean} loaded - статус
	 * @access public
	 */
	this.setLoaded = function(loaded) {
		this.loaded = loaded;
		var ico = loaded ? IconsPath + 'collapse.png' : IconsPath + 'expand.png';
		if (toggleCtrl) {
			toggleCtrl.setAttribute('src', IconsPath + 'collapse.png');
		}
	};

	var __renderProperty = function(propEl) {
		if (!propEl) {
			return "";
		}
		if (!propEl['type']) {
			return "";
		}

		var val = "", tmp, src;

		var _v = function(el, def) {
			return el.value._value || (el.value || (def || ''));
		};

		switch (propEl['type']) {
			case "int":
			case "float":
			case "price":
			case "link_to_object_type":
			case "counter":
				val = _v(propEl, 0);
				break;
			case "tags" :
			case "string" :
				val = _v(propEl);
				if (propEl.restriction == 'email') {
					val = "<a href='mailto:" + val + "' title='" + val + "' class='link'>" + val + "</a>";
					break;
				}
				if (typeof val === "object" && Object.prototype.toString.call(val) === "[object Array]") {
					tmp = val.join(", ");
					val = tmp;
				}
				if (/http:\/\//.test(val)) {
					val = "<a href=\"" + val + "\" title=\"" + val + "\" class=\"link\">" + val + "</a>";
				} else {
					val = '<span title="' + val + '">' + val + '</span>';

				}
				break;
			case 'color': {
				var value = propEl.value ? propEl.value : '';
				if (value) {
					val = '<div class="color table"><span class="value">' + value +
							'</span><span class="color-box"><i style="background-color: ' + value +
							'"></i></span></div>';
				}

				break;
			}

			case "text": {
				val = _v(propEl);
				if (val) {
					//val = '<span title="' + val + '">' + val.substring(0, 255) + '</span>';
				}
				break;
			}

			case "boolean":
				val = _v(propEl, false) ? '<img alt="" style="width:13px;height:13px;" src="/images/cms/admin/mac/tree/checked.png" />' : '';
				break;
			case "img_file": {
				val = propEl.value;
				if (val) {
					src = val._value;
					var path = src.substring(0, src.lastIndexOf('.'));
					var filename = "";
					if (typeof src == "string") {
						tmp = src.split(/\//);
						filename = tmp[tmp.length - 1];
					}
					if (val.is_broken == '1') {
						val = '<span title="404" style="color:red;font-weight: bold;cursor: pointer;">?</span>';
						val += '&nbsp;&nbsp;<span style="text-decoration: line-through;" title=\"' + src + '\">' + filename + "</span>";
					} else {
						var ext = val.ext;
						var thumbSrc = "/autothumbs.php?img=" + path + '_sl_180_120.' + ext;
						val = '<img alt="" style="width:13px;height:13px;cursor: pointer;" src="/images/cms/image.png" onmouseover="TableItem.showPreviewImage(event, \'' + thumbSrc + '\')" />';
						val += '&nbsp;&nbsp;<span title=\"' + src + '\">' + filename + "</span>";
					}
				}
				break;
			}
			case "video_file":
			case "swf_file":
			case "file": {
				val = propEl.value;
				if (val) {
					src = val._value;
					if (val.is_broken == '1') {
						val = src ? ('<span style="text-decoration: line-through;" title="' + src + '">' + src + '</span>') : '';
					} else {
						val = src ? ('<span title="' + src + '">' + src + '</span>') : '';
					}
				}
				break;
			}
			case "relation":
				var o = _v(propEl, null);
				if (o) {
					if (o.item[0]) {
						for (var i = 0; i < o.item.length; i++) {
							val += o.item[i].name;
							if (i < o.item.length - 1) {
								val += ', ';
							}
						}
						val = '<span title="' + val + '">' + val + '</span>';
					} else {
						val = o.item.name;
						var guid = 'relation-value';
						if (o.item.guid != undefined) {
							guid = o.item.guid;
						}
						val = '<span title="' + val + '" class="c-' + guid + '">' + val + '</span>';
					}
				}
				break;
			case "date":
				val = propEl['value']['formatted-date'];
				break;
			case "wysiwyg" :
				val = _v(propEl, null);
				if (val['#text']) {
					val = val['#text'].join();
				}
				val = val.replace(/<\/?[^>]+>/g, '');
				break;
			default:
				return "";
				break;
		}

		return val;
	};

	/**
	 * Получает значение свойства c именем fieldName
	 * @param {String} fieldName - имя свойства
	 * @return {Mixed} значение свойства, либо false, в случае неудачи
	 */
	this.getValue = function(fieldName) {
		var usedColumns = __getUsedColumns();

		var col = usedColumns[fieldName];

		if (fieldName == 'name') {
			return this.control.onGetValueCallback(this.name, fieldName, this);
		}

		if (col) {
			if (col.params[1] == 'static') {
				return this.control.onGetValueCallback(col, fieldName, this);
			}
		}

		if (!Data['properties']) {
			return '&nbsp;';
		}
		if (!Data['properties']['group']) {
			return '&nbsp;';
		}

		var Groups = typeof(Data['properties']['group'][0]) != 'undefined' ? Data['properties']['group'] : [Data['properties']['group']];

		for (var i = 0; i < Groups.length; i++) {
			if (!Groups[i]['property']) {
				continue;
			}
			var Props = typeof(Groups[i]['property'][0]) != 'undefined' ? Groups[i]['property'] : [Groups[i]['property']];

			for (var j = 0; j < Props.length; j++) {
				if (Props[j]['name'] == fieldName) {
					Props[j] = this.control.onGetValueCallback(Props[j], fieldName, this);
					return __renderProperty(Props[j]);
				}
			}

		}

		return this.control.onGetValueCallback('', fieldName, this);
	};

	/**
	 * Получить данные, которые отдал DataSet
	 * @return Array объект со свойствами
	 */
	this.getData = function() {
		return Data;
	};

	/**
	 * Выставляет pageing у item'а и заполняет pagesBar страницами
	 * @param {Object} pageing объект с информацией о пагинации
	 */
	this.setPageing = function(pageing) {
		if (!pageing || !pagesBar) {
			return false;
		}

		this.pageing = pageing;
		pagesBar.parentNode.style.display = 'none';
		pagesBar.classList.add('panel-sorting');
		pagesBar.innerHTML = '';
		pagesBar.style.textAlign = 'left';

		if (this.isRoot) {
			this.setPageingLimits(pageing);
		}

		if (pageing.total <= pageing.limit && pageing.offset <= 0) {
			return;
		}

		pagesBar.parentNode.style.display = '';

		var getCallback = function(page) {
			return function() {
				__self.filter.setPage(page);
				__self.applyFilter(__self.filter, true);
				return false;
			};
		};

		var totalPages = Math.ceil(pageing.total / pageing.limit);
		var currentPage = Math.ceil(pageing.offset / pageing.limit);
		var nextPage;
		var drawEllipsis = true;

		var FIRST_PAGE = 0;
		var LAST_PAGE = (Math.ceil(pageing.total / pageing.limit) - 1);
		var NUMBER_OF_PAGES_SHOWN = 1;

		var isHiddenPage = function(page) {
			return ( page != FIRST_PAGE && page != LAST_PAGE && (Math.abs(page - currentPage) > NUMBER_OF_PAGES_SHOWN) );
		};

		for (var page = 0; page < totalPages; page += 1) {
			if (isHiddenPage(page)) {
				if (drawEllipsis) {
					nextPage = document.createElement('a');
					nextPage.href = "#";
					nextPage.innerHTML = "…";
					nextPage.className = "pagination-ellipsis";
					nextPage.onclick = function() {
						return false;
					};
					pagesBar.appendChild(nextPage);
				}

				drawEllipsis = false;
				continue;
			}

			drawEllipsis = true;
			nextPage = document.createElement('a');
			nextPage.href = "#";

			if (currentPage == page) {
				nextPage.className = 'current';
			}

			nextPage.innerHTML = page + 1;
			nextPage.onclick = getCallback(page);

			pagesBar.appendChild(nextPage);
		}
	};

	/**
	 * Разворачивает элемент
	 * Метод является обязательным, вызывается Control'ом!
	 * @access public
	 */
	this.expand = function() {
		if (!this.hasChilds) {
			return false;
		}

		this.isExpanded = true;
		if (!this.loaded) {
			this.loaded = true;
			this.control.load(this.filter);
		}
		if (toggleCtrl) {
			toggleCtrl.classList.add('switch');
		}

		this.childsContainer.style.display = '';
		$(this.childsContainer).find('.checkbox input:checked').parent().addClass('checked');
		$(this.childsContainer).find('.checkbox').click(function() {
			$(this).toggleClass('checked');
		});

		if (this.loaded) {
			Control.recalcItemsPosition();
		}

		// save settings
		this.control.saveItemState(this.id);

	};

	/**
	 * Сворачивает элемент
	 * Метод является обязательным, вызывается Control'ом!
	 * @access public
	 */
	this.collapse = function() {
		if (!this.hasChilds) {
			return false;
		}

		this.isExpanded = false;
		if (toggleCtrl) {
			toggleCtrl.classList.remove('switch');
		}
		this.childsContainer.style.display = 'none';
		Control.recalcItemsPosition();
		// save settings
		this.control.saveItemState(this.id);
	};

	/**
	 * Сворачивает/разворачивает элемент в зависимости от текущего состояния
	 * Метод является обязательным, вызывается Control'ом!
	 * @access public
	 */
	this.toggle = function(el) {
		if (this.hasChilds) {
			$(el).toggleClass('switch');
			this.isExpanded ? this.collapse() : this.expand();
		}
	};

	/**
	 * Обновляет элемент, используя новые данные о нем
	 * @param {Array} _oNewData - новые данные о элементе
	 * Метод является обязательным, вызывается Control'ом!
	 * @access public
	 */
	this.update = function(_oNewData) {
		if (_oNewData) {
			if (this.id != _oNewData.id) {
				return false;
			}

			Data = _oNewData;

			//change template id
			if (typeof(_oNewData['template-id']) !== 'undefined') {
				this.templateId = _oNewData['template-id'];
			}

			// change view link
			if (typeof(_oNewData['link']) !== 'undefined' && _oNewData['link'] != this.viewLink) {
				this.viewLink = _oNewData['link'];
				labelCtrl.setAttribute('href', this.viewLink);
			}

			// change childs
			if (typeof(_oNewData['childs']) !== 'undefined' && parseInt(_oNewData['childs']) !== CountChilds) {
				CountChilds = parseInt(_oNewData['childs']);
				this.hasChilds = CountChilds > 0 || id == 0;
				var toggleWrapper = $('td.table-cell > div > span:first', labelCtrl);

				if (__self.hasChilds) {
					toggleWrapper.removeClass('catalog-toggle-off');
					toggleWrapper.removeClass('switch');
					toggleWrapper.addClass('catalog-toggle-wrapper');
					toggleWrapper.empty();

					toggleCtrl = document.createElement('span');
					toggleCtrl.className = 'catalog-toggle switch';
					toggleCtrl.src = '/images/cms/admin/mac/tree/collapse.png';
					toggleWrapper.append(toggleCtrl);
				} else {
					toggleWrapper.removeClass('catalog-toggle-wrapper');
					toggleWrapper.removeClass('switch');
					toggleWrapper.addClass('catalog-toggle-off');
					toggleWrapper.empty();
				}
			}

			// change active
			var newActive = typeof(_oNewData['is-active']) !== 'undefined' ? parseInt(_oNewData['is-active']) : 0;
			if (newActive !== this.isActive) {
				this.isActive = newActive;
				this.isActive = isObjectActivated(this, this.isActive);
				if (!this.isActive && !__self.control.objectTypesMode) {
					jQuery(__self.row).addClass('disabled');
				} else {
					jQuery(__self.row).removeClass('disabled');
				}

				if (activeColumn) {
					jQuery('div', activeColumn).html(__renderProperty({'type': 'boolean', 'value': this.isActive}));
				}
			}

			if (typeof(_oNewData['name']) !== 'undefined' && _oNewData['name'] != this.name) {
				this.name = _oNewData['name'].length ? _oNewData['name'] : "";
				labelText.innerHTML = this.name.length ? this.name : getLabel('js-smc-noname-page');
			}

		}
	};

	/**
	 * Устанавливает фильтр для детей элемента и обновляет содержимое, если потребуется
	 * @access public
	 * @param _Filter
	 * @param ignoreHierarchy
	 */
	this.applyFilter = function(_Filter, ignoreHierarchy) {
		if (_Filter instanceof Object) {
			this.filter = _Filter;
		} else {
			this.filter.clear();
		}
		if (!ignoreHierarchy) {
			this.filter.setParentElements(id);
		}
		if (this.loaded) {
			this.control.removeItem(id, true);
			this.loaded = false;
			if (this.isExpanded) {
				this.expand();
			}
		}
	};

	this.check = function() {
		if (hasCheckbox) {
			$(__self.checkBox).toggleClass('checked');
		}
	};

	/**
	 * Устанавливает или снимает выделение элемента
	 * @param {Boolean} _selected если true, то элемент будет выделен, если false, то выделение
	 * будет снято
	 */
	this.setSelected = function(_selected) {
		if (_selected) {
			if (!oldClassName) {

				oldClassName = labelCtrl.className;
				labelCtrl.classList.add('selected');
			}

			if (hasCheckbox) {
				$(this.checkBox).addClass('checked');
			}

			selected = true;
		} else {
			if (oldClassName) {
				oldClassName = null;
			}
			$(labelCtrl).removeClass('selected');
			if (hasCheckbox) {
				$(this.checkBox).removeClass('checked');
			}

			selected = false;
		}
	};

	this.getSelected = function() {
		return selected;
	};

	/**
	 * Рисуем кнопочки с выбором количества эмлементов на страницу
	 * @param pageing
	 */
	this.setPageingLimits = function(pageing) {
		var getCallback = function(limit) {
			return function() {
				__self.filter.setLimit(limit);
				__self.applyFilter(__self.filter, true);
				return false;
			};
		};
		var root = document.getElementById('per_page_limit');
		root.innerHTML = '<span>' + getLabel('js-label-elements-per-page') + '</span>';
		for (var i in __self.pageLimits) {
			var link = document.createElement('a');
			link.className = 'per_page_limit';
			link.rel = '0';
			link.innerHTML = __self.pageLimits[i];
			link.onclick = getCallback(__self.pageLimits[i]);
			if (__self.pageLimits[i] == pageing.limit) {
				link.className += ' current';
			}
			root.appendChild(link);
		}
	};

	// {main}
	__constructor();
};

// static props

TableItem.minColumnWidth = 150;
TableItem.maxColumnWidth = 800;

/**
 * Сортирвока по колонкам дефолт asc
 * asc -> ""
 * desc-> "switch"
 * не активная -> "disabled"
 * @param _FieldName
 * @param _Control
 * @param _ColumnEl
 * @returns {boolean}
 */
TableItem.orderByColumn = function(_FieldName, _Control, _ColumnEl) {
	if (!_Control || !_FieldName || !_ColumnEl) {
		return false;
	}

	var direction = "asc", className = "";
	if (_Control.orderColumn === _ColumnEl) {
		className = _ColumnEl.classList.contains("switch") ? "" : "switch";
		if (className == "") {
			_ColumnEl.classList.remove("switch");
		}
		if (className !== "") {
			_ColumnEl.classList.add(className);
		}
	} else {
		if (_Control.orderColumn) {
			_Control.orderColumn.classList.remove('switch');
			_Control.orderColumn.classList.add('disabled');
		}
		_Control.orderColumn = _ColumnEl;
		_Control.orderColumn.classList.remove('disabled');
	}
	direction = (className == "" ? "asc" : "desc");
	var defFilter = _Control.dataSet.getDefaultFilter();
	defFilter.setOrder(_FieldName, direction);
	_Control.dataSet.setDefaultFilter(defFilter);

	_Control.applyFilter();
};

TableItem.showPreviewImage = function(event, src) {
	if (!event || !src) {
		return;
	}
	var el = (!event.target) ? event.toElement : event.target;
	var x = (!event.pageX) ? event.clientX : event.pageX;
	var y = (!event.pageY) ? event.clientY : event.pageY;

	var img = document.createElement('img');
	img.setAttribute('src', src);
	img.src = src;
	img.className = 'img-fieled-preview';
	img.style.position = 'absolute';
	img.style.width = '180px';
	img.style.height = '120px';
	img.style.top = y + 10 + 'px';
	img.style.left = x + 10 + 'px';
	img.style.border = '1px solid #666';

	document.body.appendChild(img);

	el.onmouseout = function() {
		img.parentNode.removeChild(img);
	};

};
