<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
	<head>
	<title>Конференции начинающих ортодонтов.</title>
	<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
	<link rel="stylesheet" media="screen" type="text/css" href="main.css">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic">
	<script src="js/jquery-last.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script>
	lnks = new Array("contacts", "form", "price", "place", "programm", "about", "question");
	$(document).ready(function() {
		hghts = new Array();
		for (var key in lnks) {
			if ($("[name=" + lnks[key] +"]").length) {
				hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
			}
		}
		var i = document.location.hash.replace("#", "");
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					var i = target.attr("name");
					scrolling = 1;
					$('html,body').animate({
						scrollTop: target.offset().top - 30
					}, 1000, function() {
						scrolling = 0;
						// alert(target.offset().top);
					});
					$(".ch").removeClass("ch");
					$(this).parent().addClass("ch");
					// history.pushState(null, i, i);
					return false;
				}
			}
		});
	});
	var scrolling = 0;
	window.onscroll = function() {
		var scrolled = window.pageYOffset || document.documentElement.scrollTop;
		if (scrolled > 4800) {
			$("#menu").css("position", "relative");
			$("#menu").css("top", 4200);
		}
		else {
			if (scrolled > 120) {
				$("#menu").css("position", "fixed");
				$("#menu").css("top", 50);
			}
			else {
				$("#menu").css("position", "relative");
				$("#menu").css("top", 0);
			}
		}
		if (scrolling == 0) {
			for (var key in hghts) {
				if (scrolled >= hghts[key] - 300) {
					$(".ch").removeClass("ch");
					$("#mi" + lnks[key]).addClass("ch");
					break;
				}
			}
		}
	}
	$(document).ready(function() {
		initialize_map();
	});
	function reload() {
		var src = document.captcha.src;
		document.captcha.src = '/images/loading.gif';
		document.captcha.src = src + '?rand='+Math.random();
	}
	function qreload() {
		var src = document.qcaptcha.src;
		document.qcaptcha.src = '/images/loading.gif';
		document.qcaptcha.src = src + '?rand='+Math.random();
	}
	var x1 = 55.7901916;
	var y1 = 37.5469096;
	var x2 = 59.9336949;
	var y2 = 30.3086740;
	var myLatlng1;
	var myLatlng2;
	var map;
	var marker1;
	var marker2;
	function initialize_map() {
		myLatlng1 = new google.maps.LatLng(x1, y1);
		myLatlng2 = new google.maps.LatLng(x2, y2);
		var mapOptions = {
			center: myLatlng1,
			mapTypeControl:!1,
			streetViewControl:!1,
			scrollwheel:!1,
			panControl:!1,
			zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map"),
		mapOptions);
		marker1 = new google.maps.Marker({
			position: myLatlng1,
			map: map,
			title: "Москва"
		});
		marker2 = new google.maps.Marker({
			position: myLatlng2,
			map: map,
			title: "Санкт-Петербург"
		});
	}
	function map1() {
		map.setCenter(myLatlng1);
	}
	function map2() {
		map.setCenter(myLatlng2);
	}
	</script>
	</head>
<body>
	<div class="section to1000 top">
		<div class="section header"><img src="images/logo.png" /></div>
	</div>
	<div class="section to1000 group darkblue">
	<div class="left leftbar">
		<div class="menu">
			<div id="menu">
				<ul>
					<li id="miabout" class="ch"><a href="#about"><h5>О Конференции</h5></li>
					<li id="miprogramm"><a href="#programm"><h5>Программа</h5></li>
					<li id="miplace"><a href="#place"><h5>Время и место проведения</h5></a></li>
					<li id="miprice"><a href="#price"><h5>Стоимость участия</h5></a></li>
					<!--<a href="#form"><button>Регистрация</button></a>-->
					<li id="micontacts"><a href="#contacts"><h5>Контакты</h5></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="left baseinfo seminar">
		<img src="images/conf.jpg" />
		<br />
		<br />
		<br />
		<br />
		<!--
		<div class="group bordered">
		<div class="left cities">
		Москва<br />
		<!--Санкт–Петербург
		</div>
		<div class="left dates">					2 – 3 апреля&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Чт–Пт)<br />					19 сентября 2015 г.&nbsp;&nbsp;(9:00 - 19:00)				</div>			 <!--	<div class="left reg"><a href="#form"><button>Регистрация</button></a></div> 			</div>-->			<div class="onlineconf">			<font size="4"><b>V Конференция начинающих ортодонтов<br />состоялась 19 сентября 2015 г.</b></font>
			<br /><br />			Для просмотра записи конференции, нажмите значок PLAY в окне ниже<br /><br /></div>			<iframe width="610" height="400" src="https://www.youtube.com/embed/MeAVJSKC_cc" frameborder="0" allowfullscreen></iframe>			<br><br>
			<a href="http://dentalcomplex.com/company/beginners-2015/">Посмотреть фотоотчет с Конференции</a>
			<!--<div class="quest"><a href="#question"><button>Задать вопрос лектору</button></a></div>-->			<!--			<h3>Ретенционный период ортодонтического лечения: от выбора ретенционного аппарата до коррекции рецидива</h3>			<div class="group bordered">				<div class="left cities">					Санкт–Петербург				</div>				<div class="left dates">					25 – 26 апреля&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Сб–Вс)				</div>				<div class="left reg"><a href="#form"><button>Регистрация</button></a></div>			</div>-->			<div class="main">				<a name="about"></a><p>Ежегодную Конференцию начинающих ортодонтов проводит компания Ormco. В рамках мероприятия участникам предлагается насыщенная научная программа, возможность пообщаться с коллегами и получить призы и подарки от Ormco.</p>
				<p>КОНФЕРЕНЦИЯ НАЧИНАЮЩИХ ОРТОДОНТОВ – это отличная возможность:<br /><br />• получить максимум полезной информации за один день от ведущих специалистов<br /><br />• узнать о новинках ортодонтической отрасли<br /><br />• поближе познакомиться с компаниями ORMCO, KaVo и Kerr<br /><br />• пообщаться с коллегами и лекторами<br /><br />• узнать обо всех образовательных проектах ORMCO для стоматологов-ортодонтов <br />
				<hr>				<a name="programm"></a>				<h2>Программа</h2>
				<br>
				<div class="speakerdiv group">					<img class="speakerimg" src="images/coffee.jpg" />               	     	<br>               	     	<strong>09:00 – 09:45</strong>					<h4 class="speakerh4">Регистрация, welcome coffee</h4>               	     	<br>               	     	<br />               	     	<hr>			</div>               	               	  <div class="speakerdiv group">					<img class="speakerimg" src="images/open.jpg" />               	     	<br>               	     	<strong>09:00 – 09:45</strong>					<h4 class="speakerh4">Открытие конференции</h4>               	     	<br>               	     	<br />               	     	<hr>								</div>               	             			<div class="speakerdiv group">					<img class="speakerimg" src="images/popov.jpg" />               	     	<strong>10:00 – 11:00</strong>					<h4 class="speakerh4">Сергей ПОПОВ</h4>					врач-ортодонт, доктор медицинских наук, доцент, заведующий кафедрой ортодонтии СЗГМУ им. И. И. Мечникова					<br /><strong>«Роль ортодонтии в стоматологии»</strong>					<br />			</div>				               	               	               	       <div class="speakerdiv group">					<img class="speakerimg" src="images/markova.jpg" />               	     	<strong>11:00 – 12:00</strong>    					<h4 class="speakerh4">Мария МАРКОВА</h4>					врач-ортодонт, кандидат медицинских наук, ассистент кафедры ортодонтии МГМСУ им А.И. Евдокимова					<br />					<strong>«Ортодонтическое лечение с применением техники пассивного самолигирования Damon. Гистологическое  и физиологическое обоснование теории слабых сил» </strong>               	         	<br />               	         	<hr>               	 </div>				               	<div class="speakerdiv group">					<img class="speakerimg" src="images/lunch.jpg" />               	     	<br>               	     	<br>               	     	<strong>12:00 – 13:00</strong>					<h4 class="speakerh4">Ланч</h4>               	     	<br>               	     	               	     	<br>               	     	<hr>								</div>               	               	 <div class="speakerdiv group">					<img class="speakerimg" src="images/bulatova.jpg" />					<strong>13:00 – 13:45</strong>					<h4 class="speakerh4">Галина БУЛАТОВА</h4>					врач-ортодонт, главный врач клиники «Iceberg Dental Trauma Center» (Москва)					<br /><strong>«Междисциплинарное взаимодействие: ортодонтия и косметическая стоматология» </strong>					<br />			</div>			<div class="speakerdiv group">					<img class="speakerimg" src="images/blum.jpg" />					<strong>13:45 – 14:45</strong>					<h4 class="speakerh4">Станислав БЛУМ</h4>					врач-ортодонт, главный врач Центра функциональной ортодонтии и протезирования «Никольский»  (Санкт-Петербург)					<br /><strong>«Гибридная ортодонтия: функция, стабильность и скорость в комплексном ортодонтическом лечении. <br /> Диагностика с помощью InVivo, Arcus Digma и Insignia»</strong>					<br />			</div>			<div class="speakerdiv group">					<img class="speakerimg" src="images/zernov.jpg" />					<strong>14:45 – 15:45</strong>					<h4 class="speakerh4">Кирилл ЗЕРНОВ</h4>					Врач-ортодонт ООО"Стоматологическая клиника" (г. Тверь)					<br /><strong>«Ортодонтическое лечение с удалением зубов на брекет-системе Damon. Факторы, влияющие на принятие решения об удалении»</strong>					<br />			</div>			<div class="speakerdiv group">					<img class="speakerimg" src="images/idk.jpg" />					<strong>15:45 – 16:15</strong>					<h4 class="speakerh4">Дмитрий И</h4>					врач-ортодонт клиники эстетической ортодонтии «Конфиденция» (Санкт-Петербург)					<br /><strong>«Back to school! Школа ортодонтии ORMCO: все, что вы хотели знать об ортодонтии, но боялись спросить»</strong>					<br />					<hr>			</div>			<div class="speakerdiv group">					<img class="speakerimg" src="images/coffee.jpg" />               	     	<br>               	     	<br>               	     	<strong>16:15 – 16:30</strong>					<h4 class="speakerh4">Кофе-брейк</h4>               	     	<br>               	     	<br>               	               	         	<hr>								</div>               	               	 <div class="speakerdiv group">					<img class="speakerimg" src="images/zhuk.jpg" />					<strong>16:30 – 17:30</strong>					<h4 class="speakerh4">Андрей ЖУК</h4>					врач-ортодонт, кандидат медицинских наук, доцент кафедры стоматологии детского возраста и ортодонтии Первого МГМУ им. И.М. Сеченова					<br /><strong>«Клинически обоснованный подход к лечению аномалий челюстно-лицевой области»</strong>					<br />			</div>					               	<div class="speakerdiv group">					<img class="speakerimg" src="images/ermakov.jpg" />					<strong>17:30 – 18:30</strong>					<h4 class="speakerh4">Алексей ЕРМАКОВ</h4>					врач-ортодонт Центра функциональной стоматологии (г. Зеленоград)					<br /><strong>«7 преимуществ системы Insignia для клинической практики» </strong>					<br />			</div>				               	 <div class="speakerdiv group">					<strong>Розыгрыш подарков от ORMCO</strong> <sup>*</sup><br /><br />               	     <sup>*</sup> <b>Розыгрыш проводится только среди участников Конференции, лично присутствующих на мероприятии в гостинице «Аэростар» 19 сентября.</b> 				<!--               	     <p>Розыгрыш осуществляется путем вытягивания купонов каждым из участников в обмен на заполненную анкету</p>				<p>Для получения подарка необходимо обратиться в офис ООО «Ормко»:				<br />Москва, Ленинградский пр., д. 37, корп. 9, тел.: (495) 664-75-55</p>-->				</div>				<hr>				<a name="place"></a>				<h2>Где проводилась Конференция начинающих ортодонтов</h2>				<div class="places">					<span>19 сентября 2015 г., 9.00 – 19.00</span>					<h4>Москва</h4>					гостиница «Аэростар», Ленинградский проспект, д. 37, корп. 9<br />					<span onClick="map1(); $('.map').css('top', 100);">На карте</span>				</div>				<!--<div class="places">					<h4>Санкт-Петербург</h4>					Конференц-зал «Кинотеатр» гостиницы «Англетер»<br />					(Исаакиевская пл., ул. Малая Морская, 24)<br />					<span onClick="map2(); $('.map').css('top', 100);">На карте</span>				</div>-->				<hr>				<!--				<a name="price"></a>				<h2>Стоимость</h2>				<div class="prices">					<div class="group">						<div class="left pricedesc">Стоимость участия</div>						<div class="left price">Бесплатно <sup>*</sup></div>					</div>					<div class="pricedesc"><br/ ><sup>*</sup> Обязательна предварительная регистрация!</div>										<div class="small"><sup>*</sup> При условии участия в двух семинарах Блума С.А. "Комплексный подход к ведению ортодонтических пациентов на системе Damon" и "Ретенционный период ортодонтического лечения" предоставляется скидка 50% на второй семинар.</div>					<div class="group">						<div class="left pricedesc">Вторая ступень. Продвинутый уровень</div>						<div class="left price">18 000 руб.</div>					</div>									</div>-->				        <!--               	 <hr>				<a name="form"></a>				<h2>Регистрация</h2>				<div class="form">					<div class="err" id="err">						<?						if (md5($_POST["cap"]) != $_SESSION["captcha"] && $_POST["cap"] != "")							{							?>							<br />							<span class="red">Код введен неверно или устарел.</span>							<?						}						elseif ($_POST["fname"] != "" && ($_POST["phone"] != "" || $_POST["email"] != ""))							{							$emailto = "Maria.Ershova@ormco.com , Evgenya.Panova@ormco.com, Natalia.Kataeva@ormco.com";							// $emailto = "taran1972@gmail.com";							$email = "info@dentalcomplex.com";							$message = "Здравствуйте!С сайта dentalcomplex поступила заявка на конференцию начинающих ортодонтов:----------------------------------------Указанный EMail: ".$_POST["email"]."Указанный телефон: ".$_POST["phone"]."ИМЯ: ".$_POST["fname"]." ".$_POST["name"]." ".$_POST["sname"]."Место учёбы : ".$_POST["splace"]." Место работы: ".$_POST["wplace"]."Статус:  ".$_POST["occup"]." ";						$message1 = "Здравствуйте!Вы зарегистрировались на конференцию начинающих ортодонтов 19 сентября 2015 г.,Задать любые интересующие вопросы Вы можете по телефонам (812) 324-74-14, (495) 664-75-55.";							$header = "From:".$email."\nX-Mailer: PHP Auto-Mailer\nContent-Type: text/plain; charset=utf-8";							$subj = "Message from dentalcomplex register conference 19 Sep";							// $message = iconv("utf-8", "koi8r", $message);							mail($emailto, $subj, $message, $header, "-f".$email) or die("Mailing failed 1");							if ($_POST["email"] != "")								mail($_POST["email"], "Запись на семинар", $message1, $header, "-f".$email) or die("Mailing failed 2");							?>							Спасибо! Ваше сообщение отправлено!							<?						}						else							{							if ($_POST["cap"] != "")								{								?>								<br />								<span class="red">Вы не ввели имя или контактные данные!</span>								<?							}						}						?>					</div>					<form action="#form" method="post" onSubmit="var foo = 0; $('.ness').each(function() {if ($(this).val() == '') {if (foo == 0) {$('html,body').animate({scrollTop: $(this).offset().top - 80}, 500);}; $(this).addClass('redfld'); foo = 1;}}); if (foo == 0) {if (this.cap.value != '') {this.submit();}else{this.cap.style.border='1px solid #FF0000'}}; return false;">						<!--<div class="ttl">Выберите город</div>						<div class="flds"><select name="sem" onChange="if (this.value == 'Москва') {$('#map2').hide(0); $('#map1').show(0);}else{$('#map1').hide(0); $('#map2').show(0);}">							<option value="Москва">Москва</option>							<option value="Санкт-Петербург">Санкт-Петербург</option>						</select></div>						<div class="fldp">							<div id="map1">Ленинградский проспект, д. 37, корп. 9<br /><span onClick="map1(); $('.map').css('top', 100);">На карте</span></div>							<div id="map2" style="display: none;">Малоохтинский пр., 64, к. 3<br /><span onClick="map2(); $('.map').css('top', 100);">На карте</span></div>						</div>						<div class="ttl">Стоимость</div>						<div class="fld"><span id="curprice">10 000 руб.</span></div>						<div class="ttl">Имя <sup>*</sup></div>						<div class="fld"><input class="ness" type="text" size="30" name="name" id="name" /></div>						<div class="ttl">Фамилия <sup>*</sup></div>						<div class="fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>						<div class="ttl">Отчество <sup>*</sup></div>						<div class="fld"><input type="text" size="30" name="sname" id="sname" /></div>						<div class="ttl">Место учёбы <sup>*</sup></div>						<div class="fld"><input class="ness" type="text" size="30" name="splace" id="splace" /></div>						<div class="ttl"></div>						<div class="fld"><div class="small">Укажите учебное заведение, в котором Вы учитесь в настоящее время, или которое уже окончили<br /><br /></div></div>						<div class="ttl">Место работы</div>						<div class="fld"><input type="text" size="30" name="wplace" id="wplace" /></div>						<div class="ttl">Статус <sup>*</sup></div>						<div class="fld"><select class="ness" name="occup" id="occup">								<option value="студент">студент</option>								<option value="ординатор">ординатор</option>								<option value="врач">врач</option>							</select>						</div>						<div class="ttl">Email <sup>*</sup></div>						<div class="fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>						<div class="ttl">Телефон <sup>*</sup></div>						<div class="fld"><input class="ness" type="text" size="25" name="phone" id="phone" /></div>						<div class="ttlc">Введите код, указанный на картинке <sup>*</sup></div>						<div class="fldc"><img id="captcha" name="captcha" src="captcha.php" /><input type="text" size="5" name="cap" id="cap" /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="smll" onClick="reload();">другие цифры</span></div>						<div class="ttlb"></div>						<button>Зарегистрироваться на конференцию</button>						<div class="clear"></div>					</form>				</div>				<hr> -->               	               	          				<!-- ############################## -->               	 <!--				<a name="question"></a>				<h2>Задать вопрос лектору</h2>				<div class="form">					<div class="err" id="err">						<?						if (md5($_POST["qcap"]) != $_SESSION["qcaptcha"] && $_POST["qcap"] != "")							{							?>							<br />							<span class="red">Код введен неверно или устарел.</span>							<?						}						elseif ($_POST["question_name"] != "" && ($_POST["question_text"] != "" || $_POST["question_email"] != ""))							{							$emailto = "Maria.Ershova@ormco.com , Evgenya.Panova@ormco.com, Natalia.Kataeva@ormco.com";							//$emailto = "taran1972@gmail.com";							$email = "info@dentalcomplex.com";							$message = "Здравствуйте!С сайта dentalcomplex поступил вопрос лектору:----------------------------------------ИМЯ: ".$_POST["question_name"]."EMail: ".$_POST["question_email"]."Лектор: ".$_POST["question_lector"]."Вопрос:  ".$_POST["question_text"]." ";						$message1 = "Здравствуйте!Ваш вопрос будет задан лектору на конференции.";							$header = "From:".$email."\nX-Mailer: PHP Auto-Mailer\nContent-Type: text/plain; charset=utf-8";							$subj = "Message from dentalcomplex question for lector";							// $message = iconv("utf-8", "koi8r", $message);							mail($emailto, $subj, $message, $header, "-f".$email) or die("Mailing failed 1");							if ($_POST["question_email"] != "")								mail($_POST["question_email"], "Вопрос лектору", $message1, $header, "-f".$email) or die("Mailing failed 2");							?>							Спасибо! Ваше сообщение отправлено!							<?						}						else							{							if ($_POST["qcap"] != "")								{								?>								<br />								<span class="red">Вы не ввели имя, e-mail или не задали вопрос!</span>								<?							}						}						?>					</div>					<form action="#question" method="post" onSubmit="var qfoo = 0; $('.qness').each(function() {if ($(this).val() == '') {if (qfoo == 0) {$('html,body').animate({scrollTop: $(this).offset().top - 80}, 500);}; $(this).addClass('redfld'); qfoo = 1;}}); if (qfoo == 0) {if (this.qcap.value != '') {this.submit();}else{this.qcap.style.border='1px solid #FF0000'}}; return false;">						<div class="ttl">Имя <sup>*</sup></div>						<div class="fld"><input class="qness" type="text" size="30" name="question_name" id="question_name" /></div>						<div class="ttl">Email <sup>*</sup></div>						<div class="fld"><input class="qness" type="text" size="30" name="question_email" id="question_email" /></div>						<div class="ttl">Кому <sup>*</sup></div>						<div class="fld"><select class="qness" name="question_lector" id="question_lector">								<option value="лектор 1">Сергей Попов</option>								<option value="лектор 2">Мария Маркова</option>								<option value="лектор 3">Галина Булатова</option>               	               	  <option value="лектор 4">Станислав Блум</option>               	               	  <option value="лектор 5">Кирилл Зернов</option>               	               	  <option value="лектор 6">Дмитрий И</option>               	               	  <option value="лектор 7">Андрей Жук</option>               	               	  <option value="лектор 8">Алексей Ермаков</option>							</select>						</div>						<div class="ttl">Текст вопроса <sup>*</sup></div>						<div class="fld fld1"><textarea class="qness" name="question_text" id="question_text" rows="3" cols="25"></textarea></div>						<div class="ttlc">Введите код, указанный на картинке <sup>*</sup></div>						<div class="fldc"><img id="qcaptcha" name="qcaptcha" src="qcaptcha.php" /><input type="text" size="5" name="qcap" id="qcap" /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="smll" onClick="qreload();">другие цифры</span></div>						<div class="ttlb"></div>						<button>Задать вопрос</button>						<div class="clear"></div>					</form>				</div>				<hr>								-->							<a name="contacts"></a>				<h2>Контакты</h2>				<div class="contacts">					<strong>125167, Москва, Ленинградский проспект, д. 37, корп. 9</strong><br />					Тел.: (495) 664-75-55; факс: (495) 664-75-56					<br />					<br />					<strong>95112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3</strong><br />					Тел. (812) 324-74-14; факс (812) 320-20-52 					<br />					<br />					<a href="mailto:dc-sales@ormco.com">dc-sales@ormco.com</a>					<br />					<br />					<a href="http://ormcorussia.com">ormcorussia.com</a>				</div>			</div>		</div>	</div>	<div class="section to1000 footer">		Ormco Corporation &copy; 2015<br />		<a href="http://ormcorussia.com">ormcorussia.com</a>	</div>	<div class="map"><div class="close"><img src="images/close.png" onClick="$('.map').css('top', 10000);" /></div><div id="map"></div></body>
</html>