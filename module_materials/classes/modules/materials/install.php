<?php

$INFO = Array();

$INFO['name'] = "materials";
$INFO['filename'] = "modules/materials/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/materials/__admin.php";
$COMPONENTS[1] = "./classes/modules/materials/class.php";
$COMPONENTS[2] = "./classes/modules/materials/i18n.php";
$COMPONENTS[3] = "./classes/modules/materials/lang.php";
$COMPONENTS[4] = "./classes/modules/materials/permissions.php";
?>