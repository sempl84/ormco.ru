<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-materials-lists" => "Группы и страницы",
"header-materials-config" => "Настройки модуля",
"header-materials-add" => "Добавление",
"header-materials-edit" => "Редактирование",
 
"header-materials-add-groupelements"   => "Добавление группы",
"header-materials-add-item_element"     => "Добавление страницы",
 
"header-materials-edit-groupelements"   => "Редактирование группы",
"header-materials-edit-item_element"     => "Редактирование страницы",
 
"header-materials-activity" => "Изменение активности",
 
'perms-materials-view' => 'Просмотр страниц',
'perms-materials-lists' => 'Управление страницами',

'module-materials' => 'Маркетинговые материалы',
 
);
 
?>