﻿var menuService = {
    init: function () {
        $('.js-product-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    }
}

var topArrow = {
    init: function () {
        var $topLink = $('<a/>').attr('href', 'javascript:void(0)').addClass('top-arrow').appendTo($(document.body));
        $topLink.click(function () {
            $('body,html').stop().animate({ scrollTop: 0 }, 500);
            return false;
        });

        $(window).scroll(function () {
            if ($(document).scrollTop() < 50)
                $topLink.hide();
            else
                $topLink.css('display', 'block');
        }).scroll();
    }
}

var slider = {
    init: function () {
        var numberInRow = -1;
        $('.js-slider').carousel({
            interval: 10000
        });
        function setSiderItems() {
            $('.js-slider .item').each(function () {
                var next = $(this).next();
                $(this).children().slice(1).remove();
                for (var i = 0; i < numberInRow; i++) {
                    if (!next.length)
                        next = $(this).siblings(':first');
                    next.children(':first-child').clone().appendTo($(this));
                    //set next element
                    next = next.next();
                }
                $(this).children().css('width', 1 / (numberInRow + 1) * 100 + '%');
            });
        }

        $(window).resize(function () {

            if ($(window).width() > 752 && numberInRow != 3) {
                numberInRow = 3;
                setSiderItems();
            }
            else if ($(window).width() >= 400 && $(window).width() <= 752 && numberInRow != 1) {
                numberInRow = 1;
                setSiderItems();
            }
            else if ($(window).width() < 400 && numberInRow != 0) {
                numberInRow = 0;
                setSiderItems();
            }
        }).resize();
    }
}

var order = {
    init: function () {
        var $mainPanel = $('.js-order-form');
        if ($mainPanel.length != 0) {
            $mainPanel.find('.js-phone').mask('+7 (000) 0000-0000', { placeholder: "+7 (___) ____-____" });
        }
    }
}

var inputSpinner = {
    init: function () {
        $('.spinner').each(function () {
            var $pnl = $(this);
            if ($pnl.find('input:enabled').length != 0) {
                $pnl.find('input').change(function () {
                    if (isNaN($(this).val()) || parseInt($(this).val()) <= 0)
                        $(this).val(1);
                    else
                        $(this).val(parseInt($(this).val()));
                });
                $pnl.find('.btn:first-of-type').on('click', function () {
                    $pnl.find('input').val(parseInt($pnl.find('input').val()) + 1);
                });
                $pnl.find('.btn:last-of-type').on('click', function () {
                    if (parseInt($pnl.find('input').val()) > 1) {
                        $pnl.find('input').val(parseInt($pnl.find('input').val()) - 1);
                    } else
                        $pnl.find('input').val(1);
                });
            }
        });
    }
}

var initPhotoSwipeFromDOM = function (gallerySelector) {

    //add figure html tag from img alt tag
    $(gallerySelector).find('a').each(function () {
        var $lnk = $(this);
        var $img = $lnk.find('img:first');
        if ($lnk.find('figure').length == 0 && $img.length != 0) {
            var $figure = $('<figure/>').prependTo($lnk);
            if ($img.attr('alt'))
                $figure.text($img.attr('alt'));
        }
        else {
            var $figure = $('<figure/>').appendTo($lnk);
            if ($lnk.attr('title'))
                $figure.text($lnk.attr('title'));
        }
    });

    var parseThumbnailElements = function (el) {
        var thumbElements = $(el).find('a.js-photo').toArray(),
            numNodes = thumbElements.length,
            items = [],
            el,
            childElements,
            thumbnailEl,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {
            el = thumbElements[i];

            // include only element nodes 
            if (el.nodeType !== 1) {
                continue;
            }

            childElements = el.children;

            size = el.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: el.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10),
                author: '',
            };

            item.el = el; // save link to element for getThumbBoundsFn

            if (childElements.length > 0) {
                item.msrc = childElements[0].getAttribute('src'); // thumbnail url               
                item.title = childElements[0].innerHTML;

            }


            var mediumSrc = el.getAttribute('data-med');
            if (mediumSrc) {
                size = el.getAttribute('data-med-size').split('x');
                // "medium-sized" image
                item.m = {
                    src: mediumSrc,
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };
            }
            // original image
            item.o = {
                src: item.src,
                w: item.w,
                h: item.h
            };

            items.push(item);
        }

        return items;
    };

    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    var onThumbnailsClick = function (e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        var clickedListItem = closest(eTarget, function (el) {
            return el.tagName === 'A';
        });

        if (!clickedListItem) {
            return;
        }

        var clickedGallery = $(clickedListItem).parents('.js-photos:first')[0];

        var childNodes = $(clickedGallery).find('a').toArray(),
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if (childNodes[i].nodeType !== 1) {
                continue;
            }

            if (childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if (index >= 0) {
            openPhotoSwipe(index, clickedGallery);
        }
        return false;
    };

    var photoswipeParseHash = function () {
        var hash = window.location.hash.substring(1),
        params = {};

        if (hash.length < 5) { // pid=1
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        options = {

            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function (index) {
                // See Options->getThumbBoundsFn section of docs for more info
                var thumbnail = items[index].el.children[0],
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            },

            addCaptionHTMLFn: function (item, captionEl, isFake) {
                if (!item.title) {
                    captionEl.children[0].innerText = '';
                    return false;
                }
                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                return true;
            }

        };


        if (fromURL) {
            if (options.galleryPIDs) {
                for (var j = 0; j < items.length; j++) {
                    if (items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
            return;
        }

        var radios = document.getElementsByName('gallery-style');
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                if (radios[i].id == 'radio-all-controls') {

                } else if (radios[i].id == 'radio-minimal-black') {
                    options.mainClass = 'pswp--minimal--dark';
                    options.barsSize = { top: 0, bottom: 0 };
                    options.captionEl = false;
                    options.fullscreenEl = false;
                    options.shareEl = false;
                    options.bgOpacity = 0.85;
                    options.tapToClose = true;
                    options.tapToToggleControls = false;
                }
                break;
            }
        }

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

        var realViewportWidth,
            firstResize = true;

        gallery.listen('beforeResize', function () {

            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            dpiRatio = Math.min(dpiRatio, 2.5);
            realViewportWidth = gallery.viewportSize.x * dpiRatio;

            if (!firstResize) {
                gallery.invalidateCurrItems();
            }

            if (firstResize) {
                firstResize = false;
            }
        });

        gallery.listen('gettingData', function (index, item) {
            item.src = item.o.src;
            item.w = item.o.w;
            item.h = item.o.h;
        });

        gallery.init();
    };

    var galleryElements = document.querySelectorAll(gallerySelector);
    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
};


var dealers = {
    $map_canvas: null,
    $delears_tables: null,
    $ddl_cities: null,
    $ddl_coutries: null,
    dealers_data: [],
    all_cities_value: "",
    google_map: null,
    init_controls: function () {
        this.$map_canvas = $('.js-map-canvas');
        if (this.$map_canvas.length != 0) {
            var mapOptions = {
                center: new google.maps.LatLng(-34.397, 150.644),
                zoom: 8,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            this.google_map = new google.maps.Map(this.$map_canvas[0], mapOptions);
            this.$delears_tables = $('.js-dealers-table tbody');
            this.$ddl_cities = $('.js-cities').change(function () {
                dealers.set_dealers_data();
            });
            this.$ddl_coutries = $('.js-coutries').change(function () {
                dealers.set_cities_ddl();
                dealers.set_dealers_data();
            });
            this.all_cities_value = this.$ddl_cities.val();
            this.load_dealers_data();
        }
    },
    load_dealers_data: function () {
        $.ajax({
            type: "GET",
            url: "dealers.json",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function (data) {
                dealers.dealers_data = data;
                dealers.set_coutries_ddl();
                dealers.set_dealers_data();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus); alert("Error: " + errorThrown);
            }
        })
    },
    set_coutries_ddl: function () {
        var coutries = {};
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if (!coutries.hasOwnProperty(item.country)) {
                coutries[item.country] = 1;
                $("<option></option>").appendTo(this.$ddl_coutries).text(item.country).val(item.country);
            }
        }
    },
    set_cities_ddl: function () {
        var coutries = {};
        var selected_country = this.$ddl_coutries.val();
        this.$ddl_cities.empty();
        $("<option></option>").appendTo(this.$ddl_cities).text(this.all_cities_value).val(this.all_cities_value);
        var cities = [];
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if (!coutries.hasOwnProperty(item.city) && selected_country == item.country) {
                coutries[item.city] = 1;
                cities.push(item.city);

            }
        }
        cities.sort();
        for (var i = 0; i < cities.length; i++) {
            $("<option></option>").appendTo(this.$ddl_cities).text(cities[i]).val(cities[i]);
        }

        this.$ddl_cities.selectpicker('refresh');
    },
    set_dealers_data: function () {
        var count_item = 0;
        var last_location = null;
        var map_bounds = new google.maps.LatLngBounds();
        this.$delears_tables.empty();
        var selected_country = this.$ddl_coutries.val();
        var selected_city = this.$ddl_cities.val();
        var selected_country_index = this.$ddl_coutries.children().index(this.$ddl_coutries.find("option:selected"));
        var selected_city_index = this.$ddl_cities.children().index(this.$ddl_cities.find("option:selected"));
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if ((item.city == selected_city || selected_city_index == 0) &&
				(item.country == selected_country || selected_country_index == 0)) {
                var $row = $("<tr></tr>").appendTo(this.$delears_tables);
                $("<td></td>").appendTo($row).text(item.country);
                $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.webaddress).attr("target", "_blank").attr("rel", "nofollow").text(item.name);
                $("<td></td>").appendTo($row).text(item.address);
                $("<td></td>").appendTo($row).html(item.phone.split(',').join('<br/>'));
                $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.email).text(item.email);

                var location = new google.maps.LatLng(item.lat, item.lng);
                last_location = location;
                if (!item.marker) {
                    item.marker = this.create_dealer_marker(item, location);
                }
                item.marker.setMap(this.google_map);
                map_bounds.extend(location);
                count_item++;
            }
            else if (item.marker) {
                item.marker.setMap(null);
            }
        }

        if (count_item == 1) {
            this.google_map.setOptions({ center: last_location, zoom: 10 });
        }
        else {
            this.google_map.fitBounds(map_bounds);
        }

    },
    create_dealer_marker: function (dealer, location) {
        var marker = new google.maps.Marker({
            position: location,
            title: dealer.name,
            icon: '/img/marker-icon.png'
        });
        var infowindow = new google.maps.InfoWindow({
            content: '<b>' + dealer.name + '</b><br/>' + dealer.address + '<br/>' + dealer.phone
        });
        google.maps.event.addListener(marker, 'mouseover', function () {
            infowindow.open(dealers.google_map, marker);
        });
        google.maps.event.addListener(marker, 'mouseout', function () {
            infowindow.close();
        });
        return marker;
    }
}

var choiceBrackets = {
    init: function () {
        var $pnl = $('.js-choice-brackets');
        if ($pnl.length != 0) {
            function resetValues() {
                $pnl.find('.js-bottom-numbers input:enabled, .js-top-numbers input:enabled').val('');
                $pnl.find('.js-bottom-torc .tork_selector:not(.hide_it) select:enabled, .js-top-torc .tork_selector:not(.hide_it) select:enabled').each(function(){
                	$('option:eq(0)', $(this)).attr('selected', 'selected');;
                	$(this).selectpicker('refresh');
                })
            }
            $pnl.find('.js-select-top-bottom').click(function () {
                resetValues();
                $pnl.find('.js-bottom-numbers input:enabled, .js-top-numbers input:enabled').val(1);
                $pnl.find('.js-bottom-torc select:enabled, .js-top-torc select:enabled').val('ст.').selectpicker('refresh');
            	$pnl.find('.js-bottom-torc .tork_selector:not(.hide_it) select:enabled , .js-top-torc .tork_selector:not(.hide_it) select:enabled').each(function(){
                	$('option:eq(1)', $(this)).attr('selected', 'selected');;
                	$(this).selectpicker('refresh');
                })
            });

            $pnl.find('.js-select-top').click(function () {
                resetValues();
                $pnl.find('.js-top-numbers input:enabled').val(1);
                $pnl.find('.js-top-torc .tork_selector:not(.hide_it) select:enabled').each(function(){
                	$('option:eq(1)', $(this)).attr('selected', 'selected');;
                	$(this).selectpicker('refresh');
                })
            });

            $pnl.find('.js-select-bottom').click(function () {
                resetValues();
                $pnl.find('.js-bottom-numbers input:enabled').val(1);
                //$pnl.find('.js-bottom-torc select:enabled').val('ст.').selectpicker('refresh');
                $pnl.find('.js-bottom-torc .tork_selector:not(.hide_it) select:enabled').each(function(){
                	$('option:eq(1)', $(this)).attr('selected', 'selected');;
                	$(this).selectpicker('refresh');
                })
            });
        }
        
        // смена торков при выборе крючка
        // TODO добавить блокировку если нет брекетов без крючка
        $('.hk').change(function(){
        	console.log('change hk');
        	//вывести другой набор торков
        	// выяснить для какого зуба меняем торки и сменить их
        	var tooth = $(this).parents('td').data('tooth'),
        		hk_checked = ($(this).prop('checked')) ? 1 : 0,
        		tork_container = $('.js-top-torc td[data-tooth="'+tooth+'"]'),
        		selected_option = $('.selectpicker option:selected', $('.hk'+hk_checked ,tork_container)),
        		price = (selected_option.data('price')) ? selected_option.data('price') + ' руб.' : '-';
        	
    		$('.tork_selector' ,tork_container).addClass('hide_it');
    		$('.hk'+hk_checked ,tork_container).removeClass('hide_it');
    		$('.bracket_price').text(price);

        	//проверить галочку go, возможно нет брекетов со смещением если проставлен крючок (или наоборот есть), тогда надо проверить по какому-то классу и выставив нужное значение в галочке go заблокировать её (если её смена непрудостмотрена существующими брекетами) 
        });
        
        $('.tork_selector .selectpicker').change(function(){
        	var price = ($('option:selected', this).data('price'))? $('option:selected', this).data('price') + ' руб.' : '-';
        	
        	$('.bracket_price').text(price);
        });
    }
}

var productFilter = {
    init: function () {
        var $filterPnl = $('.js-filter');
        var $filterResults = $('.js-filter-results');

        if ($filterPnl.length != 0) {
            $filterPnl.find('input[type="checkbox"]').change(function () {
                var $ul = $(this).parents('ul');
                $(this).parents('li').attr('class', $(this).is(':checked') ? 'checked' : '');
                if ($ul.find('input:checked').length > 0)
                    $ul.prev().addClass('selected');
                else
                    $ul.prev().removeClass('selected');
            });
            $filterPnl.find('.close').click(function () {
                $(this).parent().removeClass('selected').next().find('input').attr('checked', false).parents('li').removeClass('checked');
                return false;
            });
            $filterResults.click(function () {
                $('html,body').animate({
                    scrollTop: $('.js-products-list').offset().top
                }, 'slow');
                return false;
            });

            function checkPosition() {
                if ($(window).width() < 977) {
                    var top = $(window).scrollTop() + $(window).height() - 100;
                    if ($filterPnl.offset().top < top && $filterPnl.offset().top + $filterPnl.height() > top)
                        $filterResults.show();
                    else
                        $filterResults.hide();
                }
                else
                    $filterResults.hide();
            }
            $(window).resize(function () {
                checkPosition();
            }).resize();

            $(window).scroll(function () {
                checkPosition();
            });
        }
    }
}

var collapseService = {
    init: function () {
        $('.js-collapse-pnl').on('hide.bs.collapse', function () {
            $('html,body').animate({
                scrollTop: $(this).offset().top - 100
            }, 600);
        });
    }
}

var googleMap = {
    init: function () {
        $('[data-lat]').each(function () {
            var lat = $(this).data('lat');
            var lng = $(this).data('lng');
            var zoom = $(this).data('zoom');
            var description = $(this).find('.js-description').html();
            var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: zoom,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var googleMap = new google.maps.Map($(this)[0], mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                title: $(this).data('title'),
                icon: '/img/marker-icon.png'
            });
            var infowindow = new google.maps.InfoWindow({
                content: description
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(googleMap, marker);
            });          

            marker.setMap(googleMap);
        })
    }
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

googleMap.init();
collapseService.init();
productFilter.init();
choiceBrackets.init();
dealers.init_controls();
slider.init();
menuService.init();
topArrow.init();
order.init();
inputSpinner.init();
initPhotoSwipeFromDOM('.js-photos');