<?php
	if (!isset($_REQUEST['id'])) {
		die();
	}

	$id = intval($_REQUEST['id']);

	require_once('standalone.php');

	try {
		$buffer = outputBuffer::current();
	} catch (coreException $e) {
		$buffer = outputBuffer::current('HTTPOutputBuffer');
	}

	if (!$buffer instanceof HTTPOutputBuffer) {
		$buffer->end();
	}

	$hierarchy = umiHierarchy::getInstance();
	$url = $hierarchy->getPathById($id);

	if ($url) {
		$buffer->redirect($url);
	} else {
		$buffer->status(404);
		$buffer->end();
	}

